// Modules
import path from 'path';
// Utils
import { CLIENT_CONSTANTS } from '@@S/../webpack/constants';

export const htmlPath = path.join(CLIENT_CONSTANTS.PATH_TO_PUBLIC_FOLDER, 'index.html');
