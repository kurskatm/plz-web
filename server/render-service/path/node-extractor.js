// Modules
import path from 'path';
import { ChunkExtractor } from '@loadable/server';
// Utils
import { SERVER_CONSTANTS } from '@@S/../webpack/constants';

const statsFileDev = path.join(SERVER_CONSTANTS.PATH_TO_SERVER_PUBLIC_FOLDER, 'stats-loadable.json');

export const nodeExtractor = new ChunkExtractor({
  inputFileSystem: global.fileSystem,
  statsFile: statsFileDev,
  entrypoints: ['app'],
  publicPath: path.join(SERVER_CONSTANTS.PATH_TO_SERVER_PUBLIC_FOLDER),
  outputPath: path.join(SERVER_CONSTANTS.PATH_TO_SERVER_PUBLIC_FOLDER),
});

export const { default: App } = nodeExtractor.requireEntrypoint();
