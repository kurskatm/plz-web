// Modules
import chalk from 'chalk';
// Utils
import { SERVER_CONSTANTS } from '@@S/../webpack/constants';

const { PROTOCOL, HOST, PORT } = SERVER_CONSTANTS;

export const mainMessages = {
  run: () => chalk`Server start on the {green.bold ${PROTOCOL}://${HOST}:${PORT}}...`,
  error: (error, ctx) => chalk`Server error: ${error}, ${ctx}`,
  close: () => chalk`{red.bold Server ${HOST}:${PORT} shutted down}...`,
};
