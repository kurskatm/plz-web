// Messages
// ---Main
import { mainMessages } from './main';

const serverMessages = {
  ...mainMessages,
};

Object.keys(serverMessages).forEach((key) => {
  const getMessage = serverMessages[key];
  serverMessages[key] = (...options) => console.log(getMessage(...options));
});

export { serverMessages };
