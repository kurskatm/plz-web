// Modules
import https from 'https';
import Koa from 'koa';
import serve from 'koa-static';
// Lib
import { RequestErrorsManager } from '@S/lib/request/errors';
import { KoaMiddleware } from '@S/lib/middleware';
import { getSertificate } from '@S/lib/web/https/get-certificate';
// import { KoaLogger } from '@Slib/logger';
import { requestDefaultSettings } from '@Slib/request/default-settings';
// Router
import { MainRouterManager } from '@Srouter';
// Utils
import { MAIN_CONSTANTS, SERVER_CONSTANTS } from '@@S/../webpack/constants';
import { getGracefulShutdown } from '@Sutils';
// Messages
import { serverMessages } from '@Smessages';

const options = getSertificate();
const app = new Koa();
const callback = () => {
  serverMessages.run();
  return app.callback();
};

const serverRun = async () => {
  app.use(serve(MAIN_CONSTANTS.APP_DIR));

  await new KoaMiddleware(app).init();
  // new KoaLogger(app).init();

  app.use(requestDefaultSettings);
  app.use(new RequestErrorsManager().setCallback);

  new MainRouterManager(app).init();

  app.on('error', (err, ctx) => {
    console.log(err);
    console.error(serverMessages.error(err, ctx));
  });

  const server = https.createServer(
    options,
    callback(),
  ).listen(SERVER_CONSTANTS.PORT);

  getGracefulShutdown(server);
};

export { serverRun };
