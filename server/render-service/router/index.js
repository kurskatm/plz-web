import { PageRouterManager } from './page-router';
import { SourcesRouter } from './sources-router';

export class MainRouterManager {
  constructor(app) {
    this.app = app;
  }

  init = () => {
    new SourcesRouter(this.app).init();
    new PageRouterManager(this.app).init();
  };
}
