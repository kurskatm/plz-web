// Modules
import path from 'path';
// Path
import { projectPath } from '@Spath/project-path';

export class SVGRouter {
  constructor(ctx) {
    this.ctx = ctx;
    this.url = ctx.request.url;
  }

  init = () => {
    const filePath = path
      .join(projectPath, this.url)
      .replace('assets', '')
      .replace('//', '/');

    this.ctx.type = 'image/svg+xml';

    try {
      const data = global.fileSystem
        .readFileSync(filePath, 'utf-8');
      this.ctx.status = 200;
      this.ctx.body = data;
    } catch (error) {
      console.log('Error read sources files');
      console.log(error);

      this.ctx.status = 404;
    }
  };
}
