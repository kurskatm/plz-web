// Utils
import { Consts } from '@utils';

const { IS_DEVELOPMENT } = Consts.ENV;

export const regexpObj = {
  jsRegExp: IS_DEVELOPMENT
    ? /(^(\/js)?\/[\w-./~]+.js$)/
    : /(^\/js\/[\w-./~]+.js$)/,
  cssRegExp: /(^\/css\/[\w-./~]+.css$)/,
  devRegExp: /^\/__webpack_hmr$/,
  hmrRegExp: /^\/[\w]+.hot-update.json$/,
  svgRegExp: /^\/assets\/svg\/[\w-./~]+.svg$/,
  imgRegExp: /^\/assets\/img\/[\w-./~]+.(png|jpe?g|gif)$/,
  fontsRegExp: /^\/assets\/fonts\/[\w-./~]+.(woff(2)?|ttf|eot|svg)$/,
};
