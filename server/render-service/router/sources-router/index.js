// Modules
import Router from '@koa/router';
// Utils
import { Consts } from '@Sutils';
// Routes
import { regexpObj } from './config';
import { CSSRouter } from './css-router';
import { DevRouter } from './dev-router';
import { HMRRouter } from './hmr-router';
import { JSRouter } from './js-router';
import { SVGRouter } from './svg-router';
import { IMGRouter } from './img-router';
import { FontsRouter } from './fonts-router';

const { IS_DEVELOPMENT } = Consts.ENV;

export class SourcesRouter {
  constructor(app) {
    this.app = app;
    this.router = new Router();
  }

  init = () => {
    if (IS_DEVELOPMENT) {
      this.router.get(regexpObj.devRegExp, async (ctx) => {
        new DevRouter(ctx).init();
      });

      this.router.get(regexpObj.hmrRegExp, async (ctx) => {
        new HMRRouter(ctx).init();
      });
    }

    this.router.get(regexpObj.jsRegExp, async (ctx) => {
      new JSRouter(ctx).init();
    });

    this.router.get(regexpObj.cssRegExp, async (ctx) => {
      new CSSRouter(ctx).init();
    });

    this.router.get(regexpObj.svgRegExp, async (ctx) => {
      new SVGRouter(ctx).init();
    });

    this.router.get(regexpObj.imgRegExp, async (ctx) => {
      new IMGRouter(ctx).init();
    });

    this.router.get(regexpObj.imgRegExp, async (ctx) => {
      new IMGRouter(ctx).init();
    });

    this.router.get(regexpObj.fontsRegExp, async (ctx) => {
      new FontsRouter(ctx).init();
    });

    this.app.use(this.router.routes());
  };
}
