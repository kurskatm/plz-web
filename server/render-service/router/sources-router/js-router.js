// Modules
import path from 'path';
// Path
import { projectPath } from '@Spath/project-path';

export class JSRouter {
  constructor(ctx) {
    this.ctx = ctx;
    this.url = ctx.request.url;
  }

  init = () => {
    const filePath = path.join(projectPath, this.url);
    this.ctx.type = 'application/javascript';

    try {
      const data = global.fileSystem
        .readFileSync(filePath, 'utf-8');
      this.ctx.status = 200;
      this.ctx.body = data;
    } catch (error) {
      console.log('Error read client script');
      console.log(error);

      this.ctx.status = 404;
    }
  };
}
