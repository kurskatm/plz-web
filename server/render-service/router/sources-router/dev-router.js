// Modules
import path from 'path';
// Path
import { projectPath } from '@Spath/project-path';

const PATH_TO_BUNDLE = '/js/bundle.js';

export class DevRouter {
  constructor(ctx) {
    this.ctx = ctx;
  }

  init = () => {
    const filePath = path.join(projectPath, PATH_TO_BUNDLE);
    this.ctx.type = 'text/event-stream';

    try {
      const data = global.fileSystem
        .readFileSync(filePath, 'utf-8');
      this.ctx.status = 200;
      this.ctx.body = data;
    } catch (error) {
      console.log('Error read client script');
      console.log(error);

      this.ctx.status = 404;
    }
  };
}
