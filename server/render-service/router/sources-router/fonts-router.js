// Modules
import path from 'path';
// Path
import { projectPath } from '@Spath/project-path';

export class FontsRouter {
  constructor(ctx) {
    this.ctx = ctx;
    this.url = ctx.request.url;
  }

  init = () => {
    const filePath = path
      .join(projectPath, this.url)
      .replace('assets', '')
      .replace('//', '/');

    this.ctx.type = 'application/octet-stream';

    try {
      const data = global.fileSystem
        .readFileSync(filePath);
      this.ctx.status = 200;
      this.ctx.body = data;
    } catch (error) {
      console.log('Error read sources files');
      console.log(error);

      this.ctx.status = 404;
    }
  };

  getExtension = () => {
    const extension = path.extname(this.url);

    if (extension) {
      return extension.slice(1);
    }

    return '';
  };
}
