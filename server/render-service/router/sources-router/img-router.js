// Modules
import path from 'path';
// Path
import { projectPath } from '@Spath/project-path';
// Lib
import { GetImgType } from '@Slib/response/types/img-types';

export class IMGRouter {
  constructor(ctx) {
    this.GetImgType = new GetImgType();
    this.ctx = ctx;
    this.url = ctx.request.url;
  }

  init = () => {
    const filePath = path
      .join(projectPath, this.url)
      .replace('assets', '')
      .replace('//', '/');

    const type = this.GetImgType.init(filePath);
    this.ctx.type = type;

    try {
      const data = global.fileSystem
        .readFileSync(filePath);
      this.ctx.status = 200;
      this.ctx.body = data;
    } catch (error) {
      console.log('Error read sources files');
      console.log(error);

      this.ctx.status = 404;
    }
  };
}
