// Modules
import Router from '@koa/router';
// Routes
import { InfoAboutRouter } from './about';
import { InfoContactsRouter } from './contacts';
import { InfoPartnersRouter } from './partners';
import { InfoReglamentRouter } from './reglament';
import { InfoHelpRouter } from './help';
import { InfoSupportRouter } from './support';
import { FreeFoodRouter } from './freeFood';
import { CheckoutRouter } from './checkout';

export class InfoPagesRouter {
  constructor(data) {
    this.router = new Router();
    this.GenerateApp = data.GenerateApp;
    this.getResponse = data.getResponse;
  }

  init = () => {
    this.setRoutes();
    return this.router.routes();
  };

  setRoutes = () => {
    new InfoAboutRouter({
      router: this.router,
      GenerateApp: this.GenerateApp,
      getResponse: this.getResponse,
    }).init();

    new InfoContactsRouter({
      router: this.router,
      GenerateApp: this.GenerateApp,
      getResponse: this.getResponse,
    }).init();

    new InfoPartnersRouter({
      router: this.router,
      GenerateApp: this.GenerateApp,
      getResponse: this.getResponse,
    }).init();

    new InfoReglamentRouter({
      router: this.router,
      GenerateApp: this.GenerateApp,
      getResponse: this.getResponse,
    }).init();

    new InfoHelpRouter({
      router: this.router,
      GenerateApp: this.GenerateApp,
      getResponse: this.getResponse,
    }).init();

    new InfoSupportRouter({
      router: this.router,
      GenerateApp: this.GenerateApp,
      getResponse: this.getResponse,
    }).init();

    new FreeFoodRouter({
      router: this.router,
      GenerateApp: this.GenerateApp,
      getResponse: this.getResponse,
    }).init();

    new CheckoutRouter({
      router: this.router,
      GenerateApp: this.GenerateApp,
      getResponse: this.getResponse,
    }).init();
  };
}
