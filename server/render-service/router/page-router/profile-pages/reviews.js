// Api
import { fetchCities } from '@Sapi/cities/fetch';
// Lib
import { SsrRequestManager } from '@Slib/request/ssr-request-manager';
// Models
import { initialState } from '@models';
// Utils
import { Consts } from '@utils';

const { PROFILE_PAGES } = Consts.ROUTES;

export class ProfileReviewsRouter {
  constructor(data) {
    this.router = data.router;
    this.GenerateApp = data.GenerateApp;
    this.getResponse = data.getResponse;
    this.requests = [fetchCities];
  }

  init = () => {
    this.router.get(
      PROFILE_PAGES.REVIEWS.PATH,
      async (ctx, next) => {
        const initialData = { ...initialState };
        const data = await new SsrRequestManager()
          .getData(initialData, this.requests, ctx);
        ctx.data = data;

        await next();
      },
      async (ctx) => {
        const html = this.GenerateApp.init(ctx, ctx.data);
        this.getResponse(ctx, html);
      },
    );
  };
}
