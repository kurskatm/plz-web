// Modules
import Router from '@koa/router';
// Routes
import { ProfileInfoRouter } from './info';
import { OrdersRouter } from './orders';
import { ProfileScoresRouter } from './scores';
import { ProfileReviewsRouter } from './reviews';
import { ProfileFavoritesRouter } from './favorites';

export class ProfilePagesRouter {
  constructor(data) {
    this.router = new Router();
    this.GenerateApp = data.GenerateApp;
    this.getResponse = data.getResponse;
  }

  init = () => {
    this.setRoutes();
    return this.router.routes();
  };

  setRoutes = () => {
    new ProfileInfoRouter({
      router: this.router,
      GenerateApp: this.GenerateApp,
      getResponse: this.getResponse,
    }).init();

    new OrdersRouter({
      router: this.router,
      GenerateApp: this.GenerateApp,
      getResponse: this.getResponse,
    }).init();

    new ProfileScoresRouter({
      router: this.router,
      GenerateApp: this.GenerateApp,
      getResponse: this.getResponse,
    }).init();

    new ProfileReviewsRouter({
      router: this.router,
      GenerateApp: this.GenerateApp,
      getResponse: this.getResponse,
    }).init();

    new ProfileFavoritesRouter({
      router: this.router,
      GenerateApp: this.GenerateApp,
      getResponse: this.getResponse,
    }).init();
  };
}
