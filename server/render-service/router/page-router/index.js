// Lib
import { GenerateApp } from '@Slib/router/generate-app';
// Routes
import { MainRouter } from './main';

export class PageRouterManager {
  constructor(app) {
    this.app = app;
    this.GenerateApp = new GenerateApp();
  }

  init = () => {
    new MainRouter({
      app: this.app,
      GenerateApp: this.GenerateApp,
      getResponse: this.getResponse,
    }).init();
  };

  getResponse = (ctx, html) => {
    ctx.type = 'text/html';

    if (html) {
      ctx.status = 200;
      ctx.body = html;
    } else {
      ctx.status = 404;
      ctx.body = 'Content not found';
    }
  }
}
