// Modules
import Router from '@koa/router';
// Utils
import { Consts } from '@utils';
// Lib
import { CheckCity } from '@Slib/router/city-middleware/check-city';
// Routes
import { InfoPagesRouter } from '../info-pages';
import { CityEndpoint } from './city-endpoint';
import { RestaurantPagesRouter } from '../restaurant-pages';
import { CheckoutPagesRouter } from '../checkout-page';
import { SpecializationPageRouter } from '../specialization-page';

const { HOME } = Consts.ROUTES;

export class CityRouter {
  constructor(data) {
    this.router = new Router();
    this.CheckCity = new CheckCity();
    this.GenerateApp = data.GenerateApp;
    this.getResponse = data.getResponse;
  }

  init = () => {
    this.router.use(HOME.PATH, this.CheckCity.init);
    this.setRoutes();

    return this.router.routes();
  };

  setRoutes = () => {
    this.router.use(
      HOME.PATH,
      new CityEndpoint({
        GenerateApp: this.GenerateApp,
        getResponse: this.getResponse,
      }).init(),
    );

    // this.router.use(
    //   HOME.PATH,
    //   new CheckoutPagesRouter({
    //     GenerateApp: this.GenerateApp,
    //     getResponse: this.getResponse,
    //   }).init(),
    // );

    this.router.use(
      HOME.PATH,
      new InfoPagesRouter({
        GenerateApp: this.GenerateApp,
        getResponse: this.getResponse,
      }).init(),
    );

    this.router.use(
      HOME.PATH,
      new RestaurantPagesRouter({
        GenerateApp: this.GenerateApp,
        getResponse: this.getResponse,
      }).init(),
    );

    // this.router.use(
    //   HOME.PATH,
    //   new ProfilePagesRouter({
    //     GenerateApp: this.GenerateApp,
    //     getResponse: this.getResponse,
    //   }).init(),
    // );
    this.router.use(
      HOME.PATH,
      new SpecializationPageRouter({
        GenerateApp: this.GenerateApp,
        getResponse: this.getResponse,
      }).init(),
    );
  };
}
