// Modules
import Router from '@koa/router';
// Api
import { fetchCities } from '@Sapi/cities/fetch';
import { fetchFilters } from '@Sapi/filters/fetch';
import { getRestaurantsAndHits } from '@Sapi/restaurant-and-hits/fetch';
import { fetchCuisines } from '@Sapi/cuisines/fetch';

// Lib
import { SsrRequestManager } from '@Slib/request/ssr-request-manager';
// Models
import { initialState } from '@models';

export class CityEndpoint {
  constructor(data) {
    this.router = new Router();
    this.GenerateApp = data.GenerateApp;
    this.getResponse = data.getResponse;
    this.requests = [
      // fetchCities,
      // fetchFilters,
      // fetchCuisines,
      // getRestaurantsAndHits,
    ];
  }

  init = () => {
    this.router.get(
      '/',
      async (ctx, next) => {
        const data = await new SsrRequestManager()
          .getData(initialState, this.requests, ctx);

        ctx.data = data;
        await next();
      },
      async (ctx) => {
        const html = this.GenerateApp.init(ctx, ctx.data);
        this.getResponse(ctx, html);
      },
    );

    return this.router.routes();
  };
}
