// Modules
import Router from '@koa/router';
// Utils
import { Consts } from '@utils';

const { ERRORS } = Consts.ROUTES;

export class ErrorPagesRouter {
  constructor(data) {
    this.router = new Router();
    this.GenerateApp = data.GenerateApp;
    this.getResponse = data.getResponse;
  }

  init = () => {
    this.setRoutes();
    return this.router.routes();
  };

  setRoutes = () => {
    this.router.get(ERRORS.ERROR_404.PATH, async (ctx) => {
      const html = this.GenerateApp.init(ctx);
      this.getResponse(ctx, html);
    });

    this.router.get(ERRORS.ERROR_500.PATH, async (ctx) => {
      const html = this.GenerateApp.init(ctx);
      this.getResponse(ctx, html);
    });

    this.router.get(ERRORS.ERROR_404_TECHNICAL_WOKRS.PATH, async (ctx) => {
      const html = this.GenerateApp.init(ctx);
      this.getResponse(ctx, html);
    });

    this.router.get(ERRORS.ERROR_502.PATH, async (ctx) => {
      const html = this.GenerateApp.init(ctx);
      this.getResponse(ctx, html);
    });
  };
}
