// Modules
import Router from '@koa/router';
// Routes
import { CheckoutRouter } from './checkout';
import { CheckoutSuccessRouter } from './checkout-success';

export class CheckoutPagesRouter {
  constructor(data) {
    this.router = new Router();
    this.GenerateApp = data.GenerateApp;
    this.getResponse = data.getResponse;
  }

  init = () => {
    this.setRoutes();
    return this.router.routes();
  };

  setRoutes = () => {
    new CheckoutRouter({
      router: this.router,
      GenerateApp: this.GenerateApp,
      getResponse: this.getResponse,
    }).init();
    new CheckoutSuccessRouter({
      router: this.router,
      GenerateApp: this.GenerateApp,
      getResponse: this.getResponse,
    }).init();
  };
}
