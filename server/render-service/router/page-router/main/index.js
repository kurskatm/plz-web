// Modules
import Router from '@koa/router';
// Routes
import { ErrorPagesRouter } from '../error-pages';
import { CityRouter } from '../city-router';
import { CheckoutPagesRouter } from '../checkout-page';
import { SpecializationPageRouter } from '../specialization-page';
import { SelectionPageRouter } from '../selection-page';
import { ProfilePagesRouter } from '../profile-pages';
// Endpoints
import { HomeCityEndpoint } from './home-city-endpoint';

export class MainRouter {
  constructor(data) {
    this.router = new Router();
    this.app = data.app;
    this.GenerateApp = data.GenerateApp;
    this.getResponse = data.getResponse;
  }

  init = () => {
    this.setRoutes();

    this.app.use(
      this.router.routes(),
      this.router.allowedMethods(),
    );
  };

  setRoutes = () => {
    new HomeCityEndpoint({
      router: this.router,
      GenerateApp: this.GenerateApp,
      getResponse: this.getResponse,
    }).init();

    this.router.use(new ErrorPagesRouter({
      GenerateApp: this.GenerateApp,
      getResponse: this.getResponse,
    }).init());

    this.router.use(new CityRouter({
      GenerateApp: this.GenerateApp,
      getResponse: this.getResponse,
    }).init());

    this.router.use(new CheckoutPagesRouter({
      GenerateApp: this.GenerateApp,
      getResponse: this.getResponse,
    }).init());

    this.router.use(new SpecializationPageRouter({
      GenerateApp: this.GenerateApp,
      getResponse: this.getResponse,
    }).init());

    this.router.use(new SelectionPageRouter({
      GenerateApp: this.GenerateApp,
      getResponse: this.getResponse,
    }).init());

    this.router.use(new ProfilePagesRouter({
      GenerateApp: this.GenerateApp,
      getResponse: this.getResponse,
    }).init());
  };
}
