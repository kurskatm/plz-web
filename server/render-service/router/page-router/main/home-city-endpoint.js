// Api
import { fetchCities } from '@Sapi/cities/fetch';
import { getRestaurantsAndHits } from '@Sapi/restaurant-and-hits/fetch';
import { fetchCuisines } from '@Sapi/cuisines/fetch';
import { fetchHotOffers } from '@Sapi/hotOffers/fetch';

// Lib
import { SsrRequestManager } from '@Slib/request/ssr-request-manager';
import { CityRouterMiddleware } from '@Slib/router/city-middleware/city-home';
// Models
import { initialState } from '@models';
// Utils
import { Consts } from '@utils';

const { CITIES } = Consts.ROUTES;

export class HomeCityEndpoint {
  constructor(data) {
    this.router = data.router;
    this.CityRouterMiddleware = new CityRouterMiddleware();
    this.GenerateApp = data.GenerateApp;
    this.getResponse = data.getResponse;
    this.requests = [];
  }

  init = () => {
    this.router.get(
      CITIES.PATH,
      this.CityRouterMiddleware.init,
      async (ctx, next) => {
        const initialData = { ...initialState };
        const data = await new SsrRequestManager()
          .getData(initialData, this.requests, ctx);
        ctx.data = data;

        await next();
      },

      async (ctx) => {
        const html = this.GenerateApp.init(ctx, ctx.data);
        this.getResponse(ctx, html);
      },
    );
  };
}
