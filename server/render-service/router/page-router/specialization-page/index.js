// Modules
import Router from '@koa/router';
// Lib
import { SpecializationRouterMiddleware } from '@Slib/router/specialization-middleware';
// Utils
import { Consts } from '@utils';
// Routes
import { SpecializationRouter } from './specialization';

const { SPECIALIZATION } = Consts.ROUTES;

export class SpecializationPageRouter {
  constructor(data) {
    this.router = new Router();
    this.GenerateApp = data.GenerateApp;
    this.getResponse = data.getResponse;
    this.SpecializationRouterMiddleware = new SpecializationRouterMiddleware();
  }

  init = () => {
    this.router.use(
      SPECIALIZATION.PATH,
      this.SpecializationRouterMiddleware.init,
    );
    this.setRoutes();
    return this.router.routes();
  };

  setRoutes = () => {
    new SpecializationRouter({
      router: this.router,
      GenerateApp: this.GenerateApp,
      getResponse: this.getResponse,
    }).init();
  };
}
