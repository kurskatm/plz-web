// Api
import { fetchCities } from '@Sapi/cities/fetch';
import { fetchContacts } from '@Sapi/contacts/fetch';
import { fetchFilters } from '@Sapi/filters/fetch';
import { fetchShopingCart } from '@Sapi/shopingCart/fetch';
import { getRestaurantsAndHits } from '@Sapi/restaurant-and-hits/fetch';
import { fetchCuisines } from '@Sapi/cuisines/fetch';
import { fetchHotOffers } from '@Sapi/hotOffers/fetch';
// Lib
import { SsrRequestManager } from '@Slib/request/ssr-request-manager';
// Models
import { initialState } from '@models';
// Utils
import { Consts } from '@utils';

const { SPECIALIZATION } = Consts.ROUTES;

export class SpecializationRouter {
  constructor(data) {
    this.router = data.router;
    this.GenerateApp = data.GenerateApp;
    this.getResponse = data.getResponse;
    this.requests = [
      fetchCities,
      fetchContacts,
      fetchFilters,
      fetchCuisines,
      getRestaurantsAndHits,
      fetchShopingCart,
      fetchHotOffers,
    ];
  }

  init = () => {
    this.router.get(
      SPECIALIZATION.PATH,
      async (ctx, next) => {
        const data = await new SsrRequestManager()
          .getData(initialState, this.requests, ctx);
        ctx.data = data;

        await next();
      },
      async (ctx) => {
        const html = this.GenerateApp.init(ctx, ctx.data);
        this.getResponse(ctx, html);
      },
    );
  };
}
