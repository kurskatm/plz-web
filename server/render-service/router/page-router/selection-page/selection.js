// Api
import { fetchCities } from '@Sapi/cities/fetch';
import { fetchContacts } from '@Sapi/contacts/fetch';
import { getRestaurantsAndHits } from '@Sapi/restaurant-and-hits/fetch';
// Lib
import { SsrRequestManager } from '@Slib/request/ssr-request-manager';
// Models
import { initialState } from '@models';
// Utils
import { Consts } from '@utils';

const { SELECTION } = Consts.ROUTES;

export class SelectionRouter {
  constructor(data) {
    this.router = data.router;
    this.GenerateApp = data.GenerateApp;
    this.getResponse = data.getResponse;
    this.requests = [
      fetchCities,
      fetchContacts,
      getRestaurantsAndHits,
    ];
  }

  init = () => {
    this.router.get(
      SELECTION.PATH,
      async (ctx, next) => {
        const data = await new SsrRequestManager()
          .getData(initialState, this.requests, ctx);
        ctx.data = data;

        await next();
      },
      async (ctx) => {
        const html = this.GenerateApp.init(ctx, ctx.data);
        this.getResponse(ctx, html);
      },
    );
  };
}
