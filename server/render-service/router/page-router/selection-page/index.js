// Modules
import Router from '@koa/router';
// Lib
import { SpecializationRouterMiddleware } from '@Slib/router/specialization-middleware';
// Utils
import { Consts } from '@utils';
// Routes
import { SelectionRouter } from './selection';

const { SELECTION } = Consts.ROUTES;

export class SelectionPageRouter {
  constructor(data) {
    this.router = new Router();
    this.GenerateApp = data.GenerateApp;
    this.getResponse = data.getResponse;
    this.SpecializationRouterMiddleware = new SpecializationRouterMiddleware();
  }

  init = () => {
    this.router.use(
      SELECTION.PATH,
      this.SpecializationRouterMiddleware.init,
    );
    this.setRoutes();
    return this.router.routes();
  };

  setRoutes = () => {
    new SelectionRouter({
      router: this.router,
      GenerateApp: this.GenerateApp,
      getResponse: this.getResponse,
    }).init();
  };
}
