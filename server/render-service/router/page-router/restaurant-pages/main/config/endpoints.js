// Modules
import urlJoin from 'url-join';
// Utils
import { Consts } from '@utils';

const { CITIES } = Consts.ROUTES;
const { RESTAURANT } = Consts.ROUTES.RESTAURANTS;

export const endpoint = urlJoin(
  RESTAURANT.MAIN.PATH,
  RESTAURANT.MAIN.PARAMS,
);

export const getRedirectEndpoint = ({
  cityName,
  restaurantName,
}) => urlJoin(
  CITIES.PATH,
  cityName,
  RESTAURANT.MAIN.PATH,
  restaurantName,
  RESTAURANT.SECTIONS.MENU.PATH,
);
