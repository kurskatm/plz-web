// Config
import {
  endpoint,
  getRedirectEndpoint,
} from './config/endpoints';

export class RestaurantMainRouter {
  constructor(data) {
    this.router = data.router;
  }

  init = () => {
    this.router.get(
      endpoint,
      async (ctx) => {
        const redirectEndpoint = getRedirectEndpoint({
          cityName: ctx?.params?.cityUrl,
          restaurantName: ctx?.params?.restaurantName,
        });

        ctx.redirect(redirectEndpoint);
        ctx.status = 301;
      },
    );
  };
}
