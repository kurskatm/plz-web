// Modules
import urlJoin from 'url-join';
// Api
import { fetchCities } from '@Sapi/cities/fetch';
import { fetchContacts } from '@Sapi/contacts/fetch';
import { getRestaurantId } from '@Sapi/restaurant/get-id-restaurant';
import { fetchRestaurantCard } from '@Sapi/restaurant/fetch-restaurant-card';
import { fetchRestaurantCategories } from '@Sapi/restaurant/fetch-categories';
import { fetchRestaurantPromo } from '@Sapi/restaurant/fetch-promo';
import { fetchRestaurantProducts } from '@Sapi/restaurant/fetch-products';
// Lib
import { SsrRequestManager } from '@Slib/request/ssr-request-manager';
import { GetStoreIdRestaurant } from '@Slib/router/restaurant-middleware/get-store-id';
// Models
import { initialState } from '@models';
// Utils
import { Consts } from '@utils';

const { RESTAURANT } = Consts.ROUTES.RESTAURANTS;

const endpoint = urlJoin(
  RESTAURANT.MAIN.PATH,
  RESTAURANT.MAIN.PARAMS,
  RESTAURANT.SECTIONS.MENU.PATH,
);

export class RestaurantMenuRouter {
  constructor(data) {
    this.router = data.router;
    this.GenerateApp = data.GenerateApp;
    this.getResponse = data.getResponse;
    this.GetStoreIdRestaurant = new GetStoreIdRestaurant();
    this.requests = [
      fetchCities,
      fetchContacts,
      getRestaurantId,
      fetchRestaurantCard,
      fetchRestaurantCategories,
      fetchRestaurantProducts,
      fetchRestaurantPromo,
    ];
  }

  init = () => {
    this.router.get(
      endpoint,
      this.GetStoreIdRestaurant.init,
      async (ctx, next) => {
        const data = await new SsrRequestManager()
          .getData(initialState, this.requests, ctx);
        ctx.data = data;

        await next();
      },
      async (ctx) => {
        const html = this.GenerateApp.init(ctx, ctx.data);
        this.getResponse(ctx, html);
      },
    );
  };
}
