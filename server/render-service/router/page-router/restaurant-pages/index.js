// Modules
import Router from '@koa/router';
import urlJoin from 'url-join';
// Lib
import { RestaurantRouterMiddleware } from '@Slib/router/restaurant-middleware/main';
// Utils
import { Consts } from '@utils';
// Routes
import { RestaurantMainRouter } from './main';
import { RestaurantMenuRouter } from './menu';
import { RestaurantReviewsRouter } from './reviews';
import { RestaurantInfoRouter } from './info';

const { RESTAURANT } = Consts.ROUTES.RESTAURANTS;

const endpoint = urlJoin(
  RESTAURANT.MAIN.PATH,
  RESTAURANT.MAIN.PARAMS,
);

export class RestaurantPagesRouter {
  constructor(data) {
    this.router = new Router();
    this.GenerateApp = data.GenerateApp;
    this.getResponse = data.getResponse;
    this.RestaurantRouterMiddleware = new RestaurantRouterMiddleware();
  }

  init = () => {
    this.router.use(
      endpoint,
      this.RestaurantRouterMiddleware.init,
    );
    this.setRoutes();

    return this.router.routes();
  };

  setRoutes = () => {
    new RestaurantMainRouter({
      router: this.router,
    }).init();

    new RestaurantMenuRouter({
      router: this.router,
      GenerateApp: this.GenerateApp,
      getResponse: this.getResponse,
    }).init();

    new RestaurantReviewsRouter({
      router: this.router,
      GenerateApp: this.GenerateApp,
      getResponse: this.getResponse,
    }).init();

    new RestaurantInfoRouter({
      router: this.router,
      GenerateApp: this.GenerateApp,
      getResponse: this.getResponse,
    }).init();
  };
}
