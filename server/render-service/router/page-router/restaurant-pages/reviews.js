// Modules
import urlJoin from 'url-join';
// Api
import { fetchCities } from '@Sapi/cities/fetch';
import { fetchContacts } from '@Sapi/contacts/fetch';
import { getRestaurantId } from '@Sapi/restaurant/get-id-restaurant';
import { fetchRestaurantCard } from '@Sapi/restaurant/fetch-restaurant-card';
// Lib
import { SsrRequestManager } from '@Slib/request/ssr-request-manager';
// Models
import { initialState } from '@models';
// Utils
import { Consts } from '@utils';

const { RESTAURANT } = Consts.ROUTES.RESTAURANTS;

const endpoint = urlJoin(
  RESTAURANT.MAIN.PATH,
  RESTAURANT.MAIN.PARAMS,
  RESTAURANT.SECTIONS.REVIEWS.PATH,
);

export class RestaurantReviewsRouter {
  constructor(data) {
    this.router = data.router;
    this.GenerateApp = data.GenerateApp;
    this.getResponse = data.getResponse;
    this.requests = [
      fetchCities,
      fetchContacts,
      getRestaurantId,
      fetchRestaurantCard,
    ];
  }

  init = () => {
    this.router.get(
      endpoint,
      async (ctx, next) => {
        const data = await new SsrRequestManager()
          .getData(initialState, this.requests, ctx);
        ctx.data = data;

        await next();
      },
      async (ctx) => {
        const html = this.GenerateApp.init(ctx, ctx.data);
        this.getResponse(ctx, html);
      },
    );
  };
}
