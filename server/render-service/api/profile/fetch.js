// Modules
import urlJoin from 'url-join';
// import { stringify } from 'qs';
// Lib
import { CookieManager } from '@Slib/cookie';
// Utils
import { Consts } from '@utils';

const { ENV } = Consts;
const { DATA } = Consts.COOKIE;

const endpointInfo = '/users/customers/current';

export const fetchProfileInfo = (ctx) => {
  const cookie = new CookieManager({ ctx });
  const accessToken = cookie.getAccessToken(DATA.ACCESS_TOKEN) || undefined;

  if (!accessToken) {
    return [];
  }

  const urlInfo = urlJoin(ENV.API, endpointInfo);

  return {
    key: 'profile.info',
    params: {
      url: urlInfo,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    },
  };
};

export default [
  fetchProfileInfo,
];
