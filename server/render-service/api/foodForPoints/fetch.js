// Modules
import urlJoin from 'url-join';
// Lib
import { CookieManager } from '@Slib/cookie';
// // Utils
import { Consts } from '@utils';

const { ENV } = Consts;
const { DATA } = Consts.COOKIE;

const endpoint = '/food-for-points';

export const fetchFoodForPoints = (ctx) => {
  const cookie = new CookieManager({ ctx });
  const cityId = ctx?.city?.id || cookie.getCookie(DATA.CITY_DATA) || undefined;
  const geocode = cookie.getCookie(DATA.GEO_CODE) || undefined;
  const geocodeParams = geocode?.lat ? `?latitude:${geocode.lat}?longitude:${geocode.lng}` : '';
  const url = urlJoin(ENV.API_BFF, endpoint, `?cityId=${cityId}${geocodeParams}`);
  return {
    key: 'foodForPoints',
    params: { url },
  };
};
