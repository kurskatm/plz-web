// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Lib
import { CookieManager } from '@Slib/cookie';
// Utils
import { Consts } from '@utils';
// Types

const { ENV } = Consts;
const { DATA } = Consts.COOKIE;

const endpoint = (cityId, strParams) => `/catalog/main/${cityId}?${strParams}`;

export const getRestaurantsAndHits = (ctx) => {
  const cookie = new CookieManager({ ctx });
  const accessToken = cookie.getAccessToken(DATA.ACCESS_TOKEN) || undefined;
  const geocode = cookie.getCookie(DATA.GEO_CODE) || undefined;
  const specId = ctx?.specId;
  const data = geocode?.lat ? {
    lat: geocode.lat,
    lng: geocode.lng,
  } : {};

  data.Filters = [specId];

  const cityId = ctx?.city?.id || cookie.getCookie(DATA.CITY_DATA) || undefined;
  const strParams = stringify(data);
  const url = urlJoin(ENV.API, endpoint(cityId, strParams));

  return {
    key: 'restaurantsAndHits',
    params: {
      url,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    },
  };
};
