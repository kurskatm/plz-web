// Modules
import urlJoin from 'url-join';
// Lib
import { CookieManager } from '@Slib/cookie';
// Utils
import { Consts } from '@Sutils';

const { ENV } = Consts;
const { DATA } = Consts.COOKIE;

export const fetchFilters = (ctx) => {
  const cookie = new CookieManager({ ctx });
  const cityId = ctx?.city?.id || cookie.getCookie(DATA.CITY_DATA);

  const url = urlJoin(ENV.API, `/catalog/filters?cityId=${cityId}`);

  return {
    key: 'filters.list',
    params: { url },
  };
};
