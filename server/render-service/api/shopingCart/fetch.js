// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Lib
import { CookieManager } from '@Slib/cookie';
// Utils
import { Consts } from '@utils';

const { ENV } = Consts;
const { DATA } = Consts.COOKIE;

const endpointInfo = '/cart/cart';
const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const fetchShopingCart = (ctx) => {
  const cookie = new CookieManager({ ctx });
  const userId = cookie.getCookie(DATA.PROFILE.USER_ID) || undefined;
  const cartId = cookie.getCookie(DATA.CART_ID) || undefined;
  const data = {
    userId,
    cartId,
  };
  const params = stringify(data, options);
  const url = urlJoin(ENV.API, endpointInfo, params);

  return {
    key: 'shopingCart',
    params: {
      url,
    },
  };
};

export default [
  fetchShopingCart,
];
