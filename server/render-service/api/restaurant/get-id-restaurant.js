// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Utils
import { Consts } from '@Sutils';

const { ENV } = Consts;

const getEndpoint = (cityId) => urlJoin(
  '/catalog/restaurants/by-url-name',
  cityId,
);
const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const getRestaurantId = (ctx) => {
  const cityId = ctx?.city?.id || undefined;
  const endpoint = getEndpoint(cityId);
  const data = {
    urlName: ctx?.params?.restaurantName || undefined,
  };
  const params = stringify(data, options);
  const url = urlJoin(ENV.API, endpoint, params);

  return {
    key: 'restaurant.main.id',
    params: { url },
  };
};
