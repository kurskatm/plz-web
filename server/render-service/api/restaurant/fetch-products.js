// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
import { CookieManager } from '@Slib/cookie';
// Utils
import { Consts } from '@Sutils';

const { ENV } = Consts;
const { DATA } = Consts.COOKIE;

const getEndpoint = (restaurantId) => urlJoin(
  '/catalog/restaurant',
  restaurantId,
  '/categories/products',
);
const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

const modifyCallback = (data = []) => data.map((item) => ({
  ...item,
  products: item.products?.map((product) => ({
    ...product,
    availableInBonusPoints: item.name === 'За баллы',
  })),
}));

export const fetchRestaurantProducts = (ctx) => {
  const restaurantId = ctx?.restaurantId || undefined;
  const cookie = new CookieManager({ ctx });
  const accessToken = cookie.getAccessToken(DATA.ACCESS_TOKEN) || undefined;
  const endpoint = getEndpoint(restaurantId);
  const data = {
    storeId: ctx?.storeId || undefined,
    showHidden: false,
  };
  const params = stringify(data, options);
  const url = urlJoin(ENV.API, endpoint, params);

  return {
    key: 'restaurant.menu.products',
    params: {
      url,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    },
    modifyCallback,
  };
};
