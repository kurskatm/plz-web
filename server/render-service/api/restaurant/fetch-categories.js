// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Utils
import { Consts } from '@Sutils';

const { ENV } = Consts;

const getEndpoint = (restaurantId) => urlJoin(
  '/catalog/restaurant',
  restaurantId,
  '/categories',
);
const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const fetchRestaurantCategories = (ctx) => {
  const restaurantId = ctx?.restaurantId || undefined;
  const endpoint = getEndpoint(restaurantId);
  const data = {
    storeId: ctx?.storeId || undefined,
    showHidden: false,
  };
  const params = stringify(data, options);
  const url = urlJoin(ENV.API, endpoint, params);

  return {
    key: 'restaurant.menu.categories',
    params: { url },
  };
};
