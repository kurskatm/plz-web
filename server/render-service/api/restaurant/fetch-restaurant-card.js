// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Lib
import { CookieManager } from '@Slib/cookie';
// Utils
import { Consts } from '@Sutils';

const { DATA } = Consts.COOKIE;
const { ENV } = Consts;

const getEndpoint = (restaurantId, strParams) => urlJoin(
  '/catalog/restaurants',
  restaurantId,
  `/card?${strParams}`,
);

export const fetchRestaurantCard = (ctx) => {
  const cookie = new CookieManager({ ctx });
  const accessToken = cookie.getAccessToken(DATA.ACCESS_TOKEN) || undefined;
  const restaurantId = ctx?.restaurantId || undefined;
  const geocode = cookie.getCookie(DATA?.GEO_CODE) || undefined;
  const data = geocode?.lat ? {
    lat: geocode.lat,
    lng: geocode.lng,
  } : {};
  const strParams = stringify(data);
  const endpoint = getEndpoint(restaurantId, strParams);

  const url = urlJoin(ENV.API, endpoint);
  if (!accessToken) {
    return {
      key: 'restaurant.main.info',
      params: { url },
    };
  }

  return {
    key: 'restaurant.main.info',
    params: {
      url,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    },
  };
};
