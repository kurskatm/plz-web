// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Utils
import { Consts } from '@Sutils';

const { ENV } = Consts;

const endpoint = '/partners/legal-details';
const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const fetchRestaurantLegal = (ctx) => {
  const data = {
    restaurantId: ctx?.restaurantId || undefined,
    storeId: ctx?.storeId || undefined,
  };
  const params = stringify(data, options);
  const url = urlJoin(ENV.API, endpoint, params);

  return {
    key: 'restaurant.info.legal',
    params: { url },
  };
};
