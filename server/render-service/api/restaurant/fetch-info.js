// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Utils
import { Consts } from '@Sutils';

const { ENV } = Consts;

const getEndpoint = (restaurantId) => urlJoin(
  '/catalog/restaurants',
  restaurantId,
  '/info',
);
const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const fetchRestaurantInfo = (ctx) => {
  const restaurantId = ctx?.restaurantId || undefined;
  const endpoint = getEndpoint(restaurantId);
  const data = {};
  const params = stringify(data, options);
  const url = urlJoin(ENV.API, endpoint, params);

  return {
    key: 'restaurant.info.info',
    params: { url },
  };
};
