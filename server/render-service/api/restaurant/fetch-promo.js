// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Utils
import { Consts } from '@Sutils';

const { ENV } = Consts;

const endpoint = '/promo/restaurant-promos';
const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const fetchRestaurantPromo = (ctx) => {
  const data = {
    restaurantId: ctx?.restaurantId || undefined,
    storeId: ctx?.storeId || undefined,
  };
  const params = stringify(data, options);
  const url = urlJoin(ENV.API, endpoint, params);

  return {
    key: 'restaurant.menu.promo',
    params: { url },
  };
};
