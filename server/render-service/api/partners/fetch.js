// Modules
import urlJoin from 'url-join';
// Utils
import { Consts } from '@Sutils';

const { ENV } = Consts;

const endpoint = 'partners/become/counters';
const url = urlJoin(ENV.API, endpoint);

export const fetchPartners = () => ({
  key: 'partners.counters',
  params: { url },
});
