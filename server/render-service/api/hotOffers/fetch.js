// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Lib
import { CookieManager } from '@Slib/cookie';
// Utils
import { Consts } from '@Sutils';

const { ENV } = Consts;
const { DATA } = Consts.COOKIE;

const endpoint = '/promo/hot-offers';
const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const fetchHotOffers = (ctx) => {
  const cookie = new CookieManager({ ctx });
  const data = {
    cityId: ctx?.city?.id || cookie.getCookie(DATA.CITY_DATA) || undefined,
  };
  const params = stringify(data, options);
  const url = urlJoin(ENV.API, endpoint, params);

  return {
    key: 'hotOffers',
    params: { url },
  };
};
