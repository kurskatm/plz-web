// Modules
import urlJoin from 'url-join';
// Utils
import { Consts } from '@Sutils';

const { ENV } = Consts;

const endpoint = '/catalog/cuisines';

export const fetchCuisines = () => {
  const url = urlJoin(ENV.API, endpoint);

  return {
    key: 'cuisines.list',
    params: { url },
  };
};
