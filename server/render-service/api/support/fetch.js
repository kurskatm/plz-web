// Modules
import urlJoin from 'url-join';
// Utils
import { Consts } from '@Sutils';

const { ENV } = Consts;

const endpoint = '/support/contact/topics';
const url = urlJoin(ENV.API, endpoint);

export const fetchSupportTopics = () => ({
  key: 'support.topics',
  params: { url },
});
