// Modules
import urlJoin from 'url-join';
// Utils
import { Consts } from '@Sutils';

const { ENV } = Consts;

const endpoint = 'location/cities/geo';
const url = urlJoin(ENV.API, endpoint);

export const fetchCities = () => ({
  key: 'cities.list',
  params: { url },
});
