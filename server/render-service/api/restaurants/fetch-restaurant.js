// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Utils
import { Consts } from '@Sutils';

const { ENV } = Consts;

const getEndpoint = (id) => `/catalog/restaurants/${id}/card`;
const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const fetchRestaurant = (ctx) => {
  const restaurantId = ctx?.restaurantId;
  const endpoint = getEndpoint(restaurantId);

  const data = {
    lat: ctx?.lat || undefined,
    lng: ctx?.lng || undefined,
  };
  const params = stringify(data, options);
  const url = urlJoin(ENV.API, endpoint, params);

  return {
    key: 'restaurant.main.data',
    params: { url },
  };
};
