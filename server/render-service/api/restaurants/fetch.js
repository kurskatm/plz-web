// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Utils
import { Consts } from '@Sutils';

const { ENV } = Consts;

const endpoint = '/catalog/restaurants';
const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const fetchRestaurants = (ctx) => {
  const data = {
    cityId: ctx?.city?.id || undefined,
    lat: ctx?.lat || undefined,
    lng: ctx?.lng || undefined,
  };
  const params = stringify(data, options);
  const url = urlJoin(ENV.API, endpoint, params);

  return {
    key: 'restaurants',
    params: { url },
  };
};
