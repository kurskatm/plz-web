// Modules
import urlJoin from 'url-join';
// Utils
import { Consts } from '@Sutils';

const { ENV } = Consts;

export const fetchHits = (ctx) => {
  const url = urlJoin(ENV.API, `/catalog/city/${ctx?.city?.id}/hits`);

  return {
    key: 'hits',
    params: { url },
  };
};
