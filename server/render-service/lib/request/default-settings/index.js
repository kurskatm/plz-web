export const requestDefaultSettings = async (ctx, next) => {
  ctx.request.socket.setTimeout(10 * 1000);
  await next();
};
