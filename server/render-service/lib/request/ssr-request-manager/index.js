// Modules
import { isFunction, isNil, isNumber } from 'lodash';
import set from 'set-value';
// Lib
import { RequestManager } from '../request-manager';

export class SsrRequestManager {
  getDataSuccess = (response, modifyCallback) => {
    if (!isNil(modifyCallback) && isFunction(modifyCallback)) {
      const value = {
        data: modifyCallback(response?.data),
        ui: { status: 'success', details: response?.status },
      };

      return value;
    }
    const value = {
      data: response?.data,
      ui: { status: 'success', details: response?.status },
    };

    return value;
  };

  getDataError = (response) => {
    const value = {
      data: null,
      ui: {
        status: 'error',
        details: response?.status,
      },
    };

    return value;
  };

  callbackRequest = (ctx) => (requestDataCallback) => {
    const { params, key, modifyCallback } = requestDataCallback(ctx);
    const request = new RequestManager(params);

    return new Promise((resolve, reject) => {
      request
        .getResponse()
        .then((response) => {
          resolve({ response, key, modifyCallback });
        })
        .catch((error) => {
          console.log(error);

          error.key = key;
          reject(error);
        });
    });
  };

  getData = (initialData, requests = [], ctx) => {
    const promisesList = requests.map(this.callbackRequest(ctx));
    const promise = Promise.allSettled(promisesList);

    return promise
      .then((val) => {
        const result = JSON.parse(JSON.stringify(initialData));
        val.forEach((data = {}) => {
          const { status, value } = data;
          const isFinishedPromise = status === 'fulfilled';
          const requestStatus = value?.response?.status;
          const isStatus = isNumber(requestStatus) && [200, 204].includes(requestStatus);
          let objValue;

          if (isFinishedPromise && isStatus) {
            objValue = this.getDataSuccess(value?.response, value?.modifyCallback);
          } else {
            objValue = this.getDataError(value?.response);
          }

          set(result, value?.key, objValue);
        });

        return ({ data: result, error: false });
      })
      .catch((error) => {
        console.log(error);
        return { data: {}, error };
      });
  };
}
