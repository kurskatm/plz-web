// Modules
import { isNil, isObject } from 'lodash';
// Lib
import { WithOptions } from './with-options';
import { WithContentType } from './with-content-type';

export class EnhanceManager {
  constructor(requestParams) {
    this.requestParams = requestParams;

    this.WithOptions = new WithOptions();
    this.WithContentType = new WithContentType();
  }

  getEnhances = () => {
    if (!isObject(this.requestParams) || isNil(this.requestParams)) {
      return this.requestParams;
    }

    this.requestParams = this.WithOptions.init(this.requestParams);
    this.requestParams = this.WithContentType.init(this.requestParams);

    return this.requestParams;
  };
}
