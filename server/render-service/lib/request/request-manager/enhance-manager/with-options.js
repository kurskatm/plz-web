// Lib
import { createXHR } from './config';

export class WithOptions {
  init = (requestParams) => {
    requestParams = {
      createXHR,
      async: true,
      method: 'GET',
      timeout: 5000,
      ...requestParams,
    };

    return requestParams;
  };
}
