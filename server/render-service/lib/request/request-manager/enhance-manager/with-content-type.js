// Modules
import isNil from 'lodash/isNil';
// Utils
import { Consts } from '@utils';
// Lib
import { DEFAULTS_CONTENT_TYPE } from './config';

const { ACCEPT, CONTENT_TYPE } = Consts.REQUEST.HEADERS;

export class WithContentType {
  init = (requestParams) => {
    requestParams.headers = requestParams?.headers || {};

    if (isNil(requestParams.headers?.[CONTENT_TYPE])) {
      requestParams.headers = {
        ...requestParams.headers,
        ...DEFAULTS_CONTENT_TYPE,
      };
    } else if (isNil(requestParams.headers?.[ACCEPT])) {
      requestParams.headers = {
        ...requestParams.headers,
        [ACCEPT]: requestParams.headers[CONTENT_TYPE],
      };
    }

    return requestParams;
  };
}
