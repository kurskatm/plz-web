// Modules
import { XMLHttpRequest } from 'xmlhttprequest';
// Utils
import { Consts } from '@Sutils';

const { ACCEPT, CONTENT_TYPE } = Consts.REQUEST.HEADERS;
const { APPLICATION_JSON } = Consts.REQUEST.RESPONSE_TYPE;

export const createXHR = () => new XMLHttpRequest();

export const DEFAULTS_CONTENT_TYPE = {
  [ACCEPT]: APPLICATION_JSON,
  [CONTENT_TYPE]: APPLICATION_JSON,
};
