// Lib
import { EnhanceManager } from './enhance-manager';
import { api } from './config';

export class RequestManager extends EnhanceManager {
  constructor(request) {
    super(request);
    this.getEnhances();
  }

  getResponse = () => new Promise((resolve, reject) => {
    api(this.requestParams)
      .then((response) => {
        response = this.setStatus(response);

        if (response.status > 199 && response.status < 500) {
          resolve(response);
        } else {
          reject(response);
        }
      })
      .catch((error) => {
        error = this.setStatus(error);

        console.log(error);
        reject(error);
      });
  });

  setStatus = (response) => {
    if (!response?.status) {
      response.status = 'unknown';
    }

    return response;
  };
}
