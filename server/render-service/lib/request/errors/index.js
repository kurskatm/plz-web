// Utils
import { Consts } from '@utils';

const { ROUTES } = Consts;

export class RequestErrorsManager {
  constructor() {
    this.ctx = null;
    this.next = null;
  }

  resolveError = () => {
    const status = this.ctx.statusCode || this.ctx.status;

    switch (status) {
      case 404: {
        this.ctx.redirect(ROUTES.ERRORS.ERROR_404.PATH);
        break;
      }

      default: {
        break;
      }
    }
  };

  emitError = (error) => {
    console.error(error);
    error.status = error.statusCode || error.status || 500;

    switch (error.status) {
      case 500:
      case 501:
      case 502:
      default: {
        this.ctx.redirect(ROUTES.ERRORS.ERROR_500.PATH);
      }
    }
  };

  setCallback = async (ctx, next) => {
    this.ctx = ctx;
    this.next = next;

    try {
      this.resolveError();
      await this.next();
    } catch (error) {
      this.emitError(error);
      ctx.app.emit('error', error, ctx);
    }
  };
}
