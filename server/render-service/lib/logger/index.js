// Modules
import logger from 'koa-logger';
// Utils
import { Consts } from '@Sutils';

const { IS_PRODUCTION } = Consts.ENV;

export class KoaLogger {
  constructor(app) {
    this.app = app;
  }

  init = () => {
    if (IS_PRODUCTION) {
      this.app.use(logger());
    }
  };
}
