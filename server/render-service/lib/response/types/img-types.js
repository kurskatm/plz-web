// Modules
import path from 'path';

export class GetImgType {
  init = (url = '') => {
    const extension = path.extname(url);

    switch (extension) {
      case '.jpg': {
        return 'image/jpeg';
      }

      case '.png': {
        return 'image/png';
      }

      case '.gif': {
        return 'image/gif';
      }

      default: {
        return '';
      }
    }
  };
}
