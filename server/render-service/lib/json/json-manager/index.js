// Modules
import { isNil, isString, isEmpty } from 'lodash';

export class JsonManager {
  jsonParse = (data) => {
    if (isString(data) && !isEmpty(data)) {
      try {
        const parseData = JSON.parse(data);
        return parseData;
      } catch (error) {
        console.log(error);
        return null;
      }
    }

    return null;
  };

  jsonStringify = (data) => {
    if (!isNil(data)) {
      try {
        const strData = JSON.stringify(data);
        return strData;
      } catch (error) {
        console.log(error);
        return null;
      }
    }

    return null;
  };
}
