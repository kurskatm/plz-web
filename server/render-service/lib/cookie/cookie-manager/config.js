// Utils
import { Consts } from '@Sutils';

const { COOKIE } = Consts;

export const COOKIE_DEFAULT_OPTIONS = {
  get expires() {
    const today = Date.now();
    const tomorrow = new Date(today);
    tomorrow.setDate(tomorrow.getDate() + COOKIE.EXPIRES);

    return tomorrow;
  },
  path: '/',
  secure: true,
  httpOnly: false,
  sameSite: 'lax',
};
