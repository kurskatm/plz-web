// Modules
import { isString, isEmpty } from 'lodash';
// Lib
import { JsonManager } from '../../json';
import { COOKIE_DEFAULT_OPTIONS } from './config';

export class CookieManager extends JsonManager {
  constructor(data) {
    super();
    this.ctx = data.ctx;
  }

  getAccessToken = (name) => {
    const accessToken = this.getCookie(name);
    // для совместимости со старым сайтом
    // if (!accessToken) {
    //   accessToken = this.getCookie('__at');
    //   if (accessToken) {
    //     this.setCookie(name);
    //     this.ctx.cookies.removeCookie('__at');
    //   }
    // }
    return accessToken;
  }

  getCookie = (name) => {
    if (isString(name) && !isEmpty(name)) {
      const cookieData = this.ctx.cookies.get(name);

      if (isString(cookieData) && !isEmpty(cookieData)) {
        const decodeCookieData = decodeURIComponent(
          Buffer.from(cookieData, 'base64')
            .toString('ascii'),
        );

        return this.jsonParse(decodeCookieData);
      }

      return cookieData;
    }

    return null;
  };

  setCookie = (name, data, userOptions = {}) => {
    if (isString(name) && !isEmpty(name)) {
      const cookieData = this.jsonStringify(data);

      if (cookieData) {
        const encodeCookieData = encodeURIComponent(cookieData);
        const buffer = Buffer.from(encodeCookieData, 'binary')
          .toString('base64');
        const options = {
          ...COOKIE_DEFAULT_OPTIONS,
          ...userOptions,
        };

        return this.ctx.cookies.set(name, buffer, options);
      }
    }

    return null;
  };

  removeCookie = (name) => {
    if (isString(name) && !isEmpty(name)) {
      return this.ctx.cookies.set(name, '');
    }

    return null;
  }
}
