// Lib
import { GetRestaurantId } from './get-restaurant-id';
import { CheckRestaurantId } from './check-restaurant-id';

export class RestaurantRouterMiddleware {
  constructor() {
    this.GetRestaurantId = new GetRestaurantId();
    this.CheckRestaurantId = new CheckRestaurantId();
  }

  init = async (ctx, next) => {
    const restaurant = await this.GetRestaurantId.init(ctx);
    ctx.restaurantId = this.CheckRestaurantId.init(ctx, restaurant);

    await next();
  };
}
