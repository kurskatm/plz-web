// Modules
import isObject from 'lodash/isObject';
import isNil from 'lodash/isNil';
// Utils
import { Consts } from '@utils';

const { ERROR_404, ERROR_500 } = Consts.ROUTES.ERRORS;

export class CheckRestaurantId {
  init = (ctx, restaurant) => {
    const isObjectRestaurant = isObject(restaurant);
    const isNilRestaurantId = isNil(restaurant?.id);

    if (!isObjectRestaurant) {
      ctx.redirect(ERROR_500.PATH);
      return null;
    }

    switch (restaurant.status) {
      case 200: {
        if (!isNilRestaurantId) {
          return restaurant.id;
        }

        ctx.redirect(ERROR_500.PATH);
        ctx.status = 301;
        return null;
      }

      case 404: {
        ctx.redirect(ERROR_404.PATH);
        ctx.status = 301;
        return null;
      }

      default: {
        ctx.redirect(ERROR_500.PATH);
        ctx.status = 301;
        return null;
      }
    }
  };
}
