// Api
import { getRestaurantId } from '@Sapi/restaurant/get-id-restaurant';
// Lib
import { RequestManager } from '../../../request/request-manager';

export class GetRestaurantId {
  init = async (ctx) => {
    const { params } = getRestaurantId(ctx);
    const restaurantId = await new RequestManager(params)
      .getResponse()
      .then((response) => {
        if (response && response.status === 200 && response?.data) {
          return {
            id: response.data.id,
            status: response.status,
          };
        }

        return {
          id: null,
          status: response.status,
        };
      });

    return restaurantId;
  };
}
