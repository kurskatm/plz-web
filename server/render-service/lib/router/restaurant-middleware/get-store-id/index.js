// Modules
import isNil from 'lodash/isNil';
// Lib
import { GetRestaurantStoreId } from './get-restaurant-store';

export class GetStoreIdRestaurant {
  constructor() {
    this.GetRestaurantStoreId = new GetRestaurantStoreId();
  }

  init = async (ctx, next) => {
    const storeId = await this.GetRestaurantStoreId.init(ctx);

    if (!isNil(storeId.storeId)) {
      ctx.storeId = storeId.storeId;
    }

    await next();
  };
}
