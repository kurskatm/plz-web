// Api
import { fetchRestaurantCard } from '@Sapi/restaurant/fetch-restaurant-card';
// Lib
import { RequestManager } from '../../../request/request-manager';

export class GetRestaurantStoreId {
  init = async (ctx) => {
    const { params } = fetchRestaurantCard(ctx);
    const restaurantStoreId = await new RequestManager(params)
      .getResponse()
      .then((response) => {
        if (response && response.status === 200 && response?.data) {
          return {
            storeId: response.data.storeId,
            status: response.status,
          };
        }

        return {
          storeId: null,
          status: response.status,
        };
      });

    return restaurantStoreId;
  };
}
