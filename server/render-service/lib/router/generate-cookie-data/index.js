// Models
import { profileModel } from '@models/profile';
import { newAddressModel } from '@models/profile/new-address';
import { newGeoCodeModel } from '@models/profile/new-geo-code';
import { shopingCartModel } from '@models/shoping-cart';
// Utils
import { Consts } from '@utils';
// Lib
import { CookieManager } from '../../cookie';

const { DATA } = Consts.COOKIE;

export class GenerateCookieData {
  constructor(ctx) {
    this.ctx = ctx;
    this.CookieManager = new CookieManager({ ctx });
  }

  getState = () => ({
    profile: this.getProfile(),
    shopingCart: this.getShopingCart(),
  });

  getProfile = () => {
    const data = {
      ...profileModel,
      city: this.ctx?.city?.id || null,
      pickup: this.CookieManager.getCookie(DATA.PICKUP) || false,
      newAddress: {
        ...newAddressModel,
        address: {
          ...newAddressModel.address,
          name: this.CookieManager.getCookie(DATA.ADDRESS) || '',
        },
      },
      newGeoCode: {
        ...newGeoCodeModel,
        data: this.CookieManager.getCookie(DATA.GEO_CODE) || null,
      },
    };

    return data;
  };

  getShopingCart = () => {
    const data = {
      ...shopingCartModel,
      id: this.CookieManager.getCookie(DATA.CART_ID) || false,
    };

    return data;
  };
}
