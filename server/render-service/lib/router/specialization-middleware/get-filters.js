// Api
import { fetchFilters } from '@Sapi/filters/fetch';
// Lib
import { RequestManager } from '../../request/request-manager';

export class GetFilters {
  constructor(ctx) {
    this.ctx = ctx;
  }

  init = async () => {
    const { params } = fetchFilters(this.ctx);

    const filters = await new RequestManager(params)
      .getResponse()
      .then((response) => {
        if (response && response.status === 200 && response?.data) {
          return response;
        }

        return null;
      });

    return filters;
  };
}
