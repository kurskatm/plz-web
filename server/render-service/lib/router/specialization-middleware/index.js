import { GetCuisines } from './get-cuisines';
import { GetFilters } from './get-filters';

export class SpecializationRouterMiddleware {
  constructor() {
    this.GetCuisines = new GetCuisines();
  }

  init = async (ctx, next) => {
    const specializationName = ctx?.params?.specId;
    const getFilters = new GetFilters(ctx);

    if (specializationName) {
      const cuisines = await this.GetCuisines.init();
      const filters = await getFilters.init();
      const allFilters = [...(cuisines?.data ? cuisines.data : []),
        ...(filters?.data ? filters.data : [])];
      const specialization = allFilters.find((item) => item?.urlName === specializationName);
      ctx.specId = specialization?.id;
    }

    await next();
  };
}
