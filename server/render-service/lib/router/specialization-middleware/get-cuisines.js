// Api
import { fetchCuisines } from '@Sapi/cuisines/fetch';
// Lib
import { RequestManager } from '../../request/request-manager';

const { params } = fetchCuisines();

export class GetCuisines {
  init = async () => {
    const cuisines = await new RequestManager(params)
      .getResponse()
      .then((response) => {
        if (response && response.status === 200 && response?.data) {
          return response;
        }

        return null;
      });

    return cuisines;
  };
}
