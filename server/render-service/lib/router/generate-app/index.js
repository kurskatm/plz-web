// Modules
import React from 'react';
import { renderToString as render } from 'react-dom/server';
// Path
import { htmlPath } from '@Spath/html-path';
import { App, nodeExtractor } from '@Spath/node-extractor';
// Lib
import { JsonManager } from '../../json';
import { GenerateCookieData } from '../generate-cookie-data';

export class GenerateApp {
  constructor() {
    this.html = this.getHtmlTemplate();
    this.JsonManager = new JsonManager();
    this.GenerateCookieData = null;
  }

  init = (ctx, data = {}) => {
    this.GenerateCookieData = new GenerateCookieData(ctx);
    const validData = !data?.error ? data.data : {};
    const cookieData = this.GenerateCookieData.getState();
    const reduxState = {
      ...validData,
      ...cookieData,
    };
    const app = this.generateApp(ctx, reduxState);
    const reduxDataStr = this.generateData(reduxState);
    const pageHtml = this.html.replace(
      '<%- body %>',
      `${reduxDataStr}<div id="root">${app}</div>`,
    );

    return pageHtml;
  };

  generateData = (data) => {
    const dataStr = this.JsonManager.jsonStringify(data);
    // убираем расхождения в стэйте между сервером и клиентом
    const dataStrFormatted = dataStr.replace('cityHits', 'products');
    const reduxData = `<script>window.REDUX_DATA=${dataStrFormatted}</script>`;

    return reduxData;
  };

  getHtmlTemplate = () => {
    try {
      const html = global.fileSystem
        .readFileSync(htmlPath, 'utf-8');

      return html;
    } catch (error) {
      console.log('Error read html file');
      console.log(error);

      return null;
    }
  };

  generateApp = (ctx, data) => {
    try {
      const html = render(nodeExtractor.collectChunks(
        <App path={ctx.request.url} reduxData={data} />,
      ));
      return html;
    } catch (error) {
      console.log('Generate app failed');
      console.log(error);
      return null;
    }
  };
}
