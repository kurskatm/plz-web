// Modules
import isString from 'lodash/isString';

export class CheckCityParam {
  init = (ctx) => {
    const cityUrl = ctx?.params?.cityUrl;
    const data = {
      cityUrl,
      valid: isString(cityUrl) && !!cityUrl.length,
    };

    return data;
  };
}
