// Utils
import { Consts } from '@utils';
// Lib
import { CookieManager } from '../../cookie/cookie-manager';

const { CITY_DATA } = Consts.COOKIE.DATA;

export class CheckCookieCity {
  init = async (ctx, userCity) => {
    const cookie = new CookieManager({ ctx });
    const cookieCity = cookie.getCookie(CITY_DATA);

    if (userCity.id && cookieCity !== userCity.id) {
      cookie.setCookie(CITY_DATA, userCity.id);
    }
  };
}
