// Modules
import isString from 'lodash/isString';
// Utils
import { Consts } from '@utils';
// Lib
import { CookieManager } from '../../cookie/cookie-manager';
import { GetCities } from './get-cities';

const { CITY_DATA } = Consts.COOKIE.DATA;
const { ERROR_500 } = Consts.ROUTES.ERRORS;

export class CityRouterMiddleware {
  constructor() {
    this.GetCities = new GetCities();
  }

  init = async (ctx, next) => {
    const cookie = new CookieManager({ ctx });
    const cookieCity = cookie.getCookie(CITY_DATA);

    if (isString(cookieCity) && cookieCity.length) {
      const cities = await this.GetCities.init();

      if (!cities) {
        ctx.redirect(ERROR_500.PATH);
        return;
      }
      const { data } = cities;
      const city = data.find(
        ({ id }) => id === cookieCity,
      );

      if (city) {
        ctx.redirect(`/${city.urlName}`);
        return;
      }
    }

    await next();
  };
}
