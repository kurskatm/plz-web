// Api
import { fetchCities } from '@Sapi/cities/fetch';
// Lib
import { RequestManager } from '../../request/request-manager';

const { params } = fetchCities();

export class GetCities {
  init = async () => {
    const cities = await new RequestManager(params)
      .getResponse()
      .then((response) => {
        if (response && response.status === 200 && response?.data) {
          return response;
        }

        return null;
      });

    return cities;
  };
}
