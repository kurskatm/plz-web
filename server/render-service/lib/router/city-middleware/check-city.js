// Utils
import { Consts } from '@utils';
// Lib
import { GetCities } from './get-cities';
import { CheckCityParam } from './check-city-param';
import { CheckCookieCity } from './check-cookie-city';

const { ERROR_404, ERROR_500 } = Consts.ROUTES.ERRORS;

export class CheckCity {
  constructor() {
    this.CheckCityParam = new CheckCityParam();
    this.CheckCookieCity = new CheckCookieCity();
    this.GetCities = new GetCities();
  }

  init = async (ctx, next) => {
    const city = await this.middleware(ctx);

    if (city) {
      ctx.city = city;
    }

    await next();
  };

  middleware = async (ctx) => {
    const { cityUrl, valid } = this.CheckCityParam.init(ctx);

    if (valid) {
      const cities = await this.GetCities.init();

      if (!cities) {
        ctx.redirect(ERROR_500.PATH);
        return undefined;
      }

      const { data } = cities;
      const index = data.findIndex(
        ({ urlName }) => urlName === cityUrl,
      );

      if (index === -1) {
        ctx.redirect(ERROR_404.PATH);
        return undefined;
      }

      if (index > -1) {
        const city = data[index];

        if (city) {
          await this.CheckCookieCity.init(ctx, city);
        }

        return city;
      }
    }

    return false;
  };
}
