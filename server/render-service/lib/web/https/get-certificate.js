// Modules
import fs from 'fs';
import path from 'path';
// Utils
import { MAIN_CONSTANTS } from '@@S/../webpack/constants';

const { APP_DIR } = MAIN_CONSTANTS;

const PATH_KEY = path.resolve(APP_DIR, 'cert/server.key');
const PATH_SERT = path.resolve(APP_DIR, 'cert/server.cert');

export const getSertificate = () => ({
  key: fs.readFileSync(PATH_KEY, 'utf8'),
  cert: fs.readFileSync(PATH_SERT, 'utf8'),
});
