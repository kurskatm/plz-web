export const getMiddleware = (compiler, devMiddleware) => (context, next) => {
  const ready = new Promise((resolve, reject) => {
    [].concat(compiler.compilers || compiler).forEach((comp) => {
      comp.hooks.failed.tap('KoaWebpack', (error) => {
        reject(error);
      });
    });

    devMiddleware.waitUntilValid(() => {
      resolve(true);
    });
  });

  const init = new Promise((resolve) => {
    devMiddleware(
      context.req,
      {
        end: (content) => {
          context.body = content;
          resolve();
        },
        getHeader: context.get.bind(context),
        setHeader: context.set.bind(context),
        locals: context.state,
      },
      () => resolve(next()),
    );
  });

  return Promise.all([ready, init]);
};
