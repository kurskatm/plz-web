// Modules
import webpackHotClient from 'webpack-hot-client';
// Utils
import { clientCompiler } from '@@S/../webpack/webpack-client/compilers';

const opts = {
  logLevel: 'silent',
};

export const hotMiddleware = () => new Promise((resolve) => {
  const client = webpackHotClient(clientCompiler, opts);
  const { server } = client;

  server.on('listening', () => resolve(client));
});
