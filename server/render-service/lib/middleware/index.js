// Modules
import webpackDevMiddleware from 'webpack-dev-middleware';
import { assign } from 'lodash';
// Utils
import { Consts } from '@Sutils';
import { clientCompiler } from '@@S/../webpack/webpack-client/compilers';
import { STATS } from '@@S/../webpack/utils/stats';
// Lib
import { getMiddleware } from './dev-middleware';
import { hotMiddleware } from './hot-middleware';

const { IS_DEVELOPMENT } = Consts.ENV;

const opts = {
  outputFileSystem: global.fileSystem,
  publicPath: '/assets/',
  stats: STATS,
};

export class KoaMiddleware {
  constructor(app) {
    this.app = app;
  }

  init = async () => {
    if (IS_DEVELOPMENT) {
      const middleware = await this.createKoaMiddleware();
      this.app.use(middleware);
    }
  };

  createKoaMiddleware = async () => {
    const hotClient = await hotMiddleware(clientCompiler);
    const expressMiddleware = webpackDevMiddleware(clientCompiler, opts);
    expressMiddleware.fileSystem = global.fileSystem;

    const middleware = getMiddleware(clientCompiler, expressMiddleware);
    const close = (callback) => {
      const next = hotClient ? () => hotClient.close(callback) : callback;
      expressMiddleware.close(next);
    };

    return assign(middleware, {
      hotClient,
      devMiddleware: expressMiddleware,
      close,
    });
  };
}
