// Configs
import { serverRun } from './server';

const serverStart = async () => {
  serverRun();
};

serverStart();
