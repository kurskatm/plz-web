// Modules
import gracefulShutdown from 'http-graceful-shutdown';
// Messages
import { serverMessages } from '@Smessages';

export const getGracefulShutdown = (server) => gracefulShutdown(server, {
  signals: 'SIGINT SIGTERM',
  timeout: 30000,
  development: false,
  finally() {
    serverMessages.close();
  },
});
