// Utils
import { objectEnv } from './object-env';

const Consts = {
  CITIES: {
    DEFAULT: {
      ID: '00000002-ae86-4653-9db8-f6bdacd094e5',
    },
  },

  COOKIE: {
    DATA: {
      ADDRESS: 'address',
      CITY_DATA: 'city',
      GEO_CODE: 'geodata',
      PICKUP: 'pickup',
      ACCESS_TOKEN: 'jwt_at',
      USER_ID: 'userId',
    },
    EXPIRES: 365,
  },

  ENV: objectEnv,

  HEADERS: {
    HTML: {
      'Content-Type': 'text/html; charset=utf-8',
    },
  },

  REQUEST: {
    HEADERS: {
      ACCEPT: 'Accept',
      AUTHORIZATION: 'Authorization',
      CONTENT_TYPE: 'Content-Type',
    },

    METHOD: {
      GET: 'GET',
      POST: 'POST',
      PUT: 'PUT',
      DELETE: 'DELETE',
    },

    RESPONSE_TYPE: {
      APPLICATION_JSON: 'application/json',
      JSON: 'json',
    },
  },

  ROUTES: {
    CITIES: { PATH: '/' },
    CHECKOUT: {
      MAIN: { PATH: '/object' },
      SUCCESS: { PATH: '/checkout/success' },
    },
    INFO_PAGES: {
      ABOUT: { PATH: '/about' },
      CONTACTS: { PATH: '/contacts' },
      PARTNERS: { PATH: '/become-partners' },
      FOOD: { PATH: '/food-for-points' },
      FEEDBACK: { PATH: '/support' },
      REGLAMENT: { PATH: '/reglament' },
      HELP: { PATH: '/help' },
      SUPPORT: { PATH: '/support' },
    },
    PROFILE_PAGES: {
      INFO: { PATH: '/profile/info' },
      ORDERS: { PATH: '/profile/orders' },
      SCORES: { PATH: '/profile/scores' },
      REVIEWS: { PATH: '/profile/reviews' },
      FAVORITE: { PATH: '/profile/favorite' },
    },
    ERRORS: {
      ERROR_404: { PATH: '/page-not-found' },
      ERROR_404_TECHNICAL_WOKRS: { PATH: '/technical-wokrs' },
      ERROR_500: { PATH: '/server-error' },
      ERROR_502: { PATH: '/bad-gateway' },
    },
    HOME: { PATH: '/' },
    SPECIALIZATION: { PATH: '/:cityId/restaurants/:specId' },
  },
};

export { Consts };
