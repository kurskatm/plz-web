// Modules
import { keys } from 'lodash';

const reducer = (accumulator, key) => {
  accumulator[key] = process.env[key];

  return accumulator;
};

export const objectEnv = keys(process.env).reduce(reducer, {});
