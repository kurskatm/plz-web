// Constants
const { MAIN_CONSTANTS } = require('../webpack/constants');
// Utils
const { getServerAliases } = require('../webpack/utils/aliases');

module.exports = {
  parser: 'babel-eslint',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
    allowImportExportEverywhere: true,
  },
  env: {
    browser: true,
    es2020: true,
    node: true,
    jasmine: true,
  },
  extends: ['eslint:recommended', 'plugin:react/recommended', 'airbnb'],
  plugins: ['react', 'import', 'optimize-regex', 'prettier', 'promise'],
  rules: {
    'linebreak-style': 0,
    'eslint linebreak-style': [0, 'error', MAIN_CONSTANTS.OS.IS_WINDOWS ? 'windows' : 'unix'],
    'import/prefer-default-export': 'off',
    'import/no-extraneous-dependencies': 'off',
    'react/jsx-filename-extension': 'off',
    'no-console': 'off',
    'no-param-reassign': 'off',
    'func-names': 'off',
  },
  settings: {
    'import/extensions': ['.js', '.ts', '.tsx', 'json'],
    'import/resolver': {
      alias: {
        map: Object.entries(getServerAliases()),
        extensions: ['.js', '.ts', '.tsx', 'json'],
      },
    },
  },
};
