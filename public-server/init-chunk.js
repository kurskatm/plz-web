'use-strict';

const initChunk = (file) => {
  if (file) {
    return eval(file);
  }

  return null;
};

module.exports = { initChunk };
