'use-strict';
// Modules
const path = require('path');
// Constants
const { MAIN_CONSTANTS } = require('../constants');

const FILE_NAME = 'postcss.config.js';
const configPath = path.resolve(MAIN_CONSTANTS.APP_DIR, FILE_NAME);

const getPostCssLoader = () => {
  const loader = {
    loader: 'postcss-loader',
    options: {
      postcssOptions: {
        config: configPath,
      },
    },
  };
  return loader;
};

module.exports = { getPostCssLoader };
