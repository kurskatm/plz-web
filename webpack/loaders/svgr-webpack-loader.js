'use-strict';

const getSvgrWebpackLoader = () => {
  const loader = {
    loader: '@svgr/webpack',
  };

  return loader;
};

module.exports = { getSvgrWebpackLoader };
