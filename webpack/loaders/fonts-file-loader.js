'use-strict';
// Constants
const { MAIN_CONSTANTS } = require('../constants');
// Utils
const { getTargetInfo } = require('../utils/target-info');
const { assetsUrl } = require('../utils/destruct-config-env');

const name = MAIN_CONSTANTS.IS_DEVELOPMENT
  ? '[name].[ext]' : '[name]-[hash:8].[ext]';

const getFontsFileLoader = (target) => {
  const { targetIsWeb } = getTargetInfo(target);

  const outputPath = 'fonts/';

  const loader = {
    loader: 'file-loader',
    options: {
      name,
      outputPath,
      publicPath: (url) => {
        return `${assetsUrl}assets/fonts/${url}`;
      },
      emitFile: targetIsWeb,
    }
  };

  return loader;
};

module.exports = { getFontsFileLoader };
