'use-strict';
// Modules
const path = require('path');
// Utils
const { CLIENT_CONSTANTS, SERVER_CONSTANTS } = require('../constants');
const { getTargetInfo } = require('../utils/target-info');

const FILE_NAME = '.babelrc';

const getBabelLoader = (target) => {
  const { targetIsWeb } = getTargetInfo(target);
  const pathToConfig = targetIsWeb
    ? CLIENT_CONSTANTS.PATH_TO_CLIENT_CONFIG
    : SERVER_CONSTANTS.SERVICES.RENDER.PATH_TO_CONFIG;

  const loader = {
    loader: 'babel-loader',
    options: {
      configFile: path.join(pathToConfig, FILE_NAME),
    },
  };

  return loader;
};

module.exports = { getBabelLoader };
