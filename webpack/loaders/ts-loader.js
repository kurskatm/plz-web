'use-strict';
const { loadableTransformer } = require('loadable-ts-transformer');

const getTsLoader = () => {
  const loader = {
    loader: 'ts-loader',
    options: {
      getCustomTransformers: () => ({ before: [loadableTransformer] }),
      transpileOnly: true,
    },
  };

  return loader;
};

module.exports = { getTsLoader };
