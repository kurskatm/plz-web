'use-strict';
// Constants
const { MAIN_CONSTANTS } = require('../constants');

const getSassLoader = () => {
  const loader = {
    loader: 'sass-loader',
    options: {
      sourceMap: MAIN_CONSTANTS.IS_DEVELOPMENT,
    },
  };

  return loader;
};

module.exports = { getSassLoader };
