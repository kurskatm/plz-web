'use-strict';
// Constants
const { MAIN_CONSTANTS } = require('../constants');

const getCssLoader = () => {
  const loader = {
    loader: 'css-loader',
    options: {
      importLoaders: 1,
      sourceMap: MAIN_CONSTANTS.IS_DEVELOPMENT,
    },
  };

  return loader;
};

module.exports = { getCssLoader };
