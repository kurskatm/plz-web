'use-strict';
// Modules
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
// Constants
const { MAIN_CONSTANTS } = require('../constants');
const { getTargetInfo } = require('../utils/target-info');

const styleLoader = {
  loader: 'style-loader',
};

const isomorphicLoader = {
  loader: 'isomorphic-style-loader'
};

const miniCssLoader = {
  loader: MiniCssExtractPlugin.loader,
  options: {
    esModule: true,
  },
};

const loader = MAIN_CONSTANTS.IS_DEVELOPMENT
  ? styleLoader
  : miniCssLoader;

const getStyleLoader = (target) => {
  const { targetIsWeb } = getTargetInfo(target);
  return targetIsWeb ? loader : isomorphicLoader;
};

module.exports = { getStyleLoader };
