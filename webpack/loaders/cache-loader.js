'use-strict';
// Modules
const path = require('path');
// Constants
const { MAIN_CONSTANTS } = require('../constants');

const getCacheLoader = (target) => {
  const cacheDirectory = path.join(
    MAIN_CONSTANTS.PATH_TO_CACHE_FOLDER,
    target,
    'cache-loader',
  );

  const loader = {
    loader: 'cache-loader',
    options: {
      cacheDirectory,
    }
  };
  return loader;
};

module.exports = { getCacheLoader };
