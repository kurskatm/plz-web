'use-strict';
// Constants
const { MAIN_CONSTANTS } = require('../constants');
// Utils
const { getTargetInfo } = require('../utils/target-info');
const { assetsUrl } = require('../utils/destruct-config-env');

const name = MAIN_CONSTANTS.IS_DEVELOPMENT
  ? '[name].[ext]' : '[name]-[hash:8].[ext]';

const getImgUrlLoader = (target) => {
  const { targetIsWeb } = getTargetInfo(target);
  const outputPath = 'img';

  const loader = {
    loader: 'file-loader',
    options: {
      name,
      outputPath,
      publicPath: (url) => {
        return `${assetsUrl}assets/img/${url}`;
      },
      emitFile: targetIsWeb,
      limit: 10 * 1024,
      noquotes: true,
    },
  };

  return loader;
};

module.exports = { getImgUrlLoader };
