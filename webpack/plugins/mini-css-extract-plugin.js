'use-strict';
// Modules
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
// Constants
const { MAIN_CONSTANTS } = require('../constants');

const name = MAIN_CONSTANTS.IS_DEVELOPMENT
  ? 'assets/css/styles.css'
  : 'css/styles-[contenthash:8].css';

const getMiniCssExtractPlugin = () => {
  const plugin = new MiniCssExtractPlugin({
    filename: name,
    chunkFilename: name,
  });

  return plugin;
};

module.exports = { getMiniCssExtractPlugin };
