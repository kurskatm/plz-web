'use-strict';
// Modules
const LoadablePlugin = require('@loadable/webpack-plugin');

const getLoadableWebpackPlugin = () => {

  const plugin = new LoadablePlugin({
    filename: 'stats-loadable.json',
    writeToDisk: false,
  });

  return plugin;
};

module.exports = { getLoadableWebpackPlugin };
