'use-strict';
// Modules
const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
// Constants
const { MAIN_CONSTANTS } = require('../constants');
// Utils
const { getTargetInfo } = require('../utils/target-info');

const getCleanWebpackPlugin = (target) => {
  const { targetIsNode } = getTargetInfo(target);

  const plugin = new CleanWebpackPlugin({
    cleanOnceBeforeBuildPatterns: [
      MAIN_CONSTANTS.IS_BUNDLE_ANALYZE
        && path.join(MAIN_CONSTANTS.APP_DIR, 'webpack/webpack-stats/**/*'),
      path.join(
        MAIN_CONSTANTS.APP_DIR,
        targetIsNode
          ? `render-service-build/**/*`
          : 'build/**/*',
      ),
    ].filter(Boolean),
  });

  return plugin;
};

module.exports = { getCleanWebpackPlugin };
