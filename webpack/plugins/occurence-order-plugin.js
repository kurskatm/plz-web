'use-strict';
// Modules
const webpack = require('webpack');

const getOccurenceOrderPlugin = () => {
  const plugin = new webpack.optimize.OccurrenceOrderPlugin();
  return plugin;
};

module.exports = { getOccurenceOrderPlugin };
