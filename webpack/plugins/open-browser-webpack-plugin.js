'use-strict';
// Modules
const OpenBrowserPlugin = require('open-browser-webpack-plugin');
// Utils
const { SERVER_CONSTANTS } = require('../constants/server.constants');

const {
  PROTOCOL,
  HOST,
  PORT,
} = SERVER_CONSTANTS;

const getOpenBrowserPlugin = () => {
  const plugin = new OpenBrowserPlugin({
    url: `${PROTOCOL}://${HOST}:${PORT}`,
  });

  return plugin;
};

module.exports = { getOpenBrowserPlugin };
