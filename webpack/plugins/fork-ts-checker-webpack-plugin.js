'use-strict';
// Modules
const path = require('path');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
// Constants
const { MAIN_CONSTANTS, CLIENT_CONSTANTS } = require('../constants');

const getForkTsCheckerWebpackPlugin = () => {
  const plugin = new ForkTsCheckerWebpackPlugin({
    eslint: {
      files: `${CLIENT_CONSTANTS.PATH_TO_PROJECT}/**/*.{ts,tsx,js,jsx}`,
    },
    typescript: {
      configFile: path.join(MAIN_CONSTANTS.APP_DIR, './src/tsconfig.json'),
      diagnosticOptions: {
        semantic: true,
        syntactic: true
      },
    },
  });

  return plugin;
};

module.exports = { getForkTsCheckerWebpackPlugin };
