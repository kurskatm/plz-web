'use-strict';
// Modules
const FixStyleOnlyEntriesPlugin = require('webpack-fix-style-only-entries');

const getFixStyleOnlyEntriesPlugin = () => {
  const plugin = new FixStyleOnlyEntriesPlugin();
  return plugin;
};

module.exports = { getFixStyleOnlyEntriesPlugin };
