'use-strict';
// Modules
const path = require('path');
const webpack = require('webpack');
// Constants
const { MAIN_CONSTANTS } = require('../constants');

const getWatchIgnorePlugin = () => {
  const plugin = new webpack.WatchIgnorePlugin([
    path.resolve(MAIN_CONSTANTS.APP_DIR, 'node_modules'),
  ]);

  return plugin;
};

module.exports = { getWatchIgnorePlugin };
