'use-strict';
// Modules
const webpack = require('webpack');

const getNamedModulesPlugin = () => {
  const plugin = new webpack.NamedModulesPlugin();
  return plugin;
};

module.exports = { getNamedModulesPlugin };
