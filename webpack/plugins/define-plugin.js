'use-strict';
// Modules
const webpack = require('webpack');
// Constants
const { MAIN_CONSTANTS, SERVER_CONSTANTS } = require('../constants');
// Utils
const { getTargetInfo } = require('../utils/target-info');
const { allConfigs } = require('../utils/get-config-env');

const getDefinePlugin = (target) => {
  const { targetIsWeb } = getTargetInfo(target);

  const plugin = new webpack.DefinePlugin({
    "process.env": targetIsWeb
      ? allConfigs.client
      : {
        ...allConfigs[SERVER_CONSTANTS.SERVICES.RENDER.NAME],
        IS_BUNDLE_ANALYZE: MAIN_CONSTANTS.IS_BUNDLE_ANALYZE,
      }
  });

  return plugin;
};

module.exports = { getDefinePlugin };
