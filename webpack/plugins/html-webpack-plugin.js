'use-strict';
// Modules
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
// Constants
const { CLIENT_CONSTANTS, MAIN_CONSTANTS } = require('../constants');
// Utils
const { assetsUrl } = require('../utils/destruct-config-env');

const mainOptions = {
  excludeChunks: [],
  alwaysWriteToDisk: false,
  inject: true,
  optimize: {
    prefetch: true,
  },
  title: 'Pozhlikbez',
  chunksSortMode: 'none',
  template: path.join(CLIENT_CONSTANTS.PATH_TO_ASSETS, 'html/template.html'),
  filename: 'index.html',
  body: '<%- body %>',
  async: ['bundle', 'vendor'],
  // defer: ['thirdparty'],
};

const devOptions = {
  minify: false,
  assetsUrl: '',
};

const prodOptions = {
  removeComments: true,
  collapseWhitespace: true,
  removeRedundantAttributes: true,
  useShortDoctype: true,
  removeEmptyAttributes: true,
  removeStyleLinkTypeAttributes: true,
  keepClosingSlash: true,
  minifyJS: true,
  minifyCSS: true,
  minifyURLs: true,
  assetsUrl,
};

const options = Object.assign(
  mainOptions,
  MAIN_CONSTANTS.IS_DEVELOPMENT
    ? devOptions : prodOptions,
);

const getHtmlWebpackPlugin = () => {
  const plugin = new HtmlWebpackPlugin(options);
  return plugin;
};

module.exports = { getHtmlWebpackPlugin };
