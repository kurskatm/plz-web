'use-strict';
// Utils
const { getTargetInfo } = require('../utils/target-info');
// Loaders
const { getCacheLoader } = require('../loaders/cache-loader');
const { getTsLoader } = require('../loaders/ts-loader');

const getTsxPreset = (target) => {
  let use = [
    getCacheLoader(target),
    getTsLoader(target),
  ];

  const preset = {
    test: /\.tsx?$/i,
    exclude: /node_modules/,
    use,
  };

  return preset;
};

module.exports = { getTsxPreset };
