'use-strict';
// Loaders
const { getImgUrlLoader } = require('../loaders/image-file-loader');
const { getImageWebpackLoader } = require('../loaders/image-webpack-loader');

const getImagePreset = (target) => {
  const use = [
    getImgUrlLoader(target),
    getImageWebpackLoader(),
  ];

  const preset = {
    test: /\.(png|jpe?g|gif)$/i,
    include: /[/\\](img)[/\\]/i,
    use,
  };

  return preset;
};

module.exports = { getImagePreset };
