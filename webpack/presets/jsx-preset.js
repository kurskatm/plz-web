'use-strict';
// Utils
const { getTargetInfo } = require('../utils/target-info');
// Loaders
const { getCacheLoader } = require('../loaders/cache-loader');
const { getBabelLoader } = require('../loaders/babel-loader');

const getJsxPreset = (target) => {
  let use = [
    getCacheLoader(target),
    getBabelLoader(target),
  ];

  const preset = {
    test: /\.jsx?$/i,
    exclude: /node_modules/,
    use,
  };

  return preset;
};

module.exports = { getJsxPreset };
