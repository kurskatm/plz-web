'use-strict';
// Loaders
const { getSvgrWebpackLoader } = require('../loaders/svgr-webpack-loader');
const { getSvgUrlLoader } = require('../loaders/svg-url-loader');

const getSvgPreset = (target) => {
  const use = [
    getSvgrWebpackLoader(),
    getSvgUrlLoader(target),
  ];

  const preset = {
    test: /\.svg$/i,
    include: /[/\\]svg[/\\]/i,
    use,
  };

  return preset;
};

module.exports = { getSvgPreset };
