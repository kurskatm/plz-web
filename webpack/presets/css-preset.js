'use-strict';
// Loaders
const { getStyleLoader } = require('../loaders/style-loader');
const { getCssLoader } = require('../loaders/css-loader');
const { getPostCssLoader } = require('../loaders/postcss-loader');
const { getCacheLoader } = require('../loaders/cache-loader');

const getCssPreset = (target) => {
  const use = [
    getStyleLoader(target),
    getCacheLoader(target),
    getCssLoader(),
    getPostCssLoader(),
  ];

  const preset = {
    test: /\.css$/i,
    use,
  };

  return preset;
};

module.exports = { getCssPreset };
