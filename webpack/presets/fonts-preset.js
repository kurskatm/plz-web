'use-strict';
// Loaders
const { getFontsFileLoader } = require('../loaders/fonts-file-loader');

const getFontsPreset = (target) => {
  const use = [
    getFontsFileLoader(target),
  ];

  const preset = {
    test: /\.(woff(2)?|ttf|eot|svg)$/i,
    include: /[/\\](fonts)[/\\]/,
    use,
  };

  return preset;
};

module.exports = { getFontsPreset };
