'use-strict';

// Modules
const path = require('path');
// Utils
const { MAIN_CONSTANTS } = require('./main.constants');

const PREFIX_PATH_CLIENT = '@';

const ASSETS_FOLDER = 'assets/';
const SOURCE_FOLDER = 'src/';
const PUBLIC_FOLDER = 'public/';
const PATH_TO_CLIENT_CONFIG = path.resolve(
  MAIN_CONSTANTS.PATH_TO_WEBPACK_FOLDER,
  'webpack-client',
);

const CLIENT_CONSTANTS = {
  PREFIX_PATH_CLIENT,

  ASSETS_FOLDER,
  PUBLIC_FOLDER,
  SOURCE_FOLDER,

  PATH_TO_CLIENT_CONFIG,
  PATH_TO_ASSETS: path.resolve(MAIN_CONSTANTS.APP_DIR, ASSETS_FOLDER),
  PATH_TO_PROJECT: path.resolve(MAIN_CONSTANTS.APP_DIR, SOURCE_FOLDER),
  PATH_TO_PUBLIC_FOLDER: path.resolve(MAIN_CONSTANTS.APP_DIR, PUBLIC_FOLDER),
};

module.exports = {
  CLIENT_CONSTANTS,
};
