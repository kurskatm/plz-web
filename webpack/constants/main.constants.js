'use-strict';

// Modules
const path = require('path');

const DEVELOPMENT = 'development';
const PRODUCTION = 'production';
const { NODE_ENV } = process.env;
const ENV_LIST = [DEVELOPMENT, PRODUCTION];
const VALID_NODE_ENV = NODE_ENV && ENV_LIST.includes(NODE_ENV)
  ? NODE_ENV : PRODUCTION;

const APP_DIR = process.cwd();

const MAIN_CONSTANTS = {
  DEVELOPMENT,
  PRODUCTION,
  NODE_ENV,
  ENV_LIST,
  VALID_NODE_ENV,
  IS_DEVELOPMENT: VALID_NODE_ENV === DEVELOPMENT,
  IS_PRODUCTION: VALID_NODE_ENV === PRODUCTION,
  IS_BUNDLE_ANALYZE: process.env.IS_BUNDLE_ANALYZE || false,

  APP_DIR,
  PATH_TO_WEBPACK_FOLDER: path.resolve(APP_DIR, 'webpack'),
  PATH_TO_CACHE_FOLDER: path.resolve(APP_DIR, '.cache'),

  TARGET: {
    WEB: 'web',
    NODE: 'node',
  },

  OS: {
    IS_WINDOWS: process.platform === 'win32',
  },
};

module.exports = {
  MAIN_CONSTANTS,
};
