const { MAIN_CONSTANTS } = require('./main.constants');
const { CLIENT_CONSTANTS } = require('./client.constants');
const { SERVER_CONSTANTS } = require('./server.constants');

const CONSTANTS = {
  MAIN_CONSTANTS,
  CLIENT_CONSTANTS,
  SERVER_CONSTANTS,
};

module.exports = CONSTANTS;
