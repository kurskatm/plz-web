'use-strict';
// Modules
const webpack = require('webpack');
// Config
const { MAIN_CONSTANTS } = require('../../constants');
const serverDevConfig = require('./render.dev.config');
const serverProdConfig = require('./render.prod.config');

const { IS_DEVELOPMENT } = MAIN_CONSTANTS;

const getServerConfig = () => {
  const serverConfig = IS_DEVELOPMENT
    ? serverDevConfig
    : serverProdConfig;
  const serverCompiler = webpack(serverConfig);

  return serverCompiler;
}

module.exports = {
  getServerConfig,
};
