'use-strict';
// Modules
const path = require('path');
const { merge } = require('webpack-merge');
// Constants
const { CLIENT_CONSTANTS, SERVER_CONSTANTS } = require('../../constants');
// Webpack configs import
const { mainConfig } = require('./main.config');

// Config
const renderServerProdConfig = {
  entry: {
    app: [
      path.resolve(CLIENT_CONSTANTS.PATH_TO_PROJECT, 'app/index.tsx'),
    ],

    renderService: [
      '@babel/polyfill',
      path.resolve(
        SERVER_CONSTANTS.SERVICES.RENDER.PATH_TO_PROJECT,
        'index.js',
      ),
    ],
  },

  output: {
    filename: 'js/[name].js',
    chunkFilename: 'js/chunks/[name].chunk.js',
    path: SERVER_CONSTANTS.PATH_TO_SERVER_PUBLIC_FOLDER,
    publicPath: '/',
    libraryTarget: 'commonjs2',
  },
};

const config = merge(mainConfig, renderServerProdConfig);
module.exports = config;
