'use-strict';
// Modules
const path = require('path');
const nodeExternals = require('webpack-node-externals');
// Constants
const { MAIN_CONSTANTS, CLIENT_CONSTANTS, SERVER_CONSTANTS } = require('../../constants');
// Utils
const { getServerAliases } = require('../../utils/aliases');
// Presets
const { getTsxPreset } = require('../../presets/tsx-preset');
const { getJsxPreset } = require('../../presets/jsx-preset');
const { getFontsPreset } = require('../../presets/fonts-preset');
const { getImagePreset } = require('../../presets/image-preset');
const { getSvgPreset } = require('../../presets/svg-preset');
// Plugins
const { getMainPluginsLoader } = require('../../plugins-loader/main');
// Optimization
const { getOptimizationMainPreset } = require('../../optimization-preset/main');

// Config
const mainConfig = {
  mode: MAIN_CONSTANTS.VALID_NODE_ENV,
  target: MAIN_CONSTANTS.TARGET.NODE,
  stats: 'errors-only',

  externals: ['@loadable/component', nodeExternals()],
  node: {
    fs: 'empty',
    child_process: 'empty',
  },
  resolve: {
    alias: getServerAliases(),
    extensions: ['.js', '.ts', '.tsx', '.json', '.mjs'],
    modules: [
      CLIENT_CONSTANTS.PATH_TO_PROJECT,
      SERVER_CONSTANTS.SERVICES.RENDER.PATH_TO_PROJECT,
      'node_modules',
    ],
  },

  performance: {
    hints: false,
  },

  optimization: getOptimizationMainPreset(MAIN_CONSTANTS.TARGET.NODE),

  module: {
    rules: [
      getTsxPreset(MAIN_CONSTANTS.TARGET.NODE),
      getJsxPreset(MAIN_CONSTANTS.TARGET.NODE),

      getFontsPreset(MAIN_CONSTANTS.TARGET.NODE),
      getImagePreset(MAIN_CONSTANTS.TARGET.NODE),
      getSvgPreset(MAIN_CONSTANTS.TARGET.NODE),
    ],
  },

  plugins: getMainPluginsLoader(MAIN_CONSTANTS.TARGET.NODE),
};

module.exports = {
  mainConfig,
};
