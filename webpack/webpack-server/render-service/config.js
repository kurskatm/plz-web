'use-strict';
// Modules
const path = require('path');
const MemoryFileSystem = require('memory-fs');
// Constants
const {
  SERVER_CONSTANTS,
} = require('../../constants');
// Utils
const { createChunksSsr } = require('../../utils/create-chunks-ssr');
const { getNonWebpackRequire } = require('../../utils/get-non-webpack-require');
// Compilers
const { getServerConfig } = require('./compilers');
const { clientCompiler } = require('../../webpack-client/compilers');

const serverConfig = getServerConfig();
__non_webpack_require__ = getNonWebpackRequire;

class BuildConfig {
  constructor() {
    this.fileSystem = new MemoryFileSystem();
    this.clientCompiler = clientCompiler;
    this.serverConfig = serverConfig;
  }

  init = async () => {
    this.preInstall();

    await this.clientPromise();
    await this.serverPromise();

    try {
      createChunksSsr();
      const renderServiceBundle = path.join(
        SERVER_CONSTANTS.PATH_TO_SERVER_PUBLIC_FOLDER,
        'js/renderService.js',
      );
  
      const data = global.fileSystem
        .readFileSync(renderServiceBundle, 'utf-8');
  
      // eslint-disable-next-line no-eval
      eval(data.toString());
    } catch (error) {
      console.log('Error run server script');
      console.log(error);
    }
  };

  preInstall = () => {
    global.fileSystem = this.fileSystem;
    this.clientCompiler.outputFileSystem = this.fileSystem;
    this.serverConfig.outputFileSystem = this.fileSystem;
  };

  clientPromise = () => new Promise((resolve, reject) => {
    console.log('Start client compile...');

    this.clientCompiler.run((error, stats) => {
      if (error) {
        console.log('Error client compile.')
        console.log(error);
        reject(error);
      } else {
        console.log('Finish client compile.');
        resolve(stats);
      }
    });
  });

  serverPromise = () => new Promise((resolve, reject) => {
    console.log('Start server compile...');

    this.serverConfig.run((error, stats) => {
      if (error) {
        console.log('Error server compile.')
        console.log(error);
        reject(error);
      } else {
        console.log('Finish server compile.');
        resolve(stats);
      }
    });
  });
}

new BuildConfig().init();
