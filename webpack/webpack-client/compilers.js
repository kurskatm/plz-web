'use-strict';
// Modules
const webpack = require('webpack');
// Constants
const { MAIN_CONSTANTS } = require('../constants');
// Utils
const { statsToAssets } = require('../utils/stats-to-assets');
// Webpack configs import
const clientDevConfig = require('./development.config');
const clientProdConfig = require('./production.config');

const clientWebpackConfig = MAIN_CONSTANTS.IS_DEVELOPMENT
  ? clientDevConfig : clientProdConfig;
const clientCompiler = webpack(clientWebpackConfig);

module.exports = { clientCompiler, clientWebpackConfig };
