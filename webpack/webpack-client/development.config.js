'use-strict';
// Modules
const path = require('path');
const { merge } = require('webpack-merge');
// Constants
const { MAIN_CONSTANTS, CLIENT_CONSTANTS } = require('../constants');
// Webpack configs import
const { mainConfig } = require('./main.config');

// Config
const devConfig = {
  mode: MAIN_CONSTANTS.DEVELOPMENT,
  devtool: 'eval-source-map',

  entry: {
    bundle: [
      'eventsource-polyfill',
      'webpack-hot-middleware/client',
      path.join(CLIENT_CONSTANTS.PATH_TO_ASSETS, 'sass/global.sass'),
      path.join(CLIENT_CONSTANTS.PATH_TO_ASSETS, 'sass/main.sass'),
      path.join(CLIENT_CONSTANTS.PATH_TO_PROJECT, 'index.tsx'),
    ],
  },

  output: {
    filename: 'js/[name].js',
    chunkFilename: 'js/chunks/[name].chunk.js',
    path: CLIENT_CONSTANTS.PATH_TO_PUBLIC_FOLDER,
    publicPath: '/',
    globalObject: 'this',
  },
};

const config = merge(mainConfig, devConfig);

module.exports = config;
