'use-strict';
// Modules
const PnpWebpackPlugin = require(`pnp-webpack-plugin`);
const { merge } = require('webpack-merge');
// Constants
const { MAIN_CONSTANTS, CLIENT_CONSTANTS } = require('../constants');
// Utils
const { getClientAliases } = require('../utils/aliases');
// Presets
const { getTsxPreset } = require('../presets/tsx-preset');
const { getJsxPreset } = require('../presets/jsx-preset');
const { getCssPreset } = require('../presets/css-preset');
const { getSassPreset } = require('../presets/sass-preset');
const { getFontsPreset } = require('../presets/fonts-preset');
const { getImagePreset } = require('../presets/image-preset');
const { getSvgPreset } = require('../presets/svg-preset');
// Plugins
const { getMainPluginsLoader } = require('../plugins-loader/main');
// Optimization
const { getOptimizationMainPreset } = require('../optimization-preset/main');
// Webpack configs import
const { analyzeConfig } = require('./analyze.config');

// Config
const config = {
  target: MAIN_CONSTANTS.TARGET.WEB,
  stats: 'errors-only',
  bail: MAIN_CONSTANTS.IS_PRODUCTION,
  node: {
    fs: 'empty',
    child_process: 'empty',
  },
  resolve: {
    alias: getClientAliases(),
    extensions: ['.js', '.ts', '.tsx', '.json', '.mjs'],
    modules: [
      CLIENT_CONSTANTS.PATH_TO_PROJECT,
      'node_modules',
    ],
    plugins: [PnpWebpackPlugin],
  },

  resolveLoader: {
    plugins: [
      PnpWebpackPlugin.moduleLoader(module),
    ],
  },

  performance: {
    hints: false,
  },

  optimization: getOptimizationMainPreset(MAIN_CONSTANTS.TARGET.WEB),

  module: {
    rules: [
      getTsxPreset(MAIN_CONSTANTS.TARGET.WEB),
      getJsxPreset(MAIN_CONSTANTS.TARGET.WEB),

      getCssPreset(MAIN_CONSTANTS.TARGET.WEB),
      getSassPreset(MAIN_CONSTANTS.TARGET.WEB),

      getFontsPreset(MAIN_CONSTANTS.TARGET.WEB),
      getImagePreset(MAIN_CONSTANTS.TARGET.WEB),
      getSvgPreset(MAIN_CONSTANTS.TARGET.WEB),
    ],
  },

  plugins: getMainPluginsLoader(MAIN_CONSTANTS.TARGET.WEB),
};

let mainConfig = config;

if (MAIN_CONSTANTS.IS_BUNDLE_ANALYZE) {
  mainConfig = merge(mainConfig, analyzeConfig);
}

module.exports = {
  mainConfig,
};
