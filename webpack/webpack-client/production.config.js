'use-strict';
// Modules
const path = require('path');
const { merge } = require('webpack-merge');
// Constants
const {
  MAIN_CONSTANTS,
  CLIENT_CONSTANTS,
} = require('../constants');
// Webpack configs import
const { mainConfig } = require('./main.config');

const config = {
  mode: MAIN_CONSTANTS.PRODUCTION,

  entry: {
    bundle: [
      path.resolve(CLIENT_CONSTANTS.PATH_TO_PROJECT, 'index.tsx'),
    ],
    'vendor-styles': [
      path.resolve(CLIENT_CONSTANTS.PATH_TO_ASSETS, 'sass/global.sass'),
    ],
    styles: [
      path.resolve(CLIENT_CONSTANTS.PATH_TO_ASSETS, 'sass/main.sass'),
    ],
  },

  output: {
    filename: 'js/[name]-[hash:8].js',
    chunkFilename: 'js/[name]-[hash:8].js',
    path: CLIENT_CONSTANTS.PATH_TO_PUBLIC_FOLDER,
    publicPath: '/',
    globalObject: 'this',
  },
};

const clientProdConfig = merge(mainConfig, config);
module.exports = clientProdConfig;
