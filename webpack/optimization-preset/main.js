'use-strict';
// Constants
const { MAIN_CONSTANTS } = require('../constants');
// Utils
const { getTargetInfo } = require('../utils/target-info');
// Optimization
const { terser } = require('../optimization/terser');
const { cssAssets } = require('../optimization/css-assets');
const { getSplitChunks } = require('../optimization/split-chunks');

const getOptimizationMainPreset = (target) => {
  const { targetIsWeb } = getTargetInfo(target);

  const optimization = {
    minimize: MAIN_CONSTANTS.IS_PRODUCTION,
    minimizer: [
      terser(target),
      cssAssets(),
    ],
  };

  if (targetIsWeb) {
    optimization.splitChunks = getSplitChunks();
  }

  return optimization;
};

module.exports = { getOptimizationMainPreset };
