'use-strict';
// Modules
const path = require('path');
const fse = require('fs-extra');
// Constants
const { SERVER_CONSTANTS } = require('../constants');

const chunkRegexp = /^js\/chunks[\\/].*\.chunk\.js$/;
const pathToStatsFile = path.join(
  SERVER_CONSTANTS.PATH_TO_SERVER_PUBLIC_FOLDER,
  'stats-loadable.json',
);

const createChunksSsr = () => {
  const statsFileBuffer = global.fileSystem.readFileSync(pathToStatsFile);
  const statsFile = JSON.parse(statsFileBuffer.toString());
  const { assetsByChunkName, outputPath } = statsFile;

  Object.keys(assetsByChunkName).forEach(key => {
    const filePath = assetsByChunkName[key];
    const isChunk = chunkRegexp.test(filePath);

    if (isChunk) {
      const fullPath = path.resolve(outputPath, filePath);
      const data = global.fileSystem.readFileSync(fullPath);

      if (data) {
        fse.outputFileSync(fullPath, data)
      }
    }
  });
};

module.exports = { createChunksSsr };
