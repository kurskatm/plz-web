'use-strict';

const STATS = {
  colors: true,
  hash: false,
  timings: false,
  chunks: false,
  assets: false,
  chunkModules: false,
  modules: false,
  children: false,
  version: false,
  cached: false,
  cachedAssets: false,
  reasons: false,
  source: false,
  errorDetails: true,
};

module.exports = { STATS };
