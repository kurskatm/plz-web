'use-strict';
// Modules
const path = require('path');
// Utils
const { initChunk } = require('../../public-server/init-chunk');

const getNonWebpackRequire = (modulePath) => {
  const regExpExecutable = /renderService\.js$/;
  const isExecutableFile = regExpExecutable.test(modulePath);

  if (isExecutableFile) {
    return modulePath;
  }

  try {
    let file = global.fileSystem
      .readFileSync(modulePath)
      .toString();

    if (path.extname(modulePath) === '.json') {
      try {
        file = JSON.parse(file);
        return file;
      } catch (error) {
        console.log(`@loadable parse JSON file error: ${error}`);
      }

    } else {
      return initChunk(file);
    }

    return null;
  } catch (error) {
    console.log(`@loadable parse JSON file error: ${error}`);
    return modulePath;
  }
};

module.exports = { getNonWebpackRequire };
