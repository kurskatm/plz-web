'use-strict';
// Modules
const fs = require('fs');
const path = require('path');
const dotenv = require('dotenv');
// Utils
const { MAIN_CONSTANTS, SERVER_CONSTANTS } = require('../constants');

const DEFAULT_ENV = {
  NODE_ENV: MAIN_CONSTANTS.VALID_NODE_ENV,
  IS_DEVELOPMENT: MAIN_CONSTANTS.IS_DEVELOPMENT,
  IS_PRODUCTION: MAIN_CONSTANTS.IS_PRODUCTION,
};

const getConfigEnv = (type, objectEnv = {}) => {
  let file;

  if (type) {
    switch (type) {
      case `client`: {
        file = fs.readFileSync(
          path.join(
            MAIN_CONSTANTS.APP_DIR,
            `webpack/webpack-client/.env.${MAIN_CONSTANTS.VALID_NODE_ENV}`,
          ),
        );
        break;
      }

      case SERVER_CONSTANTS.SERVICES.RENDER.NAME: {
        file = fs.readFileSync(
          path.join(
            MAIN_CONSTANTS.APP_DIR,
            `webpack/webpack-server/${SERVER_CONSTANTS.SERVICES.RENDER.NAME}/.env.${MAIN_CONSTANTS.VALID_NODE_ENV}`,
          ),
        );
        break;
      }

      default: {
        file = null;
        break;
      }
    }

    if (file) {
      const conf = dotenv.parse(file);

      if (conf) {
        const resultConf = Object.keys(conf).reduce(
          (acc, key) => {
            const value = JSON.stringify(conf[key]);

            acc[key] = value;
            return Object.assign({}, acc);
          },
          Object.assign({}, DEFAULT_ENV, objectEnv),
        );

        return resultConf;
      }
    }
  }

  return {};
};

const allConfigs = {
  client: getConfigEnv('client'),
  [SERVER_CONSTANTS.SERVICES.RENDER.NAME]: getConfigEnv(SERVER_CONSTANTS.SERVICES.RENDER.NAME),
};

module.exports = { allConfigs, getConfigEnv };
