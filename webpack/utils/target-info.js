'use-strict';
// Utils
const { MAIN_CONSTANTS } = require('../constants');

const getTargetInfo = (target) => ({
  target,
  targetIsWeb: target === MAIN_CONSTANTS.TARGET.WEB,
  targetIsNode: target === MAIN_CONSTANTS.TARGET.NODE,
});

module.exports = { getTargetInfo };
