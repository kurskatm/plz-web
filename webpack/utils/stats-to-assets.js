'use-strict';

const statsToAssets = ({ entrypoints }) =>
  Object.keys(entrypoints).reduce((map, key) => {
      if (!map[key]) {
          map[key] = {};
      }

      entrypoints[key].assets.forEach((file) => {
          let ext = file.split('.').pop();

          if (!map[key][ext]) {
              map[key][ext] = file;
          }
      });

      return map;
  },
  {},
);

module.exports = { statsToAssets };
