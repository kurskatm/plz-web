'use-strict';
// Modules
const fs = require('fs');
const path = require('path');
// Utils
const { MAIN_CONSTANTS, CLIENT_CONSTANTS, SERVER_CONSTANTS } = require('../constants');

const DEFAULT_ALIASES = {};

const getDirectories = (pathSrc) =>
  fs
    .readdirSync(pathSrc, { withFileTypes: true })
    .filter(dirent => dirent.isDirectory())
    .map(dirent => dirent.name);

const reducer = (pathSrc, prefixPath) => (accumulator, currentValue) => {
  const key = `${prefixPath}${currentValue.toLowerCase()}`;
  const pathDirectory = path.join(pathSrc, currentValue);

  accumulator[key] = pathDirectory;
  return accumulator;
};

const getAliases = (
  pathSrc = '',
  prefixPath = CLIENT_CONSTANTS.PREFIX_PATH_CLIENT,
  startAliases = {},
) => Object.assign(
  {},
  startAliases,
  DEFAULT_ALIASES,
  getDirectories(pathSrc)
    .reduce(reducer(pathSrc, prefixPath), { [prefixPath]: pathSrc })
);

const getClientAliases = () => Object.assign(
  {},
  getAliases(
    CLIENT_CONSTANTS.PATH_TO_PROJECT,
    CLIENT_CONSTANTS.PREFIX_PATH_CLIENT,
  ),
);

const getServerAliases = () => Object.assign(
  {},
  getAliases(
    SERVER_CONSTANTS.SERVICES.RENDER.PATH_TO_PROJECT,
    SERVER_CONSTANTS.PREFIX_PATH_SERVER,
    SERVER_CONSTANTS.ALIASES,
  ),
  getAliases(
    CLIENT_CONSTANTS.PATH_TO_PROJECT,
    CLIENT_CONSTANTS.PREFIX_PATH_CLIENT,
  ),
);

module.exports = {
  getAliases,
  getClientAliases,
  getServerAliases,
};
