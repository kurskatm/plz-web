'use-strict';
// Constants
const { SERVER_CONSTANTS } = require('../constants');
// Utils
const { allConfigs } = require('./get-config-env');

const dataEnv = allConfigs[SERVER_CONSTANTS.SERVICES.RENDER.NAME] || '';
const assetsUrl = `${dataEnv.ASSETS_URL.replace(/"/g, '')}/`;

module.exports = { assetsUrl, dataEnv };
