'use-strict';
// Constants
const { MAIN_CONSTANTS } = require('../constants');
// Utils
const { getTargetInfo } = require('../utils/target-info');
// Plugins
const { getWatchIgnorePlugin } = require('../plugins/watch-ignore-plugin');
const { getNamedModulesPlugin } = require('../plugins/named-modules-plugin');
const { getOccurenceOrderPlugin } = require('../plugins/occurence-order-plugin');
const { getFixStyleOnlyEntriesPlugin } = require('../plugins/fix-style-only-entries-plugin');
const { getDuplicatePackageCheckerPlugin  } = require('../plugins/duplicate-package-checker-plugin');
const { getForkTsCheckerWebpackPlugin  } = require('../plugins/fork-ts-checker-webpack-plugin');
const { getDefinePlugin  } = require('../plugins/define-plugin');
const { getCleanWebpackPlugin  } = require('../plugins/clean-webpack-plugin');
const { getHtmlWebpackPlugin } = require('../plugins/html-webpack-plugin');
const { getMiniCssExtractPlugin } = require('../plugins/mini-css-extract-plugin');
const { getLoadableWebpackPlugin } = require('../plugins/loadable-webpack-plugin');
const { getOpenBrowserPlugin } = require('../plugins/open-browser-webpack-plugin');

const getMainPluginsLoader = (target) => {
  const { targetIsWeb, targetIsNode } = getTargetInfo(target);

  return [
    MAIN_CONSTANTS.IS_PRODUCTION && getCleanWebpackPlugin(target),
    getDuplicatePackageCheckerPlugin(),
    getWatchIgnorePlugin(),
    getDefinePlugin(target),
    targetIsNode && getLoadableWebpackPlugin(),
    MAIN_CONSTANTS.IS_PRODUCTION && getNamedModulesPlugin(),
    targetIsWeb && MAIN_CONSTANTS.IS_DEVELOPMENT && getOccurenceOrderPlugin(),
    targetIsWeb && MAIN_CONSTANTS.IS_PRODUCTION && getFixStyleOnlyEntriesPlugin(),
    targetIsWeb && getForkTsCheckerWebpackPlugin(),
    targetIsWeb && getHtmlWebpackPlugin(),
    targetIsWeb && getMiniCssExtractPlugin(),
    targetIsWeb && getOpenBrowserPlugin(),
  ].filter(Boolean);
};

module.exports = { getMainPluginsLoader };
