'use-strict';

const { configLoader } = require('commitizen');
const longest = require('longest');
const map = require('lodash/map');

const config = configLoader.load() || {};
const regExpJiraTicket = /^(ux|devops)-[1-9][0-9]{0,5}$/gi;

const options = {
  scope: 'site-frontend',
  maxCommitWidth: 50,
};

const choicesList = {
  feature: {
    title: 'feature',
    description: 'Новая задача',
  },
  'bug-fix': {
    title: 'bug-fix',
    description: 'Исправить баг',
  },
  'hot-fix': {
    title: 'hot-fix',
    description: 'Срочное исправление бага',
  },
  epic: {
    title: 'epic',
    description: 'Новый эпик',
  },
};

const length = longest(
  Object.keys(choicesList),
).length + 1;

const choices = map(
  choicesList,
  (type, key) =>
    ({
      name: `${(`${key}:`).padEnd(length)} ${type.description}`,
      value: key,
    }),
);

module.exports = {
  prompter(cz, setCommit) {
    cz.prompt([
      {
        type: 'list',
        name: 'type',
        message: 'Выберите тип коммита:',
        choices,
        default: config.defaultType,
      },
      {
        type: 'input',
        name: 'jiraTicket',
        message: 'Введите тег задачи в JIRA (например UX-123):',
        validate: (value) => {
          const trimValue = value.trim();

          if (!trimValue.length > 0) {
            return 'Обязательное поле.';
          }

          if (!regExpJiraTicket.test(trimValue)) {
            return 'Неверный формат.';
          }

          return true;
        },
        transformer: (subject) =>
          subject.toUpperCase(),
        filter: (subject) => subject
          .trim()
          .toUpperCase(),
      },
      {
        type: 'input',
        name: 'commit',
        message: `Краткое наименование коммита (не более ${options.maxCommitWidth}):\n`,
        validate: (value) => {
          const trimValue = value.trim();

          if (!trimValue.length > 0) {
            return 'Обязательное поле.';
          }

          if (trimValue.length > options.maxCommitWidth) {
            return 'Сократите коммит.';
          }

          return true;
        },
        filter: (subject) => subject
          .trim()
          .toLowerCase(),
      },
    ]).then((answers) => {
      const { type, jiraTicket, commit } = answers;
      const str = `${type} (${options.scope}): [${jiraTicket}] ${commit}`;

      setCommit(str);
    }).catch((error) => {
      console.log(error);
    });
  },
};
