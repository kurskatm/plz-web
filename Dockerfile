FROM gitlab.chibbis.ru:4567/frontend/ssr-teplate/node:12.18.2-buster
ARG RUNCMD
ARG SITENAME
ARG DOMAIN
ARG API
WORKDIR /site-frontend
COPY . ./
RUN yarn global upgrade --cache-folder cache_yarn
RUN sed -i "/ASSETS_URL/c ASSETS_URL=https://${SITENAME}" ./webpack/webpack-server/render-service/.env.production
RUN sed -i "/HOST/c HOST=0.0.0.0" ./webpack/webpack-server/render-service/.env.production
RUN sed -i "/DOMAIN/c DOMAIN=https://${DOMAIN}" ./webpack/webpack-server/render-service/.env.production
RUN sed -i "/API([ ])=/c API=${API}" ./webpack/webpack-server/render-service/.env.production
RUN sed -i "/API_AUTH([ ])=/c API_AUTH=${API_AUTH}" ./webpack/webpack-server/render-service/.env.production
RUN sed -i "/PORT/c PORT=3000" ./webpack/webpack-server/render-service/.env.production
RUN sed -i "/DOMAIN/c DOMAIN=https://${DOMAIN}" ./webpack/webpack-client/.env.production
RUN sed -i "/API([ ])=/c API=${API}" ./webpack/webpack-client/.env.production
RUN sed -i "/API_AUTH([ ])=/c API_AUTH=${API_AUTH}" ./webpack/webpack-client/.env.production
RUN yarn config set registry https://verdaccio.chibbistest.ru
RUN yarn install
ENTRYPOINT ["yarn", "start-prod"]
