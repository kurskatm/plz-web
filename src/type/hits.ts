export interface THitsItem {
  [key: string]: string | number | unknown;
}
export interface TGetHitsArgs {
  cityId: string;
}

export type THitsList = THitsItem[];
