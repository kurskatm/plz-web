import { TRestaurantProductModifier } from '../restaurants/restaurant-products';

export interface TFavoriteRestaurant {
  id: string;
  storeId: string;
  name: string;
  urlName: string;
  logo: string;
  banner: string;
  status: number;
  positiveReviewsPercent: number;
  deliveryCost: number;
  minCost: number;
  paymentTypes: number;
  selfPickup: boolean;
  workingTime: {
    openingTime: string;
    closingTime: string;
  };
  openingDate: string;
  isOpen: boolean;
  isNew: boolean;
  hasPaymentForPoints: boolean;
  hasPromotions: boolean;
}

export type TFavoriteRestaurantsList = TFavoriteRestaurant[];

export interface TFavoriteProduct {
  id: string;
  name: string;
  price: number;
  description: string;
  imageUrls: string[];
  weight: string;
  modifiers: TRestaurantProductModifier[];
  restaurant: {
    id: string;
    name: string;
    logo: string;
  };
  availableInBonusPoints: boolean;
}

export type TFavoriteProductsList = TFavoriteProduct[];

export interface TFavoritesSummaryData {
  restaurants: TFavoriteRestaurantsList;
  products: TFavoriteProductsList;
}
