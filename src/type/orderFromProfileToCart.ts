export interface TRestaurantInfo {
  id: string;
  storeId: string;
  name: string;
  logoUrl: string;
  minimalOrderCost: number;
  averageDeliveryTimeMinutes: 0;
  supportsDeliveryToSpecifiedLocation: boolean;
}

export interface TSelectedModifier {
  id: string;
  name: string;
  price: number;
}

export type TSelectedModifiersArray = TSelectedModifier[];

export interface TSelectedModifierGroup {
  id: string;
  name: string;
  selectedModifiers: TSelectedModifiersArray;
}

export type TSelectedModifierGroupsArray = TSelectedModifierGroup[];

export interface TProduct {
  id: string;
  name: string;
  imageUri: string;
  price: number;
  count: number;
  inBonusPoint: boolean;
  description: string;
  selectedModifierGroups: TSelectedModifierGroupsArray;
  externalId: string;
}

export type TProductsArray = TProduct[];

export interface TPaymentDetailsInfo {
  deliveryCost: number;
  discount: number;
  total: number;
  bonusReward: number;
}

export interface TSuggestDeliveryConditionInfo {
  orderCost: number;
  deliveryCost: number;
}

export interface TOrderToCart {
  id: string;
  restaurant: TRestaurantInfo;
  products: TProductsArray;
  cutleryCount: number;
  paymentDetails: TPaymentDetailsInfo;
  suggestiveDeliveryCondition: TSuggestDeliveryConditionInfo;
}
