export interface TRequestParams {
  method?: string;
  async?: boolean;
  url: string;
  headers?: Record<string, unknown>;
  body?: Record<string, unknown>;
  createXHR?: () => void;
}
