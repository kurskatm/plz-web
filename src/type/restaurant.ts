export interface TRestaurantItem {
  [key: string]: string | number | unknown;
}

export interface TRestaurantStoresItem {
  id: string;
  address: string;
}
export interface TGetRestaurantsArgs {
  cityId?: string;
  lat?: number;
  lng?: number;
}

export type TRestaurantList = TRestaurantItem[];

export interface TRestaurantInfo {
  id: string;
  cityId: string;
  name: string;
  urlName: string;
  logo: string;
  stores: string[];
}

export type TRestaurantsInfoArray = TRestaurantInfo[];
