export interface TContacts {
  phoneNumber: string;
  email: string;
  address: string;
  lat: number;
  lng: number;
}
