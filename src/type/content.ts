// Modules
import {
  FC,
  ReactNode,
  NamedExoticComponent,
  MemoExoticComponent,
  ForwardRefExoticComponent,
  RefObject,
  RefAttributes,
} from 'react';

export type TSetRef = (instance: HTMLElement) => RefObject<HTMLElement>;

export type TContent =
  | NamedExoticComponent
  | MemoExoticComponent<ForwardRefExoticComponent<RefAttributes<unknown>>>
  | JSX.Element
  | ReactNode
  | FC
  | string
  | number
  | boolean
  | null
  | undefined;
