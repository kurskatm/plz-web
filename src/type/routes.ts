// Modules
import { LoadableComponent } from '@loadable/component';

export interface TCityRouterParams {
  cityUrl?: string;
}

export interface TObjConfig {
  path: string;
  exact: boolean;
  component: LoadableComponent<Record<string, unknown>>;
}
export type TRouterConfig = TObjConfig[];

export type TGetRoutes = (prefix?: string) => TRouterConfig;
