export type TTimeoutId = ReturnType<typeof setTimeout> | null;
export type TIntervalId = ReturnType<typeof setInterval> | null;
