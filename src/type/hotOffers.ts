interface TRestaurantInfo {
  id: string;
  urlName: string;
  name: string;
  logo: string;
}

export interface THotOffersItem {
  id: string;
  name: string;
  description: string;
  imagePath: string;
  restaurant: TRestaurantInfo;
}
export interface TGetHotOffersArgs {
  cityId?: string;
  lat?: number;
  lng?: number;
}

export type THotOffersList = THotOffersItem[];
