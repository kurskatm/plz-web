export interface TPromocodeInfo {
  orderId: string;
  name: string;
  coefficient: number;
}

export interface TRestaurantInfo {
  id: string;
  name: string;
  urlName: string;
  cityUrlName: string;
  logo: string;
  phoneNumber: number;
}

export interface TDeliveryAddressInfo {
  cityId: string;
  userAddressId: string;
  street: string;
  house: string;
  flat: string;
}

export interface TDeliveryDetailsInfo {
  deliveryAddress: TDeliveryAddressInfo;
  personsCount: number;
  deliverToTime: string;
  shortChange: string;
  selfPickup: boolean;
  chibbisDelivery: boolean;
  userComment: string;
}

export interface TSelectedModifier {
  id: string;
  name: string;
  price: number;
}

export type TSelectedModifiersArray = TSelectedModifier[];

export interface TModifiersGroup {
  id: string;
  selectedModifiers: TSelectedModifiersArray;
}

export type TModifiersGroupsArray = TModifiersGroup[];

export interface TModifiersInfo {
  modifiersGroups: TModifiersGroupsArray;
}

export interface TProductInfo {
  id: string;
  name: string;
  quantity: number;
  price: number;
  priceWithModifiers: number;
  paidByBonusPoints: boolean;
  externalId: string;
  modifiers: TModifiersInfo;
}

export interface TOrderInfo {
  id: string;
  customerId: string;
  restaurantId: string;
  storeId: string;
  totalCost: number;
  deliveryCost: number;
  bonusSpent: number;
  bonusReceived: number;
  status: number;
  addedOn: string;
  products: TProductInfo[];
  reviewId: string | null;
  deliveryDetails: TDeliveryDetailsInfo;
  promoCodeUsageId: string;
  paymentType: number;
  estimatedCompletionTime: string;
  isContactCustomerAvailable: boolean;
}

export interface TOrderItem {
  order: TOrderInfo;
  restaurant: TRestaurantInfo;
  promoCode: TPromocodeInfo | null;
}

export interface TNewOrdersInfo {
  items: TOrderItem[];
  after: string;
}
