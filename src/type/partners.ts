export interface TPartnersCounters {
  ordersCount: number;
  usersCount: number;
  restaurantsCount: number;
}
