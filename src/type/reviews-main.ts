export interface TReviewComment {
  author: string;
  commentator: string;
  text: string;
  addedOn: string;
}

export type TReviewCommentList = TReviewComment[];

export interface TRateValue {
  value: number;
}

export interface TRate {
  taste: TRateValue;
  speed: TRateValue;
  service: TRateValue;
  cost: TRateValue;
  setisfiedByContent: boolean;
}

export interface TOrder {
  name: string;
  quantity: number;
}

export type TOrderList = TOrder[];

export interface TRestaurantInfo {
  id: string;
  name: string;
  urlName: string;
  logo: string;
}

export interface TReviewMain {
  id: string;
  text: string;
  order: TOrderList;
  addedOn: string;
  username: string;
  restaurant: TRestaurantInfo;
}

export type TReviewsMainList = TReviewMain[];

export interface TReviewsMainFetchArgs {
  restaurantsIds: string[];
}
