export interface TNewGeocodeData {
  precision: string;
  lat: number;
  lng: number;
  street: string;
  house: string;
  cityId: string;
  cityName: string;
}
