export interface TVerifyPhoneRequestArgs {
  phone: string;
  userId?: string;
  resendToken?: string;
}
export interface TVerifyOtpRequestArgs {
  verification_code?: string;
  phone?: string;
  city_id?: string;
  refresh_token?: string;
}
