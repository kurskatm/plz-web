// Types
import { TRestaurantProductModifier } from '@type/restaurants/restaurant-products';
import { TRestaurantStore } from './restaurants/restaurant-stores';

export interface TSelectedModifier {
  id: string;
}

export interface TSelectedCategoryModifier {
  id: string;
  selectedModifiers: TSelectedModifier[];
}

export interface TRestaurantShopingCart {
  id: string | null;
  name: string | null;
  logo?: string | null;
  logoUrl?: string | null;
  stores?: TRestaurantStore[];
  isOpen?: boolean;
  openingDate?: string | number;
  urlName?: string;
}

export interface TShopingCartProduct {
  id: string;
  name: string;
  description: string;
  price: number;
  count: number;
  inBonusPoint: boolean;
  modifiers?: TRestaurantProductModifier[];
}

export interface TShopingCartOptions {
  cutlery: number | null;
  averageDeliveryTimeMinutes?: number;
  minimalOrderCost?: number;
  deliveryCost?: number;
  discount?: number;
  total?: number;
  bonusReward?: number;
  minOrderCostForFreeDelivery?: number;
  isOpen?: boolean;
  openingDate?: string;
  paymentTypes?: number;
  selfPickupStores?: TRestaurantStore[];
}

export interface TShopingCartSaveProduct {
  id: string;
  count: number;
  inBonusPoint: boolean;
  cityId: string;
  selectedModifierGroups?: TSelectedCategoryModifier[];
}

export interface TShopingCartSaveData {
  id: string;
  restaurantId: string;
  cityId: string;
  products: TShopingCartSaveProduct[];
}
