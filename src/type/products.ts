export interface TFreeProductsItem {
  [key: string]: string | number | unknown;
}
export interface TGetProductsArgs {
  cityId: string;
}

export type TFreeProductsList = TFreeProductsItem[];
