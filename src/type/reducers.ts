// Types
import { TUiModel } from '@models/ui/types';

export interface TReducerUiData {
  type: string;
  payload: {
    status: string | null | undefined;
  };
}

export type TUiReducer = (state: TUiModel, data: TReducerUiData) => TUiModel;
