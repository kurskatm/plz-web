export interface TCitiesLocation {
  lat: number;
  lng: number;
}
export interface TCitiesItem {
  id: string;
  name: string;
  urlName: string;
  whereName: string;
  utcOffset: number;
  center: TCitiesLocation;
}

export type TCitiesList = TCitiesItem[];
export type TCitiesLocationList = TCitiesLocation[];
