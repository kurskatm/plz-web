// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type ActionCreator<T, K = any> = (
  payload?: K,
) => {
  type: T;
  payload?: K;
};
