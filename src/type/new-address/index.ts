export interface TNewAddressItem {
  name: string;
  description: string;
}

export type TNewAddressesList = TNewAddressItem[];
