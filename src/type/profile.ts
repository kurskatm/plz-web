// Orders
export interface TDelivery {
  address: string | null;
  pickup: boolean | null;
}
export type TGetFavoriteRestaurantsArgs = {
  userId: string | null;
};

export interface TRestaurantInfo {
  restaurantId: string;
  name: string;
  logo: string;
  storeId: string;
}

export interface TOrderCostInfo {
  total: number;
  delivery: number;
}

export interface TBonusBalanceInfo {
  spentAmount: number;
  receivedAmount: number;
}

export interface TModifierDate {
  id: string;
  name: string;
  price: number;
}

export interface TModifiersCategoryData {
  id: string;
  selectedModifiers: TModifierDate[];
}

export type TModifiersCategoryDataArray = TModifiersCategoryData[];

export interface TDeliveryAddress {
  cityId: string;
  flat: string | null;
  house: string;
  street: string;
  userAddressId: string;
}

export interface TDeliveryDetails {
  chibbisDelivery: boolean;
  deliverToTime: string | null;
  deliveryAddress: TDeliveryAddress;
  personsCount: number;
  selfPickup: boolean;
  shortChange: number | string | null;
  userComment: string | null;
}

export interface TProductInfo {
  name: string;
  quantity: number;
  price: number;
  priceWithModifiers: number;
  paidByBonusPoints: boolean;
  externalId: string;
  modifiers: {
    modifiersGroups: TModifiersCategoryDataArray;
  } | null;
  id: string;
}

export type TProductsInfoArray = TProductInfo[];

export interface TOrderData {
  id: string;
  restaurant: TRestaurantInfo;
  customerId: string;
  orderCost: TOrderCostInfo;
  bonusBalance: TBonusBalanceInfo;
  isContactCustomerAvailable: boolean;
  status: number;
  addedOn: string;
  products: TProductsInfoArray;
  deliveryDetails: TDeliveryDetails;
  paymentType: number;
  promoCodeUsageId: string | null;
  reviewId: string | null;
}

export interface TRestaurantData {
  id: string;
  restaurantName: string;
  restaurantLogo: string;
  urlName?: string;
  cityUrlName?: string;
  phoneNumber?: number;
}

export interface TPromoCodeData {
  orderId: string;
  promoCodeName: string;
  promoCodeCoefficient: number;
}

export interface TUserOrder {
  order: TOrderData;
  restaurant: TRestaurantData;
  promoCode: TPromoCodeData | null;
}

export type TUserOrdersData = TUserOrder[];

export interface TOrderReview {
  orderId: string;
  restaurantId: string;
  liked: boolean;
  taste: number | null;
  speed: number | null;
  service: number | null;
  cost: number | null;
  satisfiedByContent: boolean;
  text: string;
}

// Reviews
export interface TReviewRate {
  cost: number | null;
  satisfiedByContent: boolean;
  taste: number | null;
  speed: number | null;
  service: number | null;
}

export interface TReviewProduct {
  name: string;
  quantity: number;
}

export type TReviewOrder = TReviewProduct[];

export interface TUserReview {
  addedOn: string;
  comments: [];
  id: string;
  liked: string;
  order: TReviewOrder;
  rate: TReviewRate;
  restaurantId: string;
  text: string;
  userId: string;
  username: string;
}

export type TUserReviewsList = TUserReview[];
