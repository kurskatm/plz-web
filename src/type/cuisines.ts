export interface TCuisine {
  id: string;
  name: string;
  urlName: string;
  sortOrder: number;
  isMajor: boolean;
  imageUrl: string | null;
}
