export interface TFilter {
  id: string;
  name: string;
  urlName: string;
  sortOrder: number;
  isMajor: boolean;
}
