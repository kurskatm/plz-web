export type TQa = (id: string) => string;

export interface TReactQaSelectors {
  qa: TQa;
}
