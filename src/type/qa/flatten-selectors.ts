export type TQaSelectorsObject = Record<string, string>;
export type TQaSelectorsValue = Record<string, string | TQaSelectorsObject>;

export type TFlattenSelectors = (
  nestedSelectors: TQaSelectorsValue,
  prefix?: string,
) => TQaSelectorsObject;
