export interface TPromocodeResult {
  promoCodeId: string;
  wasAccepted: boolean;
  acceptedMessage: string;
  errorMessage: string | null;
}
