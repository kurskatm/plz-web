export interface TReviewComment {
  author: string;
  commentator: string;
  text: string;
  addedOn: string;
}

export type TReviewCommentList = TReviewComment[];

export interface TOrder {
  name: string;
  quantity: number;
}

export type TOrderList = TOrder[];

export interface TReview {
  id: string;
  restaurantId: string;
  liked: boolean;
  text: string;
  addedOn: string;
  username: string;
  comments: TReviewCommentList;
  order: TOrderList;
}

export type TReviewList = TReview[];
