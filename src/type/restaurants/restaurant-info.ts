// Main Types
import { TRestaurantCategoriesList } from './restaurant-categories';
import { TRestaurantScheduleList } from './restaurant-schedule';
import { TDeliveryConditionsList } from './delivery-conditions';

export interface TRestaurantInfo {
  id: string;
  storeId: string;
  status: number;
  description: string;
  paymentTypes: number;
  selfPickup: boolean;
  specializations: TRestaurantCategoriesList;
  weekSchedule: TRestaurantScheduleList;
  deliveryConditions: TDeliveryConditionsList;
}
