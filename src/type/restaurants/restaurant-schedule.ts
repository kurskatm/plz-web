export interface TRestaurantSchedule {
  dayOfWeek: string;
  fromTime: number;
  toTime: number;
}

export type TRestaurantScheduleList = TRestaurantSchedule[];
