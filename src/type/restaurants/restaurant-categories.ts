export interface TRestaurantCategory {
  id: string;
  name: string;
  urlName: string;
  icon?: string;
}

export type TRestaurantCategoriesList = TRestaurantCategory[];
