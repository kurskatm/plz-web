export interface TRestaurantLegal {
  name: string;
  ogrn: string;
  inn: string;
  address: string;
}
