export interface TRestaurantProductInfo {
  id: string;
  name: string;
  positiveReviewsPercent: number;
  reviewsCount: number;
  urlName: string;
}

export interface TRestaurantProductModifierItem {
  id: string;
  name: string;
  price: number;
}

export interface TRestaurantProductModifier {
  id: string;
  name: string;
  required?: boolean;
  modifiers: TRestaurantProductModifierItem[];
  minSelectedModifiers?: number;
  maxSelectedModifiers?: number;
}

export interface TRestaurantProduct {
  id: string;
  name: string;
  price: number;
  availableInBonusPoints: boolean;
  description: string;
  imageUrls: string[];
  weight: string;
  isFavorite: boolean;
  modifiers?: TRestaurantProductModifier[];
  restaurantUrlName?: string;
  restaurantId?: string;
}

export interface TRestaurantCategoryProducts {
  id: string;
  name: string;
  urlName: string;
  products: TRestaurantProduct[];
}

export interface TRestaurantProductModifiers extends TRestaurantProduct {
  restaurant: TRestaurantProductInfo;
}
