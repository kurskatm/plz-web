export interface TDeliveryConditions {
  deliveryAreaId: string;
  areaName: string;
  deliveryCost: number;
  orderCost: number;
}

export type TDeliveryConditionsList = TDeliveryConditions[];

export interface TScheduleGroup {
  dayOfWeek: number;
  timeFrom: string;
  timeTo: string;
}

export type TScheduleGroupsList = TScheduleGroup[];

export interface TDeliveryAreaCondition {
  deliveryAreaId: string;
  deliveryAreaName: string;
  restId: string;
  restStoreId: string;
  deliveryCost: number;
  orderCost: number;
  scheduleGroups: TScheduleGroupsList;
}

export type TDeliveryAreaConditionsList = TDeliveryAreaCondition[];
