import { TShopingCartModel } from '@/models/shoping-cart/types';
import { TShopingCartProduct } from '../shoping-cart';
import { TRestaurantStore } from './restaurant-stores';

export interface TRestaurantCardSpecialization {
  id: string;
  name: string;
  urlName: string;
}

export type TRestaurantCardSpecializationList = TRestaurantCardSpecialization[];

export interface TRestaurantCardWorkingTime {
  openTime?: number;
  closeTime?: number;
  openingTime?: string;
  closingTime?: string;
}

export interface TRestaurantCard {
  id: string;
  storeId: string;
  name: string;
  urlName: string;
  logo: string;
  status: number;
  positiveReviewsPercent: number;
  reviewsCount: number;
  deliveryCost: number;
  minCost: number;
  paymentTypes: number;
  selfPickup: boolean;
  delivery: number;
  specializations: TRestaurantCardSpecializationList;
  workingTime: TRestaurantCardWorkingTime;
  openingDate: number;
  isOpen: boolean;
  isNew: boolean;
  hasSpecialPlacement: boolean;
  hasPaymentForPoints: boolean;
  hasPromotions: boolean;
  isFavorite: boolean;
  stores?: TRestaurantStore[];
}

export interface TRestaurantDataToUpdateCart {
  cityId: string;
  restaurant: TRestaurantCard;
  product: TShopingCartProduct;
  shopingCart: TShopingCartModel;
}
