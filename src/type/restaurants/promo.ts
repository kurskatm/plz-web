export interface TRestaurantPromo {
  id: string;
  name: string;
  description: string;
  imagePath: string;
}

export type TRestaurantPromoList = TRestaurantPromo[];
