export interface TRestaurant {
  id: string;
  storeId: string;
  name: string;
  urlName: string;
  logo: string;
  status: number;
  positiveReviewsPercent: number;
  reviewsCount: number;
}
