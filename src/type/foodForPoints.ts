import { TRestaurantCard } from '@type/restaurants/restaurant-card';
import { TRestaurantProduct } from '@type/restaurants/restaurant-products';

export type TRestaurantFodForPoints = Omit<TRestaurantCard, 'stores' | 'workingTime'> & {
  banner: string;
  workingTime: {
    openingTime: string;
    closingTime: string;
  };
};

export type TCityHitRestaurant = {
  id: string;
  name: string;
  logo: string;
  urlName: string;
};

export type TProductFodForPoints = Omit<
  TRestaurantProduct,
  'weight' | 'restaurantUrlName' | 'restaurantId'
> & {
  cityHitRestaurant: TCityHitRestaurant;
};

export type TFoodForPoints = {
  restaurants: TRestaurantFodForPoints[];
  products: TProductFodForPoints[];
};
