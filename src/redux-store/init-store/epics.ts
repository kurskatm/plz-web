// Modules
import { combineEpics } from 'redux-observable';
// Epics
import { allEpics } from '@epics';

export const rootEpic = combineEpics(allEpics);
