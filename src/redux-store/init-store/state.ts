/* eslint-disable @typescript-eslint/no-explicit-any */
// Models
import { initialState } from '@models';
// Utils
import { Consts } from '@utils';

const { IS_CLIENT } = Consts.ENV;

export const createInitialState = (reduxData = {}): any => {
  let state = { ...reduxData };
  if (IS_CLIENT && (window as any).REDUX_DATA) {
    state = {
      ...initialState,
      ...state,
      ...(window as any).REDUX_DATA,
    };
  }
  return state;
};
