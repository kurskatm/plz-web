// Modules
import { compose, createStore, applyMiddleware } from 'redux';
import { createEpicMiddleware } from 'redux-observable';
import { composeWithDevTools } from 'redux-devtools-extension';
import { routerMiddleware } from 'connected-react-router';
// Utils
import { Consts } from '@utils';
// Main Types
import { TInitStore } from '@reducers/types';
// Store
import { rootReducer } from './reducer';
import { createInitialState } from './state';
import { rootEpic } from './epics';

const { IS_DEVELOPMENT } = Consts.ENV;

const options = {
  name: 'Chibbis-SSR-Template-Redux-Store',
  realtime: true,
  trace: true,
  traceLimit: 25,
};

export const initStore: TInitStore = (history, reduxData) => {
  const epicMiddleware = createEpicMiddleware();
  const middleware = [routerMiddleware(history), epicMiddleware];

  let composeEnhancers;

  if (IS_DEVELOPMENT) {
    composeEnhancers = composeWithDevTools(options);
  } else {
    composeEnhancers = compose;
  }

  const enhancer = composeEnhancers(applyMiddleware(...middleware));
  const initialState = createInitialState(reduxData);
  const store = createStore(rootReducer(history), initialState, enhancer);

  epicMiddleware.run(rootEpic);

  return store;
};
