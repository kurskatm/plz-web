// Utils
import { objectEnv } from './object-env';

export const Consts = {
  ACTIONS: {
    PREFIX: '@@//',
  },

  CHAT: {
    METHOD: 'TalkMe',
    PATH: 'https://lcab.talk-me.ru/support/support.js',
  },

  COOKIE: {
    DATA: {
      ADDRESS: 'address',
      CITY_DATA: 'city',
      SMART_BANNER: 'SMART_BANNER',
      GEO_CODE: 'geodata',
      CART_ID: 'cartId',
      PICKUP: 'pickup',
      PROFILE: {
        USER_ID: 'userId',
        // ACCESS_TOKEN: '__at',
        ACCESS_TOKEN: 'jwt_at',
        REFRESH_TOKEN: 'jwt_rt',
      },
      INSTALL_MOBILE_APP_SHOW: 'install_mobile_app_show',
    },
    EXPIRES: 365,
  },

  DOM: {
    ROOT: 'root',
  },

  DEBOUNCE: {
    REQUEST_MS: 500,
    MIN_LOADING_MS: 600,
    THROTTLE_CLICK_MS: 400,
    USER_CHANGE_MS: 1000,
  },

  ENV: objectEnv,

  PAGINATION: {
    DEFAULT: {
      SKIP: 0,
      TAKE: 20,
      HAS_MORE: true,
    },
  },

  REQUEST: {
    HEADERS: {
      ACCEPT: 'Accept',
      AUTHORIZATION: 'Authorization',
      CONTENT_TYPE: 'Content-Type',
    },

    RESPONSE_TYPE: {
      APPLICATION_JSON: 'application/json',
      JSON: 'json',
    },

    UI: {
      STATUS: {
        IDLE: 'idle',
        PENDING: 'pending',
        SUCCESS: 'success',
        ABORT: 'abort',
        ERROR: 'error',
        EMPTY: 'empty',
      },
    },
  },

  ROUTES: {
    CHECKOUT: {
      MAIN: { PATH: '/object' },
      SUCCESS: { PATH: '/checkout/success' },
    },
    CITIES: { PATH: '/' },
    CART: { PATH: '/cart' },
    PROFILE: { PATH: '/profile' },
    RESTAURANTS: {
      MAIN: { PATH: '/restaurants' },
      RESTAURANT: {
        MAIN: {
          PATH: '/restaurant',
          PARAMS: '/:restaurantName',
          CLIENT_PARAMS: '/:tabName?',
        },
        SECTIONS: {
          MENU: { PATH: '/menu' },
          REVIEWS: { PATH: '/reviews' },
          INFO: { PATH: '/info' },
        },
      },
    },
    REGISTRATION: { PATH: '/registration' },
    INFO_PAGES: {
      ABOUT: { PATH: '/about' },
      CONTACTS: { PATH: '/contacts' },
      WORK_WITH_US: { PATH: 'https://job.chibbis.ru/' },
      PARTNERS: { PATH: '/become-partners' },
      FOOD: { PATH: '/food-for-points' },
      FEEDBACK: { PATH: '/support' },
      REGLAMENT: { PATH: '/reglament' },
      HELP: { PATH: '/help' },
    },
    PROFILE_PAGES: {
      INFO: { PATH: '/profile/info' },
      ORDERS: { PATH: '/profile/orders' },
      SCORES: { PATH: '/profile/scores' },
      REVIEWS: { PATH: '/profile/reviews' },
      FAVORITE: { PATH: '/profile/favorite' },
    },
    ERRORS: {
      ERROR_404: { PATH: '/page-not-found' },
      ERROR_404_TECHNICAL_WOKRS: { PATH: '/technical-wokrs' },
      ERROR_500: { PATH: '/server-error' },
      ERROR_502: { PATH: '/bad-gateway' },
    },
    HOME: { PATH: '/' },
    SPECIALIZATION: { PATH: '/:cityUrl/restaurants/:specId' },
    SELECTION: { PATH: '/:cityUrl/selection/:specId' },
  },

  EMAILS: {
    INFO: 'info@chibbis.ru',
  },

  THROTTLE: {
    DEFAULT: 400,
  },

  LINKS: {
    POLICY: 'docs/client_data_processing_policy.pdf',
  },

  REGEXP: {
    SIMPLE_PHONE: /^(\d{1})(\d{3})(\d{3})(\d{2})(\d{2})$/,
    FORMATTED_PHONE: /^(\d{1}) \({,1}(\d{3})\){,1} (\d{3})-(\d{2})-(\d{2})$/,
  },
};
