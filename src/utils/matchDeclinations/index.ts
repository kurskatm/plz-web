import { TMatchDeclinations } from './types';

export const matchDeclinations: TMatchDeclinations = (number, declinations) => {
  const normalized = +Math.floor(number).toString().slice(-2);
  if ([11, 12, 13, 14].includes(normalized)) return declinations[2];
  switch (normalized % 10) {
    case 1:
      return declinations[0];
    case 2:
    case 3:
    case 4:
      return declinations[1];
    default:
      return declinations[2];
  }
};
