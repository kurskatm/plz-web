export type TMatchDeclinations = (number: number, declinations: string[]) => string;
