export const SPECS_LIST = [
  {
    id: '00000002-ae86-4653-9db8-f6bdacd094e5',
    name: 'Суши',
    urlName: 'sushi',
  },
  {
    id: '00000001-ae86-4653-9db8-f6bdacd094e5',
    name: 'Пицца',
    urlName: 'pizza',
  },
  {
    id: '00000003-ae86-4653-9db8-f6bdacd094e5',
    name: 'Бургеры',
    urlName: 'burgers',
  },
  {
    id: '00000005-ae86-4653-9db8-f6bdacd094e5',
    name: 'Пироги',
    urlName: 'cakes',
  },
  {
    id: '00000007-ae86-4653-9db8-f6bdacd094e5',
    name: 'Шашлыки',
    urlName: 'shashlyki',
  },
  {
    id: '000003ea-ae86-4653-9db8-f6bdacd094e5',
    name: 'Китайская еда',
    urlName: 'kitajskaya',
  },
  {
    id: '00000008-ae86-4653-9db8-f6bdacd094e5',
    name: 'Бизнес-ланчи',
    urlName: 'lunches',
  },
  {
    id: '0000000b-ae86-4653-9db8-f6bdacd094e5',
    name: 'Шаверма',
    urlName: 'shaverma',
  },
  {
    id: '00000009-ae86-4653-9db8-f6bdacd094e5',
    name: 'Лапша',
    urlName: 'eda_v_korobochkah',
  },
  {
    id: '00111119-ae86-4653-9db8-f6bdacd094e5',
    name: 'Самовывоз',
    urlName: 'samovyvoz',
  },
  {
    id: '00111119-ae86-4653-9db8-f6bdacd094e5',
    name: 'Пицца и Суши',
    urlName: 'pizza-sushi',
  },
  {
    id: '00000021-ae86-4653-9db8-f6bdacd094e5',
    name: 'Картошка Фри',
    urlName: 'potato-fri',
  },
];
