import { TApplyImageFallback } from './types';

export const applyImageFallback: TApplyImageFallback = (fallBackImage: string) => ({
  currentTarget,
}: {
  currentTarget: EventTarget & HTMLImageElement;
}): void => {
  currentTarget.onerror = null;
  currentTarget.src = `${fallBackImage}`;
};
