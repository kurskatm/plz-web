type TCurrentTarget = { currentTarget: EventTarget & HTMLImageElement };

export type TApplyImageFallback = (
  fallBackImage: string,
) => ({ currentTarget }: TCurrentTarget) => void;
