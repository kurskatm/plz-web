import { TConfig } from './types';

// EVENTS QUEUE
// ---
// saveAuthData
// checkPointsAmount
// anotherRest
// anotherPointProduct
// changeAddress
// showModifiers
// saveRestId
// trackEvent
// goToRest
// withModifiers
// update cart
// ---

export const config: TConfig = {
  base: {
    restDataFrom: 'item',
    pointsAttr: 'availableInBonusPoints',
    checks: ['saveRestId', 'goToRest'],
    trackEvents: ['HitAdd'],
    anotherRest: {
      showModal: true,
      withRedirect: true,
    },
    changeAddress: {
      showModal: true,
      withRedirect: false,
    },
  },
  shopingCart: {
    restDataFrom: 'shopingCart',
    pointsAttr: 'inBonusPoint',
    checks: ['withModifiers'],
    trackEvents: [],
    anotherRest: {
      showModal: false,
      withRedirect: false,
    },
    changeAddress: {
      showModal: true,
      withRedirect: false,
    },
  },
  freeFood: {
    restDataFrom: 'item',
    pointsAttr: 'availableInBonusPoints',
    checks: ['goToRest', 'saveRestId', 'auth'],
    trackEvents: [],
    anotherRest: {
      showModal: false,
      withRedirect: false,
    },
    changeAddress: {
      showModal: true,
      withRedirect: false,
    },
  },
  favoriteFood: {
    restDataFrom: 'item',
    pointsAttr: 'availableInBonusPoints',
    checks: ['saveRestId', 'goToRest'],
    trackEvents: ['add-cart'],
    anotherRest: {
      showModal: true,
      withRedirect: true,
    },
    changeAddress: {
      showModal: true,
      withRedirect: false,
    },
  },
  restMenu: {
    restDataFrom: 'item',
    pointsAttr: 'availableInBonusPoints',
    checks: ['saveAuthData', 'checkPointsAmount', 'anotherPointProduct', 'showModifiers'],
    trackEvents: ['try_to_add_EZB', 'add-cart'],
    anotherRest: {
      showModal: true,
      withRedirect: false,
    },
    changeAddress: {
      showModal: true,
      withRedirect: false,
    },
  },
};
