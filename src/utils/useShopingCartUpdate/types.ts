import { TCitiesItem } from '@type/cities';
import { TShopingCartModel } from '@models/shoping-cart/types';
import { TShowRestaurantModals } from '@actions/restaurant/modal/action-types';
import { TUpdateShopingCart } from '@actions/shoping-cart/change/action-types';
import { TProfileModel } from '@models/profile/types';
import { TRestaurantProduct } from '@type/restaurants/restaurant-products';
import { TCityHitRestaurant } from '@type/foodForPoints';
import { TRestaurantCard } from '@type/restaurants/restaurant-card';

type TShopingCartUpdateProps = {
  city?: TCitiesItem;
  shopingCart: TShopingCartModel & {
    imageUrls?: string[];
  };
  showRestaurantModals?: TShowRestaurantModals;
  shopingCartUpdate: TUpdateShopingCart;
  profile: TProfileModel;
  configName?: 'base' | 'shopingCart' | 'freeFood' | 'favoriteFood' | 'restMenu';
  restaurantCard?: TRestaurantCard;
  balance?: string;
  address?: string;
};

export type TShopingCartItem = Omit<
  TRestaurantProduct,
  'availableInBonusPoints' | 'imageUrls' | 'weight' | 'isFavorite'
> & {
  inBonusPoint?: boolean;
  availableInBonusPoints?: boolean;
  imageUrls?: string[];
  weight?: string;
  isFavorite?: boolean;
  cityHitRestaurant?: TCityHitRestaurant;
};

export type TUseShopingCartUpdate = ({
  city,
  shopingCart,
  showRestaurantModals,
  shopingCartUpdate,
  configName,
}: TShopingCartUpdateProps) => [(item: TShopingCartItem) => (count: number) => void];

type TConfigItem = {
  restDataFrom: 'item' | 'shopingCart';
  checks: string[];
  pointsAttr: 'availableInBonusPoints' | 'inBonusPoint';
  trackEvents: string[];
  anotherRest: {
    showModal: boolean;
    withRedirect: boolean;
  };
  changeAddress: {
    showModal: boolean;
    withRedirect: boolean;
  };
};

export type TConfig = {
  base: TConfigItem;
  shopingCart: TConfigItem;
  freeFood: TConfigItem;
  favoriteFood: TConfigItem;
  restMenu: TConfigItem;
};
