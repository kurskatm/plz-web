// Modules
import { useCallback, useState, useEffect } from 'react';
import { isNil } from 'lodash';
import { useHistory } from 'react-router';
import { useDispatch } from 'react-redux';
// Lib
import { UpdateShopingCartManager } from '@lib/shoping-cart/update-cart';
import { UpdateShopingCartWithModifiersManager } from '@lib/shoping-cart/update-cart-with-modifiers';
// Utils
import { trackEvent } from '@utils/analytics';
// Actions
import { authModalShow } from '@actions/auth';
import { showRestaurantModals, showModifiersCardModal } from '@actions/restaurant/modal';
import { restaurantCardIdSave } from '@/actions/restaurant/main/id';
// Types
import { TUseShopingCartUpdate, TShopingCartItem } from './types';
// Config
import { config } from './config';

export const useShopingCartUpdate: TUseShopingCartUpdate = ({
  city,
  shopingCart,
  shopingCartUpdate,
  profile,
  restaurantCard,
  balance,
  address,
  configName = 'base',
}) => {
  const [redirectData, setRedirectData] = useState(null);
  const [shopingCartData, setShopingCartData] = useState(null);
  const [authShopingCartData, setAuthShopingCartData] = useState(null);
  const history = useHistory();
  const dispatch = useDispatch();
  useEffect(
    () => {
      if (
        !isNil(profile.newGeoCode.data) &&
        profile.newGeoCode.data.precision === 'exact' &&
        !isNil(redirectData)
      ) {
        shopingCartUpdate(redirectData.shopingCartData);
        dispatch(restaurantCardIdSave({ id: redirectData.restaurantId }));
        history.push(redirectData.link);
        setRedirectData(null);
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [profile.newGeoCode.data?.precision, redirectData],
  );

  useEffect(
    () => {
      if (
        config[configName]?.checks?.includes('saveAuthData') &&
        !isNil(profile.auth) &&
        profile.auth.length &&
        !isNil(authShopingCartData)
      ) {
        const newShopingCartState = new UpdateShopingCartManager(authShopingCartData).init();
        shopingCartUpdate(newShopingCartState);
        setAuthShopingCartData(null);
        trackEvent('try_to_add_EZB');
      }
      if (
        address?.length &&
        !isNil(profile?.newGeoCode?.data) &&
        profile?.newGeoCode?.data?.precision === 'exact' &&
        !isNil(shopingCartData)
      ) {
        const newShopingCartState = new UpdateShopingCartManager(shopingCartData).init();
        shopingCartUpdate(newShopingCartState);
        setShopingCartData(null);
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [
      profile?.auth,
      address?.length,
      shopingCartData,
      authShopingCartData,
      profile?.newGeoCode?.data,
    ],
  );

  const updateShopingCart = useCallback(
    (item: TShopingCartItem) => (count: number) => {
      if (config[configName]?.checks?.includes('auth') && !profile.auth) {
        dispatch(authModalShow(true));
        return;
      }
      const props = { item, shopingCart };

      const data = {
        cityId: city?.id,
        restaurant: {
          name: '',
          id: '',
          logo: props[config[configName].restDataFrom]?.imageUrls?.[0] || undefined,
          // @ts-ignore
          ...(props[config[configName].restDataFrom].restaurant ||
            // @ts-ignore
            props[config[configName].restDataFrom].cityHitRestaurant ||
            restaurantCard || {
              // @ts-ignore
              id: props[config[configName].restDataFrom]?.restaurantId,
              // @ts-ignore
              urlName: props[config[configName].restDataFrom]?.restaurantUrlName,
            } ||
            {}),
        },
        product: {
          id: item.id,
          name: item.name,
          description: item.description,
          price: item.price,
          inBonusPoint: item[config[configName].pointsAttr],
          modifiers: item.modifiers,
          count,
        },

        shopingCart,
      };

      const link = `/${city?.urlName}/restaurant/${
        // @ts-ignore
        item.restaurant?.urlName || item?.restaurantUrlName || item?.cityHitRestaurant?.urlName
      }`;

      // saveAuthData
      if (
        config[configName]?.checks?.includes('saveAuthData') &&
        isNil(profile.auth) &&
        item[config[configName].pointsAttr]
      ) {
        setAuthShopingCartData(data);
        return;
      }

      // checkPointsAmount
      if (
        config[configName]?.checks?.includes('checkPointsAmount') &&
        !isNil(profile.auth) &&
        profile.auth.length &&
        count > 0 &&
        item.availableInBonusPoints &&
        item.price > Number(balance)
      ) {
        dispatch(
          showRestaurantModals({
            showModal: true,
            modalType: 'not-enough-points',
            bonusPointsRest: item.price - Number(balance),
          }),
        );
        return;
      }

      // anotherRest
      if (
        config[configName].anotherRest.showModal &&
        !isNil(shopingCart.restaurant) &&
        !isNil(shopingCart.restaurant.id) &&
        // @ts-ignore
        (item?.restaurant?.id || item?.cityHitRestaurant?.id || item.restaurantId) !==
          shopingCart.restaurant.id
      ) {
        dispatch(
          showRestaurantModals({
            showModal: true,
            modalType: 'another-restaurant',
            dataToUpdateCart: data,
            link: config[configName].anotherRest.withRedirect && link,
          }),
        );
        return;
      }

      // anotherPointProduct
      if (
        config[configName]?.checks?.includes('anotherPointProduct') &&
        shopingCart.products?.filter((product) => product.inBonusPoint).length === 1 &&
        item[config[configName].pointsAttr] &&
        count === 1
      ) {
        dispatch(
          showRestaurantModals({
            showModal: true,
            modalType: 'another-point-product',
            dataToUpdateCart: data,
          }),
        );
        return;
      }

      // changeAddress
      if (
        config[configName].changeAddress.showModal &&
        profile.newGeoCode.data?.precision !== 'exact'
      ) {
        dispatch(
          showRestaurantModals({
            showModal: true,
            modalType: 'change-address',
          }),
        );
        if (config[configName].changeAddress.withRedirect) {
          setRedirectData({
            shopingCartData: new UpdateShopingCartManager(data).init(),
            // @ts-ignore
            restaurantId: item.restaurant?.id,
            // @ts-ignore
            link: `/${city?.urlName}/restaurant/${item.restaurant?.urlName}`,
          });
        }
        setShopingCartData(data);
        return;
      }

      // showModifiers
      if (
        config[configName]?.checks?.includes('showModifiers') &&
        count === 1 &&
        !item[config[configName].pointsAttr] &&
        item.modifiers.length
      ) {
        dispatch(
          showModifiersCardModal({
            showModal: true,
            productId: item.id,
          }),
        );
        return;
      }

      // saveRestId
      if (config[configName]?.checks?.includes('saveRestId')) {
        dispatch(
          restaurantCardIdSave({
            // @ts-ignore
            id: item.restaurant?.id || item?.restaurantId || item?.cityHitRestaurant?.id,
          }),
        );
      }
      // trackEvent
      if (config[configName]?.trackEvents.length) {
        config[configName]?.trackEvents.forEach((event) => {
          if (event === 'try_to_add_EZB') {
            if (item[config[configName].pointsAttr] && Number(count) !== 0) {
              trackEvent(event);
              return;
            }
          }
          trackEvent(event);
        });
      }

      // goToRest
      if (config[configName]?.checks?.includes('goToRest')) {
        history.push(link);
      }

      // update cart
      const newShopingCartState =
        item.modifiers?.length && config[configName]?.checks?.includes('withModifiers')
          ? new UpdateShopingCartWithModifiersManager(data).init()
          : new UpdateShopingCartManager(data).init();

      shopingCartUpdate(newShopingCartState);
    },
    [
      city,
      shopingCart,
      shopingCartUpdate,
      history,
      profile,
      configName,
      dispatch,
      balance,
      restaurantCard,
    ],
  );

  return [updateShopingCart];
};
