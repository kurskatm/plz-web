import type { JWTprops } from './types';

const atobImplementation = (base64: string) => {
  try {
    return Buffer.from(base64, 'base64').toString('binary');
  } catch {
    return null;
  }
};

export const decodeToken = (token: string): JWTprops => {
  const decode = (tkn: string) => {
    try {
      return JSON.parse(atobImplementation(tkn));
    } catch {
      return null;
    }
  };
  return token
    .split('.')
    .map((tkn: string) => decode(tkn))
    .reduce((acc: JWTprops, curr: JWTprops) => {
      if (curr) acc = { ...acc, ...curr };
      return acc;
    }, Object.create(null));
};
