/* eslint-disable @typescript-eslint/no-explicit-any */
type TCounterProps = {
  secondsCount: number;
  secondsCountInitial?: number;
  delayMs: number;
  trigger?: boolean;
  callBack?: () => any;
};

export type TUseCounter = ({
  secondsCount,
  delayMs,
  trigger,
  callBack,
}: TCounterProps) => [
  number,
  React.Dispatch<React.SetStateAction<boolean>>,
  number,
  React.Dispatch<React.SetStateAction<number>>,
];
