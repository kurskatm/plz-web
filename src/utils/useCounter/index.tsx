/* eslint-disable @typescript-eslint/no-explicit-any */
import { useState, useEffect } from 'react';
import { useInterval } from '@lib/auth';

import { TUseCounter } from './types';

export const useCounter: TUseCounter = ({
  secondsCount,
  secondsCountInitial,
  delayMs,
  trigger = false,
  callBack = () => {},
}) => {
  const [currentTick, setCurrentTick] = useState(secondsCount);
  const [timer, startTimer] = useState(false);
  const [attemptsCount, setAttemptsCount] = useState(1);

  useEffect(() => {
    if (trigger) {
      startTimer(true);
    }
  }, [trigger]);

  useEffect(() => {
    setCurrentTick(secondsCount);
  }, [secondsCount]);

  useInterval(
    () => {
      setCurrentTick(currentTick - 1);
      if (currentTick === 0) {
        setCurrentTick(secondsCountInitial || secondsCount);
        startTimer(false);
        if (attemptsCount === 3) {
          setAttemptsCount(attemptsCount + 1);
        }
        callBack();
      }
    },
    delayMs,
    timer,
  );

  return [currentTick, startTimer, attemptsCount, setAttemptsCount];
};
