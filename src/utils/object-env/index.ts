// Modules
import keys from 'lodash/keys';
// Types
import { TReducer } from './types';

const INITIAL_OBJECT_ENV = {
  IS_CLIENT: typeof window !== 'undefined',
};

const reducer: TReducer = (accumulator, key) => {
  accumulator[key] = process.env[key];

  return accumulator;
};

export const objectEnv = keys(process.env).reduce(reducer, INITIAL_OBJECT_ENV);
