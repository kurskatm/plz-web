import { TResultCallbackFails, TResultCallbackSuccsess } from './types';

export const cloudpaymentsInit = (
  // @ts-ignore
  paymentRequest: Record<string, string>,
  onErrorHandler: TResultCallbackFails,
  onSuccessHandler: TResultCallbackSuccsess,
): void => {
  if (!paymentRequest) {
    return;
  }

  const checkData = {
    // @ts-ignore
    Items: [],
    amounts: {
      electronic: paymentRequest.total,
    },
    email: paymentRequest.userEmail,
    // @ts-ignore
    phone: null,
    taxationSystem: paymentRequest.taxationSystem,
    calculationPlace: paymentRequest.calculationPlace,
  };

  // @ts-ignore
  checkData.Items = paymentRequest?.cart?.map((item) => ({
    label: item.productName,
    price: item.productPrice,
    quantity: item.quantity,
    amount: item.productPrice * item.quantity,
    AgentSign: item.agent,
    vat: item.vat,
    method: item.method,
    object: item.object,
    measurementUnit: 'шт',
    PurveyorData: {
      // @ts-ignore
      Phone: null,
      Name: item.purveyor?.name,
      Inn: item.purveyor?.inn,
    },
  }));

  const pay = () => {
    // @ts-ignore
    const widget = new cp.CloudPayments();
    widget.pay(
      'auth',
      {
        publicId: paymentRequest.publicId,
        description: paymentRequest.description,
        amount: paymentRequest.total,
        currency: 'RUB',
        accountId: null,
        invoiceId: `${paymentRequest.orderId}`,
        email: paymentRequest.userEmail,
        skin: 'mini',
        data: {
          CloudPayments: {
            CustomerReceipt: checkData,
          },
        },
      },
      {
        // @ts-ignore
        onSuccess(options) {
          // success
          if (onSuccessHandler) {
            console.log('CLOUDPAYMENTS SUCCESS!!!', options);
            onSuccessHandler(options?.invoiceId);
          }
        },
        // @ts-ignore
        onFail(reason, options) {
          // fail
          if (onErrorHandler) {
            onErrorHandler(reason);
          }
          console.log('CLOUDPAYMENTS FAILED!!!', options, reason);
        },
      },
    );
  };

  pay();
};
