export type TResultCallbackSuccsess = (id: string) => void;
export type TResultCallbackFails = (reason: string) => void;
