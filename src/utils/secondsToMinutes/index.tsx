export const secondsToMinutes = (secondsTotal: number): string => {
  const minutes = Math.floor(secondsTotal / 60);
  const seconds = secondsTotal % 60 < 10 ? `0${secondsTotal % 60}` : secondsTotal % 60;
  return `${minutes}:${seconds}`;
};
