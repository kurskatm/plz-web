export const numberFormat = (num: number, sep: string): string =>
  num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, sep);
