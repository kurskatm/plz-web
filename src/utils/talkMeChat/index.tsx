import { Consts } from '@utils';

const { CHAT } = Consts;
const { CHAT_KEY } = process.env;

export const TalkMeChat = (): void => {
  // @ts-ignore
  window?.supportAPIMethod = CHAT.METHOD;
  const el = document.createElement('script');
  el.type = 'text/javascript';
  el.async = true;
  el.id = 'supportScript';
  el.src = `${CHAT.PATH}?h=${CHAT_KEY}`;
  // @ts-ignore
  window[CHAT.METHOD] =
    window[CHAT.METHOD] ||
    function f() {
      // @ts-ignore
      (window[CHAT.METHOD]?.q =
        window[CHAT.METHOD]?.q ||
        // eslint-disable-next-line prefer-rest-params
        []).push(arguments);
    };
  document?.head?.appendChild(el);
};
