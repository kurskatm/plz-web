export type TPhoneFormat = (number: string, isSimple?: boolean) => string;
