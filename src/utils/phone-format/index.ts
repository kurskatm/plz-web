// Utils
import { Consts } from '@utils';
// Types
import { TPhoneFormat } from './types';

const { SIMPLE_PHONE, FORMATTED_PHONE } = Consts.REGEXP;

export const phoneFormat: TPhoneFormat = (number, isSimple = false) => {
  const matches = number.match(isSimple ? SIMPLE_PHONE : FORMATTED_PHONE);

  if (!matches) {
    return number;
  }

  return isSimple
    ? `+7 ${matches[2]} ${matches[3]}-${matches[4]}-${matches[5]}`
    : `${matches[1]}${matches[2]}${matches[3]}${matches[4]}${matches[5]}`;
};
