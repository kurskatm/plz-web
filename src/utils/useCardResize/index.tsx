// Modules
import { useState, useEffect, useCallback, useMemo } from 'react';
import { max } from 'lodash';
// Types
import { TUseCardResize } from './types';

export const useCardResize: TUseCardResize = ({
  isOneCard,
  isTwoCards,
  isTheeCards,
  isFourCards,
  cardsLength,
}) => {
  const [cardBodyHeights, setCardBodyHeights] = useState<number[]>([]);
  const [rowHeights, setRowHeights] = useState<number[]>([]);

  const cardsInARow = useMemo(() => {
    if (isFourCards) return 4;
    if (isTheeCards) return 3;
    if (isTwoCards) return 2;
    return 1;
  }, [isFourCards, isTheeCards, isTwoCards]);

  const getRowsHeights = useCallback(
    (cardHeights: number[]) => {
      if (isOneCard) {
        setRowHeights(cardHeights);
      }
      if (isTwoCards) {
        const rowsNumber = Math.ceil(cardHeights.length / cardsInARow);
        const newRowsHeight = [];
        for (let k = 0; k < rowsNumber; k += 1) {
          newRowsHeight.push(max(cardHeights.slice(2 * k, 2 * (k + 1))));
        }
        setRowHeights(newRowsHeight);
      }

      if (isTheeCards) {
        const rowsNumber = Math.ceil(cardHeights.length / cardsInARow);
        const newRowsHeight = [];
        for (let k = 0; k < rowsNumber; k += 1) {
          newRowsHeight.push(max(cardHeights.slice(3 * k, 3 * (k + 1))));
        }
        setRowHeights(newRowsHeight);
      }

      if (isFourCards) {
        const rowsNumber = Math.ceil(cardHeights.length / cardsInARow);
        const newRowsHeight = [];
        for (let k = 0; k < rowsNumber; k += 1) {
          newRowsHeight.push(max(cardHeights.slice(4 * k, 4 * (k + 1))));
        }
        setRowHeights(newRowsHeight);
      }
    },
    [isOneCard, isTwoCards, isTheeCards, isFourCards, cardsInARow],
  );

  useEffect(() => {
    if (cardBodyHeights.length === cardsLength) {
      getRowsHeights(cardBodyHeights);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [cardBodyHeights, cardsLength]);

  const getCardBodyHeight = useCallback(
    (cardHeights: number[]) => (index: number) => {
      if (isTwoCards) {
        const rowIndex = Math.floor(index / 2);
        return cardHeights[rowIndex];
      }

      if (isTheeCards) {
        const rowIndex = Math.floor(index / 3);
        return cardHeights[rowIndex];
      }

      if (isFourCards) {
        const rowIndex = Math.floor(index / 4);
        return cardHeights[rowIndex];
      }

      return cardHeights[index];
    },
    [isTwoCards, isTheeCards, isFourCards],
  );

  const setCardHeight = useCallback(
    (height: number, cardIndex: number) => {
      setCardBodyHeights([...cardBodyHeights, (cardBodyHeights[cardIndex] = height)]);
    },
    [cardBodyHeights],
  );

  return {
    getCardHeight: getCardBodyHeight(rowHeights),
    setCardHeight,
  };
};
