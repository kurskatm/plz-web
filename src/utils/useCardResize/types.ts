export interface TUseResizeProps {
  isOneCard: boolean;
  isTwoCards: boolean;
  isTheeCards?: boolean;
  isFourCards?: boolean;
  cardsLength: number;
}

export interface TUseResizeCardResult {
  getCardHeight: (index: number) => number;
  setCardHeight: (height: number, index: number) => void;
}

export type TUseCardResize = (data: TUseResizeProps) => TUseResizeCardResult;
