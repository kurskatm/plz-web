/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
// @ts-nocheck
function getUnixTimeStamp() {
  return Math.round(+new Date() / 1000);
}

function createFunctionWithTimeout(callback, opt_timeout) {
  let called = false;
  function fn() {
    if (!called) {
      called = true;
      callback();
    }
  }
  setTimeout(fn, opt_timeout || 1000);
  return fn;
}

function trySendEventToAnalytics(name, timeStamp, eventCallback) {
  if (typeof window.dataLayer !== 'undefined') {
    window.dataLayer.push({
      event: 'event-to-ga',
      eventCategory: 'user',
      eventAction: name,
      eventLabel: `timestamp:${timeStamp}`,
      eventCallback,
      eventTimeout: 500,
    });
  }
  if (!process.env.IS_PRODUCTION && typeof eventCallback !== 'undefined') {
    eventCallback();
  }
}

function trySendEventToMetrika(name, timeStamp, hitCallback) {
  if (typeof window?.ym !== 'undefined') {
    if (typeof hitCallback !== 'undefined') {
      const metrikaCallback = createFunctionWithTimeout(() => {
        hitCallback();
      }, 500);
      if (typeof timeStamp === 'undefined') {
        timeStamp = getUnixTimeStamp();
      }
      window?.ym(30584747, 'reachGoal', name, { timestamp: timeStamp }, metrikaCallback);
    } else {
      window?.ym(30584747, 'reachGoal', name);
    }
  } else if (typeof hitCallback !== 'undefined') {
    hitCallback();
  }
}

export const trackEvent = (name, hitCallback) => {
  if (process.env.IS_PRODUCTION) {
    return;
  }
  const timeStamp = getUnixTimeStamp();
  if (typeof hitCallback !== 'undefined') {
    const gaCallback = createFunctionWithTimeout(() => {
      trySendEventToMetrika(name, timeStamp, hitCallback);
    }, 500);
    trySendEventToAnalytics(name, timeStamp, gaCallback);
    return;
  }
  console.log('<-----> TRACK_EVENT :', name, timeStamp);
  trySendEventToAnalytics(name, timeStamp);

  trySendEventToMetrika(name, timeStamp, hitCallback);
};
