export default {
  divs: {
    wrapper: 'qa-shopping-cart-wrapper',
    header: 'qa-shopping-cart-header',
    content: 'qa-shopping-cart-content',
    contentTotal: 'qa-shopping-cart-content-total',
    footer: 'qa-shopping-cart-footer',
  },
  values: {
    total: 'qa-shopping-cart-total',
    totalWithDiscount: 'qa-shopping-cart-total-with-discount',
    summary: 'qa-shopping-cart-summary',
    discount: 'qa-shopping-cart-discount',
    points: 'qa-shopping-cart-points',
    delivery: 'qa-shopping-cart-delivery',
  },
};
