// Locales
// ---Main
import navigation from './navigation';
import shopingCart from './shopingCart';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const selectors: any = {
  // Main
  navigation,
  // Shoping Cart
  shopingCart,
};

export default selectors;
