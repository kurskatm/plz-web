// Modules
import React, { FC, useCallback, useMemo, createContext } from 'react';
// Lib
import { flattenSelectors } from '@lib/qa/flatten-qa-selectors';
// Main types
import { TQa } from '@type/qa/props';
// Types
import { TQaSelectorsProviderProps, TQaSelectorsContext } from './types';

const QaSelectorsContext = createContext<TQaSelectorsContext>({
  qa: () => {
    console.error('qa not implemented');
    return '';
  },
});

const QaSelectorsProviderComponent: FC<TQaSelectorsProviderProps> = ({ qaSelectors, children }) => {
  const flattenedQaSelectors = useMemo(() => flattenSelectors(qaSelectors), [qaSelectors]);

  const qaSelectorGetter = useCallback<TQa>((id: string) => flattenedQaSelectors[id] || id, [
    flattenedQaSelectors,
  ]);

  const contextValues = useMemo(
    () => ({
      qa: qaSelectorGetter,
    }),
    [qaSelectorGetter],
  );

  return (
    <QaSelectorsContext.Provider value={contextValues}>{children}</QaSelectorsContext.Provider>
  );
};

const QaSelectorsConsumer = QaSelectorsContext.Consumer;
export { QaSelectorsProviderComponent as QaSelectorsProvider, QaSelectorsConsumer };
