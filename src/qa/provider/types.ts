// Types
import { TQaSelectorsValue } from '@type/qa/flatten-selectors';
import { TQa } from '@type/qa/props';

export interface TQaSelectorsContext {
  qa: TQa;
}

export interface TQaSelectorsProviderProps {
  qaSelectors: TQaSelectorsValue;
}
