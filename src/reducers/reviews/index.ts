// Modules
import produce from 'immer';
// Constants
import {
  REVIEWS_SAVE,
  REVIEWS_PENDING,
  REVIEWS_SUCCESS,
  REVIEWS_ERROR,
  REVIEWS_RESET,
} from '@constants/reviews';
import {
  PROFILE_RESET_DATA_AFTER_CITY_UPDATE,
  PROFILE_CITY_AND_ADDRESS_UPDATE,
} from '@constants/profile/city';
// Models
import { reviewsModel as initialState } from '@models/reviews';
import { uiModel } from '@models/ui';
// Utils
import { Consts } from '@utils';
// Types
import { TReviewsReducer } from './types';

const { STATUS } = Consts.REQUEST.UI;

export const reviewsReducer: TReviewsReducer = (state = initialState, { type, payload }) =>
  produce(state, (draft) => {
    switch (type) {
      case REVIEWS_SAVE: {
        draft.data = payload;

        break;
      }

      case REVIEWS_PENDING: {
        draft.ui.status = STATUS.PENDING;
        draft.ui.details = null;

        break;
      }

      case REVIEWS_SUCCESS: {
        const { status } = payload;
        draft.ui.status = STATUS.SUCCESS;
        draft.ui.details = status;

        break;
      }

      case REVIEWS_ERROR: {
        const { status } = payload;
        draft.ui.status = STATUS.ERROR;
        draft.ui.details = status;

        break;
      }

      case REVIEWS_RESET: {
        draft.data = [];
        draft.ui = uiModel;

        break;
      }

      case PROFILE_CITY_AND_ADDRESS_UPDATE:
      case PROFILE_RESET_DATA_AFTER_CITY_UPDATE: {
        draft.data = [];
        draft.ui = uiModel;
        break;
      }

      default: {
        break;
      }
    }
  });
