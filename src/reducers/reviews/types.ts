// Modules Types
import { Reducer } from 'redux';
// Main Types
import { TReviewsModel } from '@models/reviews/types';

export type TReviewsReducer = Reducer<TReviewsModel>;
