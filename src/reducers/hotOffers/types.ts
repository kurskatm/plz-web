// Modules Types
import { Reducer } from 'redux';
// Main Types
import { THotOffersModel } from '@models/hotOffers/types';

export type THotOffersReducer = Reducer<THotOffersModel>;
