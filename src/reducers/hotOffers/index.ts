// Modules
import produce from 'immer';
// Constants
import {
  HOT_OFFERS_SAVE,
  HOT_OFFERS_RESET,
  HOT_OFFERS_PENDING,
  HOT_OFFERS_SUCCESS,
  HOT_OFFERS_ERROR,
} from '@constants/hotOffers';
import {
  PROFILE_RESET_DATA_AFTER_CITY_UPDATE,
  PROFILE_CITY_AND_ADDRESS_UPDATE,
} from '@constants/profile/city';
// Model
import { hotOffersModel as initialState } from '@models/hotOffers';
import { uiModel } from '@models/ui';
// Utils
import { Consts } from '@utils';
// Types
import { THotOffersReducer } from './types';

const { STATUS } = Consts.REQUEST.UI;

export const hotOffersReducer: THotOffersReducer = (state = initialState, { type, payload }) =>
  produce(state, (draft) => {
    switch (type) {
      case HOT_OFFERS_PENDING: {
        draft.ui.status = STATUS.PENDING;
        draft.ui.details = null;
        break;
      }

      case HOT_OFFERS_SUCCESS: {
        draft.ui.status = STATUS.SUCCESS;
        draft.ui.details = payload.status;
        break;
      }

      case HOT_OFFERS_ERROR: {
        draft.ui.status = STATUS.ERROR;
        draft.ui.details = payload.status;
        break;
      }

      case HOT_OFFERS_SAVE: {
        draft.data = payload;
        break;
      }

      case HOT_OFFERS_RESET:
      case PROFILE_CITY_AND_ADDRESS_UPDATE:
      case PROFILE_RESET_DATA_AFTER_CITY_UPDATE: {
        draft.data = [];
        draft.ui = uiModel;
        break;
      }

      default: {
        break;
      }
    }
  });
