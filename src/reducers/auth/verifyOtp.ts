// Modules
import produce from 'immer';
// Constants
import {
  AUTH_OTP_SEND,
  AUTH_SAVE_OTP_DATA,
  AUTH_OTP_PENDING,
  AUTH_OTP_SUCCESS,
  AUTH_OTP_ERROR,
  AUTH_OTP_STATUS_RESET,
  UNAUTHORIZE,
  AUTH_CLEAR_RESEND_TOKEN,
  AUTH_MODAL_SHOW,
} from '@constants/auth';
// Models
import { authVerifyOtpModel } from '@models/auth/verifyOtp';
import { uiModel } from '@/models/ui';
// Utils
import { Consts } from '@utils';
// Types
import { TAuthVerifyOtpReducer } from './types';

const { STATUS } = Consts.REQUEST.UI;

export const authVerifyOtpReducer: TAuthVerifyOtpReducer = (
  state = authVerifyOtpModel,
  { type, payload },
) =>
  produce(state, (draft) => {
    switch (type) {
      case AUTH_OTP_SEND: {
        draft.data.step = 'otp';
        break;
      }
      case AUTH_SAVE_OTP_DATA: {
        draft.data.refresh_token = payload.refresh_token;
        draft.data.access_token = payload.access_token;
        break;
      }
      case AUTH_OTP_PENDING: {
        draft.ui.status = STATUS.PENDING;
        draft.ui.details = null;

        break;
      }

      case AUTH_OTP_SUCCESS: {
        const { status } = payload;
        draft.ui.status = STATUS.SUCCESS;
        draft.ui.details = status;
        break;
      }

      case AUTH_OTP_ERROR: {
        const { status } = payload;
        draft.ui.status = STATUS.ERROR;
        draft.ui.details = status;

        break;
      }

      case AUTH_OTP_STATUS_RESET: {
        draft.ui = uiModel;
        break;
      }

      case AUTH_CLEAR_RESEND_TOKEN: {
        draft.data = {};
        draft.ui = uiModel;
        break;
      }
      case UNAUTHORIZE: {
        draft.data = {};
        draft.ui = uiModel;
        break;
      }

      case AUTH_MODAL_SHOW: {
        if (!payload && draft.ui.status === STATUS.ERROR) {
          draft.ui = uiModel;
        }
        break;
      }

      default: {
        break;
      }
    }
  });
