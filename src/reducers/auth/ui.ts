// Modules
import produce from 'immer';
// State
import { uiModel } from '@models/ui';
// Constants
import { AUTH_PENDING, AUTH_SUCCESS, AUTH_ERROR } from '@constants/auth';
// Utils
import { Consts } from '@utils';
// Types
import { TAuthUiReducer } from './types';

const { STATUS } = Consts.REQUEST.UI;
export const authUiReducer: TAuthUiReducer = (state = uiModel, { type, payload }) =>
  produce(state, (draft) => {
    switch (type) {
      case AUTH_PENDING: {
        draft.status = STATUS.PENDING;
        draft.details = null;

        break;
      }

      case AUTH_SUCCESS: {
        const { status } = payload;
        draft.status = STATUS.SUCCESS;
        draft.details = status;

        break;
      }

      case AUTH_ERROR: {
        const { status } = payload;
        draft.status = STATUS.ERROR;
        draft.details = status;

        break;
      }

      default: {
        break;
      }
    }
  });
