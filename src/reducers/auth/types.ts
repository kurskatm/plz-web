// Modules Types
import { Reducer } from 'redux';
// Main Types
import { TUiModel } from '@models/ui/types';
import { TAuthVerifyPhoneModel, TAuthVerifyOtpModel, TAuthModalModel } from '@models/auth/types';

export interface TAuthStep {
  step: string;
  resendToken: string;
}
export type TAuthUiReducer = Reducer<TUiModel>;
export type TAuthDataReducer = Reducer<TAuthStep>;
export type TAuthVerifyPhoneReducer = Reducer<TAuthVerifyPhoneModel>;
export type TAuthVerifyOtpReducer = Reducer<TAuthVerifyOtpModel>;
export type TAuthModalReducer = Reducer<TAuthModalModel>;
