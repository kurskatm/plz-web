// Modules
import produce from 'immer';
// Constants
import {
  AUTH_PHONE_SEND,
  AUTH_SAVE_PHONE_DATA,
  AUTH_PENDING,
  AUTH_SUCCESS,
  AUTH_ERROR,
  AUTH_CLEAR_RESEND_TOKEN,
  UNAUTHORIZE,
  AUTH_PHONE_STATUS_RESET,
} from '@constants/auth';
// Models
import { authVerifyPhoneModel } from '@models/auth/verifyPhone';
import { uiModel } from '@/models/ui';

// Utils
import { Consts } from '@utils';
// Types
import { TAuthVerifyPhoneReducer } from './types';

const { STATUS } = Consts.REQUEST.UI;

export const authVerifyPhoneReducer: TAuthVerifyPhoneReducer = (
  state = authVerifyPhoneModel,
  { type, payload },
) =>
  produce(state, (draft) => {
    switch (type) {
      case AUTH_PHONE_SEND: {
        draft.data.step = 'phone';
        break;
      }
      case AUTH_SAVE_PHONE_DATA: {
        draft.data.resendToken = payload.resendToken;
        break;
      }
      case AUTH_CLEAR_RESEND_TOKEN: {
        draft.data.resendToken = null;
        break;
      }
      case AUTH_PENDING: {
        draft.ui.status = STATUS.PENDING;
        draft.ui.details = null;
        break;
      }
      case AUTH_SUCCESS: {
        const { status } = payload;
        draft.ui.status = STATUS.SUCCESS;
        draft.ui.details = status;
        break;
      }
      case AUTH_ERROR: {
        const { status } = payload;
        draft.ui.status = STATUS.ERROR;
        draft.ui.details = status;
        break;
      }
      case AUTH_PHONE_STATUS_RESET: {
        draft.ui = uiModel;
        break;
      }
      case UNAUTHORIZE: {
        draft.data = {};
        draft.ui = uiModel;
        break;
      }
      default: {
        break;
      }
    }
  });
