// Modules
import { combineReducers } from 'redux';
// Reducers
import { authVerifyPhoneReducer } from './verifyPhone';
import { authVerifyOtpReducer } from './verifyOtp';
import { authModalReducer } from './modal';

export const authReducer = combineReducers({
  verifyPhone: authVerifyPhoneReducer,
  verifyOtp: authVerifyOtpReducer,
  modal: authModalReducer,
});
