// Modules
import produce from 'immer';
// Constants
import { AUTH_MODAL_SHOW, AUTH_MODAL_SAVE_PHONE } from '@constants/auth';
import { PROFILE_UNAUTHORIZED } from '@constants/profile/auth';
// Utils
import { trackEvent } from '@utils/analytics';
// Types
import { TAuthModalReducer } from './types';

export const authModalReducer: TAuthModalReducer = (
  state = {
    show: false,
    phone: '',
  },
  { type, payload },
) =>
  produce(state, (draft) => {
    switch (type) {
      case AUTH_MODAL_SHOW: {
        // пока трэкинг тут
        if (payload) {
          trackEvent('popup-login');
        }
        draft.show = payload;
        break;
      }
      case AUTH_MODAL_SAVE_PHONE: {
        draft.phone = payload;
        break;
      }
      case PROFILE_UNAUTHORIZED: {
        draft.show = false;
        draft.phone = '';
        break;
      }
      default: {
        break;
      }
    }
  });
