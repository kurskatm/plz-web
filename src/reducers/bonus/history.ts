// Modules
import produce from 'immer';
// Constants
import {
  BONUS_HISTORY_SAVE,
  BONUS_HISTORY_PENDING,
  BONUS_HISTORY_SUCCESS,
  BONUS_HISTORY_ERROR,
  BONUS_RESET_DATA,
} from '@constants/bonus';
import { PROFILE_GET_INFO_RESET_UI } from '@constants/profile/info';
import { PROFILE_RESET_ALL_UI_MODELS } from '@constants/profile';
import {
  // Auth
  PROFILE_UNAUTHORIZED,
} from '@constants/profile/auth';
// Model
import { bonusHistoryModel } from '@models/bonus/history';
import { uiModel } from '@models/ui';
// Utils
import { Consts } from '@utils';
// Types
import { TBonusHistoryReducer } from './types';

const { PENDING, SUCCESS, ERROR } = Consts.REQUEST.UI.STATUS;

export const bonusHistoryReducer: TBonusHistoryReducer = (
  state = bonusHistoryModel,
  { type, payload },
) =>
  produce(state, (draft) => {
    switch (type) {
      case BONUS_HISTORY_SAVE: {
        draft.data = payload;
        break;
      }
      case BONUS_HISTORY_PENDING: {
        draft.ui.status = PENDING;
        draft.ui.details = null;
        break;
      }

      case BONUS_HISTORY_SUCCESS: {
        draft.ui.status = SUCCESS;
        draft.ui.details = payload.status;
        break;
      }

      case BONUS_HISTORY_ERROR: {
        draft.ui.status = ERROR;
        draft.ui.details = payload.status;
        break;
      }

      case BONUS_RESET_DATA: {
        draft.ui = uiModel;
        break;
      }

      case PROFILE_GET_INFO_RESET_UI: {
        draft.ui = uiModel;
        break;
      }

      case PROFILE_RESET_ALL_UI_MODELS: {
        draft.ui = uiModel;
        break;
      }

      case PROFILE_UNAUTHORIZED: {
        draft.data = [];
        draft.ui = uiModel;
        break;
      }

      default: {
        break;
      }
    }
  });
