// Modules
import produce from 'immer';
import isNil from 'lodash/isNil';
// Constants
import {
  BONUS_SAVE,
  BONUS_PENDING,
  BONUS_SUCCESS,
  BONUS_ERROR,
  BONUS_RESET_DATA,
} from '@constants/bonus';
import { PROFILE_SET_SOCIAL_ACTIVITY_SAVE_DATA } from '@constants/profile/social-activity';
import { PROFILE_GET_INFO_RESET_UI } from '@constants/profile/info';
import { PROFILE_RESET_ALL_UI_MODELS } from '@constants/profile';
import {
  // Auth
  PROFILE_UNAUTHORIZED,
} from '@constants/profile/auth';
// Model
import { bonusBalanceModel } from '@models/bonus/balance';
import { uiModel } from '@models/ui';
// Utils
import { Consts } from '@utils';
import { trackEvent } from '@utils/analytics';
// Types
import { TBonusBalanceReducer } from './types';

const { PENDING, SUCCESS, ERROR } = Consts.REQUEST.UI.STATUS;

export const bonusBalanceReducer: TBonusBalanceReducer = (
  state = bonusBalanceModel,
  { type, payload },
) =>
  produce(state, (draft) => {
    switch (type) {
      case BONUS_SAVE: {
        draft.data = payload;
        break;
      }
      case BONUS_PENDING: {
        draft.ui.status = PENDING;
        draft.ui.details = null;
        break;
      }

      case BONUS_SUCCESS: {
        draft.ui.status = SUCCESS;
        draft.ui.details = payload.status;
        break;
      }

      case BONUS_ERROR: {
        draft.ui.status = ERROR;
        draft.ui.details = payload.status;
        break;
      }

      case PROFILE_SET_SOCIAL_ACTIVITY_SAVE_DATA: {
        if (!isNil(draft.data)) {
          draft.data.earnedBySocialActivity.push(payload.type);
          if (payload.type === 0) {
            trackEvent('вступилвк');
          }
          if (payload.type === 3) {
            trackEvent('поделилсявк');
          }
          if (payload.type === 5) {
            trackEvent('поделилсяок');
          }
        }
        break;
      }

      case BONUS_RESET_DATA: {
        draft.ui = uiModel;
        break;
      }

      case PROFILE_GET_INFO_RESET_UI: {
        draft.ui = uiModel;
        break;
      }

      case PROFILE_RESET_ALL_UI_MODELS: {
        draft.ui = uiModel;
        break;
      }

      case PROFILE_UNAUTHORIZED: {
        draft.data = null;
        draft.ui = uiModel;
        break;
      }

      default: {
        break;
      }
    }
  });
