// Modules
import { combineReducers } from 'redux';
// Reducers
import { bonusBalanceReducer } from './balance';
import { bonusHistoryReducer } from './history';
import { bonusSocialActivityReducer } from './social-activity';

export const bonusReducer = combineReducers({
  balance: bonusBalanceReducer,
  history: bonusHistoryReducer,
  socialActivity: bonusSocialActivityReducer,
});
