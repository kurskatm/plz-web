// Modules
import produce from 'immer';
// Constants
import {
  PROFILE_SET_SOCIAL_ACTIVITY_PENDING,
  PROFILE_SET_SOCIAL_ACTIVITY_SUCCESS,
  PROFILE_SET_SOCIAL_ACTIVITY_ERROR,
} from '@constants/profile/social-activity';
import {
  // Auth
  PROFILE_UNAUTHORIZED,
} from '@constants/profile/auth';
// Model
import { socialActivityModel } from '@models/bonus/social-activity';
import { uiModel } from '@models/ui';
// Utils
import { Consts } from '@utils';
// Types
import { TBonusSocialActivityReducer } from './types';

const { PENDING, SUCCESS, ERROR } = Consts.REQUEST.UI.STATUS;

export const bonusSocialActivityReducer: TBonusSocialActivityReducer = (
  state = socialActivityModel,
  { type, payload },
) =>
  produce(state, (draft) => {
    switch (type) {
      case PROFILE_SET_SOCIAL_ACTIVITY_PENDING: {
        draft.ui.status = PENDING;
        draft.ui.details = null;
        break;
      }

      case PROFILE_SET_SOCIAL_ACTIVITY_SUCCESS: {
        draft.ui.status = SUCCESS;
        draft.ui.details = payload.status;
        break;
      }

      case PROFILE_SET_SOCIAL_ACTIVITY_ERROR: {
        draft.ui.status = ERROR;
        draft.ui.details = payload.status;
        break;
      }

      case PROFILE_UNAUTHORIZED: {
        draft.ui = uiModel;
        break;
      }

      default: {
        break;
      }
    }
  });
