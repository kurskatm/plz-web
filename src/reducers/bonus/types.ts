// Modules Types
import { Reducer } from 'redux';
// Main Types
import { TUiModel } from '@models/ui/types';
import { TBonusBalanceModel, TBonusHistoryModel, TSocialActivityModel } from '@models/bonus/types';

export type TBonusUiReducer = Reducer<TUiModel>;
export type TBonusBalanceReducer = Reducer<TBonusBalanceModel>;
export type TBonusHistoryReducer = Reducer<TBonusHistoryModel>;
export type TBonusSocialActivityReducer = Reducer<TSocialActivityModel>;
