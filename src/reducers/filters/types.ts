// Modules Types
import { Reducer } from 'redux';
// Main Types
import { TFiltersModel } from '@models/filters/types';

export type TFiltersReducer = Reducer<TFiltersModel>;
