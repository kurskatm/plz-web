// Modules
import produce from 'immer';
// Constants
import {
  FILTERS_SAVE,
  FILTERS_SET,
  FILTERS_SEARCH,
  FILTERS_FETCH_PENDING,
  FILTERS_FETCH_SUCCESS,
  FILTERS_FETCH_ERROR,
  // Modal
  FILTERS_SHOW_MODAL,
} from '@constants/filters';
import {
  PROFILE_RESET_DATA_AFTER_CITY_UPDATE,
  PROFILE_CITY_AND_ADDRESS_UPDATE,
} from '@constants/profile/city';
// Models
import { filtersModel } from '@models/filters';
import { uiModel } from '@models/ui';
// Utils
import { Consts } from '@utils';
import { trackEvent } from '@utils/analytics';
// Types
import { TFiltersReducer } from './types';

const { STATUS } = Consts.REQUEST.UI;

export const filtersReducer: TFiltersReducer = (state = filtersModel, { type, payload }) =>
  produce(state, (draft) => {
    switch (type) {
      case FILTERS_SAVE: {
        draft.list.data = payload;
        break;
      }

      case FILTERS_SET: {
        draft.chosen = payload;
        break;
      }

      case FILTERS_SEARCH: {
        draft.search = payload;
        break;
      }

      case FILTERS_FETCH_PENDING: {
        draft.list.ui.status = STATUS.PENDING;
        draft.list.ui.details = null;
        break;
      }

      case FILTERS_FETCH_SUCCESS: {
        draft.list.ui.status = STATUS.SUCCESS;
        draft.list.ui.details = payload.status;
        break;
      }

      case FILTERS_FETCH_ERROR: {
        draft.list.ui.status = STATUS.ERROR;
        draft.list.ui.details = payload.status;
        break;
      }

      case FILTERS_SHOW_MODAL: {
        draft.showModal = payload;
        if (payload) {
          trackEvent('showFiltersMobile');
        }

        break;
      }

      case PROFILE_CITY_AND_ADDRESS_UPDATE:
      case PROFILE_RESET_DATA_AFTER_CITY_UPDATE: {
        draft.chosen = [];
        draft.list.data = [];
        draft.list.ui = uiModel;
        draft.search = '';
        break;
      }

      default: {
        break;
      }
    }
  });
