// Modules Types
import { Reducer } from 'redux';
// Types
import { TCheckoutModalsModel } from '@models/checkout-modals/types';

export type TCheckoutModalsReducer = Reducer<TCheckoutModalsModel>;
