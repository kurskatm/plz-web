// Modules
import produce from 'immer';
// Constants
import {
  CHANGE_ADDRESS_MODAL_SHOW,
  CHANGE_ADDRESS_MODAL_HIDE_2ND_TIME,
} from '@constants/checkout-modals';
// Model
import { addressModalModel as initialState } from '@models/checkout-modals/address';
// Types
import { TCheckoutChangeAddressReducer } from './types';

export const checkoutChangeAddressReducer: TCheckoutChangeAddressReducer = (
  state = initialState,
  { type, payload },
) =>
  produce(state, (draft) => {
    switch (type) {
      case CHANGE_ADDRESS_MODAL_SHOW: {
        // A fix for showing "mismatch address" on redirection
        if (!payload.show) {
          draft.show = false;
        } else if (draft.doNotShow2ndTime) {
          draft.doNotShow2ndTime = false;
        } else {
          draft.show = true;
        }

        break;
      }

      // A fix for showing "mismatch address" on redirection
      case CHANGE_ADDRESS_MODAL_HIDE_2ND_TIME: {
        draft.doNotShow2ndTime = payload.doNotShow2ndTime;
        break;
      }

      default: {
        break;
      }
    }
  });
