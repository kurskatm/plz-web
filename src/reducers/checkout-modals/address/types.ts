// Modules Types
import { Reducer } from 'redux';
// Types
import { TModalDataModel } from '@models/checkout-modals/address/types';

export type TCheckoutChangeAddressReducer = Reducer<TModalDataModel>;
