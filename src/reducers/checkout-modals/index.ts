// Modules
import { combineReducers } from 'redux';
// Reducers
import { checkoutChangeAddressReducer } from './address';
import { checkoutChangeBasketReducer } from './basket';
import { checkoutChangePromocodeReducer } from './promocode';

export const checkoutModalsReducer = combineReducers({
  address: checkoutChangeAddressReducer,
  basket: checkoutChangeBasketReducer,
  promocode: checkoutChangePromocodeReducer,
});
