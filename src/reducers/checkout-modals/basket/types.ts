// Modules Types
import { Reducer } from 'redux';
// Types
import { TModalDataModel } from '@models/checkout-modals/basket/types';

export type TCheckoutChangeBasketReducer = Reducer<TModalDataModel>;
