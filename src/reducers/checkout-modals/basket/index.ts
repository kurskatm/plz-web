// Modules
import produce from 'immer';
// Constants
import { CHANGE_BASKET_MODAL_SHOW } from '@constants/checkout-modals';
// Model
import { basketModalModel as initialState } from '@models/checkout-modals/basket';
// Types
import { TCheckoutChangeBasketReducer } from './types';

export const checkoutChangeBasketReducer: TCheckoutChangeBasketReducer = (
  state = initialState,
  { type, payload },
) =>
  produce(state, (draft) => {
    switch (type) {
      case CHANGE_BASKET_MODAL_SHOW: {
        draft.show = payload;
        break;
      }

      default: {
        break;
      }
    }
  });
