// Modules Types
import { Reducer } from 'redux';
// Types
import { TModalDataModel } from '@models/checkout-modals/promocode/types';

export type TCheckoutChangePromocodeReducer = Reducer<TModalDataModel>;
