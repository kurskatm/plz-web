// Modules
import produce from 'immer';
// Constants
import { CHANGE_PROMOCODE_MODAL_SHOW } from '@constants/checkout-modals';
// Model
import { promocodeModalModel as initialState } from '@models/checkout-modals/promocode';
// Types
import { TCheckoutChangePromocodeReducer } from './types';

export const checkoutChangePromocodeReducer: TCheckoutChangePromocodeReducer = (
  state = initialState,
  { type, payload },
) =>
  produce(state, (draft) => {
    switch (type) {
      case CHANGE_PROMOCODE_MODAL_SHOW: {
        draft.show = payload;
        break;
      }

      default: {
        break;
      }
    }
  });
