// Modules Types
import { Reducer } from 'redux';
// Main Types
import { TFoodForPointsModel } from '@models/foodForPoints/types';

export type TFoodForPointsReducer = Reducer<TFoodForPointsModel>;
