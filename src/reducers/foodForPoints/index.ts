// Modules
import produce from 'immer';
// State
import { foodForPointsModel as initialState } from '@models/foodForPoints';
import { uiModel } from '@models/ui';
// Constants
import {
  FOOD_FOR_POINTS_PENDING,
  FOOD_FOR_POINTS_SUCCESS,
  FOOD_FOR_POINTS_ERROR,
  FOOD_FOR_POINTS_SAVE,
  FOOD_FOR_POINTS_RESET,
} from '@constants/foodForPoints';
// Utils
import { Consts } from '@utils';
// Types
import { TFoodForPointsReducer } from './types';

const { STATUS } = Consts.REQUEST.UI;
export const foodForPointsReducer: TFoodForPointsReducer = (
  state = initialState,
  { type, payload },
) =>
  produce(state, (draft) => {
    switch (type) {
      case FOOD_FOR_POINTS_PENDING: {
        draft.ui.status = STATUS.PENDING;
        draft.ui.details = null;
        break;
      }

      case FOOD_FOR_POINTS_SUCCESS: {
        draft.ui.status = STATUS.SUCCESS;
        draft.ui.details = payload.status;
        break;
      }

      case FOOD_FOR_POINTS_ERROR: {
        draft.ui.status = STATUS.ERROR;
        draft.ui.details = payload.status;
        break;
      }

      case FOOD_FOR_POINTS_SAVE: {
        const { restaurants, cityHits: products } = payload;
        draft.data = { restaurants, products };
        break;
      }

      case FOOD_FOR_POINTS_RESET: {
        draft.ui = uiModel;
        draft.data = null;
        break;
      }

      default: {
        break;
      }
    }
  });
