// Modules
import produce from 'immer';
// Model
import { partnersCountersModel } from '@models/partners/counters';
// Constants
import {
  PARTNERS_COUNTERS_ERROR,
  PARTNERS_COUNTERS_PENDING,
  PARTNERS_COUNTERS_SUCCESS,
  PARTNERS_COUNTERS_SAVE,
} from '@constants/partners';
// Utils
import { Consts } from '@utils';
// Types
import { TPartnerMembersReducer } from './types';

const { PENDING, SUCCESS, ERROR } = Consts.REQUEST.UI.STATUS;

export const partnersCountersReducer: TPartnerMembersReducer = (
  state = partnersCountersModel,
  { type, payload = null },
) =>
  produce(state, (draft) => {
    switch (type) {
      case PARTNERS_COUNTERS_SAVE: {
        draft.data = payload;
        break;
      }

      case PARTNERS_COUNTERS_PENDING: {
        draft.ui.status = PENDING;
        draft.ui.details = null;
        break;
      }

      case PARTNERS_COUNTERS_SUCCESS: {
        const { status = null } = payload;
        draft.ui.status = SUCCESS;
        draft.ui.details = status;
        break;
      }

      case PARTNERS_COUNTERS_ERROR: {
        const { status = null } = payload;
        draft.ui.status = ERROR;
        draft.ui.details = status;
        break;
      }

      default: {
        break;
      }
    }
  });
