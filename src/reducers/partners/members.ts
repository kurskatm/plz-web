// Modules
import produce from 'immer';
// Model
import { partnersMembersModel } from '@models/partners/members';
// Constants
import {
  PARTNERS_BECOME_PARTNER_ADD,
  PARTNERS_BECOME_PARTNER_IDLE,
  PARTNERS_BECOME_PARTNER_PENDING,
  PARTNERS_BECOME_PARTNER_SUCCESS,
  PARTNERS_BECOME_PARTNER_ERROR,
} from '@constants/partners';
// Utils
import { Consts } from '@utils';
import { trackEvent } from '@utils/analytics';
// Types
import { TPartnerMembersReducer } from './types';

const { PENDING, SUCCESS, ERROR, IDLE } = Consts.REQUEST.UI.STATUS;

export const parnersMembersReducer: TPartnerMembersReducer = (
  state = partnersMembersModel,
  { type, payload = null },
) =>
  produce(state, (draft) => {
    switch (type) {
      case PARTNERS_BECOME_PARTNER_ADD: {
        draft.data = payload;
        break;
      }

      case PARTNERS_BECOME_PARTNER_IDLE: {
        draft.ui.status = IDLE;
        draft.ui.details = null;
        break;
      }

      case PARTNERS_BECOME_PARTNER_PENDING: {
        draft.ui.status = PENDING;
        draft.ui.details = null;
        break;
      }

      case PARTNERS_BECOME_PARTNER_SUCCESS: {
        const { status = null } = payload;
        draft.ui.status = SUCCESS;
        draft.ui.details = status;
        trackEvent('партнёр');
        break;
      }

      case PARTNERS_BECOME_PARTNER_ERROR: {
        const { status = null } = payload;
        draft.ui.status = ERROR;
        draft.ui.details = status;
        break;
      }

      default: {
        break;
      }
    }
  });
