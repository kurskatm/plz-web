// Modules
import { combineReducers } from 'redux';
// Reducers
import { partnersCountersReducer } from './counters';
import { parnersMembersReducer } from './members';

export const partnersReducer = combineReducers({
  counters: partnersCountersReducer,
  members: parnersMembersReducer,
});
