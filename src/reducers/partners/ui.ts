// Modules
import produce from 'immer';
// State
import { uiModel } from '@models/ui';
// Types
import { TUiModel } from '@models/ui/types';

export const partnersUiReducer = (state = uiModel, { type }: { type: string }): TUiModel =>
  produce(state, () => {
    // eslint-disable-next-line no-empty
    switch (type) {
    }
  });
