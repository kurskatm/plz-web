// Modules Types
import { Reducer } from 'redux';
// Main Types
import { TPartnersMembersModel, TPartnersCountersModel } from '@models/partners/types';

export type TPartnerMembersReducer = Reducer<TPartnersMembersModel>;
export type TPartnerCountersReducer = Reducer<TPartnersCountersModel>;
