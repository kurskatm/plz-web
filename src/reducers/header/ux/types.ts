// Modules Types
import { Reducer } from 'redux';
// Main Types
import { THeaderUxModel } from '@models/header/ux/types';

export type THeaderUxReducer = Reducer<THeaderUxModel>;
