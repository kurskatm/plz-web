// Modules
import produce from 'immer';
// Model
import { headerUxModel as initialState } from '@models/header/ux';
// Constants
import { HEADER_UX_SET_HEIGHT } from '@constants/header';
// Types
import { THeaderUxReducer } from './types';

export const headerUxReducer: THeaderUxReducer = (state = initialState, { type, payload }) =>
  produce(state, (draft) => {
    switch (type) {
      case HEADER_UX_SET_HEIGHT: {
        draft.height = payload;
        break;
      }

      default: {
        break;
      }
    }
  });
