// Modules
import { combineReducers } from 'redux';
// Reducers
import { headerUxReducer } from './ux';

export const headerReducer = combineReducers({
  ux: headerUxReducer,
});
