// Modules
import produce from 'immer';
// Constants
import {
  CITIES_FETCH_PENDING,
  CITIES_FETCH_SUCCESS,
  CITIES_FETCH_ERROR,
  CITIES_FETCH_SAVE_DATA,
  CITIES_MODAL_SHOW,
} from '@constants/cities';
// Models
import { citiesModel } from '@models/cities';
// Utils
import { Consts } from '@utils';
// Types
import { TCitiesReducer } from './types';

const { STATUS } = Consts.REQUEST.UI;

export const citiesReducer: TCitiesReducer = (state = citiesModel, { type, payload }) =>
  produce(state, (draft) => {
    switch (type) {
      case CITIES_FETCH_SAVE_DATA: {
        draft.list.data = payload;
        break;
      }

      case CITIES_FETCH_PENDING: {
        draft.list.ui.status = STATUS.PENDING;
        draft.list.ui.details = null;
        break;
      }

      case CITIES_FETCH_SUCCESS: {
        draft.list.ui.status = STATUS.SUCCESS;
        draft.list.ui.details = payload.status;
        break;
      }

      case CITIES_FETCH_ERROR: {
        draft.list.ui.status = STATUS.ERROR;
        draft.list.ui.details = payload.status;
        break;
      }

      case CITIES_MODAL_SHOW: {
        draft.modal.show = payload;
        break;
      }

      default: {
        break;
      }
    }
  });
