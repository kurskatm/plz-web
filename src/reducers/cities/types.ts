// Modules Types
import { Reducer } from 'redux';
// Main Types
import { TCitiesModel } from '@models/cities/types';

export type TCitiesReducer = Reducer<TCitiesModel>;
