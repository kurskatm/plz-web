// Modules
import produce from 'immer';
// Model
import { contactsDataModel } from '@models/contacts';
// Constants
import { CONTACTS_SAVE } from '@constants/contacts';
// Types
import { TContactsDataReducer } from './type';

export const contactsDataReducer: TContactsDataReducer = (
  state = contactsDataModel,
  { type, payload },
) =>
  produce(state, (draft) => {
    switch (type) {
      case CONTACTS_SAVE: {
        return { ...payload };
      }

      default: {
        return draft;
      }
    }
  });
