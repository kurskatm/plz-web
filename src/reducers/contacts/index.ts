// Modules
import { combineReducers } from 'redux';
// Reducers
import { contactsDataReducer } from './data';
import { contactsUiReducer } from './ui';

export const contactsReducer = combineReducers({
  data: contactsDataReducer,
  ui: contactsUiReducer,
});
