// Modules
import produce from 'immer';
// Constants
import { CONTACTS_PENDING, CONTACTS_SUCCESS, CONTACTS_ERROR } from '@constants/contacts';
// State
import { uiModel } from '@models/ui';
// Utils
import { Consts } from '@utils';
// Types
import { TUiReducer } from '@type/reducers';

const { STATUS } = Consts.REQUEST.UI;

export const contactsUiReducer: TUiReducer = (state = uiModel, { type, payload }) =>
  produce(state, (draft) => {
    switch (type) {
      case CONTACTS_PENDING: {
        draft.status = STATUS.PENDING;
        draft.details = null;
        break;
      }

      case CONTACTS_SUCCESS: {
        draft.status = STATUS.SUCCESS;
        draft.details = payload.status;
        break;
      }

      case CONTACTS_ERROR: {
        draft.status = STATUS.SUCCESS;
        draft.details = payload.status;
        break;
      }

      default: {
        break;
      }
    }
  });
