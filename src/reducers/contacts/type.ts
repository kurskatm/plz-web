// Modules Types
import { Reducer } from 'redux';
// Types
import { TContacts } from '@type/contacts';

export type TContactsDataReducer = Reducer<TContacts>;
