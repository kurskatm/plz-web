// Modules
import produce from 'immer';
// Constants
import { RESTAURANTS_SAVE } from '@constants/restaurants';
// Types
import { TRestaurantsDataReducer } from './types';

export const restaurantsDataReducer: TRestaurantsDataReducer = (state = [], { type, payload }) =>
  produce(state, (draft) => {
    switch (type) {
      case RESTAURANTS_SAVE: {
        return [...payload];
      }

      default: {
        return draft;
      }
    }
  });
