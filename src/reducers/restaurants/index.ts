// Modules
import { combineReducers } from 'redux';
// Reducers
import { restaurantsDataReducer } from './data';
import { restaurantsUiReducer } from './ui';

export const restaurantsReducer = combineReducers({
  data: restaurantsDataReducer,
  ui: restaurantsUiReducer,
});
