// Modules
import produce from 'immer';
// State
import { uiModel } from '@models/ui';
// Constants
import {
  RESTAURANTS_PENDING,
  RESTAURANTS_SUCCESS,
  RESTAURANTS_ERROR,
} from '@constants/restaurants';
// Utils
import { Consts } from '@utils';
// Types
import { TRestaurantsUiReducer } from './types';

const { STATUS } = Consts.REQUEST.UI;
export const restaurantsUiReducer: TRestaurantsUiReducer = (state = uiModel, { type, payload }) =>
  produce(state, (draft) => {
    switch (type) {
      case RESTAURANTS_PENDING: {
        draft.status = STATUS.PENDING;
        draft.details = null;
        break;
      }

      case RESTAURANTS_SUCCESS: {
        draft.status = STATUS.SUCCESS;
        draft.details = payload.status;
        break;
      }

      case RESTAURANTS_ERROR: {
        draft.status = STATUS.ERROR;
        draft.details = payload.status;
        break;
      }

      default: {
        break;
      }
    }
  });
