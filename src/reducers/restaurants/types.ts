// Modules Types
import { Reducer } from 'redux';
// Main Types
import { TUiModel } from '@models/ui/types';
import { TRestaurantCard } from '@type/restaurants/restaurant-card';

export type TRestaurantsUiReducer = Reducer<TUiModel>;
export type TRestaurantsDataReducer = Reducer<TRestaurantCard[]>;
