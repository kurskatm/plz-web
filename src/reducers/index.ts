// Modules
import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
// Utils
import { Consts } from '@utils';
// Reducers
import { citiesReducer } from './cities';
import { contactsReducer } from './contacts';
import { cuisinesReducer } from './cuisines';
import { filtersReducer } from './filters';
import { headerReducer } from './header';
import { profileReducer } from './profile';
import { restaurantReducer } from './restaurant';
import { restaurantsReducer } from './restaurants';
import { restaurantsAndHitsReducer } from './restaurants-and-hits';
import { partnersReducer } from './partners';
import { authReducer } from './auth';
import { hotOffersReducer } from './hotOffers';
import { hitsReducer } from './hits';
import { shopingCartReducer } from './shoping-cart';
import { supportReducer } from './support';
import { bonusReducer } from './bonus';
import { reviewsReducer } from './reviews';
import { checkoutModalsReducer } from './checkout-modals';
import { promocodeReducer } from './promocode';
import { notificationsReducer } from './notifications';
import { oldSiteBannerReducer } from './oldSiteBanner';
import { foodForPointsReducer } from './foodForPoints';
// Types
import { TRootReducer } from './types';

const { IS_CLIENT } = Consts.ENV;

export const rootReducer: TRootReducer = (history) =>
  combineReducers({
    auth: authReducer,
    bonus: bonusReducer,
    contacts: contactsReducer,
    cities: citiesReducer,
    checkoutModals: checkoutModalsReducer,
    cuisines: cuisinesReducer,
    filters: filtersReducer,
    header: headerReducer,
    hits: hitsReducer,
    hotOffers: hotOffersReducer,
    notifications: notificationsReducer,
    oldSiteBanner: oldSiteBannerReducer,
    partners: partnersReducer,
    profile: profileReducer,
    promocode: promocodeReducer,
    restaurant: restaurantReducer,
    restaurants: restaurantsReducer,
    restaurantsAndHits: restaurantsAndHitsReducer,
    reviews: reviewsReducer,
    router: IS_CLIENT ? connectRouter(history) : null,
    shopingCart: shopingCartReducer,
    support: supportReducer,
    foodForPoints: foodForPointsReducer,
  });
