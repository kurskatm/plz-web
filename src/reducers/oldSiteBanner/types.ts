// Modules Types
import { Reducer } from 'redux';
// Main Types
import { TOldSiteBannerModel } from '@models/oldSiteBanner/types';

export type TOldSiteBannerReducer = Reducer<TOldSiteBannerModel>;
