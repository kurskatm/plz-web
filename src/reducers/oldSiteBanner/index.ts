// Modules
import produce from 'immer';
// Constants
import { SHOW_FULL_OLD_SITE_BANNER } from '@constants/oldSiteBanner';
// Models
import { oldSiteBannerModel as InitialState } from '@models/oldSiteBanner';
// Types
import { TOldSiteBannerReducer } from './types';

export const oldSiteBannerReducer: TOldSiteBannerReducer = (
  state = InitialState,
  { type, payload },
) =>
  produce(state, (draft) => {
    switch (type) {
      case SHOW_FULL_OLD_SITE_BANNER: {
        draft.isFull = payload;
        break;
      }

      default: {
        break;
      }
    }
  });
