// Modules
import { Reducer } from 'redux';
// Types
import { TNotificationsModel } from '@models/notifications/types';

export type TNotificationsReducer = Reducer<TNotificationsModel>;
