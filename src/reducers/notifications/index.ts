// Modules
import produce from 'immer';
import { v4 as uuid } from 'uuid';
// Constants
import { PUSH_NOTIFICATION, REMOVE_NOTIFICATION } from '@constants/notifications';
import { PROFILE_UNAUTHORIZED } from '@constants/profile/auth';
// Inital state
import { notificationsModel } from '@models/notifications';
// Types
import { TNotificationsReducer } from './types';

const DEFAULT_DURATION_MSEC = 5000;
export const notificationsReducer: TNotificationsReducer = (
  state = notificationsModel,
  { type, payload },
) =>
  produce(state, (draft) => {
    switch (type) {
      case PUSH_NOTIFICATION: {
        if (!state.notifications.map((item) => item.content).includes(payload.content)) {
          draft.notifications.push({
            id: uuid(),
            type: payload.type,
            content: payload.content,
            duration: payload.duration ?? DEFAULT_DURATION_MSEC,
          });
        } else {
          const index = state.notifications
            .map((item) => item.content)
            .findIndex((content) => content === payload.content);
          if (index !== -1) {
            draft.notifications[index] = {
              ...draft.notifications[index],
              duration: payload.duration ?? DEFAULT_DURATION_MSEC,
            };
          }
        }
        break;
      }

      case REMOVE_NOTIFICATION: {
        draft.notifications = draft.notifications.filter((e) => e.id !== payload.id);
        break;
      }

      case PROFILE_UNAUTHORIZED: {
        if (draft.notifications.length) {
          draft.notifications = [];
        }
        break;
      }

      default: {
        break;
      }
    }
  });
