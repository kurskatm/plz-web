// Modules
import produce from 'immer';
import isNil from 'lodash/isNil';
// Constants
import {
  RESTAURANTS_AND_HITS_SAVE,
  RESTAURANTS_AND_HITS_PENDING,
  RESTAURANTS_AND_HITS_SUCCESS,
  RESTAURANTS_AND_HITS_ERROR,
} from '@constants/restaurants-and-hits';
import {
  PROFILE_SET_FAVOURITE_PRODUCTS_SAVE_DATA,
  PROFILE_DELETE_FAVOURITE_PRODUCTS_SAVE_DATA,
} from '@constants/profile/favourite/products';
import {
  PROFILE_SET_FAVOURITE_REST_SAVE_DATA,
  PROFILE_DELETE_FAVOURITE_REST_SAVE_DATA,
} from '@constants/profile/favourite/rests';
import {
  PROFILE_RESET_DATA_AFTER_CITY_UPDATE,
  PROFILE_CITY_AND_ADDRESS_UPDATE,
} from '@constants/profile/city';
// Model
import { restaurantsAndHitsModel as initialState } from '@models/restaurants-and-hits';
import { uiModel } from '@models/ui';
// Utils
import { Consts } from '@utils';
// Types
import { TRestaurantsAndHitsReducer } from './types';

const { STATUS } = Consts.REQUEST.UI;

export const restaurantsAndHitsReducer: TRestaurantsAndHitsReducer = (
  state = initialState,
  { type, payload },
) =>
  produce(state, (draft) => {
    switch (type) {
      case RESTAURANTS_AND_HITS_SAVE: {
        const availableInBonusPoints = false;
        const hits = (payload.products || payload.cityHits)
          // @ts-ignore
          .map((item) => ({ ...item, availableInBonusPoints }));
        const data = {
          restaurants: payload.restaurants,
          products: hits,
        };

        draft.data = data;
        break;
      }

      case RESTAURANTS_AND_HITS_PENDING: {
        draft.ui.status = STATUS.PENDING;
        draft.ui.details = null;
        break;
      }

      case RESTAURANTS_AND_HITS_SUCCESS: {
        draft.ui.status = STATUS.SUCCESS;
        draft.ui.details = payload.status;
        break;
      }

      case RESTAURANTS_AND_HITS_ERROR: {
        draft.ui.status = STATUS.ERROR;
        draft.ui.details = payload.status;
        break;
      }

      case PROFILE_SET_FAVOURITE_PRODUCTS_SAVE_DATA: {
        if (!isNil(draft.data)) {
          const index = draft.data.products.findIndex(({ id }) => id === payload.productId);

          if (index !== -1) {
            draft.data.products[index].isFavorite = !draft.data.products[index].isFavorite;
          }
        }
        break;
      }

      case PROFILE_DELETE_FAVOURITE_PRODUCTS_SAVE_DATA: {
        if (!isNil(draft.data)) {
          const index = draft.data.products.findIndex(({ id }) => id === payload.productId);

          if (index !== -1) {
            draft.data.products[index].isFavorite = !draft.data.products[index].isFavorite;
          }
        }
        break;
      }

      case PROFILE_SET_FAVOURITE_REST_SAVE_DATA: {
        if (!isNil(draft.data)) {
          const index = draft.data.restaurants.findIndex(({ id }) => id === payload.restId);

          if (index !== -1) {
            draft.data.restaurants[index].isFavorite = !draft.data.restaurants[index].isFavorite;
          }
        }
        break;
      }

      case PROFILE_DELETE_FAVOURITE_REST_SAVE_DATA: {
        if (!isNil(draft.data)) {
          const index = draft.data.restaurants.findIndex(({ id }) => id === payload.restId);

          if (index !== -1) {
            draft.data.restaurants[index].isFavorite = !draft.data.restaurants[index].isFavorite;
          }
        }
        break;
      }

      case PROFILE_CITY_AND_ADDRESS_UPDATE:
      case PROFILE_RESET_DATA_AFTER_CITY_UPDATE: {
        draft.data = null;
        draft.ui = uiModel;
        break;
      }

      default: {
        break;
      }
    }
  });
