// Modules Types
import { Reducer } from 'redux';
// Types
import { TRestaurantsAndHitsModel } from '@models/restaurants-and-hits/types';

export type TRestaurantsAndHitsReducer = Reducer<TRestaurantsAndHitsModel>;
