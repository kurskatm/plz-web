// Modules Types
import { Reducer } from 'redux';
// Types
import { TRestaurantMainModel } from '@models/restaurant/main/types';

export type TRestaurantMainReducer = Reducer<TRestaurantMainModel>;
