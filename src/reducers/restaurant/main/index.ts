// Modules
import produce from 'immer';
import isNil from 'lodash/isNil';
// Constants
import { RESTAURANT_CARD_MAIN_RESET } from '@constants/restaurant/main';
import {
  RESTAURANT_CARD_ID_SAVE,
  RESTAURANT_CARD_ID_RESET,
  RESTAURANT_FETCH_CARD_ID_PENDING,
  RESTAURANT_FETCH_CARD_ID_SUCCESS,
  RESTAURANT_FETCH_CARD_ID_ERROR,
} from '@constants/restaurant/main/id';
import {
  RESTAURANT_CARD_SAVE,
  RESTAURANT_CARD_RESET,
  RESTAURANT_FETCH_CARD_PENDING,
  RESTAURANT_FETCH_CARD_SUCCESS,
  RESTAURANT_FETCH_CARD_ERROR,
} from '@constants/restaurant/main/card';
import { RESTAURANT_STORES_SAVE } from '@/constants/restaurant/stores';
import { PROFILE_CITY_AND_ADDRESS_UPDATE } from '@constants/profile/city';
// Model
import { restaurantMainModel as initialState } from '@models/restaurant/main';
import { uiModel } from '@models/ui';
// Utils
import { Consts } from '@utils';
// Types
import { TRestaurantMainReducer } from './type';

const { STATUS } = Consts.REQUEST.UI;

export const restaurantMainReducer: TRestaurantMainReducer = (
  state = initialState,
  { type, payload },
) =>
  produce(state, (draft) => {
    switch (type) {
      // Main
      case RESTAURANT_CARD_MAIN_RESET: {
        // draft.id.data = null;
        draft.id.ui = uiModel;

        draft.info.data = null;
        draft.info.ui = uiModel;

        break;
      }

      // Fetch id
      case RESTAURANT_CARD_ID_SAVE: {
        draft.id.data = payload;
        break;
      }

      case RESTAURANT_CARD_ID_RESET: {
        draft.id.data = null;
        draft.id.ui = uiModel;
        break;
      }

      case RESTAURANT_STORES_SAVE: {
        draft.stores = payload;
        break;
      }

      case RESTAURANT_FETCH_CARD_ID_PENDING: {
        draft.id.ui.status = STATUS.PENDING;
        draft.id.ui.details = null;
        break;
      }

      case RESTAURANT_FETCH_CARD_ID_SUCCESS: {
        draft.id.ui.status = STATUS.SUCCESS;
        draft.id.ui.details = payload.status;
        break;
      }

      case RESTAURANT_FETCH_CARD_ID_ERROR: {
        draft.id.ui.status = STATUS.ERROR;
        draft.id.ui.details = payload.status;
        break;
      }

      // Fetch card
      case RESTAURANT_CARD_SAVE: {
        draft.info.data = payload;
        break;
      }

      case RESTAURANT_CARD_RESET: {
        draft.info.data = null;
        draft.info.ui = uiModel;
        break;
      }

      case RESTAURANT_FETCH_CARD_PENDING: {
        draft.info.ui.status = STATUS.PENDING;
        draft.info.ui.details = null;
        break;
      }

      case RESTAURANT_FETCH_CARD_SUCCESS: {
        draft.info.ui.status = STATUS.SUCCESS;
        draft.info.ui.details = payload.status;
        break;
      }

      case RESTAURANT_FETCH_CARD_ERROR: {
        draft.info.ui.status = STATUS.ERROR;
        draft.info.ui.details = payload.status;
        break;
      }

      case PROFILE_CITY_AND_ADDRESS_UPDATE: {
        if (!isNil(draft.id.data)) {
          draft.id.data = null;
          draft.id.ui = uiModel;
        }
        if (!isNil(draft.info.data)) {
          draft.info.data = null;
          draft.info.ui = uiModel;
        }
        if (draft.stores.length) {
          draft.stores = [];
        }
        break;
      }

      default: {
        break;
      }
    }
  });
