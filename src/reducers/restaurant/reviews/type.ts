// Modules Types
import { Reducer } from 'redux';
// Types
import { TRestaurantReviewsModel } from '@models/restaurant/reviews/types';

export type TRestaurantReviewsReducer = Reducer<TRestaurantReviewsModel>;
