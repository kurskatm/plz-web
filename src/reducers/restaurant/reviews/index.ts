// Modules
import produce from 'immer';
// Constants
import { RESTAURANT_CARD_REVIEWS_MAIN_RESET } from '@constants/restaurant/reviews';
import {
  RESTAURANT_CARD_REVIEWS_SAVE,
  RESTAURANT_CARD_REVIEWS_RESET,
  RESTAURANT_FETCH_CARD_REVIEWS_PENDING,
  RESTAURANT_FETCH_CARD_REVIEWS_SUCCESS,
  RESTAURANT_FETCH_CARD_REVIEWS_ERROR,
} from '@constants/restaurant/reviews/current';
import { PROFILE_CITY_AND_ADDRESS_UPDATE } from '@constants/profile/city';
// Model
import {
  restaurantReviewsModel as initialState,
  restaurantReviewsCurrentPaginationModel,
} from '@models/restaurant/reviews';
import { uiModel } from '@models/ui';
// Utils
import { Consts } from '@utils';
// Types
import { TRestaurantReviewsReducer } from './type';

const { STATUS } = Consts.REQUEST.UI;

export const restaurantReviewsReducer: TRestaurantReviewsReducer = (
  state = initialState,
  { type, payload },
) =>
  produce(state, (draft) => {
    switch (type) {
      // Main
      case RESTAURANT_CARD_REVIEWS_MAIN_RESET: {
        draft.current.data = [];
        draft.current.ui = uiModel;
        draft.current.pagination = restaurantReviewsCurrentPaginationModel;

        break;
      }

      // Current
      case RESTAURANT_CARD_REVIEWS_SAVE: {
        draft.current.data = draft.current.data.concat(payload);
        break;
      }

      case RESTAURANT_CARD_REVIEWS_RESET: {
        draft.current.data = [];
        draft.current.ui = uiModel;
        draft.current.pagination = restaurantReviewsCurrentPaginationModel;

        break;
      }

      case RESTAURANT_FETCH_CARD_REVIEWS_PENDING: {
        draft.current.pagination.hasMore = false;
        draft.current.ui.status = STATUS.PENDING;
        draft.current.ui.details = null;
        break;
      }

      case RESTAURANT_FETCH_CARD_REVIEWS_SUCCESS: {
        const page = draft.current.pagination.page + 1;
        const quantity = draft.current.pagination.limit * page;

        draft.current.pagination.page = page;
        draft.current.pagination.hasMore = draft.current.data.length === quantity;
        draft.current.ui.status = STATUS.SUCCESS;
        draft.current.ui.details = payload.status;
        break;
      }

      case RESTAURANT_FETCH_CARD_REVIEWS_ERROR: {
        draft.current.pagination.hasMore = false;
        draft.current.ui.status = STATUS.ERROR;
        draft.current.ui.details = payload.status;
        break;
      }

      case PROFILE_CITY_AND_ADDRESS_UPDATE: {
        if (draft.current.data.length) {
          draft.current.data = [];
          draft.current.ui = uiModel;
          draft.current.pagination = restaurantReviewsCurrentPaginationModel;
        }
        break;
      }

      default: {
        break;
      }
    }
  });
