// Modules
import produce from 'immer';
// Constants
import { RESTAURANT_CARD_MENU_RESET } from '@constants/restaurant/menu';
import {
  RESTAURANT_SEARCH_PRODUCT,
  RESTAURANT_SEARCH_PRODUCT_PENDING,
  RESTAURANT_SEARCH_PRODUCT_SUCCESS,
  RESTAURANT_SEARCH_PRODUCT_ERROR,
  RESTAURANT_SEARCH_PRODUCT_SAVE,
  RESTAURANT_SEARCH_PRODUCT_RESET,
} from '@constants/restaurant/search-products';
import { PROFILE_CITY_AND_ADDRESS_UPDATE } from '@constants/profile/city';
// Model
import { resstaurantSearchProductModel as initialState } from '@models/restaurant/search-products';
import { uiModel } from '@models/ui';
// Utils
import { Consts } from '@utils';
// Types
import { TRestaurantSearchProductsReducer } from './type';

const { STATUS } = Consts.REQUEST.UI;

export const restaurantSearchProductsReducer: TRestaurantSearchProductsReducer = (
  state = initialState,
  { type, payload },
) =>
  produce(state, (draft) => {
    switch (type) {
      case RESTAURANT_CARD_MENU_RESET: {
        draft.products.data = [];
        draft.products.ui = uiModel;
        draft.search = '';

        break;
      }

      case RESTAURANT_SEARCH_PRODUCT: {
        draft.search = payload.query;

        break;
      }

      // Products
      case RESTAURANT_SEARCH_PRODUCT_SAVE: {
        draft.products.data = payload.data;
        break;
      }

      case RESTAURANT_SEARCH_PRODUCT_RESET: {
        draft.search = '';
        draft.products.data = [];
        draft.products.ui = uiModel;
        break;
      }

      case RESTAURANT_SEARCH_PRODUCT_PENDING: {
        draft.products.ui.status = STATUS.PENDING;
        draft.products.ui.details = null;
        break;
      }

      case RESTAURANT_SEARCH_PRODUCT_SUCCESS: {
        draft.products.ui.status = STATUS.SUCCESS;
        draft.products.ui.details = payload.status;
        break;
      }

      case RESTAURANT_SEARCH_PRODUCT_ERROR: {
        draft.products.ui.status = STATUS.ERROR;
        draft.products.ui.details = payload.status;
        break;
      }

      case PROFILE_CITY_AND_ADDRESS_UPDATE: {
        if (draft.products.data.length) {
          draft.products.data = [];
          draft.products.ui = uiModel;
          draft.search = '';
        }
        break;
      }

      default: {
        break;
      }
    }
  });
