// Modules Types
import { Reducer } from 'redux';
// Types
import { TRestaurantSearchProductsModel } from '@models/restaurant/search-products/types';

export type TRestaurantSearchProductsReducer = Reducer<TRestaurantSearchProductsModel>;
