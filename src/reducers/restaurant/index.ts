// Modules
import { combineReducers } from 'redux';
// Reducers
import { restaurantMainReducer } from './main';
import { restaurantMenuReducer } from './menu';
import { restaurantReviewsReducer } from './reviews';
import { restaurantInfoReducer } from './info';
import { restaurantModalReducer } from './modal';
import { restaurantSearchProductsReducer } from './search-products';

export const restaurantReducer = combineReducers({
  main: restaurantMainReducer,
  menu: restaurantMenuReducer,
  reviews: restaurantReviewsReducer,
  info: restaurantInfoReducer,
  modal: restaurantModalReducer,
  searchProducts: restaurantSearchProductsReducer,
});
