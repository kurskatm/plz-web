// Modules
import produce from 'immer';
import isNil from 'lodash/isNil';
import omit from 'lodash/omit';
// Constants
import { RESTAURANT_CARD_MENU_RESET } from '@constants/restaurant/menu';
import {
  RESTAURANT_CARD_CATEGORIES_SAVE,
  RESTAURANT_CARD_CATEGORIES_RESET,
  RESTAURANT_FETCH_CARD_CATEGORIES_PENDING,
  RESTAURANT_FETCH_CARD_CATEGORIES_SUCCESS,
  RESTAURANT_FETCH_CARD_CATEGORIES_ERROR,
} from '@constants/restaurant/menu/categories';
import {
  RESTAURANT_CARD_PRODUCTS_SAVE,
  RESTAURANT_CARD_PRODUCTS_RESET,
  RESTAURANT_FETCH_CARD_PRODUCTS_PENDING,
  RESTAURANT_FETCH_CARD_PRODUCTS_SUCCESS,
  RESTAURANT_FETCH_CARD_PRODUCTS_ERROR,
} from '@constants/restaurant/menu/products';
import {
  RESTAURANT_CARD_PROMO_SAVE,
  RESTAURANT_CARD_PROMO_RESET,
  RESTAURANT_FETCH_CARD_PROMO_PENDING,
  RESTAURANT_FETCH_CARD_PROMO_SUCCESS,
  RESTAURANT_FETCH_CARD_PROMO_ERROR,
} from '@constants/restaurant/menu/promo';
import { RESTAURANT_CARD_MAIN_RESET } from '@constants/restaurant/main';
import {
  PROFILE_DELETE_FAVOURITE_PRODUCTS_SAVE_DATA,
  PROFILE_SET_FAVOURITE_PRODUCTS_SAVE_DATA,
} from '@constants/profile/favourite/products';
import { PROFILE_CITY_AND_ADDRESS_UPDATE } from '@constants/profile/city';
// Model
import { restaurantMenuModel as initialState } from '@models/restaurant/menu';
import { uiModel } from '@models/ui';
// Utils
import { Consts } from '@utils';
// Types
import { TRestaurantMenuReducer } from './type';

const { STATUS } = Consts.REQUEST.UI;

export const restaurantMenuReducer: TRestaurantMenuReducer = (
  state = initialState,
  { type, payload },
) =>
  produce(state, (draft) => {
    switch (type) {
      // Main
      case RESTAURANT_CARD_MENU_RESET: {
        draft.categories.data = [];
        draft.categories.ui = uiModel;

        draft.products.data = [];
        draft.products.ui = uiModel;

        draft.promo.data = [];
        draft.promo.ui = uiModel;

        break;
      }

      // Categories
      case RESTAURANT_CARD_CATEGORIES_SAVE: {
        draft.categories.data = payload;
        break;
      }

      case RESTAURANT_CARD_CATEGORIES_RESET: {
        draft.categories.data = [];
        draft.categories.ui = uiModel;
        break;
      }

      case RESTAURANT_FETCH_CARD_CATEGORIES_PENDING: {
        draft.categories.ui.status = STATUS.PENDING;
        draft.categories.ui.details = null;
        break;
      }

      case RESTAURANT_FETCH_CARD_CATEGORIES_SUCCESS: {
        draft.categories.ui.status = STATUS.SUCCESS;
        draft.categories.ui.details = payload.status;
        break;
      }

      case RESTAURANT_FETCH_CARD_CATEGORIES_ERROR: {
        draft.categories.ui.status = STATUS.ERROR;
        draft.categories.ui.details = payload.status;
        break;
      }

      // Products
      case RESTAURANT_CARD_PRODUCTS_SAVE: {
        draft.products.data = payload.data;
        // @ts-ignore
        draft.categories.data = payload?.data?.map((item) => omit(item, ['products']));
        break;
      }

      case RESTAURANT_CARD_PRODUCTS_RESET: {
        draft.products.data = [];
        draft.products.ui = uiModel;
        break;
      }

      case RESTAURANT_FETCH_CARD_PRODUCTS_PENDING: {
        draft.products.ui.status = STATUS.PENDING;
        draft.products.ui.details = null;
        break;
      }

      case RESTAURANT_FETCH_CARD_PRODUCTS_SUCCESS: {
        draft.products.ui.status = STATUS.SUCCESS;
        draft.products.ui.details = payload.status;
        draft.categories.ui.status = STATUS.SUCCESS;
        draft.categories.ui.details = payload.status;
        break;
      }

      case RESTAURANT_FETCH_CARD_PRODUCTS_ERROR: {
        draft.products.ui.status = STATUS.ERROR;
        draft.products.ui.details = payload.status;
        break;
      }

      // Promo
      case RESTAURANT_CARD_PROMO_SAVE: {
        draft.promo.data = payload;
        break;
      }

      case RESTAURANT_CARD_PROMO_RESET: {
        draft.promo.data = [];
        draft.promo.ui = uiModel;
        break;
      }

      case RESTAURANT_FETCH_CARD_PROMO_PENDING: {
        draft.promo.ui.status = STATUS.PENDING;
        draft.promo.ui.details = null;
        break;
      }

      case RESTAURANT_FETCH_CARD_PROMO_SUCCESS: {
        draft.promo.ui.status = STATUS.SUCCESS;
        draft.promo.ui.details = payload.status;
        break;
      }

      case RESTAURANT_FETCH_CARD_PROMO_ERROR: {
        draft.promo.ui.status = STATUS.ERROR;
        draft.promo.ui.details = payload.status;
        break;
      }

      // Main RESET
      case RESTAURANT_CARD_MAIN_RESET: {
        draft.products.data = [];
        draft.products.ui = uiModel;
        draft.categories.data = [];
        draft.categories.ui = uiModel;
        draft.promo.data = [];
        draft.promo.ui = uiModel;
        break;
      }

      // favourite products
      case PROFILE_SET_FAVOURITE_PRODUCTS_SAVE_DATA: {
        if (!isNil(draft.products.data)) {
          draft.products.data.forEach((category) => {
            if (category.products.map((product) => product.id).includes(payload.productId)) {
              const productIndex = category.products.findIndex(
                ({ id }) => id === payload.productId,
              );
              category.products[productIndex].isFavorite = true;
            }
          });
        }

        break;
      }

      case PROFILE_DELETE_FAVOURITE_PRODUCTS_SAVE_DATA: {
        if (!isNil(draft.products.data)) {
          draft.products.data.forEach((category) => {
            if (category.products.map((product) => product.id).includes(payload.productId)) {
              const productIndex = category.products.findIndex(
                ({ id }) => id === payload.productId,
              );
              category.products[productIndex].isFavorite = false;
            }
          });
        }

        break;
      }

      case PROFILE_CITY_AND_ADDRESS_UPDATE: {
        if (draft.categories.data.length) {
          draft.categories.data = [];
          draft.categories.ui = uiModel;

          draft.products.data = [];
          draft.products.ui = uiModel;

          draft.promo.data = [];
          draft.promo.ui = uiModel;
        }
        break;
      }

      default: {
        break;
      }
    }
  });
