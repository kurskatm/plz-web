// Modules Types
import { Reducer } from 'redux';
// Types
import { TRestaurantMenuModel } from '@models/restaurant/menu/types';

export type TRestaurantMenuReducer = Reducer<TRestaurantMenuModel>;
