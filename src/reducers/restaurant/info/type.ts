// Modules Types
import { Reducer } from 'redux';
// Types
import { TRestaurantInfoModel } from '@models/restaurant/info/types';

export type TRestaurantInfoReducer = Reducer<TRestaurantInfoModel>;
