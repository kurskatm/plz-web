// Modules
import produce from 'immer';
import isNil from 'lodash/isNil';
// Constants
import { RESTAURANT_CARD_INFO_MAIN_RESET } from '@constants/restaurant/info';
import {
  RESTAURANT_CARD_INFO_SAVE,
  RESTAURANT_CARD_INFO_RESET,
  RESTAURANT_FETCH_CARD_INFO_PENDING,
  RESTAURANT_FETCH_CARD_INFO_SUCCESS,
  RESTAURANT_FETCH_CARD_INFO_ERROR,
} from '@constants/restaurant/info/info';
import {
  RESTAURANT_CARD_LEGAL_SAVE,
  RESTAURANT_CARD_LEGAL_RESET,
  RESTAURANT_FETCH_CARD_LEGAL_PENDING,
  RESTAURANT_FETCH_CARD_LEGAL_SUCCESS,
  RESTAURANT_FETCH_CARD_LEGAL_ERROR,
} from '@constants/restaurant/info/legal';
import {
  RESTAURANT_ADDRESSES_SUGGEST_FETCH_PENDING,
  RESTAURANT_ADDRESSES_SUGGEST_FETCH_SUCCESS,
  RESTAURANT_ADDRESSES_SUGGEST_FETCH_ERROR,
  RESTAURANT_ADDRESSES_SUGGEST_SAVE,
  RESTAURANT_ADDRESSES_SUGGEST_RESET,
} from '@constants/restaurant/info/address-suggest';
import {
  RESTAURANT_GEOCODE_SUGGEST_FETCH_PENDING,
  RESTAURANT_GEOCODE_SUGGEST_FETCH_SUCCESS,
  RESTAURANT_GEOCODE_SUGGEST_FETCH_ERROR,
  RESTAURANT_GEOCODE_SUGGEST_SAVE,
  RESTAURANT_GEOCODE_SUGGEST_RESET,
} from '@constants/restaurant/info/geocode-suggest';
import {
  RESTAURANT_DELIVERY_AREAS_FETCH_PENDING,
  RESTAURANT_DELIVERY_AREAS_FETCH_SUCCESS,
  RESTAURANT_DELIVERY_AREAS_FETCH_ERROR,
  RESTAURANT_DELIVERY_AREAS_SAVE,
  RESTAURANT_DELIVERY_AREAS_RESET,
} from '@constants/restaurant/info/delivery-areas';
import { PROFILE_CITY_AND_ADDRESS_UPDATE } from '@constants/profile/city';
// Model
import { restaurantInfoModel as initialState } from '@models/restaurant/info';
import { uiModel } from '@models/ui';
// Utils
import { Consts } from '@utils';
// Types
import { TRestaurantInfoReducer } from './type';

const { STATUS } = Consts.REQUEST.UI;

export const restaurantInfoReducer: TRestaurantInfoReducer = (
  state = initialState,
  { type, payload },
) =>
  produce(state, (draft) => {
    switch (type) {
      // Main
      case RESTAURANT_CARD_INFO_MAIN_RESET: {
        draft.info.data = null;
        draft.info.ui = uiModel;

        draft.legal.data = null;
        draft.legal.ui = uiModel;

        draft.deliveryAreas.address.data = null;
        draft.deliveryAreas.address.ui = uiModel;

        draft.deliveryAreas.geocode.data = null;
        draft.deliveryAreas.geocode.ui = uiModel;

        draft.deliveryAreas.deliveryAreas.data = [];
        draft.deliveryAreas.deliveryAreas.ui = uiModel;

        break;
      }

      // Info
      case RESTAURANT_CARD_INFO_SAVE: {
        draft.info.data = payload;
        break;
      }

      case RESTAURANT_CARD_INFO_RESET: {
        draft.info.data = null;
        draft.info.ui = uiModel;
        break;
      }

      case RESTAURANT_FETCH_CARD_INFO_PENDING: {
        draft.info.ui.status = STATUS.PENDING;
        draft.info.ui.details = null;
        break;
      }

      case RESTAURANT_FETCH_CARD_INFO_SUCCESS: {
        draft.info.ui.status = STATUS.SUCCESS;
        draft.info.ui.details = payload.status;
        break;
      }

      case RESTAURANT_FETCH_CARD_INFO_ERROR: {
        draft.info.ui.status = STATUS.ERROR;
        draft.info.ui.details = payload.status;
        break;
      }

      // Legal
      case RESTAURANT_CARD_LEGAL_SAVE: {
        draft.legal.data = payload;
        break;
      }

      case RESTAURANT_CARD_LEGAL_RESET: {
        draft.legal.data = null;
        draft.legal.ui = uiModel;
        break;
      }

      case RESTAURANT_FETCH_CARD_LEGAL_PENDING: {
        draft.legal.ui.status = STATUS.PENDING;
        draft.legal.ui.details = null;
        break;
      }

      case RESTAURANT_FETCH_CARD_LEGAL_SUCCESS: {
        draft.legal.ui.status = STATUS.SUCCESS;
        draft.legal.ui.details = payload.status;
        break;
      }

      case RESTAURANT_FETCH_CARD_LEGAL_ERROR: {
        draft.legal.ui.status = STATUS.ERROR;
        draft.legal.ui.details = payload.status;
        break;
      }

      // address
      case RESTAURANT_ADDRESSES_SUGGEST_FETCH_PENDING: {
        draft.deliveryAreas.address.ui.status = STATUS.PENDING;
        draft.deliveryAreas.address.ui.details = null;
        break;
      }

      case RESTAURANT_ADDRESSES_SUGGEST_FETCH_SUCCESS: {
        draft.deliveryAreas.address.ui.status = STATUS.SUCCESS;
        draft.deliveryAreas.address.ui.details = payload.status;
        break;
      }

      case RESTAURANT_ADDRESSES_SUGGEST_FETCH_ERROR: {
        draft.deliveryAreas.address.ui.status = STATUS.ERROR;
        draft.deliveryAreas.address.ui.details = payload.status;
        break;
      }

      case RESTAURANT_ADDRESSES_SUGGEST_SAVE: {
        draft.deliveryAreas.address.data = payload.data;
        break;
      }

      case RESTAURANT_ADDRESSES_SUGGEST_RESET: {
        draft.deliveryAreas.address.data = null;
        draft.deliveryAreas.address.ui = uiModel;
        break;
      }

      // geocode
      case RESTAURANT_GEOCODE_SUGGEST_FETCH_PENDING: {
        draft.deliveryAreas.geocode.ui.status = STATUS.PENDING;
        draft.deliveryAreas.geocode.ui.details = null;
        break;
      }

      case RESTAURANT_GEOCODE_SUGGEST_FETCH_SUCCESS: {
        draft.deliveryAreas.geocode.ui.status = STATUS.SUCCESS;
        draft.deliveryAreas.geocode.ui.details = payload.status;
        break;
      }

      case RESTAURANT_GEOCODE_SUGGEST_FETCH_ERROR: {
        draft.deliveryAreas.geocode.ui.status = STATUS.ERROR;
        draft.deliveryAreas.geocode.ui.details = payload.status;
        break;
      }

      case RESTAURANT_GEOCODE_SUGGEST_SAVE: {
        draft.deliveryAreas.geocode.data = payload.data;
        break;
      }

      case RESTAURANT_GEOCODE_SUGGEST_RESET: {
        draft.deliveryAreas.geocode.data = null;
        draft.deliveryAreas.geocode.ui = uiModel;
        break;
      }

      // delivery areas
      case RESTAURANT_DELIVERY_AREAS_FETCH_PENDING: {
        draft.deliveryAreas.deliveryAreas.ui.status = STATUS.PENDING;
        draft.deliveryAreas.deliveryAreas.ui.details = null;
        break;
      }

      case RESTAURANT_DELIVERY_AREAS_FETCH_SUCCESS: {
        draft.deliveryAreas.deliveryAreas.ui.status = STATUS.SUCCESS;
        draft.deliveryAreas.deliveryAreas.ui.details = payload.status;
        break;
      }

      case RESTAURANT_DELIVERY_AREAS_FETCH_ERROR: {
        draft.deliveryAreas.deliveryAreas.ui.status = STATUS.ERROR;
        draft.deliveryAreas.deliveryAreas.ui.details = payload.status;
        break;
      }

      case RESTAURANT_DELIVERY_AREAS_SAVE: {
        draft.deliveryAreas.deliveryAreas.data = payload.data;
        break;
      }

      case RESTAURANT_DELIVERY_AREAS_RESET: {
        draft.deliveryAreas.deliveryAreas.data = null;
        draft.deliveryAreas.deliveryAreas.ui = uiModel;
        break;
      }

      case PROFILE_CITY_AND_ADDRESS_UPDATE: {
        if (!isNil(draft.info.data)) {
          draft.info.data = null;
          draft.info.ui = uiModel;
        }
        if (!isNil(draft.legal.data)) {
          draft.legal.data = null;
          draft.legal.ui = uiModel;
        }
        if (draft.deliveryAreas.deliveryAreas.data.length) {
          draft.deliveryAreas.address.data = null;
          draft.deliveryAreas.address.ui = uiModel;

          draft.deliveryAreas.geocode.data = null;
          draft.deliveryAreas.geocode.ui = uiModel;

          draft.deliveryAreas.deliveryAreas.data = [];
          draft.deliveryAreas.deliveryAreas.ui = uiModel;
        }
        break;
      }

      default: {
        break;
      }
    }
  });
