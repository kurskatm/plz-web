// Modules
import produce from 'immer';
// Constants
import { RESTAURANT_CARD_INFO_MAIN_RESET } from '@constants/restaurant/info';
import {
  RESTAURANT_ADDRESSES_SUGGEST_FETCH_PENDING,
  RESTAURANT_ADDRESSES_SUGGEST_FETCH_SUCCESS,
  RESTAURANT_ADDRESSES_SUGGEST_FETCH_ERROR,
  RESTAURANT_ADDRESSES_SUGGEST_SAVE,
  RESTAURANT_ADDRESSES_SUGGEST_RESET,
} from '@constants/restaurant/info/address-suggest';
import {
  RESTAURANT_GEOCODE_SUGGEST_FETCH_PENDING,
  RESTAURANT_GEOCODE_SUGGEST_FETCH_SUCCESS,
  RESTAURANT_GEOCODE_SUGGEST_FETCH_ERROR,
  RESTAURANT_GEOCODE_SUGGEST_SAVE,
  RESTAURANT_GEOCODE_SUGGEST_RESET,
} from '@constants/restaurant/info/geocode-suggest';
import {
  RESTAURANT_DELIVERY_AREAS_FETCH_PENDING,
  RESTAURANT_DELIVERY_AREAS_FETCH_SUCCESS,
  RESTAURANT_DELIVERY_AREAS_FETCH_ERROR,
  RESTAURANT_DELIVERY_AREAS_SAVE,
  RESTAURANT_DELIVERY_AREAS_RESET,
} from '@constants/restaurant/info/delivery-areas';
import { PROFILE_CITY_AND_ADDRESS_UPDATE } from '@constants/profile/city';
// Model
import { deliveryAreasModel as initialState } from '@models/restaurant/info/delivery-areas';
import { uiModel } from '@models/ui';
// Utils
import { Consts } from '@utils';
// Types
import { TDeliveryAreasReducer } from './types';

const { STATUS } = Consts.REQUEST.UI;

export const restaurantDeliveryAreasReducer: TDeliveryAreasReducer = (
  state = initialState,
  { type, payload },
) =>
  produce(state, (draft) => {
    switch (type) {
      case RESTAURANT_CARD_INFO_MAIN_RESET: {
        draft.address.data = null;
        draft.address.ui = uiModel;

        draft.geocode.data = null;
        draft.geocode.ui = uiModel;

        draft.deliveryAreas.data = [];
        draft.deliveryAreas.ui = uiModel;
        break;
      }

      // address
      case RESTAURANT_ADDRESSES_SUGGEST_FETCH_PENDING: {
        draft.address.ui.status = STATUS.PENDING;
        draft.address.ui.details = null;
        break;
      }

      case RESTAURANT_ADDRESSES_SUGGEST_FETCH_SUCCESS: {
        draft.address.ui.status = STATUS.SUCCESS;
        draft.address.ui.details = payload.status;
        break;
      }

      case RESTAURANT_ADDRESSES_SUGGEST_FETCH_ERROR: {
        draft.address.ui.status = STATUS.ERROR;
        draft.address.ui.details = payload.status;
        break;
      }

      case RESTAURANT_ADDRESSES_SUGGEST_SAVE: {
        draft.address.data = payload.data;
        break;
      }

      case RESTAURANT_ADDRESSES_SUGGEST_RESET: {
        draft.address.data = null;
        draft.address.ui = uiModel;
        break;
      }

      // geocode
      case RESTAURANT_GEOCODE_SUGGEST_FETCH_PENDING: {
        draft.geocode.ui.status = STATUS.PENDING;
        draft.geocode.ui.details = null;
        break;
      }

      case RESTAURANT_GEOCODE_SUGGEST_FETCH_SUCCESS: {
        draft.geocode.ui.status = STATUS.SUCCESS;
        draft.geocode.ui.details = payload.status;
        break;
      }

      case RESTAURANT_GEOCODE_SUGGEST_FETCH_ERROR: {
        draft.geocode.ui.status = STATUS.ERROR;
        draft.geocode.ui.details = payload.status;
        break;
      }

      case RESTAURANT_GEOCODE_SUGGEST_SAVE: {
        draft.geocode.data = payload.data;
        break;
      }

      case RESTAURANT_GEOCODE_SUGGEST_RESET: {
        draft.geocode.data = null;
        draft.geocode.ui = uiModel;
        break;
      }

      // delivery areas
      case RESTAURANT_DELIVERY_AREAS_FETCH_PENDING: {
        draft.deliveryAreas.ui.status = STATUS.PENDING;
        draft.deliveryAreas.ui.details = null;
        break;
      }

      case RESTAURANT_DELIVERY_AREAS_FETCH_SUCCESS: {
        draft.deliveryAreas.ui.status = STATUS.SUCCESS;
        draft.deliveryAreas.ui.details = payload.status;
        break;
      }

      case RESTAURANT_DELIVERY_AREAS_FETCH_ERROR: {
        draft.deliveryAreas.ui.status = STATUS.ERROR;
        draft.deliveryAreas.ui.details = payload.status;
        break;
      }

      case RESTAURANT_DELIVERY_AREAS_SAVE: {
        draft.deliveryAreas.data = payload.data;
        break;
      }

      case RESTAURANT_DELIVERY_AREAS_RESET: {
        draft.deliveryAreas.data = null;
        draft.deliveryAreas.ui = uiModel;
        break;
      }

      case PROFILE_CITY_AND_ADDRESS_UPDATE: {
        if (draft.deliveryAreas.data.length) {
          draft.address.data = null;
          draft.address.ui = uiModel;

          draft.geocode.data = null;
          draft.geocode.ui = uiModel;

          draft.deliveryAreas.data = [];
          draft.deliveryAreas.ui = uiModel;
        }
        break;
      }

      default: {
        break;
      }
    }
  });
