// Modules Types
import { Reducer } from 'redux';
// Types
import { TDeliveryAreasModel } from '@models/restaurant/info/delivery-areas/types';

export type TDeliveryAreasReducer = Reducer<TDeliveryAreasModel>;
