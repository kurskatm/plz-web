// Modules Types
import { Reducer } from 'redux';
// Types
import { TRestaurantModel } from '@models/restaurant/types';

export type TRestaurantReducer = Reducer<TRestaurantModel>;
