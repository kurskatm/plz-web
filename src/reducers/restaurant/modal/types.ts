// Modules Types
import { Reducer } from 'redux';
// Types
import { TRestaurantModalModel } from '@models/restaurant/modal/types';

export type TRestaurantModalReducer = Reducer<TRestaurantModalModel>;
