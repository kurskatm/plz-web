// Modules
import produce from 'immer';
import isNil from 'lodash/isNil';
// Constants
import { CLOSE_RESTAURANT_MODAL_SHOW } from '@constants/restaurant/restaurantCloseModal';
import {
  RESTAURANT_SHOW_MODALS,
  RESTAURANT_SHOW_SHOPPING_CARD,
  RESTAURANT_SHOW_MODIFIERS_CARD,
  RESTAURANT_SHOW_PROMO_CARD,
} from '@constants/restaurant/modals';
import { PROFILE_CITY_AND_ADDRESS_UPDATE } from '@constants/profile/city';
// Model
import { restaurantModalModel as initialState } from '@models/restaurant/modal';
// Utils
import { trackEvent } from '@utils/analytics';
// Types
import { TRestaurantModalReducer } from './types';

export const restaurantModalReducer: TRestaurantModalReducer = (
  state = initialState,
  { type, payload },
) =>
  produce(state, (draft) => {
    switch (type) {
      case CLOSE_RESTAURANT_MODAL_SHOW: {
        draft.closeRestaurantModal.showModal = payload;
        break;
      }

      case RESTAURANT_SHOW_MODALS: {
        draft.otherModals.showModal = payload.showModal;
        draft.otherModals.modalType = payload.modalType;
        if (!isNil(payload.bonusPointsRest)) {
          draft.otherModals.pointsRest = payload.bonusPointsRest;
        }
        if (!isNil(payload.dataToUpdateCart)) {
          draft.otherModals.dataToUpdateCart = payload.dataToUpdateCart;
        }
        if (!isNil(payload.link)) {
          draft.otherModals.link = payload.link;
        }

        if (!payload.showModal) {
          draft.otherModals.dataToUpdateCart = null;
          draft.otherModals.pointsRest = 0;
          draft.otherModals.link = null;
        }
        break;
      }

      case RESTAURANT_SHOW_SHOPPING_CARD: {
        draft.shoppingCardModal.showModal = payload;
        if (payload) {
          trackEvent('cart');
        }
        break;
      }

      case RESTAURANT_SHOW_MODIFIERS_CARD: {
        draft.modifiersCardModel.showModal = payload.showModal;
        draft.modifiersCardModel.productId = payload.productId;
        if (!isNil(payload.inBonusPoint)) {
          draft.modifiersCardModel.inBonusPoint = payload.inBonusPoint;
        }
        if (payload.showModal) {
          trackEvent('open-product-card');
        }
        break;
      }

      case RESTAURANT_SHOW_PROMO_CARD: {
        draft.promoCardModal.showModal = payload.showModal;
        draft.promoCardModal.promoId = payload.promoId;
        break;
      }

      case PROFILE_CITY_AND_ADDRESS_UPDATE: {
        if (draft.otherModals.showModal) {
          draft.otherModals.showModal = false;
          draft.otherModals.modalType = '';
        }
        break;
      }

      default: {
        break;
      }
    }
  });
