// Modules
import produce from 'immer';
import isNil from 'lodash/isNil';
import isEqual from 'lodash/isEqual';
// Constants
import {
  // Main
  SHOPING_CART_RESET,
  // Change
  UPDATE_SHOPING_CART,
  UPDATE_SHOPING_CART_CUTLERY,
  // Fetch
  SHOPING_CART_FETCH_SAVE,
  SHOPING_CART_FETCH_PENDING,
  SHOPING_CART_FETCH_SUCCESS,
  SHOPING_CART_FETCH_ERROR,
  SHOPING_CART_STORES_FETCH_SUCCESS,
  // Save
  SHOPING_CART_SAVE_PENDING,
  SHOPING_CART_SAVE_SUCCESS,
  SHOPING_CART_SAVE_ERROR,
  SHOPING_CART_SAVE_RESET_UI,
} from '@constants/shoping-cart';
import {
  CHECKOUT_SEND_ERROR,
  CHECKOUT_SEND_PENDING,
  CHECKOUT_SEND_SUCCESS,
  CHECKOUT_ADD_ORDER_ID,
} from '@constants/checkout';
import {
  PROFILE_CITY_AND_ADDRESS_UPDATE,
  PROFILE_RESET_DATA_AFTER_CITY_UPDATE,
} from '@constants/profile/city';
// Model
import { shopingCartModel as initialState } from '@models/shoping-cart';
// Utils
import { Consts } from '@utils';
//
import { trackEvent } from '@utils/analytics';
// Types
import { RESTAURANT_CARD_SAVE } from '@/constants/restaurant/main/card';
import { TShopingCartReducer } from './types';

const { STATUS } = Consts.REQUEST.UI;
export const shopingCartReducer: TShopingCartReducer = (state = initialState, { type, payload }) =>
  produce(state, (draft) => {
    switch (type) {
      // Main
      case PROFILE_RESET_DATA_AFTER_CITY_UPDATE:
      case PROFILE_CITY_AND_ADDRESS_UPDATE:
      case SHOPING_CART_RESET: {
        return { ...initialState };
      }
      // Change
      case UPDATE_SHOPING_CART: {
        return {
          ...payload,
        };
      }

      case UPDATE_SHOPING_CART_CUTLERY: {
        draft.options.cutlery = payload;
        return draft;
      }

      // Fetch
      case SHOPING_CART_STORES_FETCH_SUCCESS: {
        draft.restaurant.stores = payload;

        return draft;
      }

      case SHOPING_CART_FETCH_SAVE: {
        draft.id = payload.id;
        draft.restaurant = payload.restaurant
          ? {
              ...payload.restaurant,
              logo: payload.logoUrl,
            }
          : null;

        draft.products = payload.products;
        draft.options = payload.options || {
          cutlery: payload.cutleryCount,
          averageDeliveryTimeMinutes: payload.restaurant?.averageDeliveryTimeMinutes,
          deliveryCost: payload.paymentDetails?.deliveryCost || 0,
          minimalOrderCost: payload.restaurant?.minimalOrderCost || 0,
          discount: payload.paymentDetails?.discount || 0,
          bonusReward: payload.paymentDetails?.bonusReward || 0,
          total: payload.paymentDetails?.total || 0,
          minOrderCostForFreeDelivery: payload.suggestiveDeliveryCondition?.orderCost || null,
          isOpen: payload.restaurant?.isOpen || null,
          openingDate: payload.restaurant?.openingDate || null,
          paymentTypes: payload.restaurant?.paymentTypes || null,
          selfPickupStores: payload.restaurant?.selfPickupStores || [],
        };

        return draft;
      }

      case SHOPING_CART_FETCH_PENDING: {
        draft.fetch.ui.status = STATUS.PENDING;
        draft.fetch.ui.details = null;
        return draft;
      }

      case SHOPING_CART_FETCH_SUCCESS: {
        draft.fetch.ui.status = STATUS.SUCCESS;
        draft.fetch.ui.details = payload.status;
        return draft;
      }

      case SHOPING_CART_FETCH_ERROR: {
        draft.fetch.ui.status = STATUS.ERROR;
        draft.fetch.ui.details = payload.status;
        return draft;
      }

      // Save
      case SHOPING_CART_SAVE_PENDING: {
        draft.save.ui.status = STATUS.PENDING;
        draft.save.ui.details = null;
        draft.save.details.errorMessage = '';
        return draft;
      }

      case SHOPING_CART_SAVE_SUCCESS: {
        draft.save.ui.status = STATUS.SUCCESS;
        draft.save.ui.details = payload.status;
        draft.save.details.errorMessage = '';

        const { data } = payload;
        draft.options = {
          cutlery: data?.cutleryCount,
          averageDeliveryTimeMinutes: data?.restaurant?.averageDeliveryTimeMinutes || 0,
          deliveryCost: data?.paymentDetails?.deliveryCost || 0,
          minimalOrderCost: data?.restaurant?.minimalOrderCost || 0,
          discount: data?.paymentDetails?.discount || 0,
          bonusReward: data?.paymentDetails?.bonusReward || 0,
          total: data?.paymentDetails?.total || 0,
          minOrderCostForFreeDelivery: data?.suggestiveDeliveryCondition?.orderCost || null,
          isOpen: data?.restaurant?.isOpen || null,
          openingDate: data?.restaurant?.openingDate || null,
          paymentTypes: data?.restaurant?.paymentTypes || draft?.options?.paymentTypes || null,
          selfPickupStores: data?.restaurant?.selfPickupStores || [],
        };

        const payloadBonusProduct = payload.data?.products.find(
          ({ inBonusPoint }: { inBonusPoint: boolean }) => inBonusPoint,
        );
        if (
          (isNil(draft.bonusProductId) && !isNil(payloadBonusProduct)) ||
          (!isNil(draft.bonusProductId) &&
            !isNil(payloadBonusProduct) &&
            !isEqual(draft.bonusProductId, payloadBonusProduct.id))
        ) {
          draft.bonusProductId = payloadBonusProduct.id;
          trackEvent('add_EZB');
        }
        if (!isNil(draft.bonusProductId) && isNil(payloadBonusProduct)) {
          draft.bonusProductId = null;
        }

        return draft;
      }

      case SHOPING_CART_SAVE_ERROR: {
        draft.save.ui.status = STATUS.ERROR;
        draft.save.ui.details = payload.status;
        draft.save.details = payload.details;
        return draft;
      }

      case SHOPING_CART_SAVE_RESET_UI: {
        draft.save.ui.status = STATUS.IDLE;
        draft.save.ui.details = null;
        draft.save.details.errorMessage = '';
        return draft;
      }

      // CHECKOUT

      case CHECKOUT_ADD_ORDER_ID: {
        draft.checkout.id = payload;
        return draft;
      }

      case CHECKOUT_SEND_PENDING: {
        draft.checkout.error = null;
        draft.checkout.ui.status = STATUS.PENDING;
        draft.checkout.ui.details = null;
        return draft;
      }

      case CHECKOUT_SEND_SUCCESS: {
        if (!isNil(payload?.validationResult?.errors?.[0])) {
          draft.checkout.error = payload.validationResult.errors[0];
        }
        if (payload?.paymentResult?.type === 'error') {
          draft.checkout.error = {
            type: payload?.paymentResult?.type,
            message: payload?.paymentResult?.error,
          };
        }
        draft.checkout.id = payload.id;
        draft.checkout.paymentResult = payload?.paymentResult;
        draft.checkout.ui.status = STATUS.SUCCESS;
        draft.checkout.ui.details = payload.status;
        return draft;
      }

      case CHECKOUT_SEND_ERROR: {
        draft.checkout.ui.status = STATUS.ERROR;
        draft.checkout.ui.details = payload.status;
        return draft;
      }

      case RESTAURANT_CARD_SAVE: {
        draft.options = {
          ...draft.options,
          // @ts-ignore
          paymentTypes: payload?.paymentTypes,
        };
        return draft;
      }

      default: {
        return draft;
      }
    }
  });
