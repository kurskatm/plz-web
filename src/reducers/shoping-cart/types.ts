// Modules Types
import { Reducer } from 'redux';
// Types
import { TShopingCartModel } from '@models/shoping-cart/types';

export type TShopingCartReducer = Reducer<TShopingCartModel>;
