// Modules
import { combineReducers } from 'redux';
// Reducers
import { hitsDataReducer } from './data';
import { hitsUiReducer } from './ui';

export const hitsReducer = combineReducers({
  data: hitsDataReducer,
  ui: hitsUiReducer,
});
