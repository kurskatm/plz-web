// Modules Types
import { Reducer } from 'redux';
// Main Types
import { TUiModel } from '@models/ui/types';
import { THitProps } from '@models/products/types';

export type THitsUiReducer = Reducer<TUiModel>;
export type THitsDataReducer = Reducer<THitProps[]>;
