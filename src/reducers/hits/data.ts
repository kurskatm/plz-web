// Modules
import produce from 'immer';
// Constants
import { HITS_SAVE } from '@constants/hits';
// Types
import { THitsDataReducer } from './types';

export const hitsDataReducer: THitsDataReducer = (state = [], { type, payload }) =>
  produce(state, (draft) => {
    switch (type) {
      case HITS_SAVE: {
        return [...payload];
      }
      default: {
        return draft || null;
      }
    }
  });
