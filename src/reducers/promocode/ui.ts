// Modules
import produce from 'immer';
// State
import { uiModel } from '@models/ui';
// Constants
import { HITS_PENDING, HITS_SUCCESS, HITS_ERROR } from '@constants/hits';
// Utils
import { Consts } from '@utils';
// Types
import { TPromocodeUiReducer } from './types';

const { STATUS } = Consts.REQUEST.UI;
export const promocodeUiReducer: TPromocodeUiReducer = (state = uiModel, { type, payload }) =>
  produce(state, (draft) => {
    switch (type) {
      case HITS_PENDING: {
        draft.status = STATUS.PENDING;
        draft.details = null;
        break;
      }

      case HITS_SUCCESS: {
        draft.status = STATUS.SUCCESS;
        draft.details = payload.status;
        break;
      }

      case HITS_ERROR: {
        draft.status = STATUS.ERROR;
        draft.details = payload.status;
        break;
      }

      default: {
        break;
      }
    }
  });
