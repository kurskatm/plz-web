// Modules
import produce from 'immer';
// State
import { uiModel } from '@models/ui';
// Constants
import {
  PROMOCODE_PENDING,
  PROMOCODE_SUCCESS,
  PROMOCODE_ERROR,
  PROMOCODE_RESET,
} from '@constants/promocode';
// Utils
import { Consts } from '@utils';
// Types
import { TPromocodeValidUiReducer } from './types';

const { STATUS } = Consts.REQUEST.UI;
export const promocodeValidUiReducer: TPromocodeValidUiReducer = (
  state = uiModel,
  { type, payload },
) =>
  produce(state, (draft) => {
    switch (type) {
      case PROMOCODE_PENDING: {
        draft.status = STATUS.PENDING;
        draft.details = null;
        break;
      }

      case PROMOCODE_SUCCESS: {
        draft.status = STATUS.SUCCESS;
        draft.details = payload.status;
        break;
      }

      case PROMOCODE_ERROR: {
        draft.status = STATUS.ERROR;
        draft.details = payload.status;
        break;
      }

      case PROMOCODE_RESET: {
        draft.status = uiModel.status;
        draft.details = uiModel.details;
        break;
      }

      default: {
        break;
      }
    }
  });
