// Modules
import { combineReducers } from 'redux';
// Reducers
import { promocodeDataReducer } from './data';
import { promocodeUiReducer } from './ui';
import { promocodeValidUiReducer } from './valid-ui';

export const promocodeReducer = combineReducers({
  data: promocodeDataReducer,
  ui: promocodeUiReducer,
  validUi: promocodeValidUiReducer,
});
