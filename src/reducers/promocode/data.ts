// Modules
import produce from 'immer';
// Constants
import { PROMOCODE_SAVE, PROMOCODE_RESET } from '@constants/promocode';
// Types
import { TPromocodeDataReducer } from './types';

export const promocodeDataReducer: TPromocodeDataReducer = (state = null, { type, payload }) =>
  produce(state, (draft) => {
    switch (type) {
      case PROMOCODE_SAVE: {
        if (!state) {
          return payload;
        }

        return {
          ...draft,
          ...payload,
        };
      }

      case PROMOCODE_RESET: {
        return null;
      }

      default: {
        return draft || null;
      }
    }
  });
