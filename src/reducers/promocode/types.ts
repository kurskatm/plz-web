// Modules Types
import { Reducer } from 'redux';
// Main Types
import { TUiModel } from '@models/ui/types';
import { TPromocodeResult } from '@/type/promocode';

export type TPromocodeUiReducer = Reducer<TUiModel>;
export type TPromocodeDataReducer = Reducer<TPromocodeResult>;
export type TPromocodeValidUiReducer = Reducer<TUiModel>;
