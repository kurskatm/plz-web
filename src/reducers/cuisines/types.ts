// Modules Types
import { Reducer } from 'redux';
// Main Types
import { TCuisinesModel } from '@models/cuisines/types';

export type TCuisinesReducer = Reducer<TCuisinesModel>;
