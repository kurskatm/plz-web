// Modules
import produce from 'immer';
// Constants
import {
  CUISINES_SAVE,
  CUISINES_PUSH,
  CUISINES_REMOVE,
  CUISINES_UPDATE,
  CUISINES_FETCH_PENDING,
  CUISINES_FETCH_SUCCESS,
  CUISINES_FETCH_ERROR,
} from '@constants/cuisines';
import {
  PROFILE_RESET_DATA_AFTER_CITY_UPDATE,
  PROFILE_CITY_AND_ADDRESS_UPDATE,
} from '@constants/profile/city';
// Models
import { cuisinesModel } from '@models/cuisines';
// Utils
import { Consts } from '@utils';
// Types
import { TCuisinesReducer } from './types';

const { STATUS } = Consts.REQUEST.UI;

export const cuisinesReducer: TCuisinesReducer = (state = cuisinesModel, { type, payload }) =>
  produce(state, (draft) => {
    switch (type) {
      case CUISINES_SAVE: {
        draft.list.data = payload;
        break;
      }

      case CUISINES_PUSH: {
        draft.chosen.push(payload);
        break;
      }

      case CUISINES_REMOVE: {
        draft.chosen.splice(payload, 1);
        break;
      }

      case CUISINES_UPDATE: {
        draft.chosen = payload;
        break;
      }

      case CUISINES_FETCH_PENDING: {
        draft.list.ui.status = STATUS.PENDING;
        draft.list.ui.details = null;
        break;
      }

      case CUISINES_FETCH_SUCCESS: {
        draft.list.ui.status = STATUS.SUCCESS;
        draft.list.ui.details = payload.status;
        break;
      }

      case CUISINES_FETCH_ERROR: {
        draft.list.ui.status = STATUS.ERROR;
        draft.list.ui.details = payload.status;
        break;
      }

      case PROFILE_RESET_DATA_AFTER_CITY_UPDATE:
      case PROFILE_CITY_AND_ADDRESS_UPDATE: {
        draft.chosen = [];
        break;
      }

      default: {
        break;
      }
    }
  });
