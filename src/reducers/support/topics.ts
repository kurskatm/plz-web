// Modules
import produce from 'immer';
// Constants
import {
  SUPPORT_TOPICS_SAVE,
  SUPPORT_TOPICS_PENDING,
  SUPPORT_TOPICS_SUCCESS,
  SUPPORT_TOPICS_ERROR,
} from '@constants/support';
// Model
import { supportTopicsModel } from '@models/support/topics';
// Utils
import { Consts } from '@utils';
// Types
import { TSupportTopicsReducer } from './types';

const { PENDING, SUCCESS, ERROR } = Consts.REQUEST.UI.STATUS;

export const supportTopicsReducer: TSupportTopicsReducer = (
  state = supportTopicsModel,
  { type, payload },
) =>
  produce(state, (draft) => {
    switch (type) {
      case SUPPORT_TOPICS_SAVE: {
        draft.data = payload;
        break;
      }
      case SUPPORT_TOPICS_PENDING: {
        draft.ui.status = PENDING;
        draft.ui.details = null;
        break;
      }

      case SUPPORT_TOPICS_SUCCESS: {
        draft.ui.status = SUCCESS;
        draft.ui.details = payload.status;
        break;
      }

      case SUPPORT_TOPICS_ERROR: {
        draft.ui.status = ERROR;
        draft.ui.details = payload.status;
        break;
      }
      default: {
        break;
      }
    }
  });
