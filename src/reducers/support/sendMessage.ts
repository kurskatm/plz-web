// Modules
import produce from 'immer';
// Constants
import {
  SUPPORT_MESSAGE_SENT,
  SUPPORT_MESSAGE_PENDING,
  SUPPORT_MESSAGE_SUCCESS,
  SUPPORT_MESSAGE_ERROR,
} from '@constants/support';
// Model
import { supportTopicsModel } from '@models/support/topics';
// Utils
import { Consts } from '@utils';
// Types
import { TSupportTopicsReducer } from './types';

const { PENDING, SUCCESS, ERROR } = Consts.REQUEST.UI.STATUS;

export const supportSendMessageReducer: TSupportTopicsReducer = (
  state = supportTopicsModel,
  { type, payload },
) =>
  produce(state, (draft) => {
    switch (type) {
      case SUPPORT_MESSAGE_SENT: {
        draft.data = payload;
        break;
      }
      case SUPPORT_MESSAGE_PENDING: {
        draft.ui.status = PENDING;
        draft.ui.details = null;
        break;
      }

      case SUPPORT_MESSAGE_SUCCESS: {
        draft.ui.status = SUCCESS;
        draft.ui.details = payload.status;
        break;
      }

      case SUPPORT_MESSAGE_ERROR: {
        draft.ui.status = ERROR;
        draft.ui.details = payload.status;
        break;
      }
      default: {
        break;
      }
    }
  });
