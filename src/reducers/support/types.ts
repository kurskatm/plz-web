// Modules Types
import { Reducer } from 'redux';
// Main Types
import { TUiModel } from '@models/ui/types';
import { TSupportTopicsModel } from '@models/support/types';

export type TSupportUiReducer = Reducer<TUiModel>;
export type TSupportTopicsReducer = Reducer<TSupportTopicsModel>;
