// Modules
import { combineReducers } from 'redux';
// Reducers
import { supportTopicsReducer } from './topics';
import { supportSendMessageReducer } from './sendMessage';

export const supportReducer = combineReducers({
  topics: supportTopicsReducer,
  messageSent: supportSendMessageReducer,
});
