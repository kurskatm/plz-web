// Modules Types
import { Reducer } from 'redux';
// Types
import { TProfileModel } from '@models/profile/types';

export type TProfileReducer = Reducer<TProfileModel>;
