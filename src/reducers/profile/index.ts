// Modules
import produce from 'immer';
import isNil from 'lodash/isNil';
// Constants
// eslint-disable-next-line import/no-duplicates
import { PROFILE_RESET_ALL_UI_MODELS } from '@constants/profile';
import {
  // City
  PROFILE_CITY_UPDATE,
  PROFILE_RESET_DATA_AFTER_CITY_UPDATE,
} from '@constants/profile/city';
import {
  // Delivery
  PROFILE_UPDATE_PICKUP,
} from '@constants/profile/delivery';
import {
  // Auth
  PROFILE_AUTHORIZED,
  PROFILE_UNAUTHORIZED,
} from '@constants/profile/auth';
import {
  // Info
  PROFILE_GET_INFO_PENDING,
  PROFILE_GET_INFO_SUCCESS,
  PROFILE_GET_INFO_ERROR,
  PROFILE_GET_INFO_SAVE_DATA,
  PROFILE_GET_INFO_RESET_UI,
  // Update
  PROFILE_UPDATE_INFO_SAVE,
  PROFILE_UPDATE_INFO_SUCCESS,
  PROFILE_UPDATE_INFO_ERROR,
  PROFILE_UPDATE_INFO_PENDING,
  PROFILE_UPDATE_INFO_RESET_UI,
} from '@constants/profile/info';
import {
  // Reviews
  PROFILE_GET_REVIEWS_PENDING,
  PROFILE_GET_REVIEWS_SUCCESS,
  PROFILE_GET_REVIEWS_ERROR,
  PROFILE_GET_REVIEWS_SAVE_DATA,
  PROFILE_GET_REVIEWS_RESET_DATA,
  // Orders Reviews
  PROFILE_SET_ORDER_REVIEW_PENDING,
  PROFILE_SET_ORDER_REVIEW_SUCCESS,
  PROFILE_SET_ORDER_REVIEW_ERROR,
  PROFILE_SAVE_ORDER_REVIEW_DATA,
} from '@constants/profile/reviews';
import {
  // Friends
  PROFILE_GET_FRIENDS_PENDING,
  PROFILE_GET_FRIENDS_SUCCESS,
  PROFILE_GET_FRIENDS_ERROR,
  PROFILE_GET_FRIENDS_SAVE_DATA,
} from '@constants/profile/friends';
import {
  // Favourites
  PROFILE_GET_FAVORITES_INFO_PENDING,
  PROFILE_GET_FAVORITES_INFO_SUCCESS,
  PROFILE_GET_FAVORITES_INFO_ERROR,
  PROFILE_GET_FAVORITES_INFO_SAVE_DATA,
  PROFILE_GET_FAVORITES_INFO_RESET,
  // eslint-disable-next-line import/no-duplicates
} from '@constants/profile';
import {
  PROFILE_SET_FAVOURITE_PRODUCTS_SAVE_DATA,
  PROFILE_DELETE_FAVOURITE_PRODUCTS_SAVE_DATA,
} from '@constants/profile/favourite/products';
import {
  PROFILE_SET_FAVOURITE_REST_SAVE_DATA,
  PROFILE_DELETE_FAVOURITE_REST_SAVE_DATA,
} from '@constants/profile/favourite/rests';
import {
  // Orders
  PROFILE_SHOW_ORDER_REVIEW_MODAL,
  // Last Order without review
  PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW,
  PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_PENDING,
  PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_SUCCESS,
  PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_ERROR,
  PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_SAVE_DATA,
  PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_RESET_DATA,
  // Current Order
  PROFILE_GET_USER_ORDER_PENDING,
  PROFILE_GET_USER_ORDER_SUCCESS,
  PROFILE_GET_USER_ORDER_ERROR,
  PROFILE_GET_USER_ORDER_SAVE_DATA,
  PROFILE_RESET_USER_ORDER,
} from '@constants/profile/orders';
import {
  // Order contact me
  PROFILE_ORDER_CONTACT_ME_PENDING,
  PROFILE_ORDER_CONTACT_ME_IDLE,
  PROFILE_ORDER_CONTACT_ME_SUCCESS,
  PROFILE_ORDER_CONTACT_ME_ERROR,
  PROFILE_ORDER_CONTACT_ME_RESET,
} from '@constants/profile/orders/contact-me';
import {
  PROFILE_ORDER_REPEAT_ORDER,
  PROFILE_ORDER_REPEAT_ORDER_PENDING,
  PROFILE_ORDER_REPEAT_ORDER_SUCCESS,
  PROFILE_ORDER_REPEAT_ORDER_ERROR,
  PROFILE_ORDER_REPEAT_ORDER_SAVE,
  PROFILE_ORDER_REPEAT_ORDER_RESET,
  PROFILE_ORDER_REPEAT_ORDER_RESET_REDIRECT,
} from '@constants/profile/orders/repeat-order';
import {
  PROFILE_GET_NEW_ORDERS_PENDING,
  PROFILE_GET_NEW_ORDERS_SUCCESS,
  PROFILE_GET_NEW_ORDERS_ERROR,
  PROFILE_GET_NEW_ORDERS_SAVE_DATA,
  PROFILE_GET_NEW_ORDERS_RESET_DATA,
} from '@constants/profile/orders/new-orders';
import { RESTAURANT_CARD_REVIEWS_RESET } from '@constants/restaurant/reviews/current';
import {
  PROFILE_GET_NEW_ADDRESS_PENDING,
  PROFILE_GET_NEW_ADDRESS_SUCCESS,
  PROFILE_GET_NEW_ADDRESS_ERROR,
  PROFILE_SAVE_NEW_ADDRESS,
  PROFILE_UPDATE_NEW_ADDRESS,
  PROFILE_RESET_NEW_ADDRESS,
  PROFILE_RESET_NEW_ADDRESS_SUGGEST,
} from '@constants/profile/new-address';
import {
  PROFILE_GET_NEW_ADDRESSES_GEOCODE_PENDING,
  PROFILE_GET_NEW_ADDRESSES_GEOCODE_SUCCESS,
  PROFILE_GET_NEW_ADDRESSES_GEOCODE_ERROR,
  PROFILE_SAVE_NEW_ADDRESSES_GEOCODE,
  PROFILE_RESET_NEW_ADDRESSES_GEOCODE,
} from '@constants/profile/new-geocode';
import { RESTAURANT_SHOW_MODALS } from '@constants/restaurant/modals';
// Lib
import { getGeoCode, getAddress } from '@lib/cookie';
// Main types
import { TNewGeocodeData } from '@type/new-geocode';
// Models
import { profileModel } from '@models/profile';
import { infoModel } from '@models/profile/info';
import { reviewsModel, profileReviewsPaginationModel } from '@/models/profile/reviews';
import { favoritesSummaryModel } from '@/models/profile/favorites';
import { friendsModel } from '@/models/profile/friends';
import { ordersModel, profileOrdersPaginationModel } from '@models/profile/orders';
import { uiModel } from '@/models/ui';
// Utils
import { Consts } from '@utils';
import { trackEvent } from '@utils/analytics';
// Types
import { TProfileReducer } from './types';

const { STATUS } = Consts.REQUEST.UI;

export const profileReducer: TProfileReducer = (state = profileModel, { type, payload }) =>
  produce(state, (draft) => {
    switch (type) {
      // Auth
      case PROFILE_AUTHORIZED: {
        draft.auth = payload.access_token;
        draft.userId = payload.userId as string;
        break;
      }
      case PROFILE_UNAUTHORIZED: {
        draft.auth = null;
        draft.userId = null;
        draft.info = infoModel;
        draft.reviews = reviewsModel;
        draft.favorites = favoritesSummaryModel;
        draft.friends = friendsModel;
        draft.orders = ordersModel;
        break;
      }

      case PROFILE_RESET_ALL_UI_MODELS: {
        draft.info.ui = uiModel;
        draft.orders.newOrders.data = [];
        draft.orders.newOrders.after = null;
        draft.orders.newOrders.pagination = profileOrdersPaginationModel;
        draft.orders.newOrders.ui = uiModel;
        draft.reviews.ui = uiModel;
        draft.favorites.ui = uiModel;
        draft.orders.newOrders.ui = uiModel;
        break;
      }
      // City
      case PROFILE_CITY_UPDATE: {
        draft.city = payload;
        break;
      }

      case PROFILE_RESET_DATA_AFTER_CITY_UPDATE: {
        draft.newAddress.address = null;
        draft.newAddress.suggest.data = [];
        draft.newAddress.suggest.ui = uiModel;

        draft.newGeoCode.data = null;
        draft.newGeoCode.ui = uiModel;
        break;
      }

      case PROFILE_UPDATE_PICKUP: {
        draft.pickup = payload;
        break;
      }

      case PROFILE_UPDATE_INFO_PENDING: {
        draft.info.updateUi.status = STATUS.PENDING;
        draft.info.updateUi.details = null;
        break;
      }

      case PROFILE_UPDATE_INFO_SUCCESS: {
        draft.info.updateUi.status = STATUS.SUCCESS;
        draft.info.updateUi.details = payload;
        break;
      }

      case PROFILE_UPDATE_INFO_ERROR: {
        draft.info.updateUi.status = STATUS.ERROR;
        draft.info.updateUi.details = payload;
        break;
      }

      case PROFILE_UPDATE_INFO_RESET_UI: {
        draft.info.updateUi.status = STATUS.IDLE;
        draft.info.updateUi.details = null;
        break;
      }

      case PROFILE_UPDATE_INFO_SAVE: {
        draft.info.data = {
          ...draft.info.data,
          ...payload.payload,
          emailAddress: payload?.payload?.email,
        };

        if (payload.emailChanged) {
          // @ts-ignore
          draft.info.data.emailConfirmed = false;
        }
        break;
      }

      case PROFILE_GET_INFO_PENDING: {
        draft.info.ui.status = STATUS.PENDING;
        draft.info.ui.details = null;

        break;
      }

      case PROFILE_GET_INFO_SUCCESS: {
        const { status } = payload;
        draft.info.ui.status = STATUS.SUCCESS;
        draft.info.ui.details = status;

        break;
      }

      case PROFILE_GET_INFO_ERROR: {
        const { status } = payload;
        draft.info.ui.status = STATUS.ERROR;
        draft.info.ui.details = status;

        break;
      }

      case PROFILE_GET_INFO_SAVE_DATA: {
        draft.info.data = payload;

        break;
      }

      case PROFILE_GET_INFO_RESET_UI: {
        draft.info.ui = uiModel;
        draft.friends.ui = uiModel;
        break;
      }

      case PROFILE_GET_REVIEWS_PENDING: {
        draft.reviews.ui.status = STATUS.PENDING;
        draft.reviews.ui.details = null;
        draft.reviews.pagination.hasMore = false;

        break;
      }

      case PROFILE_GET_REVIEWS_SUCCESS: {
        const { status } = payload;
        draft.reviews.ui.status = STATUS.SUCCESS;
        draft.reviews.ui.details = status;

        const page = draft.reviews.pagination.page + 1;
        const quantity = draft.reviews.pagination.limit * page;
        draft.reviews.pagination.page = page;
        draft.reviews.pagination.hasMore = draft.reviews.data.length === quantity;

        break;
      }

      case PROFILE_GET_REVIEWS_ERROR: {
        const { status } = payload;
        draft.reviews.pagination.hasMore = false;
        draft.reviews.ui.status = STATUS.ERROR;
        draft.reviews.ui.details = status;

        break;
      }

      case PROFILE_GET_REVIEWS_SAVE_DATA: {
        draft.reviews.data = draft.reviews.data.concat(payload.data);
        draft.reviews.reviewsLength = payload.length;

        break;
      }

      case PROFILE_GET_REVIEWS_RESET_DATA: {
        draft.reviews.data = [];
        draft.reviews.pagination = profileReviewsPaginationModel;
        draft.reviews.ui = uiModel;
        break;
      }

      case PROFILE_GET_FRIENDS_PENDING: {
        draft.friends.ui.status = STATUS.PENDING;
        draft.friends.ui.details = null;

        break;
      }

      case PROFILE_GET_FRIENDS_SUCCESS: {
        const { status } = payload;
        draft.friends.ui.status = STATUS.SUCCESS;
        draft.friends.ui.details = status;

        break;
      }

      case PROFILE_GET_FRIENDS_ERROR: {
        const { status } = payload;
        draft.friends.ui.status = STATUS.ERROR;
        draft.friends.ui.details = status;

        break;
      }

      case PROFILE_GET_FRIENDS_SAVE_DATA: {
        draft.friends.data = payload;

        break;
      }

      // Get favorite restaurants
      case PROFILE_GET_FAVORITES_INFO_PENDING: {
        draft.favorites.ui.status = STATUS.PENDING;
        draft.favorites.ui.details = null;
        break;
      }

      case PROFILE_GET_FAVORITES_INFO_SUCCESS: {
        draft.favorites.ui.status = STATUS.SUCCESS;
        draft.favorites.ui.details = payload;
        break;
      }

      case PROFILE_GET_FAVORITES_INFO_ERROR: {
        draft.favorites.ui.status = STATUS.SUCCESS;
        draft.favorites.ui.details = payload;
        break;
      }

      case PROFILE_GET_FAVORITES_INFO_SAVE_DATA: {
        draft.favorites.data = payload;
        break;
      }

      case PROFILE_GET_FAVORITES_INFO_RESET: {
        draft.favorites.data = null;
        draft.favorites.ui = uiModel;
        break;
      }

      // Set favorite products
      case PROFILE_SET_FAVOURITE_PRODUCTS_SAVE_DATA: {
        if (draft.favorites.data?.products) {
          draft.favorites.data?.products.push(payload.newProduct);
        }
        break;
      }

      case PROFILE_SET_FAVOURITE_REST_SAVE_DATA: {
        if (draft.favorites.data?.restaurants) {
          draft.favorites.data.restaurants.push(payload.newRestaurant);
        }
        break;
      }

      case PROFILE_DELETE_FAVOURITE_PRODUCTS_SAVE_DATA: {
        if (draft.favorites.data?.products) {
          draft.favorites.data.products = draft.favorites.data.products.filter(
            (e) => e.id !== payload.productId,
          );
        }
        break;
      }

      case PROFILE_DELETE_FAVOURITE_REST_SAVE_DATA: {
        if (draft.favorites.data?.restaurants) {
          draft.favorites.data.restaurants = draft.favorites.data.restaurants.filter(
            (e) => e.id !== payload.restId,
          );
        }
        break;
      }

      case PROFILE_GET_NEW_ORDERS_PENDING: {
        draft.orders.newOrders.ui.status = STATUS.PENDING;
        draft.orders.newOrders.ui.details = null;
        draft.orders.newOrders.pagination.hasMore = false;
        break;
      }

      case PROFILE_GET_NEW_ORDERS_SUCCESS: {
        const page = draft.orders.newOrders.pagination.page + 1;
        const quantity = draft.orders.newOrders.pagination.limit * page;

        draft.orders.newOrders.pagination.page = page;
        draft.orders.newOrders.pagination.hasMore = draft.orders.newOrders.data.length === quantity;
        draft.orders.newOrders.ui.status = STATUS.SUCCESS;
        draft.orders.newOrders.ui.details = payload.status;
        break;
      }

      case PROFILE_GET_NEW_ORDERS_ERROR: {
        draft.orders.newOrders.pagination.hasMore = false;
        draft.orders.newOrders.ui.status = STATUS.ERROR;
        draft.orders.newOrders.ui.details = payload.status;
        break;
      }

      case PROFILE_GET_NEW_ORDERS_SAVE_DATA: {
        draft.orders.newOrders.data = draft.orders.newOrders.data.concat(payload.data);
        draft.orders.newOrders.after = payload.after;
        break;
      }

      case PROFILE_GET_NEW_ORDERS_RESET_DATA: {
        draft.orders.newOrders.data = [];
        draft.orders.newOrders.after = null;
        draft.orders.newOrders.pagination = profileOrdersPaginationModel;
        draft.orders.newOrders.ui = uiModel;
        break;
      }

      case PROFILE_SHOW_ORDER_REVIEW_MODAL: {
        const { showModal, orderId } = payload;
        draft.orders.modal.showModal = showModal;
        draft.orders.modal.orderId = orderId;
        if (!showModal) {
          draft.orders.reviews.add.ui = uiModel;
        }
        break;
      }

      case PROFILE_SET_ORDER_REVIEW_PENDING: {
        draft.orders.reviews.add.ui.status = STATUS.PENDING;
        draft.orders.reviews.add.ui.details = null;
        break;
      }

      case PROFILE_SET_ORDER_REVIEW_SUCCESS: {
        draft.orders.reviews.add.ui.status = STATUS.SUCCESS;
        draft.orders.reviews.add.ui.details = payload.status;
        break;
      }

      case PROFILE_SET_ORDER_REVIEW_ERROR: {
        draft.orders.reviews.add.ui.status = STATUS.ERROR;
        draft.orders.reviews.add.ui.details = payload.status;
        break;
      }

      case PROFILE_SAVE_ORDER_REVIEW_DATA: {
        const { orderId } = payload;
        draft.orders.reviews.add.data.push(orderId);

        const index = draft.orders.newOrders.data.findIndex(({ order }) => order.id === orderId);
        if (index !== -1) {
          draft.orders.newOrders.data[index] = {
            ...draft.orders.newOrders.data[index],
            order: {
              ...draft.orders.newOrders.data[index].order,
              reviewId: 'hasReview',
            },
          };
        }
        break;
      }

      case RESTAURANT_CARD_REVIEWS_RESET: {
        draft.orders.reviews.lastOrder.data = null;
        draft.orders.reviews.lastOrder.ui = uiModel;
        break;
      }

      case PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW: {
        if (draft.orders.reviews.add.ui !== uiModel) {
          draft.orders.reviews.add.ui = uiModel;
        }

        if (!isNil(draft.orders.reviews.lastOrder.data)) {
          draft.orders.reviews.lastOrder.data = null;
        }

        break;
      }

      case PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_PENDING: {
        draft.orders.reviews.lastOrder.ui.status = STATUS.PENDING;
        draft.orders.reviews.lastOrder.ui.details = null;
        break;
      }

      case PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_SUCCESS: {
        draft.orders.reviews.lastOrder.ui.status = STATUS.SUCCESS;
        draft.orders.reviews.lastOrder.ui.details = payload.status;
        break;
      }

      case PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_ERROR: {
        draft.orders.reviews.lastOrder.ui.status = STATUS.ERROR;
        draft.orders.reviews.lastOrder.ui.details = payload.status;
        break;
      }

      case PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_SAVE_DATA: {
        draft.orders.reviews.lastOrder.data = payload.order;
        break;
      }

      case PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_RESET_DATA: {
        draft.orders.reviews.lastOrder.data = null;
        draft.orders.reviews.lastOrder.ui = uiModel;
        break;
      }

      case PROFILE_GET_USER_ORDER_PENDING: {
        draft.orders.userOrder.ui.status = STATUS.PENDING;
        draft.orders.userOrder.ui.details = null;
        break;
      }

      case PROFILE_GET_USER_ORDER_SUCCESS: {
        draft.orders.userOrder.ui.status = STATUS.SUCCESS;
        draft.orders.userOrder.ui.details = payload.status;
        break;
      }

      case PROFILE_GET_USER_ORDER_ERROR: {
        draft.orders.userOrder.ui.status = STATUS.ERROR;
        draft.orders.userOrder.ui.details = payload.status;
        break;
      }

      case PROFILE_GET_USER_ORDER_SAVE_DATA: {
        draft.orders.userOrder.data = payload.order;
        break;
      }

      case PROFILE_RESET_USER_ORDER: {
        draft.orders.userOrder.data = null;
        draft.orders.userOrder.ui = uiModel;
        break;
      }

      case PROFILE_ORDER_CONTACT_ME_PENDING: {
        draft.orders.contactMe.ui.status = STATUS.PENDING;
        draft.orders.userOrder.ui.details = null;
        break;
      }

      case PROFILE_ORDER_CONTACT_ME_IDLE: {
        draft.orders.contactMe.ui.status = STATUS.IDLE;
        draft.orders.userOrder.ui.details = null;
        break;
      }

      case PROFILE_ORDER_CONTACT_ME_SUCCESS: {
        draft.orders.contactMe.ui.status = STATUS.SUCCESS;
        draft.orders.contactMe.ui.details = payload.status;
        draft.orders.contactMe.orderId = payload.orderId;
        trackEvent('call-to-me');

        const index = draft.orders.newOrders.data.findIndex(
          ({ order }) => order.id === payload.orderId,
        );
        if (index !== -1) {
          // eslint-disable-next-line max-len
          draft.orders.newOrders.data[index].order.isContactCustomerAvailable = !draft.orders
            .newOrders.data[index].order.isContactCustomerAvailable;
        }

        break;
      }

      case PROFILE_ORDER_CONTACT_ME_ERROR: {
        draft.orders.contactMe.ui.status = STATUS.ERROR;
        draft.orders.contactMe.orderId = payload.orderId;
        draft.orders.contactMe.ui.details = payload.status;
        draft.orders.contactMe.details = payload.details;

        const index = draft.orders.newOrders.data.findIndex(
          ({ order }) => order.id === payload.orderId,
        );
        if (index !== -1) {
          // eslint-disable-next-line max-len
          draft.orders.newOrders.data[index].order.isContactCustomerAvailable = !draft.orders
            .newOrders.data[index].order.isContactCustomerAvailable;
        }
        break;
      }

      case PROFILE_ORDER_CONTACT_ME_RESET: {
        draft.orders.contactMe.orderId = '';
        draft.orders.contactMe.ui.status = STATUS.IDLE;
        draft.orders.contactMe.ui.details = null;
        draft.orders.contactMe.details = null;

        if (!isNil(payload?.orderId)) {
          const index = draft.orders.newOrders.data.findIndex(
            ({ order }) => order.id === payload.orderId,
          );
          if (index !== -1) {
            // eslint-disable-next-line max-len
            draft.orders.newOrders.data[index].order.isContactCustomerAvailable = !draft.orders
              .newOrders.data[index].order.isContactCustomerAvailable;
          }
        }

        break;
      }

      case PROFILE_ORDER_REPEAT_ORDER: {
        draft.orders.repeatOrder.orderId = payload.orderId;
        break;
      }

      case PROFILE_ORDER_REPEAT_ORDER_PENDING: {
        draft.orders.repeatOrder.ui.status = STATUS.PENDING;
        draft.orders.repeatOrder.ui.details = null;
        trackEvent('repeatOrder');
        break;
      }

      case PROFILE_ORDER_REPEAT_ORDER_SUCCESS: {
        draft.orders.repeatOrder.ui.status = STATUS.SUCCESS;
        draft.orders.repeatOrder.ui.details = payload.status;
        break;
      }

      case PROFILE_ORDER_REPEAT_ORDER_ERROR: {
        draft.orders.repeatOrder.ui.status = STATUS.ERROR;
        draft.orders.repeatOrder.ui.details = payload.status;
        break;
      }

      case PROFILE_ORDER_REPEAT_ORDER_SAVE: {
        draft.orders.repeatOrder.data = payload;
        draft.orders.repeatOrder.needRedirect = true;
        break;
      }

      case PROFILE_ORDER_REPEAT_ORDER_RESET: {
        draft.orders.repeatOrder.data = null;
        draft.orders.repeatOrder.ui = uiModel;
        draft.orders.repeatOrder.orderId = '';
        draft.orders.repeatOrder.needRedirect = false;
        break;
      }

      case PROFILE_ORDER_REPEAT_ORDER_RESET_REDIRECT: {
        draft.orders.repeatOrder.needRedirect = false;
        break;
      }

      case PROFILE_GET_NEW_ADDRESS_PENDING: {
        draft.newAddress.suggest.ui.status = STATUS.PENDING;
        draft.newAddress.suggest.ui.details = null;
        break;
      }

      case PROFILE_GET_NEW_ADDRESS_SUCCESS: {
        draft.newAddress.suggest.ui.status = STATUS.SUCCESS;
        draft.newAddress.suggest.ui.details = payload.status;
        break;
      }

      case PROFILE_GET_NEW_ADDRESS_ERROR: {
        draft.newAddress.suggest.ui.status = STATUS.ERROR;
        draft.newAddress.suggest.ui.details = payload.status;
        break;
      }

      case PROFILE_SAVE_NEW_ADDRESS: {
        draft.newAddress.suggest.data = payload.data;
        break;
      }

      case PROFILE_UPDATE_NEW_ADDRESS: {
        draft.newAddress.address = payload;
        break;
      }

      case PROFILE_RESET_NEW_ADDRESS: {
        draft.newAddress.suggest.data = [];
        draft.newAddress.address = null;
        draft.newAddress.suggest.ui = uiModel;
        break;
      }

      case PROFILE_RESET_NEW_ADDRESS_SUGGEST: {
        draft.newAddress.suggest.data = [];
        draft.newAddress.suggest.ui = uiModel;
        break;
      }

      case PROFILE_GET_NEW_ADDRESSES_GEOCODE_PENDING: {
        draft.newGeoCode.ui.status = STATUS.PENDING;
        draft.newGeoCode.ui.details = null;
        break;
      }

      case PROFILE_GET_NEW_ADDRESSES_GEOCODE_SUCCESS: {
        draft.newGeoCode.ui.status = STATUS.SUCCESS;
        draft.newGeoCode.ui.details = payload.status;
        break;
      }

      case PROFILE_GET_NEW_ADDRESSES_GEOCODE_ERROR: {
        draft.newGeoCode.ui.status = STATUS.ERROR;
        draft.newGeoCode.ui.details = payload.status;
        break;
      }

      case PROFILE_SAVE_NEW_ADDRESSES_GEOCODE: {
        draft.newGeoCode.data = payload;
        draft.city = payload.cityId;
        break;
      }

      case PROFILE_RESET_NEW_ADDRESSES_GEOCODE: {
        draft.newGeoCode.data = null;
        draft.newGeoCode.ui = uiModel;
        break;
      }

      case RESTAURANT_SHOW_MODALS: {
        if (
          !payload.showModal &&
          draft.newGeoCode.data &&
          draft.newGeoCode.data.precision !== 'exact'
        ) {
          draft.newGeoCode.data = (getGeoCode() as TNewGeocodeData) || null;
        }
        if (!payload.showModal && draft.newAddress.suggest.data.length) {
          draft.newAddress.suggest.data = [];
          draft.newAddress.suggest.ui = uiModel;
        }
        if (
          !payload.showModal &&
          !(getAddress() as string) &&
          draft.newAddress.address &&
          draft.newAddress.address.name &&
          draft.newAddress.address.name.length
        ) {
          draft.newAddress.address = null;
        }
        break;
      }

      default: {
        break;
      }
    }
  });
