// Modules
import { Reducer, Store } from 'redux';
import { History } from 'history';
// Types
import { TReduxState } from '@models/types';

export type TRootReducer = (history: History) => Reducer<TReduxState>;
export type TInitStore = (history: History, reduxData: TReduxState) => Store<TReduxState>;
