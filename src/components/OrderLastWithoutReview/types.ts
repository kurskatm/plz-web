// Main types
import { TUserOrder } from '@type/profile';

export type TOrderProps = {
  order: TUserOrder;
};
