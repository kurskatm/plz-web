// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Selectors
import { restaurantLastOrderWithoutReviewModalSelector as mapStateToProps } from '@selectors/structured-selectors/restaurant/reviews';

const withConnect = connect(mapStateToProps);

export const enhance = compose(withConnect);
