import React, {
  FC, memo, useEffect, useState,
} from 'react';
import isNil from 'lodash/isNil';
// Components
import OrderComponent from '@components/Order/Content/OrderComponent';
// Enhance
import { enhance } from './enhance';
// Types
import { TOrderProps } from './types';

const LastOrderWithoutReviewComponent: FC<TOrderProps> = ({ order }) => {
  const [newOrder, setNewOrder] = useState(order);

  useEffect(() => {
    if (!isNil(order) && newOrder !== order) {
      setNewOrder(order);
    }

    if (isNil(order) && newOrder !== order) {
      setTimeout(() => setNewOrder(undefined), 400);
    }
  }, [newOrder, order]);

  return <OrderComponent order={newOrder} />;
};

const LastOrderWithoutReviewMemo = memo(LastOrderWithoutReviewComponent);
export default enhance(LastOrderWithoutReviewMemo);
