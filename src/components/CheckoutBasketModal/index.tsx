/* eslint-disable @typescript-eslint/no-unused-vars */
// Modules
import React, { FC, memo, useCallback } from 'react';
import { ModalWindow } from 'chibbis-ui-kit';
// Components
import ModalBody from './ModalBody';
// Enhance
import { enhance } from './enhance';
// Types
import { TChangeBasketModalProps } from './types';

const ChangeBasketModalComponent: FC<TChangeBasketModalProps> = ({
  showModal,
  changeBasketModalShow,
}) => {
  const closeModal = useCallback((): unknown => changeBasketModalShow(false), [
    changeBasketModalShow,
  ]);

  return (
    <ModalWindow
      classNameWrapper="basket-change-modal"
      closable
      setVisible={closeModal}
      visible={showModal}
    >
      <ModalBody closeModal={closeModal} />
    </ModalWindow>
  );
};

const ChangeBasketModalMemo = memo(ChangeBasketModalComponent);
export default enhance(ChangeBasketModalMemo);
