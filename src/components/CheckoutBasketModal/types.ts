// Main Types
import { TChangeBasketModalShow } from '@actions/checkout/modals/action-types';

export interface TChangeBasketModalProps {
  showModal: boolean;
  changeBasketModalShow: TChangeBasketModalShow;
}
