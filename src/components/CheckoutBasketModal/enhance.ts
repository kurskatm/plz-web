// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { changeBasketModalShow } from '@actions/checkout/modals';
// Selectors
import { basketModalStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/checkout-modals';

const mapDispatchToProps = {
  changeBasketModalShow,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
