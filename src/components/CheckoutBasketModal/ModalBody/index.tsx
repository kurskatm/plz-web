// Modules
import React, { FC, memo } from 'react';
// Module Components
import { Button } from 'chibbis-ui-kit';
// Types
import { ChangeBasketModalProps } from './types';

const ChangeBasketModalBodyComponent: FC<ChangeBasketModalProps> = ({ closeModal }) => (
  <div className="basket-change-modal__body">
    <div className="basket-change-modal__header">
      Состав корзины, стоимость
      <br />
      блюд и условия доставки
      <br />
      могут изменить для нового
      <br />
      адреса
    </div>
    <div className="basket-change-modal__buttons">
      <Button htmlType="button" size="small" type="primary" onClick={closeModal}>
        Хорошо, проверю
      </Button>
    </div>
  </div>
);

const ChangeBasketModalBodyMemo = memo(ChangeBasketModalBodyComponent);
export default ChangeBasketModalBodyMemo;
