// Modules
import React, { FC, memo } from 'react';
import { TBlackModalBaseProps } from './types';

const BlackModalBaseComponent: FC<TBlackModalBaseProps> = ({ clickHandler }) => (
  <div className="black-modal-base" onClickCapture={clickHandler} />
);

export default memo(BlackModalBaseComponent);
