// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { profileShowOrdersReviewModal } from '@actions/profile';
// Selectors
import { profileOrderReviewModalSelector as mapStateToProps } from '@selectors/structured-selectors/profile';

const mapDispatchToProps = {
  profileShowOrdersReviewModal,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
