export interface TOrderReviewFooterProps {
  setOrderReviewIsSuccess: boolean;
  setOrderReviewIsError: boolean;
  closeModal: () => void;
}
