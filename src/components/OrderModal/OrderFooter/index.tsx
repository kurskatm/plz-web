// Modules
import React, {
  FC, memo, useMemo, useCallback, useEffect,
} from 'react';
import classNames from 'classnames';
import { Link, useLocation } from 'react-router-dom';
// Utils
import { Consts } from '@/utils';
import { trackEvent } from '@utils/analytics';
// Enhance
import { enhance } from './enhance';
// Types
import { TOrderReviewFooterProps } from './types';

const { PATH } = Consts.ROUTES.INFO_PAGES.CONTACTS;

const RestaurantPath = new RegExp('/[a-z]*/restaurant/');
const ProfilePath = new RegExp('/[a-z]*/profile/');

const OrderFooterComponent: FC<TOrderReviewFooterProps> = ({
  setOrderReviewIsError,
  setOrderReviewIsSuccess,
  closeModal,
}) => {
  const location = useLocation();
  useEffect(
    () => {
      if (setOrderReviewIsSuccess) {
        if (location.pathname.match(ProfilePath)) {
          trackEvent('rate-order-orders');
        }
        if (location.pathname.match(RestaurantPath)) {
          trackEvent('rate-order-restaurant');
        }
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [setOrderReviewIsSuccess],
  );

  const getFooterStyles = useCallback(
    () =>
      classNames(
        { order_footer__success: setOrderReviewIsSuccess },
        { order_footer__error: setOrderReviewIsError },
      ),
    [setOrderReviewIsError, setOrderReviewIsSuccess],
  );

  const renderStatus = useMemo(() => {
    if (setOrderReviewIsSuccess) {
      return <div className={getFooterStyles()}>✓️️ Отзыв отправлен и скоро будет опубликован</div>;
    }
    if (setOrderReviewIsError) {
      return (
        <div className={getFooterStyles()}>
          Произошёл сбой. Повторите попытку через несколько минут или
          <br />
          <Link to={PATH} onClick={closeModal}>
            <span>Напишите нам</span>
          </Link>
        </div>
      );
    }

    return null;
  }, [getFooterStyles, setOrderReviewIsError, setOrderReviewIsSuccess, closeModal]);

  return renderStatus;
};

const OrderFooterMemo = memo(OrderFooterComponent);
export default enhance(OrderFooterMemo);
