// Modules
import React, {
  FC, memo, useCallback, useMemo,
} from 'react';
import { ModalWindow } from 'chibbis-ui-kit';
import OrdersModalContent from '../Order';
import LastOrderModalContent from '../OrderLastWithoutReview';
import OrderModalFooter from './OrderFooter';
// Enhance
import { enhance } from './enhance';
// Types
import { TShowOrdersReviewModalProps } from './types';

const OrderModalComponent: FC<TShowOrdersReviewModalProps> = ({
  showModal,
  profileShowOrdersReviewModal,
  lastOrderModal,
}) => {
  const closeModal = useCallback(() => {
    profileShowOrdersReviewModal({
      showModal: false,
      orderId: '',
    });
  }, [profileShowOrdersReviewModal]);

  const renderModalContent = useMemo(() => {
    if (lastOrderModal) {
      return <LastOrderModalContent />;
    }

    return <OrdersModalContent />;
  }, [lastOrderModal]);

  return (
    <ModalWindow
      classNameWrapper="orders-review-modal"
      closable
      setVisible={closeModal}
      visible={showModal}
      footerContent={<OrderModalFooter closeModal={closeModal} />}
    >
      {renderModalContent}
    </ModalWindow>
  );
};

OrderModalComponent.defaultProps = {
  lastOrderModal: false,
};

const OrderModalMemo = memo(OrderModalComponent);
export default enhance(OrderModalMemo);
