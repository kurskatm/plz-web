// Main Types
import { TProfileShowOrdersReviewModal } from '@actions/profile/action-types';

export interface TShowOrdersReviewModalProps {
  showModal: boolean;
  profileShowOrdersReviewModal: TProfileShowOrdersReviewModal;
  lastOrderModal?: boolean;
}
