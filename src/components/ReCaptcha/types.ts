export interface TReCaptchaProps {
  token: string;
  tokenHandler?: (result: string) => void;
}
