// Modules
import React, { FC, useCallback } from 'react';
import { Checkbox } from 'chibbis-ui-kit';
import { GoogleReCaptcha, useGoogleReCaptcha } from 'react-google-recaptcha-v3';

// Types
import { TReCaptchaProps } from './types';

export const GoogleRecaptchaComponent: FC<TReCaptchaProps> = ({ tokenHandler, token }) => {
  const handleReCaptchaVerify = useCallback(() => {}, []);
  const { executeRecaptcha } = useGoogleReCaptcha();
  const changeHandler = useCallback(async () => {
    if (!executeRecaptcha) {
      return null;
    }

    const result = await executeRecaptcha('submit');

    return tokenHandler(result);
  }, [executeRecaptcha, tokenHandler]);
  return (
    <div>
      <Checkbox
        name="recapcha"
        required
        view="Я не робот"
        disabled={token}
        onChange={changeHandler}
      />
      <GoogleReCaptcha action="submit" onVerify={handleReCaptchaVerify} />
    </div>
  );
};
