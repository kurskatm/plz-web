// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { authModalShow } from '@actions/auth';
import { showModifiersCardModal } from '@actions/restaurant/modal';
import { restaurantCardIdSave } from '@/actions/restaurant/main/id';

// Selectors
import { hitsAuthStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/home';
import { restaurantCardFetchSuccess, restaurantCardSave } from '@/actions/restaurant/main/card';

const mapDispatchToProps = {
  authModalShow,
  showModifiersCardModal,
  restaurantCardIdSave,
  restaurantCardSave,
  restaurantCardFetchSuccess,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
