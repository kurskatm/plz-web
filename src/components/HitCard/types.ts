// Main Types
import { TRestaurantProduct } from '@type/restaurants/restaurant-products';
import { TProfileModel } from '@models/profile/types';
import { TShowModifiersCardModal } from '@actions/restaurant/modal/action-types';
import { TCitiesItem } from '@type/cities';
import { TRestaurantCardIdSave } from '@actions/restaurant/main/id/action-types';
import {
  TRestaurantCardSave,
  TRestaurantCardFetchSuccess,
} from '@/actions/restaurant/main/card/action-types';
import { TRestaurantCard } from '@/type/restaurants/restaurant-card';
import { TCityHitRestaurant } from '@type/foodForPoints';

export interface THitCardProps {
  disabled?: boolean;
  item: TRestaurantProduct & {
    cityHitRestaurant?: TCityHitRestaurant;
  };
  quantity?: number;
  link?: string;
  updateShopingCart: (quantity: number) => void;
  onLikeClick: (e: MouseEvent, liked: boolean, cb: () => void) => void;
  authModalShow: (data: boolean) => void;
  showModifiersCardModal: TShowModifiersCardModal;
  profile: TProfileModel;
  withLike?: boolean;
  index?: number;
  setItemHeight?: (height: number, index: number) => void;
  bodyHeight?: number;
  city?: TCitiesItem;
  restaurantCardIdSave: TRestaurantCardIdSave;
  withRestLogo?: boolean;
  restaurant: TRestaurantCard;
  restaurantCardSave: TRestaurantCardSave;
  restaurantCardFetchSuccess: TRestaurantCardFetchSuccess;
}
