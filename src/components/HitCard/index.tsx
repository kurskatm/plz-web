// Modules
import React, {
  FC, memo, useCallback, useEffect, useMemo, useRef, useState,
} from 'react';
import { Link } from 'react-router-dom';
import { useHistory } from 'react-router';
import Truncate from 'react-truncate';
import debounce from 'lodash/debounce';
import isNil from 'lodash/isNil';
// import isEmpty from 'lodash/isEmpty';
import classNames from 'classnames';
// Modules Components
import {
  Amount, Button, Liked, ScoresIcon,
} from 'chibbis-ui-kit';
// Utils
import { applyImageFallback } from '@/utils';
// Assets
import cardFoodDefault from '@/../assets/svg/main/card-food-default.svg';
import cardRestDefault from '@/../assets/svg/main/card-rest-default.svg';
// Enhance
import { enhance } from './enhance';
// Types
import { THitCardProps } from './types';

const RestaurantPath = new RegExp('/[a-z]*/restaurant/');

const HitCardCompomnent: FC<THitCardProps> = ({
  disabled,
  item,
  quantity,
  restaurant,
  updateShopingCart,
  onLikeClick,
  authModalShow,
  showModifiersCardModal,
  restaurantCardSave,
  restaurantCardFetchSuccess,
  profile,
  link,
  withLike = true,
  index,
  bodyHeight,
  setItemHeight,
  city,
  restaurantCardIdSave,
  withRestLogo = true,
}) => {
  const cardRef = useRef<HTMLDivElement>();
  const likeRef = useRef<HTMLDivElement>();
  const panelRef = useRef<HTMLDivElement>();
  const contentRef = useRef<HTMLDivElement>();
  const restRef = useRef<HTMLDivElement>();
  const [renderImage, setRenderImage] = useState<boolean>(false);
  const [liked, setLiked] = useState(item.isFavorite);
  const [height, setHeight] = useState<number>(contentRef.current?.offsetHeight);

  const history = useHistory();

  const url = useMemo(
    () =>
      `/${city?.urlName}/restaurant/${
        // @ts-ignore Property 'restaurant' is missing in type 'TRestaurantProduct'
        item.restaurant?.urlName || item?.restaurantUrlName || item?.cityHitRestaurant?.urlName
      }`,
    [
      city?.urlName,
      // @ts-ignore Property 'restaurant' is missing in type 'TRestaurantProduct'
      item.restaurant?.urlName,
      item?.restaurantUrlName,
      item?.cityHitRestaurant,
    ],
  );

  useEffect(() => {
    const content = contentRef.current;
    if (!isNil(content) && setItemHeight) {
      const cardContentResizeObserver = new ResizeObserver(() => {
        setItemHeight(content.offsetHeight, index);
      });
      cardContentResizeObserver.observe(content);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [index]);

  useEffect(() => {
    if (height !== bodyHeight) {
      setHeight(bodyHeight);
    }
  }, [bodyHeight, height]);

  const scrollEventRef = useRef(
    debounce(() => {
      if (cardRef.current) {
        const windowHeight = window.innerHeight;
        const elementPosition = Math.round(cardRef.current.getBoundingClientRect().top);

        if (!renderImage && elementPosition > 0 && elementPosition <= windowHeight + 500) {
          setRenderImage(true);
        }
      }
    }, 50),
  );

  useEffect(
    () => {
      scrollEventRef.current();

      const callback = scrollEventRef.current;
      document.addEventListener('scroll', callback);

      return () => {
        document.removeEventListener('scroll', callback);
      };
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  useEffect(
    () => {
      if (item.isFavorite !== liked) {
        setLiked(item.isFavorite);
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [item.isFavorite],
  );

  const getMainStyles = useCallback(
    () =>
      classNames('hit-card', {
        'hit-card-for-points': item?.availableInBonusPoints,
        'hit-card-for-points__remove': quantity !== 0 && !item?.modifiers?.length,
      }),
    [item?.availableInBonusPoints, quantity, item?.modifiers?.length],
  );

  const onChange = useCallback(
    (newQuantity: string) => {
      updateShopingCart(+newQuantity);
    },
    [updateShopingCart],
  );

  const handleClick = useCallback(() => {
    const updateShopingCartCb = () => {
      const value = quantity ? 0 : 1;
      updateShopingCart(value);
    };

    if (restaurant) {
      restaurantCardSave(restaurant);
      restaurantCardFetchSuccess({ status: 200 });
    }

    if (!item.availableInBonusPoints) {
      if (item.modifiers?.length) {
        showModifiersCardModal({
          showModal: true,
          productId: item.id,
        });
      } else {
        updateShopingCartCb();
      }
    } else if (!profile.auth) {
      authModalShow(true);
      updateShopingCartCb();
    } else {
      updateShopingCartCb();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [authModalShow, profile.auth, quantity, updateShopingCart, item, showModifiersCardModal]);

  const renderButton = useMemo(() => {
    if (!isNil(link)) {
      return (
        <Link to={link}>
          <Button
            htmlType="button"
            className="hit__button"
            disabled={disabled}
            onClick={handleClick}
            size="small"
            ariaLabel="Взять"
          >
            {quantity && !item.modifiers.length ? 'Убрать' : 'Взять'}
          </Button>
        </Link>
      );
    }
    return (
      <Button
        htmlType="button"
        className="hit__button"
        disabled={disabled}
        onClick={handleClick}
        size="small"
      >
        {quantity && !item.modifiers.length ? 'Убрать' : 'Взять'}
      </Button>
    );
  }, [disabled, handleClick, link, quantity, item]);

  const getCoast = useMemo(
    () => () => {
      if (isNil(item.price)) {
        return null;
      }

      if (item?.availableInBonusPoints) {
        return (
          <div className="hit-cart__for-points">
            <div>
              <ScoresIcon width={24} height={24} />
            </div>
            <div>{item.price}</div>
          </div>
        );
      }

      return `${item.price} ₽`;
    },
    [item?.availableInBonusPoints, item?.price],
  );

  const likeClickHandler = useCallback(
    (e) => {
      onLikeClick(e, liked, () => setLiked(!liked));
    },
    [onLikeClick, setLiked, liked],
  );

  const onCardClick = useCallback(
    (event: MouseEvent) => {
      if (
        // @ts-ignore
        !likeRef.current?.contains(event.target)
        // @ts-ignore
        && !panelRef.current.contains(event.target)
        // @ts-ignore
        && !restRef?.current?.contains(event.target)
        // @ts-ignore
        && (item?.restaurant || history.location.pathname.match(RestaurantPath))
      ) {
        showModifiersCardModal({
          showModal: true,
          productId: item.id,
          inBonusPoint: item.availableInBonusPoints,
        });
      }
    },
    [item, showModifiersCardModal, history.location.pathname],
  );

  const handleRestClick = () => {
    // @ts-ignore
    restaurantCardIdSave({ id: item.restaurant?.id || item?.cityHitRestaurant.id });
    history.push(url);
    window.scrollTo(0, 0);
  };

  return (
    <div
      ref={cardRef}
      className={getMainStyles()}
      // @ts-ignore
      onClickCapture={(e: MouseEvent) => onCardClick(e)}
    >
      <div className="hit-card-inner zoom-hover">
        <div className="hit-card__top">
          {withLike && (
            <div className="hit-cart__top-like" ref={likeRef}>
              <Liked selected={liked} onClick={likeClickHandler} />
            </div>
          )}
        </div>

        <div className="hit-card__image-wrap">
          {renderImage && (
            <figure>
              <img
                src={item?.imageUrls[0] || cardFoodDefault}
                onError={applyImageFallback(cardFoodDefault)}
                alt={item.name}
                width="320"
                height="160"
              />
            </figure>
          )}
        </div>

        {!history.location.pathname.match(RestaurantPath) && withRestLogo && (
          <div
            className="hit-card__rest-logo"
            // @ts-ignore
            onClickCapture={handleRestClick}
            ref={restRef}
          >
            <img
              // @ts-ignore Property 'restaurant' is missing in type 'TRestaurantProduct'
              src={item?.restaurant?.logo || item?.cityHitRestaurant.logo || cardRestDefault}
              onError={applyImageFallback(cardRestDefault)}
              // @ts-ignore Property 'restaurant' is missing in type 'TRestaurantProduct'
              alt={item?.restaurant?.name || item?.cityHitRestaurant.name}
              width="48"
              height="48"
            />
          </div>
        )}

        <div className="hit-card__content-wrap" ref={contentRef} style={{ height }}>
          <div>{item.name && <div className="hit-card__title">{item.name}</div>}</div>

          <div>
            {item.description && (
              <div className="hit-card__description">
                <Truncate lines={2}>{item.description}</Truncate>
              </div>
            )}
          </div>

          <div className="hit-card__panel">
            <div>{!isNil(item.price) && <div className="hit-card__coast">{getCoast()}</div>}</div>

            {/* <div>
              {!isNil(item.weight) && <div className="hit-card__weight">{item.weight}</div>}
            </div> */}

            <div>
              <div className="hit-card__by-wrap" ref={panelRef}>
                {!item?.availableInBonusPoints && !item?.modifiers?.length && quantity ? (
                  <Amount
                    key={`${item.id}-${quantity}`}
                    name={`home-hit-${item.id}-amount`}
                    onChange={onChange}
                    value={quantity}
                    className="hit-card__amount-buttons"
                    noBorder
                    noInput
                  />
                ) : (
                  renderButton
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

HitCardCompomnent.defaultProps = {
  disabled: false,
  quantity: 0,
};

const HitCardMemo = memo(HitCardCompomnent);
export default enhance(HitCardMemo);
