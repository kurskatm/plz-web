// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { authModalShow } from '@actions/auth';
// Selectores
import { authStructuredSelector } from '@selectors/structured-selectors/auth';

const mapDispatchToProps = {
  authModalShow,
};
const withConnect = connect(authStructuredSelector, mapDispatchToProps);

export const enhance = compose(withConnect);
