// Modules
import React, {
  FC, memo, useCallback,
} from 'react';
import { ModalWindow } from 'chibbis-ui-kit';
import AccountLogin from '@components/AccountLogin';
// Enhance
import { enhance } from './enhance';
// Types
import { TModalProps } from './types';

const AccountModalComponent: FC<TModalProps> = ({ modalShow, authModalShow }) => {
  const closeModal = useCallback((): unknown => authModalShow(false), [authModalShow]);
  const toggleVisible = (show: boolean) => authModalShow(show);

  return (
    <ModalWindow
      classNameWrapper="enter-modal"
      closable
      setVisible={closeModal}
      visible={modalShow}
    >
      <AccountLogin toggleVisible={toggleVisible} form="client" />
    </ModalWindow>
  );
};

const AccountModalMemo = memo(AccountModalComponent);
export default enhance(AccountModalMemo);
