export interface TModalProps {
  authModalShow: (data: boolean) => void;
  modalShow: boolean;
}
