// Main Types
import { TCitiesItem } from '@type/cities';

export type TWorkingTime = {
  openTime: number;
  closeTime: number;
};

export type TSpecialization = {
  id: string;
  name: string;
  urlName: string;
};

export interface TRestaurantCardProps {
  id?: string;
  city?: TCitiesItem;
  logo: string;
  name: string;
  urlName?: string;
  specializations: TSpecialization[];
  workingTime: TWorkingTime;
  liked: boolean;
  minCost: number;
  positiveReviewsPercent: number;
  deliveryCost: number;
  isNew?: boolean;
  hasSpecialPlacement?: boolean;
  hasPaymentForPoints?: boolean;
  hasPromotions?: boolean;
}

export type TRestaurantCardPropsList = TRestaurantCardProps[];

export interface TRestaurantCardBodyProps {
  deliveryCost: number;
  liked: boolean;
  logo: string;
  minCost: number;
  name: string;
  positiveReviewsPercent: number;
  specializations?: TSpecialization[];
  urlName?: string;
  workingTime: TWorkingTime;
}

export interface TRestaurantCardFooterProps {
  positiveReviewsPercent: number;
}

export interface TRestaurantCardStockProps {
  isNew?: boolean;
  hasSpecialPlacement?: boolean;
  hasPaymentForPoints?: boolean;
  hasPromotions?: boolean;
}
