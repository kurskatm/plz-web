// Modules
import React, { FC, useCallback, memo } from 'react';
import { Link } from 'react-router-dom';
import isString from 'lodash/isString';
import classnames from 'classnames';
// Modules Components
import RestaurantCardContentComponent from './Content';
import RestaurantCardStockComponent from './Content/Stock';
// Enhance
import { enhance } from './enhance';
// Types
import { TRestaurantCardProps } from './types';

const RestaurantCardComponent: FC<TRestaurantCardProps> = ({
  city,
  logo,
  name,
  urlName,
  specializations,
  workingTime,
  liked,
  minCost,
  positiveReviewsPercent,
  deliveryCost,
  isNew,
  hasSpecialPlacement,
  hasPaymentForPoints,
  hasPromotions,
}) => {
  const getLink = useCallback(() => {
    if (isString(urlName)) {
      return `/${city?.urlName}/restaurant/${urlName}/menu`;
    }

    return '';
  }, [city?.urlName, urlName]);

  const getStyles = useCallback(
    () =>
      classnames('restaurant-card', {
        'restaurant-card_is-disabled': !workingTime,
      }),
    [workingTime],
  );

  return (
    <div className={getStyles()}>
      <Link to={getLink()}>
        <RestaurantCardStockComponent
          hasSpecialPlacement={hasSpecialPlacement}
          hasPaymentForPoints={hasPaymentForPoints}
          hasPromotions={hasPromotions}
          isNew={isNew}
        />
        <RestaurantCardContentComponent
          logo={logo}
          name={name}
          specializations={specializations}
          workingTime={workingTime}
          liked={liked}
          minCost={minCost}
          positiveReviewsPercent={positiveReviewsPercent}
          deliveryCost={deliveryCost}
        />
      </Link>
    </div>
  );
};

const RestaurantCardMemo = memo(RestaurantCardComponent);
export default enhance(RestaurantCardMemo);
