// Modules
import React, { FC, memo } from 'react';
// Modules Components
import { SmallFeedback } from 'chibbis-ui-kit';
// Types
import { TRestaurantCardFooterProps } from '../types';

const RestaurantCardFooterComponent:
FC<TRestaurantCardFooterProps> = ({
  positiveReviewsPercent,
}: TRestaurantCardFooterProps) => (
  <div className="restaurant-card__footer">
    <div className="restaurant-card__reviews">
      <SmallFeedback
        percent={positiveReviewsPercent}
      />
    </div>
  </div>
);

export default memo(RestaurantCardFooterComponent);
