// Modules
import React, { FC, useMemo, memo } from 'react';
// Modules Components
import { SmallFeedback } from 'chibbis-ui-kit';
// Lib
import { getHoursAndMinutes } from '@lib/restaurants/getHoursAndMinutes';
// Assets
import cardRestDefault from '@/../assets/svg/main/card-rest-default.svg';
// Types
import { TRestaurantCardBodyProps } from '../types';

const RestaurantCardBodyComponent: FC<TRestaurantCardBodyProps> = ({
  deliveryCost,
  logo = cardRestDefault,
  minCost,
  name,
  positiveReviewsPercent,
  workingTime,
}: TRestaurantCardBodyProps) => {
  const workingTimeFormatted = useMemo(
    () =>
      (workingTime
        ? `c ${getHoursAndMinutes(workingTime.openTime)} до ${getHoursAndMinutes(
          workingTime.closeTime,
        )}`
        : 'Временно закрыт'),
    [workingTime],
  );
  const getMinCost = useMemo(() => `от ${minCost} ₽`, [minCost]);
  const getDeliveryCost = useMemo(
    () => (deliveryCost ? `Доставка ${deliveryCost} ₽` : 'Доставка бесплатно'),
    [deliveryCost],
  );

  return (
    <div className="restaurant-card__body">
      <div className="restaurant-card__logo">
        <img className="restaurant-card__image" src={logo} alt={name} />
      </div>
      <div className="restaurant-card__info">
        <div className="restaurant-card__title">{name}</div>
        <div className="restaurant-card__details">
          <div className="restaurant-card__working-time">{workingTimeFormatted}</div>
          <div className="restaurant-card__min-cost">{getMinCost}</div>
          <div className="restaurant-card__delivery-cost">
            <span>{getDeliveryCost}</span>
            <div className="restaurant-card__reviews">
              <SmallFeedback percent={positiveReviewsPercent} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default memo(RestaurantCardBodyComponent);
