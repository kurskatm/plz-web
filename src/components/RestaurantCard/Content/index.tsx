// Modules
import React, { FC, memo } from 'react';
import { Liked } from 'chibbis-ui-kit';
// Components
import RestaurantCardBodyComponent from './Body';
// Types
import { TRestaurantCardProps } from '../types';

const RestaurantCardContentComponent: FC<TRestaurantCardProps> = ({
  logo,
  name,
  specializations,
  workingTime,
  liked,
  minCost,
  positiveReviewsPercent,
  deliveryCost,
}) => (
  <div className="restaurant-card__content">
    <RestaurantCardBodyComponent
      deliveryCost={deliveryCost}
      liked={liked}
      logo={logo}
      name={name}
      minCost={minCost}
      positiveReviewsPercent={positiveReviewsPercent}
      specializations={specializations}
      workingTime={workingTime}
    />
    <div role="presentation" className="restaurant-card__like" onClick={(e) => e.preventDefault()}>
      <Liked selected={liked} />
    </div>
  </div>
);

export default memo(RestaurantCardContentComponent);
