// Modules
import React, { FC, useMemo, memo } from 'react';
// Modules Components
import { RibbonLabel } from 'chibbis-ui-kit';
// Utils
import { getRibbonProps } from '@lib/restaurants/get-label';
// Types
import { TRestaurantCardStockProps } from '../types';

const RestaurantCardStockComponent:
FC<TRestaurantCardStockProps> = ({
  isNew,
  hasSpecialPlacement,
  hasPaymentForPoints,
  hasPromotions,
}: TRestaurantCardStockProps) => {
  const ribbonProps = useMemo(() => getRibbonProps({
    isNew,
    hasSpecialPlacement,
    hasPaymentForPoints,
    hasPromotions,
  }), [
    isNew,
    hasSpecialPlacement,
    hasPaymentForPoints,
    hasPromotions,
  ]);

  return ribbonProps ? (
    <div className="restaurant-card__stock">
      <RibbonLabel isRadial color={ribbonProps.color}>
        {ribbonProps.text}
      </RibbonLabel>
    </div>
  ) : null;
};

export default memo(RestaurantCardStockComponent);
