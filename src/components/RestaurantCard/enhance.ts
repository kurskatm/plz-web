// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Selectors
import { restaurantCardStructuredSelector as initialState } from '@selectors/structured-selectors/restaurant/other';

const withConnect = connect(initialState);
export const enhance = compose(withConnect);
