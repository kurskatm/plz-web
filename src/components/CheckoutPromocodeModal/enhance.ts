// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { changePromocodeModalShow } from '@actions/checkout/modals';
// Selectors
import { promocodeModalStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/checkout-modals';

const mapDispatchToProps = {
  changePromocodeModalShow,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
