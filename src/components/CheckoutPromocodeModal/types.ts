// Main Types
import { TChangePromocodeModalShow } from '@actions/checkout/modals/action-types';

export interface TChangePromocodeModalProps {
  showModal: boolean;
  changePromocodeModalShow: TChangePromocodeModalShow;
}
