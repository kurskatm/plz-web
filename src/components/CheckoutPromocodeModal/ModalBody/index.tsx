// Modules
import React, { FC, memo } from 'react';
// Module Components
import { Button } from 'chibbis-ui-kit';
// Assets
import PromocodeIcon from '@/../assets/svg/main/promocode-icon.svg';
// Types
import { ChangePromocodeModalProps } from './types';

const ChangePromocodeModalBodyComponent: FC<ChangePromocodeModalProps> = ({ closeModal }) => (
  <div className="promocode-change-modal__body">
    <div className="promocode-change-modal__icon">
      <img src={PromocodeIcon} alt="promocode" />
    </div>
    <div className="promocode-change-modal__header">
      Не забудьте заново ввести
      <br />
      промокод, кода будете
      <br />
      оформлять заказ
    </div>
    <div className="promocode-change-modal__buttons">
      <Button htmlType="button" size="small" type="primary" onClick={closeModal}>
        Понятно
      </Button>
    </div>
  </div>
);

const ChangePromocodeModalBodyMemo = memo(ChangePromocodeModalBodyComponent);
export default ChangePromocodeModalBodyMemo;
