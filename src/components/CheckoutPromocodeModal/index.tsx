/* eslint-disable @typescript-eslint/no-unused-vars */
// Modules
import React, { FC, memo, useCallback } from 'react';
import { ModalWindow } from 'chibbis-ui-kit';
// Components
import ModalBody from './ModalBody';
// Enhance
import { enhance } from './enhance';
// Types
import { TChangePromocodeModalProps } from './types';

const ChangePromocodeModalComponent: FC<TChangePromocodeModalProps> = ({
  showModal,
  changePromocodeModalShow,
}) => {
  const closeModal = useCallback((): unknown => changePromocodeModalShow(false), [
    changePromocodeModalShow,
  ]);

  return (
    <ModalWindow
      classNameWrapper="promocode-change-modal"
      closable
      setVisible={closeModal}
      visible={showModal}
    >
      <ModalBody closeModal={closeModal} />
    </ModalWindow>
  );
};

const ChangePromocodeModalMemo = memo(ChangePromocodeModalComponent);
export default enhance(ChangePromocodeModalMemo);
