// Modules
import classNames from 'classnames';
// Types
import { TGetReviewType } from './types';

export const getReviewType: TGetReviewType = (liked) =>
  classNames(
    'review-item-header-review-type',
    `review-item-header-review-type-${liked ? 'positive' : 'negative'}`,
  );
