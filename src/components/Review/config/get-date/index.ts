// Modules
import dayjs from 'dayjs';
// Types
import { TGetDate } from './types';

export const getDate: TGetDate = (date) => {
  const DATE_TODAY = dayjs(new Date()).locale('ru').format('D MMMM YYYY');
  const commentDate = dayjs(date).locale('ru').format('D MMMM YYYY');
  const commentTime = dayjs(date).format('hh:mm');
  const isToday = DATE_TODAY === commentDate;

  if (isToday) {
    return `Сегодня в ${commentTime}`;
  }

  return `${commentDate} в ${commentTime}`;
};
