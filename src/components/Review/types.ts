// Main Types
import { TReview } from '@type/restaurants/reviews';

export interface ReviewProps {
  item: TReview;
}
