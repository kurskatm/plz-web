// Modules
import React, {
  FC, memo, useCallback, useMemo,
} from 'react';
// Mainn types
import { TOrder, TOrderList } from '@type/restaurants/reviews';
// Config
import { getDate } from '@lib/date/get-date';
import { getReviewType } from './config/get-review-type';
// Types
import { ReviewProps } from './types';

const ReviewComponent: FC<ReviewProps> = ({ item }) => {
  const getDateCallback = useCallback((date: string) => getDate(date), []);

  const getReviewTypeCallback = useCallback((liked: boolean) => getReviewType(liked), []);
  const getShortName = useCallback(
    (name: string) => {
      if (name.length <= 24) {
        return name;
      }

      return `${name.slice(0, 21)}...`;
    },
    [],
  );

  const renderOrders = useMemo(
    () => (order: TOrder, index: number) => {
      if (index !== item?.order.length - 1) {
        if (order.quantity === 1) {
          return `${order.name}, `;
        }

        return <span>{`${order.quantity} × ${order.name}, `}</span>;
      }

      if (order.quantity === 1) {
        return `${order.name}`;
      }

      return <span>{`${order.quantity} × ${order.name}`}</span>;
    },
    [item?.order],
  );

  const getOrder = useCallback(
    (orderList: TOrderList) => {
      if (orderList.length) {
        return orderList.map(renderOrders);
      }

      return <span>Заказ не был указан</span>;
    },
    [renderOrders],
  );

  return (
    <div key={item.id} className="review-item">
      <div className="review-item-header">
        <div className={getReviewTypeCallback(item.liked)} />

        <div className="review-item-header-content">
          <div className="review-item-header-message">{item.text || ''}</div>
          {!!getOrder(item.order) && (
            <div className="review-item-header-order">{getOrder(item.order)}</div>
          )}

          <div className="review-item-footer">
            <div className="review-item-author">
              {
                item.username
                  ? getShortName(item.username)
                  : 'Анонимно'
              }
            </div>
            <div className="review-item-date">{getDateCallback(item.addedOn)}</div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default memo(ReviewComponent);
