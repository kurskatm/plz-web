// Modules
import React, {
  FC, memo, useCallback, useMemo,
} from 'react';
import { useMediaQuery } from 'react-responsive';
import classNames from 'classnames';
// Assets
import ForeFingerImg from '@/../assets/svg/main/forefinger.svg';
import QuitImg from '@/../assets/svg/main/quit.svg';
// Lib
import { Cookies } from '@lib/cookie/cookie-manage';
// Utils
import { Consts } from '@utils';
import { trackEvent } from '@utils/analytics';
// Enhance
import { enhance } from './enhance';
// Types
import { TBannerProps } from './types';

const { ENV } = Consts;

const redirectToOldSite = () => {
  const DOMAIN = ENV.API === 'https://api.chibbistest.ru' ? '.chibbistest.ru' : '.chibbis.ru';
  Cookies.set(
    'new-site',
    0,
    {
      expires: 30 * 24 * 60 * 60,
      domain: DOMAIN,
    },
    true,
  );
  window.location.reload();
};

const MoveToOldDesignBannerComponent: FC<TBannerProps> = ({ isFull, showFullBanner }) => {
  const isMobile = useMediaQuery({
    maxWidth: 768,
  });

  const onClose = useCallback(
    (e) => {
      e.stopPropagation();
      showFullBanner(false);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const onLinkClick = useCallback(
    (e) => {
      e.stopPropagation();
      showFullBanner(!isFull);
      if (isFull) {
        trackEvent('go-to-old-site');
        setTimeout(redirectToOldSite, 500);
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [isFull],
  );

  const getStyles = useCallback(
    () =>
      classNames(
        'chibbis-old-design-banner',
        `chibbis-old-design-banner-${isMobile ? 'mobile' : 'desktop'}-${isFull ? 'full' : 'short'}`,
      ),
    [isFull, isMobile],
  );

  const text = useMemo(() => {
    if (isMobile && !isFull) return '';

    if (!isMobile && !isFull) return 'Старая версия';

    return 'Старая версия сайта';
  }, [isFull, isMobile]);

  const wrapperStyles = useMemo(() => {
    if (isFull) {
      return {
        width: '100%',
        float: 'initial',
      };
    }

    return {
      width: 'min-content',
      float: 'left',
    };
  }, [isFull]);

  return (
    // @ts-ignore css-float options
    <div className="chibbis-old-design-banner__wrapper" style={wrapperStyles}>
      <div className={getStyles()} onClick={onLinkClick} role="presentation">
        <div className="chibbis-old-design-banner__img">
          <img src={ForeFingerImg} alt="link to old site" />
        </div>
        <div className="chibbis-old-design-banner__text">{text}</div>
      </div>
      {isFull && (
        <div className="chibbis-old-design-banner__close_img" onClickCapture={onClose}>
          <img src={QuitImg} alt="quit" />
        </div>
      )}
    </div>
  );
};

const MoveToOldDesignBannerMemo = memo(MoveToOldDesignBannerComponent);
export default enhance(MoveToOldDesignBannerMemo);
