// Main types
import { TShowFullOldSiteBanner } from '@actions/oldSiteBanner/action-types';

export interface TBannerProps {
  showFullBanner: TShowFullOldSiteBanner;
  isFull: boolean;
}
