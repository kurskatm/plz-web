// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { showFullBanner } from '@actions/oldSiteBanner';
// Selectors
import { oldSiteBannerSelector as mapStateToProps } from '@selectors/structured-selectors/oldSiteBanner';

const mapDispatchToProps = {
  showFullBanner,
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
