// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Selectors
import { profileOrderReviewDataSelector as mapStateToProps } from '@selectors/structured-selectors/profile';

const withConnect = connect(mapStateToProps);

export const enhance = compose(withConnect);
