import React, { FC, useCallback, memo } from 'react';
// import classNames from 'classnames';
import dayjs from 'dayjs';
// Utils
import { applyImageFallback } from '@/utils';
// Assets
import cardRestDefault from '@/../assets/svg/main/card-rest-default.svg';
// Types
import { TOrderHeaderProps } from '../types';

const OrderHeaderComponent: FC<TOrderHeaderProps> = ({
  title, cost, logo, date,
}) => {
  const getTime = useCallback(() => {
    const day = dayjs(date).locale('ru').format('DD MMMM');
    const time = dayjs(date).locale('ru').format('HH:mm');

    return `${day} в ${time}`;
  }, [date]);

  return (
    <div className="profile-order-header">
      <img
        src={logo || cardRestDefault}
        onError={applyImageFallback(cardRestDefault)}
        alt={title}
        className="profile-order-header__logo"
      />
      <div className="profile-order-header__row">
        <div className="profile-order-header__title">{title}</div>
        <div className="profile-order-header__cost">{`${cost} ₽`}</div>
      </div>
      <div className="profile-order-header__row">
        <div className="profile-order-header__date">{getTime()}</div>
      </div>
    </div>
  );
};

export default memo(OrderHeaderComponent);
