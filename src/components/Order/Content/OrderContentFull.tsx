import React, {
  FC, memo, useMemo, useCallback,
} from 'react';
import isNil from 'lodash/isNil';
// Lib
import { getPaymentType } from '@lib/profile/get-paymentType';
// Main types
import { TProductInfo } from '@type/profile';
// Modules Components
import { ScoresIcon } from 'chibbis-ui-kit';
// Types
import { TOrderProps, TModifiersCategoryDataArray } from '../types';

const OrderContentFullComponent: FC<TOrderProps> = ({ order }) => {
  const getPrice = useCallback((forBonus: boolean, price: number) => {
    if (forBonus) {
      return (
        <>
          {price}
          {' '}
          <ScoresIcon width={16} height={16} />
        </>
      );
    }

    return `${price} ₽`;
  }, []);

  const getModifiersNames = useCallback((modifiersGroups: TModifiersCategoryDataArray) => {
    const modifiersText: string[] = [];
    modifiersGroups.forEach(({ selectedModifiers }) => {
      selectedModifiers.forEach(({ name }) => {
        modifiersText.push(name);
      });
    });

    if (modifiersText.length) {
      const text = modifiersText.join(', ').toLowerCase().trim();
      return `(${text[0].toUpperCase() + text.slice(1)})`;
    }

    return '';
  }, []);

  const renderProducts = useMemo(
    () => (product: TProductInfo) => (
      <div
        className="profile-order-content-full__row"
        key={`${product.name} ${product.quantity} ${product.price}`}
      >
        <div className="profile-order-content-full__col profile-order-product">
          {`${product.quantity} × ${product.name} ${getModifiersNames(
            product.modifiers.modifiersGroups,
          )}`}
        </div>
        <div className="profile-order-content-full__col profile-order-price">
          {getPrice(product.paidByBonusPoints, product.priceWithModifiers)}
        </div>
      </div>
    ),
    [getPrice, getModifiersNames],
  );

  const getAddress = useCallback(() => {
    const { street, house, flat } = order.order.deliveryDetails.deliveryAddress;
    if (!isNil(street) && !isNil(house) && !isNil(flat)) return `${street} ${house}, кв. ${flat}`;

    if (!isNil(street) && !isNil(house) && isNil(flat)) return `${street} ${house}`;

    if (isNil(street) && !isNil(house) && !isNil(flat)) return `${house}, кв. ${flat}`;

    if (isNil(street) && !isNil(house) && isNil(flat)) return `${house}`;

    return `${street}` || `${house}`;
  }, [order.order.deliveryDetails]);

  const getPaymentValue = useCallback(() => {
    const { paymentType } = order.order;
    const { shortChange } = order.order.deliveryDetails;
    const result = getPaymentType(paymentType);
    if (!isNil(shortChange) && result === 'Наличными') {
      return `${result}, сдача с ${shortChange}`;
    }
    return result;
  }, [order.order]);

  // const getDiscount = useCallback(() => {
  //   if (!isNil(order.promoCode)) {
  //     const { promoCodeCoefficient } = order.promoCode;
  //     return `${promoCodeCoefficient} ₽`;
  //   }

  //   return '0 ₽';
  // }, [order.promoCode]);

  return (
    <div className="profile-order-content-full">
      <div className="profile-order-content-full__block">
        {order.order.products.map(renderProducts)}
        {/* <div className="profile-order-content-full__discount">
          <div className="profile-order-content-full__row">
            <div className="profile-order-content-full__col">Скидка по промокоду</div>
            <div className="profile-order-content-full__col">{getDiscount()}</div>
          </div>
        </div> */}
        <div className="profile-order-content-full__delivery">
          <div className="profile-order-content-full__row">
            <div className="profile-order-content-full__col">Доставка</div>
            <div className="profile-order-content-full__col">{`${order.order.orderCost.delivery} ₽`}</div>
          </div>
        </div>
        <div className="profile-order-content-full__bonus">
          <div className="profile-order-content-full__row">
            <div className="profile-order-content-full__col">Баллов за заказ</div>
            <div className="profile-order-content-full__col profile-order-price">
              +&ensp;
              {getPrice(true, order.order.bonusBalance.receivedAmount)}
            </div>
          </div>
        </div>
      </div>
      <div className="profile-order-content-full__block">
        <div className="profile-order-content-full__row" style={{ marginTop: 17 }}>
          <div className="profile-order-content-full__col">Количество приборов</div>
          <div className="profile-order-content-full__col">
            {order.order.deliveryDetails.personsCount}
          </div>
        </div>
      </div>
      <div className="profile-order-content-full__block">
        <div className="profile-order-content-full__title">
          {!order.order.deliveryDetails.selfPickup ? 'Адрес доставки' : 'Адрес самовывоза'}
        </div>
        <div className="profile-order-content-full__row">
          <div className="profile-order-content-full__col">{getAddress()}</div>
        </div>
      </div>
      {order.order.deliveryDetails.userComment?.length && (
        <div className="profile-order-content-full__block">
          <div className="profile-order-content-full__title">Комментарий к заказу</div>
          <div className="profile-order-content-full__row">
            <div className="profile-order-content-full__col">
              {order.order.deliveryDetails.userComment}
            </div>
          </div>
        </div>
      )}
      <div className="profile-order-content-full__block">
        <div className="profile-order-content-full__title">Способ оплаты</div>
        <div className="profile-order-content-full__row">
          <div className="profile-order-content-full__col">{getPaymentValue()}</div>
        </div>
      </div>
    </div>
  );
};

export default memo(OrderContentFullComponent);
