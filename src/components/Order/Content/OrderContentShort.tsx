import React, { FC, memo } from 'react';
// Types
import { TOrderShortContentProps } from '../types';

const OrderContentShortComponent: FC<TOrderShortContentProps> = ({ info }) => (
  <div className="order-content-short">
    <div className="order-content-short__info">{info}</div>
  </div>
);

export default memo(OrderContentShortComponent);
