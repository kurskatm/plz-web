// Main types
import { TProfileSetOrderReview } from '@actions/profile/action-types';

export interface TReviewBlockProps {
  orderId: string;
  restaurantId: string;
  dateAdded: string;
  profileSetOrderReview: TProfileSetOrderReview;
  setOrderReviewIsSuccess: boolean;
  setOrderReviewIsError: boolean;
  showTimeBlock: boolean;
  setShowTimeBlock: React.Dispatch<React.SetStateAction<boolean>>;
}
