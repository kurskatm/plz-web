// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { profileSetOrderReview } from '@actions/profile';
// Selectors
import { profileSetOrderReviewSelector as mapStateToProps } from '@selectors/structured-selectors/profile';

const mapDispatchToProps = {
  profileSetOrderReview,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
