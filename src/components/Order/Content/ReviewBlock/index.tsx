// Modules
import React, {
  FC, memo, useCallback, useState, useEffect, useMemo, useRef,
} from 'react';
import debounce from 'lodash/debounce';
import classNames from 'classnames';
import dayjs from 'dayjs';
import { Form } from 'react-final-form';
// Components
import {
  ScoresIcon,
  FlapperIcon,
  BigLikeIcon,
  BigDislikeIcon,
  InputField,
  Button,
} from 'chibbis-ui-kit';
// Lib
import { checkOfCorrectSentence, checkSymbolsInSentence } from '@lib/profile/order-review';
// Main Types
import { TOrderReview } from '@type/profile';
// Enhance
import { enhance } from './enahnce';
// Types
import { TReviewBlockProps } from './types';

const DEFAULT_ORDER_REVIEW: TOrderReview = {
  orderId: '',
  restaurantId: '',
  liked: false,
  taste: null,
  speed: null,
  service: null,
  cost: null,
  satisfiedByContent: true,
  text: '',
};

const OrderReviewBlockComponent: FC<TReviewBlockProps> = ({
  restaurantId,
  orderId,
  dateAdded,
  profileSetOrderReview,
  setOrderReviewIsSuccess,
  setShowTimeBlock,
  showTimeBlock,
}) => {
  const setProfileOrderReview = useRef(
    debounce((result: TOrderReview) => {
      profileSetOrderReview({
        review: result,
      });
    }, 50),
  );
  const [isLikesClicked, setLikesClicked] = useState<boolean>(false);
  const [likedValue, setLikedValue] = useState<number[]>([]);
  const [orderReview, setOrderReview] = useState<TOrderReview>(DEFAULT_ORDER_REVIEW);

  useEffect(
    () => {
      setLikesClicked(false);
      setShowTimeBlock(false);
      setLikedValue([]);
      setOrderReview(DEFAULT_ORDER_REVIEW);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [orderId],
  );

  useEffect(() => {
    if (isLikesClicked && orderReview !== DEFAULT_ORDER_REVIEW) {
      setProfileOrderReview.current(orderReview);
    }
  }, [isLikesClicked, orderReview]);

  const onLikesButtonClick = useCallback(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (event: any, value: number) => {
      event.stopPropagation();
      setLikesClicked(true);
      setLikedValue([value]);
    },
    [],
  );

  const getLikeButtonsStyles = useCallback(
    (value: number) =>
      classNames('order_review__likes_button', {
        order_review__likes_button_clicked: likedValue.includes(value),
      }),
    [likedValue],
  );

  const onSubmit = useCallback(
    (values) => {
      if (dayjs().diff(dayjs(dateAdded), 'day') > 27) {
        setShowTimeBlock(true);
      } else {
        setOrderReview({
          ...orderReview,
          orderId,
          restaurantId,
          text: values.comment.trim(),
          liked: !!likedValue.includes(1),
        });
      }
    },
    [likedValue, orderId, orderReview, restaurantId, dateAdded, setShowTimeBlock],
  );

  const onValidate = useCallback((values: { comment: string }) => {
    if (!values.comment || (values.comment && values.comment.trim().length < 25)) {
      return {
        comment: 'Комментарий должен быть не менее 25 символов',
      };
    }

    if (
      values.comment
      && (!checkOfCorrectSentence(values.comment, 4) || !checkSymbolsInSentence(values.comment))
    ) {
      return {
        comment: 'Укажите адекватный комментарий',
      };
    }

    if (values.comment.length > 100) {
      return {
        comment: 'Комментарий должен быть не более 100 символов',
      };
    }

    return undefined;
  }, []);

  const renderLikes = useMemo(() => {
    if (setOrderReviewIsSuccess) {
      return (
        <div className="order_review__gratitude">
          <div className="order_review__gratitude_inner">
            Спасибо за ответ
            <span role="img" aria-label="flapper">
              <FlapperIcon width={24} height={24} />
            </span>
          </div>
          <div className="order_review__gratitude_bonuses">
            Мы начислим вам
            <span role="img" aria-label="bonuses">
              <ScoresIcon />
              100 баллов
            </span>
          </div>
        </div>
      );
    }

    if (showTimeBlock) {
      return (
        <div className="order_review__time-is-up">
          Прошло 28 дней, и вы не оставили отзыв.
          <br />
          Нам жаль, но срок истёк
        </div>
      );
    }

    if (isLikesClicked) {
      return (
        <Form
          onSubmit={onSubmit}
          initialValues={{
            comment: '',
          }}
          validate={onValidate}
          render={({ pristine, invalid, handleSubmit }) => (
            <form onSubmit={handleSubmit}>
              <div className="order_review__likes">
                <div
                  className={getLikeButtonsStyles(1)}
                  role="presentation"
                  onClickCapture={(event) => onLikesButtonClick(event, 1)}
                >
                  <BigLikeIcon width={20} height={22} />
                  Понравилось
                </div>
                <div
                  className={getLikeButtonsStyles(2)}
                  role="presentation"
                  onClickCapture={(event) => onLikesButtonClick(event, 2)}
                >
                  <BigDislikeIcon width={20} height={22} />
                  Не понравилось
                </div>
              </div>
              <div className="order_review__likes_comment">
                <div className="order_review__likes_comment__title">Комментарий</div>
                <div className="order_review__likes_comment__input">
                  <InputField
                    clear
                    name="comment"
                    placeholder="Что понравилось, а что нет"
                    theme="grey"
                    maxLength={101}
                  />
                </div>
              </div>
              <div className="order_review__likes_submit_button">
                <Button
                  htmlType="submit"
                  size="small"
                  type="primary"
                  disabled={pristine || invalid}
                >
                  Отправить
                </Button>
              </div>
            </form>
          )}
        />
      );
    }

    return (
      <div className="order_review__likes">
        <div
          className={getLikeButtonsStyles(1)}
          role="presentation"
          onClick={(event) => onLikesButtonClick(event, 1)}
        >
          <BigLikeIcon width={20} height={22} />
          Понравилось
        </div>
        <div
          className={getLikeButtonsStyles(2)}
          role="presentation"
          onClick={(event) => onLikesButtonClick(event, 2)}
        >
          <BigDislikeIcon width={20} height={22} />
          Не понравилось
        </div>
      </div>
    );
  }, [
    setOrderReviewIsSuccess,
    isLikesClicked,
    showTimeBlock,
    getLikeButtonsStyles,
    onLikesButtonClick,
    onSubmit,
    onValidate,
  ]);

  return <div className="order_review__block">{renderLikes}</div>;
};

OrderReviewBlockComponent.defaultProps = {
  showTimeBlock: false,
  orderId: '',
  restaurantId: '',
};

const OrderReviewBlockMemo = memo(OrderReviewBlockComponent);
export default enhance(OrderReviewBlockMemo);
