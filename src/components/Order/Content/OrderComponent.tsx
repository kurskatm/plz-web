import React, {
  useCallback, FC, memo, useState,
} from 'react';
// Components
import OrderHeader from './OrderHeader';
import OrderContentShort from './OrderContentShort';
import OrderReviewBlock from './ReviewBlock';
// Types
import { TOrderProps } from '../types';

const OrderComponent: FC<TOrderProps> = ({ order }) => {
  const [showTimeBlock, setShowTimeBlock] = useState<boolean>(false);

  const getShortInfo = useCallback(() => {
    if (order?.order.products.length > 1) {
      return order?.order.products.map((item) => `${item.quantity} × ${item.name}`).join(', ');
    }

    return order?.order.products.map((item) => `${item.quantity} × ${item.name}`);
  }, [order?.order.products]);

  return (
    <div className="order">
      <div className="order__header">
        <OrderHeader
          title={order?.restaurant.restaurantName || ''}
          date={order?.order.addedOn}
          cost={order?.order.orderCost.total}
          logo={order?.restaurant.restaurantLogo || ''}
        />
      </div>
      <div className="order__content">
        {!showTimeBlock && <OrderContentShort info={getShortInfo()} />}
        <OrderReviewBlock
          orderId={order?.order.id}
          restaurantId={order?.restaurant.id}
          dateAdded={order?.order.addedOn}
          showTimeBlock={showTimeBlock}
          setShowTimeBlock={setShowTimeBlock}
        />
      </div>
    </div>
  );
};

const OrderMemo = memo(OrderComponent);
export default OrderMemo;
