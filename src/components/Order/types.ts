// Main types
import { TUserOrder, TModifiersCategoryDataArray } from '@type/profile';

export interface TOrderShortContentProps {
  info: string | string[];
}

export type TOrderProps = {
  order: TUserOrder;
};

export interface TOrderHeaderProps {
  title: string;
  cost: number;
  logo: string;
  date: string;
}

export { TModifiersCategoryDataArray };
