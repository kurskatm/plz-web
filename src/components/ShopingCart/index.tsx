// Modules
import React, {
  FC, memo, useCallback, useEffect, useMemo, useState, useRef,
} from 'react';
import equal from 'fast-deep-equal';
import { useLocation } from 'react-router';
import { useMediaQuery } from 'react-responsive';
// Lib
import { manageShopingCartSavingOnUpdate } from '@lib/shoping-cart/save-cart-on-update';
import { qaShopingCartSelector } from '@lib/qa/getShopingCartSelectors';
// Utils
import { usePrevious } from '@/utils/usePrevious';
import { Consts } from '@utils';
// Components
import ShopingCartContent from './CartContent';
import EmptyShopingCart from './EmptyCart';
// Enhance
import { enhance } from './enhance';
// Types
import { TShopingCartProps } from './types';

const { IS_CLIENT } = Consts.ENV;

const QA_PREFIX = 'shopingCart.divs.wrapper';

// ломается сср без этого TODO - отрефакторить
const ssrWindow = IS_CLIENT
  ? window
  : {
    // @ts-ignore
    addEventListener: () => null,
    // @ts-ignore
    removeEventListener: () => null,
    innerHeight: 0,
    innerWidth: 0,
  };

const areEqual = (prevProps: TShopingCartProps, nextProps: TShopingCartProps) =>
  prevProps.headerHeight === nextProps.headerHeight
  && equal(prevProps.shopingCart, nextProps.shopingCart);

const ShopingCartComponent: FC<TShopingCartProps> = ({
  headerHeight,
  profile,
  shopingCart,
  shopingCartWithNoApiStatuses,
  shopingCartIsPending,
  shopingCartReset,
  shopingCartSave,
  shopingCartUpdate,
  shopingCartUpdateCutlery,
  showShoppingCardModal,
  city,
  isModal = false,
  qa,
}) => {
  const prevProductsLength = usePrevious(shopingCart.products?.length);
  const location = useLocation();
  const isCheckout = useMemo(() => {
    const CheckoutPath = new RegExp('/[a-z]*/checkout');
    return location.pathname.match(CheckoutPath);
  }, [location]);

  const isMobile = useMediaQuery({
    maxWidth: 768,
  });

  const shopingCartRef = useRef(shopingCart);

  useEffect(
    () => {
      manageShopingCartSavingOnUpdate({
        shopingCart,
        prevProductsLength,
        profile,
        shopingCartReset,
        shopingCartSave,
      });
      shopingCartRef.current = shopingCart;
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [JSON.stringify(shopingCartWithNoApiStatuses)],
  );

  // Universal solution, suitable for Safari which doesn't support viewport units
  const getModalContentHeight = useCallback(() => {
    const modalWindowHeader = isMobile ? 32 : 0;
    const emptySpace = headerHeight + modalWindowHeader;
    const contentHeight = ssrWindow.innerHeight - emptySpace;
    return contentHeight;
  }, [headerHeight, isMobile]);
  const [modalContentHeight, setModalContentHeight] = useState<number>(getModalContentHeight);
  useEffect(() => {
    const updateModalContentSize = () => {
      setModalContentHeight(getModalContentHeight);
    };

    updateModalContentSize();
    ssrWindow.addEventListener('resize', updateModalContentSize);
    return () => {
      ssrWindow.removeEventListener('resize', updateModalContentSize);
    };
  }, [getModalContentHeight]);

  const getInlineMainStyles = useCallback(
    () => ({
      top: `${headerHeight}px`,
      // Universal solution, suitable for Safari which doesn't support viewport units
      height: `${modalContentHeight}px`,
    }),
    [modalContentHeight, headerHeight],
  );

  const isRestaurantValid = useMemo(() => !!shopingCart.restaurant && !!shopingCart.restaurant.id, [
    shopingCart.restaurant,
  ]);

  const getQaSelector = useMemo(() => qaShopingCartSelector(isModal, qa(`${QA_PREFIX}`)), [
    qa,
    isModal,
  ]);

  return (
    <div className="shopping-cart" data-qa={getQaSelector}>
      <div className="shopping-cart-view" style={getInlineMainStyles()}>
        {isRestaurantValid ? (
          <ShopingCartContent
            cityUrlName={city?.urlName}
            shopingCart={shopingCart}
            shopingCartUpdate={shopingCartUpdate}
            shopingCartUpdateCutlery={shopingCartUpdateCutlery}
            shopingCartIsPending={shopingCartIsPending}
            showShoppingCardModal={showShoppingCardModal}
            isCheckout={isCheckout}
            isModal={isModal}
          />
        ) : (
          <EmptyShopingCart />
        )}
      </div>
    </div>
  );
};

const ShopingCartMemo = memo(ShopingCartComponent, areEqual);
export default enhance(ShopingCartMemo);
