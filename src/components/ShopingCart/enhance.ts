// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { shopingCartUpdate, shopingCartUpdateCutlery } from '@actions/shoping-cart/change';
import { shopingCartSave } from '@actions/shoping-cart/save';
import { shopingCartReset } from '@/actions/shoping-cart';
import { showShoppingCardModal } from '@actions/restaurant/modal';
// Enhancers
import { withQa } from '@/enhancers/withQa';
// Selectors
import { shopingCartStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/shoping-cart';

const mapDispatchToProps = {
  shopingCartSave,
  shopingCartUpdate,
  shopingCartReset,
  shopingCartUpdateCutlery,
  showShoppingCardModal,
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withQa(), withConnect);
