import { TShopingCartOptions } from '@type/shoping-cart';
import { TShowShoppingCardModal } from '@actions/restaurant/modal/action-types';
import { TRestaurantCard } from '@type/restaurants/restaurant-card';
import { TQa } from '@/type/qa/props';

export interface TCartFooterProps {
  options?: TShopingCartOptions;
  shopingCartIsPending: boolean;
  cityUrlName: string;
  showShoppingCardModal: TShowShoppingCardModal;
  isCheckout: RegExpMatchArray;
  restaurantCard: TRestaurantCard;
  isModal: boolean;
  qa: TQa;
  saveDetails: {
    errorMessage: string;
  };
}
