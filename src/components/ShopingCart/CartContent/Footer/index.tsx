/* eslint-disable max-len */
// Modules
import React, {
  FC, memo, useMemo, useContext, CSSProperties,
} from 'react';
import { useHistory } from 'react-router-dom';
import { isMobile } from 'react-device-detect';
import dayjs from 'dayjs';
import { isNil, isBoolean } from 'lodash';
// Modules Components
import { Button } from 'chibbis-ui-kit';
// Assets
import DeliveryTrack from '@/../assets/svg/restaurant/delivery-track.svg';
import Meal from '@/../assets/svg/restaurant/meal.svg';
import PadLock from '@/../assets/img/restaurant/padlock.png';
// Lib
import { ShopingCartFooterContext } from '@lib/shoping-cart/context';
import { qaShopingCartSelector } from '@lib/qa/getShopingCartSelectors';
// Enhance
import { enhance } from './enhance';
// Types
import { TCartFooterProps } from './types';

const QA_PREFIX_DIV = 'shopingCart.divs.footer';
const QA_PREFIX_VALUES = 'shopingCart.values';

const CartFooterComponent: FC<TCartFooterProps> = ({
  options,
  shopingCartIsPending,
  cityUrlName,
  showShoppingCardModal,
  isCheckout,
  restaurantCard,
  isModal,
  qa,
  saveDetails,
}) => {
  const { isPushed }: { isPushed: boolean } = useContext(ShopingCartFooterContext);
  const history = useHistory();
  const clickHandler = () => {
    history.push(`/${cityUrlName}/checkout`);
    showShoppingCardModal(false);
  };

  const {
    deliveryCost,
    discount,
    total,
    minimalOrderCost,
    minOrderCostForFreeDelivery,
    isOpen,
    openingDate,
    averageDeliveryTimeMinutes,
  } = options;
  const getFooterSelector = useMemo(() => qaShopingCartSelector(isModal, qa(`${QA_PREFIX_DIV}`)), [
    qa,
    isModal,
  ]);
  const getQaTotalSelector = useMemo(
    () => qaShopingCartSelector(isModal, qa(`${QA_PREFIX_VALUES}.total`)),
    [qa, isModal],
  );

  const getQaDeliverySelector = useMemo(
    () => qaShopingCartSelector(isModal, qa(`${QA_PREFIX_VALUES}.delivery`)),
    [qa, isModal],
  );

  const hasNotEnoughForMinCostBoolean = useMemo(() => total < minimalOrderCost, [
    minimalOrderCost,
    total,
  ]);

  const hasNotEnoughForMinCost = useMemo(() => {
    if (hasNotEnoughForMinCostBoolean && isOpen) {
      return (
        <div className="shoping-cart-footer__warning-message_min-cost">
          <img src={Meal} alt="delivery" />
          <div>{`Добавьте еще на ${minimalOrderCost - total} ₽`}</div>
        </div>
      );
    }

    return null;
  }, [hasNotEnoughForMinCostBoolean, isOpen, minimalOrderCost, total]);

  const hasDeliveryCost = useMemo(() => {
    if (!hasNotEnoughForMinCostBoolean && isOpen) {
      return (
        <div className="shoping-cart-footer__warning-message_delivery-info">
          <div data-qa={getQaDeliverySelector}>
            {`Доставка ${deliveryCost > 0 ? `${deliveryCost} ₽` : 'бесплатно'}`}
          </div>
          <div className="shoping-cart-footer__warning-message_delivery-time">{`~ ${averageDeliveryTimeMinutes} мин`}</div>
        </div>
      );
    }
    return null;
  }, [
    deliveryCost,
    averageDeliveryTimeMinutes,
    getQaDeliverySelector,
    isOpen,
    hasNotEnoughForMinCostBoolean,
  ]);

  const hasNotEnoughForDelivery = useMemo(() => {
    const totalOrder = total - deliveryCost;
    if (
      deliveryCost > 0
      && !isNil(minOrderCostForFreeDelivery)
      && totalOrder < minOrderCostForFreeDelivery
      && !hasNotEnoughForMinCostBoolean
      && isOpen
    ) {
      return (
        <div className="shoping-cart-footer__warning-message_delivery">
          <img src={DeliveryTrack} alt="delivery" />
          {`Ещё ${minOrderCostForFreeDelivery - totalOrder} ₽ и будет бесплатно`}
        </div>
      );
    }

    return null;
  }, [minOrderCostForFreeDelivery, deliveryCost, isOpen, hasNotEnoughForMinCostBoolean, total]);

  const restaurantIsClosed = useMemo(() => {
    if (!isOpen && openingDate) {
      const openDateWithoutTZ = openingDate.replace(/\+\d\d:\d\d/, '');
      const formattedTime = dayjs(openDateWithoutTZ).format('HH:mm');
      return (
        <div className="shoping-cart-footer__warning-message_rest-closed">
          <div>
            <img src={PadLock} alt="delivery" />
            Ресторан закрыт.
          </div>
          <div>{`Откроется в ${formattedTime}`}</div>
        </div>
      );
    }

    if ((!isOpen && !openingDate && !saveDetails.errorMessage) || restaurantCard?.status === 2) {
      return (
        <div className="shoping-cart-footer__warning-message_rest-closed">
          <div>
            <img src={PadLock} alt="delivery" />
            Ресторан закрыт
          </div>
        </div>
      );
    }

    return null;
  }, [isOpen, openingDate, restaurantCard?.status, saveDetails.errorMessage]);

  const restauranNotDeliverToThisAddress = useMemo(() => {
    if (saveDetails.errorMessage) {
      return (
        <div className="shoping-cart-footer__warning-message_rest-closed">
          <div>
            <img src={PadLock} alt="delivery" />
            Ресторан не доставляет по этому адресу
          </div>
        </div>
      );
    }

    return null;
  }, [saveDetails.errorMessage]);

  const isSubmitDisabled = useMemo(
    () =>
      (isBoolean(isOpen) && !isOpen) || restaurantCard?.status === 2 || total < minimalOrderCost,
    [minimalOrderCost, total, isOpen, restaurantCard?.status],
  );

  const getFooterStyles = useMemo(() => {
    if (isPushed) {
      const styles: CSSProperties = {
        position: 'relative',
        bottom: 36,
        marginTop: 36,
      };
      return styles;
    }

    return undefined;
  }, [isPushed]);

  return (
    <>
      {isCheckout && (
        <div className="shoping-cart-footer__checkout-info">
          <div className="shoping-cart-footer__checkout-info__row">
            <div>Сумма заказа</div>
            <div>
              {total - deliveryCost}
               ₽
            </div>
          </div>
          {discount > 0 && (
            <div className="shoping-cart-footer__checkout-info__row">
              <div>Скидка</div>
              <div>
                −
                {discount}
                 ₽
              </div>
            </div>
          )}
          <div className="shoping-cart-footer__checkout-info__row">
            <div>Доставка</div>
            <div>
              {deliveryCost}
               ₽
            </div>
          </div>
          <div className="shoping-cart-footer__checkout-info__row">
            <div>Итого к оплате</div>
            <div className="shoping-cart-footer__checkout-info__cell-bold">
              {total - discount}
               ₽
            </div>
          </div>
        </div>
      )}
      {(!isCheckout || isMobile) && (
        <div className="shoping-cart-footer" data-qa={getFooterSelector} style={getFooterStyles}>
          {hasDeliveryCost}
          {hasNotEnoughForDelivery}
          {hasNotEnoughForMinCost}
          {restaurantIsClosed}
          {restauranNotDeliverToThisAddress}
          {isOpen ? (
            <div className="shoping-cart-footer__submit-button">
              <Button
                onClick={clickHandler}
                htmlType="button"
                loading={shopingCartIsPending}
                useSpinner
                disabled={isSubmitDisabled}
              >
                {!shopingCartIsPending ? (
                  <div className="shoping-cart-footer__submit-button__content">
                    <div>Заказать </div>
                    <div
                      className="shoping-cart-footer__submit-button__content__total"
                      data-qa={getQaTotalSelector}
                    >
                      {`${total} ₽`}
                    </div>
                  </div>
                ) : undefined}
              </Button>
            </div>
          ) : null}
        </div>
      )}
    </>
  );
};

const CartFooterMemo = memo(CartFooterComponent);
export default enhance(CartFooterMemo);
