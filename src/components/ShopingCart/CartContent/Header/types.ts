// Main Types
import { TShopingCartModel } from '@models/shoping-cart/types';
import { TQa } from '@/type/qa/props';

export interface TCartHeaderProps {
  shopingCart: TShopingCartModel;
  isModal: boolean;
  qa: TQa;
}
