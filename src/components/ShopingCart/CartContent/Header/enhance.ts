// Modules
import { compose } from 'redux';
// Enhancers
import { withQa } from '@/enhancers/withQa';

export const enhance = compose(withQa());
