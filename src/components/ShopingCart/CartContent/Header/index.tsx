// Modules
import React, { FC, memo, useMemo } from 'react';
import { useMediaQuery } from 'react-responsive';
// Lib
import { qaShopingCartSelector } from '@lib/qa/getShopingCartSelectors';
// Utils
import { applyImageFallback } from '@/utils';
// Assets
import cardRestDefault from '@/../assets/svg/main/card-rest-default.svg';
// Enhance
import { enhance } from './enhance';
// Types
import { TCartHeaderProps } from './types';

const QA_PREFIX = 'shopingCart.divs';

const CartHeaderComponent: FC<TCartHeaderProps> = ({ shopingCart, isModal, qa }) => {
  const isTabletOrMobile = useMediaQuery({
    query: '(max-width: 768px)',
  });

  const getQaSelector = useMemo(() => qaShopingCartSelector(isModal, qa(`${QA_PREFIX}.header`)), [
    qa,
    isModal,
  ]);

  if (isTabletOrMobile) {
    return (
      <div className="shoping-cart-header-block" data-qa={getQaSelector}>
        <div className="shoping-cart-header-logo">
          <img
            src={shopingCart.restaurant.logo || shopingCart.restaurant.logoUrl || cardRestDefault}
            onError={applyImageFallback(cardRestDefault)}
            alt={shopingCart.restaurant.name}
          />
        </div>
        <div className="shoping-cart-header-left">
          <div className="shoping-cart-header-left__description">Заказ в ресторане</div>
          <div className="shoping-cart-header-left__name">{shopingCart.restaurant.name}</div>
        </div>
      </div>
    );
  }

  return (
    <div className="shoping-cart-header-block" data-qa={getQaSelector}>
      <div className="shoping-cart-header-left">
        <div className="shoping-cart-header-left__description">Заказ в ресторане</div>
        <div className="shoping-cart-header-left__name">{shopingCart.restaurant.name}</div>
      </div>

      <div className="shoping-cart-header-logo">
        <img
          src={shopingCart.restaurant.logo || shopingCart.restaurant.logoUrl || cardRestDefault}
          onError={applyImageFallback(cardRestDefault)}
          alt={shopingCart.restaurant.name}
        />
      </div>
    </div>
  );
};

const CartHeaderMemo = memo(CartHeaderComponent);
export default enhance(CartHeaderMemo);
