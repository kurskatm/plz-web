// Modules
import React, { FC, memo } from 'react';
import classnames from 'classnames';
// Images
import HourGlass from '@/../assets/svg/restaurant/hour-glass.svg';
import { currTimeToTicks } from '@lib/restaurants/getHoursAndMinutes';
// Components
import CartHeader from './Header';
import CartBody from './Body';
import CartFooter from './Footer';
// Enhance
import { enhance } from './enhance';
// Types
import { TShopingCartContentProps } from './types';

const TEN_MINUTES = 6000000000;

const ShopingCartContentComponent: FC<TShopingCartContentProps> = ({
  shopingCart,
  shopingCartUpdate,
  shopingCartUpdateCutlery,
  shopingCartIsPending,
  cityUrlName,
  showShoppingCardModal,
  isCheckout,
  isModal,
  restaurantCard,
}) => {
  const closeTime = restaurantCard?.workingTime?.closeTime || 0;

  const isOpen = restaurantCard?.isOpen || false;
  const currTime = currTimeToTicks();
  const renderCloseSoonWarning = closeTime > currTime
    // @ts-ignore
    && closeTime - currTime < TEN_MINUTES
    && isOpen;

  return (
    <div
      className={classnames('shoping-cart-content', {
        'shoping-cart-content__no-close-soon-warning': !renderCloseSoonWarning,
      })}
    >
      <CartHeader shopingCart={shopingCart} isModal={isModal} />
      {renderCloseSoonWarning && (
        <div className="shoping-cart-content__close-soon">
          <img src={HourGlass} alt="hourGlass" />
          Ресторан скоро закроется
        </div>
      )}
      <CartBody
        shopingCart={shopingCart}
        shopingCartUpdate={shopingCartUpdate}
        shopingCartUpdateCutlery={shopingCartUpdateCutlery}
        shopingCartIsPending={shopingCartIsPending}
        isModal={isModal}
      />
      <CartFooter
        cityUrlName={cityUrlName}
        shopingCartIsPending={shopingCartIsPending}
        showShoppingCardModal={showShoppingCardModal}
        isCheckout={isCheckout}
        isModal={isModal}
        saveDetails={shopingCart.save.details}
      />
    </div>
  );
};

const CartContentMemo = memo(ShopingCartContentComponent);
export default enhance(CartContentMemo);
