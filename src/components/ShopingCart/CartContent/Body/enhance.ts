// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Enhancers
import { withQa } from '@/enhancers/withQa';

import { shopingCartBodyStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/shoping-cart';

const withConnect = connect(mapStateToProps, null);
export const enhance = compose(withQa(), withConnect);
