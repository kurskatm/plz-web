// Main Types
import { TShopingCartProduct } from '@type/shoping-cart';

export interface TCartProductProps {
  item: TShopingCartProduct;
  shopingCartIsPending: boolean;
  updateShopingCart: (quantity: number) => void;
}
