/* eslint-disable no-return-assign */
// Modules
import React, {
  FC, memo, useCallback, useMemo,
} from 'react';
import classnames from 'classnames';
// Modules Components
import { Amount, ScoresIcon, Button } from 'chibbis-ui-kit';
// Types
import { TCartProductProps } from './types';

const CartProductComponent: FC<TCartProductProps> = ({
  item,
  shopingCartIsPending,
  updateShopingCart,
}) => {
  const getResultCoast = useCallback(() => {
    let modifiersCost = 0;
    if (item?.modifiers?.length) {
      modifiersCost = item.modifiers
        .reduce((acc, val) => [...acc, ...val.modifiers], [])
        .reduce((acc, modifier) => (acc += modifier.price), 0);
    }

    return item.inBonusPoint ? item.price : `${(item.price + modifiersCost) * item.count} ₽`;
  }, [item]);

  const pendingMemo = useMemo(() => ({ isPending: shopingCartIsPending, useSpinner: true }), [
    shopingCartIsPending,
  ]);

  const renderModifiers = useCallback(() => {
    const modifiersNames: string[] = [];

    if (item.modifiers?.length) {
      item.modifiers.forEach((category) =>
        category.modifiers.forEach((modifier) => modifiersNames.push(modifier.name)));
    }

    const modifierText = modifiersNames.length > 1
      ? modifiersNames.join(', ').toLowerCase()
      : modifiersNames[0].toLowerCase();

    const finalText = `${modifierText.slice(0, 1).toUpperCase()}${modifierText.slice(1)}`;

    return finalText;
  }, [item.modifiers]);

  return (
    <div
      className={classnames('shoping-cart-content-product', {
        'shoping-cart-content-product__bonus-point': item.inBonusPoint,
      })}
    >
      {item.inBonusPoint && <div className="shoping-cart-content-product__ribbon">бесплатно</div>}
      <div
        className={classnames('shoping-cart-content-product__top', {
          'shoping-cart-content-product__top__bonus-point': item.inBonusPoint,
        })}
      >
        <div className="shoping-cart-content-product__name">{item.name}</div>

        {!!item.modifiers?.length && (
          <div className="shoping-cart-content-product__modifiers-description">
            {renderModifiers()}
          </div>
        )}
      </div>

      <div className="shoping-cart-content-product__bottom">
        <div className="shoping-cart-content-product__coast">
          <div className="shoping-cart-content-product__coast-price">{getResultCoast()}</div>
          {item.inBonusPoint && (
            <ScoresIcon className="shoping-cart-content-product__coast-points" />
          )}
        </div>

        {!item.inBonusPoint && (
          <div className="shoping-cart-content-product__amount">
            <Amount
              key={`${item.id}-${item.count}`}
              name={item.id}
              onChange={updateShopingCart}
              value={item.count}
              contentPending={pendingMemo}
              noInput
            />
          </div>
        )}

        {item.inBonusPoint && (
          <div className="shoping-cart-content-product__amount">
            <Button
              htmlType="button"
              size="small"
              type="secondary"
              onClick={() => updateShopingCart(0)}
              className="shoping-cart-content-product__bonus-delete-button"
            >
              Убрать
            </Button>
          </div>
        )}
      </div>
    </div>
  );
};

export default memo(CartProductComponent);
