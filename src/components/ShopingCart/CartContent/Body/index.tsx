// Modules
import React, {
  FC, memo, useCallback, useMemo,
} from 'react';
// Modules Components
import { Amount, ScoresIcon } from 'chibbis-ui-kit';
// Main Types
import { TShopingCartProduct } from '@type/shoping-cart';
// Lib
import { qaShopingCartSelector } from '@lib/qa/getShopingCartSelectors';
// Utils
import { useShopingCartUpdate } from '@utils';
// Components
import CartProduct from './Product';
// Enhance
import { enhance } from './enhance';
// Types
import { TCartBodyProps } from './types';

const QA_PREFIX = 'shopingCart.divs';

const CartBodyComponent: FC<TCartBodyProps> = ({
  shopingCart,
  shopingCartUpdate,
  shopingCartUpdateCutlery,
  shopingCartIsPending,
  isModal,
  qa,
  city,
  profile,
}) => {
  const onChangeCutlery = useCallback(
    (quantity: string) => {
      shopingCartUpdateCutlery(+quantity);
    },
    [shopingCartUpdateCutlery],
  );

  const [updateShopingCart] = useShopingCartUpdate({
    city,
    shopingCart,
    shopingCartUpdate,
    profile,
    configName: 'shopingCart',
  });

  const renderItem = useMemo(
    () => (item: TShopingCartProduct, index: number) => (
      <CartProduct
        key={`${item.id} ${index}`}
        item={item}
        shopingCartIsPending={shopingCartIsPending}
        updateShopingCart={updateShopingCart(item)}
      />
    ),
    [shopingCartIsPending, updateShopingCart],
  );

  const pendingMemo = useMemo(() => ({ isPending: shopingCartIsPending, useSpinner: true }), [
    shopingCartIsPending,
  ]);

  const getQaContentSelector = useMemo(
    () => qaShopingCartSelector(isModal, qa(`${QA_PREFIX}.content`)),
    [qa, isModal],
  );

  const getQaPointsSelector = useMemo(
    () => qaShopingCartSelector(isModal, qa(`${QA_PREFIX}.points`)),
    [qa, isModal],
  );

  const { bonusReward } = shopingCart.options;

  return (
    <div className="shoping-cart-content-block" data-qa={getQaContentSelector}>
      {shopingCart.products.map(renderItem)}
      <div className="shoping-cart-content-cutlery">
        <div className="shoping-cart-content-cutlery__icon" />
        <div className="shoping-cart-content-cutlery__title">Приборы</div>

        <div className="shoping-cart-content-cutlery__amount">
          <Amount
            name={`cutlery-${shopingCart.restaurant.id}`}
            onChange={onChangeCutlery}
            value={shopingCart?.options?.cutlery || 1}
            contentPending={pendingMemo}
            noInput
          />
        </div>
      </div>

      <div className="shoping-cart-content-block__bonus-reward">
        {bonusReward > 0 && (
          <>
            <div className="shoping-cart-content-block__bonuses" data-qa={getQaPointsSelector}>
              {`Будет начислено +${bonusReward}`}
            </div>
            <ScoresIcon className="shoping-cart-content-block__icon" />
          </>
        )}
      </div>
    </div>
  );
};

const CardBodyMemo = memo(CartBodyComponent);
export default enhance(CardBodyMemo);
