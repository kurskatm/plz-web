// Main Types
import {
  TUpdateShopingCart,
  TUpdateShopingCartCutlery,
} from '@actions/shoping-cart/change/action-types';
import { TShopingCartModel } from '@models/shoping-cart/types';
import { TQa } from '@/type/qa/props';
import { TProfileModel } from '@models/profile/types';
import { TCitiesItem } from '@type/cities';

export interface TCartBodyProps {
  shopingCart: TShopingCartModel;
  shopingCartUpdate: TUpdateShopingCart;
  shopingCartUpdateCutlery: TUpdateShopingCartCutlery;
  shopingCartIsPending: boolean;
  isModal: boolean;
  qa: TQa;
  city?: TCitiesItem;
  profile: TProfileModel;
}
