// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Enhancers
import { withQa } from '@/enhancers/withQa';
// Selectors
import { shopingCartContentStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/shoping-cart';

const mapDispatchToProps = {};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withQa(), withConnect);
