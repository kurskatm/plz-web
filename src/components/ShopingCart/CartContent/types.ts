// Main Types
import {
  TUpdateShopingCart,
  TUpdateShopingCartCutlery,
} from '@actions/shoping-cart/change/action-types';
import { TShowShoppingCardModal } from '@actions/restaurant/modal/action-types';
import { TShopingCartModel } from '@models/shoping-cart/types';
import { TRestaurantCard } from '@type/restaurants/restaurant-card';

export interface TShopingCartContentProps {
  shopingCart: TShopingCartModel;
  shopingCartUpdate: TUpdateShopingCart;
  shopingCartUpdateCutlery: TUpdateShopingCartCutlery;
  showShoppingCardModal: TShowShoppingCardModal;
  shopingCartIsPending: boolean;
  cityUrlName: string;
  isCheckout: RegExpMatchArray;
  isModal: boolean;
  restaurantCard: TRestaurantCard;
}
