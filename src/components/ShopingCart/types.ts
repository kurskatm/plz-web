// Main Types
import { TShowShoppingCardModal } from '@actions/restaurant/modal/action-types';
import {
  TUpdateShopingCart,
  TUpdateShopingCartCutlery,
} from '@actions/shoping-cart/change/action-types';
import { TShopingCartSave } from '@actions/shoping-cart/save/action-types';
import {
  TShopingCartModel,
  TRestaurantShopingCart,
  TShopingCartProduct,
} from '@models/shoping-cart/types';
import { TProfileModel } from '@models/profile/types';
import { TShopingCartReset } from '@/actions/shoping-cart/action-types';
import { TCitiesItem } from '@/type/cities';
import { TQa } from '@/type/qa/props';

export interface TShopingCartProps {
  headerHeight: number;
  profile: TProfileModel;
  shopingCart: TShopingCartModel;
  shopingCartWithNoApiStatuses: {
    restaurant: TRestaurantShopingCart;
    products: TShopingCartProduct[];
    cutlery: number;
  };
  shopingCartIsPending: boolean;
  shopingCartRestaurantIsPending: boolean;
  shopingCartReset: TShopingCartReset;
  shopingCartSave: TShopingCartSave;
  shopingCartUpdate: TUpdateShopingCart;
  shopingCartUpdateCutlery: TUpdateShopingCartCutlery;
  showShoppingCardModal: TShowShoppingCardModal;
  city: TCitiesItem;
  isModal?: boolean;
  qa: TQa;
}
