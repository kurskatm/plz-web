// Modules
import React, { FC, memo } from 'react';

const EmptyShopingCartComponent: FC = () => (
  <div className="empty-shoping-cart">
    <div className="empty-shoping-cart__icon" />

    <div className="empty-shoping-cart__description">В корзине пусто</div>
  </div>
);

export default memo(EmptyShopingCartComponent);
