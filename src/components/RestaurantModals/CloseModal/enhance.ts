// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { closeRestaurantModalShow } from '@actions/restaurant/modal';
// Selectores
import { restaurantModalShowStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/restaurant/modal';

const mapDispatchToProps = {
  closeRestaurantModalShow,
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
