// Modules
import React, {
  FC, memo, useMemo, useCallback,
} from 'react';
import { Link } from 'react-router-dom';
// Main types
import { TRestaurantCard } from '@type/restaurants/restaurant-card';
// Enhance
import { enhance } from './enhance';
// Types
import { TRestaurantsLinksProps } from './types';

const RestaurantsLinksComponent: FC<TRestaurantsLinksProps> = ({ rests, city }) => {
  const getLink = useCallback((name: string) => `/${city}/restaurant/${name}` || '/', [city]);

  const renderCount = useCallback(() => {
    if (rests.length === 1) {
      return '1 ресторан';
    }

    if (rests.length > 1 || rests.length < 5) {
      return `${rests.length} ресторана`;
    }

    return `${rests.length} ресторанов`;
  }, [rests]);

  const renderLink = useMemo(
    () => (item: TRestaurantCard) => (
      <Link to={getLink(item.urlName)} key={item.id}>
        <div className="restaurant-close-modal__rest-logo">
          <img src={item.logo} alt="logo" />
        </div>
      </Link>
    ),
    [getLink],
  );

  return (
    <div className="restaurant-close-modal__links-body">
      <div className="restaurant-close-modal__links-count">
        {`${renderCount()} готовы принять заказ`}
      </div>
      <div className="restaurant-close-modal__links-rests">{rests.slice(0, 5).map(renderLink)}</div>
    </div>
  );
};

const RestaurantsLinksMemo = memo(RestaurantsLinksComponent);
export default enhance(RestaurantsLinksMemo);
