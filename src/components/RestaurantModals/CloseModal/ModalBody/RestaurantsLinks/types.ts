// Main types
import { TRestaurantCard } from '@type/restaurants/restaurant-card';

export interface TRestaurantsLinksProps {
  rests: TRestaurantCard[];
  city: string;
}
