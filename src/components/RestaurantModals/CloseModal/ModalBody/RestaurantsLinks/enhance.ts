// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Selectores
import { restaurantModalBodyStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/restaurant/modal';

const withConnect = connect(mapStateToProps);

export const enhance = compose(withConnect);
