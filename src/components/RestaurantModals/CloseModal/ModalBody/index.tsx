// Modules
import React, { FC, memo } from 'react';
import { Link } from 'react-router-dom';
// Module Components
import { Button } from 'chibbis-ui-kit';
// Utils
import { Consts } from '@utils';
// Components
import RestaurantLinks from './RestaurantsLinks';
// Types
import { TModalBodyProps } from './types';

const { PATH } = Consts.ROUTES.CITIES;

const RestaurantCloseModalBodyComponent: FC<TModalBodyProps> = ({ openingTime }) => (
  <div className="restaurant-close-modal__body">
    <div className="restaurant-close-modal__header">
      Ресторан закрылся
      <br />
      и не принимает заказы
    </div>
    <div className="restaurant-close-modal__close-time">{`Откроется завтра в ${openingTime}`}</div>
    <div className="restaurant-close-modal__links">
      <RestaurantLinks />
    </div>
    <div className="restaurant-close-modal__button">
      <Link to={PATH}>
        <Button htmlType="button" size="small" type="secondary">
          Посмотреть все
        </Button>
      </Link>
    </div>
  </div>
);

const RestaurantCloseModalBodyMemo = memo(RestaurantCloseModalBodyComponent);
export default RestaurantCloseModalBodyMemo;
