// Main types
import { TCloseRestaurantModalShow } from '@actions/restaurant/modal/action-types';

export interface TRestaurantCloseModalProps {
  closeRestaurantModalShow: TCloseRestaurantModalShow;
  modalShow: boolean;
  openingTime: string;
}
