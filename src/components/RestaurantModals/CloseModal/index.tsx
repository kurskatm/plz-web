// Modules
import React, { FC, memo, useCallback } from 'react';
import { ModalWindow } from 'chibbis-ui-kit';
import ModalBody from './ModalBody';
// Enhance
import { enhance } from './enhance';
// Types
import { TRestaurantCloseModalProps } from './types';

const RestaurantCloseModalComponent: FC<TRestaurantCloseModalProps> = ({
  modalShow,
  closeRestaurantModalShow,
  openingTime,
}) => {
  const closeModal = useCallback((): unknown => closeRestaurantModalShow(false), [
    closeRestaurantModalShow,
  ]);

  return (
    <ModalWindow
      classNameWrapper="restaurant-close-modal"
      closable
      setVisible={closeModal}
      visible={modalShow}
    >
      <ModalBody openingTime={openingTime} />
    </ModalWindow>
  );
};

const RestaurantCloseModalMemo = memo(RestaurantCloseModalComponent);
export default enhance(RestaurantCloseModalMemo);
