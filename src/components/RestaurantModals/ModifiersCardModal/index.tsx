// Modules
import React, { FC, memo, useCallback } from 'react';
import { ModalWindow } from 'chibbis-ui-kit';
import classNames from 'classnames';
// Components
import ModalBody from './ModalBody';
// Enhance
import { enhance } from './enhance';
// Types
import { TModifiersCardModalProps } from './types';

const RestaurantModifiersCardModalComponent: FC<TModifiersCardModalProps> = ({
  showModal,
  showOtherRestaurantModals,
  showModifiersCardModal,
  product,
}) => {
  const closeModal = useCallback(
    (): unknown =>
      showModifiersCardModal({
        showModal: false,
        productId: null,
        inBonusPoint: false,
      }),
    [showModifiersCardModal],
  );

  const getMainStyles = useCallback(
    () =>
      classNames('restaurant-modifiers-card-modal', {
        'restaurant-modifiers-card-modal-deeper': showOtherRestaurantModals,
      }),
    [showOtherRestaurantModals],
  );

  return (
    <ModalWindow classNameWrapper={getMainStyles()} setVisible={closeModal} visible={showModal}>
      <ModalBody product={product} closeModal={closeModal} />
    </ModalWindow>
  );
};

const RestaurantModifiersCardModalMemo = memo(RestaurantModifiersCardModalComponent);
export default enhance(RestaurantModifiersCardModalMemo);
