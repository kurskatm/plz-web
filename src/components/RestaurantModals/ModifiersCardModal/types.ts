// Main types
import { TShowModifiersCardModal } from '@actions/restaurant/modal/action-types';
import { TRestaurantProductModifiers } from '@type/restaurants/restaurant-products';

export interface TModifiersCardModalProps {
  showModifiersCardModal: TShowModifiersCardModal;
  showModal: boolean;
  showOtherRestaurantModals: boolean;
  product: TRestaurantProductModifiers;
}
