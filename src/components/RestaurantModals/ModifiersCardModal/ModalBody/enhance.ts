// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { shopingCartUpdate } from '@actions/shoping-cart/change';
import { restaurantCardIdSave } from '@/actions/restaurant/main/id';
import { showRestaurantModals } from '@actions/restaurant/modal';
import { authModalShow } from '@actions/auth';
// Selectores
import { modifiersModalBodyStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/restaurant/modal';

const mapDispatchToProps = {
  shopingCartUpdate,
  restaurantCardIdSave,
  showRestaurantModals,
  authModalShow,
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
