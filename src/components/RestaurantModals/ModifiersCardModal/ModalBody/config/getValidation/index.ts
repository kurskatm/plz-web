// Types
import { isNil } from 'lodash';
import { TGetFormValidation, TErrors } from './types';

export const getFormValidation: TGetFormValidation = (values, product) => {
  const errors: TErrors = {};

  if (!isNil(product)) {
    product.modifiers.forEach((category) => {
      if (
        category.minSelectedModifiers === 1
        && category.maxSelectedModifiers === 1
        && category.required
      ) {
        const radioCategory = `radio-category-${category.id}`;
        if (!values[radioCategory]?.length) {
          errors[radioCategory] = 'Следует выбрать опцию';
        }
      }

      if (category.maxSelectedModifiers > 1) {
        const checkboxCategory = `checkbox-category-${category.id}`;

        if (values[checkboxCategory]?.length < category.minSelectedModifiers) {
          errors[checkboxCategory] = 'Не все опции выбраны';
        }
      }
    });
  }

  if (Number(values.amount) === 0 && !product?.availableInBonusPoints) {
    errors.amount = 'Должно быть больше 0';
  }

  return errors;
};
