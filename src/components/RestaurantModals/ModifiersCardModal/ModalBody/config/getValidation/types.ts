// Main types
import { TRestaurantProductModifiers } from '@type/restaurants/restaurant-products';

export interface TValues {
  amount: string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [propName: string]: any;
}

export interface TErrors {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [propName: string]: any;
}

export type TGetFormValidation = (values: TValues, product: TRestaurantProductModifiers) => TErrors;
