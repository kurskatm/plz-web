/* eslint-disable @typescript-eslint/indent */
// Modules
import isNil from 'lodash/isNil';
// Types
import { TGetPrice } from './types';

export const getPrice: TGetPrice = (formValues, modifiers, basePrice) => {
  if (modifiers.length) {
    let newPrice: number = basePrice;
    modifiers.forEach((category) => {
      const radioCategory = `radio-category-${category.id}`;
      const checkboxCategory = `checkbox-category-${category.id}`;

      if (!isNil(formValues[radioCategory]) && formValues[radioCategory].length) {
        const currentModifier = category.modifiers.find(
          ({ id }) => formValues[radioCategory] === id,
        );

        newPrice += currentModifier.price;
      }

      if (!isNil(formValues[checkboxCategory]) && formValues[checkboxCategory].length) {
        const checkboxCategoryValues: string[] = formValues[checkboxCategory];

        const validModifiersPrice = category.modifiers
          .filter(({ id }) => checkboxCategoryValues.includes(id))
          .map(({ price }) => price);

        newPrice = validModifiersPrice.reduce((acc, prev) => acc + prev, newPrice);
      }

      return newPrice;
    });

    const priceWithAmount = Number(formValues.amount) * newPrice;

    return priceWithAmount;
  }

  const priceWithAmount = Number(formValues.amount) * basePrice;
  return priceWithAmount;
};
