// Main types
import { TRestaurantProductModifier } from '@type/restaurants/restaurant-products';

export interface TValues {
  amount: string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [propName: string]: any;
}

export type TGetPrice = (
  values: TValues,
  modifiers: TRestaurantProductModifier[],
  basePrice: number,
) => number;
