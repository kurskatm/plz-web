// Main types
import { TRestaurantProductModifiers } from '@type/restaurants/restaurant-products';
import { TShopingCartModel } from '@models/shoping-cart/types';

export interface TValues {
  amount: string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [propName: string]: any;
}

export type TGetFormInitialValues = (
  product: TRestaurantProductModifiers,
  shopingCart: TShopingCartModel,
) => TValues;

export type TImplementCategories = (obj: TValues, categories: string[]) => TValues;

export type TGetAmount = (
  product: TRestaurantProductModifiers,
  shopingCart: TShopingCartModel,
) => string;
