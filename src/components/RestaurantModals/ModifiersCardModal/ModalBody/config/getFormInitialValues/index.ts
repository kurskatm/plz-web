// Types
import { isNil } from 'lodash';
import {
  TGetFormInitialValues, TValues, TImplementCategories, TGetAmount,
} from './types';

export const getAmount: TGetAmount = (product, shopingCart) => {
  if (product?.modifiers?.length) {
    return '1';
  }

  const shopingCartItem = shopingCart?.products.find(
    (item) => item.id === product?.id && item.inBonusPoint === product?.availableInBonusPoints,
  );

  if (!isNil(shopingCartItem) && !shopingCartItem.inBonusPoint) {
    return String(shopingCartItem?.count);
  }

  if (!isNil(shopingCartItem) && shopingCartItem.inBonusPoint) {
    return '0';
  }

  return '1';
};

export const implementRadioCategories: TImplementCategories = (obj, categories) => {
  categories.forEach((category) => {
    if (isNil(category)) return;
    obj[category] = '';
  });

  return obj;
};

export const implementCheckboxCategories: TImplementCategories = (obj, categories) => {
  categories.forEach((category) => {
    if (isNil(category)) return;
    obj[category] = [];
  });

  return obj;
};

export const getFormInitialValues: TGetFormInitialValues = (product, shopingCart) => {
  const values: TValues = {
    amount: getAmount(product, shopingCart),
  };

  const radioCategories = [].concat(
    product.modifiers.map((category) => {
      if (
        category.minSelectedModifiers === 1
        && category.maxSelectedModifiers === 1
        && category.required
      ) {
        return `radio-category-${category.id}`;
      }

      return undefined;
    }),
  );

  const checkboxCategories = [].concat(
    product.modifiers.map((category) => {
      if (category.maxSelectedModifiers > 1) {
        return `checkbox-category-${category.id}`;
      }

      return undefined;
    }),
  );

  const valuesWithRadioCategories = implementRadioCategories(values, radioCategories);

  const valuesWithCheckboxCategories = implementCheckboxCategories(
    valuesWithRadioCategories,
    checkboxCategories,
  );

  return valuesWithCheckboxCategories;
};
