export interface TErrors {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [propName: string]: any;
}

export type TGetScrollIntoView = (errors: TErrors) => void;
