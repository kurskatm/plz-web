// Types
import { TGetScrollIntoView } from './types';

export const scrollIntoViewHelper: TGetScrollIntoView = (errors) => {
  if (Object.keys(errors).length) {
    const firstError = Object.keys(errors)[0];
    const element = document.querySelector(`[name="${firstError}"]`);

    if (element) {
      element.scrollIntoView({
        behavior: 'smooth',
        block: 'center',
      });
    }
  }
};
