// Modules
import isNil from 'lodash/isNil';
// Types
import { TGetSubmitData, TRestaurantProductModifiers } from './types';

export const getSubmitData: TGetSubmitData = (formValues, product) => {
  if (!isNil(product)) {
    const newProduct: TRestaurantProductModifiers = {
      ...product,
      modifiers: [],
    };

    product.modifiers.forEach((category) => {
      const radioCategory = `radio-category-${category.id}`;
      const checkboxCategory = `checkbox-category-${category.id}`;

      if (!isNil(formValues[radioCategory]) && formValues[radioCategory].length) {
        const currentModifier = category.modifiers.find(
          ({ id }) => formValues[radioCategory] === id,
        );

        // newProduct.price += currentModifier.price;
        newProduct.modifiers.push({
          ...category,
          modifiers: [currentModifier],
        });
      }

      if (!isNil(formValues[checkboxCategory]) && formValues[checkboxCategory].length) {
        const checkboxCategoryValues: string[] = formValues[checkboxCategory];

        const validModifiers = category.modifiers.filter(({ id }) =>
          checkboxCategoryValues.includes(id));

        // const validModifiersPrice = validModifiers.map(({ price }) => price);

        // newProduct.price = validModifiersPrice
        //  .reduce((acc, prev) => acc + prev, newProduct.price);
        newProduct.modifiers.push({
          ...category,
          modifiers: validModifiers,
        });
      }
    });

    return newProduct;
  }

  return product;
};
