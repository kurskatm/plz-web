// Main types
import { TRestaurantProductModifiers } from '@type/restaurants/restaurant-products';

export interface TValues {
  amount: string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [propName: string]: any;
}

export type TGetSubmitData = (
  values: TValues,
  product: TRestaurantProductModifiers,
) => TRestaurantProductModifiers;

export { TRestaurantProductModifiers };
