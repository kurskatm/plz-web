// Main types
import {
  TRestaurantProductModifiers,
  TRestaurantProductModifier,
  TRestaurantProductModifierItem,
} from '@type/restaurants/restaurant-products';
import { TUpdateShopingCart } from '@actions/shoping-cart/change/action-types';
import { TShopingCartModel } from '@models/shoping-cart/types';
import { TCitiesItem } from '@type/cities';
import { TRestaurantCardIdSave } from '@actions/restaurant/main/id/action-types';
import { TShowRestaurantModals } from '@actions/restaurant/modal/action-types';
import { TNewGeocodeData } from '@type/new-geocode';

export interface TModalBodyProps {
  product: TRestaurantProductModifiers;
  shopingCartUpdate: TUpdateShopingCart;
  shopingCart: TShopingCartModel;
  city: TCitiesItem;
  restaurantCardIdSave: TRestaurantCardIdSave;
  closeModal: () => unknown;
  initialAddress: string;
  geocode: TNewGeocodeData;
  balance: string | number;
  auth: string;
  showRestaurantModals: TShowRestaurantModals;
  authModalShow: (data: boolean) => void;
}

export { TRestaurantProductModifier, TRestaurantProductModifierItem };
