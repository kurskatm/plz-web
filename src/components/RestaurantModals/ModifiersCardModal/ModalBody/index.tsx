// Modules
import React, {
  FC, memo, useCallback, useState, useEffect, useMemo, useRef,
} from 'react';
import isNil from 'lodash/isNil';
import throttle from 'lodash/throttle';
import classNames from 'classnames';
import { useHistory } from 'react-router';
import { Form, Field, FieldInputProps } from 'react-final-form';
import { FormApi } from 'final-form';
// Assets
import cardFoodDefault from '@/../assets/svg/main/card-food-default.svg';
// Modules Components
import {
  AmountField, Button, RadioField, Checkbox, ScoresIcon,
} from 'chibbis-ui-kit';
// Lib
import { UpdateShopingCartManager } from '@lib/shoping-cart/update-cart';
import { UpdateShopingCartWithModifiersManager } from '@lib/shoping-cart/update-cart-with-modifiers';
// Utils
import { trackEvent } from '@utils/analytics';
import { applyImageFallback } from '@/utils';
// Config
import { getFormInitialValues, getAmount } from './config/getFormInitialValues';
import { getFormValidation } from './config/getValidation';
import { scrollIntoViewHelper } from './config/scrollIntoView';
import { getPrice } from './config/getPrice';
import { getSubmitData } from './config/getSubmitData';
// Enhance
import { enhance } from './enhance';
// Types
import { TModalBodyProps, TRestaurantProductModifier } from './types';

const RestaurantPath = new RegExp('/[a-z]*/restaurant/');

const ModifiersModalBodyComponent: FC<TModalBodyProps> = ({
  product,
  city,
  shopingCart,
  shopingCartUpdate,
  restaurantCardIdSave,
  closeModal,
  initialAddress,
  geocode,
  balance,
  showRestaurantModals,
  auth,
  authModalShow,
}) => {
  const history = useHistory();

  const modalBodyRef = useRef<HTMLDivElement>();
  const scrollRef = useRef<HTMLDivElement>();
  const titleRef = useRef<HTMLDivElement>();
  const [titleFixed, setTitleFixed] = useState<boolean>(false);
  const [authShopingCartData, setAuthShopingCartData] = useState(null);
  const [shopingCartData, setShopingCartData] = useState(null);
  const [redirectData, setRedirectData] = useState(null);
  const [initialAmount, setInitialAmount] = useState(getAmount(product, shopingCart));
  const [productInCart, setProductInCart] = useState(null);

  useEffect(() => {
    setInitialAmount(getAmount(product, shopingCart));
    const shopingCartItem = shopingCart?.products.find(({ id }) => id === product?.id);
    if (!isNil(shopingCartItem)) setProductInCart(true);
    if (isNil(shopingCartItem)) setProductInCart(false);
  }, [product, shopingCart]);

  useEffect(
    () => {
      if (!isNil(auth) && auth.length && !isNil(authShopingCartData)) {
        shopingCartUpdate(authShopingCartData);
        setAuthShopingCartData(null);
        trackEvent('try_to_add_EZB');
      }
      if (
        initialAddress.length
        && !isNil(geocode)
        && geocode.precision === 'exact'
        && !isNil(shopingCartData)
      ) {
        shopingCartUpdate(shopingCartData);
        setShopingCartData(null);
        if (history.location.pathname.match(RestaurantPath)) {
          trackEvent('add-cart');
        }
      }
      if (
        initialAddress.length
        && !isNil(geocode)
        && geocode.precision === 'exact'
        && !isNil(redirectData)
      ) {
        restaurantCardIdSave({ id: redirectData.restaurantId });
        history.push(redirectData.link);
        setRedirectData(null);
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [auth, initialAddress.length, geocode, shopingCartData, authShopingCartData, redirectData],
  );

  const scrollCollback = throttle(() => {
    const top = scrollRef.current.scrollTop;
    const isFixed = top >= 240 + 22;
    setTitleFixed(isFixed);
  }, 100);

  useEffect(() => {
    scrollRef.current?.addEventListener('scroll', scrollCollback);

    return () => {
      // eslint-disable-next-line react-hooks/exhaustive-deps
      scrollRef.current?.removeEventListener('scroll', scrollCollback);
    };
  }, [scrollCollback, titleFixed]);

  const getTitleStyles = useCallback(
    () =>
      classNames('product-card-with-modifiers__title', {
        'product-card-with-modifiers__title-fixed': titleFixed,
      }),
    [titleFixed],
  );

  const getTopFixedTitle = useCallback(() => {
    const modalHeight = modalBodyRef.current?.offsetHeight;
    const marginTopOffset = (window.innerHeight - modalHeight - 24) / 2;
    // modal padding-bottom = 24
    if (titleFixed) {
      return {
        top: `${marginTopOffset}px`,
      };
    }

    return {};
  }, [titleFixed]);

  const getInitialValues = useCallback(() => {
    if (!isNil(product)) {
      return getFormInitialValues(product, shopingCart);
    }

    return undefined;
  }, [product, shopingCart]);

  const getValidation = useCallback((values) => getFormValidation(values, product), [product]);

  const onSubmit = useCallback(
    (values, form) => {
      if (form.getState().valid) {
        const link = `/${city?.urlName}/restaurant/${product?.restaurant.urlName}`;
        if (!product?.modifiers.length) {
          const data = {
            cityId: city.id,
            restaurant: {
              ...product?.restaurant,
            },
            product: {
              id: product?.id,
              name: product?.name,
              description: product?.description,
              price: product?.price,
              inBonusPoint: product?.availableInBonusPoints,
              count:
                initialAmount !== values.amount || product?.availableInBonusPoints || !productInCart
                  ? Number(values.amount)
                  : +values.amount + 1,
            },
            shopingCart,
          };

          const newShopingCartState = new UpdateShopingCartManager(data).init();

          if (isNil(auth) && product?.availableInBonusPoints) {
            setAuthShopingCartData(newShopingCartState);
            closeModal();
            setTimeout(() => {
              authModalShow(true);
            }, 50);
          } else if (!initialAddress.length && geocode?.precision !== 'exact') {
            setShopingCartData(newShopingCartState);
            closeModal();
            setTimeout(() => {
              showRestaurantModals({
                showModal: true,
                modalType: 'change-address',
              });
            }, 50);
          } else if (
            !isNil(shopingCart.restaurant)
            && !isNil(shopingCart.restaurant.id)
            && product?.restaurant.id !== shopingCart.restaurant.id
          ) {
            closeModal();
            setTimeout(() => {
              showRestaurantModals({
                showModal: true,
                modalType: 'another-restaurant',
                // @ts-ignore
                dataToUpdateCart: data,
                link: !history.location.pathname.match(RestaurantPath) ? link : null,
              });
            }, 50);
          } else if (
            Number(values.amount) > 0
            && product?.price > balance
            && product?.availableInBonusPoints
            && !isNil(auth)
            && auth.length
          ) {
            closeModal();
            setTimeout(() => {
              showRestaurantModals({
                showModal: true,
                modalType: 'not-enough-points',
                bonusPointsRest: product.price - Number(balance),
              });
            }, 50);
          } else if (
            shopingCart.products?.filter((item) => item.inBonusPoint).length === 1
            && product.availableInBonusPoints
            && Number(values.amount) === 1
          ) {
            closeModal();
            setTimeout(() => {
              showRestaurantModals({
                showModal: true,
                modalType: 'another-point-product',
                // @ts-ignore
                dataToUpdateCart: data,
              });
            }, 50);
          } else {
            shopingCartUpdate(newShopingCartState);
            if (history.location.pathname.match(RestaurantPath)) {
              trackEvent('add-cart');
            }
          }
        }

        if (product?.modifiers.length) {
          const newProduct = getSubmitData(values, product);
          const data = {
            cityId: city.id,
            restaurant: {
              ...newProduct?.restaurant,
            },
            product: {
              id: newProduct?.id,
              name: newProduct?.name,
              description: newProduct?.description,
              price: newProduct?.price,
              inBonusPoint: newProduct?.availableInBonusPoints,
              modifiers: newProduct?.modifiers,
              count: values.amount,
            },
            shopingCart,
          };

          const newShopingCartState = new UpdateShopingCartWithModifiersManager(data).init();
          if (!initialAddress.length && geocode?.precision !== 'exact') {
            showRestaurantModals({
              showModal: true,
              modalType: 'change-address',
            });
            setShopingCartData(newShopingCartState);
            closeModal();
          } else if (
            !isNil(shopingCart.restaurant)
            && !isNil(shopingCart.restaurant.id)
            && product?.restaurant.id !== shopingCart.restaurant.id
          ) {
            closeModal();
            setTimeout(() => {
              showRestaurantModals({
                showModal: true,
                modalType: 'another-restaurant',
                // @ts-ignore
                dataToUpdateCart: data,
                link: !history.location.pathname.match(RestaurantPath) ? link : null,
              });
            }, 50);
          } else {
            shopingCartUpdate(newShopingCartState);
            if (history.location.pathname.match(RestaurantPath)) {
              trackEvent('add-cart');
            }
          }
        }

        if (
          !history.location.pathname.match(RestaurantPath)
          && !initialAddress.length
          && geocode?.precision !== 'exact'
        ) {
          setRedirectData({
            restaurantId: product?.restaurant.id,
            link: `/${city?.urlName}/restaurant/${product?.restaurant.urlName}`,
          });
        }
        if (
          !history.location.pathname.match(RestaurantPath)
          && geocode?.precision === 'exact'
          && !isNil(shopingCart.restaurant)
          && !isNil(shopingCart.restaurant.id)
          && product?.restaurant.id === shopingCart.restaurant.id
        ) {
          restaurantCardIdSave({ id: product?.restaurant.id });
          history.push(link);
        }

        if (initialAddress.length) {
          closeModal();
        }
      }
    },
    [
      city,
      history,
      product,
      restaurantCardIdSave,
      closeModal,
      shopingCart,
      shopingCartUpdate,
      initialAddress,
      geocode?.precision,
      showRestaurantModals,
      initialAmount,
      productInCart,
      balance,
      authModalShow,
      auth,
    ],
  );

  const getCategoryOptions = useCallback((category: TRestaurantProductModifier) => {
    if (category.minSelectedModifiers === 1 && category.maxSelectedModifiers === 1) {
      return '';
    }

    if (category.minSelectedModifiers > 0 && category.maxSelectedModifiers > 1) {
      return `от ${category.minSelectedModifiers} опций`;
    }

    return '';
  }, []);

  const onCheckboxChange = useCallback(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (input: FieldInputProps<any, HTMLElement>, checkboxValue: string) => {
      const values = input.value;
      if (!isNil(values)) {
        if (!values.includes(checkboxValue)) {
          input.onChange([...values, checkboxValue]);
        } else {
          const filteredValue = values.filter((elem: string) => elem !== checkboxValue);
          input.onChange(filteredValue);
        }
      } else {
        input.onChange([checkboxValue]);
      }
    },
    [],
  );

  const getCheckboxDisabled = useCallback(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (input: FieldInputProps<any, HTMLElement>, checkboxValue: string, max: number) => {
      if (!input.value.includes(checkboxValue) && input.value.length === max) {
        return true;
      }

      return false;
    },
    [],
  );

  const getFieldError = useCallback((form: FormApi, categoryId: string) => {
    const radioCategory = `radio-category-${categoryId}`;
    const checkboxCategory = `checkbox-category-${categoryId}`;

    const { submitFailed } = form.getState();

    const radioError = form.getFieldState(radioCategory)?.error;

    const checkboxError = form.getFieldState(checkboxCategory)?.error;

    if (radioError?.length && submitFailed) {
      return true;
    }

    if (checkboxError?.length && submitFailed) {
      return true;
    }

    return false;
  }, []);

  const getTitleColor = useCallback((hasError: boolean) => {
    if (hasError) {
      return {
        color: '#E73232',
      };
    }

    return {
      color: '#8A9095',
    };
  }, []);

  const renderPrice = useCallback(
    (form: FormApi) => {
      if (!isNil(product) && !isNil(form.getState().values)) {
        return getPrice(
          // @ts-ignore
          form.getState().values,
          product.modifiers,
          product.price,
        );
      }

      return product?.price;
    },
    [product],
  );

  const getProductPrice = useCallback(
    (form: FormApi) => {
      if (!isNil(product?.price)) {
        if (!product?.availableInBonusPoints) {
          return (
            <div className="product-card-with-modifiers__coast-and-weight">
              <div className="product-card-with-modifiers__coast">{`${renderPrice(form)} ₽`}</div>
              {/*
              {!isNil(product?.weight) && (
                <div className="product-card-with-modifiers__weight">{product?.weight}</div>
              )}
              */}
            </div>
          );
        }
        return (
          <div className="product-card-with-modifiers__coast-and-weight">
            <div className="product-card-with-modifiers__coast-in-bonus">
              <div>
                <ScoresIcon width={24} height={24} />
              </div>
              <div>{product?.price}</div>
            </div>
          </div>
        );
      }

      return null;
    },
    [product?.availableInBonusPoints, product?.price, renderPrice],
  );

  const renderModifierCategories = useMemo(
    () => (category: TRestaurantProductModifier, form: FormApi) => (
      <div className="product-card-with-modifiers__category" key={category.id}>
        <div
          className="product-card-with-modifiers__category_title"
          style={getTitleColor(getFieldError(form, category.id))}
        >
          {category.name}
          {' '}
          {getCategoryOptions(category)}
        </div>
        <div className="product-card-with-modifiers__category_list">
          {category.modifiers.map((modifier) => {
            if (category.minSelectedModifiers === 1 && category.maxSelectedModifiers === 1) {
              return (
                <div className="product-card-with-modifiers__modifier_item" key={modifier.id}>
                  <div className="product-card-with-modifiers__left-column">
                    <div className="product-card-with-modifiers__modifier_item_button">
                      <RadioField
                        className="product-card-with-modifiers__radio"
                        name={`radio-category-${category.id}`}
                        value={modifier.id}
                        view={modifier.name}
                      />
                    </div>
                  </div>
                  <div className="product-card-with-modifiers__right-column">
                    <div className="product-card-with-modifiers__price">
                      {`+${modifier.price} ₽`}
                    </div>
                  </div>
                </div>
              );
            }

            return (
              <div className="product-card-with-modifiers__modifier_item" key={modifier.id}>
                <div className="product-card-with-modifiers__left-column">
                  <div className="product-card-with-modifiers__modifier_item_button">
                    <Field
                      name={`checkbox-category-${category.id}`}
                      render={({ input }) => (
                        <Checkbox
                          className="product-card-with-modifiers__checkbox"
                          name={`checkbox-category-${category.id}`}
                          value={input.value.includes(modifier.id)}
                          view={modifier.name}
                          onChange={() => onCheckboxChange(input, modifier.id)}
                          disabled={getCheckboxDisabled(
                            input,
                            modifier.id,
                            category.maxSelectedModifiers,
                          )}
                        />
                      )}
                    />
                  </div>
                </div>
                <div className="product-card-with-modifiers__right-column">
                  <div className="product-card-with-modifiers__price">{`+${modifier.price} ₽`}</div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    ),
    [getCategoryOptions, onCheckboxChange, getCheckboxDisabled, getFieldError, getTitleColor],
  );

  const getButtonStyles = useCallback(() => {
    const validProductInCart = shopingCart.products?.find(({ id }) => id === product?.id);
    const isIncluded = !isNil(validProductInCart)
      && validProductInCart.inBonusPoint === product?.availableInBonusPoints;
    return classNames('product-card-with-modifiers__button', {
      'product-card-with-modifiers__button-bonus': product?.availableInBonusPoints,
      'product-card-with-modifiers__button-bonus-remove':
        product?.availableInBonusPoints && isIncluded,
    });
  }, [shopingCart.products, product]);

  const getPanelStyles = useCallback(
    () =>
      classNames('product-card-with-modifiers__panel', {
        'product-card-with-modifiers__panel-bonus': product?.availableInBonusPoints,
      }),
    [product?.availableInBonusPoints],
  );

  const getButtonText = useMemo(() => {
    const validProductInCart = shopingCart.products?.find(({ id }) => id === product?.id);
    const isIncluded = !isNil(validProductInCart)
      && validProductInCart.inBonusPoint === product?.availableInBonusPoints;
    if (product?.availableInBonusPoints && isIncluded) {
      return 'Убрать';
    }

    return 'Взять';
  }, [product, shopingCart.products]);

  return (
    <Form
      onSubmit={onSubmit}
      initialValues={getInitialValues()}
      validate={getValidation}
      render={({ handleSubmit, form, submitting }) => (
        <form
          id="productCardWithModifiersForm"
          onSubmit={(event) => {
            handleSubmit(event);
            scrollIntoViewHelper(form.getState().errors);
          }}
        >
          <div
            className="product-card-with-modifiers"
            ref={modalBodyRef}
            style={{
              maxHeight: `${window.innerHeight - 200}px`,
            }}
          >
            <div className="product-card-with-modifiers__content-wrap">
              <div className="product-card-with-modifiers__with-scroll" ref={scrollRef}>
                <div className="product-card-with-modifiers__image-wrap">
                  <figure>
                    <img
                      src={product?.imageUrls[0] || cardFoodDefault}
                      onError={applyImageFallback(cardFoodDefault)}
                      alt={product?.name || 'img'}
                      width="400"
                      height="200"
                    />
                  </figure>
                </div>

                <div className="product-card-with-modifiers__info">
                  <div className="product-card-with-modifiers__title-wrap">
                    <div className={getTitleStyles()} ref={titleRef} style={getTopFixedTitle()}>
                      {product?.name}
                    </div>
                  </div>

                  <div className="product-card-with-modifiers__description">
                    {product?.description}
                  </div>

                  {product?.modifiers.length > 0 && (
                    <div className="product-card-with-modifiers__modifiers">
                      {product?.modifiers.map((category) =>
                        renderModifierCategories(category, form))}
                    </div>
                  )}
                </div>
              </div>

              <div className="product-card-with-modifiers__without-scroll">
                <div className={getPanelStyles()}>
                  {!product?.availableInBonusPoints && (
                    <div className="product-card-with-modifiers__amount">
                      <Field
                        name="amount"
                        render={({ input }) => (
                          <AmountField
                            name="amount"
                            onChange={input.onChange}
                            value={input.value}
                            className="product-card__amount-buttons"
                            noBorder
                            minValue={1}
                          />
                        )}
                      />
                    </div>
                  )}

                  {getProductPrice(form)}

                  <div className={getButtonStyles()}>
                    <Button
                      htmlType="submit"
                      className="product-card__button"
                      size="small"
                      disabled={submitting}
                      ariaLabel={getButtonText}
                    >
                      {getButtonText}
                    </Button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      )}
    />
  );
};

const ModifiersModalBodyMemo = memo(ModifiersModalBodyComponent);
export default enhance(ModifiersModalBodyMemo);
