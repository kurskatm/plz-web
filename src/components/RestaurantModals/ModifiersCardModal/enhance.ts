// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { showModifiersCardModal } from '@actions/restaurant/modal';
// Selectores
import { modifiersModalStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/restaurant/modal';

const mapDispatchToProps = {
  showModifiersCardModal,
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
