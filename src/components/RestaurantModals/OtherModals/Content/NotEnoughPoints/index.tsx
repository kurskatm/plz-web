// Modules
import React, { FC, memo, useCallback } from 'react';
// Modules Components
import { Link } from 'react-router-dom';
import { Button, ScoresIcon } from 'chibbis-ui-kit';
// Utils
import { Consts } from '@utils';
// Types
import { TNotEnoughPointsProps } from './types';

const { PATH } = Consts.ROUTES.PROFILE;

const NotEnoughPointsModalBodyComponent: FC<TNotEnoughPointsProps> = ({ restBonusPoints }) => {
  const getLink = useCallback(() => `${PATH}/scores#howtoget`, []);

  return (
    <div className="not-enough-points-modal__body">
      <div className="not-enough-points-modal__body-text">
        Вам не хватает
        {' '}
        <ScoresIcon width={20} height={20} />
        {restBonusPoints}
        {' '}
        баллов,
        <br />
        чтобы взять это блюдо
      </div>
      <div className="not-enough-points-modal__body-button">
        <Link to={getLink()}>
          <Button htmlType="button" type="secondary">
            Как получить баллы?
          </Button>
        </Link>
      </div>
    </div>
  );
};

const NotEnoughPointsModalBodyMemo = memo(NotEnoughPointsModalBodyComponent);
export default NotEnoughPointsModalBodyMemo;
