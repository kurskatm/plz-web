// Main types
import { TUpdateShopingCart } from '@/actions/shoping-cart/change/action-types';
import { TRestaurantDataToUpdateCart } from '@/type/restaurants/restaurant-card';
import { TRestaurantProduct } from '@/type/restaurants/restaurant-products';

export interface TAnotherPointsProductProps {
  closeModal: () => void;
  dataToUpdateCart: TRestaurantDataToUpdateCart;
  shopingCartUpdate: TUpdateShopingCart;
  shoppingCartFetchPending: boolean;
  productsList: TRestaurantProduct[];
}
