// Main Types
import { TRestaurantProduct } from '@type/restaurants/restaurant-products';

export interface TProductCardProps {
  item: TRestaurantProduct;
}
