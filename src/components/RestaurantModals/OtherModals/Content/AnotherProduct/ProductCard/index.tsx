// Modules
import React, {
  FC, memo, useMemo, useRef,
} from 'react';
import isNil from 'lodash/isNil';
// import Truncate from 'react-truncate';
// Modules Components
import { ScoresIcon } from 'chibbis-ui-kit';
// Types
import { TProductCardProps } from './types';

const HitCardCompomnent: FC<TProductCardProps> = ({ item }) => {
  const cardRef = useRef<HTMLDivElement>();

  const getCoast = useMemo(
    () => () => {
      if (isNil(item.price)) {
        return null;
      }

      if (item?.availableInBonusPoints) {
        return (
          <div className="modal-product-card__for-points">
            <div>
              <ScoresIcon width={16} height={16} />
            </div>
            <div>{item.price}</div>
          </div>
        );
      }

      return `${item.price} ₽`;
    },
    [item?.availableInBonusPoints, item?.price],
  );

  return (
    <div ref={cardRef} className="modal-product-card">
      <div className="modal-product-card__image-wrap">
        {item?.imageUrls[0] && (
          <figure>
            <img src={item.imageUrls[0]} alt={item.name} />
          </figure>
        )}
      </div>

      <div className="modal-product-card__content-wrap">
        {item.name && <div className="modal-product-card__title">{item.name}</div>}

        <div className="modal-product-card__panel">
          {!isNil(item.price) && <div className="modal-product-card__coast">{getCoast()}</div>}

          {!isNil(item.weight) && <div className="modal-product-card__weight">{item.weight}</div>}
        </div>
      </div>
    </div>
  );
};

const HitCardMemo = memo(HitCardCompomnent);
export default HitCardMemo;
