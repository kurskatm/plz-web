// Modules
import React, {
  FC, memo, useCallback, useState, useEffect, useMemo,
} from 'react';
// Lib
import { UpdateShopingCartManager } from '@lib/shoping-cart/update-cart';
// Main types
import { TRestaurantProduct } from '@/type/restaurants/restaurant-products';
// Utils
import { trackEvent } from '@utils/analytics';
// Modules Components
import { Button } from 'chibbis-ui-kit';
// Components
import AnotherElemArrow from '@components/AnotherElemArrow';
import ProductCard from './ProductCard';
// Enhance
import { enhance } from './enhance';
// Types
import { TAnotherPointsProductProps } from './types';

const AnotherPointProductModalBodyComponent: FC<TAnotherPointsProductProps> = ({
  closeModal,
  dataToUpdateCart,
  shopingCartUpdate,
  productsList,
}) => {
  const [list, setList] = useState<TRestaurantProduct[]>([]);

  useEffect(() => {
    if (!!productsList && productsList.length && !!dataToUpdateCart) {
      const filteredList = productsList.filter((product) =>
        dataToUpdateCart.shopingCart.products.map((item) => item.id).includes(product.id));
      const currentAddingProduct = productsList.find(
        ({ id }) => dataToUpdateCart.product.id === id,
      );
      setList([...filteredList, currentAddingProduct]);
    }
  }, [dataToUpdateCart, productsList]);

  const onConfirmClick = useCallback(() => {
    // @ts-ignore
    const newShopingCartState = new UpdateShopingCartManager(dataToUpdateCart).init();

    shopingCartUpdate(newShopingCartState);
    trackEvent('try_to_add_EZB');
    closeModal();
  }, [dataToUpdateCart, closeModal, shopingCartUpdate]);

  const renderCards = useMemo(
    () => (item: TRestaurantProduct) => (
      <div className="another-point-product-modal__body-point-card" key={item.id}>
        <ProductCard item={item} />
      </div>
    ),
    [],
  );

  return (
    <div className="another-point-product-modal__body">
      <div className="another-point-product-modal__body-text">
        Вы уже добавили одно блюдо.
        <br />
        Хотите заменить его?
      </div>
      <div className="another-point-product-modal__body-cards">{list.map(renderCards)}</div>
      <AnotherElemArrow />
      <div className="another-point-product-modal__body-button">
        <Button htmlType="button" type="primary" onClick={onConfirmClick}>
          Заменить и продолжить
        </Button>
      </div>
    </div>
  );
};

const AnotherPointProductModalBodyMemo = memo(AnotherPointProductModalBodyComponent);
export default enhance(AnotherPointProductModalBodyMemo);
