// Modules
import React, {
  FC,
  memo,
  useState,
  useEffect,
  useCallback,
  useRef,
  createRef,
  useMemo,
} from 'react';
import debounce from 'lodash/debounce';
import isNil from 'lodash/isNil';
import { useMediaQuery } from 'react-responsive';
// Lib
import { removeGeoCode, removeAddress } from '@lib/cookie';
import { AddressInputManager } from '@lib/address';
import { AutoYandexLocation } from '@lib/yandex';
// Utils
import { trackEvent } from '@utils/analytics';
// Components
import {
  Input, GeoIcon, LoadingSpinner, GoBackArrowIcon,
} from 'chibbis-ui-kit';
import AddressList from './List';
import Message from './Message';
// Enhance
import { enhance } from './enhance';
// Types
import {
  TAddressInputProps,
  TOption,
  TGeoState,
  TNewAddressesList,
  TNewGeocodeData,
} from './types';

const inputHtmlRef = createRef<HTMLInputElement>();
const listRef = createRef<HTMLInputElement>();

const AddressComponent: FC<TAddressInputProps> = ({
  fetchSuggest,
  geocode,
  initialAddress,
  getGeocode,
  list,
  listIsSuccess,
  resetSuggest,
  closeModal,
  geocodeIsPending,
  citiesList,
  cityId,
  AutoLocationFooterRef,
}) => {
  const isMobile = useMediaQuery({
    maxWidth: 768,
  });

  const updateGeocode = useRef(
    debounce((result: { name: string; description: string }) => {
      getGeocode({
        description: result.description,
        name: result.name,
      });
    }, 200),
  );

  const [address, setAddress] = useState<string>(initialAddress);
  const [addressList, setList] = useState<TNewAddressesList>([]);
  const [showList, setShowList] = useState<boolean>(false);
  const [prevGeoState, setPrevGeoState] = useState<TGeoState>();
  const [prevGeoData, setPrevGeoData] = useState<TNewGeocodeData>(geocode);
  const [isAutoLocation, setIsAutoLocation] = useState<boolean>(false);
  const [autoLocationPending, setAutoLocationPending] = useState<boolean>(false);
  const [isCityValid, setIsCityValid] = useState<boolean>(true);
  const [activeAddress, setActiveAddress] = useState<number>(0);

  useEffect(
    () => {
      const inputAddressManager = new AddressInputManager({
        cityId,
        citiesList,
        fetchSuggest,
        initialAddress,
        inputRef: inputHtmlRef.current,
      });

      return () => {
        inputAddressManager.unsubscribe();
      };
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [cityId, initialAddress],
  );

  useEffect(() => {
    const autoLocationManager = new AutoYandexLocation({
      autoLocationRef: AutoLocationFooterRef.current,
      cities: citiesList,
      setAddress,
      setIsAutoLocation,
      setAutoLocationPending,
      setIsCityValid,
    });

    return () => {
      autoLocationManager.unsubscribe();
    };
  }, [AutoLocationFooterRef, citiesList]);

  useEffect(() => {
    if (isMobile) {
      const goBackArrowImg = document.body
        .getElementsByClassName('change-address-modal__input')[0]
        .getElementsByClassName('icon_go-back-arrow')[0];
      if (!isNil(goBackArrowImg)) {
        goBackArrowImg.addEventListener('click', () => closeModal());

        return () => {
          goBackArrowImg.removeEventListener('click', () => closeModal());
        };
      }
    }

    return undefined;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isMobile]);

  useEffect(() => {
    if (!addressList.length && list.length) {
      setList(list);
      setShowList(true);
    }
    if (!list.length && listIsSuccess) {
      setList(list);
      setShowList(false);
    }
    if (list.length && address.length < 3) {
      resetSuggest();
      setList([]);
      setShowList(false);
      removeGeoCode();
      removeAddress();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [address.length, list, listIsSuccess, addressList.length]);

  useEffect(
    () => {
      if (!isNil(geocode) && geocode.precision !== 'exact' && addressList.length) {
        setShowList(true);
      }
      if (!isNil(geocode) && geocode.precision === 'exact' && prevGeoData !== geocode) {
        if (citiesList.map(({ id }) => id).includes(geocode.cityId)) {
          setShowList(false);
          closeModal();
          resetSuggest();
          trackEvent('address-popup');
          setPrevGeoData(geocode);
          setIsCityValid(true);
        } else {
          setIsCityValid(false);
        }
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [geocode, addressList.length, prevGeoData],
  );

  const onOptionSelect = useCallback(
    (option: TOption) => {
      const validData: TGeoState = {
        name: option.name,
        description: option.description,
      };
      setAddress(option.name);
      if (validData !== prevGeoState) {
        updateGeocode.current({
          name: option.name,
          description: option.description,
        });
        setPrevGeoState(validData);
        setShowList(false);
      }
      inputHtmlRef.current.focus();
    },
    [prevGeoState],
  );

  const onInputChange = useCallback(
    ({ target }) => {
      setAddress(target.value);
      if (isAutoLocation && !isCityValid) {
        setIsAutoLocation(false);
        setIsCityValid(true);
      }
    },
    [isAutoLocation, isCityValid],
  );

  const onInputClear = useCallback(
    () => {
      resetSuggest();
      setList([]);
      setShowList(false);
      removeGeoCode();
      removeAddress();
      setAddress('');
      setIsCityValid(true);
      setIsAutoLocation(false);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const onAddressChange = useCallback(() => {
    setAddress('');
    inputHtmlRef.current.focus();
  }, []);

  const onResetAutoLocation = useCallback(() => {
    updateGeocode.current({
      name: address,
      description: address,
    });
    setIsAutoLocation(false);
  }, [address]);

  const icon = useMemo(() => {
    if (isMobile) {
      return GoBackArrowIcon;
    }

    return GeoIcon;
  }, [isMobile]);

  const theme = useMemo(() => {
    if (isMobile) {
      return 'white';
    }

    return 'grey';
  }, [isMobile]);

  const autoLocationErrorNotification = useMemo(() => {
    if (isAutoLocation && !isCityValid) {
      return (
        <div className="change-address-modal__notification-error">
          <div className="change-address-modal__notification-error-inner">
            Не удалось определить точный адрес
          </div>
        </div>
      );
    }

    return null;
  }, [isAutoLocation, isCityValid]);

  const onKeyPress = useCallback(
    (event) => {
      event.stopPropagation();
      if (['ArrowDown'].includes(event.key)) {
        if (activeAddress < list.length - 1) setActiveAddress(activeAddress + 1);
      }
      if (['ArrowUp'].includes(event.key)) {
        if (activeAddress > 0) setActiveAddress(activeAddress - 1);
      }
      if (['Enter'].includes(event.key)) {
        onOptionSelect(list[activeAddress]);
      }
    },
    [activeAddress, setActiveAddress, list, onOptionSelect],
  );
  return (
    <>
      <div className="change-address-modal__input" onKeyDownCapture={onKeyPress}>
        <Input
          autocomplete={false}
          clear
          icon={icon}
          inputRef={inputHtmlRef}
          name="address"
          onChange={onInputChange}
          onClear={onInputClear}
          placeholder="Введите улицу и дом"
          value={address}
          theme={theme}
          noBorder={isMobile}
        />
      </div>
      {autoLocationErrorNotification}
      {showList && (
        <AddressList
          list={list}
          listRef={listRef}
          onOptionSelect={onOptionSelect}
          showList={showList}
          activeAddress={activeAddress}
        />
      )}
      {(geocodeIsPending || autoLocationPending) && (
        <LoadingSpinner className="change-address-modal-list__spinner" size="big" color="orange" />
      )}
      {!showList && !(geocodeIsPending || autoLocationPending) && (
        <Message
          address={address}
          initialAddress={initialAddress}
          closeModal={closeModal}
          listIsSuccess={listIsSuccess}
          listLenght={list.length}
          isAutoLocation={isAutoLocation}
          isCityValid={isCityValid}
          onResetAutoLocation={onResetAutoLocation}
          onAddressChange={onAddressChange}
        />
      )}
    </>
  );
};

const AddressMemo = memo(AddressComponent);
export default enhance(AddressMemo);
