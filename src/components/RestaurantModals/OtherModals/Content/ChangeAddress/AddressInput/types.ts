// Main Types
import { RefObject } from 'react';
import { TNewAddressesList } from '@type/new-address';
import { TNewGeocodeData } from '@type/new-geocode';
import {
  TFetchNewAddressesSuggest,
  TResetNewAddressesSuggest,
} from '@actions/profile/new-address/action-types';
import { TProfileGetNewGeocode } from '@actions/profile/new-geocode/action-types';
import { TCitiesList } from '@/type/cities';

export interface TAddressInputProps {
  getGeocode: TProfileGetNewGeocode;
  fetchSuggest: TFetchNewAddressesSuggest;
  resetSuggest: TResetNewAddressesSuggest;
  closeModal: () => void;
  initialAddress: string;
  geocode: TNewGeocodeData;
  list: TNewAddressesList;
  listIsSuccess: boolean;
  geocodeIsPending: boolean;
  cityId: string;
  citiesList: TCitiesList;
  AutoLocationFooterRef: RefObject<HTMLDivElement>;
}

export interface TOption {
  id?: string;
  name: string;
  description: string;
}

export interface TGeoState {
  name: string;
  description: string;
}

export { TNewGeocodeData, TNewAddressesList };
