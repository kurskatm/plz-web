export interface TMessageProps {
  initialAddress: string;
  address: string;
  closeModal: () => void;
  listIsSuccess: boolean;
  listLenght: number;
  isAutoLocation: boolean;
  isCityValid: boolean;
  onResetAutoLocation: () => void;
  onAddressChange: () => void;
}
