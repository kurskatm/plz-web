// Modules
import React, { FC, memo, useMemo } from 'react';
// Assets
import AddressMap from '@/../assets/svg/map/change-address-map-icon.svg';
// Components
import { Button } from 'chibbis-ui-kit';
// Types
import { TMessageProps } from './types';

const AddressModalMessageComponent: FC<TMessageProps> = ({
  address,
  initialAddress,
  closeModal,
  listIsSuccess,
  listLenght,
  isAutoLocation,
  isCityValid,
  onResetAutoLocation,
  onAddressChange,
}) => {
  const renderMessage = useMemo(() => {
    if (!isAutoLocation && !isCityValid) {
      return (
        <div className="change-address-modal__message-empty">
          <img src={AddressMap} alt="address-map" />
          <div className="change-address-modal__message-text">Нас здесь нет</div>
        </div>
      );
    }

    if (!address || (isAutoLocation && !isCityValid)) {
      return (
        <div className="change-address-modal__message-empty">
          <img src={AddressMap} alt="address-map" />
          <div className="change-address-modal__message-text">Укажите адрес доставки</div>
        </div>
      );
    }

    if (initialAddress.length && initialAddress === address) {
      return (
        <div className="change-address-modal__message-empty">
          <img src={AddressMap} alt="address-map" />
          <div className="change-address-modal__message-text">Вы сейчас здесь?</div>
          <div className="change-address-modal__message-buttons">
            <Button
              className="change-address-modal__message-button"
              onClick={closeModal}
              htmlType="button"
              size="small"
              type="primary"
            >
              Да
            </Button>
            <Button
              className="change-address-modal__message-button"
              onClick={onAddressChange}
              htmlType="button"
              size="small"
              type="secondary"
            >
              Нет
            </Button>
          </div>
        </div>
      );
    }

    if (isAutoLocation && isCityValid) {
      return (
        <div className="change-address-modal__message-empty">
          <img src={AddressMap} alt="address-map" />
          <div className="change-address-modal__message-text">{address}</div>
          <Button
            className="change-address-modal__message-button"
            onClick={onResetAutoLocation}
            htmlType="button"
            size="small"
            type="primary"
          >
            Подтвердить
          </Button>
        </div>
      );
    }

    if (address && listIsSuccess && !listLenght) {
      return (
        <div className="change-address-modal__message-no-data">
          <span>Адрес не найден</span>
          <span>Попробуйте ввести адрес по-другому</span>
        </div>
      );
    }

    return null;
  }, [
    initialAddress,
    address,
    listIsSuccess,
    listLenght,
    closeModal,
    onResetAutoLocation,
    onAddressChange,
    isAutoLocation,
    isCityValid,
  ]);

  return <div className="change-address-modal__message">{renderMessage}</div>;
};

export default memo(AddressModalMessageComponent);
