// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { fetchNewAddressesSuggest, resetNewAddressesSuggest } from '@actions/profile/new-address';
import { profileGetNewGeocode } from '@actions/profile/new-geocode';
// Selectors
import { changeAddressInputModalStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/restaurant/modal';

const mapDispatchToProps = {
  fetchSuggest: fetchNewAddressesSuggest,
  resetSuggest: resetNewAddressesSuggest,
  getGeocode: profileGetNewGeocode,
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
