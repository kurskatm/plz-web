// Modules
import React, {
  FC, memo, useCallback, useMemo,
} from 'react';
import classnames from 'classnames';
// Types
import { TAddressListProps, TOption } from './types';

const AddressListComponent: FC<TAddressListProps> = ({
  showList,
  list,
  listRef,
  onOptionSelect,
  activeAddress,
}) => {
  const onClick = useCallback((option: TOption) => onOptionSelect(option), [onOptionSelect]);

  const renderList = useMemo(
    () => ({ name, description }: TOption, idx: number) => (
      <div
        key={`${name}-${description}`}
        role="presentation"
        className={classnames('change-address-modal-list__item', {
          'change-address-modal-list__item-active': idx === activeAddress,
        })}
        onClickCapture={(e) => {
          e.stopPropagation();
          onClick({ name, description });
        }}
      >
        <div
          className={classnames('change-address-modal-list__item-name', {
            'change-address-modal-list__item-name-active': idx === activeAddress,
          })}
        >
          {name}
        </div>
        <div
          className={classnames('change-address-modal-list__item-description', {
            'change-address-modal-list__item-description-active': idx === activeAddress,
          })}
        >
          {description}
        </div>
      </div>
    ),
    [onClick, activeAddress],
  );

  const renderContent = useMemo(
    () => () => {
      if (showList) {
        return list.map(renderList);
      }

      return null;
    },
    [list, renderList, showList],
  );

  return (
    <div className="change-address-modal-list">
      <div ref={listRef} className="change-address-modal-list__inner">
        {renderContent()}
      </div>
    </div>
  );
};

const AddressListMemo = memo(AddressListComponent);
export default AddressListMemo;
