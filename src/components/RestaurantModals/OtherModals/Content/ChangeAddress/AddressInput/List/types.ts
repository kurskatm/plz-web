// Modules Types
import { RefObject } from 'react';
// Main Types
import { TNewAddressesList } from '@type/new-address';
import { TOption } from '../types';

export interface TAddressListProps {
  list: TNewAddressesList;
  listRef: RefObject<HTMLDivElement>;
  onOptionSelect: (option: TOption) => void;
  showList: boolean;
  activeAddress: number;
}

export { TOption };
