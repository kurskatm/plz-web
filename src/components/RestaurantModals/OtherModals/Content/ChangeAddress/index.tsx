// Modules
import React, { FC, memo, createRef } from 'react';
// Components
import AddressInput from './AddressInput';
import AddressFooter from './AddressFooter';
// Types
import { TChangeAddressModalBodyProps } from './types';

const AutoLocationFooterRef = createRef<HTMLDivElement>();

const ChangeAddressModalBodyComponent: FC<TChangeAddressModalBodyProps> = ({ closeModal }) => (
  <div className="change-address-modal__body">
    <div className="change-address-modal__body-input">
      <AddressInput closeModal={closeModal} AutoLocationFooterRef={AutoLocationFooterRef} />
    </div>
    <div className="change-address-modal__body-footer">
      <AddressFooter closeModal={closeModal} AutoLocationFooterRef={AutoLocationFooterRef} />
    </div>
  </div>
);

const ChangeAddressModalBodyMemo = memo(ChangeAddressModalBodyComponent);
export default ChangeAddressModalBodyMemo;
