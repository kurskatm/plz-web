// Modules
import React, { FC, memo, useCallback } from 'react';
import { useHistory } from 'react-router';
// Assets
import GoHome from '@/../assets/svg/main/go-back.svg';
import GeoMark from '@/../assets/svg/map/geo-mark.svg';
// Lib
import {
  removeGeoCode, removeAddress, removeCookieCityData, removeCartId,
} from '@lib/cookie';
// Enhance
import { enhance } from './enhance';
// Types
import { TAddressFooterProps } from './types';

const AddressFooterComponent: FC<TAddressFooterProps> = ({
  closeModal,
  profileCityUpdate,
  profileResetDataAferCityUpdate,
  AutoLocationFooterRef,
}) => {
  const history = useHistory();

  const onGoHomeClick = useCallback(
    () => {
      removeCookieCityData();
      removeGeoCode();
      removeAddress();
      removeCartId();
      profileCityUpdate(null);
      profileResetDataAferCityUpdate();
      closeModal();
      setTimeout(() => {
        history.push('/');
      }, 100);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  return (
    <div className="change-address-modal__footer-inner">
      <div
        className="change-address-modal__footer-item"
        role="presentation"
        onClick={onGoHomeClick}
      >
        <img src={GoHome} alt="go-home" width={8} height={14} />
        <span>Изменить город</span>
      </div>
      <div className="change-address-modal__footer-item" ref={AutoLocationFooterRef}>
        <img src={GeoMark} alt="geo-mark" width={14} height={14} />
        <span>Автоопределение</span>
      </div>
    </div>
  );
};

const AddressFooterMemo = memo(AddressFooterComponent);
export default enhance(AddressFooterMemo);
