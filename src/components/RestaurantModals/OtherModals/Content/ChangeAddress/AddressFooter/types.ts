// Main Types
import { RefObject } from 'react';
import {
  TProfileCityUpdate,
  TProfileResetDataAferCityUpdate,
} from '@actions/profile/city/action-types';

export interface TAddressFooterProps {
  closeModal: () => void;
  profileResetDataAferCityUpdate: TProfileResetDataAferCityUpdate;
  profileCityUpdate: TProfileCityUpdate;
  AutoLocationFooterRef: RefObject<HTMLDivElement>;
}
