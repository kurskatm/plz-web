// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { profileCityUpdate, profileResetDataAferCityUpdate } from '@actions/profile/city';

const mapDispatchToProps = {
  profileResetDataAferCityUpdate,
  profileCityUpdate,
};
const withConnect = connect(null, mapDispatchToProps);

export const enhance = compose(withConnect);
