// Main types
import { TRestaurantCardIdSave } from '@/actions/restaurant/main/id/action-types';
import { TUpdateShopingCart } from '@/actions/shoping-cart/change/action-types';
import { TRestaurantDataToUpdateCart } from '@/type/restaurants/restaurant-card';
import { TRestaurantShopingCart } from '@type/shoping-cart';

export interface TAnotherRestaurantProps {
  closeModal: () => void;
  dataToUpdateCart: TRestaurantDataToUpdateCart;
  link: string | null;
  shopingCartUpdate: TUpdateShopingCart;
  shoppingCartFetchPending: boolean;
  restaurantCardIdSave: TRestaurantCardIdSave;
  shopingCartRest: TRestaurantShopingCart;
}
