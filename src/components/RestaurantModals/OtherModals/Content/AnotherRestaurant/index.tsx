// Modules
import React, { FC, memo, useCallback } from 'react';
import { useHistory } from 'react-router';
import isNil from 'lodash/isNil';
// Lib
import { UpdateShopingCartManager } from '@lib/shoping-cart/update-cart';
import { UpdateShopingCartWithModifiersManager } from '@lib/shoping-cart/update-cart-with-modifiers';
// Modules Components
import { Button } from 'chibbis-ui-kit';
// Components
import AnotherElemArrow from '@components/AnotherElemArrow';
// Utils
import { trackEvent } from '@utils/analytics';
import { applyImageFallback } from '@/utils';
// Assets
import cardRestDefault from '@/../assets/svg/main/card-rest-default.svg';
// Enhance
import { enhance } from './enhance';
// Types
import { TAnotherRestaurantProps } from './types';

const AnotherRestaurantModalBodyComponent: FC<TAnotherRestaurantProps> = ({
  closeModal,
  dataToUpdateCart,
  shopingCartRest,
  link,
  shopingCartUpdate,
  restaurantCardIdSave,
}) => {
  const history = useHistory();

  const onCancelClick = useCallback(() => {
    closeModal();
  }, [closeModal]);

  const onConfirmClick = useCallback(() => {
    // @ts-ignore
    let newShopingCartState;
    if (!dataToUpdateCart.product.modifiers?.length) {
      newShopingCartState = new UpdateShopingCartManager(dataToUpdateCart).init();
    } else {
      newShopingCartState = new UpdateShopingCartWithModifiersManager(dataToUpdateCart).init();
    }

    shopingCartUpdate(newShopingCartState);
    closeModal();
    if (!isNil(link)) {
      restaurantCardIdSave({ id: dataToUpdateCart.restaurant.id });
      setTimeout(() => history.push(link), 50);
    }
    trackEvent('add-cart');
    if (dataToUpdateCart.product.inBonusPoint) {
      trackEvent('try_to_add_EZB');
    }
  }, [dataToUpdateCart, link, history, closeModal, shopingCartUpdate, restaurantCardIdSave]);

  return (
    <div className="another-restaurant-modal__body">
      <div className="another-restaurant-modal__body-text">
        Заменить блюда из ресторана
        <br />
        <div className="another-restaurant-modal__body-rest-name" onClickCapture={onCancelClick}>
          {shopingCartRest?.name}
          ?
        </div>
      </div>
      <div className="another-restaurant-modal__body-logos">
        <div className="another-restaurant-modal__body-logo" onClickCapture={onCancelClick}>
          <img
            src={shopingCartRest?.logo || shopingCartRest?.logoUrl || cardRestDefault}
            onError={applyImageFallback(cardRestDefault)}
            alt={shopingCartRest?.name}
            width="120"
            height="130"
          />
        </div>
        <div className="another-restaurant-modal__body-logo" onClickCapture={onConfirmClick}>
          <img
            src={dataToUpdateCart?.restaurant?.logo || cardRestDefault}
            onError={applyImageFallback(cardRestDefault)}
            alt={dataToUpdateCart?.restaurant?.name}
            width="140"
            height="150"
          />
        </div>
      </div>
      <AnotherElemArrow className="another-restaurant-modal__body-arrow" />
      <div className="another-restaurant-modal__body-buttons">
        <Button htmlType="button" type="secondary" onClick={onCancelClick}>
          Оставить
        </Button>
        <Button htmlType="button" type="primary" onClick={onConfirmClick}>
          Заменить
        </Button>
      </div>
    </div>
  );
};

const AnotherRestaurantModalBodyMemo = memo(AnotherRestaurantModalBodyComponent);
export default enhance(AnotherRestaurantModalBodyMemo);
