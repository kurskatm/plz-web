// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { shopingCartUpdate } from '@actions/shoping-cart/change';
import { restaurantCardIdSave } from '@actions/restaurant/main/id';
// Selectores
import { anotherRestaurantModalBodyStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/restaurant/modal';

const mapDispatchToProps = {
  shopingCartUpdate,
  restaurantCardIdSave,
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
