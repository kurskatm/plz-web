// Modules
import React, {
  FC, memo, useCallback, useMemo, useEffect,
} from 'react';
import classNames from 'classnames';
import { ModalWindow } from 'chibbis-ui-kit';
// Utils
import { trackEvent } from '@utils/analytics';
// Components
import AnotherProduct from '../Content/AnotherProduct';
import AnotherRestaurant from '../Content/AnotherRestaurant';
import ChangeAddress from '../Content/ChangeAddress';
import NotEnoughPoints from '../Content/NotEnoughPoints';
// Enhance
import { enhance } from './enhance';
// Types
import { TRestaurantsOtherModalsProps } from './types';

const RestaurantsOtherModalsComponent: FC<TRestaurantsOtherModalsProps> = ({
  showRestaurantModals,
  showModal,
  modalType,
  restBonusPoints,
  dataToUpdateCart,
  link,
}) => {
  useEffect(() => {
    if (showModal && modalType === 'change-address') {
      trackEvent('show-address-popup');
    }
  }, [modalType, showModal]);

  const closeModal = useCallback(() => {
    showRestaurantModals({
      showModal: false,
      modalType: '',
    });
  }, [showRestaurantModals]);

  const renderModalBodyContent = useMemo(() => {
    if (modalType === 'not-enough-points') {
      return (
        <NotEnoughPoints
          // @ts-ignore
          restBonusPoints={restBonusPoints}
        />
      );
    }

    if (modalType === 'another-point-product') {
      return <AnotherProduct closeModal={closeModal} dataToUpdateCart={dataToUpdateCart} />;
    }

    if (modalType === 'another-restaurant') {
      return (
        <AnotherRestaurant
          closeModal={closeModal}
          dataToUpdateCart={dataToUpdateCart}
          link={link}
        />
      );
    }

    if (modalType === 'change-address') {
      return <ChangeAddress closeModal={closeModal} />;
    }

    return null;
  }, [closeModal, dataToUpdateCart, modalType, restBonusPoints, link]);

  const getModalStyles = useCallback(
    () =>
      classNames('restaurant-other-modals', {
        'not-enoght-points-restaurant': modalType === 'not-enough-points',
        'another-point-product-restaurant': modalType === 'another-point-product',
        'product-from-another-restaurant': modalType === 'another-restaurant',
        'change-address-restaurant': modalType === 'change-address',
      }),
    [modalType],
  );

  return (
    <ModalWindow
      classNameWrapper={getModalStyles()}
      closable
      setVisible={closeModal}
      visible={showModal}
    >
      {renderModalBodyContent}
    </ModalWindow>
  );
};

const RestaurantsOtherModalsMemo = memo(RestaurantsOtherModalsComponent);
export default enhance(RestaurantsOtherModalsMemo);
