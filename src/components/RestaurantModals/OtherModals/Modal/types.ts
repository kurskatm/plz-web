// Main types
import { TRestaurantDataToUpdateCart } from '@/type/restaurants/restaurant-card';
import { TShowRestaurantModals } from '@actions/restaurant/modal/action-types';

export interface TRestaurantsOtherModalsProps {
  showRestaurantModals: TShowRestaurantModals;
  showModal: boolean;
  modalType:
  | 'not-enough-points'
  | 'another-restaurant'
  | 'another-point-product'
  | 'change-address'
  | '';
  restBonusPoints: number;
  dataToUpdateCart: TRestaurantDataToUpdateCart;
  link: string | null;
}
