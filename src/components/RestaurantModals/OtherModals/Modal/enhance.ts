// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { showRestaurantModals } from '@actions/restaurant/modal';
// Selectores
import { restaurantOtherModalsShowStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/restaurant/modal';

const mapDispatchToProps = {
  showRestaurantModals,
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
