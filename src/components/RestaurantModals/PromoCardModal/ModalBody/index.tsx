// Modules
import React, { FC, memo } from 'react';
// Types
import { TPromoCardModalBodyProps } from './types';

const PromoCardModalBodyComponent: FC<TPromoCardModalBodyProps> = ({ promo }) => (
  <div className="restaurant-promo-card-modal__body">{promo.description}</div>
);

export default memo(PromoCardModalBodyComponent);
