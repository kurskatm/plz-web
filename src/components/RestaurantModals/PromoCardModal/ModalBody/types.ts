// Main types
import { TRestaurantPromo } from '@type/restaurants/promo';

export interface TPromoCardModalBodyProps {
  promo: TRestaurantPromo;
}
