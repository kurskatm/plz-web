// Main types
import { TRestaurantPromo } from '@type/restaurants/promo';
import { TShowPromoCardModal } from '@actions/restaurant/modal/action-types';

export interface TPromoCardModalProps {
  showPromoCardModal: TShowPromoCardModal;
  showModal: boolean;
  promo: TRestaurantPromo;
  index: number;
}
