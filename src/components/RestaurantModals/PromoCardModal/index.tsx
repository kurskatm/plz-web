// Modules
import React, {
  FC, memo, useCallback, useMemo,
} from 'react';
import { ModalWindow } from 'chibbis-ui-kit';
import isNil from 'lodash/isNil';
import classNames from 'classnames';
// Components
import ModalBody from './ModalBody';
// Enhance
import { enhance } from './enhance';
// Types
import { TPromoCardModalProps } from './types';

const RestaurantModifiersCardModalComponent: FC<TPromoCardModalProps> = ({
  showModal,
  promo,
  index,
  showPromoCardModal,
}) => {
  const closeModal = useCallback(
    () =>
      showPromoCardModal({
        showModal: false,
        promoId: null,
      }),
    [showPromoCardModal],
  );

  const headerModal = useMemo(() => {
    if (!isNil(promo)) {
      return <div className="restaurant-promo-card-modal__header">{promo.name}</div>;
    }

    return null;
  }, [promo]);

  const getWrapperStyles = useCallback(
    () => classNames('restaurant-promo-card-modal', `restaurant-promo-card-modal__${index % 3}`),
    [index],
  );

  return (
    <ModalWindow
      setVisible={closeModal}
      visible={showModal}
      classNameWrapper={getWrapperStyles()}
      headerContent={headerModal}
    >
      {!isNil(promo) && <ModalBody promo={promo} />}
    </ModalWindow>
  );
};

const RestaurantModifiersCardModalMemo = memo(RestaurantModifiersCardModalComponent);
export default enhance(RestaurantModifiersCardModalMemo);
