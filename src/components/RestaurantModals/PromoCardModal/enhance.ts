// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { showPromoCardModal } from '@actions/restaurant/modal';
// Selectores
import { promoModalStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/restaurant/modal';

const mapDispatchToProps = {
  showPromoCardModal,
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
