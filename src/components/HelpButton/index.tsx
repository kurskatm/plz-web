// Modules
import React, { FC, memo, useCallback } from 'react';
import { useParams } from 'react-router-dom';
import urlJoin from 'url-join';
// Modules Components
import { Button, Link } from 'chibbis-ui-kit';
// Utils
import { Consts } from '@utils';
// Main Types
import { TCityRouterParams } from '@type/routes';
// Types
import { THelpButtonProps } from './types';

const { INFO_PAGES } = Consts.ROUTES;

const HelpButtonComponent: FC<THelpButtonProps> = ({ isText }) => {
  const { cityUrl = '/' } = useParams<TCityRouterParams>();

  const getUrl = useCallback(() => {
    const url = urlJoin(`/${cityUrl}`, INFO_PAGES.HELP.PATH);
    return url;
  }, [cityUrl]);

  return (
    <Button className="help-button" htmlType="button" size="small" type="white">
      <Link to={getUrl()}>
        <div className="help-button__icon" />
        {isText && <div className="help-button__text">Помощь</div>}
      </Link>
    </Button>
  );
};

HelpButtonComponent.defaultProps = {
  isText: false,
};

export default memo(HelpButtonComponent);
