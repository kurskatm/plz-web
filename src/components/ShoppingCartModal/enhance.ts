// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { showShoppingCardModal } from '@actions/restaurant/modal';
// Selectores
import { showShoppingCardStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/header';

const mapDispatchToProps = {
  showShoppingCardModal,
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
