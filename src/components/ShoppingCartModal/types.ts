// Main types
import { TShowShoppingCardModal } from '@actions/restaurant/modal/action-types';

export interface TShoppingCartModalProps {
  showShoppingCardModal: TShowShoppingCardModal;
  showModal: boolean;
}
