// Modules
import React, { FC, memo, useCallback } from 'react';
import { useMediaQuery } from 'react-responsive';

// Components
import { ModalWindow } from 'chibbis-ui-kit';
import ShopingCart from '@components/ShopingCart';
// Enhance
import { enhance } from './enhance';
// Types
import { TShoppingCartModalProps } from './types';

const ShoppingCartModalComponent: FC<TShoppingCartModalProps> = ({
  showModal,
  showShoppingCardModal,
}) => {
  const closeModal = useCallback(
    () => {
      showShoppingCardModal(false);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const isMobile = useMediaQuery({
    query: '(max-width: 768px)',
  });

  return (
    <ModalWindow
      classNameWrapper="shopping-cart-modal"
      setVisible={closeModal}
      visible={showModal && isMobile}
      closable
    >
      <ShopingCart isModal />
    </ModalWindow>
  );
};

const ShoppingCartModalMemo = memo(ShoppingCartModalComponent);
export default enhance(ShoppingCartModalMemo);
