export interface TBlackLayoutProps {
  children?: unknown;
  open?: boolean;
  time?: number;
}
