// Modules
import React, {
  FC,
  memo,
  useCallback,
  useEffect,
  useState,
  // useRef,
} from 'react';
import classnames from 'classnames';
// Main Types
// import { TTimeoutId } from '@type/timers';
// Types
import { TBlackLayoutProps } from './types';

const BlackLayoutComponent: FC<TBlackLayoutProps> = ({
  children,
  open,
  // time,
}) => {
  // const timeoutIdRef = useRef<TTimeoutId>(null);

  const [view, setView] = useState<boolean>(open);
  const [ui, setUi] = useState<boolean>(open);

  useEffect(() => {
    if (open !== view) {
      // clearTimeout(timeoutIdRef.current);

      if (open) {
        setView(true);
        window.requestAnimationFrame(() => setUi(true));
      } else {
        setUi(false);
        setView(false);
        // timeoutIdRef.current = setTimeout(() => {
        //   setView(false);
        // }, time);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [open]);

  // useEffect(
  //   () => () => {
  //     clearTimeout(timeoutIdRef.current);
  //   },
  //   [],
  // );

  const getStyles = useCallback(
    () =>
      classnames(
        'black-layout',
        `black-layout-view-${view ? 'on' : 'off'}`,
        `black-layout-ui-${ui ? 'on' : 'off'}`,
      ),
    [ui, view],
  );

  return <div className={getStyles()}>{children}</div>;
};

BlackLayoutComponent.defaultProps = {
  open: false,
  time: 300,
};

export default memo(BlackLayoutComponent);
