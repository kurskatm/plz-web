export type TStatusBadgeProps = {
  color?: string;
  className?: string;
  children: unknown;
};
