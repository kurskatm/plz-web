import React, { FC, memo, useCallback } from 'react';
import classNames from 'classnames';

import { TStatusBadgeProps } from './types';

const StatusBadge: FC<TStatusBadgeProps> = ({ color = 'default', children, className }) => {
  const getStyles = useCallback(
    () => classNames('status-badge', `status-badge_color_${color}`, className),
    [color, className],
  );

  return <div className={getStyles()}>{children}</div>;
};

export default memo(StatusBadge);
