export interface TMenuItem {
  id: string;
  localizedName: string | JSX.Element;
  route: string;
}

export type TMenuConfig = TMenuItem[];

export interface TMenuProps {
  active: string;
  menuConfig: TMenuConfig;
}
