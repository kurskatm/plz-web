// Modules
import React, {
  FC, memo, useCallback, useMemo,
} from 'react';
import { useHistory } from 'react-router-dom';
import classNames from 'classnames';
// Types
import { TMenuProps, TMenuItem } from './types';

const MenuComponent: FC<TMenuProps> = ({ active, menuConfig }) => {
  const { push } = useHistory();

  const onClick = useCallback(
    (id, route) => () => {
      if (id !== active) {
        push(route);
      }
    },
    [active, push],
  );

  const renderItem = useMemo(
    () => ({ id, localizedName, route }: TMenuItem) => (
      <div
        key={id}
        role="presentation"
        className={classNames('main-menu__wrap', active === id && 'main-menu__wrap-active')}
        onClick={onClick(id, route)}
      >
        {localizedName}
      </div>
    ),
    [active, onClick],
  );

  return <div className="main-menu">{menuConfig.map(renderItem)}</div>;
};

export default memo(MenuComponent);
