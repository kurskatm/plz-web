import React, { FC, useMemo, memo } from 'react';
// Components
import { ScoresSocialsCard } from 'chibbis-ui-kit';
// Types
import { TScoresSocialsProps, TScoreSocialsCard } from './types';

const ScoresSocials: FC<TScoresSocialsProps> = ({ socialsList = [], onClick }) => {
  const renderList = useMemo(
    () =>
      socialsList.map(({
        image, score, activated, link, follow, points,
      }: TScoreSocialsCard) => (
        <div
          className="scores-socials__item"
          key={`${score}-${link}`}
          role="presentation"
          onClick={() => onClick(points)}
        >
          <ScoresSocialsCard
            activated={activated}
            image={image}
            score={score}
            link={link}
            follow={follow}
          />
        </div>
      )),
    [socialsList, onClick],
  );
  return <div className="scores-socials">{renderList}</div>;
};

export default memo(ScoresSocials);
