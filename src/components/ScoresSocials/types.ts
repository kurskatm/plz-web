export type TScoreSocialsCard = {
  image: string;
  score?: string;
  activated: boolean;
  follow?: boolean;
  link: string;
  points: number;
};

export interface TScoresSocialsProps {
  socialsList: TScoreSocialsCard[];
  onClick?: (data: number) => void;
}
