/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, {
  FC, memo, useCallback, useEffect, useState,
} from 'react';
import classnames from 'classnames';
import { isMobile, isIOS } from 'react-device-detect';
import { getCookieSmartBanner, setCookieSmartBanner } from '@lib/cookie';
import SmartBannerLogo from '@/../assets/img/header/smartbanner_logo.png';
import SmartBannerStars from '@/../assets/img/header/smartbanner_stars.png';
import { getAppLink } from './utils';

import { TMobileSmartBannerProps } from './types';

const MobileSmartBanner: FC<TMobileSmartBannerProps> = () => {
  const isAlreadyShowed = getCookieSmartBanner();
  const link = getAppLink(isIOS);

  const [isVisible, setVisible] = useState(isMobile && !isAlreadyShowed);
  const hideBannder = useCallback(() => {
    setCookieSmartBanner('showed');
    setVisible(false);
  }, [setVisible]);
  const getStyles = useCallback(
    () =>
      classnames('mobile-smart-banner', {
        'mobile-smart-banner_is-visible': isVisible,
      }),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [isVisible],
  );

  useEffect(() => {
    const timeoutId = setTimeout(hideBannder, 20000);
    return () => {
      clearTimeout(timeoutId);
      hideBannder();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className={getStyles()}>
      <div className="mobile-smart-banner__wrapper pad-m">
        <div className="mobile-smart-banner__close" onClick={hideBannder} />
        <div className="mobile-smart-banner__info">
          <img alt="smartbanner" className="mobile-smart-banner__logo" src={SmartBannerLogo} />
          <div className="mobile-smart-banner__desc">
            <div className="mobile-smart-banner__text">Chibbis — Доставка еды</div>
            <img className="mobile-smart-banner__rating" alt="rating" src={SmartBannerStars} />
          </div>
        </div>
        <a href={link} target="_blank" rel="noreferrer" className="mobile-smart-banner__link">
          Установить
        </a>
      </div>
    </div>
  );
};

const MobileSmartBannerMemo = memo(MobileSmartBanner);
export default MobileSmartBannerMemo;
