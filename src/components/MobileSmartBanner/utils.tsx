import { Consts } from '@/utils';

const { IS_CLIENT } = Consts.ENV;

export const getAppLink = (isIOS: boolean): string => {
  let result = '';
  if (IS_CLIENT) {
    if (/huawei/i.test(window?.navigator?.userAgent)) {
      result = 'https://redirect.appmetrica.yandex.com/serve/171490864286238295';
    } else if (isIOS) {
      result = 'https://redirect.appmetrica.yandex.com/serve/25843223574431127';
    } else {
      result = 'https://redirect.appmetrica.yandex.com/serve/1179573625829732881';
    }
  }
  return result;
};
