export interface TContentProps {
  children: unknown;
  className?: string;
}
