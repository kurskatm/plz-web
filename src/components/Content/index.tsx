// Modules
import React, { FC, useCallback, memo } from 'react';
import classnames from 'classnames';
// Types
import { TContentProps } from './types';

const ContentComponent: FC<TContentProps> = ({ children, className }) => {
  const getStyles = useCallback(() => classnames('content-body', className), [className]);

  return <div className={getStyles()}>{children}</div>;
};

export default memo(ContentComponent);
