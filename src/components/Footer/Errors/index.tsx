// Modules
import loadable from '@loadable/component';
import React, { FC, memo } from 'react';
// Modules Components
import { Copyright, Divider } from 'chibbis-ui-kit';

const Content = loadable(() => import('@components/Content'));

const FooterErrorsComponent: FC = () => (
  <div className="footer__errors">
    <Divider />
    <Content className="pad-f">
      <div className="footer__errors-inner">
        <div className="footer__errors__copyright">
          <Copyright str="Chibbis, все права защищены" />
        </div>
      </div>
    </Content>
  </div>
);

export default memo(FooterErrorsComponent);
