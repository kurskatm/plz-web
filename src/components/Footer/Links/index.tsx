// Modules
import loadable from '@loadable/component';
import React, { FC, memo, useCallback } from 'react';
import { useParams } from 'react-router-dom';
import { useMediaQuery } from 'react-responsive';
// import isNil from 'lodash/isNil';
// Modules Components
import { Schedule } from 'chibbis-ui-kit';
// Lib
import { getFooterMenuList } from '@lib/links/footer';
// Main Types
import { TCityRouterParams } from '@type/routes';
// Components
import SocialNetworks from '../SocialNetworks';
import Apps from '../Apps';
// Enhance
import { enhance } from './enhance';
// Types
import { TFooterProps } from './types';

const Content = loadable(() => import('@components/Content'));

const FooterLinksComponent: FC<TFooterProps> = ({ profileAuth }) => {
  const { cityUrl } = useParams<TCityRouterParams>();

  const isTabletOrMobile = useMediaQuery({
    query: '(max-width: 768px)',
  });

  const getFooterMenu = useCallback(() => getFooterMenuList({ cityUrl, profileAuth }), [
    cityUrl,
    profileAuth,
  ]);

  return (
    <Content className="pad-f">
      <div className="footer__links">
        <div className="footer__links-title">Полезные ссылки</div>
        <div className="footer__links__wrap">
          <div className="footer__links__menu">
            <Schedule
              className="footer__schedule"
              items={getFooterMenu()}
              type={isTabletOrMobile ? 'vertical' : 'horizontal'}
            />
            <div className="footer__links__social-wrap">
              <SocialNetworks isTabletOrMobile={false} />
            </div>
          </div>
          <div className="footer__links__app-wrap">
            <Apps />
          </div>
        </div>
      </div>
    </Content>
  );
};

const FooterLinksMemo = memo(FooterLinksComponent);
export default enhance(FooterLinksMemo);
