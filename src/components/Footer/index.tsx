// Modules
import React, { FC, memo } from 'react';
// Components
import FooterCompany from './Company';
import FooterLinks from './Links';
import FooterInfo from './Info';
import FooterSpecializations from './Specializations';
import Ceo from '../Ceo';

const FooterComponent: FC = () => (
  <div className="footer">
    <Ceo />
    <FooterSpecializations />
    <FooterCompany />
    <FooterLinks />
    <FooterInfo />
  </div>
);

export default memo(FooterComponent);
