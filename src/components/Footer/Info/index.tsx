// Modules
import loadable from '@loadable/component';
import React, { FC, memo } from 'react';
import isString from 'lodash/isString';
// Modules Components
import { Copyright } from 'chibbis-ui-kit';
// Enhance
import { enhance } from './enhance';
// Types
import { TFooterInfoProps } from './types';

const Content = loadable(() => import('@components/Content'));

const FooterInfoComponent: FC<TFooterInfoProps> = ({ name }) => (
  <div className="footer__info">
    <Content className="pad-f">
      <div className="footer__info-inner">
        <div className="footer__info__copyright">
          <Copyright str="Chibbis, все права защищены" />
        </div>
        {isString(name) && <div className="footer__info__address">{name}</div>}
      </div>
    </Content>
  </div>
);

export default enhance(memo(FooterInfoComponent));
