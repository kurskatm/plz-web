// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Selectors
import { footerAuthInfoStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/footer';

const withConnect = connect(mapStateToProps, null);

export const enhance = compose(withConnect);
