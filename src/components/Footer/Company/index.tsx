/* eslint-disable @typescript-eslint/no-unused-vars */
// Modules
import loadable from '@loadable/component';
import React, {
  FC, memo, useCallback, useMemo,
} from 'react';
import isNil from 'lodash/isNil';
import { useParams } from 'react-router-dom';
import { useMediaQuery } from 'react-responsive';
// Modules Components
import {
  // Schedule,
  Link,
} from 'chibbis-ui-kit';
// Lib
import { getFooterCompanyLinks } from '@lib/links/footer';
import { TScheduleItem } from '@lib/links/types';
// Main Types
import { TCityRouterParams } from '@type/routes';
import skresident from '@/../assets/svg/footer/skresidentIcon.svg';
// Enhance
import { enhance } from './enhance';
// Types
import { TFooterProps } from './types';

const Content = loadable(() => import('@components/Content'));

const FooterCompanyComponent: FC<TFooterProps> = ({ profileAuth }) => {
  const { cityUrl } = useParams<TCityRouterParams>();
  const isTabletOrWeb = useMediaQuery({
    minWidth: 769,
    maxWidth: 1100,
  });

  const getFooterMenu = useCallback(() => getFooterCompanyLinks({ cityUrl, profileAuth }), [
    cityUrl,
    profileAuth,
  ]);

  const renderLinks = useMemo(
    () => (item: TScheduleItem, index: number) => {
      if (!item.outside) {
        if (isTabletOrWeb && !isNil(profileAuth) && index === getFooterMenu().length - 1) {
          return (
            <>
              <br key={item.id} />
              <Link
                to={item.router}
                className="footer__company__link-item"
                border={false}
                color="black"
                key={item.id}
              >
                {item.name}
              </Link>
            </>
          );
        }
        return (
          <Link
            to={item.router}
            className="footer__company__link-item"
            border={false}
            color="black"
            key={item.id}
          >
            {item.name}
          </Link>
        );
      }

      return (
        <Link
          to={item.router}
          className="footer__company__link-item"
          border={false}
          outside
          target="_blank"
          color="black"
          key={item.id}
        >
          {item.name}
        </Link>
      );
    },
    [getFooterMenu, isTabletOrWeb, profileAuth],
  );

  return (
    <Content className="pad-f">
      <div className="footer__company">
        <div className="footer__company-items">
          <div className="footer__company-title">Компания</div>
          <div className="footer__company__menu">
            <div className="footer__company__links-list">{getFooterMenu().map(renderLinks)}</div>
          </div>
        </div>
        <div className="footer__company-icon">
          <Link
            className="footer__company-link"
            to="https://sk.ru/"
            border={false}
            outside
            target="_blank"
          >
            <img src={skresident} alt="skresident" width="67" height="50" />
          </Link>
        </div>
      </div>
    </Content>
  );
};

const FooterCompanyMemo = memo(FooterCompanyComponent);
export default enhance(FooterCompanyMemo);
