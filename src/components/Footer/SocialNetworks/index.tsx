// Modules
import React, { FC, memo } from 'react';
// Modules Components
import { Divider, SocialButtons } from 'chibbis-ui-kit';
// Lib
import { socialNetworkList } from '@lib/social-networks';
// Types
import { TSocialNetworks } from './types';

const SocialNetworksComponent: FC<TSocialNetworks> = ({ isTabletOrMobile }) => (
  <div className="footer__social-networks">
    {isTabletOrMobile && (
      <div>
        <Divider prefix="Мы в соцсетях" />
      </div>
    )}

    <div>
      <SocialButtons items={socialNetworkList} />
    </div>
  </div>
);

export default memo(SocialNetworksComponent);
