// Modules
import React, { FC, memo } from 'react';
import loadable from '@loadable/component';
import Specializations from '@components/Specializations';
import { SPECS_LIST } from '@utils/specializations';

const Content = loadable(() => import('@components/Content'));

const FooterSpecializations: FC = () => (
  <div className="footer__specs">
    <Content className="pad-f">
      <Specializations specializations={SPECS_LIST} links />
    </Content>
  </div>
);

export default memo(FooterSpecializations);
