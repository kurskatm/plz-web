// Modules
import loadable from '@loadable/component';
import React, { FC, memo } from 'react';

const Content = loadable(() => import('@components/Content'));
const HelpButton = loadable(() => import('@components/HelpButton'));

const FooterTopComponent: FC = () => (
  <Content className="pad-f">
    <div className="footer__top">
      <div className="footer__top__help">
        <HelpButton isText />
      </div>
    </div>
  </Content>
);

export default memo(FooterTopComponent);
