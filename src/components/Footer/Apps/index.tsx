// Modules
import React, { FC, memo } from 'react';
// Modules Components
import { AppButtons } from 'chibbis-ui-kit';
// Lib
import { installAppsList } from '@lib/app-buttons';

const AppsComponent: FC = () => (
  <div className="main__app-buttons">
    <AppButtons items={installAppsList} />
  </div>
);

export default memo(AppsComponent);
