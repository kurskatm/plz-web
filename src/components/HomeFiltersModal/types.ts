// Main types
import { TContent } from '@/type/content';
import { TFiltersShowModal } from '@actions/filters/action-types';

export interface THomeFiltersModalProps {
  bodyContent: TContent;
  showModal: boolean;
  filtersShowModal: TFiltersShowModal;
}
