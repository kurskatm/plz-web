// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { filtersShowModal } from '@actions/filters';
// Selectors
import { homeFiltersModalStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/home';

const mapDispatchToProps = {
  filtersShowModal,
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
