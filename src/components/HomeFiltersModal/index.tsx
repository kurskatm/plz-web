// Modules
import React, { FC, memo, useCallback } from 'react';
import { ModalWindow } from 'chibbis-ui-kit';
// Enhance
import { enhance } from './enhance';
// Types
import { THomeFiltersModalProps } from './types';

const HomeFiltersModalComponent: FC<THomeFiltersModalProps> = ({
  bodyContent,
  filtersShowModal,
  showModal,
}) => {
  const closeModal = useCallback(() => {
    filtersShowModal(false);
  }, [filtersShowModal]);

  return (
    <ModalWindow
      classNameWrapper="home-filters-modal"
      closable
      setVisible={closeModal}
      visible={showModal}
    >
      {bodyContent}
    </ModalWindow>
  );
};

const HomeFiltersModalMemo = memo(HomeFiltersModalComponent);
export default enhance(HomeFiltersModalMemo);
