// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Selectors
import { agreementStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/agreement';

const withConnect = connect(mapStateToProps);
export const enhance = compose(withConnect);
