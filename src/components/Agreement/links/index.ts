// Utils
import { Consts } from '@utils';

const { PATH } = Consts.ROUTES.INFO_PAGES.REGLAMENT;

export const userAgreementLink = (city: string): string => `/${city}${PATH}`;

export const privacyPolicyLink = 'https://chibbis.ru/privacy-policy';
