// Modules
import React, { FC, memo } from 'react';
import { CheckboxField } from 'chibbis-ui-kit';
// Config
import { userAgreementLink, privacyPolicyLink } from './links';
// Enhance
import { enhance } from './enhance';
// Types
import { TAgreementProps } from './types';

const licenseAgreementComponent: FC<TAgreementProps> = ({ city }) => (
  <span className="agreement">
    {'Согласен с '}
    <a href={userAgreementLink(city?.urlName)} target="_blank" rel="noreferrer">
      пользовательским соглашением
    </a>
    {' и '}
    <a href={privacyPolicyLink} target="_blank" rel="noreferrer">
      политикой конфиденциальности
    </a>
  </span>
);

const licenseAgreement = enhance(licenseAgreementComponent);

const AgreementComponent: FC = () => (
  <CheckboxField name="agreement" required view={licenseAgreement} />
);

export default memo(AgreementComponent);
