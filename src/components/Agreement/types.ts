// Main types
import { TCitiesItem } from '@type/cities';

export interface TAgreementProps {
  city: TCitiesItem;
}
