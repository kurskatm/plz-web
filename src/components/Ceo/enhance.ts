// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Selectors
import { getCity as mapStateToProps } from '@selectors/selectors/cities';

const withConnect = connect(mapStateToProps);

export const enhance = compose(withConnect);
