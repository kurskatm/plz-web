import React, { FC, memo } from 'react';
import { enhance } from './enhance';

import { TCeoComponentProps } from './types';

const CeoComponent: FC<TCeoComponentProps> = ({ whereName = 'вашем городе' }) => (
  <div className="ceo pad-f">
    <div className="ceo__wrapper">
      <div className="ceo__title">{`Chibbis \u2013 Cервис заказ еды на дом в ${whereName}`}</div>
      <p className="ceo__description">
        {`Онлайн заказ и доставка еды в ${whereName}.
        Все скидки и акции от ресторанов в одном месте на сервисе Чиббис (Чиббис, Chibbis)`}
      </p>
    </div>
  </div>
);

const CeoComponentMemo = memo(CeoComponent);
export default enhance(CeoComponentMemo);
