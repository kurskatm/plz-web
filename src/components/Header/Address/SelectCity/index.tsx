// Modules
import React, { FC, memo, useCallback } from 'react';
import classNames from 'classnames';
// Enhance
import { enhance } from './enhance';
// Types
import { TSelectCityProps } from './types';

const SelectCityComponent: FC<TSelectCityProps> = ({ citiesModalShow, city, showModal }) => {
  const onShowModal = useCallback(() => {
    citiesModalShow(true);
  }, [citiesModalShow]);

  const getImgStyles = useCallback(
    () => classNames(!showModal ? 'header-select-city-img' : 'header-select-city-img-show'),
    [showModal],
  );

  return (
    <div className="header-select-city-inner">
      <button type="button" onClick={onShowModal} className="header-select-city-title">
        {city.name}
        <div className={getImgStyles()} />
      </button>
    </div>
  );
};

const SelectCityMemo = memo(SelectCityComponent);
export default enhance(SelectCityMemo);
