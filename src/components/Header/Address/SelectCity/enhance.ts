// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { citiesModalShow } from '@actions/cities';
// Selectors
import { citiesModalShowStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/cities-list';

const mapDispatchToProps = {
  citiesModalShow,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
