// Main Types
import { TCitiesItem } from '@type/cities';
import { TCitiesModalShow } from '@actions/cities/action-types';

export interface TSelectCityProps {
  showModal: boolean;
  city: TCitiesItem;
  citiesModalShow: TCitiesModalShow;
}
