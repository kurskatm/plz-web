// Modules
import React, {
  FC, memo, useCallback, useMemo, useEffect, useState,
} from 'react';
import classnames from 'classnames';
import isNil from 'lodash/isNil';
// Utils
import { Consts } from '@/utils';
// Components
import InputAddress from './InputAddress';
// Enhance
import { enhance } from './enhance';
// Types
import { TAddressProps } from './types';

const { IS_CLIENT } = Consts.ENV;

const AddressComponent: FC<TAddressProps> = ({ profile }) => {
  const [headerTabletMaxWidth, setHeaderTabletMaxWidth] = useState<number | null>(null);
  const [leftSectionWidth, setLeftSectionWidth] = useState<number>(
    IS_CLIENT && document.body.querySelector('.header__left-section')
      ? document.body.querySelector('.header__left-section')?.clientWidth
      : 105,
  );
  const [rightSectionWidth, setRightSectionWidth] = useState<number>(
    IS_CLIENT && document.body.querySelector('.header__right-section')
      ? document.body.querySelector('.header__right-section')?.clientWidth
      : 105,
  );

  useEffect(() => {
    if (IS_CLIENT) {
      const leftSectionResizeObserver = new ResizeObserver(() => {
        setLeftSectionWidth(document.body.querySelector('.header__left-section')?.clientWidth);
      });
      leftSectionResizeObserver.observe(document.body.querySelector('.header__left-section'));
    }
  }, []);

  useEffect(() => {
    if (IS_CLIENT) {
      const rightSectionResizeObserver = new ResizeObserver(() => {
        setRightSectionWidth(document.body.querySelector('.header__right-section')?.clientWidth);
      });
      rightSectionResizeObserver.observe(document.body.querySelector('.header__right-section'));
    }
  }, []);

  useEffect(() => {
    if (IS_CLIENT && !isNil(leftSectionWidth) && !isNil(rightSectionWidth)) {
      const resizeObserver = new ResizeObserver(() => {
        if (document.body.clientWidth < 768 || document.body.clientWidth > 1300) {
          setHeaderTabletMaxWidth(null);
        }
        if (document.body.clientWidth > 768 && document.body.clientWidth <= 1024) {
          setHeaderTabletMaxWidth(
            document.body.clientWidth - leftSectionWidth - rightSectionWidth - 96,
            // page content padding left + right = 96
          );
        }
        if (document.body.clientWidth > 1024 && document.body.clientWidth < 1300) {
          setHeaderTabletMaxWidth(
            document.body.clientWidth - leftSectionWidth - rightSectionWidth - 80 - 80,
            // page content padding left + right = 80
            // middle section padding left + right = 80
          );
        }
      });
      resizeObserver.observe(document.body);
    }
  }, [leftSectionWidth, rightSectionWidth]);

  const getStyles = useCallback(
    () => classnames('header__address-inner', profile.pickup && 'header__address-is-pickup'),
    [profile.pickup],
  );

  const headerStyles = useMemo(() => {
    if (IS_CLIENT && !isNil(headerTabletMaxWidth)) {
      return {
        maxWidth: headerTabletMaxWidth,
      };
    }
    return {
      maxWidth: '100%',
    };
  }, [headerTabletMaxWidth]);

  return (
    <div className="header__address" style={headerStyles}>
      <div className={getStyles()}>
        <div className="header__address__input-wrap">
          <InputAddress />
        </div>
      </div>
    </div>
  );
};

const AddressMemo = memo(AddressComponent);
export default enhance(AddressMemo);
