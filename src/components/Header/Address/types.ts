// Main Types
import { TProfileModel } from '@models/profile/types';

export interface TAddressProps {
  profile: TProfileModel;
}
