/* eslint-disable @typescript-eslint/no-explicit-any */

export interface TCookieFunctionArgs {
  name: string;
  value?: string;
  options?: any;
}

export type TCookieFunction = (args: TCookieFunctionArgs) => any;
