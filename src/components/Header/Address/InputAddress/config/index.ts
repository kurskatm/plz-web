/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-useless-escape */
// Types
import { TCookieFunction } from './types';

export const getCookie: TCookieFunction = (args) => {
  const matches = document.cookie.match(
    new RegExp(`(?:^|; )${args.name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1')}=([^;]*)`),
  );
  return matches ? decodeURIComponent(matches[1]) : null;
};

export const updateCookie: TCookieFunction = (args) => {
  args.options = {
    path: '/',
    // при необходимости добавьте другие значения по умолчанию
    ...args.options,
  };

  if (args.options.expires instanceof Date) {
    args.options.expires = args.options.expires.toUTCString();
  }

  let updatedCookie = `${encodeURIComponent(args.name)}=${encodeURIComponent(args.value)}`;

  for (const optionKey in args.options) {
    updatedCookie += `; ${optionKey}`;
    const optionValue = args.options[optionKey];
    if (optionValue !== true) {
      updatedCookie += `=${optionValue}`;
    }
  }

  document.cookie = updatedCookie;
};

export const deleteCookie: TCookieFunction = ({ name }: { name: string }) => {
  updateCookie({
    name,
    value: '',
    options: {
      'max-age': -1,
    },
  });
};
