// Main Types
import { TCitiesItem } from '@type/cities';
import { TShowRestaurantModals } from '@actions/restaurant/modal/action-types';

export interface TInputAddressProps {
  initialAddress: string;
  showRestaurantModals: TShowRestaurantModals;
  city: TCitiesItem;
}
