// Modules
import React, {
  FC, memo, useMemo, useCallback,
} from 'react';
import { useMediaQuery } from 'react-responsive';
// Assets
import LocationDot from '@/../assets/svg/header/location-dot.svg';
// Utils
import { Consts } from '@/utils';
// Enhance
import { enhance } from './enhance';
// Types
import { TInputAddressProps } from './types';

const { IS_CLIENT } = Consts.ENV;

const InputAddressComponent: FC<TInputAddressProps> = ({
  initialAddress,
  showRestaurantModals,
  city,
}) => {
  const isMobile = useMediaQuery({
    maxWidth: 768,
  });

  const onClick = useCallback(
    () => {
      showRestaurantModals({
        showModal: true,
        modalType: 'change-address',
      });
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const getValidText = useCallback((text: string) => {
    if (IS_CLIENT && document.body.clientWidth < 360 && text.length > 50) {
      return `${text.slice(0, 47)}...`;
    }

    if (IS_CLIENT && document.body.clientWidth < 390 && text.length > 60) {
      return `${text.slice(0, 57)}...`;
    }

    return text;
  }, []);

  const getAddress = useMemo(() => {
    if (!initialAddress.length) {
      return (
        <>
          <span className="header__input-address-valid">{city?.name}</span>
          <span className="header__input-address-empty">
            {isMobile ? 'Адрес доставки не указан' : 'Уточнить адрес'}
          </span>
        </>
      );
    }

    if (initialAddress && initialAddress.match(city?.name)) {
      return (
        <span className="header__input-address-valid">
          {getValidText(initialAddress.split(city.name)[1].slice(2))}
        </span>
      );
    }

    return <span className="header__input-address-valid">{getValidText(initialAddress)}</span>;
  }, [city?.name, initialAddress, getValidText, isMobile]);

  return (
    <div className="header__input-address">
      <div className="header__input-address-inner" role="presentation" onClickCapture={onClick}>
        {!isMobile && <img src={LocationDot} alt="location-dot" />}
        {getAddress}
      </div>
    </div>
  );
};

const InputAddressMemo = memo(InputAddressComponent);
export default enhance(InputAddressMemo);
