// Modules
import React, { FC, memo, useCallback } from 'react';
import throttle from 'lodash/throttle';
import classnames from 'classnames';
// Modules Components
import { CloseIcon } from 'chibbis-ui-kit';
// Lib
import { setPickup } from '@lib/cookie';
// Types
import { TPickupProps } from './types';

const PickupComponent: FC<TPickupProps> = ({ pickup, updatePickup }) => {
  const addPick = throttle(() => {
    if (!pickup) {
      setPickup(true);
      updatePickup(true);
    }
  }, 100);
  const addPickup = useCallback(addPick, [addPick]);

  const removePick = throttle(() => {
    if (pickup) {
      setPickup(false);
      updatePickup(false);
    }
  }, 100);
  const removePickup = useCallback(removePick, [removePick]);

  const getStyles = useCallback(
    () => classnames('header__pickup', pickup && 'header__pickup-is-pickup'),
    [pickup],
  );

  return (
    <div role="presentation" className={getStyles()} onClick={addPickup}>
      <div className="header__pickup__label">Самовывоз</div>
      {pickup && (
        <div role="presentation" className="header__pickup__close" onClick={removePickup}>
          <CloseIcon width={12} height={12} />
        </div>
      )}
    </div>
  );
};

export default memo(PickupComponent);
