// Main Types
import { TProfileUpdatePickup } from '@actions/profile/action-types';

export interface TPickupProps {
  pickup: boolean;
  updatePickup: TProfileUpdatePickup;
}
