// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Selectors
import { headerAddressStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/header';

const withConnect = connect(mapStateToProps);

export const enhance = compose(withConnect);
