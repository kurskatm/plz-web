// Modules
import React, {
  FC, memo, useMemo, useCallback,
} from 'react';
import isNil from 'lodash/isNil';
import classNames from 'classnames';
// Components
import Address from '../Address';
import Title from '../Title';
// Types
import { TMiddleSectionProps } from './types';

const MiddleSectionComponent: FC<TMiddleSectionProps> = ({ noAddress, title }) => {
  const getStyles = useCallback(
    () =>
      classNames('header__middle-section', { 'header__middle-section-with-address': !noAddress }),
    [noAddress],
  );

  const renderSection = useMemo(
    () => () => {
      if (!isNil(title)) {
        return <Title title={title} />;
      }

      if (!noAddress) {
        return <Address />;
      }

      return null;
    },
    [noAddress, title],
  );

  return <div className={getStyles()}>{renderSection()}</div>;
};

export default memo(MiddleSectionComponent);
