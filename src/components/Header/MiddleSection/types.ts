export interface TMiddleSectionProps {
  noAddress: boolean;
  title: string;
}
