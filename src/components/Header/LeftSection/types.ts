export interface LeftSectionProps {
  isTabletOrMobile: boolean;
  goBackLink?: string;
  goBackText?: string;
  noMenu?: boolean;
  toggleVisible: () => void;
}
