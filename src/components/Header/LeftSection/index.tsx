// Modules
import React, { FC, memo, useMemo } from 'react';
import isString from 'lodash/isString';
// Components
import Menu from '../Menu';
import Logo from '../Logo';
import GoBack from '../GoBack';
// Types
import { LeftSectionProps } from './types';

const LeftSectionComponent: FC<LeftSectionProps> = ({
  isTabletOrMobile,
  toggleVisible,
  goBackLink,
  goBackText,
  noMenu,
}) => {
  const renderSection = useMemo(
    () => () => {
      if (isString(goBackText)) {
        return (
          <GoBack
            goBackLink={goBackLink}
            goBackText={goBackText}
            isTabletOrMobile={isTabletOrMobile}
          />
        );
      }

      if (isTabletOrMobile && !noMenu) {
        return <Menu toggleVisible={toggleVisible} />;
      }

      if (!isTabletOrMobile || !noMenu) {
        return <Logo />;
      }

      return null;
    },
    [goBackLink, goBackText, isTabletOrMobile, noMenu, toggleVisible],
  );

  return <div className="header__left-section">{renderSection()}</div>;
};

export default memo(LeftSectionComponent);
