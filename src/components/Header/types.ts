// Main Types
import { TNewGeocodeData } from '@type/new-geocode';
import { THeaderUxSetHeight } from '@actions/header/action-types';
import { TCitiesItem } from '@type/cities';

export interface THeaderProps {
  initialAddress: string;
  geocode: TNewGeocodeData;
  headerUxSetHeight: THeaderUxSetHeight;
  goBackLink?: string;
  goBackText?: string;
  noAddress?: boolean;
  noMenu?: boolean;
  noRightSection?: boolean;
  title?: string;
  authModalShow?: () => void;
  cost?: number;
  costForPoints?: number;
  city: TCitiesItem;
  headerHeight: number;
}
