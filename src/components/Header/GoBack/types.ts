// Types
import { TCitiesItem } from '@type/cities';

export interface GoBackProps {
  city: TCitiesItem;
  goBackLink?: string;
  goBackText?: string;
  isTabletOrMobile: boolean;
}
