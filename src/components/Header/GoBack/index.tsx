// Modules
import React, {
  FC, memo, useCallback, useMemo,
} from 'react';
import { Link } from 'react-router-dom';
import { useLastLocation } from 'react-router-last-location';
import isString from 'lodash/isString';
import classnames from 'classnames';
// Utils
import { Consts } from '@utils';
// Enhance
import { enhance } from './enhance';
// Types
import { GoBackProps } from './types';

const { PATH } = Consts.ROUTES.CITIES;

const GoBackComponent: FC<GoBackProps> = ({
  city, goBackLink, goBackText, isTabletOrMobile,
}) => {
  const lastLocation = useLastLocation();

  const getStylesIcon = useCallback(
    () =>
      classnames(
        'header__go-back__icon',
        `header__go-back__icon-${isTabletOrMobile ? 'mobile' : 'desktop'}`,
      ),
    [isTabletOrMobile],
  );

  const getLink = useCallback(() => {
    if (isString(goBackLink)) {
      return goBackLink;
    }

    if (isString(lastLocation)) {
      return lastLocation;
    }

    return city?.urlName || PATH;
  }, [city, goBackLink, lastLocation]);

  const getText = useMemo(
    () => () => {
      if (isString(goBackText)) {
        return goBackText;
      }

      return 'Назад';
    },
    [goBackText],
  );

  return (
    <div className="header__go-back">
      <Link to={getLink()}>
        <div className="header__go-back__inner">
          <div className={getStylesIcon()} />
          {!isTabletOrMobile && <div className="header__go-back__text">{getText()}</div>}
        </div>
      </Link>
    </div>
  );
};

const GoBackMemo = memo(GoBackComponent);
export default enhance(GoBackMemo);
