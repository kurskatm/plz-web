// Modules
import React, { FC, memo } from 'react';
// Types
import { TTitleProps } from './types';

const TitleComponent: FC<TTitleProps> = ({ title }) => <div className="header__title">{title}</div>;

export default memo(TitleComponent);
