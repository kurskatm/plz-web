// Modules
import React, { FC, memo } from 'react';

const HeaderErrorComponent: FC = () => (
  <div className="header__error__logo">
    <div className="header__error__logo-inner" />
  </div>
);

export default memo(HeaderErrorComponent);
