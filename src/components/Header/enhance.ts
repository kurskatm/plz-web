// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { headerUxSetHeight } from '@actions/header';
import { authModalShow } from '@actions/auth';
// Selectors
import { headerStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/header';

const mapDispatchToProps = {
  headerUxSetHeight,
  authModalShow,
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
