// Modules
import React, {
  FC, memo, useCallback, useMemo,
} from 'react';
import isNil from 'lodash/isNil';
import { Link, AvatarIcon } from 'chibbis-ui-kit';
// Lib
import { getAvatarIcon } from '@lib/get-avatar-icon';
// Enhance
import { enhance } from './enhance';
// Types
import { TProfileProps } from './types';

const ProfileComponent: FC<TProfileProps> = ({ profile, city, toggleVisible }) => {
  const showModal = useCallback(() => toggleVisible(true), [toggleVisible]);
  const getLink = `/${city?.urlName}/profile/info`;
  const getAvatar = useMemo(() => {
    if (!isNil(profile.info.data)) {
      const Icon = getAvatarIcon(profile.info.data.avatar);
      return <Icon width={20} height={20} />;
    }

    return <AvatarIcon />;
  }, [profile.info]);

  const getLinkName = useCallback(() => {
    if (!isNil(profile.info.data) && !!profile.info.data.fullName) {
      return profile.info.data.fullName;
    }

    return 'Мой кабинет';
  }, [profile.info]);

  const renderButton = useMemo(() => {
    if (!profile.auth) {
      return (
        <div className="header__profile" onClickCapture={showModal}>
          <AvatarIcon />
          <div className="header__profile__title">Войти</div>
        </div>
      );
    }

    return (
      <Link className="header__profile-text" to={getLink}>
        <div className="header__profile">
          {getAvatar}
          <div className="header__profile__title">{getLinkName()}</div>
        </div>
      </Link>
    );
  }, [profile.auth, getLinkName, getAvatar, showModal, getLink]);

  return renderButton;
};

const ProfileMemo = memo(ProfileComponent);
export default enhance(ProfileMemo);
