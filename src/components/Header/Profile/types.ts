import { TCitiesItem } from '@/type/cities';
import { TProfileModel } from '@models/profile/types';

export interface TProfileProps {
  profile: TProfileModel;
  toggleVisible?: (data: boolean) => void;
  city: TCitiesItem;
}
