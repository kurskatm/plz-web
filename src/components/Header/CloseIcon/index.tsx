// Modules
import React, { FC, memo } from 'react';

const CloseIconComponent: FC = () => (
  <div className="bold-close-icon-black">
    <div className="bold-close-icon-black_inner" />
  </div>
);

export default memo(CloseIconComponent);
