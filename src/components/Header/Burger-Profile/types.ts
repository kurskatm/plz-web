import { TProfileModel } from '@models/profile/types';
import { TCitiesItem } from '@type/cities';

export interface TProfileProps {
  profile: TProfileModel;
  balance: number;
  toggleVisible?: (data: boolean) => void;
  city: TCitiesItem;
}
