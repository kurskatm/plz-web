// Modules
import React, {
  FC, memo, useCallback, useMemo,
} from 'react';
import isNil from 'lodash/isNil';
import { useParams } from 'react-router-dom';
import urlJoin from 'url-join';
import { Link, AvatarIcon } from 'chibbis-ui-kit';
// Lib
import { getAvatarIcon } from '@lib/get-avatar-icon';
// Main Types
import { TCityRouterParams } from '@type/routes';
// Utils
import { Consts } from '@utils';
// Enhance
import { enhance } from './enhance';
// Types
import { TProfileProps } from './types';

const { PATH: ProfileUrl } = Consts.ROUTES.PROFILE_PAGES.INFO;
const { PATH: FreeFoodUrl } = Consts.ROUTES.INFO_PAGES.FOOD;

const ProfileComponent: FC<TProfileProps> = ({ profile, balance, toggleVisible }) => {
  const { cityUrl = '' } = useParams<TCityRouterParams>();

  const showModal = useCallback(() => toggleVisible(true), [toggleVisible]);

  const getProfileLink = useMemo(() => urlJoin(cityUrl, ProfileUrl), [cityUrl]);
  const getFreeFoodLink = useMemo(() => urlJoin(cityUrl, FreeFoodUrl), [cityUrl]);

  const getAvatar = useMemo(() => {
    if (!isNil(profile.info.data)) {
      const Icon = getAvatarIcon(profile.info.data.avatar);
      return <Icon width={20} height={20} />;
    }

    return <AvatarIcon />;
  }, [profile.info]);

  const getLinkName = useMemo(() => {
    if (!isNil(profile.info.data)) {
      if (profile.info.data.fullName?.length > 10) {
        return (
          <span
            style={{
              maxWidth: '96px',
              whiteSpace: 'nowrap',
              overflow: 'hidden',
              textOverflow: 'ellipsis',
            }}
          >
            {profile.info.data.fullName}
          </span>
        );
      }
      return <span>{profile.info.data.fullName || 'Мой кабинет'}</span>;
    }

    return <span>Мой кабинет</span>;
  }, [profile.info]);

  const getPoints = useMemo(() => {
    if (!isNil(profile.info.data) && !isNil(balance)) {
      let newBalanceForm = '';
      if (balance < 1000) {
        newBalanceForm = balance.toString();
      }
      if (balance > 1000 && balance < 1000000) {
        newBalanceForm = `${balance.toString().slice(0, 1)} ${balance.toString().slice(1)}`;
      }
      if (balance > 1000000) {
        newBalanceForm = `${balance.toString().slice(0, 1)} ${balance
          .toString()
          .slice(1, 4)} ${balance.toString().slice(4)}`;
      }
      return (
        <>
          <span>{`У вас ${newBalanceForm}`}</span>
          <div className="header-burger__profile__points-img" />
        </>
      );
    }

    return null;
  }, [profile.info, balance]);

  const renderButton = useMemo(() => {
    if (!profile.auth) {
      return (
        <div className="header-burger__profile" onClickCapture={showModal}>
          <AvatarIcon />
          <div className="header-burger__profile__title">Войти</div>
        </div>
      );
    }

    return (
      <Link to={getProfileLink}>
        <div className="header-burger__profile__info">
          <div className="header-burger__profile__main">
            {getAvatar}
            {getLinkName}
          </div>
          <div className="header-burger__profile__points">{getPoints}</div>
          <div className="header-burger__profile__free-food">
            <Link to={getFreeFoodLink}>Как потратить баллы?</Link>
          </div>
        </div>
      </Link>
    );
  }, [profile.auth, getAvatar, getLinkName, getPoints, showModal, getProfileLink, getFreeFoodLink]);

  return renderButton;
};

const ProfileMemo = memo(ProfileComponent);
export default enhance(ProfileMemo);
