export interface TMenuProps {
  toggleVisible: () => void;
}
