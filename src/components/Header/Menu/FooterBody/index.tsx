// Modules
import React, { FC, memo, useCallback } from 'react';
import { useParams } from 'react-router-dom';
// Modules Components
import { Schedule } from 'chibbis-ui-kit';
// Lib
import { getFooterCompanyLinks, getFooterMenuList } from '@lib/links/footer';
// Main Types
import { TCityRouterParams } from '@type/routes';

const FooterComponent: FC = () => {
  const { cityUrl } = useParams<TCityRouterParams>();

  const getFooterMenuLinks = useCallback(() => getFooterMenuList({ cityUrl }), [cityUrl]);

  const getFooterMenuCompany = useCallback(() => getFooterCompanyLinks({ cityUrl }), [cityUrl]);

  return (
    <div className="header-menu__footer">
      <div className="header-menu__footer__links">
        <div className="header-menu__footer__links-title">Компания</div>
        <Schedule items={getFooterMenuCompany()} />
      </div>
      <div className="header-menu__footer__info">
        <div className="header-menu__footer__links-title">Полезные ссылки</div>
        <Schedule items={getFooterMenuLinks()} />
      </div>
    </div>
  );
};

export default memo(FooterComponent);
