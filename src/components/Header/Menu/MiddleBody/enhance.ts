// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { cuisinesFetch, cuisinesPush, cuisinesUpdate } from '@actions/cuisines';
// Selectors
import { headerMenuMiddleStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/header';

const mapDispatchToProps = {
  cuisinesFetch,
  cuisinesPush,
  cuisinesUpdate,
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
