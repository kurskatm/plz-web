// Main Types
import { RefObject } from 'react';
import { TCuisinesFetch, TCuisinesPush, TCuisinesUpdate } from '@actions/cuisines/action-types';
import { TCitiesItem } from '@type/cities';
import { TCuisine } from '@type/cuisines';

export interface TMiddleProps {
  city: TCitiesItem;
  cuisines: TCuisine[];
  cuisinesIsIdle: boolean;
  cuisinesFetch: TCuisinesFetch;
  cuisinesPush: TCuisinesPush;
  cuisinesUpdate: TCuisinesUpdate;
  cuisinesLinkRef: RefObject<HTMLDivElement>;
}

export interface TItemNav {
  title: string;
  to: string;
}
