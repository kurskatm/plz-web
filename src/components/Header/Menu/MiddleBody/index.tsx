// Modules
import React, {
  FC, memo, useCallback, useEffect, useMemo,
} from 'react';
import { Link, useHistory } from 'react-router-dom';
// Main Types
import { TCuisine } from '@type/cuisines';
// Enhance
import { enhance } from './enhance';
// Types
import { TMiddleProps, TItemNav } from './types';

const MiddleComponent: FC<TMiddleProps> = ({
  city,
  cuisinesIsIdle,
  cuisinesFetch,
  // cuisinesPush,
  cuisinesUpdate,
  cuisines,
  cuisinesLinkRef,
}) => {
  const { location } = useHistory();

  useEffect(
    () => {
      if (cuisinesIsIdle) {
        cuisinesFetch();
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const isHome = useCallback(() => city && location.pathname === `/${city?.urlName}`, [
    city,
    location.pathname,
  ]);

  const isRestaurantsPage = useCallback(
    () => location.pathname === `/${city?.urlName}/restaurants`,
    [city?.urlName, location.pathname],
  );

  const onLinkClick = useCallback(
    (id: string) => {
      cuisinesUpdate([id]);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const renderNavItem = useMemo(
    () => ({ title, to }: TItemNav) => (
      <div className="header-menu-middle__nav__item" ref={cuisinesLinkRef}>
        <Link
          to={to}
          onClick={() => {
            cuisinesUpdate([]);
            if (isHome()) {
              window.scrollTo({
                top: 160,
                behavior: 'smooth',
              });
            }
          }}
        >
          <div className="header-menu-middle__nav__item-inner">{title}</div>
        </Link>
      </div>
    ),
    [cuisinesLinkRef, cuisinesUpdate, isHome],
  );

  const renderItem = useMemo(
    () => ({ id, name, urlName }: TCuisine) => (
      <div key={id} className="header-menu-middle__nav__item">
        <Link
          to={`/${city?.urlName}/restaurants/${urlName}`}
          onClick={() => {
            onLinkClick(id);
            cuisinesLinkRef.current.click();
            if (isHome()) {
              window.scrollTo({
                top: 160,
                behavior: 'smooth',
              });
            }
          }}
        >
          <div className="header-menu-middle__nav__item-inner">{name}</div>
        </Link>
      </div>
    ),
    [city?.urlName, onLinkClick, cuisinesLinkRef, isHome],
  );

  return (
    <div className="header-menu-middle">
      <div className="header-menu-middle__nav">
        {!isHome()
          && renderNavItem({
            title: 'На главную',
            to: `/${city?.urlName}`,
          })}
        {!isRestaurantsPage()
          && renderNavItem({
            title: 'Рестораны',
            to: `/${city?.urlName}`,
          })}
        {cuisines.slice(0, 9).map(renderItem)}
      </div>
    </div>
  );
};

const MiddleMemo = memo(MiddleComponent);
export default enhance(MiddleMemo);
