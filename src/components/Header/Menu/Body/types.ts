// Modules Types
import { RefObject } from 'react';

export interface TMenuBodyProps {
  menuIsOpen: boolean;
  menuRef: RefObject<HTMLDivElement>;
  cuisinesLinkRef: RefObject<HTMLDivElement>;
  closeIconRef: RefObject<HTMLDivElement>;
  toggleVisible: () => void;
}
