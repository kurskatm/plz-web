// Modules
import React, {
  FC, memo, useCallback, useEffect, useState,
} from 'react';
import classNames from 'classnames';
// Main Types
import { TTimeoutId } from '@type/timers';
// Components
import MenuHeader from '../HeaderBody';
import MenuMiddle from '../MiddleBody';
import MenuFooter from '../FooterBody';
// Types
import { TMenuBodyProps } from './types';

let timeoutId: TTimeoutId;

const MenuBodyComponent: FC<TMenuBodyProps> = ({
  closeIconRef,
  menuIsOpen,
  menuRef,
  cuisinesLinkRef,
  toggleVisible,
}) => {
  const [view, setView] = useState<boolean>(menuIsOpen);

  useEffect(
    () => {
      if (menuIsOpen !== view) {
        clearTimeout(timeoutId);

        if (menuIsOpen) {
          setTimeout(() => setView(true), 25);
        } else {
          setView(false);
        }
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [menuIsOpen],
  );

  useEffect(
    () => () => {
      clearTimeout(timeoutId);
    },
    [],
  );

  const getStyles = useCallback(
    () => classNames('header__menu-body', `header__menu-body__${view ? 'open' : 'closed'}`),
    [view],
  );

  return (
    <div className={getStyles()} ref={menuRef}>
      <div className="header__menu-body__inner">
        <div className="header__menu-body__content">
          <div className="header__menu-body__content-inner">
            <MenuHeader toggleVisible={toggleVisible} closeIconRef={closeIconRef} />
            <MenuMiddle cuisinesLinkRef={cuisinesLinkRef} />
            <MenuFooter />
          </div>
        </div>
      </div>
    </div>
  );
};

export default memo(MenuBodyComponent);
