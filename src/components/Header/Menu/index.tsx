// Modules
import React, {
  createRef, FC, memo, useEffect, useState,
} from 'react';
// Main Components
import BlackLayout from '@components/BlackLayout';
// Lib
import { MenuManager } from '@lib/header';
// Components
import MenuBody from './Body';
// Types
import { TMenuProps } from './types';

const menuTriggerRef = createRef<HTMLDivElement>();
const menuRef = createRef<HTMLDivElement>();
const closeIconRef = createRef<HTMLDivElement>();
const cuisinesLinkRef = createRef<HTMLDivElement>();
const MenuComponent: FC<TMenuProps> = ({ toggleVisible }) => {
  const [menuIsOpen, setMenuIsOpen] = useState<boolean>(false);

  useEffect(
    () => {
      const menuManager = new MenuManager({
        initialMenuState: menuIsOpen,
        closeIconRef: closeIconRef.current,
        menuTriggerRef: menuTriggerRef.current,
        menuRef: menuRef.current,
        cuisinesLinkRef: cuisinesLinkRef.current,
        setMenuIsOpenReact: setMenuIsOpen,
      });

      return () => {
        menuManager.unsubscribe();
      };
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  return (
    <div className="header__menu">
      <div ref={menuTriggerRef} className="header__menu__icon">
        <div className="header__menu__icon-line" />
        <div className="header__menu__icon-line" />
        <div className="header__menu__icon-line" />
      </div>
      <BlackLayout open={menuIsOpen}>
        <MenuBody
          menuIsOpen={menuIsOpen}
          menuRef={menuRef}
          cuisinesLinkRef={cuisinesLinkRef}
          closeIconRef={closeIconRef}
          toggleVisible={toggleVisible}
        />
      </BlackLayout>
    </div>
  );
};

export default memo(MenuComponent);
