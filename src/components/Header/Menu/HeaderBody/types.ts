// Modules Types
import { RefObject } from 'react';

export interface TMenuHeaderProps {
  closeIconRef: RefObject<HTMLDivElement>;
  toggleVisible: () => void;
}
