// Modules
import React, { FC, memo } from 'react';
// import loadable from '@loadable/component';
// Components
import Profile from '../../Burger-Profile';
// Types
import { TMenuHeaderProps } from './types';

// const Search = loadable(() => import('../../../Search'));

const MenuHeaderComponent: FC<TMenuHeaderProps> = ({ closeIconRef, toggleVisible }) => (
  <div className="header-menu__header">
    <div className="header-menu__header__top">
      <div className="header-menu__header__top__logo-wrap" />

      <div ref={closeIconRef} className="header-menu__header__top__close-wrap" />
    </div>

    <div className="header-menu__header__top__profile-wrap">
      <Profile toggleVisible={toggleVisible} />
    </div>

    {/* <div className="header-menu__header__search-wrap">
      <Search />
    </div> */}
  </div>
);

export default memo(MenuHeaderComponent);
