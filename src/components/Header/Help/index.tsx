// Modules
import React, { FC, memo } from 'react';
import loadable from '@loadable/component';

const HelpButton = loadable(() => import('@components/HelpButton'));

const HelpComponent: FC = () => (
  <div className="header__help">
    <HelpButton />
  </div>
);

export default memo(HelpComponent);
