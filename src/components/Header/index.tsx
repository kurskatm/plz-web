// Modules
import React, {
  FC, memo, useEffect, useRef,
} from 'react';
import { isNil } from 'lodash';
import { AutoSizer } from 'react-virtualized';
import { useMediaQuery } from 'react-responsive';
import throttle from 'lodash/throttle';
// Components
import RestaurantsModal from '@components/RestaurantModals/OtherModals/Modal';
import AccountModal from '../AccountModal';
import ShoppingCartModal from '../ShoppingCartModal';
import LeftSection from './LeftSection';
import MiddleSection from './MiddleSection';
import RightSection from './RightSection';
import Notifications from './Notifications';
// Config
import { deleteCookie, getCookie } from './Address/InputAddress/config';
// Enhance
import { enhance } from './enhance';
// Types
import { THeaderProps } from './types';

const HeaderComponent: FC<THeaderProps> = ({
  initialAddress,
  geocode,
  headerUxSetHeight,
  goBackLink,
  goBackText,
  noAddress,
  noMenu,
  noRightSection,
  title,
  cost,
  costForPoints,
  city,
  authModalShow: toggleAccountVisible,
  headerHeight,
}) => {
  const onResize = useRef(
    throttle(({ height }) => {
      headerUxSetHeight(height);
    }, 1000),
  );

  useEffect(
    () => {
      if (initialAddress.length && !isNil(geocode) && geocode.precision !== 'exact') {
        const addressCookie = getCookie({ name: 'address' });
        const geodataCookie = getCookie({ name: 'geodata' });

        if (!isNil(addressCookie) && !isNil(geodataCookie)) {
          deleteCookie({ name: 'address' });
          deleteCookie({ name: 'geodata' });
        }
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const isTabletOrMobileLogo = useMediaQuery({
    query: '(max-width: 1024px)',
  });

  const isMobile = useMediaQuery({
    maxWidth: 768,
  });

  return (
    <AutoSizer disableWidth onResize={onResize.current} style={{ height: 'auto' }}>
      {() => (
        <div className="header">
          <AccountModal />
          <ShoppingCartModal />
          <RestaurantsModal />
          {/* <MobileSmartBanner /> */}
          <div className="header-inner">
            <div className="header-inner-wrap pad-m">
              <LeftSection
                isTabletOrMobile={isTabletOrMobileLogo}
                goBackLink={goBackLink}
                goBackText={goBackText}
                noMenu={noMenu}
                toggleVisible={toggleAccountVisible}
              />
              <MiddleSection noAddress={noAddress} title={title} />
              {!noRightSection && (
                <RightSection
                  cost={cost}
                  costForPoints={costForPoints}
                  city={city}
                  toggleVisible={toggleAccountVisible}
                  isMobile={isMobile}
                />
              )}
            </div>
          </div>
          {/* @ts-ignore */}
          <Notifications headerHeight={headerHeight ?? 64} />
        </div>
      )}
    </AutoSizer>
  );
};

HeaderComponent.defaultProps = {
  noAddress: false,
  noMenu: false,
  noRightSection: false,
};

const HeaderMemo = memo(HeaderComponent);
export default enhance(HeaderMemo);
