/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
// Modules
import classNames from 'classnames';
import React, {
  FC,
  memo,
  useCallback,
  useState,
  useRef,
  useEffect,
  useMemo,
} from 'react';
// Main types
import { TNotification } from '@/models/notifications/types';
// Enhance
import { enhance } from './enhance';
// Types
import { TNotificationsProps } from './types';

const NotificationsComponent: FC<TNotificationsProps> = ({
  headerHeight,
  notifications,
  removeNotification,
}) => {
  const [viewNotifications, setViewNotifications] = useState(notifications);
  const notificationsRefs = useRef<Map<string, HTMLDivElement>>(new Map());
  const timersRefs = useRef<Map<string, number>>(new Map());

  // For adding notifications
  useEffect(
    () => {
      if (notifications.length > viewNotifications.length) {
        const newNotifications = notifications.slice(viewNotifications.length);

        setViewNotifications(
          (prevNotifications) =>
            prevNotifications.concat(newNotifications),
        );

        // eslint-disable-next-line no-restricted-syntax
        for (const notif of newNotifications) {
          if (Number.isFinite(notif.duration)) {
            const timer = window.setTimeout(
              () => {
                removeNotification({
                  id: notif.id,
                });
              },
              notif.duration,
            );
            timersRefs.current.set(notif.id, timer);
          }
        }
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [notifications.length],
  );

  // For removing notifications
  const closeNotification = useCallback(
    (id: string) => {
      if (timersRefs.current.has(id)) {
        window.clearTimeout(timersRefs.current.get(id));
        timersRefs.current.delete(id);
      }

      if (notificationsRefs.current.has(id)) {
        // const notificationEl = notificationsRefs.current.get(id);
        notificationsRefs.current.delete(id);
        setViewNotifications(
          (prevNotifications) =>
            prevNotifications.filter((e) => e.id !== id),
        );

        // TODO: discover circumstances of this potential bug
        /*
        notificationEl.ontransitionend = () => {
          notificationEl.ontransitionend = () => {
            setViewNotifications(
              (prevNotifications) =>
                prevNotifications.filter((e) => e.id !== id),
            );
          };

          notificationEl.classList.add('hidden');
        };

        notificationEl.classList.add('faded-out');
        */
      }
    },
    [],
  );
  useEffect(
    () => {
      if (notifications.length < viewNotifications.length) {
        const oldNotifications = viewNotifications
          .filter((e) => !notifications.some(
            (e1) =>
              e1.id === e.id,
          ));

        // eslint-disable-next-line no-restricted-syntax
        for (const notif of oldNotifications) {
          closeNotification(notif.id);
        }
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [notifications.length],
  );

  // Cleanup hook
  useEffect(
    () =>
      () => {
        // eslint-disable-next-line no-restricted-syntax
        for (const [, timerDesc] of timersRefs.current) {
          window.clearTimeout(timerDesc);
        }
      },
    [],
  );

  const renderNotification = useCallback(
    (notification: TNotification) => {
      const className = classNames(
        'header__notification',
        notification.type,
      );

      return (
        <div
          key={notification.id}
          className={className}
          ref={(el) => {
            if (!notificationsRefs.current.has(notification.id)) {
              notificationsRefs.current.set(notification.id, el);
            }
          }}
          onClick={() => {
            removeNotification({
              id: notification.id,
            });
          }}
        >
          {notification.content}
        </div>
      );
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const rootStyle = useMemo(
    () =>
      ({
        top: `${headerHeight}px`,
      }),
    [headerHeight],
  );

  return (
    <div className="header__notification-list" style={rootStyle}>
      {viewNotifications.map(renderNotification)}
    </div>
  );
};

const NotificationsMemo = memo(NotificationsComponent);
export default enhance(NotificationsMemo);
