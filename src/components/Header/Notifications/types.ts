import { TRemoveNotification } from '@/actions/notifications/action-types';
import { TNotification } from '@models/notifications/types';

export interface TNotificationsProps {
  // From enhancer
  notifications: TNotification[];
  removeNotification: TRemoveNotification;
  // From raw props
  headerHeight: number;
}
