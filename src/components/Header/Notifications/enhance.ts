// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { removeNotification } from '@actions/notifications';
// Selectors
import { notificationsStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/notifications';

const mapDispatchToProps = {
  removeNotification,
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
