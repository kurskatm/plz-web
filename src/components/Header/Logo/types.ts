// Types
import { TCitiesItem } from '@type/cities';

export interface TLogoProps {
  city: TCitiesItem;
}
