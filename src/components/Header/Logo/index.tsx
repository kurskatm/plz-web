// Modules
import React, {
  FC, memo, useCallback, useMemo,
} from 'react';
import { useHistory } from 'react-router-dom';
import urlJoin from 'url-join';
// Modules Components
import { Link } from 'chibbis-ui-kit';
// Utils
import { Consts } from '@utils';
// Enhance
import { enhance } from './enhance';
// Types
import { TLogoProps } from './types';

const { CITIES } = Consts.ROUTES;

const LogoComponent: FC<TLogoProps> = ({ city }) => {
  const { location } = useHistory();

  const getHome = useCallback(() => {
    const endpoint = city?.urlName || '';
    return urlJoin(CITIES.PATH, endpoint);
  }, [city]);

  const isNotLink = useCallback(() => {
    const pathList = [getHome(), CITIES.PATH];

    return pathList.includes(location.pathname);
  }, [getHome, location.pathname]);

  const renderLogo = useMemo(
    () => () => {
      if (isNotLink()) {
        return <div className="header__logo-inner" />;
      }

      return (
        <Link to={getHome()}>
          <div className="header__logo-inner" />
        </Link>
      );
    },
    [getHome, isNotLink],
  );

  return <div className="header__logo">{renderLogo()}</div>;
};

const LogoMemo = memo(LogoComponent);
export default enhance(LogoMemo);
