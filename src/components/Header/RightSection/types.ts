import { TCitiesItem } from '@/type/cities';

export interface TRightSectionProps {
  isMobile: boolean;
  toggleVisible: () => void;
  cost?: number;
  costForPoints?: number;
  city: TCitiesItem;
}
