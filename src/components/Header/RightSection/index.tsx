// Modules
import React, { FC, memo } from 'react';
// Components
import Profile from '../Profile';
import Cart from '../Cart';
// import Help from '../Help';
// Types
import { TRightSectionProps } from './types';

const RightSectionComponent: FC<TRightSectionProps> = ({
  isMobile,
  toggleVisible,
  cost,
  costForPoints,
  city,
}) => (
  <div className="header__right-section">
    {!isMobile && (
      <div className="header__right-section__profile-wrap">
        <Profile city={city} toggleVisible={toggleVisible} />
      </div>
    )}
    <div className="header__right-section__cart-wrap">
      <Cart city={city} cost={cost} costForPoints={costForPoints} />
    </div>
  </div>
);

export default memo(RightSectionComponent);
