/* eslint-disable no-nested-ternary */
// Modules
import React, {
  FC, memo, useMemo, useCallback,
} from 'react';
import { useMediaQuery } from 'react-responsive';
import { useHistory } from 'react-router-dom';
import classnames from 'classnames';
// Modules Component
import { CartIcon } from 'chibbis-ui-kit';
// Assets
import ScoresIconInvert from '@/../assets/svg/shoping-cart/chibcoin-invert.svg';
// Utils
import { Consts } from '@utils';
// Enhance
import { enhance } from './enhance';
// Types
import { TCartProps } from './types';

const { PATH } = Consts.ROUTES.CHECKOUT.MAIN;

const RestPath = new RegExp('/[a-z]*/restaurant/');
const CheckoutPath = new RegExp('/[a-z]*/checkout');

const CartComponent: FC<TCartProps> = ({
  cost,
  costForPoints,
  city,
  showShoppingCardModal,
  shopingCartOptions,
  shopingCartRestaurant,
}) => {
  const history = useHistory();
  const { location } = history;

  const isMobileOrTablet = useMediaQuery({
    maxWidth: 1024,
  });
  const isMobile = useMediaQuery({
    maxWidth: 768,
  });

  // @ts-ignore
  const { total, minimalOrderCost } = shopingCartOptions;

  const checkoutUrl = `/${city?.urlName}${PATH}`;
  const restUrl = `/${city?.urlName}/restaurant/${shopingCartRestaurant?.urlName}/menu`;

  const isSubmitDisabled = useMemo(
    () =>
      // eslint-disable-next-line max-len
      total < minimalOrderCost,
    [minimalOrderCost, total],
  );
  // eslint-disable-next-line max-len
  const isShowModal = isMobile || location.pathname.match(RestPath) || location.pathname.match(CheckoutPath);

  const toRest = isSubmitDisabled && !location.pathname.match(RestPath);

  const onClick = useCallback(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (e: any) => {
      e.stopPropagation();
      if (isShowModal) {
        return showShoppingCardModal(true);
      }
      if (toRest) {
        history.push(restUrl);
        return null;
      }
      if (isSubmitDisabled) return null;
      history.push(checkoutUrl);
      showShoppingCardModal(false);
      return null;
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [isShowModal, toRest, location, isSubmitDisabled],
  );

  const getCost = useCallback((costNum: number) => {
    const costStr = String(costNum);
    if (costStr.length > 3) {
      return `${costStr.slice(0, costStr.length - 3)} ${costStr.slice(costStr.length - 3)}`;
    }

    return costStr;
  }, []);

  const getCartCostValue = useMemo(() => {
    if (!cost && !costForPoints) {
      return '';
    }

    if (!cost && costForPoints) {
      return (
        <span>
          {costForPoints}
          {' '}
          <img
            src={ScoresIconInvert}
            alt="chib-coin"
            style={{
              position: 'relative',
              top: 2,
            }}
          />
        </span>
      );
    }

    return `${getCost(cost)} ₽`;
  }, [cost, costForPoints, getCost]);

  const renderCartIcon = useMemo(() => {
    if (isMobile) {
      return <CartIcon active={!!cost || !!costForPoints} />;
    }

    if (isMobileOrTablet && !cost && !costForPoints) {
      return null;
    }

    return <CartIcon active={!!cost || !!costForPoints} />;
  }, [cost, costForPoints, isMobile, isMobileOrTablet]);

  const renderButtonContent = useMemo(
    () => (
      <div
        className={classnames('header__cart', {
          header__cart__clickable: !isSubmitDisabled || isShowModal || toRest,
        })}
        style={{
          backgroundColor: !cost && !costForPoints ? '#FFFFFF' : '#FF8610',
        }}
        role="presentation"
        onClick={onClick}
      >
        {renderCartIcon}
        {!isMobile && <div className="header__cart-cost">{getCartCostValue}</div>}
      </div>
    ),
    [
      isSubmitDisabled,
      isShowModal,
      isMobile,
      renderCartIcon,
      getCartCostValue,
      onClick,
      cost,
      costForPoints,
      toRest,
    ],
  );

  const renderButton = useMemo(() => {
    if (!cost && !costForPoints && !isMobile) {
      return null;
    }
    return renderButtonContent;
  }, [isMobile, renderButtonContent, cost, costForPoints]);

  return renderButton;
};

const CartMemo = memo(CartComponent);
export default enhance(CartMemo);
