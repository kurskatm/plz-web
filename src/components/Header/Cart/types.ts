// Main types
import { TCitiesItem } from '@/type/cities';
import { TShowShoppingCardModal } from '@actions/restaurant/modal/action-types';
import { TShopingCartOptions, TRestaurantShopingCart } from '@type/shoping-cart';

export interface TCartProps {
  showShoppingCardModal: TShowShoppingCardModal;
  cost: number;
  costForPoints: number;
  city: TCitiesItem;
  shopingCartOptions?: TShopingCartOptions;
  shopingCartRestaurant: TRestaurantShopingCart;
}
