// Modules
import React, {
  FC, memo, useEffect, useRef, useState,
} from 'react';
import { AutoSizer } from 'react-virtualized';
import isBoolean from 'lodash/isBoolean';
import throttle from 'lodash/throttle';
import isNil from 'lodash/isNil';
// Types
import { TCollapseHeightProps } from './types';

const CollapseHeightComponent: FC<TCollapseHeightProps> = ({
  children,
  delay,
  disableWidth,
  ignoreAll,
  updateCondition,
  preloadCondition,
}) => {
  const isFirstRender = useRef(false);

  const [currentListWidth, setCurrentListWidth] = useState(disableWidth ? null : 0);
  const [currentListHeight, setCurrentListHeight] = useState(0);
  const [listHeight, setListHeight] = useState(0);

  useEffect(() => {
    if (!isFirstRender.current && updateCondition) {
      isFirstRender.current = true;
    }
  }, [updateCondition]);

  useEffect(() => {
    const isBooleanFirst = isBoolean(preloadCondition);
    const isMount = isFirstRender.current;
    const condition = isBooleanFirst && !isMount ? preloadCondition : updateCondition;

    if ((ignoreAll || condition) && currentListHeight !== listHeight) {
      setListHeight(currentListHeight);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentListHeight, preloadCondition, updateCondition, listHeight]);

  const onResize = throttle(({ width, height }) => {
    if (!disableWidth && width !== currentListWidth) {
      setCurrentListWidth(width);
    }

    if (height !== currentListHeight) {
      setCurrentListHeight(height);
    }
  }, delay);

  return (
    <div
      className="collapse-height"
      style={{
        height: `${listHeight}px`,
        width: isNil(currentListWidth) ? 'auto' : `${currentListWidth}px`,
      }}
    >
      <div
        style={{
          position: disableWidth ? 'relative' : 'absolute',
        }}
      >
        <AutoSizer
          disableWidth={disableWidth}
          onResize={onResize}
          style={{ height: 'auto', width: 'auto' }}
        >
          {() => children}
        </AutoSizer>
      </div>
    </div>
  );
};

CollapseHeightComponent.defaultProps = {
  delay: 500,
  disableWidth: true,
  ignoreAll: false,
  updateCondition: false,
};

export default memo(CollapseHeightComponent);
