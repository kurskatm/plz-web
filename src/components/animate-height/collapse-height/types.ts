export interface TCollapseHeightProps {
  children: JSX.Element;
  delay?: number;
  disableWidth?: boolean;
  ignoreAll?: boolean;
  updateCondition?: boolean;
  preloadCondition?: boolean;
}
