// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { cuisinesPush } from '@actions/cuisines';
// Selectors
import { restaurantInfoSpecializationsStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/restaurant/info';

const mapDispatchToProps = {
  cuisinesPush,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
