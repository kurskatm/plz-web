// Moduls
import React, {
  FC, memo, useCallback, useMemo,
} from 'react';
import { useHistory } from 'react-router-dom';
import urlJoin from 'url-join';
import { stringify } from 'query-string';
// Modules Components
import { Button, Link } from 'chibbis-ui-kit';
// Main Types
import { TRestaurantCategory } from '@type/restaurants/restaurant-categories';
// Enhance
import { enhance } from './enhance';
// Types
import { TSpecializationsProps } from './types';

const options = {
  skipNull: true,
  skipEmptyString: true,
};

const SpecializationsComponent: FC<TSpecializationsProps> = ({
  city,
  specializations,
  cuisinesPush,
  links,
}) => {
  const history = useHistory();

  const onClick = useCallback(
    (id: string, urlName: string) => () => {
      const urlParams = { cuisines: [urlName] };
      const strUrlParams = stringify(urlParams, options);
      const url = urlJoin('/', `/${city?.urlName}`, `/?${strUrlParams}`);
      cuisinesPush(id);
      history.push(url);
    },
    [city, history, cuisinesPush],
  );

  const renderItem = useMemo(
    () => ({ id, name, urlName }: TRestaurantCategory) => (
      !links ? (
        <div key={id} className="rest-specializations-item">
          <Button htmlType="button" onClick={onClick(id, urlName)} size="small" type="white">
            {name}
          </Button>
        </div>
      ) : (
        <div key={id} className="rest-specializations-item">
          <Link
            border={false}
            outside
            noActive
            to={`/${city?.urlName}/restaurants/${urlName}`}
            className="rest-specializations-link"
          >
            {name}
          </Link>
        </div>
      )
    ),
    [onClick, links, city],
  );

  return (city?.urlName
    ? <div className="rest-specializations">{specializations.map(renderItem)}</div> : null);
};

const SpecializationsMemo = memo(SpecializationsComponent);
export default enhance(SpecializationsMemo);
