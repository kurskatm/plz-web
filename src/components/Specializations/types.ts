// Main Types
import { TCitiesItem } from '@type/cities';
import { TRestaurantCategoriesList } from '@type/restaurants/restaurant-categories';
import { TCuisinesPush } from '@actions/cuisines/action-types';

export interface TSpecializationsProps {
  city: TCitiesItem;
  specializations: TRestaurantCategoriesList;
  cuisinesPush: TCuisinesPush;
  links?: boolean;
}
