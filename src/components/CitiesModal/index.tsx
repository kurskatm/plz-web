// Modules
import React, { FC, memo, useCallback } from 'react';
import { ModalWindow } from 'chibbis-ui-kit';
import SelectCitiesList from '../CitiesModalSelect';
// Enhance
import { enhance } from './enhance';
// Types
import { TSelectCityModalProps } from './types';

const CitiesModalComponent: FC<TSelectCityModalProps> = ({ showModal, citiesModalShow }) => {
  const closeModal = useCallback((): unknown => citiesModalShow(false), [citiesModalShow]);
  // const toggleVisible = (show: boolean) => citiesModalShow(show);

  return (
    <ModalWindow
      classNameWrapper="select-city-modal"
      closable
      setVisible={closeModal}
      visible={showModal}
    >
      <SelectCitiesList />
    </ModalWindow>
  );
};

const CitiesModalMemo = memo(CitiesModalComponent);
export default enhance(CitiesModalMemo);
