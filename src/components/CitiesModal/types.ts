// Main Types
import { TCitiesModalShow } from '@actions/cities/action-types';

export interface TSelectCityModalProps {
  showModal: boolean;
  citiesModalShow: TCitiesModalShow;
}
