// Modules
import React, {
  FC, memo, useMemo, createRef, useState, useCallback, useEffect,
} from 'react';
import { isMobile, isAndroid, isIOS } from 'react-device-detect';
import isNil from 'lodash/isNil';
// Modules Components
import { AppButtons } from 'chibbis-ui-kit';
// Lib
import { installAppsList } from '@lib/app-buttons';
import { getUserId, getMobileAppInstallShow, setMobileAppInstallShow } from '@lib/cookie';

const InstallSocketComponent: FC = () => {
  const [showBlock, setShowBlock] = useState<boolean>(true);

  const closeIconRef = createRef<HTMLDivElement>();

  useEffect(() => {
    const showInstallMobileApp = getMobileAppInstallShow();
    if (!isNil(showInstallMobileApp)) {
      setShowBlock(false);
    }
  }, []);

  const onClose = useCallback((e) => {
    e.stopPropagation();
    setShowBlock(false);
    const userId = getUserId();
    if (!isNil(userId)) {
      setMobileAppInstallShow();
    }
  }, []);

  const renderLinkButton = useMemo(() => {
    if (window.navigator.userAgent.toLocaleLowerCase().match(/huawei/)) {
      return <AppButtons items={[installAppsList[2]]} />;
    }

    if (isAndroid) {
      return <AppButtons items={[installAppsList[0]]} />;
    }

    if (isIOS) {
      return <AppButtons items={[installAppsList[1]]} />;
    }

    return null;
  }, []);

  const renderUi = useMemo(() => {
    if (showBlock && isMobile) {
      return (
        <div className="install-socket">
          <div className="install-socket__inner">
            <div className="install-socket__mobile-application" />
            <div className="install-socket__content">
              <div
                className="install-socket__close-wrap"
                ref={closeIconRef}
                onClickCapture={onClose}
              />
              <div className="install-socket__text">
                Приложение
                <br />
                Chibbis
              </div>
              <div className="install-socket__app-buttons">{renderLinkButton}</div>
            </div>
          </div>
        </div>
      );
    }

    return null;
  }, [showBlock, closeIconRef, onClose, renderLinkButton]);

  return renderUi;
};

const InstallSocketMemo = memo(InstallSocketComponent);
export default InstallSocketMemo;
