// Main Types
import { TRestaurantCard } from '@type/restaurants/restaurant-card';
import { TCloseRestaurantModalShow } from '@actions/restaurant/modal/action-types';

export interface TRestaurantCloseTimerProps {
  restaurant: TRestaurantCard;
  closeRestaurantModalShow: TCloseRestaurantModalShow;
}
