// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { closeRestaurantModalShow } from '@actions/restaurant/modal';
// Selectors
import { shopingCartRestInfoStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/shoping-cart';

const mapDispatchToProps = {
  closeRestaurantModalShow,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
