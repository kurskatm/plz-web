/* eslint-disable @typescript-eslint/no-unused-vars */
// Modules
import React, {
  FC, memo, useMemo, useState, useEffect, useCallback, useRef,
} from 'react';
import isNil from 'lodash/isNil';
import debounce from 'lodash/debounce';
// Lib
import { getHoursAndMinutes } from '@lib/restaurants/getHoursAndMinutes';
// Modules Components
import { HourglassIcon } from 'chibbis-ui-kit';
import RestaurantCloseModal from '../RestaurantModals/CloseModal';
// Enhance
import { enhance } from './enhance';
// Types
import { TRestaurantCloseTimerProps } from './types';

const RestaurantCloseTimerComponent: FC<TRestaurantCloseTimerProps> = ({
  restaurant,
  closeRestaurantModalShow,
}) => {
  const [time, setTime] = useState<number>();
  const [date, setDate] = useState<number>();
  const [openingTime, setOpeningTime] = useState<string>('');

  const interval = useRef(null);
  const openModal = useRef(
    debounce(() => {
      closeRestaurantModalShow(true);
    }, 10),
  );

  const getTime = useCallback(() => {
    if (!isNil(date)) {
      setTime(Math.floor((date - Date.now()) / 60000));
    }
  }, [date]);

  useEffect(() => {
    if (!isNil(restaurant)) {
      const closeTime = getHoursAndMinutes(restaurant.workingTime.closeTime);
      const closeHours = closeTime.split(':')[0];
      const closeMinutes = closeTime.split(':')[1];
      const currentDate = new Date().setHours(+closeHours, +closeMinutes);
      const openTime = getHoursAndMinutes(restaurant.workingTime.openTime);
      setOpeningTime(openTime);
      setDate(currentDate);
      if (isNil(time)) {
        setTime(Math.floor((currentDate - Date.now()) / 60000));
      }
    }
  }, [restaurant, time]);

  useEffect(() => {
    if (time >= 0 && time <= 30) {
      interval.current = setInterval(getTime, 10000);
    }
    if (time === -1) {
      openModal.current();
    }

    return () => clearInterval(interval.current);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [time]);

  const renderUi = useMemo(() => {
    if (time >= 0 && time <= 30) {
      return (
        <div className="restaurant-close-timer">
          <div className="restaurant-close-timer__content">
            <HourglassIcon width={14} height={14} />
            Ресторан перестанет принимать заказы через
            {` ${time} мин`}
          </div>
        </div>
      );
    }

    return null;
  }, [time]);

  return (
    <>
      {renderUi}
      <RestaurantCloseModal openingTime={openingTime} />
    </>
  );
};

const RestaurantCloseTimerMemo = memo(RestaurantCloseTimerComponent);
export default enhance(RestaurantCloseTimerMemo);
