export type TLoadingComponentProps = {
  isLoading: boolean;
  timedOut: boolean;
  pastDelay: boolean;
  error: boolean;
};
