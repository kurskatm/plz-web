import React, { FC, memo } from 'react';

import { TLoadingComponentProps } from './types';

const LoadingComponent: FC<TLoadingComponentProps> = ({
  isLoading,
  timedOut,
  pastDelay,
  error,
}) => {
  if (isLoading) {
    if (timedOut) {
      return <div>Loader timed out!</div>;
    }

    if (pastDelay) {
      return <div>Loading...</div>;
    }
    return null;
  }

  if (error) {
    return <div>Error! Component failed to load</div>;
  }
  return null;
};

export default memo(LoadingComponent);
