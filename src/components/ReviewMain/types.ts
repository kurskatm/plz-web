// Main Types
import { TReviewMain } from '@type/reviews-main';
import { TRestaurantCardIdSave } from '@actions/restaurant/main/id/action-types';
import {
  TRestaurantCardFetchSuccess,
  TRestaurantCardSave,
} from '@/actions/restaurant/main/card/action-types';
import { TRestaurantCard } from '@/type/restaurants/restaurant-card';

export interface ReviewProps {
  item: TReviewMain;
  url: string;
  index: number;
  setItemHeight: (height: number, index: number) => void;
  bodyHeight: number;
  restaurant: TRestaurantCard;
  restaurantCardSave: TRestaurantCardSave;
  restaurantCardFetchSuccess: TRestaurantCardFetchSuccess;
  restaurantCardIdSave?: TRestaurantCardIdSave;
}
