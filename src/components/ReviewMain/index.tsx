/* eslint-disable @typescript-eslint/no-unused-vars */
// Modules
import React, {
  FC, memo, useCallback, useMemo, createRef, useEffect, useState,
} from 'react';
import { Link } from 'react-router-dom';
import Truncate from 'react-truncate';
// Utils
import { applyImageFallback } from '@/utils';
// Assets
import cardRestDefault from '@/../assets/svg/main/card-rest-default.svg';
// Main Types
import { TOrder } from '@type/reviews-main';
// Config
import { getDate } from '@lib/date/get-date';
// Enhance
import { enhance } from './enhance';
// Types
import { ReviewProps } from './types';

const ReviewMainPageComponent: FC<ReviewProps> = ({
  item,
  url,
  index,
  setItemHeight,
  bodyHeight,
  restaurant,
  restaurantCardIdSave,
  restaurantCardSave,
  restaurantCardFetchSuccess,
}) => {
  const bodyRef = createRef<HTMLDivElement>();

  const [height, setHeight] = useState<number>(bodyRef.current?.offsetHeight);

  useEffect(() => {
    const DIV = bodyRef.current;
    const cardResizeObserver = new ResizeObserver(() => {
      setItemHeight(DIV.offsetHeight, index);
    });
    cardResizeObserver.observe(DIV);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [index]);

  useEffect(() => {
    if (height !== bodyHeight) {
      setHeight(bodyHeight);
    }
  }, [bodyHeight, height]);

  const getDateCallback = useCallback((date: string) => getDate(date), []);

  const getUrl = useCallback((urlName: string) => `${url}/${urlName}/reviews`, [url]);

  const getText = useCallback((text: string, maxLength: number) => {
    if (text.length > maxLength) {
      return `${text.slice(0, maxLength - 3)}...`;
    }

    return text;
  }, []);

  const getBodyStyles = useCallback((length: number) => {
    if (length > 100) {
      return {
        height: 66,
        lines: 3,
      };
    }

    if (length > 50) {
      return {
        height: 44,
        lines: 2,
      };
    }

    return {
      height: 22,
      lines: 1,
    };
  }, []);

  const renderOrders = useMemo(() => (order: TOrder) => `${order.quantity} × ${order.name}`, []);
  const clickHandler = useCallback(() => {
    restaurantCardIdSave({ id: item.restaurant.id });
    if (restaurant) {
      restaurantCardSave(restaurant);
      restaurantCardFetchSuccess({ status: 200 });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [restaurantCardIdSave, restaurantCardSave, restaurant]);

  return (
    <div key={item.id} className="review-main-item">
      <Link className="review-main" to={getUrl(item.restaurant.urlName)} onClick={clickHandler}>
        <div className="review-main-item__inner">
          <div className="review-main-item-header">
            <div className="review-main-item-header-author">{item.username || 'Анонимно'}</div>
            <div className="review-main-item-header-date">{getDateCallback(item.addedOn)}</div>
          </div>
          <div className="review-main-item-body" ref={bodyRef} style={{ height }}>
            <div
              className="review-main-item-body-message"
              style={{
                marginBottom: item.order.length > 0 ? 18 : 0,
                maxHeight: getBodyStyles(item.text.length).height,
              }}
            >
              <Truncate lines={getBodyStyles(item.text.length).lines} trimWhitespace>
                {item.text}
              </Truncate>
            </div>
            {item.order.length > 0 && (
              <div className="review-main-item-body-order">
                {getText(item.order.map(renderOrders).join(', '), 90)}
              </div>
            )}
          </div>
          <div className="review-main-item-footer">
            <div className="review-main-item-footer-logo">
              <img
                src={item.restaurant.logo || cardRestDefault}
                onError={applyImageFallback(cardRestDefault)}
                alt={item.restaurant.name}
                width="31"
                height="33"
              />
            </div>
            <div className="review-main-item-footer-name">{item.restaurant.name}</div>
          </div>
        </div>
      </Link>
    </div>
  );
};

const ReviewMainPage = memo(ReviewMainPageComponent);
export default enhance(ReviewMainPage);
