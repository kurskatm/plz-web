// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { restaurantCardIdSave } from '@/actions/restaurant/main/id';
import { restaurantCardFetchSuccess, restaurantCardSave } from '@/actions/restaurant/main/card';
// Selectors
import { homeHotOfferStructuredSelector as mapStateToProps } from '@/selectors/structured-selectors/home';

const mapDispatchToProps = {
  restaurantCardIdSave,
  restaurantCardSave,
  restaurantCardFetchSuccess,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
