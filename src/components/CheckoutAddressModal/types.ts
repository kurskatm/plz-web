// Main Types
import {
  TChangeAddressModalShow,
  TChangeAddressModalHide2ndTime,
} from '@actions/checkout/modals/action-types';
import { TShowRestaurantModals } from '@actions/restaurant/modal/action-types';

export interface TChangeAddressModalProps {
  showModal: boolean;
  city: string;
  address: string;
  changeAddressModalShow: TChangeAddressModalShow;
  // A fix for showing "mismatch address" on redirection
  changeAddressModalHide2ndTime: TChangeAddressModalHide2ndTime;
  showRestaurantModals: TShowRestaurantModals;
}
