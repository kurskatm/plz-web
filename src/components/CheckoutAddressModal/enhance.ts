// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { changeAddressModalShow, changeAddressModalHide2ndTime } from '@actions/checkout/modals';
import { showRestaurantModals } from '@actions/restaurant/modal';
// Selectors
import { addressModalStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/checkout-modals';

const mapDispatchToProps = {
  changeAddressModalShow,
  changeAddressModalHide2ndTime,
  showRestaurantModals,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
