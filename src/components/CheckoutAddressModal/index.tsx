// Modules
import React, { FC, memo, useCallback } from 'react';
import { ModalWindow } from 'chibbis-ui-kit';
import { useLocation } from 'react-router';
// Components
import ModalBody from './ModalBody';
// Enhance
import { enhance } from './enhance';
// Types
import { TChangeAddressModalProps } from './types';

const HomePath = new RegExp('/[a-z]{2,}');
const RestaurantPath = new RegExp('/[a-z]*/restaurant/');
const FoodForPointsPath = new RegExp('/[a-z]*/food-for-points');

const ChangeAddressModalComponent: FC<TChangeAddressModalProps> = ({
  showModal,
  changeAddressModalShow,
  changeAddressModalHide2ndTime,
  city,
  address,
  showRestaurantModals,
}) => {
  const location = useLocation();

  const closeModal = useCallback(() => {
    changeAddressModalShow({
      show: false,
    });
  }, [changeAddressModalShow]);

  const onChangeAddressClick = useCallback(
    () => {
      closeModal();
      if (
        location.pathname.match(HomePath)
        || location.pathname.match(RestaurantPath)
        || location.pathname.match(FoodForPointsPath)
      ) {
        setTimeout(() => {
          showRestaurantModals({
            showModal: true,
            modalType: 'change-address',
          });
        }, 400);
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [location],
  );

  const onRedirectToRests = useCallback(() => {
    changeAddressModalHide2ndTime({
      doNotShow2ndTime: true,
    });
  }, [changeAddressModalHide2ndTime]);

  return (
    <ModalWindow
      classNameWrapper="address-change-modal"
      closable
      setVisible={closeModal}
      visible={showModal}
    >
      <ModalBody
        address={address}
        city={city}
        closeModal={closeModal}
        onChangeAddressClick={onChangeAddressClick}
        onRedirectToRests={onRedirectToRests}
      />
    </ModalWindow>
  );
};

const ChangeAddressModalMemo = memo(ChangeAddressModalComponent);
export default enhance(ChangeAddressModalMemo);
