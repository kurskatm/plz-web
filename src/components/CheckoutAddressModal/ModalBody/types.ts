export interface ChangeAddressModalProps {
  address: string;
  city: string;
  onChangeAddressClick: () => void;
  closeModal: () => void;
  // A fix for showing "mismatch address" on redirection
  onRedirectToRests: () => void;
}
