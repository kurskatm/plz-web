// Modules
import React, { FC, memo, useCallback } from 'react';
import { useLocation } from 'react-router';
import { Link } from 'react-router-dom';
// Module Components
import { Button } from 'chibbis-ui-kit';
// Utils
import { Consts } from '@utils';
// Types
import { ChangeAddressModalProps } from './types';

const { PATH } = Consts.ROUTES.CITIES;

const RestaurantPath = new RegExp('/[a-z]*/restaurant/');
const FoodForPointsPath = new RegExp('/[a-z]*/food-for-points');

const ChangeAddressModalBodyComponent: FC<ChangeAddressModalProps> = ({
  address,
  city,
  onChangeAddressClick,
  closeModal,
  onRedirectToRests,
}) => {
  const location = useLocation();
  const getLink = useCallback(() => `${PATH}${city}`, [city]);

  const redirectToRests = useCallback(() => {
    closeModal();
    // A fix for showing "mismatch address" on redirection
    if (location.pathname.match(RestaurantPath) || location.pathname.match(FoodForPointsPath)) {
      onRedirectToRests();
    }
  }, [closeModal, onRedirectToRests, location]);

  return (
    <div className="address-change-modal__body">
      <div className="address-change-modal__header">
        Ресторан не доставляет
        <br />
        по этому адресу
        <div className="address-change-modal__header-address">{address}</div>
      </div>
      <div className="address-change-modal__buttons">
        <Button htmlType="button" size="small" type="primary" onClick={onChangeAddressClick}>
          Изменить адрес
        </Button>

        <Link to={getLink()}>
          <Button htmlType="button" size="small" type="secondary" onClick={redirectToRests}>
            Посмотреть другие рестораны
          </Button>
        </Link>
      </div>
    </div>
  );
};

const ChangeAddressModalBodyMemo = memo(ChangeAddressModalBodyComponent);
export default ChangeAddressModalBodyMemo;
