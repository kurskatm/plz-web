import React, {
  FC, memo, useMemo, useCallback,
} from 'react';
import { useHistory } from 'react-router-dom';
import classnames from 'classnames';

import { TTabsComponentProps } from './types';

const TabsComponent: FC<TTabsComponentProps> = ({ list = [], currentTab }) => {
  const history = useHistory();
  const getStyles = useCallback(
    (anchor) =>
      classnames('tabs__item', {
        'is-active': currentTab === anchor,
      }),
    [currentTab],
  );

  const onClick = useCallback(
    (anchor: string) => {
      history.push(anchor);
    },
    [history],
  );

  const renderList = useMemo(
    () =>
      list.map(({ anchor, text }) => (
        <li className={getStyles(anchor)} key={anchor}>
          {/* <a href={anchor} className="tabs__link">
            {text}
          </a> */}
          <div className="tabs__link" onClickCapture={() => onClick(anchor)}>
            {text}
          </div>
        </li>
      )),
    [list, getStyles, onClick],
  );
  return <ul className="tabs">{renderList}</ul>;
};

export default memo(TabsComponent);
