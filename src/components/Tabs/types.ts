export type TTabsItem = {
  anchor: string;
  text: string;
};

export type TTabsComponentProps = {
  list: TTabsItem[];
  currentTab: string;
};
