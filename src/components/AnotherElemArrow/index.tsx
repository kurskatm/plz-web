// Modules
import React, { FC, memo } from 'react';
import classnames from 'classnames';
// Types
import { TAnotherElemArrow } from './types';

const AnotherElemArrowComponent: FC<TAnotherElemArrow> = ({ className }) => (
  <div className={classnames('anotherElemArrow', className)}>
    <div className="anotherElemArrow__inner" />
  </div>
);

export default memo(AnotherElemArrowComponent);
