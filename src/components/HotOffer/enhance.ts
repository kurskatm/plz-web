// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { restaurantCardIdSave } from '@/actions/restaurant/main/id';
// Selectors
import { homeHotOfferStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/home';

const mapDispatchToProps = {
  restaurantCardIdSave,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
