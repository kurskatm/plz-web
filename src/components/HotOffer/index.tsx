// Modules
import React, {
  FC, useCallback, useMemo, memo,
} from 'react';
import { Link } from 'react-router-dom';
// Utils
import { applyImageFallback } from '@/utils';
// Assets
import cardFoodDefault from '@/../assets/svg/main/card-food-default.svg';
import cardRestDefault from '@/../assets/svg/main/card-rest-default.svg';
// Enhance
import { enhance } from './enhance';
// Types
import { THotOfferProps } from './types';

const HotOfferComponent: FC<THotOfferProps> = ({
  imagePath,
  name,
  description,
  restaurantLogo,
  restaurantName,
  restaurantId,
  city,
  restaurantCardIdSave,
}) => {
  const getOfferLink = useCallback(() => `/${city}/restaurant/${restaurantName}`, [
    restaurantName,
    city,
  ]);

  const handleClick = () => {
    restaurantCardIdSave({ id: restaurantId });
  };
  const renderImages = useMemo(
    () => (
      <div className="hot-offer__images">
        <img
          src={imagePath || cardFoodDefault}
          onError={applyImageFallback(cardFoodDefault)}
          className="hot-offer__image"
          alt={name}
        />
        {restaurantLogo ? (
          <img
            src={restaurantLogo || cardRestDefault}
            onError={applyImageFallback(cardRestDefault)}
            className="hot-offer__logo"
            alt="logo"
          />
        ) : null}
      </div>
    ),
    [imagePath, name, restaurantLogo],
  );

  const renderDescription = useCallback((text: string) => {
    if (text.length > 220) {
      return `${text.slice(0, 218)}...`;
    }

    return text;
  }, []);

  return (
    <Link to={getOfferLink()} onClick={handleClick} className="hot-offer">
      {renderImages}
      <div className="hot-offer__info">
        <div className="hot-offer__title">{name}</div>
        <div className="hot-offer__description">{renderDescription(description)}</div>
      </div>
    </Link>
  );
};

const HotOfferMemo = memo(HotOfferComponent);

// @ts-ignore
export default enhance(HotOfferMemo);
