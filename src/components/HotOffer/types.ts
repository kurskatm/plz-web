import { TRestaurantCardIdSave } from '@/actions/restaurant/main/id/action-types';
import { TRestaurantCard } from '@/type/restaurants/restaurant-card';

export interface THotOfferProps {
  id: string;
  imagePath: string;
  name: string;
  description: string;
  restaurantLogo?: string;
  restaurantId?: string;
  restaurantName: string;
  city?: string;
  restaurant: TRestaurantCard;
  restaurantCardIdSave: TRestaurantCardIdSave;
}
