// Modules
import React, { FC, memo, useState } from 'react';
// Main Components
import Content from '@components/Content';
// Components
import Title from '@pages/cities/CitiesList/Title';
import List from '@pages/cities/CitiesList/List';

const SelectCitiesModalComponent: FC = () => {
  const [search, setSearch] = useState<string>('');

  return (
    <div className="select-cities-list">
      <div className="select-cities-list-title">
        <Title search={search} setSearch={setSearch} />
      </div>
      <Content className="select-cities-list-content">
        <List search={search} />
      </Content>
    </div>
  );
};

export default memo(SelectCitiesModalComponent);
