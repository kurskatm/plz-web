/* eslint-disable @typescript-eslint/no-explicit-any */
export type TItem = Record<string, any>;

export interface TSlider {
  list: TItem[];
  prefix?: JSX.Element;
  renderItem: (data: any) => JSX.Element;
  autoScrollTrigger?: string;
  className?: string;
  minMaxStyle?: string;
}
