export interface TSmoothScrollXArgs {
  element: HTMLElement;
  direction: 'left' | 'right';
  speed: number;
  distance: number;
  step: number;
}

export type TSmoothScrollX = (data: TSmoothScrollXArgs) => void;
