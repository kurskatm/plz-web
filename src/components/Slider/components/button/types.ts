export interface TSliderButtonProps {
  contentRef: React.RefObject<HTMLDivElement>;
  position?: 'left' | 'right';
}
