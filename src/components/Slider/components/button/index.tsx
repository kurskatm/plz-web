// Modules
import React, { FC, memo, useCallback } from 'react';
import classNames from 'classnames';
// Lib
import { smoothScrollX } from '../../lib/smooth-scroll-x';
// Types
import { TSliderButtonProps } from './types';

const SliderButtonComponent: FC<TSliderButtonProps> = ({ contentRef, position }) => {
  const scrollElement = useCallback(() => {
    smoothScrollX({
      element: contentRef.current,
      direction: position,
      speed: 25,
      distance: 150,
      step: 10,
    });
  }, [contentRef, position]);

  const getMainStyles = useCallback(
    () => classNames('slider-button', `slider-button-${position}`),
    [position],
  );

  return <div role="presentation" className={getMainStyles()} onClick={scrollElement} />;
};

SliderButtonComponent.defaultProps = {
  position: 'left',
};

export default memo(SliderButtonComponent);
