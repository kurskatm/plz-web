// Modules
import React, {
  FC, memo, useCallback, useEffect, useRef, useState, useMemo,
} from 'react';
import { AutoSizer } from 'react-virtualized';
import throtlle from 'lodash/throttle';
import isNil from 'lodash/isNil';
import classNames from 'classnames';
import { useMediaQuery } from 'react-responsive';
// Lib
import { HideScrollXManager } from '@lib/hide-scroll-x';
// Components
import SliderButton from './components/button';
// Types
import { TSlider } from './types';

const SliderComponent: FC<TSlider> = ({
  list,
  prefix,
  renderItem,
  autoScrollTrigger,
  className,
  minMaxStyle,
}) => {
  const outerRef = useRef<HTMLDivElement>();
  const contentRef = useRef<HTMLDivElement>();

  const [hasScroll, setHasScroll] = useState<boolean>(false);
  const [hasLeftArrow, setHasLeftArrow] = useState<boolean>(false);
  const [hasRightArrow, setHasRightArrow] = useState<boolean>(true);

  useEffect(() => {
    const hideScrollXManager = new HideScrollXManager({
      contentRef: contentRef.current,
      outerRef: outerRef.current,
    });

    hideScrollXManager.init();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const scrollCollbackStart = throtlle(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (e: any) => {
      const { scrollLeft } = e.target;

      if (scrollLeft && !hasLeftArrow) {
        return setHasLeftArrow(true);
      }

      if (!scrollLeft && hasLeftArrow) {
        return setHasLeftArrow(false);
      }

      return null;
    },
    200,
  );

  const scrollCollbackEnd = throtlle(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (e: any) => {
      const { offsetWidth, scrollWidth, scrollLeft } = e.target;
      const isEnd = offsetWidth + scrollLeft >= scrollWidth;

      if (isEnd && hasRightArrow) {
        return setHasRightArrow(false);
      }

      if (!isEnd && !hasRightArrow) {
        return setHasRightArrow(true);
      }

      return null;
    },
    200,
  );

  useEffect(
    () => {
      const ref = contentRef.current;
      contentRef.current.addEventListener('scroll', scrollCollbackStart);

      return () => {
        ref.removeEventListener('scroll', scrollCollbackStart);
      };
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [hasLeftArrow],
  );

  const scrollThrottled = useRef(
    throtlle((btnX, width) => {
      const { scrollLeft } = contentRef.current;
      const xOffset = 200;
      const targetScroll = scrollLeft + btnX + xOffset - width;
      contentRef.current.scrollTo({
        left: targetScroll,
        behavior: 'smooth',
      });
    }, 500),
  );

  useEffect(
    () => {
      if (autoScrollTrigger) {
        const activeBtn = document.getElementById(autoScrollTrigger);
        const btnX = activeBtn.getBoundingClientRect().x;
        const { width } = contentRef.current.getBoundingClientRect();
        scrollThrottled.current(btnX, width);
      }
    }, // eslint-disable-next-line react-hooks/exhaustive-deps
    [autoScrollTrigger],
  );

  useEffect(
    () => {
      const ref = contentRef.current;
      contentRef.current.addEventListener('scroll', scrollCollbackEnd);

      return () => {
        ref.removeEventListener('scroll', scrollCollbackEnd);
      };
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [hasRightArrow],
  );

  const onResize = throtlle(() => {
    const isScroll = contentRef.current.offsetHeight - contentRef.current.clientHeight > 0;

    if (isScroll !== hasScroll) {
      setHasScroll(isScroll);
    }
  });

  const getStylesSliderScroll = useCallback(
    () => classNames('main-slider-scroll', hasScroll && 'main-slider-scroll-padding'),
    [hasScroll],
  );

  const isDesktop = useMediaQuery({
    minWidth: 1025,
  });

  const getMinMaxSliderStyles = useMemo(() => {
    if (!isNil(minMaxStyle)) return minMaxStyle;

    return 'min-content';
  }, [minMaxStyle]);

  const getStyles = useCallback(() => classNames('main-slider-content', className), [className]);

  return (
    <div className="main-slider">
      {hasScroll && hasLeftArrow && isDesktop && <SliderButton contentRef={contentRef} />}
      <AutoSizer disableHeight onResize={onResize} style={{ width: '100%', height: 'auto' }}>
        {() => (
          <div ref={outerRef} className={getStylesSliderScroll()}>
            <div
              ref={contentRef}
              className={getStyles()}
              style={{
                gridTemplateColumns: `repeat(${
                  list.length + (prefix ? 1 : 0)
                }, minmax(${getMinMaxSliderStyles}, 0))`,
              }}
            >
              {prefix && <div className="main-slider-prefix">{prefix}</div>}
              {list.map(renderItem)}
            </div>
          </div>
        )}
      </AutoSizer>
      {hasScroll && hasRightArrow && isDesktop && (
        <SliderButton contentRef={contentRef} position="right" />
      )}
    </div>
  );
};

export default memo(SliderComponent);
