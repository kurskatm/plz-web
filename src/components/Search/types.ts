export interface TSearchProps {
  action?: (value: string) => void;
  placeholder?: string;
  initialValue?: string;
  needToClearValueWithUnmount?: boolean;
}
