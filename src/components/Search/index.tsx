// Modules
import React, {
  FC, memo, useCallback, useEffect, useRef, useState,
} from 'react';
import throttle from 'lodash/throttle';
import isNil from 'lodash/isNil';
import { useMediaQuery } from 'react-responsive';
// Modules Components
import { Input } from 'chibbis-ui-kit';
// Types
import { TSearchProps } from './types';

const SearchComponent: FC<TSearchProps> = ({
  action,
  placeholder,
  initialValue,
  needToClearValueWithUnmount = true,
}) => {
  const actionRef = useRef(throttle(action, 1000));

  const getInitialValue = useCallback(() => {
    if (!isNil(initialValue)) {
      return initialValue;
    }

    return '';
  }, [initialValue]);

  const [value, setValue] = useState(getInitialValue());

  useEffect(() => {
    actionRef.current(value);
  }, [value]);

  useEffect(
    () => () => {
      if (needToClearValueWithUnmount) {
        action('');
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const onChange = useCallback(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (e: any) => {
      setValue(e.target.value);
    },
    [],
  );

  const isDesktop = useMediaQuery({
    minWidth: 1025,
  });

  return (
    <div className="search__form-wrap">
      <Input
        autocomplete={false}
        clear
        name="search"
        onChange={onChange}
        placeholder={placeholder}
        searchIcon
        value={value}
        theme="grey"
        withButton={isDesktop}
      />
    </div>
  );
};

SearchComponent.defaultProps = {
  action: () => null,
  placeholder: 'Поиск',
  initialValue: '',
};

export default memo(SearchComponent);
