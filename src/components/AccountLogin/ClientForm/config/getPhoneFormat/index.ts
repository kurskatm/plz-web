// Types
import { TGetValidPhoneFormat } from './types';

export const getPhoneFormat: TGetValidPhoneFormat = (phone) => {
  const pattern = new RegExp(/(\d{3})(\d{3})(\d{2})(\d{2})/g);
  const validPhone = phone.substring(1).replace(pattern, '+7 ($1) $2-$3-$4');
  return validPhone;
};
