// Types
import { TGetFormValidation, TErrors } from './types';

export const getFormValidation: TGetFormValidation = (values) => {
  const errors: TErrors = {};

  if (values.phone && values.phone[15] === '_') {
    errors.phone = 'Введите номер полностью';
  }
  if (values.phone) {
    if (values.phone[3] === '_') {
      errors.phone = 'Введите номер полностью';
    }
    if (Number(values.phone[3]) === 0 || Number(values.phone[3]) === 1) {
      errors.phone = 'Второе число должно отличаться от 0 и 1';
    }
  }
  return errors;
};
