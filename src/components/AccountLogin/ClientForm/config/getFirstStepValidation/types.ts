export interface TValues {
  phone: string;
}

export interface TErrors {
  phone?: string;
}

export type TGetFormValidation = (data: TValues) => TErrors;
