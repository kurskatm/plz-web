/* eslint-disable jsx-a11y/interactive-supports-focus */
// Modules
import React, {
  FC, memo, useCallback, useMemo,
} from 'react';
import { Link } from 'chibbis-ui-kit';
import { Form } from 'react-final-form';
import urlJoin from 'url-join';
import { useParams } from 'react-router-dom';
// Utils
import { Consts } from '@utils';
// Main Types
import { TCityRouterParams } from '@type/routes';
// Components
import DigitsField from '../../DigitsField';
import { getPhoneFormat } from '../config/getPhoneFormat';
// Types
import { TSecondStepProps } from './types';
// import { trackEvent } from '@/utils/analytics';

const { INFO_PAGES } = Consts.ROUTES;

const FormClientSecondStep: FC<TSecondStepProps> = ({
  otpRequestStatus,
  phone,
  onSubmit,
  setPrevStep,
  authModalShow,
  onSendSmsSubmit,
  currentTick,
  attemptsCount,
  setAttemptsCount,
  startTimer,
  authOtpStatusReset,
}) => {
  const { cityUrl } = useParams<TCityRouterParams>();

  const onSmsSendClick = useCallback(
    (e) => {
      e.stopPropagation();
      // повторно можно отправить код только 3 раза
      setAttemptsCount((previous) => previous + 1);
      onSendSmsSubmit();
      startTimer(true);
    },
    [onSendSmsSubmit, setAttemptsCount, startTimer],
  );

  const handleDontRecallClick = useCallback(
    (el) => {
      authModalShow(false);
      setPrevStep(el);
    },
    [authModalShow, setPrevStep],
  );

  const description = useMemo(() => {
    if (attemptsCount !== 1) {
      return `Код отправлен на номер ${getPhoneFormat(phone)}`;
    }

    return `Введите последние цифры входящего звонка.
    Мы звоним на номер ${getPhoneFormat(phone)}`;
  }, [attemptsCount, phone]);

  const timerText = useMemo(() => {
    if (currentTick !== 60 && attemptsCount === 1) {
      return `${currentTick} сек`;
    }

    if (currentTick !== 60 && attemptsCount !== 1) {
      return `Отправить ещё раз через ${currentTick} сек`;
    }

    return '';
  }, [attemptsCount, currentTick]);

  const renderFooter = useMemo(() => {
    if (attemptsCount === 4 && currentTick === 60) {
      return (
        <>
          <button
            className="client-form__footer-link"
            onClickCapture={setPrevStep}
            onKeyDown={setPrevStep}
            type="button"
          >
            Использовать другой номер
          </button>
          <div
            className="client-form__footer-linkWrapper"
            onClick={handleDontRecallClick}
            role="presentation"
          >
            <Link
              to={urlJoin(`/${cityUrl}`, INFO_PAGES.HELP.PATH, '#dont-recall')}
              className="client-form__footer-link"
              border={false}
            >
              Что делать, если звонок/СМС не поступили
              {' '}
            </Link>
          </div>
        </>
      );
    }
    if (attemptsCount < 4 && currentTick === 60) {
      return (
        <>
          <button
            className="client-form__footer-link"
            onClickCapture={onSmsSendClick}
            onKeyDown={onSmsSendClick}
            type="button"
          >
            {attemptsCount === 1
              ? 'Не поступает звонок? Получить СМС с кодом'
              : 'Отправить СМС повторно'}
          </button>
          <button
            className="client-form__footer-link"
            onClickCapture={setPrevStep}
            onKeyDown={setPrevStep}
            type="button"
          >
            Использовать другой номер
          </button>
        </>
      );
    }

    return (
      <button
        className="client-form__footer-link"
        onClickCapture={setPrevStep}
        onKeyDown={setPrevStep}
        type="button"
      >
        Использовать другой номер
      </button>
    );
  }, [attemptsCount, cityUrl, handleDontRecallClick, setPrevStep, onSmsSendClick, currentTick]);

  return (
    <Form
      onSubmit={onSubmit}
      render={({ handleSubmit }) => (
        <form onSubmit={handleSubmit} className="client-form">
          <div className="client-form__description">{description}</div>
          <DigitsField
            otpRequestStatus={otpRequestStatus}
            onSubmit={onSubmit}
            authOtpStatusReset={authOtpStatusReset}
          />
          {otpRequestStatus === 'error' && (
            <div className="client-form__error">Код подтверждения указан неверно</div>
          )}
          {currentTick !== 60 && <div className="client-form__second-step-timer">{timerText}</div>}
          <div className="client-form__footer">{renderFooter}</div>
        </form>
      )}
    />
  );
};

export default memo(FormClientSecondStep);
