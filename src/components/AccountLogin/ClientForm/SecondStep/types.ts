// Main types
import { TAuthOtpStatusReset } from '@/actions/auth/action-types';

export interface TSecondStepProps {
  onSubmit: (code: string) => void;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  setPrevStep: (e: any) => void;
  phone: string;
  otpRequestStatus?: string;
  authModalShow: (data: boolean) => void;
  onSendSmsSubmit: () => void;
  currentTick: number;
  startTimer: React.Dispatch<React.SetStateAction<boolean>>;
  attemptsCount: number;
  setAttemptsCount: React.Dispatch<React.SetStateAction<number>>;
  authOtpStatusReset: TAuthOtpStatusReset;
}
