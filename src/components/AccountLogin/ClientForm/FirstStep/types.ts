// Main types
import { TAuthPhoneStatusReset } from '@/actions/auth/action-types';

export interface TFirstStepProps {
  phoneRequestStatus?: string;
  onSubmit: ({ phone }: { [key: string]: string }) => void;
  phoneRequestDetails: string;
  modalShow: boolean;
  resendToken?: string;
  authPhoneStatusReset: TAuthPhoneStatusReset;
  currentTick: number;
  startTimer: React.Dispatch<React.SetStateAction<boolean>>;
  cityUrlName: string;
}
