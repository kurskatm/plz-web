// Modules
import React, {
  FC, memo, useCallback, createRef, useEffect, useRef, useState,
} from 'react';
import { InputPhone, Button } from 'chibbis-ui-kit';
import { Form } from 'react-final-form';
import { FormApi } from 'final-form';

import classnames from 'classnames';
import isNil from 'lodash/isNil';
import debounce from 'lodash/debounce';
// Config
import { userAgreementLink, privacyPolicyLink } from '@components/Agreement/links';
import { getFormValidation } from '../config/getFirstStepValidation';
// Types
import { TFirstStepProps } from './types';

const inputFieldRef = createRef<HTMLInputElement>();

const ClientFormFirstStep: FC<TFirstStepProps> = ({
  phoneRequestStatus,
  onSubmit,
  phoneRequestDetails,
  modalShow,
  resendToken,
  authPhoneStatusReset,
  currentTick,
  startTimer,
  cityUrlName,
}) => {
  const [shouldResetStatus, setShouldResetStatus] = useState(true);

  const resetPhoneError = useRef(
    debounce(() => {
      authPhoneStatusReset();
    }, 500),
  );

  useEffect(() => {
    if (phoneRequestStatus === 'pending') {
      setShouldResetStatus(true);
    }
  }, [phoneRequestStatus]);

  useEffect(
    () => {
      if (inputFieldRef.current && modalShow && !resendToken) {
        inputFieldRef.current.focus();
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [inputFieldRef.current, modalShow, resendToken],
  );

  const getValidation = useCallback((values) => getFormValidation(values), []);

  const getFieldError = useCallback(
    (form: FormApi) => {
      const { phone } = form.getState().errors;
      const { phone: phoneDirty } = form.getState().dirtyFields;
      if (form.getState().modifiedSinceLastSubmit && shouldResetStatus) {
        resetPhoneError.current();
        setShouldResetStatus(false);
      }

      if (!isNil(phone) && phoneDirty) {
        return <div className="client-form__error">{phone}</div>;
      }

      if (phoneRequestStatus === 'error') {
        return <div className="client-form__error">{phoneRequestDetails}</div>;
      }

      return null;
    },
    [phoneRequestDetails, phoneRequestStatus, shouldResetStatus],
  );

  const buttonText = currentTick === 60 ? 'Позвонить мне' : `через ${currentTick} сек`;

  const onSubmitClick = useCallback(
    (values) => {
      startTimer(true);
      onSubmit(values);
    },
    [onSubmit, startTimer],
  );

  return (
    <Form
      onSubmit={onSubmitClick}
      validate={getValidation}
      render={({
        handleSubmit, pristine, valid, form,
      }) => (
        <form onSubmit={handleSubmit} className="client-form">
          <div className="client-form__field">
            <InputPhone
              name="phone"
              theme="grey"
              typeMask="phone_third"
              required
              disabled={phoneRequestStatus === 'pending' || currentTick !== 60}
              inputRef={inputFieldRef}
            />
          </div>
          {getFieldError(form)}
          <div style={{ marginTop: !isNil(getFieldError(form)) ? 16 : 0 }}>
            <Button
              key={!valid}
              className={classnames('client-form__button', {
                'client-form__button__disabled': currentTick !== 60,
              })}
              disabled={
                pristine || !valid || phoneRequestStatus === 'pending' || currentTick !== 60
              }
            >
              {buttonText}
            </Button>
          </div>
          <div className="client-form__info">
            На ваш номер поступит звонок. Запомните последние 4 цифры
          </div>
          {/* <div className="client-form__agreement">
            При входе или регистрации вы принимаетe
            <br />
            <a
              className="account-login__button"
              target="_blank"
              href={userAgreementLink(cityUrlName)}
              rel="noreferrer"
            >
              пользовательское соглашение
            </a>
            {' и'}
            <br />
            <a
              className="account-login__button"
              target="_blank"
              href={privacyPolicyLink}
              rel="noreferrer"
            >
              политику конфиденциальности
            </a>
          </div> */}
        </form>
      )}
    />
  );
};

export default memo(ClientFormFirstStep);
