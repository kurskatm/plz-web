import { TVerifyPhoneRequestArgs, TVerifyOtpRequestArgs } from '@type/auth';
import { TAuthOtpStatusReset, TAuthPhoneStatusReset } from '@/actions/auth/action-types';
import { TCitiesItem } from '@type/cities';

export interface TClientFormProps {
  authPhoneSend: (data: TVerifyPhoneRequestArgs) => void;
  authOtpSend: (data: TVerifyOtpRequestArgs) => void;
  authClearResendTokenData: () => void;
  resendToken?: string;
  phoneRequestStatus?: string;
  phoneRequestDetails?: string;
  otpRequestStatus?: string;
  authModalShow: (data: boolean) => void;
  modalPhone: string;
  modalShow: boolean;
  city: TCitiesItem;
  authPhoneStatusReset: TAuthPhoneStatusReset;
  authOtpStatusReset: TAuthOtpStatusReset;
}
