// Modules
import React, {
  FC, memo, useMemo, useCallback, useState, useEffect,
} from 'react';
import isNil from 'lodash/isNil';
// Consts
import { TIMER_DELAY, TIMER_STOP_COUNTER } from '@lib/auth/config';
// Utils
import { trackEvent } from '@/utils/analytics';
import { useCounter } from '@utils';
// Components
import ClientFormFirstStep from './FirstStep';
import ClientFormSecondStep from './SecondStep';
// Types
import { TClientFormProps } from './types';

const ClientFormComponent: FC<TClientFormProps> = ({
  resendToken,
  authPhoneSend,
  authOtpSend,
  authClearResendTokenData,
  authPhoneStatusReset,
  authOtpStatusReset,
  phoneRequestStatus,
  phoneRequestDetails,
  otpRequestStatus,
  authModalShow,
  modalPhone,
  city,
  modalShow,
}) => {
  const [phoneNumber, setPhone] = useState<string>(null);

  const [currentTick, startTimer, attemptsCount, setAttemptsCount] = useCounter({
    secondsCount: TIMER_STOP_COUNTER,
    delayMs: TIMER_DELAY,
  });

  useEffect(() => {
    if (modalPhone && modalPhone.length) {
      setPhone(modalPhone);
      authPhoneSend({ phone: modalPhone.replace(/[^\d]/g, '') });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalPhone]);

  const verifyPhoneSubmit = useCallback(
    ({ phone }: { [key: string]: string }) => {
      setPhone(phone.replace(/[^\d]/g, ''));
      if (!isNil(resendToken) && resendToken.length) {
        authPhoneSend({
          phone: phone.replace(/[^\d]/g, ''),
          resendToken,
        });
        trackEvent('login-sms');
      } else {
        authPhoneSend({
          phone: phone.replace(/[^\d]/g, ''),
        });
        trackEvent('login-call');
      }
    },
    [authPhoneSend, resendToken],
  );
  const verifyOtpSubmit = useCallback(
    (code: string) => {
      authOtpSend({
        phone: phoneNumber,
        verification_code: code,
        city_id: city?.id,
      });
    },
    [authOtpSend, phoneNumber, city],
  );

  const onSendSmsSubmit = useCallback(() => {
    authPhoneSend({
      phone: phoneNumber,
      resendToken,
    });
    trackEvent('login-sms');
  }, [resendToken, phoneNumber, authPhoneSend]);

  const setPrevStep = useCallback(
    (e: MouseEvent) => {
      e.stopPropagation();
      authClearResendTokenData();
    },
    [authClearResendTokenData],
  );

  const renderStep = useMemo(() => {
    if (resendToken) {
      return (
        <ClientFormSecondStep
          setPrevStep={setPrevStep}
          phone={phoneNumber}
          onSubmit={verifyOtpSubmit}
          otpRequestStatus={otpRequestStatus}
          authModalShow={authModalShow}
          onSendSmsSubmit={onSendSmsSubmit}
          currentTick={currentTick}
          attemptsCount={attemptsCount}
          setAttemptsCount={setAttemptsCount}
          startTimer={startTimer}
          authOtpStatusReset={authOtpStatusReset}
        />
      );
    }

    return (
      <ClientFormFirstStep
        phoneRequestStatus={phoneRequestStatus}
        onSubmit={verifyPhoneSubmit}
        phoneRequestDetails={phoneRequestDetails}
        modalShow={modalShow}
        resendToken={resendToken}
        authPhoneStatusReset={authPhoneStatusReset}
        currentTick={currentTick}
        startTimer={startTimer}
        cityUrlName={city?.urlName}
      />
    );
  }, [
    resendToken,
    phoneNumber,
    verifyOtpSubmit,
    verifyPhoneSubmit,
    setPrevStep,
    phoneRequestStatus,
    otpRequestStatus,
    authModalShow,
    phoneRequestDetails,
    modalShow,
    authPhoneStatusReset,
    onSendSmsSubmit,
    currentTick,
    attemptsCount,
    startTimer,
    setAttemptsCount,
    authOtpStatusReset,
    city,
  ]);

  return renderStep;
};

export default memo(ClientFormComponent);
