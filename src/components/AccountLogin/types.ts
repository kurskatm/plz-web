import { TVerifyPhoneRequestArgs, TVerifyOtpRequestArgs } from '@type/auth';
import { TAuthPhoneStatusReset, TAuthOtpStatusReset } from '@actions/auth/action-types';
import { TCitiesItem } from '@type/cities';

export interface TAccountLoginProps {
  form: string;
  phoneRequestStatus: string;
  phoneRequestDetails: string;
  otpRequestStatus: string;
  authPhoneSend: (data: TVerifyPhoneRequestArgs) => void;
  authOtpSend: (data: TVerifyOtpRequestArgs) => void;
  authClearResendTokenData: () => void;
  authPhoneStatusReset: TAuthPhoneStatusReset;
  authOtpStatusReset: TAuthOtpStatusReset;
  toggleVisible: (show: boolean) => void;
  resendToken?: string;
  authModalShow: (data: boolean) => void;
  modalPhone: string;
  modalShow: boolean;
  city: TCitiesItem;
}
