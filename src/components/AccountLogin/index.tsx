// Modules
import React, {
  FC, memo, useEffect, useMemo, useState,
} from 'react';
import { Link } from 'chibbis-ui-kit';
import { useHistory } from 'react-router-dom';

// Components
import ClientForm from './ClientForm';
// import PartnerForm from './PartnerForm';
// Types
import { TAccountLoginProps } from './types';
// Enchance
import { enhance } from './enhance';

const AccountLoginComponent: FC<TAccountLoginProps> = ({
  resendToken,
  form = 'client',
  authPhoneSend,
  authOtpSend,
  authClearResendTokenData,
  authModalShow,
  authPhoneStatusReset,
  authOtpStatusReset,
  phoneRequestStatus,
  phoneRequestDetails,
  otpRequestStatus,
  modalPhone,
  modalShow,
  city,
  auth,
}) => {
  const [currentForm] = useState(form);
  const history = useHistory();
  const renderClientForm = useMemo(
    () =>
      currentForm === 'client'
      && otpRequestStatus !== 'success' && (
        <ClientForm
          resendToken={resendToken}
          authPhoneSend={authPhoneSend}
          authOtpSend={authOtpSend}
          authClearResendTokenData={authClearResendTokenData}
          phoneRequestStatus={phoneRequestStatus}
          otpRequestStatus={otpRequestStatus}
          authModalShow={authModalShow}
          phoneRequestDetails={phoneRequestDetails}
          modalPhone={modalPhone}
          city={city}
          modalShow={modalShow}
          authPhoneStatusReset={authPhoneStatusReset}
          authOtpStatusReset={authOtpStatusReset}
        />
      ),
    [
      resendToken,
      authPhoneSend,
      authOtpSend,
      authClearResendTokenData,
      phoneRequestStatus,
      otpRequestStatus,
      currentForm,
      authModalShow,
      phoneRequestDetails,
      modalPhone,
      city,
      modalShow,
      authPhoneStatusReset,
      authOtpStatusReset,
    ],
  );

  useEffect(() => {
    console.log(otpRequestStatus);
    if (auth) {
      history.push('/object');
    }
  }, [auth]);

  return (
    <div className="account-login">
      <div className="account-login__logo" />
      <div className="account-login__title">Войти в аккаунт</div>
      <div className="account-login__form">{renderClientForm}</div>
    </div>
  );
};

export const AccountLoginComponentMemo = memo(AccountLoginComponent);

export default enhance(AccountLoginComponentMemo);
