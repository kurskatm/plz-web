// Modules
import React, { FC, memo, useMemo } from 'react';
import { InputField, Button } from 'chibbis-ui-kit';
import { Form } from 'react-final-form';
// Components
// Types
import { TPartnerEnterFormProps } from './types';

const PartnerFormComponent: FC<TPartnerEnterFormProps> = ({ onSubmit }) => {
  const renderForm = useMemo(
    () => (
      <Form
        onSubmit={onSubmit}
        render={({ handleSubmit, pristine, valid }) => (
          <form id="partnerForm" onSubmit={handleSubmit} className="partner-form">
            <div className="partner-form__field">
              <InputField label="Электронная почта" name="email" theme="grey" required />
            </div>
            <div className="partner-form__field">
              <InputField label="Пароль" name="password" type="password" theme="grey" required />
            </div>
            <div className="partner-form__button">
              <Button disabled={pristine || !valid}>Войти</Button>
            </div>
          </form>
        )}
      />
    ),
    [onSubmit],
  );

  return renderForm;
};

export default memo(PartnerFormComponent);
