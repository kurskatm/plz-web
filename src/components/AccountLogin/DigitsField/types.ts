// Main types
import { TAuthOtpStatusReset } from '@/actions/auth/action-types';

export interface TDigitsFieldProps {
  onSubmit: (s: string) => void;
  otpRequestStatus?: string;
  authOtpStatusReset: TAuthOtpStatusReset;
}
