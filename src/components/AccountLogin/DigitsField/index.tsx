// Modules
import React, {
  FC, useRef, useState, useEffect, memo,
} from 'react';
import { Input } from 'chibbis-ui-kit';

// Types
import { TDigitsFieldProps } from './types';

const DigitsFieldComponent: FC<TDigitsFieldProps> = ({
  otpRequestStatus,
  onSubmit,
  authOtpStatusReset,
}) => {
  const firstDigitRef = useRef(null);
  const secondDigitRef = useRef(null);
  const thirdDigitRef = useRef(null);
  const fourthDigitRef = useRef(null);
  const fifthDigitRef = useRef(null);
  const [oneDigitValue, setOneDigitValue] = useState('');
  const [secondDigitValue, setSecondDigitValue] = useState('');
  const [thirdDigitValue, setThirdDigitValue] = useState('');
  const [fourthDigitValue, setFourthDigitValue] = useState('');
  const [refsList, setRefList] = useState([]);
  const [activeDigit, setActiveDigit] = useState('');
  const onDigitSet = (
    refIndex: number,
    setFunc: (s: string) => void,
    clearFunc?: (s: string) => void,
  ) => (e: Event) => {
    const val = (e.target as HTMLInputElement).value;
    if (val.match(new RegExp(/^\d$/))) {
      setFunc(val);
      if (refIndex === 4 && val.length > 0) {
        onSubmit(`${oneDigitValue}${secondDigitValue}${thirdDigitValue}${val}`);
        return;
      }
      if (val.length > 0 && clearFunc) {
        // clearFunc('');
        refsList[refIndex].focus();
      }
    }
  };

  const onKeyDownHandler = (
    refIndex: number,
    setFunc: (s: string) => void,
    clearFunc: (s: string) => void,
  ) => (e: KeyboardEvent) => {
    const val = (e.target as HTMLInputElement).value;
    const { keyCode } = e;

    if (val.length === 1 && val.match(new RegExp(/^\d$/)) && setFunc && keyCode !== 8) {
      // keyCode 8 - backspace
      refsList[refIndex].focus();
      setFunc(String.fromCharCode(keyCode));
      if (refIndex === 3) {
        onSubmit(
          `${oneDigitValue}${secondDigitValue}${thirdDigitValue}${String.fromCharCode(keyCode)}`,
        );
      }
    }
    if (keyCode === 8) {
      // keyCode 8 - backspace
      clearFunc('');
      refsList[refIndex - 2].focus();
      e.preventDefault();
      if (otpRequestStatus === 'error') {
        authOtpStatusReset();
      }
    }
  };

  useEffect(() => {
    setRefList([
      firstDigitRef.current,
      secondDigitRef.current,
      thirdDigitRef.current,
      fourthDigitRef.current,
      fifthDigitRef.current,
    ]);
  }, []);

  return (
    <div ref={fifthDigitRef} className="client-form__field">
      <div className="client-form__digits-row">
        <div className="client-form__digit">
          <Input
            name="digit"
            maxLength={1}
            inputRef={firstDigitRef}
            onFocus={() => {
              setOneDigitValue('');
              setActiveDigit('first');
            }}
            onBlur={() => {
              setActiveDigit(null);
            }}
            active={activeDigit === 'first'}
            onChange={onDigitSet(1, setOneDigitValue, setSecondDigitValue)}
            onKeyDown={onKeyDownHandler(1, setSecondDigitValue, setOneDigitValue)}
            theme="grey"
            value={oneDigitValue}
            inputmode="tel"
            required
            disabled={otpRequestStatus === 'pending'}
            autoFocus
          />
        </div>
        <div className="client-form__digit">
          <Input
            name="digit"
            maxLength={1}
            inputRef={secondDigitRef}
            onFocus={() => {
              setSecondDigitValue('');
              setActiveDigit('second');
            }}
            onBlur={() => {
              setActiveDigit(null);
            }}
            active={activeDigit === 'second'}
            onChange={onDigitSet(2, setSecondDigitValue, setThirdDigitValue)}
            onKeyDown={onKeyDownHandler(2, setThirdDigitValue, setSecondDigitValue)}
            theme="grey"
            value={secondDigitValue}
            inputmode="tel"
            required
            disabled={otpRequestStatus === 'pending'}
          />
        </div>
        <div className="client-form__digit">
          <Input
            name="digit"
            maxLength={1}
            inputRef={thirdDigitRef}
            onFocus={() => {
              setThirdDigitValue('');
              setActiveDigit('third');
            }}
            onBlur={() => {
              setActiveDigit(null);
            }}
            active={activeDigit === 'third'}
            onChange={onDigitSet(3, setThirdDigitValue, setFourthDigitValue)}
            onKeyDown={onKeyDownHandler(3, setFourthDigitValue, setThirdDigitValue)}
            theme="grey"
            value={thirdDigitValue}
            inputmode="tel"
            required
            disabled={otpRequestStatus === 'pending'}
          />
        </div>
        <div className="client-form__digit">
          <Input
            name="digit"
            maxLength={1}
            inputRef={fourthDigitRef}
            onFocus={() => {
              setFourthDigitValue('');
              setActiveDigit('fourth');
            }}
            onBlur={() => {
              setActiveDigit(null);
            }}
            active={activeDigit === 'fourth'}
            onChange={onDigitSet(4, setFourthDigitValue)}
            onKeyDown={onKeyDownHandler(4, () => {}, setFourthDigitValue)}
            theme="grey"
            value={fourthDigitValue}
            inputmode="tel"
            required
          />
        </div>
      </div>
    </div>
  );
};

export default memo(DigitsFieldComponent);
