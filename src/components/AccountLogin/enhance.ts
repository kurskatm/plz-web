// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import {
  authPhoneSend,
  authOtpSend,
  authClearResendTokenData,
  authModalShow,
  authPhoneStatusReset,
  authOtpStatusReset,
} from '@actions/auth';
// Selectors
import { authStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/auth';

const mapDispatchToProps = {
  authClearResendTokenData,
  authPhoneSend,
  authOtpSend,
  authModalShow,
  authPhoneStatusReset,
  authOtpStatusReset,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);
export const enhance = compose(withConnect);
