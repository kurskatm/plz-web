// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { restaurantCardIdSave } from '@/actions/restaurant/main/id';
import { restaurantCardSave, restaurantCardFetchSuccess } from '@actions/restaurant/main/card';

const mapDispatchToProps = {
  restaurantCardIdSave,
  restaurantCardSave,
  restaurantCardFetchSuccess,
};
const withConnect = connect(null, mapDispatchToProps);

export const enhance = compose(withConnect);
