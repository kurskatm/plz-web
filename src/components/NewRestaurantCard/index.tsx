/* eslint-disable jsx-a11y/click-events-have-key-events */
// Modules
import React, {
  FC, memo, useCallback, useEffect, useRef, useState,
} from 'react';
import { Link } from 'react-router-dom';
import debounce from 'lodash/debounce';
import isNil from 'lodash/isNil';
import classNames from 'classnames';

// Modules Components
import { Liked, RibbonLabel, SmallFeedback } from 'chibbis-ui-kit';
// Lib
import { getHoursAndMinutes } from '@lib/restaurants/getHoursAndMinutes';
import { getRibbonProps } from '@lib/restaurants/get-label';
// Utils
import { applyImageFallback } from '@/utils';
// Assets
import cardRestDefault from '@/../assets/svg/main/card-rest-default.svg';
// Types
import { TRestaurantCardProps } from './types';
// Enhance
import { enhance } from './enhance';

const RestaurantCardComponent: FC<TRestaurantCardProps> = ({
  onLikeClick,
  item,
  url,
  restaurantCardIdSave,
  restaurantCardSave,
  restaurantCardFetchSuccess,
}: TRestaurantCardProps) => {
  const cardRef = useRef<HTMLDivElement>();
  const [renderImage, setRenderImage] = useState<boolean>(false);
  const [liked, setLiked] = useState(item.isFavorite);

  useEffect(() => {
    setLiked(item.isFavorite);
  }, [item.isFavorite]);

  const scrollEventRef = useRef(
    debounce(() => {
      if (cardRef.current) {
        const windowHeight = window.innerHeight;
        const elementPosition = Math.round(cardRef.current.getBoundingClientRect().top);

        if (!renderImage && elementPosition > 0 && elementPosition <= windowHeight + 500) {
          setRenderImage(true);
        }
      }
    }, 50),
  );

  useEffect(
    () => {
      scrollEventRef.current();

      const callback = scrollEventRef.current;
      document.addEventListener('scroll', callback);

      return () => {
        document.removeEventListener('scroll', callback);
      };
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const getTime = useCallback(() => {
    if (item?.workingTime) {
      if (item.workingTime?.openTime || item.workingTime?.closeTime) {
        const openTime = getHoursAndMinutes(item.workingTime?.openTime);
        const closeTime = getHoursAndMinutes(item.workingTime?.closeTime);
        if (openTime === closeTime) {
          return 'Круглосуточно';
        }
        const strTime = `c ${openTime} до ${closeTime}`;
        return strTime;
      }
      if (item.workingTime?.openingTime || item.workingTime?.closingTime) {
        const openTime = item.workingTime?.openingTime;
        const closeTime = item.workingTime?.closingTime;
        if (openTime === closeTime) {
          return 'Круглосуточно';
        }
        const strTime = `c ${openTime.substring(0, 5)} до ${closeTime.substring(0, 5)}`;
        return strTime;
      }
    }
    return 'Закрыто';
  }, [item?.workingTime]);

  const getMinCoast = useCallback(() => `От ${item.minCost} ₽`, [item.minCost]);

  const getMinDeliveryCoast = useCallback(
    () => (item.deliveryCost ? `От ${item.deliveryCost} ₽` : 'Доставка бесплатно'),
    [item.deliveryCost],
  );

  const ribbonProps = useCallback(
    () =>
      getRibbonProps({
        isNew: item.isNew,
        hasSpecialPlacement: item.hasSpecialPlacement,
        hasPaymentForPoints: item.hasPaymentForPoints,
        hasPromotions: item.hasPromotions,
      }),
    [item.isNew, item.hasSpecialPlacement, item.hasPaymentForPoints, item.hasPromotions],
  );

  const getMainStyle = useCallback(
    () => classNames('new-restaurant-card', { 'new-restaurant-card__is-close': !item.isOpen }),
    [item.isOpen],
  );

  const likeClickHandler = useCallback(
    (e) => {
      e.stopPropagation();
      onLikeClick(e, liked, () => setLiked(!liked));
    },
    [onLikeClick, setLiked, liked],
  );

  const handleClick = () => {
    restaurantCardIdSave({ id: item?.id });
    restaurantCardSave(item);
    restaurantCardFetchSuccess({ status: 200 });
    window.scrollTo(0, 0);
  };

  return (
    <div ref={cardRef} className={getMainStyle()}>
      <Link to={url} onClick={handleClick}>
        {!isNil(ribbonProps()) && (
          <div className="new-restaurant-card_ribbon">
            <RibbonLabel disabled={!item.isOpen} isRadial color={ribbonProps().color}>
              {ribbonProps().text}
            </RibbonLabel>
          </div>
        )}

        <div className="new-restaurant-card__inner">
          <div className="left-block">
            <div className="new-restaurant-card__logo">
              {renderImage && (
                <img
                  src={item.logo || cardRestDefault}
                  onError={applyImageFallback(cardRestDefault)}
                  alt={item.name}
                  width="114"
                  height="120"
                />
              )}
            </div>
          </div>

          <div className="center-block">
            <div className="center-block__wrap">
              <div className="new-restaurant-card__name">{item.name}</div>
              <div className="new-restaurant-card__liked">
                <div
                  role="presentation"
                  className="new-restaurant-card__like-wrap"
                  // @ts-ignore
                  onClick={likeClickHandler}
                >
                  <Liked selected={liked} />
                </div>
              </div>
            </div>

            <div className="center-block__wrap">
              <div className="new-restaurant-card__button">{getTime()}</div>
            </div>

            {!isNil(item.minCost) && (
              <div className="center-block__wrap center-block__wrap-with-icon">
                <div className="new-restaurant-card__shopping_bag" />
                <div className="new-restaurant-card__info">{getMinCoast()}</div>
              </div>
            )}
            {!isNil(item.deliveryCost) && (
              <div className="center-block__wrap center-block__wrap-with-icon">
                <>
                  <div className="new-restaurant-card__running_man" />
                  <div className="new-restaurant-card__info">{getMinDeliveryCoast()}</div>
                </>
                <div className="new-restaurant-card__reviews-wrap">
                  <SmallFeedback disabled={!item.isOpen} percent={item.positiveReviewsPercent} />
                </div>
              </div>
            )}
          </div>
        </div>
      </Link>
    </div>
  );
};

const RestaurantCardMemo = memo(RestaurantCardComponent);
export default enhance(RestaurantCardMemo);
