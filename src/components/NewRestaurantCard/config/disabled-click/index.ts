// Types
import { TCalbackDisabledClick } from './types';

export const calbackDisabledClick: TCalbackDisabledClick = (e) => {
  e.preventDefault();
};
