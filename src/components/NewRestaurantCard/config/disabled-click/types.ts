export type TCalbackDisabledClick = (e: React.SyntheticEvent<HTMLDivElement>) => void;
