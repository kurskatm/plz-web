// Main Types
import { TRestaurantCard } from '@type/restaurants/restaurant-card';
import { TRestaurantCardIdSave } from '@actions/restaurant/main/id/action-types';
import {
  TRestaurantCardFetchSuccess,
  TRestaurantCardSave,
} from '@/actions/restaurant/main/card/action-types';

export interface TRestaurantCardProps {
  item: TRestaurantCard;
  onLikeClick: (e: MouseEvent, liked: boolean, cb: () => void) => void;
  url: string;
  restaurantCardIdSave: TRestaurantCardIdSave;
  restaurantCardSave: TRestaurantCardSave;
  restaurantCardFetchSuccess: TRestaurantCardFetchSuccess;
}
