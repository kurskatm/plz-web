// Modules
import React, {
  FC, memo, useEffect, useCallback, useState,
} from 'react';
import { useMediaQuery } from 'react-responsive';
import classNames from 'classnames';
import debounce from 'lodash/debounce';

// Modules Components
import { Button, LoadingSpinner } from 'chibbis-ui-kit';
// Components
import CollapseHeight from '../animate-height/collapse-height';
// Config
import { getQuantity } from './config/get-quantity';
// Types
import { TContentViewerProps } from './types';

const ContentViewerComponent: FC<TContentViewerProps> = ({
  list,
  renderItem,
  responsive,
  dataIsPending,
  wrapper: Wrapper,
  useAutoLoad,
  viewFullList,
}) => {
  const isDesktop = useMediaQuery({
    minWidth: 1025,
  });

  const isTablet = useMediaQuery({
    minWidth: 641,
    maxWidth: 1024,
  });

  const isMobile = useMediaQuery({
    maxWidth: 640,
  });

  const [quantity, setQuantity] = useState<number>(
    getQuantity({
      isDesktop,
      isTablet,
      isMobile,
      responsive,
    }),
  );

  const [count, setCount] = useState<number>(10);

  const [renderSpinner, setRenderSpinner] = useState(true);

  useEffect(() => {
    if (isDesktop || isTablet || isMobile) {
      const newQuantity = getQuantity({
        isDesktop,
        isTablet,
        isMobile,
        responsive,
      });
      const newCount = Math.ceil((quantity * count) / newQuantity);

      if (newQuantity !== quantity) {
        setQuantity(newQuantity);
        setCount(newCount);
      }
    }
  }, [isDesktop, isTablet, isMobile, responsive, quantity, count]);

  useEffect(() => {
    setCount(viewFullList ? list.length : 1);
    if (list.length !== 0) setRenderSpinner(false);
  }, [list.length, viewFullList]);

  const getMainStyle = useCallback(
    () => classNames('content-viewer', { 'data-is-pending': dataIsPending }),
    [dataIsPending],
  );

  const loadMore = useCallback(() => {
    setCount(count + 1);
    setRenderSpinner(false);
  }, [count]);

  const loadMoreThrottled = debounce(loadMore, 500);

  const scrollEvent = useCallback(
    ({ target }: Event) => {
      const { scrollingElement } = target as Document;
      const { scrollHeight, clientHeight, scrollTop } = scrollingElement;
      const maxScroll = scrollHeight - clientHeight;
      const scrollPercentFetch = (scrollHeight / 100) * (isMobile ? 90 : 70);
      const isCalledFetch = scrollTop >= maxScroll - scrollPercentFetch;
      if (isCalledFetch && list.length > quantity * count) {
        setRenderSpinner(true);
        loadMoreThrottled();
      }
    },
    [loadMoreThrottled, list, quantity, count, isMobile],
  );

  useEffect(() => {
    if (useAutoLoad) document.addEventListener('scroll', scrollEvent);

    return () => {
      if (useAutoLoad) document.removeEventListener('scroll', scrollEvent);
    };
  }, [count, scrollEvent, useAutoLoad]);

  return (
    <div className={getMainStyle()}>
      <div className="content-viewer__content">
        <CollapseHeight ignoreAll>
          <Wrapper>
            {quantity ? list.slice(0, quantity * count).map(renderItem) : list.map(renderItem)}
          </Wrapper>
        </CollapseHeight>
        {renderSpinner && (
          <LoadingSpinner className="content-viewer__spinner" size="big" color="orange" />
        )}
      </div>

      {!useAutoLoad && list.length > quantity * count && (
        <div className="content-viewer__button-wrap">
          <Button htmlType="button" onClick={loadMore} size="small" type="secondary">
            Показать еще
          </Button>
        </div>
      )}
    </div>
  );
};

ContentViewerComponent.defaultProps = {
  list: [],
  renderItem: (): void => null,
  useAutoLoad: false,
};

export default memo(ContentViewerComponent);
