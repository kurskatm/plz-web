// Types
import { TResponsive } from '../../types';

interface TGetQuantityArgs {
  isDesktop: boolean;
  isTablet: boolean;
  isMobile: boolean;
  responsive: TResponsive;
}

export type TGetQuantity = (data: TGetQuantityArgs) => number | null;
