/* eslint-disable @typescript-eslint/no-explicit-any */

export interface TResponsive {
  desktop: number;
  tablet: number;
  mobile: number;
}

export interface TContentViewerProps {
  list: any;
  renderItem: any;
  responsive: TResponsive;
  wrapper: any;
  dataIsPending?: boolean;
  useAutoLoad?: boolean;
  viewFullList?: boolean;
}
