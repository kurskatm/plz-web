// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

// get hot offers
export const HOT_OFFERS_FETCH = `${PREFIX}hotOffersFetch`;
export const HOT_OFFERS_SAVE = `${PREFIX}hotOffersSave`;
export const HOT_OFFERS_RESET = `${PREFIX}hotOffersReset`;

export const HOT_OFFERS_PENDING = `${PREFIX}hotOffersPending`;
export const HOT_OFFERS_SUCCESS = `${PREFIX}hotOffersSuccess`;
export const HOT_OFFERS_ERROR = `${PREFIX}hotOffersError`;
