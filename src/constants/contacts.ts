// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const CONTACTS_FETCH = `${PREFIX}contactsFetch`;
export const CONTACTS_SAVE = `${PREFIX}contactsSave`;

export const CONTACTS_PENDING = `${PREFIX}contactsPending`;
export const CONTACTS_SUCCESS = `${PREFIX}contactsSuccess`;
export const CONTACTS_ERROR = `${PREFIX}contactsError`;
