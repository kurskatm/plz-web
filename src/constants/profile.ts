// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const PROFILE_RESET_ALL_UI_MODELS = `${PREFIX}profile/resetAllUiModels`;

// City
export const PROFILE_CITY_UPDATE = `${PREFIX}profileCity/update`;

// Delivery
export const PROFILE_UPDATE_ADDRESS = `${PREFIX}profile/updateAddress`;
export const PROFILE_UPDATE_PICKUP = `${PREFIX}profile/updatePickup`;

// User Geocode
export const PROFILE_GET_ADDRESSES_GEOCODE = `${PREFIX}profileGetAddressesGeocode`;
export const PROFILE_SAVE_ADDRESSES_GEOCODE = `${PREFIX}profileSaveAddressesGeocode`;

export const PROFILE_GET_ADDRESSES_GEOCODE_PENDING = `${PREFIX}profileGetAddressesGeocodePending`;
export const PROFILE_GET_ADDRESSES_GEOCODE_SUCCESS = `${PREFIX}profileGetAddressesGeocodeSuccess`;
export const PROFILE_GET_ADDRESSES_GEOCODE_ERROR = `${PREFIX}profileGetAddressesGeocodeError`;

export const PROFILE_RESET_ADDRESSES_GEOCODE = `${PREFIX}profileResetAddressesGeocode`;
// User favorite
export const PROFILE_GET_FAVORITE = `${PREFIX}profile/getFavorite`;
export const PROFILE_GET_FAVORITE_RESTS = `${PREFIX}profile/getFavoriteRests`;
export const PROFILE_GET_FAVORITE_RESTS_PENDING = `${PREFIX}profile/getFavoriteRestsPending`;
export const PROFILE_GET_FAVORITE_RESTS_SUCCESS = `${PREFIX}profile/getFavoriteRestsSuccess`;
export const PROFILE_GET_FAVORITE_RESTS_ERROR = `${PREFIX}profile/getFavoriteRestsError`;
export const PROFILE_GET_FAVORITE_RESTS_SAVE_DATA = `${PREFIX}profile/getFavoriteRestsSaveData`;

export const PROFILE_GET_FAVORITE_PRODUCTS = `${PREFIX}profile/getFavoriteProducts`;
export const PROFILE_GET_FAVORITE_PRODUCTS_PENDING = `${PREFIX}profile/getFavoriteProductsPending`;
export const PROFILE_GET_FAVORITE_PRODUCTS_SUCCESS = `${PREFIX}profile/getFavoriteProductsSuccess`;
export const PROFILE_GET_FAVORITE_PRODUCTS_ERROR = `${PREFIX}profile/getFavoriteProductsError`;
export const PROFILE_GET_FAVORITE_PRODUCTS_SAVE_DATA = `${PREFIX}profile/getFavoriteProductsSaveData`;
export const PROFILE_SET_FAVORITE_PRODUCT = `${PREFIX}profile/setFavoriteProduct`;
// Auth
export const PROFILE_AUTHORIZED = `${PREFIX}profile/auth`;
export const PROFILE_UNAUTHORIZED = `${PREFIX}profile/unauth`;
// Info
export const PROFILE_GET_INFO = `${PREFIX}profile/info`;
export const PROFILE_GET_INFO_PENDING = `${PREFIX}profile/infoPending`;
export const PROFILE_GET_INFO_SUCCESS = `${PREFIX}profile/infoSuccess`;
export const PROFILE_GET_INFO_ERROR = `${PREFIX}profile/infoError`;
export const PROFILE_GET_INFO_SAVE_DATA = `${PREFIX}profile/infoSaveData`;
export const PROFILE_UPDATE_INFO = `${PREFIX}profile/updateInfo`;
export const PROFILE_UPDATE_INFO_PENDING = `${PREFIX}profile/updateInfoPending`;
export const PROFILE_UPDATE_INFO_SUCCESS = `${PREFIX}profile/updateInfoSucces`;
export const PROFILE_UPDATE_INFO_ERROR = `${PREFIX}profile/updateInfoError`;

// Reviews
export const PROFILE_GET_REVIEWS = `${PREFIX}profile/reviews`;
export const PROFILE_GET_REVIEWS_PENDING = `${PREFIX}profile/reviewsPending`;
export const PROFILE_GET_REVIEWS_SUCCESS = `${PREFIX}profile/reviewsSuccess`;
export const PROFILE_GET_REVIEWS_ERROR = `${PREFIX}profile/reviewsError`;
export const PROFILE_GET_REVIEWS_SAVE_DATA = `${PREFIX}profile/reviewsSaveData`;

// Friends
export const PROFILE_GET_FRIENDS = `${PREFIX}profile/friends`;
export const PROFILE_GET_FRIENDS_PENDING = `${PREFIX}profile/friendsPending`;
export const PROFILE_GET_FRIENDS_SUCCESS = `${PREFIX}profile/friendsSuccess`;
export const PROFILE_GET_FRIENDS_ERROR = `${PREFIX}profile/friendsError`;
export const PROFILE_GET_FRIENDS_SAVE_DATA = `${PREFIX}profile/friendsSaveData`;

// Orders
export const PROFILE_GET_ORDERS = `${PREFIX}profile/orders`;
export const PROFILE_GET_ORDERS_PENDING = `${PREFIX}profile/ordersPending`;
export const PROFILE_GET_ORDERS_SUCCESS = `${PREFIX}profile/ordersSuccess`;
export const PROFILE_GET_ORDERS_ERROR = `${PREFIX}profile/ordersError`;
export const PROFILE_GET_ORDERS_SAVE_DATA = `${PREFIX}profile/ordersSaveData`;

export const PROFILE_SHOW_ORDER_REVIEW_MODAL = `${PREFIX}profile/showOrdersReviewModal`;

// Last order without review
export const PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW = `${PREFIX}profile/lastOrderWithoutReview`;
export const PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_PENDING = `${PREFIX}profile/lastOrderWithoutReviewPending`;
export const PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_SUCCESS = `${PREFIX}profile/lastOrderWithoutReviewSuccess`;
export const PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_ERROR = `${PREFIX}profile/lastOrderWithoutReviewError`;
export const PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_SAVE_DATA = `${PREFIX}profile/lastOrderWithoutReviewSaveData`;

// Order Reviews
export const PROFILE_SET_ORDER_REVIEW = `${PREFIX}profile/orderReview`;
export const PROFILE_SET_ORDER_REVIEW_PENDING = `${PREFIX}profile/orderReviewPending`;
export const PROFILE_SET_ORDER_REVIEW_SUCCESS = `${PREFIX}profile/orderReviewSuccess`;
export const PROFILE_SET_ORDER_REVIEW_ERROR = `${PREFIX}profile/orderReviewError`;
export const PROFILE_SAVE_ORDER_REVIEW_DATA = `${PREFIX}profile/orderReviewSaveData`;

// Current Order
export const PROFILE_GET_USER_ORDER = `${PREFIX}profile/userOrder`;
export const PROFILE_GET_USER_ORDER_PENDING = `${PREFIX}profile/userOrderPending`;
export const PROFILE_GET_USER_ORDER_SUCCESS = `${PREFIX}profile/userOrderSuccess`;
export const PROFILE_GET_USER_ORDER_ERROR = `${PREFIX}profile/userOrderError`;
export const PROFILE_GET_USER_ORDER_SAVE_DATA = `${PREFIX}profile/userOrderSaveData`;

// Rests info
export const PROFILE_GET_RESTS_INFO = `${PREFIX}profile/restsInfo`;
export const PROFILE_GET_RESTS_INFO_PENDING = `${PREFIX}profile/restsInfoPending`;
export const PROFILE_GET_RESTS_INFO_SUCCESS = `${PREFIX}profile/restsInfoSuccess`;
export const PROFILE_GET_RESTS_INFO_ERROR = `${PREFIX}profile/restsInfoError`;
export const PROFILE_GET_RESTS_INFO_SAVE_DATA = `${PREFIX}profile/restsInfoSaveData`;

// Get favorites
export const PROFILE_GET_FAVORITES_INFO = `${PREFIX}profile/favoritesInfo`;
export const PROFILE_GET_FAVORITES_INFO_PENDING = `${PREFIX}profile/favoritesInfoPending`;
export const PROFILE_GET_FAVORITES_INFO_SUCCESS = `${PREFIX}profile/favoritesInfoSuccess`;
export const PROFILE_GET_FAVORITES_INFO_ERROR = `${PREFIX}profile/favoritesInfoError`;
export const PROFILE_GET_FAVORITES_INFO_SAVE_DATA = `${PREFIX}profile/favoritesInfoSaveData`;
export const PROFILE_GET_FAVORITES_INFO_RESET = `${PREFIX}profile/favoritesInfoReset`;
