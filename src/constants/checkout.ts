// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const CHECKOUT_SEND = `${PREFIX}checkoutSend`;
export const CHECKOUT_SEND_SUCCESS = `${PREFIX}checkoutSendSuccess`;
export const CHECKOUT_SEND_PENDING = `${PREFIX}checkoutSendPending`;
export const CHECKOUT_SEND_ERROR = `${PREFIX}checkoutSendError`;
export const CHECKOUT_ADD_ORDER_ID = `${PREFIX}checkoutAddOrderId`;
