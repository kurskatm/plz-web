// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const PUSH_NOTIFICATION = `${PREFIX}pushNotification`;
export const REMOVE_NOTIFICATION = `${PREFIX}removeNotification`;
