// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const SHOW_FULL_OLD_SITE_BANNER = `${PREFIX}showFullOldSiteBanner`;
