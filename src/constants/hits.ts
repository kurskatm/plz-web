// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

// get hits
export const HITS_FETCH = `${PREFIX}hitsFetch`;
export const HITS_SAVE = `${PREFIX}hitsSave`;

export const HITS_PENDING = `${PREFIX}hitsPending`;
export const HITS_SUCCESS = `${PREFIX}hitsSuccess`;
export const HITS_ERROR = `${PREFIX}hitsError`;
