// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const RESTAURANT_CARD_MENU_RESET = `${PREFIX}restaurantCardMenuReset`;
