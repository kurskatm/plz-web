// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const RESTAURANT_CARD_CATEGORIES_FETCH = `${PREFIX}restaurantCardCategoriesFetch`;
export const RESTAURANT_CARD_CATEGORIES_SAVE = `${PREFIX}restaurantCardCategoriesSave`;
export const RESTAURANT_CARD_CATEGORIES_RESET = `${PREFIX}restaurantCardCategoriesReset`;

export const RESTAURANT_FETCH_CARD_CATEGORIES_PENDING = `${PREFIX}restaurantFetchCardCategoriesPending`;
export const RESTAURANT_FETCH_CARD_CATEGORIES_SUCCESS = `${PREFIX}restaurantFetchCardCategoriesSuccess`;
export const RESTAURANT_FETCH_CARD_CATEGORIES_ERROR = `${PREFIX}restaurantFetchCardCategoriesError`;
