// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const RESTAURANT_CARD_PRODUCTS_FETCH = `${PREFIX}restaurantCardProductsFetch`;
export const RESTAURANT_CARD_PRODUCTS_SAVE = `${PREFIX}restaurantCardProductsSave`;
export const RESTAURANT_CARD_PRODUCTS_RESET = `${PREFIX}restaurantCardProductsReset`;

export const RESTAURANT_FETCH_CARD_PRODUCTS_PENDING = `${PREFIX}restaurantFetchCardProductsPending`;
export const RESTAURANT_FETCH_CARD_PRODUCTS_SUCCESS = `${PREFIX}restaurantFetchCardProductsSuccess`;
export const RESTAURANT_FETCH_CARD_PRODUCTS_ERROR = `${PREFIX}restaurantFetchCardProductsError`;
