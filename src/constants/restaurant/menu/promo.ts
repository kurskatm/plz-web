// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const RESTAURANT_CARD_PROMO_FETCH = `${PREFIX}restaurantCardPromoFetch`;
export const RESTAURANT_CARD_PROMO_SAVE = `${PREFIX}restaurantCardPromoSave`;
export const RESTAURANT_CARD_PROMO_RESET = `${PREFIX}restaurantCardPromoReset`;

export const RESTAURANT_FETCH_CARD_PROMO_PENDING = `${PREFIX}restaurantFetchCardPromoPending`;
export const RESTAURANT_FETCH_CARD_PROMO_SUCCESS = `${PREFIX}restaurantFetchCardPromoSuccess`;
export const RESTAURANT_FETCH_CARD_PROMO_ERROR = `${PREFIX}restaurantFetchCardPromoError`;
