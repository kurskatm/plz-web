// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const RESTAURANT_CARD_ID_FETCH = `${PREFIX}restaurantCardIdFetch`;
export const RESTAURANT_CARD_ID_SAVE = `${PREFIX}restaurantCardIdSave`;
export const RESTAURANT_CARD_ID_RESET = `${PREFIX}restaurantCardIdReset`;

export const RESTAURANT_FETCH_CARD_ID_PENDING = `${PREFIX}restaurantFetchCardIdPending`;
export const RESTAURANT_FETCH_CARD_ID_SUCCESS = `${PREFIX}restaurantFetchCardIdSuccess`;
export const RESTAURANT_FETCH_CARD_ID_ERROR = `${PREFIX}restaurantFetchCardIdError`;
