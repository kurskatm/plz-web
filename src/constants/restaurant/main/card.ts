// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const RESTAURANT_CARD_FETCH = `${PREFIX}restaurantCardFetch`;
export const RESTAURANT_CARD_SAVE = `${PREFIX}restaurantCardSave`;
export const RESTAURANT_CARD_RESET = `${PREFIX}restaurantCardReset`;

export const RESTAURANT_FETCH_CARD_PENDING = `${PREFIX}restaurantFetchCardPending`;
export const RESTAURANT_FETCH_CARD_SUCCESS = `${PREFIX}restaurantFetchCardSuccess`;
export const RESTAURANT_FETCH_CARD_ERROR = `${PREFIX}restaurantFetchCardError`;
