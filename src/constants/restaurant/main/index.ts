// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const RESTAURANT_CARD_MAIN_RESET = `${PREFIX}restaurantCardMainReset`;
