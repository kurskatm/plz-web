// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const RESTAURANT_SEARCH_PRODUCT = `${PREFIX}restaurantSearchProduct`;

export const RESTAURANT_SEARCH_PRODUCT_PENDING = `${PREFIX}restaurantSearchProduct/pending`;
export const RESTAURANT_SEARCH_PRODUCT_SUCCESS = `${PREFIX}restaurantSearchProduct/success`;
export const RESTAURANT_SEARCH_PRODUCT_ERROR = `${PREFIX}restaurantSearchProduct/error`;

export const RESTAURANT_SEARCH_PRODUCT_SAVE = `${PREFIX}restaurantSearchProduct/save`;
export const RESTAURANT_SEARCH_PRODUCT_RESET = `${PREFIX}restaurantSearchProduct/reset`;
