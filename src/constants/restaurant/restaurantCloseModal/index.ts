// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const CLOSE_RESTAURANT_MODAL_SHOW = `${PREFIX}closeRestaurantModalShow`;
