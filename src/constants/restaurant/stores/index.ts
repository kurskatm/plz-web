// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const RESTAURANT_STORES_ERROR = `${PREFIX}restaurantsStoresError`;
export const RESTAURANT_STORES_FETCH = `${PREFIX}restaurantsStoresFetch`;
export const RESTAURANT_STORES_PENDING = `${PREFIX}restaurantsStoresPending`;
export const RESTAURANT_STORES_SUCCESS = `${PREFIX}restaurantsStoresSuccess`;
export const RESTAURANT_STORES_SAVE = `${PREFIX}restaurantsStoresSave`;
