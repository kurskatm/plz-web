// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const RESTAURANT_CARD_LEGAL_FETCH = `${PREFIX}restaurantCardLegalFetch`;
export const RESTAURANT_CARD_LEGAL_SAVE = `${PREFIX}restaurantCardLegalSave`;
export const RESTAURANT_CARD_LEGAL_RESET = `${PREFIX}restaurantCardLegalReset`;

export const RESTAURANT_FETCH_CARD_LEGAL_PENDING = `${PREFIX}restaurantFetchCardLegalPending`;
export const RESTAURANT_FETCH_CARD_LEGAL_SUCCESS = `${PREFIX}restaurantFetchCardLegalSuccess`;
export const RESTAURANT_FETCH_CARD_LEGAL_ERROR = `${PREFIX}restaurantFetchCardLegalError`;
