// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const RESTAURANT_GEOCODE_SUGGEST_FETCH = `${PREFIX}restaurantGeocodeSuggest/fetch`;
export const RESTAURANT_GEOCODE_SUGGEST_SAVE = `${PREFIX}restaurantGeocodeSuggest/save`;
export const RESTAURANT_GEOCODE_SUGGEST_RESET = `${PREFIX}restaurantGeocodeSuggest/reset`;

export const RESTAURANT_GEOCODE_SUGGEST_FETCH_PENDING = `${PREFIX}restaurantGeocodeSuggest/fetchPending`;
export const RESTAURANT_GEOCODE_SUGGEST_FETCH_SUCCESS = `${PREFIX}restaurantGeocodeSuggest/fetchSuccess`;
export const RESTAURANT_GEOCODE_SUGGEST_FETCH_ERROR = `${PREFIX}restaurantGeocodeSuggest/fetchError`;
