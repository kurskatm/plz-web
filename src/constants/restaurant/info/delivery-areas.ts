// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const RESTAURANT_DELIVERY_AREAS_FETCH = `${PREFIX}restaurantDeliveryAreas/fetch`;
export const RESTAURANT_DELIVERY_AREAS_SAVE = `${PREFIX}restaurantDeliveryAreas/save`;
export const RESTAURANT_DELIVERY_AREAS_RESET = `${PREFIX}restaurantDeliveryAreas/reset`;

export const RESTAURANT_DELIVERY_AREAS_FETCH_PENDING = `${PREFIX}restaurantDeliveryAreas/fetchPending`;
export const RESTAURANT_DELIVERY_AREAS_FETCH_SUCCESS = `${PREFIX}restaurantDeliveryAreas/fetchSuccess`;
export const RESTAURANT_DELIVERY_AREAS_FETCH_ERROR = `${PREFIX}restaurantDeliveryAreas/fetchError`;
