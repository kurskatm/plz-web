// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const RESTAURANT_ADDRESSES_SUGGEST_FETCH = `${PREFIX}restaurantAddressSuggest/fetch`;
export const RESTAURANT_ADDRESSES_SUGGEST_SAVE = `${PREFIX}restaurantAddressSuggest/save`;
export const RESTAURANT_ADDRESSES_SUGGEST_RESET = `${PREFIX}restaurantAddressSuggest/reset`;

export const RESTAURANT_ADDRESSES_SUGGEST_FETCH_PENDING = `${PREFIX}restaurantAddressSuggest/fetchPending`;
export const RESTAURANT_ADDRESSES_SUGGEST_FETCH_SUCCESS = `${PREFIX}restaurantAddressSuggest/fetchSuccess`;
export const RESTAURANT_ADDRESSES_SUGGEST_FETCH_ERROR = `${PREFIX}restaurantAddressSuggest/fetchError`;
