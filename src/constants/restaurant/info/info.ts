// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const RESTAURANT_CARD_INFO_FETCH = `${PREFIX}restaurantCardInfoFetch`;
export const RESTAURANT_CARD_INFO_SAVE = `${PREFIX}restaurantCardInfoSave`;
export const RESTAURANT_CARD_INFO_RESET = `${PREFIX}restaurantCardInfoReset`;

export const RESTAURANT_FETCH_CARD_INFO_PENDING = `${PREFIX}restaurantFetchCardInfoPending`;
export const RESTAURANT_FETCH_CARD_INFO_SUCCESS = `${PREFIX}restaurantFetchCardInfoSuccess`;
export const RESTAURANT_FETCH_CARD_INFO_ERROR = `${PREFIX}restaurantFetchCardInfoError`;
