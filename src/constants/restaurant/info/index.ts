// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const RESTAURANT_CARD_INFO_MAIN_RESET = `${PREFIX}restaurantCardInfoMainReset`;
