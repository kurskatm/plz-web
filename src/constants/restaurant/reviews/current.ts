// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const RESTAURANT_CARD_REVIEWS_FETCH = `${PREFIX}restaurantCardReviewsFetch`;
export const RESTAURANT_CARD_REVIEWS_SAVE = `${PREFIX}restaurantCardReviewsSave`;
export const RESTAURANT_CARD_REVIEWS_RESET = `${PREFIX}restaurantCardReviewsReset`;

export const RESTAURANT_FETCH_CARD_REVIEWS_PENDING = `${PREFIX}restaurantFetchCardReviewsPending`;
export const RESTAURANT_FETCH_CARD_REVIEWS_SUCCESS = `${PREFIX}restaurantFetchCardReviewsSuccess`;
export const RESTAURANT_FETCH_CARD_REVIEWS_ERROR = `${PREFIX}restaurantFetchCardReviewsError`;

export const RESTAURANT_CARD_REVIEWS_CHANGE_PAGINATION_LIMIT = `${PREFIX}restaurantCardReviewsChangePaginationLimit`;
