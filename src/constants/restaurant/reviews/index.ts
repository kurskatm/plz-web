// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const RESTAURANT_CARD_REVIEWS_MAIN_RESET = `${PREFIX}restaurantCardReviewsMainReset`;
