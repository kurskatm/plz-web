// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const RESTAURANT_SHOW_MODALS = `${PREFIX}restaurantShowModals`;

export const RESTAURANT_SHOW_SHOPPING_CARD = `${PREFIX}restaurantShowShoppingCard`;

export const RESTAURANT_SHOW_MODIFIERS_CARD = `${PREFIX}restaurantShowModifiersCard`;

export const RESTAURANT_SHOW_PROMO_CARD = `${PREFIX}restaurantShowPromoCard`;
