// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

// get restaurants
export const RESTAURANTS_FETCH = `${PREFIX}restaurantsFetch`;
export const RESTAURANTS_SAVE = `${PREFIX}restaurantsSave`;

export const RESTAURANTS_PENDING = `${PREFIX}restaurantsPending`;
export const RESTAURANTS_SUCCESS = `${PREFIX}restaurantsSuccess`;
export const RESTAURANTS_ERROR = `${PREFIX}restaurantsError`;
