// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

// get restaurants
export const SUPPORT_TOPICS_FETCH = `${PREFIX}supportTopicsFetch`;
export const SUPPORT_TOPICS_SAVE = `${PREFIX}supportTopicsSave`;

export const SUPPORT_TOPICS_PENDING = `${PREFIX}supportTopicsPending`;
export const SUPPORT_TOPICS_SUCCESS = `${PREFIX}supportTopicsSuccess`;
export const SUPPORT_TOPICS_ERROR = `${PREFIX}supportTopicsError`;
export const SUPPORT_MESSAGE_PENDING = `${PREFIX}supportMessageSendPending`;
export const SUPPORT_MESSAGE_SUCCESS = `${PREFIX}supportMessageSendSuccess`;
export const SUPPORT_MESSAGE_ERROR = `${PREFIX}supportMessageSendError`;
export const SUPPORT_MESSAGE_SEND = `${PREFIX}supportMessageSend`;
export const SUPPORT_MESSAGE_SENT = `${PREFIX}supportMessageSent`;
