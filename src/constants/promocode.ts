// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

// get hits
export const PROMOCODE_VALIDATE = `${PREFIX}promocodeValidate`;
export const PROMOCODE_SAVE = `${PREFIX}promocodeSave`;

export const PROMOCODE_PENDING = `${PREFIX}promocodePending`;
export const PROMOCODE_SUCCESS = `${PREFIX}promocodeSuccess`;
export const PROMOCODE_ERROR = `${PREFIX}promocodeError`;
export const PROMOCODE_RESET = `${PREFIX}promocodeReset`;
