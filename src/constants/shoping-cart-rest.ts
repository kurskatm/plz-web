// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const SHOPCART_RESTAURANT_FETCH = `${PREFIX}restaurantShopCartRestFetch`;
export const SHOPCART_RESTAURANT_SAVE = `${PREFIX}restaurantShopCartRestSave`;
export const SHOPCART_RESTAURANT_RESET = `${PREFIX}restaurantShopCartRestReset`;

export const SHOPCART_RESTAURANT_FETCH_PENDING = `${PREFIX}restaurantFetchShopCartRestPending`;
export const SHOPCART_RESTAURANT_FETCH_SUCCESS = `${PREFIX}restaurantFetchShopCartRestSuccess`;
export const SHOPCART_RESTAURANT_FETCH_ERROR = `${PREFIX}restaurantFetchShopCartRestError`;
