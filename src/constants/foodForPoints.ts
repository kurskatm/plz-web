// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

// get free products
export const FOOD_FOR_POINTS_FETCH = `${PREFIX}foodForPointsFetch`;

export const FOOD_FOR_POINTS_PENDING = `${PREFIX}foodForPointsPending`;
export const FOOD_FOR_POINTS_SAVE = `${PREFIX}foodForPointsSave`;
export const FOOD_FOR_POINTS_SUCCESS = `${PREFIX}foodForPointsSuccess`;
export const FOOD_FOR_POINTS_ERROR = `${PREFIX}foodForPointsError`;
export const FOOD_FOR_POINTS_RESET = `${PREFIX}foodForPointsReset`;
