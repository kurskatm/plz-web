// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const RESTAURANTS_AND_HITS_FETCH = `${PREFIX}restaurantsAndHitsFetch`;
export const RESTAURANTS_AND_HITS_SAVE = `${PREFIX}restaurantsAndHitsSave`;

export const RESTAURANTS_AND_HITS_PENDING = `${PREFIX}restaurantsAndHitsPending`;
export const RESTAURANTS_AND_HITS_SUCCESS = `${PREFIX}restaurantsAndHitsSuccess`;
export const RESTAURANTS_AND_HITS_ERROR = `${PREFIX}restaurantsAndHitsError`;
