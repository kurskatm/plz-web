// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

// Main
export const SHOPING_CART_RESET = `${PREFIX}shoping-cart-reset`;

// Cart
export const UPDATE_SHOPING_CART = `${PREFIX}shoping-cart/update`;

// Cutlery
export const UPDATE_SHOPING_CART_CUTLERY = `${PREFIX}shoping-cart/updateCutlery`;

// Fetch
export const SHOPING_CART_FETCH = `${PREFIX}shoping-cart-fetch`;
export const SHOPING_CART_STORES_FETCH = `${PREFIX}shoping-cart-fetch-stores`;
export const SHOPING_CART_FETCH_SAVE = `${PREFIX}shoping-cart-fetch/save`;
export const SHOPING_CART_FETCH_PENDING = `${PREFIX}shoping-cart-fetch/pending`;
export const SHOPING_CART_FETCH_SUCCESS = `${PREFIX}shoping-cart-fetch/success`;
export const SHOPING_CART_STORES_FETCH_SUCCESS = `${PREFIX}shoping-cart-fetch-stores/success`;
export const SHOPING_CART_FETCH_ERROR = `${PREFIX}shoping-cart-fetch/error`;

// Save
export const SHOPING_CART_SAVE = `${PREFIX}shoping-cart-save`;
export const SHOPING_CART_SAVE_PENDING = `${PREFIX}shoping-cart-save/pending`;
export const SHOPING_CART_SAVE_SUCCESS = `${PREFIX}shoping-cart-save/success`;
export const SHOPING_CART_SAVE_ERROR = `${PREFIX}shoping-cart-save/error`;
// Hack for getting rid of unneccessary "address did not match" pop-ups
export const SHOPING_CART_SAVE_RESET_UI = `${PREFIX}shoping-cart-save/resetUi`;
