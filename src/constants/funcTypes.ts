// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const F_TYPES_MODAL_SHOW = `${PREFIX}funcTypesModalShow`;

export const F_TYPES_FETCH = `${PREFIX}funcTypesFetch`;
export const F_TYPES_FETCH_PENDING = `${PREFIX}funcTypesFetchPending`;
export const F_TYPES_FETCH_SUCCESS = `${PREFIX}funcTypesFetchSuccess`;
export const F_TYPES_FETCH_ERROR = `${PREFIX}funcTypesFetchError`;
export const F_TYPES_FETCH_SAVE_DATA = `${PREFIX}funcTypesFetchSaveData`;
