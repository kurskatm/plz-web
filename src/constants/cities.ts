// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const CITIES_MODAL_SHOW = `${PREFIX}citiesModalShow`;

export const CITIES_FETCH = `${PREFIX}citiesFetch`;
export const CITIES_FETCH_PENDING = `${PREFIX}citiesFetchPending`;
export const CITIES_FETCH_SUCCESS = `${PREFIX}citiesFetchSuccess`;
export const CITIES_FETCH_ERROR = `${PREFIX}citiesFetchError`;
export const CITIES_FETCH_SAVE_DATA = `${PREFIX}citiesFetchSaveData`;
