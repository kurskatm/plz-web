// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

// Ux
export const HEADER_UX_SET_HEIGHT = `${PREFIX}headerUXSetHeight`;
