// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const INSTALL_MOBILE_APP_SHOW = `${PREFIX}installMobileAppShow`;
