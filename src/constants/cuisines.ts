// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const CUISINES_FETCH = `${PREFIX}cuisinesFetch`;
export const CUISINES_SAVE = `${PREFIX}cuisinesSave`;
export const CUISINES_PUSH = `${PREFIX}cuisinesPush`;
export const CUISINES_REMOVE = `${PREFIX}cuisinesRemove`;
export const CUISINES_UPDATE = `${PREFIX}cuisinesUpdate`;

export const CUISINES_FETCH_PENDING = `${PREFIX}cuisinesFetchPending`;
export const CUISINES_FETCH_SUCCESS = `${PREFIX}cuisinesFetchSuccess`;
export const CUISINES_FETCH_ERROR = `${PREFIX}cuisinesFetchError`;
