// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

// get free products
export const FREE_PRODUCTS_FETCH = `${PREFIX}freeProductsFetch`;
export const FREE_PRODUCTS_SAVE = `${PREFIX}freeProductsSave`;
export const FREE_PRODUCTS_RESET = `${PREFIX}freeProductsReset`;

export const FREE_PRODUCTS_PENDING = `${PREFIX}freeProductsPending`;
export const FREE_PRODUCTS_SUCCESS = `${PREFIX}freeProductsSuccess`;
export const FREE_PRODUCTS_ERROR = `${PREFIX}freeProductsError`;
