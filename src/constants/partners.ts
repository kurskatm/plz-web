// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

// Add partner
export const PARTNERS_BECOME_PARTNER_ADD = `${PREFIX}partnersBecomePartnerAdd`;
export const PARTNERS_BECOME_PARTNER_IDLE = `${PREFIX}partnersBecomePartnerIdle`;
export const PARTNERS_BECOME_PARTNER_PENDING = `${PREFIX}partnersBecomePartnerPending`;
export const PARTNERS_BECOME_PARTNER_SUCCESS = `${PREFIX}partnersBecomePartnerSuccess`;
export const PARTNERS_BECOME_PARTNER_ERROR = `${PREFIX}partnersBecomePartnerError`;

// Get counters
export const PARTNERS_COUNTERS_FETCH = `${PREFIX}partnersCountersFetch`;
export const PARTNERS_COUNTERS_PENDING = `${PREFIX}partnersCountersPending`;
export const PARTNERS_COUNTERS_SUCCESS = `${PREFIX}partnersCountersSuccess`;
export const PARTNERS_COUNTERS_ERROR = `${PREFIX}partnersCountersError`;
export const PARTNERS_COUNTERS_SAVE = `${PREFIX}partnersCountersSave`;
