// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

// get reviews
export const REVIEWS_FETCH = `${PREFIX}reviewsFetch`;
export const REVIEWS_SAVE = `${PREFIX}reviewsSave`;
export const REVIEWS_RESET = `${PREFIX}reviewsReset`;

export const REVIEWS_PENDING = `${PREFIX}reviewsPending`;
export const REVIEWS_SUCCESS = `${PREFIX}reviewsSuccess`;
export const REVIEWS_ERROR = `${PREFIX}reviewsError`;
