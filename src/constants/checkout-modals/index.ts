// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const CHANGE_ADDRESS_MODAL_SHOW = `${PREFIX}changeAddressModalShow`;
// A fix for showing "mismatch address" on redirection
export const CHANGE_ADDRESS_MODAL_HIDE_2ND_TIME = `${PREFIX}changeAddressModalHide2ndTime`;

export const CHANGE_BASKET_MODAL_SHOW = `${PREFIX}changeBasketModalShow`;

export const CHANGE_PROMOCODE_MODAL_SHOW = `${PREFIX}changePromocodeModalShow`;
