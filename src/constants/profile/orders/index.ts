// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

// Orders
export const PROFILE_GET_ORDERS = `${PREFIX}profile/orders`;
export const PROFILE_GET_ORDERS_PENDING = `${PREFIX}profile/ordersPending`;
export const PROFILE_GET_ORDERS_SUCCESS = `${PREFIX}profile/ordersSuccess`;
export const PROFILE_GET_ORDERS_ERROR = `${PREFIX}profile/ordersError`;
export const PROFILE_GET_ORDERS_SAVE_DATA = `${PREFIX}profile/ordersSaveData`;
export const PROFILE_GET_ORDERS_RESET_DATA = `${PREFIX}profile/ordersResetDate`;

export const PROFILE_SHOW_ORDER_REVIEW_MODAL = `${PREFIX}profile/showOrdersReviewModal`;

// Last order without review
export const PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW = `${PREFIX}profile/lastOrderWithoutReview`;
export const PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_PENDING = `${PREFIX}profile/lastOrderWithoutReviewPending`;
export const PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_SUCCESS = `${PREFIX}profile/lastOrderWithoutReviewSuccess`;
export const PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_ERROR = `${PREFIX}profile/lastOrderWithoutReviewError`;
export const PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_SAVE_DATA = `${PREFIX}profile/lastOrderWithoutReviewSaveData`;
export const PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_RESET_DATA = `${PREFIX}profile/lastOrderWithoutReviewResetData`;

// Current Order
export const PROFILE_GET_USER_ORDER = `${PREFIX}profile/userOrder`;
export const PROFILE_GET_USER_ORDER_PENDING = `${PREFIX}profile/userOrderPending`;
export const PROFILE_GET_USER_ORDER_SUCCESS = `${PREFIX}profile/userOrderSuccess`;
export const PROFILE_GET_USER_ORDER_ERROR = `${PREFIX}profile/userOrderError`;
export const PROFILE_GET_USER_ORDER_SAVE_DATA = `${PREFIX}profile/userOrderSaveData`;
export const PROFILE_RESET_USER_ORDER = `${PREFIX}profile/resetUserOrder`;
