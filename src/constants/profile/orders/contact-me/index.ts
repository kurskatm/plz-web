// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const PROFILE_ORDER_CONTACT_ME = `${PREFIX}profile/contactMe`;

export const PROFILE_ORDER_CONTACT_ME_PENDING = `${PREFIX}profile/contactMePending`;
export const PROFILE_ORDER_CONTACT_ME_IDLE = `${PREFIX}profile/contactMeIdle`;
export const PROFILE_ORDER_CONTACT_ME_SUCCESS = `${PREFIX}profile/contactMeSuccess`;
export const PROFILE_ORDER_CONTACT_ME_ERROR = `${PREFIX}profile/contactMeError`;

export const PROFILE_ORDER_CONTACT_ME_RESET = `${PREFIX}profile/contactMeReset`;
