// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const PROFILE_ORDER_REPEAT_ORDER = `${PREFIX}profile/repeatOrder`;

export const PROFILE_ORDER_REPEAT_ORDER_PENDING = `${PREFIX}profile/repeatOrderPending`;
export const PROFILE_ORDER_REPEAT_ORDER_SUCCESS = `${PREFIX}profile/repeatOrderSuccess`;
export const PROFILE_ORDER_REPEAT_ORDER_ERROR = `${PREFIX}profile/repeatOrderError`;

export const PROFILE_ORDER_REPEAT_ORDER_SAVE = `${PREFIX}profile/repeatOrderSave`;
export const PROFILE_ORDER_REPEAT_ORDER_RESET = `${PREFIX}profile/repeatOrderReset`;
export const PROFILE_ORDER_REPEAT_ORDER_RESET_REDIRECT = `${PREFIX}profile/repeatOrderResetRedirect`;
