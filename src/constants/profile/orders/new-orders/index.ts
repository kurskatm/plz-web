// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

// Orders
export const PROFILE_GET_NEW_ORDERS = `${PREFIX}profile/newOrders`;

export const PROFILE_GET_NEW_ORDERS_PENDING = `${PREFIX}profile/newOrdersPending`;
export const PROFILE_GET_NEW_ORDERS_SUCCESS = `${PREFIX}profile/newOrdersSuccess`;
export const PROFILE_GET_NEW_ORDERS_ERROR = `${PREFIX}profile/newOrdersError`;

export const PROFILE_GET_NEW_ORDERS_SAVE_DATA = `${PREFIX}profile/newOrdersSaveData`;
export const PROFILE_GET_NEW_ORDERS_RESET_DATA = `${PREFIX}profile/newOrdersResetData`;
