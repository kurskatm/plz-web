// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const PROFILE_GET_FAVORITE = `${PREFIX}profile/getFavorite`;
export const PROFILE_GET_FAVORITE_PRODUCTS = `${PREFIX}profile/getFavoriteProducts`;
export const PROFILE_GET_FAVORITE_PRODUCTS_PENDING = `${PREFIX}profile/getFavoriteProductsPending`;
export const PROFILE_GET_FAVORITE_PRODUCTS_SUCCESS = `${PREFIX}profile/getFavoriteProductsSuccess`;
export const PROFILE_GET_FAVORITE_PRODUCTS_ERROR = `${PREFIX}profile/getFavoriteProductsError`;
export const PROFILE_GET_FAVORITE_PRODUCTS_SAVE_DATA = `${PREFIX}profile/getFavoriteProductsSaveData`;

export const PROFILE_GET_FAVORITE_RESET_DATA = `${PREFIX}profile/getFavoriteResetData`;

export const PROFILE_SET_FAVOURITE_PRODUCTS = `${PREFIX}profile/setFavouriteProducts`;
export const PROFILE_SET_FAVOURITE_PRODUCTS_PENDING = `${PREFIX}profile/setFavouriteProductsPending`;
export const PROFILE_SET_FAVOURITE_PRODUCTS_SUCCESS = `${PREFIX}profile/setFavouriteProductsSuccess`;
export const PROFILE_SET_FAVOURITE_PRODUCTS_ERROR = `${PREFIX}profile/setFavouriteProductsError`;
export const PROFILE_SET_FAVOURITE_PRODUCTS_SAVE_DATA = `${PREFIX}profile/setFavouriteProductsSaveData`;

export const PROFILE_DELETE_FAVOURITE_PRODUCTS = `${PREFIX}profile/deleteFavouriteProducts`;
export const PROFILE_DELETE_FAVOURITE_PRODUCTS_PENDING = `${PREFIX}profile/deleteFavouriteProductsPending`;
export const PROFILE_DELETE_FAVOURITE_PRODUCTS_SUCCESS = `${PREFIX}profile/deleteFavouriteProductsSuccess`;
export const PROFILE_DELETE_FAVOURITE_PRODUCTS_ERROR = `${PREFIX}profile/deleteFavouriteProductsError`;
export const PROFILE_DELETE_FAVOURITE_PRODUCTS_SAVE_DATA = `${PREFIX}profile/deleteFavouriteProductsSaveData`;
