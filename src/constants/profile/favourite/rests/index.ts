// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const PROFILE_GET_FAVORITE = `${PREFIX}profile/getFavorite`;
export const PROFILE_GET_FAVORITE_RESTS = `${PREFIX}profile/getFavoriteRests`;
export const PROFILE_GET_FAVORITE_RESTS_PENDING = `${PREFIX}profile/getFavoriteRestsPending`;
export const PROFILE_GET_FAVORITE_RESTS_SUCCESS = `${PREFIX}profile/getFavoriteRestsSuccess`;
export const PROFILE_GET_FAVORITE_RESTS_ERROR = `${PREFIX}profile/getFavoriteRestsError`;
export const PROFILE_GET_FAVORITE_RESTS_SAVE_DATA = `${PREFIX}profile/getFavoriteRestsSaveData`;

export const PROFILE_SET_FAVOURITE_REST = `${PREFIX}profile/setFavouriteRest`;
export const PROFILE_SET_FAVOURITE_REST_PENDING = `${PREFIX}profile/setFavouriteRestPending`;
export const PROFILE_SET_FAVOURITE_REST_SUCCESS = `${PREFIX}profile/setFavouriteRestSuccess`;
export const PROFILE_SET_FAVOURITE_REST_ERROR = `${PREFIX}profile/setFavouriteRestError`;
export const PROFILE_SET_FAVOURITE_REST_SAVE_DATA = `${PREFIX}profile/setFavouriteRestSaveData`;

export const PROFILE_DELETE_FAVOURITE_REST = `${PREFIX}profile/deleteFavouriteRest`;
export const PROFILE_DELETE_FAVOURITE_REST_PENDING = `${PREFIX}profile/deleteFavouriteRestPending`;
export const PROFILE_DELETE_FAVOURITE_REST_SUCCESS = `${PREFIX}profile/deleteFavouriteRestSuccess`;
export const PROFILE_DELETE_FAVOURITE_REST_ERROR = `${PREFIX}profile/deleteFavouriteRestError`;
export const PROFILE_DELETE_FAVOURITE_REST_SAVE_DATA = `${PREFIX}profile/deleteFavouriteRestSaveData`;
