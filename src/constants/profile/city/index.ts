// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

// City
export const PROFILE_CITY_UPDATE = `${PREFIX}profileCity/update`;

export const PROFILE_RESET_DATA_AFTER_CITY_UPDATE = `${PREFIX}profile/resetDataAfterCityUpdate`;

export const PROFILE_CITY_AND_ADDRESS_UPDATE = `${PREFIX}profile/updateCityAndAddress`;
