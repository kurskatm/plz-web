// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

// Delivery
export const PROFILE_UPDATE_ADDRESS = `${PREFIX}profile/updateAddress`;
export const PROFILE_UPDATE_PICKUP = `${PREFIX}profile/updatePickup`;
