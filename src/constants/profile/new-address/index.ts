// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

// User Geocode
export const PROFILE_GET_NEW_ADDRESS = `${PREFIX}profile/newAddress`;
export const PROFILE_SAVE_NEW_ADDRESS = `${PREFIX}profile/saveNewAddress`;

export const PROFILE_UPDATE_NEW_ADDRESS = `${PREFIX}profile/updateNewAddress`;

export const PROFILE_GET_NEW_ADDRESS_PENDING = `${PREFIX}profile/newAddressPending`;
export const PROFILE_GET_NEW_ADDRESS_SUCCESS = `${PREFIX}profile/newAddressSuccess`;
export const PROFILE_GET_NEW_ADDRESS_ERROR = `${PREFIX}profile/newAddressError`;

export const PROFILE_RESET_NEW_ADDRESS = `${PREFIX}profile/resetNewAddress`;
export const PROFILE_RESET_NEW_ADDRESS_SUGGEST = `${PREFIX}profile/resetNewAddressSuggest`;
