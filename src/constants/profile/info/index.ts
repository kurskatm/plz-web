// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const PROFILE_GET_INFO = `${PREFIX}profile/info`;
export const PROFILE_GET_INFO_PENDING = `${PREFIX}profile/infoPending`;
export const PROFILE_GET_INFO_SUCCESS = `${PREFIX}profile/infoSuccess`;
export const PROFILE_GET_INFO_ERROR = `${PREFIX}profile/infoError`;
export const PROFILE_GET_INFO_SAVE_DATA = `${PREFIX}profile/infoSaveData`;
export const PROFILE_GET_INFO_RESET_UI = `${PREFIX}profile/infoResetUi`;

export const PROFILE_UPDATE_INFO = `${PREFIX}profile/updateInfo`;
export const PROFILE_UPDATE_INFO_SAVE = `${PREFIX}profile/updateInfoSave`;
export const PROFILE_UPDATE_INFO_PENDING = `${PREFIX}profile/updateInfoPending`;
export const PROFILE_UPDATE_INFO_SUCCESS = `${PREFIX}profile/updateInfoSucces`;
export const PROFILE_UPDATE_INFO_ERROR = `${PREFIX}profile/updateInfoError`;
export const PROFILE_UPDATE_INFO_RESET_UI = `${PREFIX}profile/updateInfoResetUi`;
