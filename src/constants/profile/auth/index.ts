// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const PROFILE_AUTHORIZED = `${PREFIX}profile/auth`;
export const PROFILE_UNAUTHORIZED = `${PREFIX}profile/unauth`;
