// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

// User Geocode
export const PROFILE_GET_NEW_ADDRESSES_GEOCODE = `${PREFIX}profile/newGeocode`;
export const PROFILE_SAVE_NEW_ADDRESSES_GEOCODE = `${PREFIX}profile/saveNewGeocode`;

export const PROFILE_GET_NEW_ADDRESSES_GEOCODE_PENDING = `${PREFIX}profile/newGeocodePending`;
export const PROFILE_GET_NEW_ADDRESSES_GEOCODE_SUCCESS = `${PREFIX}profile/newGeocodeSuccess`;
export const PROFILE_GET_NEW_ADDRESSES_GEOCODE_ERROR = `${PREFIX}profile/newGeocodeError`;

export const PROFILE_RESET_NEW_ADDRESSES_GEOCODE = `${PREFIX}profile/resetNewGeocode`;
