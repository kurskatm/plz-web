// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

// Friends
export const PROFILE_GET_FRIENDS = `${PREFIX}profile/friends`;
export const PROFILE_GET_FRIENDS_PENDING = `${PREFIX}profile/friendsPending`;
export const PROFILE_GET_FRIENDS_SUCCESS = `${PREFIX}profile/friendsSuccess`;
export const PROFILE_GET_FRIENDS_ERROR = `${PREFIX}profile/friendsError`;
export const PROFILE_GET_FRIENDS_SAVE_DATA = `${PREFIX}profile/friendsSaveData`;
