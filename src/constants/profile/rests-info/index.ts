// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const PROFILE_GET_RESTS_INFO = `${PREFIX}profile/restsInfo`;
export const PROFILE_GET_RESTS_INFO_PENDING = `${PREFIX}profile/restsInfoPending`;
export const PROFILE_GET_RESTS_INFO_SUCCESS = `${PREFIX}profile/restsInfoSuccess`;
export const PROFILE_GET_RESTS_INFO_ERROR = `${PREFIX}profile/restsInfoError`;
export const PROFILE_GET_RESTS_INFO_SAVE_DATA = `${PREFIX}profile/restsInfoSaveData`;
