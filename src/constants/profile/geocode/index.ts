// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

// User Geocode
export const PROFILE_GET_ADDRESSES_GEOCODE = `${PREFIX}profileGetAddressesGeocode`;
export const PROFILE_SAVE_ADDRESSES_GEOCODE = `${PREFIX}profileSaveAddressesGeocode`;

export const PROFILE_GET_ADDRESSES_GEOCODE_PENDING = `${PREFIX}profileGetAddressesGeocodePending`;
export const PROFILE_GET_ADDRESSES_GEOCODE_SUCCESS = `${PREFIX}profileGetAddressesGeocodeSuccess`;
export const PROFILE_GET_ADDRESSES_GEOCODE_ERROR = `${PREFIX}profileGetAddressesGeocodeError`;

export const PROFILE_RESET_ADDRESSES_GEOCODE = `${PREFIX}profileResetAddressesGeocode`;
