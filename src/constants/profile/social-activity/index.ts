// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const PROFILE_SET_SOCIAL_ACTIVITY = `${PREFIX}profile/setSocialActivity`;
export const PROFILE_SET_SOCIAL_ACTIVITY_PENDING = `${PREFIX}profile/setSocialActivityPending`;
export const PROFILE_SET_SOCIAL_ACTIVITY_SUCCESS = `${PREFIX}profile/setSocialActivitySuccess`;
export const PROFILE_SET_SOCIAL_ACTIVITY_ERROR = `${PREFIX}profile/setSocialActivityError`;
export const PROFILE_SET_SOCIAL_ACTIVITY_SAVE_DATA = `${PREFIX}profile/setSocialActivitySaveData`;
