// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

// Reviews
export const PROFILE_GET_REVIEWS = `${PREFIX}profile/reviews`;
export const PROFILE_GET_REVIEWS_PENDING = `${PREFIX}profile/reviewsPending`;
export const PROFILE_GET_REVIEWS_SUCCESS = `${PREFIX}profile/reviewsSuccess`;
export const PROFILE_GET_REVIEWS_ERROR = `${PREFIX}profile/reviewsError`;
export const PROFILE_GET_REVIEWS_SAVE_DATA = `${PREFIX}profile/reviewsSaveData`;
export const PROFILE_GET_REVIEWS_RESET_DATA = `${PREFIX}profile/reviewsResetData`;

// Order Reviews
export const PROFILE_SET_ORDER_REVIEW = `${PREFIX}profile/orderReview`;
export const PROFILE_SET_ORDER_REVIEW_PENDING = `${PREFIX}profile/orderReviewPending`;
export const PROFILE_SET_ORDER_REVIEW_SUCCESS = `${PREFIX}profile/orderReviewSuccess`;
export const PROFILE_SET_ORDER_REVIEW_ERROR = `${PREFIX}profile/orderReviewError`;
export const PROFILE_SAVE_ORDER_REVIEW_DATA = `${PREFIX}profile/orderReviewSaveData`;
