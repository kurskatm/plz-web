// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

// Get bonus
export const BONUS_FETCH = `${PREFIX}bonusFetch`;
export const BONUS_PENDING = `${PREFIX}bonusPending`;
export const BONUS_SUCCESS = `${PREFIX}bonusSuccess`;
export const BONUS_ERROR = `${PREFIX}bonusError`;
export const BONUS_SAVE = `${PREFIX}bonusSave`;
// Get bonus history
export const BONUS_HISTORY_FETCH = `${PREFIX}bonusHistoryFetch`;
export const BONUS_HISTORY_PENDING = `${PREFIX}bonusHistoryPending`;
export const BONUS_HISTORY_SUCCESS = `${PREFIX}bonusHistorySuccess`;
export const BONUS_HISTORY_ERROR = `${PREFIX}bonusHistoryError`;
export const BONUS_HISTORY_SAVE = `${PREFIX}bonusHistorySave`;

export const BONUS_RESET_DATA = `${PREFIX}bonusResetData`;

export const SOCIAL_ACTIVITY = {
  follow: {
    0: 'vk',
    1: 'inst',
    2: 'tiktok',
  },
  share: {
    3: 'vk',
    4: 'fb',
    5: 'ok',
  },
};
