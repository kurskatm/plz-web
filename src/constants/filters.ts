// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

export const FILTERS_FETCH = `${PREFIX}filtersFetch`;
export const FILTERS_SAVE = `${PREFIX}filtersSave`;
export const FILTERS_SET = `${PREFIX}filtersSet`;
export const FILTERS_SEARCH = `${PREFIX}filtersSearch`;

export const FILTERS_FETCH_PENDING = `${PREFIX}filtersFetchPending`;
export const FILTERS_FETCH_SUCCESS = `${PREFIX}filtersFetchSuccess`;
export const FILTERS_FETCH_ERROR = `${PREFIX}filtersFetchError`;

export const FILTERS_SHOW_MODAL = `${PREFIX}filtersShowModal`;
