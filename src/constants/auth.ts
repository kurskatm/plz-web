// Utils
import { Consts } from '@utils';

const { PREFIX } = Consts.ACTIONS;

// get auth
export const AUTH_FETCH = `${PREFIX}authFetch`;
export const AUTH_PHONE_SEND = `${PREFIX}authPhoneSend`;
export const AUTH_PHONE_CLEAN = `${PREFIX}authPhoneClean`;
export const AUTH_OTP_SEND = `${PREFIX}authOtpSend`;
export const AUTH_SAVE_PHONE_DATA = `${PREFIX}authSavePhoneData`;
export const AUTH_SAVE_OTP_DATA = `${PREFIX}authSaveOtpData`;

export const AUTH_PENDING = `${PREFIX}authPending`;
export const AUTH_SUCCESS = `${PREFIX}authSuccess`;
export const AUTH_ERROR = `${PREFIX}authError`;

export const AUTH_PHONE_STATUS_RESET = `${PREFIX}authPhoneStatusReset`;

export const AUTH_OTP_PENDING = `${PREFIX}authOtpPending`;
export const AUTH_OTP_SUCCESS = `${PREFIX}authOtpSuccess`;
export const AUTH_OTP_ERROR = `${PREFIX}authOtpError`;

export const AUTH_OTP_STATUS_RESET = `${PREFIX}authOtpStatusReset`;

export const AUTH_CLEAR_RESEND_TOKEN = `${PREFIX}authClearResendToken`;

export const AUTH_MODAL_SHOW = `${PREFIX}authModalShow`;

export const UNAUTHORIZE = `${PREFIX}unauthorize`;

export const AUTH_MODAL_SAVE_PHONE = `${PREFIX}authModalSavePhone`;
