// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Selectors
import { enhanceWithRedirectHomeStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/enhancers/redirect-home';

const withConnect = connect(mapStateToProps);
export const enhance = compose(withConnect);
