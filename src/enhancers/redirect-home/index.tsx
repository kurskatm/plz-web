// Modules
import React, { memo } from 'react';
import { Redirect } from 'react-router-dom';
// Enhance
import { enhance } from './enhance';
// Types
import { TWithRedirectHomeProps, TWithRedirectHome } from './types';

export const withRedirectHome: TWithRedirectHome = () => (WrappedComponent) =>
  memo(
    enhance(
      ({
        component,
        computedMatch,
        dispatch,
        exact,
        redirectUrl,
        path,
      }: TWithRedirectHomeProps) => {
        if (redirectUrl) {
          return <Redirect to={redirectUrl} />;
        }

        return (
          <WrappedComponent
            component={component}
            computedMatch={computedMatch}
            dispatch={dispatch}
            exact={exact}
            path={path}
          />
        );
      },
    ),
  );
