// Modules
import { LoadableComponent } from '@loadable/component';
import { NamedExoticComponent } from 'react';
import { Dispatch } from 'redux';

export interface TComputedMatch {
  isExact: boolean;
  params: Record<string, unknown>;
  path: string;
  url: string;
}

export interface TWithRedirectHomeProps {
  component?: LoadableComponent<Record<string, unknown>>;
  computedMatch?: TComputedMatch;
  dispatch?: Dispatch;
  exact?: boolean;
  redirectUrl?: string;
  path?: string;
}

export type TWrappedComponent = NamedExoticComponent<TWithRedirectHomeProps>;

export type TWithRedirectHome = () => (
  WrappedComponent: TWrappedComponent,
) => NamedExoticComponent<TWithRedirectHomeProps>;
