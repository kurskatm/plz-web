// Modules
import { TNewGeocodeData } from '@type/new-geocode';
import { LoadableComponent } from '@loadable/component';
import { NamedExoticComponent } from 'react';
import { Dispatch } from 'redux';

export interface TComputedMatch {
  isExact: boolean;
  params: Record<string, unknown>;
  path: string;
  url: string;
}

export interface TWithCheckCityProps {
  component?: LoadableComponent<Record<string, unknown>>;
  computedMatch?: TComputedMatch;
  dispatch?: Dispatch;
  exact?: boolean;
  isCity?: boolean;
  path?: string;
  shopingCartId?: string;
  shopingCartFetch?: (data: Record<string, unknown>) => void;
  shopingCartFetchSuccess?: boolean;
  geocode?: TNewGeocodeData;
}

export type TWrappedComponent = NamedExoticComponent<TWithCheckCityProps>;

export type TWithCheckCity = () => (
  WrappedComponent: TWrappedComponent,
) => NamedExoticComponent<TWithCheckCityProps>;
