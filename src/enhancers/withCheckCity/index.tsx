// Modules
import React, { memo } from 'react';
import { isNil } from 'lodash';
// Components
import { RedirectNotFound } from '@routes/RedirectNotFound';
// Lib
import { getCartId, removeCartId } from '@lib/cookie';
// Enhance
import { enhance } from './enhance';
// Types
import { TWithCheckCityProps, TWithCheckCity } from './types';

export const withCheckCity: TWithCheckCity = () => (WrappedComponent) =>
  memo(
    enhance(
      ({
        component,
        computedMatch,
        dispatch,
        exact,
        isCity,
        path,
        shopingCartFetch,
        shopingCartId,
        shopingCartFetchSuccess,
        geocode,
      }: TWithCheckCityProps) => {
        const cartIdFromCookie = getCartId();
        if (!shopingCartFetchSuccess) {
          const args = {
            cartId: shopingCartId,
          };

          if (!isNil(geocode?.lat)) {
            // @ts-ignore
            args.lat = geocode?.lat;
          }

          if (!isNil(geocode?.lng)) {
            // @ts-ignore
            args.lng = geocode?.lng;
          }

          if (cartIdFromCookie) {
            setTimeout(() => {
              shopingCartFetch(args);
              removeCartId();
            }, 600);
          } else {
            shopingCartFetch(args);
          }
        }

        if (isCity) {
          return (
            <WrappedComponent
              component={component}
              computedMatch={computedMatch}
              dispatch={dispatch}
              exact={exact}
              isCity={isCity}
              path={path}
            />
          );
        }

        return <RedirectNotFound />;
      },
    ),
  );
