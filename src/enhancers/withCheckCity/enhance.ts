// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Selectors
import { enhanceWithCheckCityStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/enhancers/with-check-city';
// Actions
import { shopingCartFetch } from '@actions/shoping-cart/fetch';

const mapDispatchToProps = {
  shopingCartFetch,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);
export const enhance = compose(withConnect);
