/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable @typescript-eslint/no-explicit-any */
// Modules
import React, { memo } from 'react';
// Consumer
import { QaSelectorsConsumer } from '@qa/provider';
// Types
import { TWithQa } from './types';

export const withQa: TWithQa = () => (WrappedComponent) =>
  memo(({ ...props }: any) => (
    <QaSelectorsConsumer>{({ qa }) => <WrappedComponent qa={qa} {...props} />}</QaSelectorsConsumer>
  ));
