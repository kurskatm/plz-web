/* eslint-disable @typescript-eslint/no-explicit-any */
// Modules Types
import { NamedExoticComponent } from 'react';
// import { WithIntlProps } from 'react-intl';

export type TWithQa = (options?: any) => (WrappedComponent: NamedExoticComponent<any>) => any;
