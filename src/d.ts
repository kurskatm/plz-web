/* eslint-disable @typescript-eslint/no-explicit-any */
/// <reference types="node" />
// eslint-disable-next-line @typescript-eslint/triple-slash-reference
/// <reference types="react" />
/// <reference types="react-dom" />

declare module '*.bmp' {
  const src: string;
  export default src;
}

declare module '*.gif' {
  const src: string;
  export default src;
}

declare module '*.jpg' {
  const src: string;
  export default src;
}

declare module '*.jpeg' {
  const src: string;
  export default src;
}

declare module '*.png' {
  const src: string;
  export default src;
}

declare module '*.webp' {
  const src: string;
  export default src;
}

declare module '*.svg' {
  import * as React from 'react';

  export const ReactComponent: React.FunctionComponent<React.SVGProps<SVGSVGElement>>;

  const src: string;
  export default src;
}

declare module '*.module.css' {
  const classes: { readonly [key: string]: string };
  export default classes;
}

declare module 'chibbis-ui-kit' {
  const Amount: any;
  const AmountField: any;
  const AppButtons: any;
  const AvatarIcon: any;
  const Checkbox: any;
  const Copyright: any;
  const Coll: any;
  const Dropdown: any;
  const Grid: any;
  const Row: any;
  const ArrowEmptyIcon: any;
  const BoxIcon: any;
  const Divider: any;
  const Hint: any;
  const Layout: any;
  const GridItem: any;
  const Select: any;
  const Autocomplete: any;
  const Option: any;
  const GeoIcon: any;
  const GoBackArrowIcon: any;
  const HomeIcon: any;
  const HourglassIcon: any;
  const InstagramOutlineIcon: any;
  const LockIcon: any;
  const OrderOutlineIcon: any;
  const RepostOutlineIcon: any;
  const SearchIcon: any;
  const CartIcon: any;
  const ClickOutlineIcon: any;
  const CloseIcon: any;
  const ClearIcon: any;
  const CutleryOutlineIcon: any;
  const FeedbackOutlineIcon: any;
  const FriendsOutlineIcon: any;
  const Input: any;
  const InputField: any;
  const InputPhone: any;
  const ModalWindow: any;
  const RadioField: any;
  const SelectField: any;
  const AutocompleteField: any;
  const CheckboxField: any;
  const TextareaField: any;
  const Button: any;
  const HelpIcon: any;
  const BoxOutlineIcon: any;
  const BoxInlineIcon: any;
  const UserIcon: any;
  const FlameIcon: any;
  const HundredPointsIcon: any;
  const MoneyBoyIcon: any;
  const OkHandIcon: any;
  const SparklesIcon: any;
  const CircleStarIcon: any;
  const VkOutlineIcon: any;
  const IconText: any;
  const IconWrap: any;
  const Link: any;
  const Liked: any;
  const Enum: any;
  const Title: any;
  const Time: any;
  const Price: any;
  const SmallFeedback: any;
  const RibbonLabel: any;
  const Schedule: any;
  const SocialButtons: any;
  const Text: any;
  const useBeforeFirstRender: any;
  const ScoresSocialsCard: any;
  const AlienIcon: any;
  const BirdIcon: any;
  const PaperPlaneIcon: any;
  const PandaIcon: any;
  const FoxIcon: any;
  const LionIcon: any;
  const HorseIcon: any;
  const ScoresIcon: any;
  const FavoriteIcon: any;
  const LightingIcon: any;
  const ReviewsIcon: any;
  const SwitchField: any;
  const BigLikeIcon: any;
  const BigDislikeIcon: any;
  const FlapperIcon: any;
  const PlateIcon: any;
  const LoadingSpinner: any;

  export {
    Amount,
    AppButtons,
    AvatarIcon,
    Checkbox,
    Dropdown,
    Hint,
    Layout,
    GridItem,
    Select,
    Autocomplete,
    Option,
    Coll,
    Grid,
    Row,
    ArrowEmptyIcon,
    BoxIcon,
    BoxOutlineIcon,
    GeoIcon,
    GoBackArrowIcon,
    CartIcon,
    ClickOutlineIcon,
    CloseIcon,
    ClearIcon,
    CutleryOutlineIcon,
    Copyright,
    FeedbackOutlineIcon,
    FriendsOutlineIcon,
    HelpIcon,
    HomeIcon,
    InstagramOutlineIcon,
    LockIcon,
    LoadingSpinner,
    OrderOutlineIcon,
    RepostOutlineIcon,
    SearchIcon,
    UserIcon,
    FlameIcon,
    HundredPointsIcon,
    MoneyBoyIcon,
    OkHandIcon,
    SparklesIcon,
    CircleStarIcon,
    Input,
    InputField,
    InputPhone,
    RadioField,
    SwitchField,
    TextareaField,
    SelectField,
    AutocompleteField,
    CheckboxField,
    Button,
    Link,
    Liked,
    ModalWindow,
    Enum,
    Title,
    Time,
    Price,
    RibbonLabel,
    Schedule,
    SmallFeedback,
    Text,
    AmountField,
    Divider,
    SocialButtons,
    useBeforeFirstRender,
    ScoresSocialsCard,
    AlienIcon,
    BirdIcon,
    PandaIcon,
    PaperPlaneIcon,
    FoxIcon,
    LionIcon,
    HorseIcon,
    ScoresIcon,
    FavoriteIcon,
    LightingIcon,
    ReviewsIcon,
    BigLikeIcon,
    BigDislikeIcon,
    FlapperIcon,
    PlateIcon,
    BoxInlineIcon,
    HourglassIcon,
  };
}

declare module 'react-google-recaptcha-v3' {
  const GoogleReCaptcha: any;
  const GoogleReCaptchaProvider: any;
  const useGoogleReCaptcha: any;

  export { GoogleReCaptcha, GoogleReCaptchaProvider, useGoogleReCaptcha };
}
