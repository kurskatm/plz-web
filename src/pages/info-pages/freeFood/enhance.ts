// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { authModalShow } from '@actions/auth';
import { profileDeleteFavoriteRests, profileSetFavoriteRests } from '@actions/profile/favourite';
import { changeAddressModalShow } from '@actions/checkout/modals';
import { pushNotification } from '@actions/notifications';
import { shopingCartUpdate } from '@actions/shoping-cart/change';
import { shopingCartSaveResetUi } from '@actions/shoping-cart/save';

import { foodForPointsFetch, foodForPointsReset } from '@actions/foodForPoints';

// Selectors
import { freeProductsStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/products';

const mapDispatchToProps = {
  authModalShow,
  profileDeleteFavoriteRests,
  profileSetFavoriteRests,
  changeAddressModalShow,
  pushNotification,
  shopingCartUpdate,
  shopingCartSaveResetUi,
  foodForPointsFetch,
  foodForPointsReset,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
