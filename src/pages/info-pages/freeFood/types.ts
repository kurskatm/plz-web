// actions
import {
  TProfileDeleteFavoriteRests,
  TProfileSetFavoriteRests,
} from '@actions/profile/favourite/action-types';
import { TChangeAddressModalShow } from '@/actions/checkout/modals/action-types';
import { TUpdateShopingCart } from '@actions/shoping-cart/change/action-types';
import { TShopingCartSaveResetUi } from '@actions/shoping-cart/save/action-types';
import { TFoodForPointsFetch, TFoodForPointsReset } from '@actions/foodForPoints/action-types';

// models
import { TShopingCartModel } from '@models/shoping-cart/types';
import { TProfileModel } from '@models/profile/types';

// types
import { TCitiesItem } from '@type/cities';
import { TFoodForPoints } from '@type/foodForPoints';

export type TFreeFoodComponentProps = {
  cityId: string;
  city: TCitiesItem;
  authModalShow: (data: boolean) => void;
  profile: TProfileModel;
  profileDeleteFavoriteRests: TProfileDeleteFavoriteRests;
  profileSetFavoriteRests: TProfileSetFavoriteRests;
  changeAddressModalShow: TChangeAddressModalShow;
  shopingCartSaveError: boolean;
  shopingCart: TShopingCartModel;
  shopingCartUpdate: TUpdateShopingCart;
  shopingCartSaveResetUi: TShopingCartSaveResetUi;
  foodForPoints: TFoodForPoints;
  foodForPointsisIdle: boolean;
  foodForPointsisError: boolean;
  foodForPointsisPending: boolean;
  foodForPointsFetch: TFoodForPointsFetch;
  foodForPointsReset: TFoodForPointsReset;
};
