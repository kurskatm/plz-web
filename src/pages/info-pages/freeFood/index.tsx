/* eslint-disable max-len */
// Modules
import React, { FC, useEffect, memo, useCallback, useMemo } from 'react';
import { useMediaQuery } from 'react-responsive';
import loadable from '@loadable/component';

import { useCardResize } from '@utils/useCardResize';
import { likeClickHandler } from '@lib/like';

// Components
// Main Components
import GoToOldSite from '@components/MoveToOldDesignBanner/Banner';
import ContentViewer from '@components/ContentViewer';
import RestaurantCard from '@components/NewRestaurantCard';
import CheckoutAddressModal from '@/components/CheckoutAddressModal';
import HitCard from '@components/HitCard';

// Components
import { ScoresIcon } from 'chibbis-ui-kit';
// Main Types
import { TRestaurantCard } from '@type/restaurants/restaurant-card';
import { TRestaurantProductModifiers } from '@type/restaurants/restaurant-products';

// Utils
import { Consts, useShopingCartUpdate } from '@utils';
// Types
import { TFreeFoodComponentProps } from './types';
// Enhance
import { enhance } from './enhance';

const { CITIES } = Consts.ROUTES;

const Header = loadable(() => import('@components/Header'));
const Footer = loadable(() => import('@components/Footer'));

const CardWrapper: FC = ({ children }) => (
  <div className="free-food-page__rests__list">{children}</div>
);

const CardWrapperProducts: FC = ({ children }) => (
  <div className="free-food-page__products__list">{children}</div>
);

const FreeFoodComponent: FC<TFreeFoodComponentProps> = ({
  cityId,
  city,
  authModalShow,
  profile,
  profileDeleteFavoriteRests,
  profileSetFavoriteRests,
  shopingCartSaveError,
  shopingCartUpdate,
  shopingCart,
  shopingCartSaveResetUi,
  foodForPoints,
  foodForPointsisIdle,
  foodForPointsFetch,
  foodForPointsReset,
}) => {
  const isTwoCards = useMediaQuery({
    minWidth: 769,
  });
  const isOneCard = useMediaQuery({
    maxWidth: 768,
  });

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  useEffect(() => {
    if (foodForPointsisIdle) {
      const foodForPointsArgs = { cityId };

      if (profile.newGeoCode.data) {
        // @ts-ignore
        foodForPointsArgs.lat = profile.newGeoCode.data.lat;
        // @ts-ignore
        foodForPointsArgs.lng = profile.newGeoCode.data.lng;
      }
      foodForPointsFetch(foodForPointsArgs);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(
    () => () => {
      foodForPointsReset();
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const { setCardHeight, getCardHeight } = useCardResize({
    cardsLength: foodForPoints?.products?.length,
    isOneCard,
    isTwoCards,
  });

  const getUrl = useCallback((urlName: string) => `/${city?.urlName}/restaurant/${urlName}`, [
    city?.urlName,
  ]);

  // const history = useHistory();

  const renderItem = useMemo(
    () => (item: TRestaurantCard) => (
      <RestaurantCard
        onLikeClick={likeClickHandler({
          id: item.id,
          type: 'restId',
          additionalData: {
            newRestaurant: item,
          },
          likeAction: profileSetFavoriteRests,
          unLikeAction: profileDeleteFavoriteRests,
          auth: !profile.auth && authModalShow,
        })}
        key={item.id}
        item={item}
        url={getUrl(item.urlName)}
      />
    ),
    [getUrl, profile.auth, authModalShow, profileDeleteFavoriteRests, profileSetFavoriteRests],
  );

  useEffect(
    () => {
      if (shopingCartSaveError) {
        shopingCartSaveResetUi();
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [shopingCartSaveError],
  );

  const [updateShopingCart] = useShopingCartUpdate({
    city,
    shopingCart,
    shopingCartUpdate,
    profile,
    configName: 'freeFood',
  });

  const renderScoresSpendFood = useMemo(
    () => (item: TRestaurantProductModifiers, index: number) =>
      foodForPoints?.products?.length ? (
        <>
          <HitCard
            key={item.id}
            onLikeClick={() => {}}
            item={item}
            updateShopingCart={updateShopingCart(item)}
            withLike={false}
            index={index}
            setItemHeight={setCardHeight}
            bodyHeight={getCardHeight(index)}
            withRestLogo={false}
          />
        </>
      ) : null,
    [foodForPoints, updateShopingCart, setCardHeight, getCardHeight],
  );

  return (
    <>
      <div className="home-page free-food-page grid-wrapper">
        <header className="grid-wrapper__header">
          <Header
            goBackText="На главную"
            goBackLink={CITIES.PATH}
            noAddress
            noMenu
            title="Бесплатная еда"
          />
        </header>
        <main className="grid-wrapper__main">
          <div className="free-food-page__content">
            <div className="free-food-page__steps pad-f">
              <div className="free-food-page__step free-food-page__step_one">
                <div className="free-food-page__num">1</div>
                <div className="free-food-page__step-wrap">
                  <div className="free-food-page__title">Зарегистрируйтесь</div>
                  <div className="free-food-page__description">
                    и сразу получите&nbsp;
                    <ScoresIcon />
                    600 баллов
                  </div>
                </div>
              </div>
              <div className="free-food-page__step free-food-page__step_two">
                <div className="free-food-page__num">2</div>
                <div className="free-food-page__step-wrap">
                  <div className="free-food-page__title">Выберите ресторан</div>
                  <div className="free-food-page__description">
                    с оранжевой ленточкой «За баллы»
                  </div>
                </div>
              </div>
              <div className="free-food-page__step free-food-page__step_three">
                <div className="free-food-page__num">3</div>
                <div className="free-food-page__step-wrap">
                  <div className="free-food-page__title">Добавьте еду в корзину</div>
                  <div className="free-food-page__description">хотя бы на минимальную сумму</div>
                </div>
              </div>
              <div className="free-food-page__step free-food-page__step_four">
                <div className="free-food-page__num">4</div>
                <div className="free-food-page__step-wrap">
                  <div className="free-food-page__title">Возьмите +1 блюдо бесплатно</div>
                  <div className="free-food-page__description">из категории «За баллы»</div>
                </div>
              </div>
            </div>
            {foodForPoints?.restaurants?.length && (
              <div className="free-food-page__rests pad-f">
                <ContentViewer
                  list={foodForPoints?.restaurants}
                  renderItem={renderItem}
                  responsive={{
                    desktop: 12,
                    tablet: 6,
                    mobile: 3,
                  }}
                  wrapper={CardWrapper}
                  useAutoLoad
                />
              </div>
            )}
            {foodForPoints?.products?.length && (
              <div className="free-food-page__products pad-f">
                <div className="free-food-page__products__title">Лучшие блюда за баллы</div>
                <ContentViewer
                  list={foodForPoints?.products}
                  renderItem={renderScoresSpendFood}
                  responsive={{
                    desktop: 12,
                    tablet: 8,
                    mobile: 4,
                  }}
                  wrapper={CardWrapperProducts}
                />
              </div>
            )}
          </div>
        </main>
        <footer className="grid-wrapper__footer">
          <Footer />
        </footer>
        <CheckoutAddressModal />
      </div>
      <GoToOldSite />
    </>
  );
};

export default enhance(memo(FreeFoodComponent));
