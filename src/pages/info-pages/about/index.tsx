// Modules
import React, { memo, FC, useEffect } from 'react';
import loadable from '@loadable/component';
// Modules Components
import {
  Title,
  Text,
  FlameIcon,
  HundredPointsIcon,
  MoneyBoyIcon,
  OkHandIcon,
  SparklesIcon,
  CircleStarIcon,
} from 'chibbis-ui-kit';
import GoToOldSite from '@components/MoveToOldDesignBanner/Banner';
// Utils
import { Consts } from '@utils';

const { CITIES } = Consts.ROUTES;

const Header = loadable(() => import('@components/Header'));
const Footer = loadable(() => import('@components/Footer'));
const Content = loadable(() => import('@components/Content'));

/* eslint-disable max-len */
const AboutComponent: FC = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <>
      <div className="info-page about-page grid-wrapper">
        <header className="grid-wrapper__header">
          <Header
            goBackText="На главную"
            goBackLink={CITIES.PATH}
            noAddress
            noMenu
            title="О проекте"
          />
        </header>
        <main className="grid-wrapper__main">
          <div className="info-page__header">
            <div className="info-page__header-food" />
            <Content className="pad-f">
              <div className="info-page__header-text">
                <Title variant="h3">
                  Chibbis &ndash; это сервис по заказу еды, где&nbsp;представлены лучшие заведения
                  твоего&nbsp;города.
                </Title>
                <Text variant="variant3">
                  Сделать заказ &ndash; легко как раз-два-три. Приглашай друзей, копи баллы и
                  обменивай их на&nbsp;бесплатную еду или&nbsp;подарки, ищи самые выгодные
                  предложения!
                </Text>
              </div>
            </Content>
          </div>
          <div>
            <Content className="pad-f">
              <Text className="info-page__text">
                <h2 className="about-page__title">Наши преимущества</h2>
                <ul className="check-list">
                  <li>
                    <h3 className="check-list__title">
                      Очевидная выгода
                      <span role="img" aria-label="splash">
                        <SparklesIcon width={18} height={18} />
                      </span>
                    </h3>
                    <p>
                      Никаких наценок! Стоимость блюд, как в&nbsp;ресторанах, но&nbsp;приятные
                      бонусы только у&nbsp;нас.
                    </p>
                  </li>
                  <li>
                    <h3 className="check-list__title">
                      Горячие предложения
                      <span role="img" aria-label="100">
                        <FlameIcon width={18} height={18} />
                      </span>
                    </h3>
                    <p>Ежедневные акции и скидки от&nbsp;разных ресторанов в&nbsp;одном месте.</p>
                  </li>
                  <li>
                    <h3 className="check-list__title">
                      Гарантия качества
                      <span role="img" aria-label="100">
                        <HundredPointsIcon width={18} height={18} />
                      </span>
                    </h3>
                    <p>Потому что мы сотрудничаем только с&nbsp;лучшими заведениями.</p>
                  </li>
                  <li>
                    <h3 className="check-list__title">
                      Еда за баллы
                      <span role="img" aria-label="balls">
                        <MoneyBoyIcon width={18} height={18} />
                      </span>
                    </h3>
                    <p>Вы можете обменять накопленные баллы на еще одно блюдо к&nbsp;заказу.</p>
                  </li>
                  <li>
                    <h3 className="check-list__title">
                      Все заказы онлайн!
                      <span role="img" aria-label="online">
                        <CircleStarIcon width={18} height={18} />
                      </span>
                    </h3>
                    <p>Никаких разговоров с оператором, никакой путаницы в&nbsp;вашем заказе.</p>
                  </li>
                  <li>
                    <h3 className="check-list__title">
                      Простота
                      <span role="img" aria-label="online">
                        <OkHandIcon width={18} height={18} />
                      </span>
                    </h3>
                    <p>
                      Над сервисом работают ведущие разработчики России, поэтому сервис прост
                      и&nbsp;понятен в&nbsp;обращении.
                    </p>
                  </li>
                </ul>
              </Text>
            </Content>
          </div>
          <div className="about-page__fund">
            <Content className="pad-f">
              <div className="about-page__fund-logo" />
              <div className="about-page__fund-text">
                Инновационный проект реализуется при&nbsp;финансовой поддержке Фонда содействия
                инновациям (http://fasie.ru) в&nbsp;целях реализации национальной программы Цифровая
                экономика Российской Федерации
              </div>
            </Content>
          </div>
        </main>
        <footer className="grid-wrapper__footer">
          <Footer />
        </footer>
      </div>
      <GoToOldSite />
    </>
  );
};

export default memo(AboutComponent);
