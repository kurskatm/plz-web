import { TCitiesItem } from '@type/cities';

export interface TReglamentProps {
  city: TCitiesItem;
}
