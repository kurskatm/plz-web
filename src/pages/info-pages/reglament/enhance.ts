// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Selectors
import { reglamentStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/reglament';

const withConnect = connect(mapStateToProps);
export const enhance = compose(withConnect);
