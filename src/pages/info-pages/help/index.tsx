// Modules
import React, { memo, FC, useEffect } from 'react';
import loadable from '@loadable/component';
import GoToOldSite from '@components/MoveToOldDesignBanner/Banner';
// Utils
import { Consts } from '@utils';

const { CITIES } = Consts.ROUTES;

const Header = loadable(() => import('@components/Header'));
const Footer = loadable(() => import('@components/Footer'));
const Content = loadable(() => import('@components/Content'));
const Help = loadable(() => import('./Help'));
const HelpFooter = loadable(() => import('./Help/Footer'));

const HelpComponent: FC = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <>
      <div className="info-page help-page grid-wrapper">
        <header className="grid-wrapper__header">
          <Header
            goBackText="На главную"
            goBackLink={CITIES.PATH}
            noAddress
            noMenu
            title="Помощь"
          />
        </header>
        <main className="grid-wrapper__main">
          <div className="info-page__content-wrap">
            <Content className="pad-f">
              <Help />
            </Content>
          </div>
        </main>
        <HelpFooter />
        <footer className="grid-wrapper__footer">
          <Footer />
        </footer>
      </div>
      <GoToOldSite />
    </>
  );
};

export default memo(HelpComponent);
