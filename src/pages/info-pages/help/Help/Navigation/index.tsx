// Modules
import React, { FC, memo } from 'react';
import { Link } from 'react-scroll';
// Utils
import { HELP_NAV } from '../consts';

const HelpNavigationComponent: FC = () => (
  <div className="help__nav">
    <div className="help__nav-items">
      {HELP_NAV.map(({ title, anchor }) => (
        <div key={anchor} className="help__nav-item">
          <Link activeClass="active" to={anchor} spy smooth hashSpy offset={-150}>
            <span>{title}</span>
          </Link>
        </div>
      ))}
    </div>
  </div>
);

export default memo(HelpNavigationComponent);
