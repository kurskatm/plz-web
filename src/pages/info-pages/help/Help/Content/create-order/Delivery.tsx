// Modules
import React, { FC, memo } from 'react';
import { Title } from 'chibbis-ui-kit';
// Assets
import DeliveryCourierDesktop from '@/../assets/img/help/delivery-courier-desktop.jpg';
import DeliverySelfDesktop from '@/../assets/img/help/delivery-self-desktop.jpg';
import DeliveryCourierTablet from '@/../assets/img/help/delivery-courier-tablet.jpg';
import DeliverySelfTablet from '@/../assets/img/help/delivery-self-tablet.jpg';
import DeliveryCourierMobile from '@/../assets/img/help/delivery-courier-mobile.jpg';
import DeliverySelfMobile from '@/../assets/img/help/delivery-self-mobile.jpg';

const HelpCreateOrderBasketComponent: FC = () => (
  <>
    <div className="desktop">
      <Title>Оформление заказа</Title>
      <p>
        Выберите способ получения заказа &laquo;Доставка&raquo; или&nbsp;&laquo;Самовывоз&raquo;.
      </p>
      <div className="help__delivery-desktop-courier">
        <div className="help__delivery-desktop-title help__delivery-desktop-title-courier">
          <Title variant="h3">Доставка курьером</Title>
          <div className="help__delivery-desktop-title-column help__delivery-desktop-title-phone">
            Укажите ваш номер телефона
          </div>
          <div className="help__delivery-desktop-title-column help__delivery-desktop-title-address">
            Дополнительный адрес
          </div>
          <div className="help__delivery-desktop-title-column help__delivery-desktop-title-time">
            Ваши пожелания, например удобное для
            <br />
            вас время доставки курьером
          </div>
          <div className="help__delivery-desktop-title-column help__delivery-desktop-title-payment">
            Выберите удобный способ оплаты
          </div>
          <div className="help__delivery-desktop-title-column help__delivery-desktop-title-promocode">
            Введите промокод, если он у вас есть
          </div>
          <div>Нажмите кнопку &laquo;Оплатить&raquo;</div>
        </div>
        <div className="help__delivery-desktop-img">
          <img src={DeliveryCourierDesktop} alt="Доставка курьером" height={562} />
        </div>
      </div>
      <div className="help__delivery-desktop-self">
        <div className="help__delivery-desktop-title help__delivery-desktop-title-self">
          <Title variant="h3">Самовывоз</Title>
          <div className="help__delivery-desktop-title-column help__delivery-desktop-title-phone">
            Укажите ваш номер телефона
          </div>
          <div className="help__delivery-desktop-title-column help__delivery-desktop-title-address__selfPickup">
            Выберите, где вы хотите забрать заказ
          </div>
          <div className="help__delivery-desktop-title-column help__delivery-desktop-title-time__selfPickup">
            Ваши пожелания, например удобное для
            <br />
            вас время доставки курьером
          </div>
          <div className="help__delivery-desktop-title-column help__delivery-desktop-title-payment">
            Выберите удобный способ оплаты
          </div>
          <div className="help__delivery-desktop-title-column help__delivery-desktop-title-promocode">
            Введите промокод, если он у вас есть
          </div>
        </div>
        <div className="help__delivery-desktop-img">
          <img src={DeliverySelfDesktop} alt="Доставка курьером" height={562} />
        </div>
      </div>
      <div className="help__comment">
        Если вы нажали &laquo;Оформить заказ&raquo; и не зарегистрированы, появится окно,
        в&nbsp;которое нужно ввести свой телефон. На этот номер позвонит Chibbis. Принимать звонок
        не&nbsp;нужно, достаточно указать последние 4 цифры номера, с&nbsp;которого вам позвонили.
        Если звонка не&nbsp;было, вы сможете запросить код по&nbsp;СМС.
      </div>
      <p>
        После того, как вы оформили заказ с вами свяжется CHIBBIS и&nbsp;вам только останется
        дождаться своего заказа.
      </p>
    </div>
    <div className="tablet">
      <div className="help__delivery-tablet">
        <div className="help__delivery-tablet-courier">
          <img src={DeliveryCourierTablet} alt="Доставка курьером" height={562} />
        </div>
        <div className="help__delivery-tablet-self">
          <p>
            При выборе &laquo;Самовывоз&raquo; вам будут предложены адреса, где&nbsp;забрать заказ.
          </p>
          <img src={DeliverySelfTablet} alt="Самовывоз" height={246} />
        </div>
      </div>
      <p>
        После оформления заказа в течение часа вам позвонит Chibbis и удостоверит в том, что заказ
        принят и уточнит время когда сможете забрать/получить его.
      </p>
      <p>Если вы еще не зарегистрированы на сайте, то&nbsp;вам предложат регистрацию.</p>
    </div>
    <div className="mobile">
      <img
        className="help__img full-width mobile"
        src={DeliveryCourierMobile}
        alt="Доставка курьером"
        height={1272.5}
      />
      <p>При выборе &laquo;Самовывоз&raquo; вам будут предложены адреса, где&nbsp;забрать заказ.</p>
      <img
        className="help__img full-width mobile"
        src={DeliverySelfMobile}
        alt="Самовывоз"
        height={353}
      />
      <p>
        После оформления заказа в течение часа
        <br />
        вам позвонит Chibbis и удостоверит в том,
        <br />
        что заказ принят и уточнит время когда
        <br />
        сможете забрать/получить его.
      </p>
      <p>Если вы еще не зарегистрированы на сайте, то&nbsp;вам предложат регистрацию.</p>
    </div>
  </>
);

export default memo(HelpCreateOrderBasketComponent);
