// Modules
import React, { FC, memo } from 'react';
// Assets
import AllHitsDesktop from '@/../assets/img/help/all-hits-desktop.jpg';
import AllHitsTablet from '@/../assets/img/help/all-hits-tablet.jpg';
import RestHitsDesktop from '@/../assets/img/help/rest-hits-desktop.jpg';
import RestHitsTablet from '@/../assets/img/help/rest-hits-tablet.jpg';
import RestHitsMobile from '@/../assets/img/help/rest-hits-mobile.jpg';
import RestCard from '@/../assets/img/help/rest-card-mobile.jpg';

const HelpCreateOrderHitsComponent: FC = () => (
  <>
    <div className="desktop tablet">
      <p>
        Хиты продаж, это блюда, которые пользуются наибольшей популярностью у&nbsp;всех
        пользователей. На&nbsp;главной странице показываются хиты всех ресторанов.
        <br />
        Но вы также сможете выбрать &laquo;хиты продаж&raquo; в&nbsp;каждом конкретном ресторане.
      </p>
      <img
        className="help__img full-width desktop"
        src={AllHitsDesktop}
        alt="Все хиты продаж"
        height={458}
      />
      <img
        className="help__img full-width tablet"
        src={AllHitsTablet}
        alt="Все хиты продаж"
        height={1085.5}
      />
      <p>
        Когда вы выбрали ресторан, у вас есть возможность переключаться по&nbsp;разделам: меню
        (выбор видов блюд), акции, вся информация о&nbsp;ресторане, отзывы о&nbsp;ресторане
        и&nbsp;блюдах.
      </p>
      <img
        className="help__img full-width desktop"
        src={RestHitsDesktop}
        alt="Хиты продаж ресторана"
        height={460.5}
      />
      <img
        className="help__img full-width tablet"
        src={RestHitsTablet}
        alt="Хиты продаж ресторана"
        height={993}
      />
    </div>
    <div className="mobile">
      <p>Выберите подходящий ресторан из списка, например &laquo;Ташир Пицца&raquo;</p>
      <img
        className="help__img full-width mobile"
        src={RestCard}
        alt="Карточка ресторана Ташир Пицца"
        height={133.3}
      />
      <div className="help__comment">
        Если вы видите бэйджик &laquo;за баллы&raquo; это значит, что у&nbsp;вас есть возможность
        обменять баллы на&nbsp;часть стоимости блюда или&nbsp;получить любое блюдо бесплатно
        (в&nbsp;обмен на&nbsp;сумму баллов)
      </div>
      <p>Категории выбираются в рубрикаторе, где их можно листать справа-налево.</p>
      <img
        className="help__img full-width"
        src={RestHitsMobile}
        alt="Хиты продаж ресторана"
        height={600}
      />
    </div>
  </>
);

export default memo(HelpCreateOrderHitsComponent);
