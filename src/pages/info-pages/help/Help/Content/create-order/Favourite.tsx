// Modules
import React, { FC, memo } from 'react';
import { Title } from 'chibbis-ui-kit';
// Assets
import Favorite1 from '@/../assets/img/help/favourite-1.jpg';
import Favorite2 from '@/../assets/img/help/favourite-2.jpg';

const HelpCreateOrderFavouriteComponent: FC = () => (
  <>
    <Title variant="h3">Избранное и рейтинг</Title>
    <div className="help__favourites">
      <div className="help__favourites-column">
        <div className="help__favourites-img">
          <img className="help__img" src={Favorite1} alt="Избранное 1" />
        </div>
        <div className="help__favourites-text">
          Напротив каждого ресторана или блюда есть сердце, нажав на&nbsp;него вы добавите их
          в&nbsp;свои &laquo;избранные&raquo; Если сердце красного цвета, значит ресторан или блюдо
          в &laquo;избранном&raquo; и&nbsp;вам не&nbsp;придется искать их заново
        </div>
      </div>
      <div className="help__favourites-column">
        <div className="help__favourites-img">
          <img className="help__img" src={Favorite2} alt="Избранное 2" />
        </div>
        <div className="help__favourites-text">
          У каждого ресторана есть &laquo;рейтинг доверия&raquo; и&nbsp;формируется количеством
          отзывов тех, кто уже заказывал там еду
        </div>
      </div>
    </div>
  </>
);

export default memo(HelpCreateOrderFavouriteComponent);
