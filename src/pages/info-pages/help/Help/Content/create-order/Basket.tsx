// Modules
import React, { FC, memo } from 'react';
import { Title } from 'chibbis-ui-kit';
// Assets
import TakeDesktop from '@/../assets/img/help/take-desktop.jpg';
import TakeTablet from '@/../assets/img/help/take-tablet.jpg';
import TakeMobile from '@/../assets/img/help/take-mobile.jpg';
import CartDesktop from '@/../assets/img/help/take-basket-desktop.jpg';

const HelpCreateOrderBasketComponent: FC = () => (
  <>
    <Title variant="h3">Корзина</Title>
    <div className="desktop">
      <p>Добавляйте в корзину понравившиеся вам блюда, нажав кнопку &laquo;Взять&raquo;.</p>
      <p>
        Если вы еще не зарегистрированы, то сервис предложит вам регистрацию до&nbsp;этапа
        оформления заказа. Ищите &laquo;Как пройти регистрацию на&nbsp;Chibbis и&nbsp;зачем это
        нужно&raquo;.
      </p>
      <p>
        Если вы уже зарегистрировались, то сможете обменивать накопленные вами баллы
        на&nbsp;бесплатные блюда в&nbsp;дополнение к&nbsp;заказанным блюдам.
      </p>
      <img
        className="help__img full-width desktop"
        src={TakeDesktop}
        alt="Еда за баллы"
        height={512.26}
      />
      <p>
        Когда выбранные вами блюда у вас в корзине, вы можете убрать их из&nbsp;нее нажав
        на&nbsp;&laquo;-&raquo; или&nbsp;добавить количество, нажав на&nbsp;&laquo;+&raquo;.
      </p>
      <p>
        После добавления заказов в корзину вы увидите вашу скидку и&nbsp;стоимость доставки (тут
        доставка бесплатная).
        <br />
        Нажмите кнопку &laquo;Оформить заказ&raquo; и перейдите на&nbsp;страницу оформления.
      </p>
      <img
        className="help__img full-width desktop"
        src={CartDesktop}
        alt="Корзина"
        height={512.26}
      />
    </div>
    <div className="tablet mobile">
      <p>
        Нажав на кнопку ВЗЯТЬ вы добавите блюдо в корзину, также вы сможете выбрать количество блюд
        через &laquo;+ и -&raquo; в&nbsp;карточке блюда или&nbsp;уже в&nbsp;корзине.
      </p>
      <img
        className="help__img full-width tablet"
        src={TakeTablet}
        alt="Еда за баллы"
        height={609}
      />
      <img
        className="help__img full-width mobile"
        src={TakeMobile}
        alt="Еда за баллы"
        height={725}
      />
      <p>
        После нажатия &laquo;Оформить заказ&raquo; вам нужно выбрать тип получения заказа: доставка
        курьером или&nbsp;самовывоз. Нужно ввести контактные данные и&nbsp;комментарий, например
        об&nbsp;удобном времени доставки. Также вы можете ввести промокод, если он у&nbsp;вас есть.
      </p>
    </div>
  </>
);

export default memo(HelpCreateOrderBasketComponent);
