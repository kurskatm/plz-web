// Modules
import React, { FC, memo } from 'react';
import { Title } from 'chibbis-ui-kit';
// Assets
import FiltersDesktop from '@/../assets/img/help/filters-desktop.jpg';
import FiltersTablet from '@/../assets/img/help/filters-tablet.jpg';
import FiltersMobile from '@/../assets/img/help/filters-mobile.jpg';
import FiltersResultMobile from '@/../assets/img/help/filters-result-mobile.jpg';
import OrderFiltersMobile from '@/../assets/img/help/order-filters-mobile.jpg';

const HelpCreateOrderFiltersComponent: FC = () => (
  <>
    <Title>Как сделать заказ на сайте?</Title>
    <p className="desktop tablet">
      Выберите категорию ресторанов, которая вас интересует. Сделать это можно, нажав на&nbsp;одну
      из&nbsp;категорий вверху или&nbsp;внизу страницы или&nbsp;поставив галочку на&nbsp;нужной
      категории в&nbsp;фильтрах. Пользуясь фильтрами, можно не&nbsp;только выбрать категорию
      заведения, но&nbsp;и&nbsp;выбрать рестораны, работающие круглосуточно, с&nbsp;возможностью
      взять блюдо за&nbsp;баллы, с&nbsp;акциями или&nbsp;удобной для&nbsp;вас формой оплаты.
    </p>
    <p className="mobile">
      Выберите тип еды, которая вас интересует: суши, пицца, бургеры и&nbsp;т.д. Либо найдите блюдо
      по&nbsp;названию.
    </p>
    <img
      className="help__img full-width desktop"
      src={FiltersDesktop}
      alt="Как сделать заказ на сайте?"
      height={376}
    />
    <img
      className="help__img full-width tablet"
      src={FiltersTablet}
      alt="Как сделать заказ на сайте?"
      height={686}
    />
    <img
      className="help__img full-width mobile"
      src={FiltersMobile}
      alt="Как сделать заказ на сайте?"
      height={648}
    />
    <div className="mobile">
      <p>Ниже отобразятся лучшие рестораны вашего города, где вы можете заказать данные блюда.</p>
      <img
        className="help__img full-width mobile"
        src={FiltersResultMobile}
        alt="Результаты поиска"
        height={692}
      />
      <p>
        В фильтрах можно выбрать категорию заведения, круглосуточные рестораны или новые рестораыны,
        акциями, возможностью взять блюдо за баллы, и т.д.
      </p>
      <img className="help__img full-width" src={OrderFiltersMobile} alt="Фильтры" height={648} />
    </div>
  </>
);

export default memo(HelpCreateOrderFiltersComponent);
