// Modules
import React, { FC, memo } from 'react';
// Components
import Filters from './Filters';
import Hits from './Hits';
import Favourite from './Favourite';
import Basket from './Basket';
import Delivery from './Delivery';

const HelpCreateOrderComponent: FC = () => (
  <section id="create-order">
    <Filters />
    <Hits />
    <Favourite />
    <Basket />
    <Delivery />
  </section>
);

export default memo(HelpCreateOrderComponent);
