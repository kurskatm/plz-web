/* eslint-disable max-len */
// Modules
import React, { FC, memo } from 'react';
import { Text } from 'chibbis-ui-kit';
import { Element } from 'react-scroll';
// Components
import MyCity from './my-city';
import CreateOrder from './create-order';
import Registration from './registration';
import DontRecall from './dont-recall';
import CancelOrder from './cancel-order';
import RestReply from './rest-review';
import AboutPoints from './about-points';
import GetPoints from './get-points';
import UsePoints from './use-points';
import WhenPoints from './when-to-use-points';
import PayPoints from './payment-points';
import Birthday from './birthday';
import Discount from './discount';
import PromoCode from './promocode';
import RestPromotion from './rest-promotions';

const HelpContentComponent: FC = () => (
  <Text>
    <div className="help__content">
      <Element name="my-city">
        <MyCity />
      </Element>
      <Element name="create-order">
        <CreateOrder />
      </Element>
      <Element name="registration">
        <Registration />
      </Element>
      <Element name="dont-recall">
        <DontRecall />
      </Element>
      <Element name="cancel-order">
        <CancelOrder />
      </Element>
      <Element name="rest-reply">
        <RestReply />
      </Element>
      <Element name="about-points">
        <AboutPoints />
      </Element>
      <Element name="get-points">
        <GetPoints />
      </Element>
      <Element name="use-points">
        <UsePoints />
      </Element>
      <Element name="when-points">
        <WhenPoints />
      </Element>
      <Element name="pay-points">
        <PayPoints />
      </Element>
      <Element name="birthday">
        <Birthday />
      </Element>
      <Element name="discount">
        <Discount />
      </Element>
      <Element name="rest-promotion">
        <RestPromotion />
      </Element>
      <Element name="promo-code">
        <PromoCode />
      </Element>
    </div>
  </Text>
);

export default memo(HelpContentComponent);
