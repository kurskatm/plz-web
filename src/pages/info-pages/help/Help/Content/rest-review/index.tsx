/* eslint-disable max-len */
// Modules
import React, { FC, memo } from 'react';
import { Title } from 'chibbis-ui-kit';

const HelpRestReplyComponent: FC = () => (
  <section id="rest-reply">
    <Title>Как оставить отзыв о ресторане?</Title>
    <div className="desktop">
      <Title variant="h3">
        Достаточно войти на сайт Chibbis с того номера телефона, с которого вы делали заказ.
      </Title>
    </div>
    <p className="tablet mobile">
      Чтобы оставить отзыв о ресторане, достаточно войти на сайт Chibbis c того номера телефона, с
      которого вы делали заказ.
    </p>
    <p>
      Если с момента заказа уже прошло 12 часов, то появится окошко, в&nbsp;котором можно оставить
      отзыв о&nbsp;ресторане, в&nbsp;котором вы делали заказ.
    </p>
    <p>
      Если вам не хочется оставлять отзыв сразу, вы можете отложить на потом. Когда вам будет
      удобно, заходите на&nbsp;страничку ресторана на&nbsp;Chibbis, там в&nbsp;разделе
      &laquo;Отзывы&raquo; делитесь своими впечатлениями о&nbsp;данном ресторане.
    </p>
    <p>
      Для нас важно ваше мнение о еде и ресторанах, поэтому за&nbsp;каждый отзыв мы дарим 100 баллов
      на ваш счет в&nbsp;Chibbis. Баллы будут зачислены сразу после того, как&nbsp;вы оставите
      отзыв.
    </p>
  </section>
);

export default memo(HelpRestReplyComponent);
