// Modules
import React, { FC, memo } from 'react';
import { Link, Title } from 'chibbis-ui-kit';
// Utils
import { socialNetworkByName } from '@lib/social-networks/config';

const HelpPromoCodeComponent: FC = () => (
  <section id="promo-code">
    <Title>Где найти промокод и как его применить?</Title>
    <p>
      Мы часто разыгрываем промокоды в&nbsp;
      <Link to={socialNetworkByName.vk.link} target="_blank" border={false} outside color="blue">
        группе Вконтакте
      </Link>
      {/* &nbsp;или в&nbsp;
      <Link to={socialNetworkByName.instagram.link} target="_blank" border={false} outside>
        Instagram
      </Link> */}
      .
      <br />
      <br />
    </p>
    <ol>
      <li>Подписывайтесь, участвуйте в конкурсах и&nbsp;играх, выигрывайте промокоды!</li>
      <li>
        Подпишитесь на&nbsp;
        <Link to={socialNetworkByName.vk.link} target="_blank" border={false} outside color="blue">
          рассылку в&nbsp;ВК
        </Link>
        &nbsp;и получайте промокоды прямо в&nbsp;ЛС.
      </li>
    </ol>
    <p>
      Чтобы применить промокод, при оформлении заказа нажмите кнопку &laquo;У&nbsp;меня есть
      промокод!&raquo;, в&nbsp;поле &laquo;Промокод&raquo; укажите кодовое слово и&nbsp;баллы
      автоматически зачислятся после заказа.
    </p>
  </section>
);

export default memo(HelpPromoCodeComponent);
