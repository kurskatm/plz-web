// Modules
import React, { FC, memo } from 'react';
import { Title } from 'chibbis-ui-kit';

const HelpUsePointsComponent: FC = () => (
  <section id="use-points">
    <Title>Как пользоваться баллами?</Title>
    <p>
      Баллы CHIBBIS можно потратить на дополнительное блюдо к&nbsp;вашему заказу.
      <br />
      Для этого вам нужно выбрать ресторан с отметкой &laquo;За&nbsp;баллы&raquo; &ndash;
      пользуйтесь фильтрами, чтобы искать только те рестораны, в&nbsp;которых есть блюда
      за&nbsp;баллы.
    </p>
  </section>
);

export default memo(HelpUsePointsComponent);
