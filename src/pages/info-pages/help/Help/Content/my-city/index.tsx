// Modules
import React, { FC, memo } from 'react';
import { Link, Title } from 'chibbis-ui-kit';
// Utils
import { Consts } from '@utils';

const { ROUTES } = Consts;

const HelpMyCityComponent: FC = () => (
  <section id="my-city">
    <div className="help__title-first">
      <Title>Eсть ли Chibbis в моем городе?</Title>
    </div>
    <p>
      Чтобы узнать, есть ли CHIBBIS в вашем городе, нужно зайти на сайт&nbsp;
      <Link to={ROUTES.CITIES.PATH} border={false} color="blue">
        chibbis.ru
      </Link>
      <br />В левом верхнем углу нажмите на стрелку рядом с&nbsp;названием города, после откроется
      список всех доступных городов. Остается только выбрать ваш.
    </p>
    <div className="help__comment">
      Заказать еду на CHIBBIS можно более чем&nbsp;в&nbsp;160 городах России
    </div>
  </section>
);

export default memo(HelpMyCityComponent);
