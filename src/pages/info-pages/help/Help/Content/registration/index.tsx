// Modules
import React, { FC, memo } from 'react';
import { Title } from 'chibbis-ui-kit';
// Assets
import RegStartDesktop from '@/../assets/img/help/reg-start-desktop.jpg';
import RegStartMobile from '@/../assets/img/help/reg-start-mobile.jpg';
import RegCallDesktop from '@/../assets/img/help/reg-call-desktop.jpg';
import RegCallMobile from '@/../assets/img/help/reg-call-mobile.jpg';

const HelpRegistrationComponent: FC = () => (
  <section id="registration">
    <Title>Как зарегистрироваться на Chibbis и&nbsp;для чего это нужно?</Title>
    <p>
      Регистрация на Chibbis даст вам возможность копить баллы и&nbsp;пользоваться ими. Также
      в&nbsp;личном кабинете можно смотреть свои предыдущие заказы и&nbsp;повторить их в&nbsp;один
      клик.
    </p>
    <p>
      Чтобы зарегистрироваться на сайте, нужно в&nbsp;правом верхнем углу нажать
      на&nbsp;&laquo;Войти/Зарегистрироваться&raquo;. В&nbsp;появившемся окошке укажите свой номер
      телефона, вам на&nbsp;этот номер позвонит Chibbis. Принимать звонок не&nbsp;нужно, достаточно
      запомнить 4 последние цифры входящего номера.
    </p>
    <div className="help__registration desktop tablet">
      <img
        className="help__img desktop tablet"
        src={RegStartDesktop}
        alt="Как зарегистрироваться на Chibbis и для чего это нужно?"
      />
      <img
        className="help__img desktop tablet"
        src={RegCallDesktop}
        alt="Как зарегистрироваться на Chibbis и для чего это нужно?"
      />
    </div>
    <div className="help__registration mobile">
      <img
        className="help__img mobile"
        src={RegStartMobile}
        alt="Как зарегистрироваться на Chibbis и для чего это нужно?"
      />
      <img
        className="help__img mobile"
        src={RegCallMobile}
        alt="Как зарегистрироваться на Chibbis и для чего это нужно?"
      />
    </div>
  </section>
);

export default memo(HelpRegistrationComponent);
