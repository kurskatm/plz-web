// Modules
import React, { FC, memo } from 'react';
import { Link, Title } from 'chibbis-ui-kit';
// Utils
import { socialNetworkByName } from '@lib/social-networks/config';

const HelpBirthdayComponent: FC = () => (
  <section id="birthday">
    <Title>Как получить подарок на день рождения?</Title>
    <p>
      День рождения &ndash; отличный повод заказать что-нибудь вкусное и&nbsp;порадовать себя
      и&nbsp;гостей. Ко дню рождения мы дарим подарок &ndash; промокод на&nbsp;500 баллов. Чтобы его
      получить, подпишитесь на&nbsp;рассылку во&nbsp;
      <Link to={socialNetworkByName.vk.link} target="_blank" border={false} outside color="blue">
        Вконтакте
      </Link>
      , мы пришлем подарок с&nbsp;поздравлением в&nbsp;личные сообщения.
    </p>
  </section>
);

export default memo(HelpBirthdayComponent);
