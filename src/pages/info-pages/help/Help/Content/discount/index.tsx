// Modules
import React, { FC, memo } from 'react';
import { Link, Title } from 'chibbis-ui-kit';
// Utils
import { socialNetworkByName } from '@lib/social-networks/config';

const HelpDiscountComponent: FC = () => (
  <section id="discount">
    <Title>Как получить скидку?</Title>
    <p>
      Подписывайтесь на нас в соцсетях и участвуйте в&nbsp;розыгрышах, конкурсах и&nbsp;играх
      &ndash; мы часто разыгрываем призы: баллы, бесплатные заказы еды и&nbsp;другие подарки.
      С&nbsp;нами весело! А&nbsp;подписавшись на&nbsp;нашу секретную рассылку во&nbsp;
      <Link to={socialNetworkByName.vk.link} target="_blank" border={false} outside color="blue">
        Вконтакте
      </Link>
      &nbsp;вы получите промокоды в&nbsp;личные сообщения.
    </p>
  </section>
);

export default memo(HelpDiscountComponent);
