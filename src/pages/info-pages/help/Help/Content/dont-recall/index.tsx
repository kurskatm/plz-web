// Modules
import React, { FC, memo } from 'react';
import { Title } from 'chibbis-ui-kit';
// Assets
import SmsStartDesktop from '@/../assets/img/help/sms-start-desktop.jpg';
import SmsCodeDesktop from '@/../assets/img/help/sms-code-desktop.jpg';
import SmsStartMobile from '@/../assets/img/help/sms-start-mobile.jpg';
import SmsCodeMobile from '@/../assets/img/help/sms-code-mobile.jpg';

const HelpDontRecallComponent: FC = () => (
  <section id="dont-recall">
    <Title>Что делать, если мне не позвонили?</Title>
    <p>
      Если при регистрации или попытке войти в&nbsp;свой аккаунт CHIBBIS не&nbsp;смог вам
      дозвониться в&nbsp;течение минуты, то по&nbsp;истечении этого времени вы можете
      или&nbsp;повторить звонок, или&nbsp;запросить СМС-код подтверждения.
    </p>
    <div className="help__dont-recall desktop tablet">
      <img
        className="help__img desktop tablet"
        src={SmsStartDesktop}
        alt="Что делать, если мне не позвонили?"
      />
      <img
        className="help__img desktop tablet"
        src={SmsCodeDesktop}
        alt="Что делать, если мне не позвонили?"
      />
    </div>
    <div className="help__dont-recall mobile">
      <img
        className="help__img mobile"
        src={SmsStartMobile}
        alt="Что делать, если мне не позвонили?"
      />
      <img
        className="help__img mobile"
        src={SmsCodeMobile}
        alt="Что делать, если мне не позвонили?"
      />
    </div>
  </section>
);

export default memo(HelpDontRecallComponent);
