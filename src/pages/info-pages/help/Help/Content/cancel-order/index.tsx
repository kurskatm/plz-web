/* eslint-disable max-len */
// Modules
import React, { FC, memo } from 'react';
import { Title } from 'chibbis-ui-kit';
// Components
import MailLink from '../mail-link';

const HelpCancelOrderComponent: FC = () => (
  <section id="cancel-order">
    <Title>Как отменить заказ?</Title>
    <Title variant="h3">Два способа отменить заказ</Title>
    <p>
      Дождитесь звонка от менеджера ресторана и&nbsp;отмените заказ при&nbsp;разговоре с&nbsp;ним.
    </p>
    <p>
      Напишите нам на&nbsp;
      <MailLink />, указав в письме, в&nbsp;каком ресторане вы делали заказ и&nbsp;с&nbsp;какого
      номера телефона вы его совершили. Не забудьте пояснить, что&nbsp;вы хотите отменить заказ.
    </p>
  </section>
);

export default memo(HelpCancelOrderComponent);
