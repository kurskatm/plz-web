/* eslint-disable max-len */
// Modules
import React, { FC, memo } from 'react';
import { Link } from 'chibbis-ui-kit';
// Utils
import { Consts } from '@utils';

const { EMAILS } = Consts;

const HelpMailLinkComponent: FC = () => (
  <Link to={`mailto:${EMAILS.INFO}`} border={false} outside color="blue">
    {EMAILS.INFO}
  </Link>
);

export default memo(HelpMailLinkComponent);
