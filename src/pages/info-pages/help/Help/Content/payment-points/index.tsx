// Modules
import React, { FC, memo } from 'react';
import { Title } from 'chibbis-ui-kit';

const HelpPayPointsComponent: FC = () => (
  <section id="pay-points">
    <Title>Можно ли оплатить заказ баллами?</Title>
    <p>
      Нет, ни частично, ни полностью оплатить заказ баллами нельзя. Накопленные баллы можно обменять
      только на&nbsp;одно дополнительное блюдо к&nbsp;заказу. Для этого нужно набрать в&nbsp;корзину
      блюд на&nbsp;минимальную сумму заказа, а&nbsp;после этого добавить что-то одно из&nbsp;раздела
      &laquo;За&nbsp;баллы&raquo;.
    </p>
  </section>
);

export default memo(HelpPayPointsComponent);
