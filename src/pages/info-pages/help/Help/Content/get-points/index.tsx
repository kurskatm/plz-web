// Modules
import React, { FC, memo } from 'react';
import { Title } from 'chibbis-ui-kit';

const HelpGetPointsComponent: FC = () => (
  <section id="get-points">
    <Title>Как получить баллы?</Title>
    <Title variant="h3">Есть несколько способов</Title>
    <ol>
      <li>
        За регистрацию на сайте chibbis.ru или установку мобильного приложения вы получаете сразу
        600 баллов на&nbsp;счет.
      </li>
      <li>10% от суммы вашего заказа будет возвращаться на&nbsp;ваш бонусный счет баллами.</li>
      {/* <li>
        Присоединяйтесь к нам, вступайте в&nbsp;
        <Link to={socialNetworkByName.vk.link} target="_blank" border={false} outside>
          группу Chibbis Вконтакте
        </Link>
        &nbsp;и получите за&nbsp;это 150 дополнительных баллов.
      </li> */}
      {/* <li>
        Подпишитесь на&nbsp;
        <Link to={socialNetworkByName.instagram.link} target="_blank" border={false} outside>
          Chibbis в&nbsp;Instagram
        </Link>
        &nbsp;и получите за&nbsp;это еще 150 баллов.
      </li> */}
      <li>
        В нашей группе мы часто проводим конкурсы и&nbsp;розыгрыши, в&nbsp;которых вы можете
        выиграть &laquo;Баллы CHIBBIS&raquo;. Присоединяйтесь, развлекайтесь и&nbsp;пополняйте свой
        счет выигранными баллами.
      </li>
      <li>
        Поделитесь ссылкой на chibbis.ru в социальных сетях, и&nbsp;мы подарим 300 баллов. Для этого
        войдите в&nbsp;личный кабинет, раздел &laquo;Баллы&raquo; и&nbsp;нажмите
        &laquo;Поделиться&raquo;.
      </li>
      <li>
        Оставляйте отзывы. После каждого заказа вы сможете оставить отзыв о&nbsp;ресторане, который
        для&nbsp;вас готовил и&nbsp;получить в&nbsp;подарок 100 баллов.
      </li>
      <li>
        Приглашайте своих друзей. Вместе веселее и&nbsp;выгоднее: 10% от&nbsp;суммы заказа вашего
        друга вернуться вам в&nbsp;виде баллов, как и&nbsp;вашему же другу. А&nbsp;если друг вашего
        друга сделает заказ, то&nbsp;ваш бонусный счет пополнится на&nbsp;5% его заказа.
      </li>
    </ol>
  </section>
);

export default memo(HelpGetPointsComponent);
