// Modules
import React, { FC, memo } from 'react';
import { Title } from 'chibbis-ui-kit';
// Assets
import AboutPoints from '@/../assets/img/help/about-points.jpg';

const HelpAboutPointsComponent: FC = () => (
  <section id="about-points">
    <Title>Что такое баллы и для чего нужны?</Title>
    <p>
      Баллы &ndash; это бонусы, которые дарит CHIBBIS за&nbsp;вашу активность в&nbsp;сервисе.
      1&nbsp;балл = 1&nbsp;рубль. К каждому заказу вы можете добавить одно блюдо в&nbsp;подарок,
      оплатив его баллами.
    </p>
    <p>
      Если вы не зарегистрированы, то после регистрации 1&nbsp;блюдо &laquo;За баллы&raquo;
      для&nbsp;вас бесплатно.
    </p>
    <p>
      Если у вас накопились баллы, нажмите &laquo;Взять&raquo; и&nbsp;добавьте блюдо в&nbsp;корзину.
      В&nbsp;рамках одного заказа можно добавить только одно блюдо &laquo;За баллы&raquo;.
    </p>
    <img
      className="help__img full-width desktop"
      src={AboutPoints}
      alt="Что такое баллы и для чего нужны?"
    />
  </section>
);

export default memo(HelpAboutPointsComponent);
