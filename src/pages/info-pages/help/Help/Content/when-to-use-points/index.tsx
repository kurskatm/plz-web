// Modules
import React, { FC, memo } from 'react';
import { Title } from 'chibbis-ui-kit';

const HelpWhenPointsComponent: FC = () => (
  <section id="when-points">
    <Title>Когда можно пользоваться баллами?</Title>
    <p>
      Баллы можно потратить сразу же при первом заказе. При регистрации мы дарим вам 600 баллов, их
      можно будет потратить в&nbsp;любое время.
    </p>
  </section>
);

export default memo(HelpWhenPointsComponent);
