// Modules
import React, { FC, memo } from 'react';
import { Title } from 'chibbis-ui-kit';

const HelpRestPromotionComponent: FC = () => (
  <section id="rest-promotion">
    <Title>Как узнать об акциях в ресторанах?</Title>
    <p>
      Рестораны часто проводят акции, на Chibbis их легко найти. Рестораны с&nbsp;акциями помечены
      бэйджем &laquo;Акция&raquo;. Можно воспользоваться фильтром и&nbsp;выбрать только те
      рестораны, в&nbsp;которых проводятся акции.
    </p>
  </section>
);

export default memo(HelpRestPromotionComponent);
