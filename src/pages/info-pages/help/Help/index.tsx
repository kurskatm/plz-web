// Modules
import React, { FC, memo } from 'react';
import { GridItem, Layout } from 'chibbis-ui-kit';
// Components
import Navigation from './Navigation';
import Content from './Content';

const HelpComponent: FC = () => (
  <Layout className="help" vertical="top">
    <GridItem grow={1} className="help__nav-wrap">
      <Navigation />
    </GridItem>
    <GridItem grow={999} className="help__content-wrap">
      <Content />
    </GridItem>
  </Layout>
);

export default memo(HelpComponent);
