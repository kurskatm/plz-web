// Modules
import React, { FC, memo } from 'react';
import { Link, Title } from 'chibbis-ui-kit';
// Utils
import { socialNetworkByName } from '@lib/social-networks/config';
// Components
import MailLink from '../Content/mail-link';

const HelpFooterComponent: FC = () => (
  <div className="help__footer">
    <Title>Не нашли ответ на свой вопрос?</Title>
    <p>
      Задайте его в&nbsp;
      <Link to={socialNetworkByName.vk.link} target="_blank" border={false} outside color="blue">
        группе Вконтакте
      </Link>
      <br />
      или напишите нам&nbsp;
      <MailLink />
    </p>
  </div>
);

export default memo(HelpFooterComponent);
