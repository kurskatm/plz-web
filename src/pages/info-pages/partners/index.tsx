// Modules
import React, { memo, FC, useEffect } from 'react';
import loadable from '@loadable/component';
import { GoogleReCaptchaProvider } from 'react-google-recaptcha-v3';
import GoToOldSite from '@components/MoveToOldDesignBanner/Banner';
// Utils
import { Consts } from '@utils';

const { CITIES } = Consts.ROUTES;

const Header = loadable(() => import('@components/Header'));
const Footer = loadable(() => import('@components/Footer'));
const Partners = loadable(() => import('./Partners'));

const PartnersPageComponent: FC = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <>
      <GoogleReCaptchaProvider reCaptchaKey={process.env.RECAPTCHA_KEY}>
        <div className="partners-page grid-wrapper">
          <header className="grid-wrapper__header">
            <Header
              goBackText="На главную"
              goBackLink={CITIES.PATH}
              noAddress
              noMenu
              title="Партнерам"
            />
          </header>
          <main className="grid-wrapper__main">
            <div className="partners-page__content-wrap">
              <Partners />
            </div>
          </main>
          <footer className="grid-wrapper__footer">
            <Footer />
          </footer>
        </div>
      </GoogleReCaptchaProvider>
      <GoToOldSite />
    </>
  );
};

export default memo(PartnersPageComponent);
