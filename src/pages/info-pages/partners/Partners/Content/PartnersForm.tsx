// Modules
import React, { FC, useState, useCallback, memo } from 'react';
import { InputField, InputPhone, RadioField, CheckboxField, Button } from 'chibbis-ui-kit';
import { Form } from 'react-final-form';
import { GoogleRecaptchaComponent } from '@components/ReCaptcha';
// Utils
import { getPreparedData } from '../../utils';
// Config
import { getValidation } from './config/getValidation';
// Types
import { TPartnersFormProps } from '../types';

// @ts-ignore
const LicenseAgreement = ({ city }) => () => (
  <span>
    {'Согласен с '}
    <a href={`/${city}/reglament`}>политикой конфиденциальности</a>
  </span>
);

const PartnersFormComponent: FC<TPartnersFormProps> = ({ submitAction, city }) => {
  const [token, setToken] = useState(null);
  const submitHandler = useCallback(
    (formData: Record<string, string>) => {
      submitAction({ token, ...getPreparedData(formData) });
    },
    [token, submitAction],
  );

  const getFormValidation = useCallback(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (values: Record<string, any>) => getValidation(values),
    [],
  );

  return (
    <Form
      onSubmit={submitHandler}
      validate={getFormValidation}
      render={({ handleSubmit, pristine, valid }) => (
        <form id="partnersForm" onSubmit={handleSubmit} className="partners-form">
          <div className="partners-form__row" onSubmit={handleSubmit}>
            <div className="partners-form__col">
              <div className="partners-form__field">
                <div className="partners-form__field-title">Город</div>
                <InputField theme="grey" name="cityName" required />
              </div>
              <div className="partners-form__field">
                <div className="partners-form__field-title">Фирменное название</div>
                <InputField theme="grey" name="restaurantName" required />
              </div>
              <div className="partners-form__field">
                <div className="partners-form__field-title">Контактное лицо</div>
                <InputField theme="grey" name="name" placeholder="ФИО" required />
              </div>
              <div className="partners-form__field">
                <div className="partners-form__field-title">Электронная почта</div>
                <InputField theme="grey" name="email" required />
              </div>
              <div className="partners-form__field">
                <div className="partners-form__field-title">Телефон</div>
                <InputPhone theme="grey" name="phoneNumber" required />
              </div>
            </div>
            <div className="partners-form__col">
              <div className="partners-form__fiel partners-form__field-sub-title">
                Сколько у вас сейчас своих заказов в месяц?
              </div>
              <div className="partners-form__field">
                <RadioField name="ordersCount" value="0-300" view="0-300" />
              </div>
              <div className="partners-form__field">
                <RadioField name="ordersCount" value="300-500" view="300-500" />
              </div>
              <div className="partners-form__field">
                <RadioField name="ordersCount" value="500-1000" view="500-1000" />
              </div>
              <div className="partners-form__field">
                <RadioField name="ordersCount" value=">1000" view=">1000" />
              </div>
              <div className="partners-form__field">
                <div className="partners-form__hint-wrap">
                  Мы собираем эти данные для последующей оценки собственной эффективности
                </div>
              </div>
              <div className="partners-form__field">
                <CheckboxField
                  name="agreement"
                  required
                  view={LicenseAgreement({ city: city?.urlName })}
                />
              </div>
              <div className="partners-form__field">
                <GoogleRecaptchaComponent tokenHandler={setToken} token={token} />
              </div>
            </div>
          </div>
          <div className="partners-form__button">
            <Button disabled={pristine || !valid || !token}>отправить</Button>
          </div>
        </form>
      )}
    />
  );
};

export default memo(PartnersFormComponent);
