// Modules
import React, { FC, memo } from 'react';
import { Title } from 'chibbis-ui-kit';

// Types
import { TPartnersFeaturesProps } from '../types';

const PartnersFeaturesComponent: FC<TPartnersFeaturesProps> = ({ title, features }) => (
  <div className="partners-features">
    <Title variant="h3">{title}</Title>
    <ul className="partners-features__list">
      {features.map((item, index) => (
        <li key={title.charCodeAt(index) + Math.random()} className="partners-features__item">
          {item}
        </li>
      ))}
    </ul>
  </div>
);

export default memo(PartnersFeaturesComponent);
