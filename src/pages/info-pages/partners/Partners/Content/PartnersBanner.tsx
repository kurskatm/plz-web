// Modules
import React, { FC, memo } from 'react';

const PartnersBannerComponent: FC = () => (
  <div className="partners-banner">
    <div className="partners-banner__title">
      Получи + 20% к заказам
      <br />
      уже в первый месяц сотрудничества
    </div>
    <div className="partners-banner__subtitle">Всего за 15% от суммы выполненных заказов</div>
  </div>
);
export default memo(PartnersBannerComponent);
