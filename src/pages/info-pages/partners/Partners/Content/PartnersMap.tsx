// Modules
import React, { FC, useCallback, memo, ReactNode } from 'react';
import { Map, ZoomControl, Placemark } from 'react-yandex-maps';

import mark from '@/../assets/svg/map/pin-placemark.svg';

import { numberFormat } from '@utils/number-format';

// Types
import { TCitiesLocation } from '@type/cities';
import { TPartnersMapProps } from '../types';

const PartnersMapComponent: FC<TPartnersMapProps> = ({ counters = {}, locations = [] }) => {
  const { ordersCount = 0, usersCount = 0, restaurantsCount = 0 } = counters || {};
  const getMarkers = useCallback(
    () =>
      locations.map(
        (position: TCitiesLocation): ReactNode => (
          // @ts-ignore
          <Placemark
            key={`${position?.lat}_${position?.lng}`}
            geometry={[position?.lat, position?.lng]}
            options={{
              iconLayout: 'default#image',
              iconImageHref: mark,
              iconImageSize: [48, 48],
              iconImageOffset: [-23, -47],
            }}
          />
        ),
      ),
    [locations],
  );

  return (
    <div className="partners-map">
      <div className="partners-map__wrap">
        <div className="partners-map__map">
          {/* @ts-ignore */}
          <Map
            className="yandex-map"
            state={{
              center: [55.20098, 67.17814],
              zoom: 3,
              controls: [],
              behaviors: ['drag'],
            }}
            options={{
              suppressMapOpenBlock: true,
            }}>
            {/* @ts-ignore */}
            <ZoomControl options={{ size: 'small' }} />
            {/* @ts-ignore */}
            {getMarkers()}
          </Map>
        </div>
        <div className="partners-map__counters">
          <div className="partners-map__counters-item">
            <div className="partners-map__counters-title">Городов:</div>
            160+
          </div>
          <div className="partners-map__counters-item">
            <div className="partners-map__counters-title">Заказов:</div>
            {numberFormat(ordersCount, ' ')}
          </div>
          <div className="partners-map__counters-item">
            <div className="partners-map__counters-title">Клиентов:</div>
            {numberFormat(usersCount, ' ')}
          </div>
          <div className="partners-map__counters-item">
            <div className="partners-map__counters-title">Ресторанов:</div>
            {numberFormat(restaurantsCount, ' ')}
          </div>
        </div>
      </div>
    </div>
  );
};

export default memo(PartnersMapComponent);
