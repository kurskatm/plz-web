import subwayImg from '@/../assets/img/info-pages/subway.png';
import sushiMarketImg from '@/../assets/img/info-pages/sushi-market.png';
import shtolleImg from '@/../assets/img/info-pages/stolle.png';
import shashlikoffImg from '@/../assets/img/info-pages/shahlykoff.png';
import yakitoriaImg from '@/../assets/img/info-pages/yakitoriya.png';
import radugaImg from '@/../assets/img/info-pages/rainbow.png';
import triSousaImg from '@/../assets/img/info-pages/tri-sousa.png';
import papaJohnesImg from '@/../assets/img/info-pages/papa-johnes.png';
import sushiMasterImg from '@/../assets/img/info-pages/sushi-master.png';
import kartoshkaImg from '@/../assets/img/info-pages/kk.png';
import suhisetImg from '@/../assets/img/info-pages/sushi-set.png';
import sushiWokImg from '@/../assets/img/info-pages/sushi-wok.png';
import tashirImg from '@/../assets/img/info-pages/tashir.png';
import blackStarImg from '@/../assets/img/info-pages/blackstarburger.png';

export {
  blackStarImg,
  subwayImg,
  tashirImg,
  sushiMarketImg,
  shashlikoffImg,
  yakitoriaImg,
  radugaImg,
  triSousaImg,
  papaJohnesImg,
  sushiMasterImg,
  kartoshkaImg,
  suhisetImg,
  sushiWokImg,
  shtolleImg,
};
