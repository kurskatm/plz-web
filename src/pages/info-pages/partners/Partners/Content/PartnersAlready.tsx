// Modules
import React, { FC, memo } from 'react';
import classNames from 'classnames';

// Images
import * as Images from './images';

// Types
import { TPartnersAlreadyProps, TPartnersAlreadyProp } from '../types';

const PartnersAlreadyImageComponent: FC<TPartnersAlreadyProp> = ({
  src,
  title,
  alt,
}: TPartnersAlreadyProp) => {
  const getStyles = () =>
    classNames('partners-already__image', {
      [`partners-already__image_img-${title}`]: title,
    });
  // @ts-ignore
  const imgSrc = Images[src];

  return (
    <li key={`${src}_${title}`} className="partners-already__item">
      <img className={getStyles()} src={imgSrc} title={title} alt={alt || title} />
    </li>
  );
};

const PartnersAlreadyComponent: FC<TPartnersAlreadyProps> = ({
  partners,
}: TPartnersAlreadyProps) => (
  <ul className="partners-already__list">{partners.map(PartnersAlreadyImageComponent)}</ul>
);

export default memo(PartnersAlreadyComponent);
