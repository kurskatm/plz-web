// Modules
import React, { FC, useCallback, memo } from 'react';
import classNames from 'classnames';
import { Title } from 'chibbis-ui-kit';

// Types
import { TPartnersSchemaProps } from '../types';

const PartnersSchemaComponent: FC<TPartnersSchemaProps> = ({ title, description, imageIndex }) => {
  const getStyles = useCallback(
    () =>
      classNames('partners-schema__image', {
        [`partners-schema__image_img-${imageIndex}`]: imageIndex,
      }),
    [imageIndex],
  );
  return (
    <div className="partners-schema">
      <div className={getStyles()} />
      <div className="partners-schema__content">
        <Title variant="h3">{title}</Title>
        <div className="partners-schema__description">{description}</div>
      </div>
    </div>
  );
};

export default memo(PartnersSchemaComponent);
