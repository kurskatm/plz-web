// Modules
import React, { FC } from 'react';
import { BoxIcon as CustomIcon } from 'chibbis-ui-kit';

const PinIcon: FC = () => <CustomIcon className="partners-form__icon-pin" width={13} height={16} />;

const OrgIcon: FC = () => <CustomIcon className="partners-form__icon-org" width={13} height={16} />;

const PersonIcon: FC = () => (
  <CustomIcon className="partners-form__icon-person" width={13} height={16} />
);

const EmailIcon: FC = () => (
  <CustomIcon className="partners-form__icon-email" width={13} height={16} />
);

export { PinIcon, OrgIcon, PersonIcon, EmailIcon };
