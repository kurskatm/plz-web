// Modules
import isNil from 'lodash/isNil';
// Types
import { TGetValidation } from './types';

export const getValidation: TGetValidation = (values) => {
  const nameMask = new RegExp(/^[А-ЯЁа-яёa-zA-Z\s]*$/);
  const phoneMask = new RegExp(/^\+7\s\(9\d\d\)\s\d\d\d-\d\d-\d\d$/);
  const emailMask = new RegExp(
    /^((([0-9A-Za-z]{1}[-0-9A-z.]{0,30}[0-9A-Za-z]?)|([0-9А-Яа-я]{1}[-0-9А-я.]{0,30}[0-9А-Яа-я]?))@([-A-Za-z]{1,}\.){1,}[-A-Za-z]{2,})$/,
  );

  if (!isNil(values.name) && !String(values.name).match(nameMask)) {
    return {
      name: 'Пожалуйста, заполните это поле',
    };
  }

  if (!isNil(values.phoneNumber) && !String(values.phoneNumber).match(phoneMask)) {
    return {
      phoneNumber: 'Пожалуйста, заполните это поле',
    };
  }

  if (!isNil(values.email) && !String(values.email).match(emailMask)) {
    return {
      email: 'Пожалуйста, заполните это поле',
    };
  }

  if (isNil(values.ordersCount)) {
    return {
      ordersCount: 'Пожалуйста, заполните это поле',
    };
  }

  return undefined;
};
