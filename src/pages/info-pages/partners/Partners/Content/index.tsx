// Modules
import React, { FC, memo } from 'react';
import { Title } from 'chibbis-ui-kit';
// Components
import PartnersFeatures from './PartnersFeatures';
import PartnersSchema from './PartnersSchema';
import PartnersAlready from './PartnersAlready';
import PartnersMap from './PartnersMap';
import PartnersBanner from './PartnersBanner';
// Consts
import { FEATURES, SCHEMAS, PARTNERS } from '../consts';

// Types
import { TPartnersContentProps } from '../types';

const PartnersContentComponent: FC<TPartnersContentProps> = ({
  counters,
  locations,
  countersStatusIsIdle,
}) => (
  <>
    <PartnersBanner />
    <div className="partners__content">
      <section className="partners__section">
        <div className="partners__title">
          <Title>Выгоды</Title>
        </div>
        <div className="partners__features-wrap">
          {FEATURES.map(({ title, features }, index) => (
            <div className="partners__features">
              <PartnersFeatures
                key={title.charCodeAt(index) + Math.random()}
                title={title}
                features={features}
              />
            </div>
          ))}
        </div>
      </section>
      <section className="partners__section">
        <div className="partners__title">
          <Title>Схема работы</Title>
        </div>
        <div className="partners__schemas-wrap">
          {SCHEMAS.map(({ title, description }, index) => (
            <div className="partners__schemas">
              <PartnersSchema
                key={title.charCodeAt(index)}
                title={title}
                description={description}
                imageIndex={index}
              />
            </div>
          ))}
        </div>
      </section>
      <section className="partners__section">
        <div className="partners__title">
          <Title>С нами уже работают</Title>
        </div>
        <PartnersAlready partners={PARTNERS} />
      </section>
      <section className="partners__section">
        {!countersStatusIsIdle && (
          <>
            <div className="partners__title-map">
              <Title>Работаем в 160+ городах</Title>
            </div>
            <PartnersMap counters={counters} locations={locations} />
          </>
        )}
      </section>
    </div>
  </>
);

export default memo(PartnersContentComponent);
