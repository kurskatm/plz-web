// Modules
import React, { FC, useEffect, memo, useState } from 'react';
import { ModalWindow, Button } from 'chibbis-ui-kit';

// Components
import Content from './Content';
import PartnersForm from './Content/PartnersForm';

// Enhance
import { enhance } from './enhance';

// Types
import { TPartnersProps } from './types';

const PartnersFormHeader: FC = () => (
  <div className="partners-modal__header">
    <div>Регистрация</div>
    <div>ресторана</div>
  </div>
);

const PartnersSuccessContent: FC = () => (
  <div className="partners-modal-response__content">
    <div className="partners-modal-response__icon" />
    <h2>Заяка отправлена!</h2>
    <p className="partners-modal-response__text">Менеджер скоро свяжется с вами</p>
  </div>
);

const PartnersErrorContent: FC = () => (
  <div className="partners-modal-response__content">
    <div className="partners-modal-response__icon-error" />
    <h2>Что-то пошло не так</h2>
    <p className="partners-modal-response__text">
      Мы уже знаем об ошибке и работаем над её устранением
    </p>
  </div>
);

const PartnersComponent: FC<TPartnersProps> = ({
  city,
  counters,
  countersStatusIsIdle,
  locations,
  members,
  partnersBecomePartner,
  partnersCounters,
  partnersBecomePartnerIdle,
  countersStatusIsError,
  pushNotification,
}) => {
  const [modalVis, setModalVis] = useState(false);

  useEffect(
    () => {
      if (countersStatusIsError) {
        pushNotification({
          type: 'error',
          content: 'Не удалось',
        });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [countersStatusIsError],
  );

  useEffect(() => {
    if (countersStatusIsIdle) {
      partnersCounters();
    }
  }, [countersStatusIsIdle, partnersCounters]);

  const closeModal = () => {
    setModalVis(false);
    partnersBecomePartnerIdle();
  };

  return (
    <div className="partners">
      <Content
        counters={counters}
        locations={locations}
        countersStatusIsIdle={countersStatusIsIdle}
      />
      <div className="partners-button">
        <div className="partners-button__wrap">
          <Button onClick={() => setModalVis(true)}>оставить заявку</Button>
        </div>
      </div>
      <ModalWindow
        headerContent={
          !['success', 'error'].includes(members.ui.status) ? <PartnersFormHeader /> : null
        }
        classNameWrapper={
          !['success', 'error'].includes(members.ui.status) ? 'partners-modal' : null
        }
        visible={modalVis}
        setVisible={closeModal}>
        {!['success', 'error'].includes(members.ui.status) && (
          <PartnersForm city={city} submitAction={partnersBecomePartner} />
        )}
        {members.ui.status === 'success' && <PartnersSuccessContent />}
        {members.ui.status === 'error' && <PartnersErrorContent />}
      </ModalWindow>
    </div>
  );
};

export default enhance(memo(PartnersComponent));
