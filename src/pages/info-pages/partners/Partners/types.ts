import { ReactNode } from 'react';
import { TCitiesItem, TCitiesLocation } from '@type/cities';
import { TPartnersCountersGet } from '@actions/partners/action-types';
import { TPushNotification } from '@actions/notifications/action-types';

export interface TPartnersFeaturesProps {
  title: string;
  features: ReactNode[];
}

export interface TPartnersSchemaProps {
  title: string;
  description: string;
  imageIndex?: number;
}

export interface TPartnersCounter {
  ordersCount: number;
  usersCount: number;
  restaurantsCount: number;
}
export interface TPartnersAlreadyProp {
  title?: string;
  src: string;
  alt?: string;
}

export interface TPartnersAlreadyProps {
  partners: TPartnersAlreadyProp[];
}

export interface TPartnersProps {
  city: TCitiesItem;
  counters: TPartnersCounter;
  locations: TCitiesLocation[];
  members: {
    data: number;
    ui: { [key: string]: string };
  };
  partnersBecomePartner?: () => void;
  partnersBecomePartnerIdle?: () => void;
  countersStatusIsIdle: boolean;
  countersStatusIsError: boolean;
  partnersCounters: TPartnersCountersGet;
  pushNotification: TPushNotification;
}

export interface TErrorMessageClickHadlerProps {
  errorMsgClickHandler: () => void;
}

export interface TPartnersFormProps {
  city: TCitiesItem;
  submitAction: (data: Record<string, string>) => void;
}

export interface TPartnersContentProps {
  counters: TPartnersCounter;
  locations: TCitiesLocation[];
  countersStatusIsIdle: boolean;
}

export interface TPartnersMapProps {
  counters: TPartnersCounter;
  locations: TCitiesLocation[];
}
