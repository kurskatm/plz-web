// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import {
  partnersBecomePartner,
  partnersCounters,
  partnersBecomePartnerIdle,
} from '@actions/partners';
import { pushNotification } from '@actions/notifications';
// Selectors
import { partnersStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/partners';

const mapDispatchToProps = {
  partnersBecomePartner,
  partnersCounters,
  partnersBecomePartnerIdle,
  pushNotification,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);
export const enhance = compose(withConnect);
