import React from 'react';

export const FEATURES = [
  {
    title: 'Гарантия фиксированной стоимости привлечения заказа',
    features: [
      <p>
        <span>Не&nbsp;нужно набирать, содержать и&nbsp;контролировать</span> отдел маркетинга.
      </p>,
      <p>
        <span>Не&nbsp;нужно рисковать</span> своими деньгами на&nbsp;рекламу/сайт/мобильное
        приложение/соц.сети.
      </p>,
      <p>
        <span>Не&nbsp;нужно придумывать</span> акции, которые чаще всего убыточные.
      </p>,
    ],
  },
  {
    title: 'Быстрый старт или масштабирование',
    features: [
      <p>
        У&nbsp;нас уже
        <span> есть клиенты</span> в&nbsp;вашем городе и&nbsp;они любят пробовать новые рестораны.
      </p>,
      <p>
        У&nbsp;нас уже
        <span> есть готовый сайт, группы</span> в&nbsp;соц сетях и&nbsp;
        <span>мобильные приложения.</span>
      </p>,
      <p>
        У&nbsp;нас уже
        <span> есть отдел контента,</span> который быстро перенесет ваше меню к&nbsp;нам
        на&nbsp;сервис.
      </p>,
    ],
  },
  {
    title: 'Без риска',
    features: [
      <p>
        Ни&nbsp;один маркетолог или агентство не&nbsp;будет работать чисто за&nbsp; процент, они
        обязательно попросят у&nbsp;вас еще и&nbsp; фиксированную оплату, чтобы снизить свои риски.
      </p>,
      <p>
        <span>Мы&nbsp;уверены в&nbsp;результатах,</span> поэтому готовы работать за&nbsp;процент.
      </p>,
      <p>
        <span>Мы&nbsp;заинтересованы</span> в&nbsp;том, чтобы обеспечить вам как можно больше
        заказов.
      </p>,
    ],
  },
];
export const SCHEMAS = [
  {
    title: 'Вы оставляете заявку',
    description: 'на нашем сайте',
  },
  {
    title: 'Мы связываемся с вами',
    description: 'и сами создаем страницу вашего ресторана на сервисе + подписываем договор',
  },
  {
    title: 'Вы регулярно получаете от нас поток заказов',
    description: 'и оплачиваете только выполненные заказы',
  },
];
export const PARTNERS = [
  {
    src: 'subwayImg',
    title: 'subway',
  },
  {
    src: 'tashirImg',
    title: 'tashir',
  },
  {
    src: 'sushiMarketImg',
    title: 'sushi-market',
  },
  {
    src: 'shashlikoffImg',
    title: 'shashlikoff',
  },
  {
    src: 'yakitoriaImg',
    title: 'yakitoria',
  },
  {
    src: 'radugaImg',
    title: 'raduga',
  },
  {
    src: 'triSousaImg',
    title: 'tri-sousa',
  },
  {
    src: 'papaJohnesImg',
    title: 'papa-johnes',
  },
  {
    src: 'sushiMasterImg',
    title: 'sushi-master',
  },
  {
    src: 'kartoshkaImg',
    title: 'kartoshka',
  },
  {
    src: 'suhisetImg',
    title: 'suhiset',
  },
  {
    src: 'sushiWokImg',
    title: 'sushi-wok',
  },
  {
    src: 'blackStarImg',
    title: 'black-star',
  },
  {
    src: 'shtolleImg',
    title: 'shtolle',
  },
];
