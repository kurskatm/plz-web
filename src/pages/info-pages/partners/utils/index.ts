import { omit } from 'lodash';

export const getPreparedData = (data: Record<string, string>): Record<string, string> => ({
  ...omit(data, ['token', 'agreement']),
});
