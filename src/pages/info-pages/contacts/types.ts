import { TPushNotification } from '@actions/notifications/action-types';

export interface TSupportFormValues {
  name: string;
  email: string;
  phoneNumber: string;
  selectedTopicId: string;
  message: string;
}

export type TContactProps = {
  setStatus: (data: string) => void;
  topics: TSupportSelectItem[];
  topicsStatusIsIdle: boolean;
  topicsStatusIsError: boolean;
  supportTopicsFetch: () => void;
  supportMessageSend: (data: Record<string, string>) => void;
  pushNotification: TPushNotification;
  messageSendStatus?: string;
  messageSendIsError: boolean;
  status?: string;
};

export type TSupportSelectItem = {
  id: string;
  name: string;
};
