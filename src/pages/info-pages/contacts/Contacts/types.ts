import { TCitiesItem } from '@type/cities';
import { TContacts } from '@type/contacts';

export interface TContactsProps {
  city: TCitiesItem;
  contacts: TContacts;
}

export interface TContactsColumnsProps {
  cleanPhone?: string;
  phone?: string;
  email?: string;
  address?: string;
}

export interface TContactsMapAddressProps {
  address?: string;
  lat?: number;
  lng?: number;
}

export interface TContactsAddressProps {
  address?: string;
  className?: string;
}
