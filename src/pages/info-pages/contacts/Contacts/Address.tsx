// Modules
import React, { FC, memo, useCallback } from 'react';
import classNames from 'classnames';
// Types
import { TContactsAddressProps } from './types';

const ContactsAddressComponent: FC<TContactsAddressProps> = ({ address, className }) => {
  const getStyles = useCallback(() => classNames('contacts__address', className), [className]);

  return (
    <div className={getStyles()}>
      <div className="contacts__title">Адрес:</div>
      <div>{address}</div>
    </div>
  );
};

export default memo(ContactsAddressComponent);
