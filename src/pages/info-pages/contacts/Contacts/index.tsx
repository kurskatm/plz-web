// Modules
import React, { FC, memo, useCallback } from 'react';
// Modules Components
import { Text } from 'chibbis-ui-kit';
// Lib
import { GetPhone } from '@lib/masks/get-phone';
import { GetCleanPhone } from '@lib/masks/get-clean-phone';
// Components
import ContactsColumns from './Columns';
import ContactsMapAddress from './MapAddress';
// Enhance
import { enhance } from './enhance';
// Types
import { TContactsProps } from './types';

const GetCleanPhoneManager = new GetCleanPhone();
const GetPhoneManager = new GetPhone();

const ContactsComponent: FC<TContactsProps> = ({ city, contacts }) => {
  const { phoneNumber, email, address, lat, lng } = contacts;

  const getCity = useCallback(() => (city ? city.name : 'undefined'), [city]);

  const getCleanPhone = useCallback(() => GetCleanPhoneManager.init(phoneNumber), [phoneNumber]);

  const getPhone = useCallback(() => GetPhoneManager.init(getCleanPhone()), [getCleanPhone]);

  return (
    <div className="info-page__contacts">
      <Text variant="variant1" className="contacts-page__intro">
        Для связи с нашим контактным центром в городе {getCity()} воспользуйтесь одним из удобных
        для вас способом связи:
      </Text>
      <Text className="contacts">
        <ContactsColumns
          address={address}
          cleanPhone={getCleanPhone()}
          phone={getPhone()}
          email={email}
        />
        <ContactsMapAddress lat={lat} lng={lng} />
      </Text>
    </div>
  );
};

const ContactsComponentMemo = memo(ContactsComponent);
export default enhance(ContactsComponentMemo);
