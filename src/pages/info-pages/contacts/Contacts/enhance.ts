// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Selectors
import { contactsStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/contacts';

const withConnect = connect(mapStateToProps);
export const enhance = compose(withConnect);
