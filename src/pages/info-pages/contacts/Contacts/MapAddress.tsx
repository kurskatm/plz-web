// @ts-nocheck
// Modules
import React, { FC, memo, useCallback } from 'react';
import { Layout, GridItem, Text } from 'chibbis-ui-kit';
import { Map, Placemark } from 'react-yandex-maps';
// Utils
import { Consts } from '@utils';

import mark from '@/../assets/svg/map/pin-placemark.svg';

// Types
import { TContactsMapAddressProps } from './types';

const { IS_CLIENT } = Consts.ENV;

const ContactsMapAddressComponent: FC<TContactsMapAddressProps> = ({ lat, lng }) => {
  const getMarkers = useCallback(() => [lat, lng], [lat, lng]);

  return (
    <>
      <Layout className="contacts__map-container">
        <GridItem grow={2} className="contacts__dbl-column contacts__column-extra">
          {IS_CLIENT && (
            <Map
              className="contacts__map-container__map"
              state={{
                center: getMarkers(),
                zoom: 16,
                controls: [],
                behaviors: [],
              }}
              options={{
                suppressMapOpenBlock: true,
              }}>
              <Placemark
                geometry={getMarkers()}
                options={{
                  iconLayout: 'default#image',
                  iconImageHref: mark,
                  iconImageSize: [48, 48],
                  iconImageOffset: [-23, -47],
                }}
              />
            </Map>
          )}
        </GridItem>
        <GridItem grow={1} className="contacts__column contacts__column-extra">
          <div className="contacts__title contacts__title-requisites">Реквизиты</div>
          <Text variant="variant1" className="contacts__addr-req">
            <div className="contacts__requisites">
              <div className="contacts__requisites-item">ООО &laquo;Чиббис&raquo;</div>
              <div className="contacts__requisites-item">ИНН: 2901254484</div>
              <div className="contacts__requisites-item">КПП: 290101001</div>
              <div className="contacts__requisites-item">ОГРН: 1152901000284</div>
            </div>
          </Text>
        </GridItem>
      </Layout>
    </>
  );
};

export default memo(ContactsMapAddressComponent);
