// Modules
import React, { FC, memo } from 'react';
import { useMediaQuery } from 'react-responsive';
import { Layout, GridItem, Link, SocialButtons } from 'chibbis-ui-kit';
// Lib
import { socialNetworkList } from '@lib/social-networks';
// Types
import { TContactsColumnsProps } from './types';

const ContactsColumnsComponent: FC<TContactsColumnsProps> = ({ email, address }) => {
  const isTablet = useMediaQuery({
    maxWidth: 1024,
  });

  return (
    <Layout className="contacts__columns">
      <GridItem grow={1} className="contacts__column">
        <Layout column={isTablet} vertical={isTablet ? 'top' : null}>
          <GridItem className="contacts__row">
            <div className="contacts__title">Наша почта</div>
            <Link
              to="mailto:{Email}"
              color="black"
              className="contacts__text"
              border={false}
              outside>
              {email}
            </Link>
          </GridItem>
          <GridItem className="contacts__row">
            <div className="contacts__title">Партнерам</div>
            <Link
              to="mailto:{Email}"
              color="black"
              className="contacts__text"
              border={false}
              outside>
              rest@chibbis.ru
            </Link>
          </GridItem>
          <GridItem className="contacts__row">
            <div className="contacts__title">Для медиа</div>
            <Link
              to="mailto:{Email}"
              color="black"
              className="contacts__text"
              border={false}
              outside>
              press@chibbis.ru
            </Link>
          </GridItem>
        </Layout>
      </GridItem>
      <GridItem grow={1} className="contacts__column contacts__column-extra">
        <div className="contacts__title">Соцсети</div>
        <div className="contacts__social">
          <SocialButtons items={socialNetworkList} />
        </div>
      </GridItem>
      <GridItem grow={1} className="contacts__column">
        <div className="contacts__title">Адрес</div>
        <div className="contacts__address">{address}</div>
      </GridItem>
    </Layout>
  );
};

export default memo(ContactsColumnsComponent);
