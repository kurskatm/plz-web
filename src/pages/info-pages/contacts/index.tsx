// Modules
import React, { memo, useState, FC, useEffect, useRef, useCallback } from 'react';
import loadable from '@loadable/component';
import { GoogleReCaptchaProvider } from 'react-google-recaptcha-v3';
import classNames from 'classnames';
import GoToOldSite from '@components/MoveToOldDesignBanner/Banner';
// Utils
import { Consts } from '@utils';
// Components
import SupportForm from './SupportForm';
// Enhance
import { enhance } from './enhance';
// Types
import { TContactProps } from './types';

const { CITIES } = Consts.ROUTES;

const Header = loadable(() => import('@components/Header'));
const Footer = loadable(() => import('@components/Footer'));
const Contacts = loadable(() => import('./Contacts'));

const ContactsComponent: FC<TContactProps> = ({
  topics,
  topicsStatusIsIdle,
  topicsStatusIsError,
  supportTopicsFetch,
  supportMessageSend,
  messageSendStatus,
  messageSendIsError,
  pushNotification,
}) => {
  const [status, setStatus] = useState('idle');
  const timer = useRef(null);
  const setStatusHandler = useCallback(
    (arg: string) => {
      setStatus(arg);
    },
    [setStatus],
  );
  const getStyles = useCallback(
    () =>
      classNames('info-page contacts-page grid-wrapper', {
        'is-success': status === 'success',
        'is-error': status === 'error',
      }),
    [status],
  );

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  useEffect(() => {
    setStatus(messageSendStatus);

    if (messageSendStatus === 'error') {
      window.scrollTo({
        top: 0,
      });
      clearTimeout(timer.current);
      timer.current = setTimeout(() => setStatus('idle'), 5000);
    }
    return () => clearTimeout(timer.current);
  }, [messageSendStatus]);

  useEffect(
    () => {
      if (topicsStatusIsError) {
        pushNotification({
          type: 'error',
          content: 'Не удалось получить темы сообщений',
        });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [topicsStatusIsError],
  );

  useEffect(
    () => {
      if (messageSendIsError) {
        pushNotification({
          type: 'error',
          content: 'Не удалось отправить сообщение',
        });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [messageSendIsError],
  );

  return (
    <>
      <GoogleReCaptchaProvider reCaptchaKey={process.env.RECAPTCHA_KEY}>
        <div className={getStyles()}>
          <header className="grid-wrapper__header">
            <Header
              goBackText="На главную"
              goBackLink={CITIES.PATH}
              noAddress
              noMenu
              title="Связаться с нами"
            />
          </header>
          <main className="grid-wrapper__main">
            <div className="contacts-page__wrapper pad-f">
              <div className="contacts-page__grid">
                <Contacts />
                {/* @ts-ignore */}
                <SupportForm
                  status={status}
                  topics={topics}
                  setStatus={setStatusHandler}
                  topicsStatusIsIdle={topicsStatusIsIdle}
                  supportTopicsFetch={supportTopicsFetch}
                  supportMessageSend={supportMessageSend}
                />
              </div>
            </div>
          </main>
          <footer className="grid-wrapper__footer">
            <Footer />
          </footer>
        </div>
      </GoogleReCaptchaProvider>
      <GoToOldSite />
    </>
  );
};

export default enhance(memo(ContactsComponent));
