/* eslint-disable @typescript-eslint/no-explicit-any */

export type TGetValidation = (values: Record<string, any>) => Record<string, any>;
