import React, { FC, memo, useEffect, useState, ReactNode, useMemo, useCallback } from 'react';
import classNames from 'classnames';
import {
  InputField,
  InputPhone,
  Button,
  TextareaField,
  SelectField,
  Option,
  PaperPlaneIcon,
} from 'chibbis-ui-kit';
import { Form } from 'react-final-form';
import { GoogleRecaptchaComponent } from '@/components/ReCaptcha';
import Agreement from '@components/Agreement';
// Config
import { getValidation } from './config/getValidation';
// Types
import { TContactProps } from '../types';

const SupportFormComponent: FC<TContactProps> = ({
  status,
  topics,
  topicsStatusIsIdle,
  supportTopicsFetch,
  supportMessageSend,
  setStatus,
}) => {
  const [token, setToken] = useState(null);
  const selectOptions = useMemo(
    (): ReactNode[] =>
      topics.map(({ id, name }) => <Option key={id} id={id} value={name} disabled={false} />),
    [topics],
  );

  useEffect(() => {
    if (topicsStatusIsIdle && !topics.length) {
      supportTopicsFetch();
    }
  }, [topicsStatusIsIdle, supportTopicsFetch, topics]);

  const renderBanner = useMemo(
    () => (
      <div
        className={classNames('contacts-page__banner', 'pad-m', {
          'is-success': status === 'success',
          'is-error': status === 'error',
        })}>
        {status === 'success'
          ? '✓️️ Сообщение отправлено'
          : '😖 Не удалось отправить сообщение. Попробуйте через несколько минут'}
      </div>
    ),
    [status],
  );

  const getFormValidation = useCallback(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (values: Record<string, any>) => getValidation(values),
    [],
  );

  const renderForm = useMemo(
    () => (
      <Form
        onSubmit={(values) => {
          supportMessageSend({ token, ...values });
        }}
        validate={getFormValidation}
        initialValues={{
          agreement: true,
        }}
        render={({ handleSubmit, pristine, valid, values }) => (
          <form id="partnersForm" onSubmit={handleSubmit} className="support-form">
            <div className="support-form__row">
              <section className="support-form__section">
                <h2 className="support-form__m-title">Напишите нам</h2>
                <h3 className="support-form__title">Укажите данные для обратной связи</h3>
                <div className="support-form__field">
                  <div className="support-form__field-label">Контактное лицо</div>
                  <InputField theme="grey" placeholder="ФИО" name="name" />
                </div>
                <div className="support-form__field">
                  <div className="support-form__field-label">Телефон</div>
                  <InputPhone theme="grey" name="phoneNumber" required />
                </div>
                <div className="support-form__field">
                  <div className="support-form__field-label">Электронная почта</div>
                  <InputField theme="grey" name="email" required />
                </div>
              </section>
              <section className="support-form__section">
                <h3 className="support-form__title">Выберите тему сообщения</h3>
                <div className="support-form__field">
                  {selectOptions.length ? (
                    <SelectField key={`select_${pristine}`} name="selectedTopic" required>
                      {selectOptions}
                    </SelectField>
                  ) : undefined}
                </div>
              </section>
              <section className="support-form__section">
                <h3 className="support-form__title">Сообщение</h3>
                <div className="support-form__field">
                  <TextareaField
                    maxLength={600}
                    name="message"
                    theme="grey"
                    placeholder="Текст Сообщения"
                    required
                  />
                </div>
              </section>
              <section className="support-form__section">
                <div className="support-form__field">
                  <GoogleRecaptchaComponent tokenHandler={setToken} token={token} />
                </div>
                <div className="support-form__field">
                  <Agreement />
                </div>
              </section>
            </div>
            {status === 'error' && renderBanner}
            <div className="support-form__button">
              <Button key={!valid} disabled={pristine || !valid || !token || !values.agreement}>
                отправить
              </Button>
            </div>
          </form>
        )}
      />
    ),
    [supportMessageSend, token, selectOptions, status, renderBanner, getFormValidation],
  );
  const renderSuccessResult = useMemo(
    () => (
      <div className="support-result">
        <div className="support-result__content">
          <div className="support-result__icon">
            <PaperPlaneIcon width={70} height={70} />
          </div>
          <div className="support-result__title">Запрос отправлен!</div>
          <div className="support-result__description">
            Спасибо, что используете сервис доставки еды CHIBBIS.RU Мы стараемся стать лучше каждый
            день!
          </div>
          <div className="support-result__button">
            <Button onClick={() => setStatus('idle')} size="small" type="secondary">
              отправить ещё одно сообщение
            </Button>
          </div>
        </div>
      </div>
    ),
    [setStatus],
  );

  const renderContent = useMemo(() => (status === 'success' ? renderSuccessResult : renderForm), [
    status,
    renderSuccessResult,
    renderForm,
  ]);

  return renderContent;
};

export default memo(SupportFormComponent);
