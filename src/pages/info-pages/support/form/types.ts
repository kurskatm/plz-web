import { TPushNotification } from '@actions/notifications/action-types';

export interface TSupportFormValues {
  name: string;
  email: string;
  phoneNumber: string;
  selectedTopicId: string;
  message: string;
}

export type TSupportFormProps = {
  topics: TSupportSelectItem[];
  topicsStatusIsIdle: boolean;
  topicsStatusIsError: boolean;
  messageSendIsSuccess: boolean;
  messageSendIsError: boolean;
  supportTopicsFetch: () => void;
  supportMessageSend: (data: Record<string, string>) => void;
  pushNotification: TPushNotification;
};

export type TSupportSelectItem = {
  id: string;
  name: string;
};
