import React, { FC, memo, useEffect, useState, ReactNode, useMemo } from 'react';
import {
  InputField,
  InputPhone,
  CheckboxField,
  Button,
  TextareaField,
  SelectField,
  Option,
} from 'chibbis-ui-kit';
import { Form } from 'react-final-form';
import { GoogleRecaptchaComponent } from '@/components/ReCaptcha';

// Enhance
import { enhance } from './enhance';
// Types
import { TSupportFormProps } from './types';

const LicenseAgreement = () => (
  <span>
    Согласен с<a href="/reglament"> политикой конфиденциальности</a>
  </span>
);
const SupportFormComponent: FC<TSupportFormProps> = ({
  topics,
  topicsStatusIsIdle,
  topicsStatusIsError,
  supportTopicsFetch,
  supportMessageSend,
  messageSendIsSuccess,
  messageSendIsError,
  pushNotification,
}) => {
  const [token, setToken] = useState(null);
  const selectOptions = useMemo(
    (): ReactNode[] => topics.map(({ id, name }) => <Option key={id} id={id} value={name} />),
    [topics],
  );

  useEffect(() => {
    if (topicsStatusIsIdle) {
      supportTopicsFetch();
    }
  }, [topicsStatusIsIdle, supportTopicsFetch]);

  useEffect(
    () => {
      if (messageSendIsSuccess) {
        pushNotification({
          type: 'success',
          content: 'Обращение было отправлено успешно',
        });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [messageSendIsSuccess],
  );

  useEffect(
    () => {
      if (messageSendIsError) {
        pushNotification({
          type: 'error',
          content: 'Не удалось отправить обращение',
        });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [messageSendIsError],
  );

  useEffect(
    () => {
      if (topicsStatusIsError) {
        pushNotification({
          type: 'error',
          content: 'Не удалось получить темы для обращений',
        });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [topicsStatusIsError],
  );

  return (
    <Form
      onSubmit={(values) => {
        supportMessageSend({ token, ...values });
      }}
      render={({ handleSubmit, pristine, valid }) => (
        <form id="partnersForm" onSubmit={handleSubmit} className="support-form">
          <div className="support-form__row" onSubmit={handleSubmit}>
            <section className="support-form__section">
              <h3 className="support-form__title">Укажите данные для обратной связи</h3>
              <div className="support-form__field">
                <InputField
                  theme="grey"
                  label="Контакное лицо"
                  placeholder="ФИО"
                  name="name"
                  required
                />
              </div>
              <div className="support-form__field">
                <InputPhone theme="grey" label="Телефон" name="phoneNumber" required />
              </div>
              <div className="support-form__field">
                <InputField theme="grey" label="Электронная почта" name="email" required />
              </div>
            </section>
            <section className="support-form__section">
              <h3 className="support-form__title">Выберите тему сообщения</h3>
              <div className="support-form__field">
                <SelectField key={`select_${pristine}`} name="selectedTopicId">
                  {selectOptions}
                </SelectField>
              </div>
            </section>
            <section className="support-form__section">
              <h3 className="support-form__title">Сообщение</h3>
              <div className="support-form__field">
                <TextareaField
                  maxLength={600}
                  name="message"
                  theme="grey"
                  placeholder="Поле ввода"
                  required
                />
              </div>
            </section>
            <section className="support-form__section">
              <div className="support-form__field">
                <CheckboxField name="agreement" required view={LicenseAgreement} />
              </div>
              <div className="support-form__field">
                <GoogleRecaptchaComponent tokenHandler={setToken} token={token} />
              </div>
            </section>
          </div>
          <div className="support-form__button">
            <Button key={!valid} disabled={pristine || !valid || !token}>
              отправить
            </Button>
          </div>
        </form>
      )}
    />
  );
};

export default enhance(memo(SupportFormComponent));
