// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { supportTopicsFetch, supportMessageSend } from '@actions/support';
import { pushNotification } from '@actions/notifications';
// Selectors
import { supportStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/support';

const mapDispatchToProps = {
  supportTopicsFetch,
  supportMessageSend,
  pushNotification,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);
export const enhance = compose(withConnect);
