// Modules
import React, { memo, FC, useEffect } from 'react';
import loadable from '@loadable/component';
import { GoogleReCaptchaProvider } from 'react-google-recaptcha-v3';
import GoToOldSite from '@components/MoveToOldDesignBanner/Banner';

// Utils
import { Consts } from '@utils';
// Components
import SupportForm from './form';

const { CITIES } = Consts.ROUTES;

const Header = loadable(() => import('@components/Header'));
const Footer = loadable(() => import('@components/Footer'));
const Content = loadable(() => import('@components/Content'));

/* eslint-disable max-len */
const SupportComponent: FC = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <>
      <GoogleReCaptchaProvider reCaptchaKey={process.env.RECAPTCHA_KEY}>
        <div className="info-page support-page grid-wrapper">
          <header className="grid-wrapper__header">
            <Header
              goBackText="На главную"
              goBackLink={CITIES.PATH}
              noAddress
              noMenu
              title="Написать нам"
            />
          </header>
          <main className="grid-wrapper__main">
            <div className="support-page__wrapper">
              <Content>
                <SupportForm />
              </Content>
            </div>
          </main>
          <footer className="grid-wrapper__footer">
            <Footer />
          </footer>
        </div>
      </GoogleReCaptchaProvider>
      <GoToOldSite />
    </>
  );
};

export default memo(SupportComponent);
