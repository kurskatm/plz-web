// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { profileGetUserOrder, profileResetAllUiModels } from '@actions/profile';
import { profileResetUserOrderData } from '@actions/profile/orders';
import {
  profileOrderContactMe,
  profileOrderContactMeReset,
  profileOrderContactMeIdle,
} from '@actions/profile/contact-me';
// Selectors
import { profileUserOrder as mapStateToProps } from '@selectors/structured-selectors/profile';

const mapDispatchToProps = {
  profileGetUserOrder,
  profileOrderContactMe,
  profileOrderContactMeReset,
  profileOrderContactMeIdle,
  profileResetAllUiModels,
  profileResetUserOrderData,
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
