/* eslint-disable max-len */
/* eslint-disable no-nested-ternary */
// Modules
import React, { FC, memo, useEffect, useCallback, useMemo, useRef, useState } from 'react';
import loadable from '@loadable/component';
import isNil from 'lodash/isNil';
import { useQueryParam, StringParam } from 'use-query-params';
import classnames from 'classnames';
// Components
import OrderComponent from '@components/Order/Content/OrderContentFull';
import OrderHeader from '@pages/profile-pages/widgets/ProfileOrder/Content/OrderContent/OrderHeader';
import InstallSocket from '@components/InstallSocketModul';
import LastOrderWithoutReview from '@pages/restaurant/reviews/last-order-review';
import {
  // BoxInlineIcon,
  Button,
  LoadingSpinner,
} from 'chibbis-ui-kit';
// Lib
import { getOrderStatus } from '@lib/profile/order-status';
// Utils
import { Consts, useCounter, secondsToMinutes } from '@utils';
import { trackEvent } from '@utils/analytics';
// Enhance
import { enhance } from './enhance';
// Types
import { TCheckoutSuccessProps } from './types';

import { TIMER_COUNTER, TIMER_DELAY } from './constants';

const { CITIES } = Consts.ROUTES;
const Header = loadable(() => import('@components/Header'));
const Footer = loadable(() => import('@components/Footer/Info'));
const DONT_CALL_FLAG = 'перезванивать для проверки заказа не требуется';

const CheckoutComponent: FC<TCheckoutSuccessProps> = ({
  userOrder,
  userOrderIsPending,
  userOrderIsSuccess,
  userOrderIsError,
  profileGetUserOrder,
  profileResetUserOrderData,
  // Contact me
  contactMeIsPending,
  contactMeIsError,
  contactMeIsSuccess,
  contactMeOrderId,
  contactMeStatusDetails,
  profileOrderContactMe,
  profileOrderContactMeReset,
  profileOrderContactMeIdle,
  city,
}) => {
  const interval = useRef(null);
  const [orderId, setOrderId] = useQueryParam('orderId', StringParam);
  const [showContent, setShowContent] = useState<boolean>(false);

  const [currentTick] = useCounter({
    secondsCount:
      contactMeIsError && Boolean(contactMeStatusDetails?.retryAfter)
        ? contactMeStatusDetails?.retryAfter
        : TIMER_COUNTER,
    secondsCountInitial: TIMER_COUNTER,
    delayMs: TIMER_DELAY,
    trigger:
      contactMeIsSuccess || (contactMeIsError && Boolean(contactMeStatusDetails?.retryAfter)),
    callBack: () => {
      profileOrderContactMeReset({ orderId: contactMeOrderId });
    },
  });
  const { userComment } = userOrder?.order?.deliveryDetails ?? {};

  useEffect(
    () => () => {
      profileOrderContactMeReset({ orderId: contactMeOrderId });
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  useEffect(() => {
    if (isNil(userOrder) && !userOrderIsPending && !userOrderIsSuccess) {
      profileGetUserOrder({
        orderId: orderId || '003b58e5-ae86-4653-9db8-f6bdacd094e5',
      });
    }
    if (!isNil(userOrder)) {
      setOrderId(userOrder.order.id, 'push');
      trackEvent('заказ');
      const { deliveryDetails } = userOrder.order;
      if (deliveryDetails.selfPickup) {
        trackEvent('self-delivery');
      }
      if (!deliveryDetails.selfPickup && isNil(deliveryDetails.deliverToTime)) {
        trackEvent('delivery');
      }
      if (!deliveryDetails.selfPickup && !isNil(deliveryDetails.deliverToTime)) {
        trackEvent('delivery-on-time');
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userOrder?.order.id]);

  useEffect(() => {
    if (userOrderIsSuccess || userOrderIsError) {
      setShowContent(true);
    }
  }, [userOrderIsError, userOrderIsSuccess]);

  useEffect(() => {
    interval.current = setInterval(() => {
      profileGetUserOrder({
        orderId: orderId || '003b58e5-ae86-4653-9db8-f6bdacd094e5',
      });
    }, 60000);
    return () => clearInterval(interval.current);
  }, [orderId, profileGetUserOrder]);

  useEffect(
    () => () => {
      profileResetUserOrderData();
      profileOrderContactMeIdle();
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const renderStatusDescription = useMemo(() => {
    const subtitle: string =
      userComment && userComment.indexOf(DONT_CALL_FLAG) >= 0
        ? 'Ресторан скоро начнёт готовить ваш заказ'
        : 'Ресторан скоро свяжется с вами для подтверждения заказа';
    return <div className="checkout-success-page__description">{subtitle}</div>;
  }, [userComment]);

  useEffect(() => {
    if (contactMeOrderId.length && contactMeOrderId === orderId) {
      if (contactMeIsError) {
        setTimeout(() => {
          profileOrderContactMeReset({
            orderId: contactMeOrderId,
          });
        }, 180000);
      } else {
        setTimeout(() => {
          profileOrderContactMeReset();
        }, 30000);
      }
    }
  }, [contactMeOrderId, orderId, contactMeIsError, profileOrderContactMeReset]);

  const getPayedStatus = useCallback((paymentType: number) => {
    if (paymentType === 4) {
      return 2;
    }

    return 7;
  }, []);

  const onContactMeClick = useCallback(() => {
    profileOrderContactMe({
      orderId,
    });
  }, [orderId, profileOrderContactMe]);

  const getBtnStyles = useCallback(
    () =>
      classnames({
        'checkout-success-page__call-me__btn-success': contactMeIsSuccess,
        'checkout-success-page__call-me__btn-fail': contactMeIsError,
      }),
    [contactMeIsSuccess, contactMeIsError],
  );

  const renderSecondButton = useMemo(
    () => (
      <div className="checkout-success-page__call-me">
        <>
          <Button
            htmlType="button"
            onClick={onContactMeClick}
            size="small"
            type="primary"
            loading={contactMeIsPending}
            disabled={contactMeIsSuccess || contactMeIsError}
            className={getBtnStyles()}>
            {contactMeIsSuccess
              ? 'запрос отправлен'
              : contactMeIsError
              ? 'запрос отправлен с ошибкой'
              : 'cвяжитесь со мной'}
          </Button>
          <div className="checkout-success-page__call-me__message">
            {(contactMeIsSuccess || contactMeIsError) &&
              `Повторить запрос можно через ${secondsToMinutes(currentTick)}`}
          </div>
          <div className="checkout-success-page__call-me__message">
            <div className="checkout-success-page__call-me__message__text">
              {contactMeIsError && `${contactMeStatusDetails?.errorMessage}`}
            </div>
          </div>
        </>
      </div>
    ),
    [
      contactMeIsError,
      contactMeIsPending,
      contactMeIsSuccess,
      contactMeStatusDetails?.errorMessage,
      currentTick,
      getBtnStyles,
      onContactMeClick,
    ],
  );

  return (
    <div className="checkout-success-page grid-wrapper">
      <header className="grid-wrapper__header">
        <Header
          goBackText="На главную"
          goBackLink={CITIES.PATH}
          noAddress
          noMenu
          // title="Спасибо за заказ"
        />
      </header>
      <main className="grid-wrapper__main">
        {showContent ? (
          <div className="checkout-success-page__content-wrap">
            {!isNil(userOrder) && (
              <>
                <div className="checkout-success-page__head">
                  <div className="checkout-success-page__title">
                    {getOrderStatus(userOrder?.order.status)}
                  </div>
                  {userOrder?.order.status === 1 ? renderStatusDescription : null}
                </div>
                <div className="checkout-success-page__order-header">
                  <OrderHeader
                    title={userOrder?.restaurant.restaurantName}
                    date={userOrder?.order.addedOn}
                    cost={userOrder?.order.orderCost.total}
                    logo={userOrder?.restaurant.restaurantLogo}
                    status={getPayedStatus(userOrder?.order.paymentType)}
                    restId={userOrder?.restaurant.id}
                    restUrlName={userOrder?.restaurant.urlName}
                    city={city?.urlName}
                  />
                </div>
                <div className="checkout-success-page__order-content">
                  <OrderComponent order={userOrder} />
                </div>
              </>
            )}
            <div className="checkout-success-page__controls">
              {renderSecondButton}
              <LastOrderWithoutReview />
              {/* <div className="checkout-success-page__gift">
              <BoxInlineIcon />
              Получить подарок
            </div> */}
            </div>
            <InstallSocket />
          </div>
        ) : (
          <div className="checkout-success-page__spinner">
            <LoadingSpinner size="big" color="orange" />
          </div>
        )}
      </main>
      <footer className="grid-wrapper__footer">
        <Footer />
      </footer>
    </div>
  );
};

const CheckoutComponentMemo = memo(CheckoutComponent);
export default enhance(CheckoutComponentMemo);
