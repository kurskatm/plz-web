// Main types
import { TUserOrder } from '@type/profile';
import { TProfileGetUserOrder, TProfileResetAllUiModels } from '@actions/profile/action-types';
import {
  TProfileOrderContactMe,
  TProfileOrderContactMeReset,
  TProfileOrderContactMeIdle,
} from '@actions/profile/contact-me/action-types';
import { TProfileResetUserOrderData } from '@actions/profile/orders/action-types';
import { TCitiesItem } from '@/type/cities';
// Models
import { TContactMeDetailsModel } from '@models/profile/types';

export interface TCheckoutSuccessProps {
  userOrder: TUserOrder;
  userOrderIsPending: boolean;
  userOrderIsSuccess: boolean;
  userOrderIsError: boolean;
  profileGetUserOrder: TProfileGetUserOrder;
  profileOrderContactMe: TProfileOrderContactMe;
  profileOrderContactMeReset: TProfileOrderContactMeReset;
  profileOrderContactMeIdle: TProfileOrderContactMeIdle;
  contactMeIsPending: boolean;
  contactMeIsError: boolean;
  contactMeIsSuccess: boolean;
  contactMeOrderId: string;
  contactMeStatusDetails: TContactMeDetailsModel;
  profileResetAllUiModels: TProfileResetAllUiModels;
  profileResetUserOrderData: TProfileResetUserOrderData;
  city: TCitiesItem;
}
