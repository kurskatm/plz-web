// Modules
import React, { FC, memo, useState } from 'react';
// Main Components
import Content from '@components/Content';
// Components
import Title from './Title';
import List from './List';

import { TCitiesListProps } from './types';

const CitiesListComponent: FC<TCitiesListProps> = ({ setCitiesListHeight }) => {
  const [search, setSearch] = useState<string>('');

  return (
    <div className="cities-list__form">
      <div className="cities-list__top-wrapper">
        <Title search={search} setSearch={setSearch} />
      </div>
      <Content className="pad-m">
        <List search={search} setCitiesListHeight={setCitiesListHeight} />
      </Content>
    </div>
  );
};

export default memo(CitiesListComponent);
