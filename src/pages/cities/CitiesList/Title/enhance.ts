// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { profileCityUpdate } from '@actions/profile';
// Selectors
import { citiesListTitleStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/cities-list';

const mapDispatchToProps = {
  cityUpdate: profileCityUpdate,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
