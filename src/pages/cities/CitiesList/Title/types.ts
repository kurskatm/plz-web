// Modules
import { Dispatch, SetStateAction } from 'react';
// Actions
import { TProfileCityUpdate } from '@actions/profile/action-types';
// Types
import { TCitiesList, TCitiesItem } from '@type/cities';

export interface TCitiesTitleProps {
  cities: TCitiesList;
  cityUpdate: TProfileCityUpdate;
  search: string;
  setSearch: Dispatch<SetStateAction<string>>;
}

export { TCitiesItem };
