// Types
import { TCitiesItem } from '@type/cities';

export interface TTitleCityProps {
  choiceNo: () => void;
  choiceYes: () => void;
  supposedCity: TCitiesItem | null;
}
