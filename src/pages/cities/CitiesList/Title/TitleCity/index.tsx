// Modules
import React, { FC, memo, createRef, useEffect } from 'react';
import isNil from 'lodash/isNil';
import { cityIn } from 'lvovich';
// Modules Components
import { Button, Title, Link } from 'chibbis-ui-kit';
// Utils
import { trackEvent } from '@utils/analytics';
// Types
import { TTitleCityProps } from './types';

const TitleCityComponent: FC<TTitleCityProps> = ({ choiceYes, supposedCity }) => {
  const citiesListRef = createRef<HTMLDivElement>();

  useEffect(
    () => {
      if (!isNil(citiesListRef.current)) {
        trackEvent('автоопределение');
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [citiesListRef.current],
  );

  return (
    <div className="cities-list__title__choice" ref={citiesListRef}>
      <div className="cities-list__title__choice-text">
        <Title>{`Рестораны в ${cityIn(supposedCity.name)}?`}</Title>
      </div>
      <div className="cities-list__title__choice-buttons">
        <div>
          <Link to={supposedCity.urlName}>
            <Button htmlType="button" onClick={choiceYes} size="small">
              Показать
            </Button>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default memo(TitleCityComponent);
