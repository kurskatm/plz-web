// Modules
import { Dispatch, SetStateAction } from 'react';
// Types
import { TCitiesItem } from '@type/cities';

export interface TTitleCityMainProps {
  choiceNo: () => void;
  choiceYes: () => void;
  search: string;
  setSearch: Dispatch<SetStateAction<string>>;
  supposedCity: TCitiesItem | null;
}
