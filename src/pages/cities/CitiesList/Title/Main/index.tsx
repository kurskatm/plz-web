// Modules
import React, { FC, memo } from 'react';
import isNil from 'lodash/isNil';
// Main Components
import Content from '@components/Content';
// Components
import TitleCity from '../TitleCity';
import Search from '../../Search';
// Types
import { TTitleCityMainProps } from './types';

const TitleMainComponent: FC<TTitleCityMainProps> = ({
  choiceNo,
  choiceYes,
  search,
  setSearch,
  supposedCity,
}) => (
  <div className="cities-list__title">
    <Content>
      <div className="cities-list__title-inner">
        {!isNil(supposedCity) && (
          <div className="cities-list__title-inner-main">
            <TitleCity choiceNo={choiceNo} choiceYes={choiceYes} supposedCity={supposedCity} />
          </div>
        )}
        <div className="cities-list__title-inner-search pad-m">
          <div className="cities-list__search-header">Выберите город</div>
          <Search search={search} setSearch={setSearch} />
        </div>
      </div>
    </Content>
  </div>
);

export default memo(TitleMainComponent);
