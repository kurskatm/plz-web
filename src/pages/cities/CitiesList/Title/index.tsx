// Modules
import React, { FC, memo, useCallback, useEffect, useState } from 'react';
import throttle from 'lodash/throttle';
import isNil from 'lodash/isNil';
// Lib
import { CreateYandexLocation } from '@lib/yandex';
// Utils
import { Consts } from '@utils';
// Components
import TitleMain from './Main';
// Enhance
import { enhance } from './enhance';
// Types
import { TCitiesTitleProps, TCitiesItem } from './types';

const { THROTTLE } = Consts;

const TitleComponent: FC<TCitiesTitleProps> = ({ cities, cityUpdate, search, setSearch }) => {
  const [supposedCity, updateSupposedCity] = useState<TCitiesItem | null>(null);

  useEffect(
    () => {
      const location = new CreateYandexLocation({
        cities,
        updateSupposedCity,
      });
      location.inject();
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const choiceNoThrottle = throttle(() => updateSupposedCity(null), THROTTLE.DEFAULT);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const choiceNo = useCallback(choiceNoThrottle, []);

  const choiceYesThrottle = throttle(() => {
    if (!isNil(supposedCity)) {
      cityUpdate(supposedCity.id);
    }
  }, THROTTLE.DEFAULT);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const choiceYes = useCallback(choiceYesThrottle, [cityUpdate, supposedCity]);

  return (
    <TitleMain
      choiceNo={choiceNo}
      choiceYes={choiceYes}
      search={search}
      setSearch={setSearch}
      supposedCity={supposedCity}
    />
  );
};

const TitleMemo = memo(TitleComponent);
export default enhance(TitleMemo);
