// Types
import { TCitiesList } from '@type/cities';

export interface TListProps {
  cities: TCitiesList;
  search: string;
  setCitiesListHeight?: React.Dispatch<React.SetStateAction<number>>;
}
