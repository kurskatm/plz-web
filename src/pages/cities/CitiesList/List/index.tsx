// Modules
import React, { createRef, FC, memo, useCallback, useEffect, useState } from 'react';
import classNames from 'classnames';
import isNil from 'lodash/isNil';
// Lib
import { CitiesListManage, CitiesListHeightManage } from '@lib/cities-list';
// Utils
import { Consts } from '@utils';
// Components
import DesktopList from './DesktopList';
// Enhance
import { enhance } from './enhance';
// Types
import { TListProps } from './types';

const { IS_CLIENT } = Consts.ENV;
const blockRef = createRef<HTMLDivElement>();
const citiesManage = new CitiesListManage();
let heightManage: CitiesListHeightManage | null;

const ListComponent: FC<TListProps> = ({ cities, search, setCitiesListHeight = () => null }) => {
  const initialCities = useCallback(
    () =>
      citiesManage.initCities({
        cities,
        search,
      }),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const [validCities, setCities] = useState(initialCities());

  useEffect(() => {
    citiesManage.getCities({
      cities,
      search,
      setCitiesReact: setCities,
    });
  }, [cities, search]);

  useEffect(() => {
    const resizeObserver = new ResizeObserver((entries) => {
      setCitiesListHeight(entries[0].target.clientHeight);
    });
    resizeObserver.observe(blockRef.current);
  }, [setCitiesListHeight]);

  useEffect(() => {
    if (IS_CLIENT) {
      heightManage = new CitiesListHeightManage({
        blockRef: blockRef.current,
      });
    }

    return () => {
      if (!isNil(heightManage)) {
        heightManage.unsubscribe();
      }
    };
  }, []);

  useEffect(() => {
    heightManage.getColls();
  }, [validCities]);

  const getStyles = useCallback(
    () => classNames('cities-list__list', `cities-list__list-${IS_CLIENT ? 'hidden' : 'visible'}`),
    [],
  );

  return (
    <div ref={blockRef} className={getStyles()}>
      <DesktopList search={search} validCities={validCities} />
    </div>
  );
};

const ListComponentMemo = memo(ListComponent);
export default enhance(ListComponentMemo);
