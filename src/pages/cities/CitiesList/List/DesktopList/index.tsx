// Modules
import React, { FC, memo, useMemo } from 'react';
// Components
import GroupList from './GroupList';
// Types
import { TDesktopListProps, TGroupListItem } from './types';

const DesktopListComponent: FC<TDesktopListProps> = ({ validCities, search }) => {
  const renderList = useMemo(
    () => (item: TGroupListItem) => (
      <GroupList key={item[0].name[0]} groupItem={item} search={search} />
    ),
    [search],
  );

  return <>{validCities.map(renderList)}</>;
};

export default memo(DesktopListComponent);
