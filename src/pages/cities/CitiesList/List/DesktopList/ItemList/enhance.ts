// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { profileCityUpdate } from '@actions/profile';
import { cuisinesUpdate } from '@/actions/cuisines';

const mapDispatchToProps = {
  cityUpdate: profileCityUpdate,
  cuisinesUpdate,
};
const withConnect = connect(null, mapDispatchToProps);

export const enhance = compose(withConnect);
