// Main Types
import { TCuisinesUpdate } from '@/actions/cuisines/action-types';
import { TProfileCityUpdate } from '@actions/profile/action-types';
import { TCitiesItem } from '@type/cities';

export interface TItemListProps {
  cityUpdate: TProfileCityUpdate;
  item: TCitiesItem;
  search: string;
  cuisinesUpdate: TCuisinesUpdate;
}
