// Modules
import React, { FC, memo, useCallback } from 'react';
import throttle from 'lodash/throttle';
// Modules Components
import Highlighter from 'react-highlight-words';
import { Link } from 'chibbis-ui-kit';
// Utils
import { Consts } from '@utils';
// Enhance
import { enhance } from './enhance';
// Types
import { TItemListProps } from './types';

const { THROTTLE } = Consts;

const ItemListComponent: FC<TItemListProps> = ({ cityUpdate, item, search, cuisinesUpdate }) => {
  const onClickThrottle = throttle(() => {
    cityUpdate(item.id);
    cuisinesUpdate([]);
  }, THROTTLE.DEFAULT);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const onClick = useCallback(onClickThrottle, [cityUpdate, item.urlName, item.id, item.name]);

  return (
    <div className="cities-list__item">
      <span role="presentation" onClick={onClick} className="cities-list__item-inner">
        <Link border={false} color="gray" to={`/${item.urlName}`}>
          <Highlighter
            highlightClassName="cities-list__item-mark"
            searchWords={[search]}
            autoEscape
            textToHighlight={item.name}
          />
        </Link>
      </span>
    </div>
  );
};

const ItemListMemo = memo(ItemListComponent);
export default enhance(ItemListMemo);
