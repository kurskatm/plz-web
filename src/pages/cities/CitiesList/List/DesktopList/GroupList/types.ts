// Types
import { TCitiesList, TCitiesItem } from '@type/cities';

export interface TGroupListProps {
  groupItem: TCitiesList;
  search: string;
}

export type TGroupListItem = TCitiesItem;
