// Modules
import React, { FC, memo, useMemo } from 'react';
// Components
import ItemList from '../ItemList';
// Types
import { TGroupListProps, TGroupListItem } from './types';

const GroupListComponent: FC<TGroupListProps> = ({ groupItem, search }) => {
  const renderItem = useMemo(
    () => (item: TGroupListItem) => <ItemList key={item.id} item={item} search={search} />,
    [search],
  );

  return <>{groupItem.map(renderItem)}</>;
};

export default memo(GroupListComponent);
