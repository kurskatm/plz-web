// Types
import { TCitiesList, TCitiesItem } from '@type/cities';

export interface TDesktopListProps {
  search: string;
  validCities: TCitiesList[];
}

export type TGroupListItem = TCitiesItem[];
