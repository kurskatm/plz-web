// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Selectors
import { citiesListListStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/cities-list';

const withConnect = connect(mapStateToProps);
export const enhance = compose(withConnect);
