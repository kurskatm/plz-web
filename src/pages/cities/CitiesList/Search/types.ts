// Modules
import { Dispatch, SetStateAction } from 'react';

export interface TSearchProps {
  search: string;
  setSearch: Dispatch<SetStateAction<string>>;
}
