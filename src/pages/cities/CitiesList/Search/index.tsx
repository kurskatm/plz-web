// Modules
import React, { FC, memo, useCallback } from 'react';
// Modules Components
import { Input } from 'chibbis-ui-kit';
// Types
import { TSearchProps } from './types';

const SearchComponent: FC<TSearchProps> = ({ search, setSearch }) => {
  const onChange = useCallback(
    (e: Event) => {
      const target = e.target as HTMLInputElement;
      setSearch(target.value);
    },
    [setSearch],
  );

  return (
    <div className="cities-list__search">
      <Input
        autocomplete={false}
        clear
        name="search"
        onChange={onChange}
        placeholder="Найти город"
        searchIcon
        value={search}
        theme="grey"
      />
    </div>
  );
};

export default memo(SearchComponent);
