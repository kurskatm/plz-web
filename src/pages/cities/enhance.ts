// Enhancers
import { withRedirectHome } from '@enhancers/redirect-home';
// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { citiesFetch } from '@actions/cities';
// Selectors
import { citiesFetchStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/cities-list';

const mapDispatchToProps = {
  citiesFetch,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withRedirectHome(), withConnect);
