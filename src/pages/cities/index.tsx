// Modules
import React, { memo, FC } from 'react';
// Enhance
import AccountLogin from '@/components/AccountLogin';
import { enhance } from './enhance';
// Types
import { TCitiesProps } from './types';

const LoginComponent: FC<TCitiesProps> = () => (
  <>
    <div className="login-page">
      <main>
        <div className="login-page__content-wrap">
          <AccountLogin toggleVisible={() => {}} form="client" />
        </div>
      </main>
    </div>
  </>
);

const LoginMemo = memo(LoginComponent);

export default enhance(LoginMemo);
