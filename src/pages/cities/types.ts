// Main types
import { TCitiesFetch } from '@actions/cities/action-types';

export interface TCitiesProps {
  citiesFetch: TCitiesFetch;
  citiesIsIdle: boolean;
}
