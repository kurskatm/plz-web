// Main types
import { TCitiesItem } from '@/type/cities';
import { TPushNotification } from '@actions/notifications/action-types';
import { TPromocodeReset } from '@actions/promocode/action-types';
import { TInfoModel } from '@models/profile/types';
import { TPromocodeResult } from '@type/promocode';
import { TRestaurantStore } from '@type/restaurants/restaurant-stores';
import { TNewGeocodeData } from '@type/new-geocode';

export type TCheckoutFormProps = {
  orderId?: string;
  auth: boolean;
  checkoutIsPending: boolean;
  checkoutOptionalError: { type: string; message: string };
  cartId: string;
  value?: string;
  paymentTypes?: number;
  info: TInfoModel;
  checkoutData: Record<string, string>;
  geocode: TNewGeocodeData;
  address: string;
  city: TCitiesItem;
  checkoutStatus: string;
  stores: TRestaurantStore[];
  send: () => void;
  reset: () => void;
  shopingCartSave: (data: Record<string, string>) => void;
  timeList: Record<string, string>[];
  total?: number;
  discount?: number;
  deliveryCost?: number;
  personsCount?: number;
  promocodeValidIsPending: boolean;
  promocodeValidIsError: boolean;
  promocodeData: TPromocodeResult;
  promocodeReset: TPromocodeReset;
  pushNotification: TPushNotification;
  authModalShow: (data: boolean) => void;
};
