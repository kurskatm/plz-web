// Modules
import React, { FC, memo, useCallback } from 'react';
import classnames from 'classnames';
// Components
import { RadioField } from 'chibbis-ui-kit';
// Types
import { TDeliveryGroupProps } from './types';

const CommonTradeAreaGroupComponent: FC<TDeliveryGroupProps> = ({ tradeArea = 'cellar' }) => {
  console.log(tradeArea)
  const getAreaStyles = useCallback(
    (area: string) => console.log(area) ||
      classnames(
        {
          'is-cellar': area === 'cellar',
          'is-base': area === 'base',
        },
        'checkout-form__delivery-wrap',
      ),
    [],
  );

  return (
    <>
      <div className="checkout-form__field">
        <div className={getAreaStyles(tradeArea)}>
          <RadioField
            className="radio-field-cellar"
            name="objectCommonTradeAreaLocation"
            value="cellar"
            view="В подвале"
          />
          <RadioField
            className="radio-field-base"
            name="objectCommonTradeAreaLocation"
            value="base"
            view="В цоколе"
          />
        </div>
      </div>
    </>
  );
};

export default memo(CommonTradeAreaGroupComponent);
