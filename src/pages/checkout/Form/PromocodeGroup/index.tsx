/* eslint-disable max-len */
// Modules
import React, { FC, memo, useCallback, useMemo, createRef, useEffect } from 'react';
import isNil from 'lodash/isNil';
import { FormApi } from 'final-form';
import classnames from 'classnames';
// Lib
import { CheckoutPromocodeManager } from '@lib/checkout/promocode-manager';
// Utils
import { usePrevious } from '@/utils/usePrevious';
// Components
import { InputField } from 'chibbis-ui-kit';
import ToggleField from '../ToggleField';
// Enhance
import { enhance } from './enhance';
// Types
import { TCheckoutFormVals, TPromocodeGroupProps } from './types';

const inputHtmlRef = createRef<HTMLInputElement>();

const PromocodeGroupComponent: FC<TPromocodeGroupProps> = ({
  form: formProp,
  promocodeData,
  auth,
  promocodeFetch,
  promocodeReset,
}) => {
  useEffect(
    () => {
      if (!isNil(inputHtmlRef.current)) {
        const closeIconRef = document.body
          .getElementsByClassName('checkout-form__input-promocode')[0]
          .getElementsByClassName('icon_clear')[0];
        const promocodeManager = new CheckoutPromocodeManager({
          fetchSuggest: promocodeFetch,
          resetSuggest: promocodeReset,
          inputRef: inputHtmlRef.current,
          closeIconRef,
        });

        return () => {
          promocodeManager.unsubscribe();
        };
      }

      return undefined;
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [inputHtmlRef.current],
  );

  const prevAuth = usePrevious(!!auth);

  useEffect(
    () => {
      if (
        !!auth !== prevAuth &&
        !isNil(formProp.getState().values.promocode) &&
        formProp.getState().values.promocode.length > 2
      ) {
        promocodeReset();
        promocodeFetch(formProp.getState().values.promocode);
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [auth],
  );

  const onPromocodeToggleChange = useCallback(
    (form: FormApi<TCheckoutFormVals, Partial<TCheckoutFormVals>>) => {
      const { promocodeToggle } = form.getState().values;
      if (!isNil(promocodeToggle) && !promocodeToggle) {
        if (!isNil(promocodeData)) {
          promocodeReset();
        }
        form.change('promocode', '');
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [promocodeData],
  );

  const promocodeToggleFieldStyles = useMemo(
    () =>
      classnames('toggle-field', 'toggle-field__promocode', {
        'toggle-field__with-promocode':
          !isNil(promocodeData?.acceptedMessage) ||
          !isNil(promocodeData?.errorMessage) ||
          (formProp.getState().values.promocodeToggle &&
            (!formProp.getState().values.promocode ||
              formProp.getState().values.promocode.length < 3)),
      }),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [
      promocodeData?.acceptedMessage,
      promocodeData?.errorMessage,
      // eslint-disable-next-line react-hooks/exhaustive-deps
      formProp.getState().values,
    ],
  );

  const promocodeFieldClassName = useMemo(
    () =>
      classnames('checkout-form__input-promocode', {
        'checkout-form__input-promocode__success': promocodeData?.wasAccepted,
        'checkout-form__input-promocode__error':
          !promocodeData?.wasAccepted && !!promocodeData?.errorMessage,
      }),
    [promocodeData],
  );

  const renderMessage = useMemo(
    () => {
      if (
        formProp.getState().values.promocodeToggle &&
        (!formProp.getState().values.promocode || formProp.getState().values.promocode.length < 3)
      ) {
        return (
          <div className="checkout-form__promocode-subscription is-error">
            Должно быть не менее 3 символов
          </div>
        );
      }

      if (!isNil(promocodeData)) {
        if (promocodeData.wasAccepted) {
          return (
            <div className="checkout-form__promocode-subscription is-success">
              {promocodeData.acceptedMessage}
            </div>
          );
        }

        if (!promocodeData.wasAccepted && promocodeData.errorMessage) {
          return (
            <div className="checkout-form__promocode-subscription is-error">
              {promocodeData.errorMessage}
            </div>
          );
        }
      }

      return null;
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [promocodeData, formProp.getState().values],
  );

  return (
    <div className="checkout-form__promocode-field">
      <ToggleField
        name="promocodeToggle"
        title="Промокод"
        checked={formProp.getState().values.promocodeToggle}
        className={promocodeToggleFieldStyles}>
        <InputField
          theme="grey"
          name="promocode"
          autoFocus
          className={promocodeFieldClassName}
          inputRef={inputHtmlRef}
          clear
        />
        {onPromocodeToggleChange(formProp)}
      </ToggleField>
      {renderMessage}
    </div>
  );
};

const PromocodeGroupMemo = memo(PromocodeGroupComponent);
export default enhance(PromocodeGroupMemo);
