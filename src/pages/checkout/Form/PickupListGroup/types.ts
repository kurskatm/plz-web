export type TPickupItemProps = {
  address: string;
  id: string;
};

export type TPickupListProps = {
  pickups?: TPickupItemProps[];
};
