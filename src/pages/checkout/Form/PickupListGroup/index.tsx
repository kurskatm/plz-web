// Modules
import React, { FC, memo } from 'react';
import { RadioField } from 'chibbis-ui-kit';
// Type
import type { TPickupListProps, TPickupItemProps } from './types';

const PickupItem: FC<TPickupItemProps> = ({ address, id }) => (
  <div key={`${id}`} className="pickup__item">
    <RadioField name="pickupAddress" key={id} value={id} view={`${address}`} required />
  </div>
);

const PickupListComponent: FC<TPickupListProps> = ({ pickups }) => (
  <div className="checkout-form__pickup-list">
    {pickups.map((item: TPickupItemProps, index: number) => (
      // @ts-ignore
      <PickupItem index={index} key={item.id} {...item} />
    ))}
  </div>
);

export default memo(PickupListComponent);
