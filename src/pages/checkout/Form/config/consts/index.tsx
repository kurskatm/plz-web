export const timeFrom = [
  {
    id: 0,
    name: '10:00',
  },
  {
    id: 1,
    name: '10:30',
  },
  {
    id: 2,
    name: '11:00',
  },
  {
    id: 3,
    name: '11:30',
  },
  {
    id: 4,
    name: '12:00',
  },
  {
    id: 5,
    name: '12:30',
  },
  {
    id: 6,
    name: '13:00',
  },
  {
    id: 7,
    name: '13:30',
  },
  {
    id: 8,
    name: '14:00',
  },
  {
    id: 9,
    name: '14:30',
  },
  {
    id: 10,
    name: '15:00',
  },
  {
    id: 11,
    name: '15:30',
  },
  {
    id: 11,
    name: '16:00',
  },
  {
    id: 12,
    name: '16:30',
  },
  {
    id: 13,
    name: '17:00',
  },
  {
    id: 14,
    name: '17:30',
  },
  {
    id: 15,
    name: '18:00',
  },
  {
    id: 16,
    name: '18:30',
  },
  {
    id: 17,
    name: '19:00',
  },
  {
    id: 18,
    name: '19:30',
  },
  {
    id: 19,
    name: '20:00',
  },
  {
    id: 20,
    name: '20:30',
  },
  {
    id: 21,
    name: '21:00',
  },
  {
    id: 22,
    name: '21:30',
  },
  {
    id: 23,
    name: '22:00',
  },
];

export const timeTo = [...timeFrom];

export const List = [
  {
    id: '1',
    value: 'Hello',
  },
  {
    id: '2',
    value: 'Hello World',
  },
  {
    id: '3',
    value: 'Hello W',
  },
];
