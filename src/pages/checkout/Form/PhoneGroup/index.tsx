// Modules
import React, { FC, memo, useRef, useCallback, useState, useEffect } from 'react';
import isNil from 'lodash/isNil';
import debounce from 'lodash/debounce';
// Component
import { InputPhone } from 'chibbis-ui-kit';
// Enhance
import { enhance } from './enhance';
// Types
import { TPhoneFieldProps } from './types';

const PhoneFieldComponent: FC<TPhoneFieldProps> = ({
  auth,
  authModalSavePhone,
  authModalShow,
  formValue,
  initValue,
  pristine,
  unauthorize,
}) => {
  const [authPhone, setAuthPhone] = useState<string>(initValue || '');

  const updateAuthPhone = useRef(
    debounce((result: string) => {
      authModalSavePhone(result);
      authModalShow(true);
    }, 200),
  );

  useEffect(
    () => {
      if (initValue.length && initValue !== authPhone) {
        setAuthPhone(initValue);
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [initValue],
  );

  const checkAuthPhone = useCallback(
    (phoneNumber: string | undefined | null, formPristine: boolean) => {
      const phoneNumberStr = String(phoneNumber).replace(/[^\d]/g, '');
      if (
        !isNil(phoneNumber) &&
        phoneNumberStr.length === 11 &&
        authPhone !== phoneNumberStr &&
        !formPristine
      ) {
        if (!auth) {
          updateAuthPhone.current(phoneNumberStr);
          setAuthPhone(phoneNumberStr);
        } else {
          unauthorize();
          updateAuthPhone.current(phoneNumberStr);
          setAuthPhone(phoneNumberStr);
        }
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [authPhone, auth],
  );

  return (
    <div className="checkout-form__field">
      <InputPhone theme="grey" label="Телефон" name="phoneNumber" required />
      {checkAuthPhone(formValue, pristine)}
    </div>
  );
};

const PhoneFieldMemo = memo(PhoneFieldComponent);
export default enhance(PhoneFieldMemo);
