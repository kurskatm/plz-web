// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { authModalShow, authModalSavePhone, unauthorize } from '@actions/auth';

const mapDispatchToProps = {
  authModalShow,
  authModalSavePhone,
  unauthorize,
};

const withConnect = connect(null, mapDispatchToProps);

export const enhance = compose(withConnect);
