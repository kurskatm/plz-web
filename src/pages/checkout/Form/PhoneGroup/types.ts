import { TUnauthorize } from '@actions/auth/action-types';

export interface TPhoneFieldProps {
  auth: boolean;
  formValue: string;
  initValue: string;
  pristine: boolean;
  authModalShow: (data: boolean) => void;
  authModalSavePhone: (data: string) => void;
  unauthorize: TUnauthorize;
}
