import { ReactNode } from 'react';

export type TToggleFieldProps = {
  name: string;
  title: string;
  checked: boolean;
  children?: ReactNode;
  className?: string;
};
