// Modules
import React, { FC, memo } from 'react';
import { SwitchField } from 'chibbis-ui-kit';
// Types
import type { TToggleFieldProps } from './types';

const ToggleFieldComponent: FC<TToggleFieldProps> = ({
  name,
  title,
  checked,
  children,
  className,
}) => (
  <div className={className}>
    <div className="toggle-field__controller">
      <SwitchField name={name} view={title} />
    </div>
    {checked && children && <div className="toggle-field__content">{children}</div>}
  </div>
);

ToggleFieldComponent.defaultProps = {
  className: 'toggle-field',
};

export default memo(ToggleFieldComponent);
