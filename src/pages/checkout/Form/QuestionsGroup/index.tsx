/* eslint-disable max-len */
// Modules
import React, {
  FC, memo, useCallback, useMemo, createRef, useEffect,
} from 'react';
import isNil from 'lodash/isNil';
import { FormApi } from 'final-form';
import classnames from 'classnames';
// Lib
import { CheckoutPromocodeManager } from '@lib/checkout/promocode-manager';
// Utils
import { usePrevious } from '@/utils/usePrevious';
// Components
import { InputField, CheckboxField, SelectField, Option } from 'chibbis-ui-kit';
import ToggleField from '../ToggleField';
import CommonTradeAreaGroup from '../CommonTradeAreaGroup';

// Enhance
import { enhance } from './enhance';
// Types
import { TCheckoutFormVals, TPromocodeGroupProps } from './types';

const inputHtmlRef = createRef<HTMLInputElement>();

const CATEGORIES = ['А', 'Б', 'В1', 'В2', 'В3', 'В4', 'Г', 'Д'];
const QuestionsGroupComponent: FC<TPromocodeGroupProps> = ({
  values,
}) => {
  const questions = values?.selectedTopic?.id?.questions?.split(',') || [];

  console.log(values)
  const fields = [
    null,
    null,
    <div className="checkout-form__questions-field">
      <div className="checkout-form__field-label">Ширина здания, м</div>
      <SelectField key="select_objectCategory" name="objectWidth" required>
        <Option id={50} value="менее 60" />
        <Option id={70} value="более 60" />
      </SelectField>
    </div>,
    <div className="checkout-form__questions-field">
      <div className="checkout-form__field-label">Категория по взрывопожарной или пожарной опасности</div>
      <SelectField key="select_objectCategory" name="objectCategory" required>
        {CATEGORIES.map((item) => (<Option id={item} value={item} />))}
      </SelectField>
    </div>,
    <div className="checkout-form__questions-field">
      <InputField
        theme="grey"
        name="objectUpfloors"
        label="Количество надземных этажей"
        clear
      />
    </div>,
    <div className="checkout-form__questions-field">
      <InputField
        theme="grey"
        name="objectFireRoomArea"
        label="Площадь этажа пожарного отсека, м2"
        clear
      />
    </div>,
    <div className="checkout-form__questions-field">
      <InputField
        theme="grey"
        name="objectHeight"
        label="Высота здания, м"
        clear
      />
    </div>,
    <div className="checkout-form__questions-field">
      <InputField
        theme="grey"
        name="objectVolume"
        label="Объём здания, куб.м."
        clear
      />
    </div>,
    <div className="checkout-form__questions-field">
      <InputField
        theme="grey"
        name="objectUnderFloors"
        label="Количество подземных этажей (в т.ч. подвал)"
        clear
      />
    </div>,
    <div className="checkout-form__questions-field">
      <CheckboxField name="objectIsUnderFloor" required view="Имеется ли в здании цокольный этаж?" />
    </div>,
    <div className="checkout-form__questions-field">
      <InputField
        theme="grey"
        name="objectUnderFloorArea"
        label="Укажите площадь подвала (цоколя), м2"
        clear
      />
    </div>,
    <div className="checkout-form__questions-field">
      <InputField
        theme="grey"
        name="objectCommonTradeArea"
        label="Общая торговая площадь, м2"
        clear
      />
    </div>,
    <div className="checkout-form__questions-field">
      <CommonTradeAreaGroup tradeArea={values.objectCommonTradeAreaLocation} />
    </div>,
    <div className="checkout-form__questions-field">
      <InputField
        theme="grey"
        name="objectTradeArea"
        label="Площадь торгового зала, м2"
        clear
      />
    </div>,
    <div className="checkout-form__questions-field">
      <CheckboxField name="objectDinnerRoom" required view="Обеденный зал расположен в подвале" />
    </div>,
    <div className="checkout-form__questions-field">
      <InputField
        theme="grey"
        name="objectTradeArea"
        label="Число посетителей (мест), чел."
        clear
      />
    </div>,
    <div className="checkout-form__questions-field">
      <CheckboxField name="objectBlackTradeRooms" required view="Имеются торговый(ые) зал(ы) без естественного освещения" />
    </div>,
  ];

  const filteredFields = questions.map(item => fields[item] || null);
  console.log(fields, fields.length)
  return (
    <div className="checkout-form__questions">
      {filteredFields}
    </div>
  );
};

const PromocodeGroupMemo = memo(QuestionsGroupComponent);
export default enhance(PromocodeGroupMemo);
