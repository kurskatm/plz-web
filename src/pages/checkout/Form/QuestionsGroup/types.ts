// Main types
import { TPromocodeValidate, TPromocodeReset } from '@actions/promocode/action-types';
import { TPromocodeResult } from '@type/promocode';
import { FormApi } from 'final-form';
import { TCheckoutFormVals } from '../../types';

export interface TPromocodeGroupProps {
  promocodeFetch: TPromocodeValidate;
  promocodeReset: TPromocodeReset;
  promocodeData: TPromocodeResult;
  form: FormApi<TCheckoutFormVals, Partial<TCheckoutFormVals>>;
  auth: string;
}

export { TCheckoutFormVals };
