// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { promocodeFetch, promocodeReset } from '@actions/promocode';
// Selectors
import { shopingCartPromocodeStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/shoping-cart';

const mapDispatchToProps = {
  promocodeFetch,
  promocodeReset,
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
