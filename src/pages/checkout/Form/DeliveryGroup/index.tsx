// Modules
import React, { FC, memo, useCallback } from 'react';
import classnames from 'classnames';
// Components
import { RadioField } from 'chibbis-ui-kit';
import AddressGroup from '../AddressGroup';
import PickupList from '../PickupListGroup';
// Types
import { TDeliveryGroupProps } from './types';

const DeliveryGroupComponent: FC<TDeliveryGroupProps> = ({ formDeliveryType = 'delivery', stores }) => {
  console.log(formDeliveryType)
  const getDeliveryStyles = useCallback(
    (delivery: string) =>
      classnames(
        {
          'is-pickup': delivery === 'SelfPickup',
          'is-delivery': delivery === 'delivery',
        },
        'checkout-form__delivery-wrap',
      ),
    [],
  );

  return (
    <>
      <div className="checkout-form__field">
        <div className={getDeliveryStyles(formDeliveryType)}>
          <RadioField
            className="radio-field-delivery"
            name="objectType"
            value="delivery"
            view="Здание"
          />
          <RadioField
            className="radio-field-pickup"
            name="objectType"
            value="SelfPickup"
            view="Помещение"
            // disabled={!stores?.length}
          />
        </div>
      </div>
      <div className="checkout-form__field">
        <AddressGroup />
      </div>
    </>
  );
};

export default memo(DeliveryGroupComponent);
