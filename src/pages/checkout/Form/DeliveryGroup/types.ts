// Main types
import { TRestaurantStore } from '@/type/restaurants/restaurant-stores';

export interface TDeliveryGroupProps {
  formDeliveryType: string;
  stores: TRestaurantStore[];
}
