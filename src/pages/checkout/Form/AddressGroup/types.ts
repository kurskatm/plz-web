// Main Types
import { TShowRestaurantModals } from '@actions/restaurant/modal/action-types';

export interface TAddressGroupProps {
  showRestaurantModals: TShowRestaurantModals;
  initialAddress: string;
}
