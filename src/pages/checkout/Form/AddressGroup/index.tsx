// Modules
import React, { FC, memo, useCallback } from 'react';
import classNames from 'classnames';
import { useMediaQuery } from 'react-responsive';
// Components
import { InputField } from 'chibbis-ui-kit';
// Enhance
import { enhance } from './enhance';
// Types
import { TAddressGroupProps } from './types';

const AddressCheckoutFormComponent: FC<TAddressGroupProps> = ({
  showRestaurantModals,
  initialAddress,
}) => {
  const isTwoLinedLabelPhone = useMediaQuery({
    maxWidth: 365,
  });

  const isTwoLinedLabelTablet = useMediaQuery({
    minWidth: 769,
    maxWidth: 835,
  });

  const onAddressClick = useCallback(
    () => {
      showRestaurantModals({
        modalType: 'change-address',
        showModal: true,
      });
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const getWrapStyles = useCallback(
    () =>
      classNames('checkout-form__address-wrap', {
        'checkout-form__address-wrap-two-lines': isTwoLinedLabelPhone || isTwoLinedLabelTablet,
      }),
    [isTwoLinedLabelPhone, isTwoLinedLabelTablet],
  );

  return (
    <div className="checkout-form__address-group">
      <div className="checkout-form__field">
        <div className="checkout-form__address-label">Адрес</div>
        <div
          className="checkout-form__address-text"
          role="presentation"
          onClickCapture={onAddressClick}
          style={{
            color: initialAddress.length ? '#262626' : '#8A9095',
          }}>
          {initialAddress.length ? initialAddress : 'Укажите улицу и дом'}
        </div>
      </div>
      {/* <div className={getWrapStyles()}>
        <InputField theme="grey" label="Квартира / офис" name="flat" />
        <InputField theme="grey" label="Подъезд" name="porch" />
        <InputField theme="grey" label="Этаж" name="floor" />
      </div> */}
    </div>
  );
};

const AddressCheckoutFormMemo = memo(AddressCheckoutFormComponent);
export default enhance(AddressCheckoutFormMemo);
