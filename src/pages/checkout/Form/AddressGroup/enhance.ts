// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { showRestaurantModals } from '@actions/restaurant/modal';
// Selectors
import { checkoutAddressStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/header';

const mapDispatchToProps = {
  showRestaurantModals,
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
