/* eslint-disable react/jsx-indent */
// Modules
import React, {
  FC, memo, useMemo, useEffect, useRef, useCallback, ReactNode,
} from 'react';
import { Form } from 'react-final-form';
import { useHistory } from 'react-router-dom';
import isNil from 'lodash/isNil';
import debounce from 'lodash/debounce';
// Lib
import { removeCartId } from '@lib/cookie';
import { getInitialPaymentType } from '@lib/restaurants/payment-types';
// Componentns
import Agreement from '@components/Agreement';
import { cloudpaymentsInit } from '@/utils/ cloudpayments';
import {
  InputField, SelectField, Option, CheckboxField, Button,
} from 'chibbis-ui-kit';

import CommentsInfoGroup from './CommentsInfoGroup';
import DeliveryGroup from './DeliveryGroup';
import TitleGroup from './HeaderGroup';
import QuestionsGroup from './QuestionsGroup';
import Panel from './Panel';
import PhoneField from './PhoneGroup';
// Utils
import { deliveryCheckoutData } from '../utils';
// Types
import { TCheckoutFormProps } from './types';
import { TCheckoutFormVals } from '../types';

// DATA
import { default as DATA } from './data/f_sp2.json';

const CheckoutFormComponent: FC<TCheckoutFormProps> = ({
  auth,
  geocode,
  address,
  stores,
  city,
  send,
  checkoutIsPending,
  promocodeValidIsPending,
  promocodeValidIsError,
  promocodeData,
  pushNotification,
  personsCount,
  authModalShow,
  fTypesFetch,
}) => {
  const history = useHistory();
  const selectOptions = useMemo(
    (): ReactNode[] =>
      DATA.map(({
        _id, f, name, questions,
      }) => (
        <Option
          key={_id?.$oid}
          id={{ f, questions }}
          value={`${f} ${name}`}
          disabled={false}
        />
      )),
    [DATA],
  );
  useEffect(() => {
    console.log(fTypesFetch, DATA);
  }, [fTypesFetch]);

  const submitHandler = (vals: TCheckoutFormVals) => {
    if (!auth) {
      authModalShow(true);
      return;
    }

    send(
      // @ts-ignore
      deliveryCheckoutData({
        ...vals,
        cityId: city.id,
        house: geocode?.house,
        street: geocode?.street,
        lat: geocode?.lat,
        lng: geocode?.lng,
        promoCodeId: promocodeData?.promoCodeId,
        personsCount,
      }),
    );
  };

  const redirectToSuccess = useCallback(
    (id: string) => {
      console.log();
    },
    [history],
  );

  const onCloudPaymentsSuccess = useCallback(
    (id: string) => {
      redirectToSuccess(id);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const onCloudPaymentsFails = useCallback(
    (reason: string) => {
      const transReason = reason?.indexOf('User has cancelled') > -1 ? 'Оплата отменена пользователем' : reason;
      pushNotification({
        type: 'error',
        content: transReason,
      });
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const addressForField = useMemo(() => {
    if (address) {
      if (address.includes('Россия,')) {
        return address.replace('Россия, ', '');
      }
      return address;
    }

    return '';
  }, [address]);

  const initialValues = useMemo(
    () => ({
      phoneNumber: ' ',
    }),
    [],
  );

  return (
    <Form
      onSubmit={submitHandler}
      initialValues={initialValues}
      keepDirtyOnReinitialize
      render={({
        handleSubmit, pristine, valid, values, form,
      }) => {
        const promocodeBlock = values.promocodeToggle
          && (promocodeValidIsPending
            || promocodeValidIsError
            || (!isNil(promocodeData) && !promocodeData.wasAccepted));

        const disableOrderBtn = checkoutIsPending || !valid || promocodeBlock || !values.agreement;
        console.log(values);

        return (
          <div>
            <form id="сheckoutForm" onSubmit={handleSubmit} className="checkout-form">
              <div className="pad-h">
                <TitleGroup
                  title={values?.name}
                />
                <div className="checkout-form__row" onSubmit={handleSubmit}>
                  <div className="checkout-form__col">
                    <InputField
                      theme="grey"
                      name="name"
                      label="Название"
                      required
                    />
                    <DeliveryGroup formDeliveryType={values.objectType} />
                    <SelectField key={`select_${pristine}`} name="selectedTopic" required>
                      {selectOptions}
                    </SelectField>
                    <QuestionsGroup values={values} />
                  </div>
                  <div className="checkout-form__buttons">
                    <div className="checkout-form__button">
                      <Button onClick={() => {
                        pushNotification({
                          type: 'success',
                          content: 'Объект сохранен!',
                        });
                      }}
                      >
                        Сохранить
                      </Button>
                    </div>
                    <div className="checkout-form__button">
                      <Button disabled onClick={() => {}}>
                        Калькуляторы
                      </Button>
                    </div>
                    <div className="checkout-form__button">
                    <Button disabled onClick={() => {}}>
                      Удалить
                    </Button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        );
      }}
    />
  );
};

export default memo(CheckoutFormComponent);
