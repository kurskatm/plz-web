// Modules
import React, { FC, memo } from 'react';
// Types
import { TFormTitleProps } from './types';

const FormTitleComponent: FC<TFormTitleProps> = ({ title = 'Новый объект' }) => <div className="checkout-form__title">{title}</div>;

export default memo(FormTitleComponent);
