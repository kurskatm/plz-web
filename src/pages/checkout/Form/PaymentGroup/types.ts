export type TPaymentGroupProps = {
  activeType: string;
  total: number;
  paymentTypes: number;
};
