// Modules
import React, { FC, memo, useState, useCallback, useMemo } from 'react';
// Components
import { RadioField, Input } from 'chibbis-ui-kit';
import { Field, FieldInputProps, FieldMetaState } from 'react-final-form';
// Lib
import { getPaymentTypes } from '@lib/restaurants/payment-types';
// Type
import type { TPaymentGroupProps } from './types';

// eslint-disable-next-line no-useless-escape
const NumericMask = new RegExp(/^[1-9]{1}[0-9]*$/);
const capitalizeFirstLetter = (str: string) => str?.charAt(0)?.toUpperCase() + str?.slice(1);

const PaymentGroupFormComponent: FC<TPaymentGroupProps> = ({ activeType, total, paymentTypes }) => {
  const [exchange, setExchange] = useState<string>('');
  const payments = getPaymentTypes(paymentTypes);

  const renderPaymentTypes = useMemo(
    () =>
      payments ? (
        <div className="payment-group__list">
          {payments.map((pay) => (
            <RadioField
              key={`${pay?.type}_${pay.value}`}
              name="paymentMethod"
              className={`payment-radio-${pay?.value}`}
              value={pay?.value}
              view={capitalizeFirstLetter(pay?.name)}
            />
          ))}
        </div>
      ) : (
        <div className="payment-group__list">
          <RadioField
            name="paymentMethod"
            className="payment-radio-byCardOnline"
            value="byCardOnline"
            view="Картой онлайн"
            disbaled
          />
          <RadioField
            name="paymentMethod"
            className="payment-radio-byCardToCourier"
            value="byCardToCourier"
            view="Картой курьеру"
            disbaled
          />
          <RadioField
            name="paymentMethod"
            className="payment-radio-cash"
            value="cash"
            view="Наличными"
            disbaled
          />
        </div>
      ),
    [payments],
  );
  const onExchangeChange = useCallback(
    (event: Event, input: FieldInputProps<string, HTMLElement>) => {
      const { value } = event.target as HTMLInputElement;
      if ((value.length && NumericMask.test(value)) || !value.length) {
        setExchange(value);
        input.onChange(event);
      }
    },
    [],
  );

  const getValidation = useCallback(
    (shortChange) => {
      if (shortChange && shortChange?.length && Number(shortChange) < total) {
        return {
          shortChange: 'Значение не может быть меньше суммы заказа',
        };
      }

      return undefined;
    },
    [total],
  );

  const renderError = useMemo(
    () => (meta: FieldMetaState<string>) => {
      if ((meta.modified || !meta.pristine) && meta.error) {
        return (
          <div className="payment-group__cash-exchange-error">
            <div className="payment-group__cash-exchange-error_inner">{meta.error.shortChange}</div>
          </div>
        );
      }

      return null;
    },
    [],
  );

  return (
    <div className={`payment-group is-${activeType}`}>
      {renderPaymentTypes}
      {activeType === 'byCardOnline' ? (
        <div className="payment-group__info">
          <div className="payment-group__text">
            <div className="payment-group__reserve-money-icon" />
            Мы зарезервируем сумму заказа на вашей карте и спишем после доставки
          </div>
          <div className="payment-group__text">
            <div className="payment-group__cancel-order-icon" />
            При отмене заказа деньги моментально вернутся
          </div>
        </div>
      ) : null}
      {activeType === 'cash' ? (
        <div className="payment-group__cash-exchange">
          <Field
            name="shortChange"
            render={({ input, meta }) => (
              <>
                <Input
                  theme="grey"
                  label="Сдача с"
                  name="shortChange"
                  value={exchange}
                  onChange={(event: Event) => onExchangeChange(event, input)}
                  clear
                  inputmode="numeric"
                />
                {!!meta.error && renderError(meta)}
              </>
            )}
            validate={getValidation}
          />
        </div>
      ) : null}
    </div>
  );
};

export default memo(PaymentGroupFormComponent);
