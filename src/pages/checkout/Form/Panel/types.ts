export interface TPanelProps {
  total: number;
  discount: number;
  deliveryCost: number;
  paymentMethod: string;
  loading: boolean;
  disabled: boolean;
  disabledApple: boolean;
  onClick: () => void;
}
