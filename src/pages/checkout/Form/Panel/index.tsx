// Modules
import React, { FC, memo } from 'react';
import { useMediaQuery } from 'react-responsive';
import isNil from 'lodash/isNil';
// Components
import { Button } from 'chibbis-ui-kit';
// Types
import { TPanelProps } from './types';

const PanelComponent: FC<TPanelProps> = ({
  total,
  discount,
  disabled,
  disabledApple,
  loading,
  onClick,
  paymentMethod,
}) => {
  const isMobileOrTablet = useMediaQuery({
    maxWidth: 768,
  });

  const isTablet = useMediaQuery({
    maxWidth: 1024,
  });

  return (
    <div className="checkout-form__button">
      {!isNil(total) && !isNil(discount) && isTablet && (
        <div className="checkout-form__button__total">
          <div className="checkout-form__button__total__text">Итого к оплате</div>
          {total - discount !== total && (
            <div className="checkout-form__button__total__num">{` ${total} ₽ `}</div>
          )}
          <div className="checkout-form__button__total__num-with-discount">
            {`${total - discount} ₽`}
          </div>
        </div>
      )}
      {paymentMethod !== 'apple' ? (
        <Button loading={loading} disabled={disabled} onClick={onClick}>
          {isMobileOrTablet ? 'оплатить заказ' : 'оформить заказ'}
        </Button>
      ) : (
        <Button className="btn-apple" disabled={disabledApple}>
          Купить с ApplePay
        </Button>
      )}
    </div>
  );
};

export default memo(PanelComponent);
