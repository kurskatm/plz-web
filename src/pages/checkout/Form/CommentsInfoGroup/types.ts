// Main types
import { FormApi } from 'final-form';
import { TCheckoutFormVals } from '../../types';

export interface TCommentsInfoGroupProps {
  commentsChecked: boolean;
  disabled: boolean;
  formDeliveryType: string;
  pristine: boolean;
  timeChecked: boolean;
  dontCallChecked: boolean;
  timeList: Record<string, string>[];
  form: FormApi<TCheckoutFormVals, Partial<TCheckoutFormVals>>;
}
