// Modules
import React, { FC, memo } from 'react';
// Componetns
import { InputField } from 'chibbis-ui-kit';
import ToggleField from '../ToggleField';
import PromocodeField from '../PromocodeGroup';
// Types
import { TCommentsInfoGroupProps } from './types';

const CommentsInfoGroupComponent: FC<TCommentsInfoGroupProps> = ({
  commentsChecked,
  disabled,
  dontCallChecked,
  form,
}) => (
  <div className="checkout-form__field">
    <ToggleField name="commentToggle" title="Комментарий" checked={commentsChecked}>
      <InputField autoFocus disabled={disabled} theme="grey" name="comment" maxLength={300} />
    </ToggleField>
    <PromocodeField form={form} />
    <ToggleField name="dontCall" title="Не перезванивать мне" checked={dontCallChecked} />
  </div>
);

export default memo(CommentsInfoGroupComponent);
