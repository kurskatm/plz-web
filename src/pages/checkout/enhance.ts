// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { checkoutSend } from '@actions/checkout';
import { shopingCartReset } from '@actions/shoping-cart';
import { shopingCartSave, shopingCartSaveResetUi } from '@actions/shoping-cart/save';
import { authModalShow } from '@actions/auth';
import { restaurantStoresFetch } from '@actions/restaurant/stores';
import { restaurantCardFetch } from '@actions/restaurant/main/card';
import { changeAddressModalShow } from '@actions/checkout/modals';
import { profileResetAllUiModels } from '@actions/profile';
import { pushNotification } from '@actions/notifications';
import { promocodeFetch, promocodeReset } from '@actions/promocode';
import { showShoppingCardModal } from '@actions/restaurant/modal';
import { fTypesFetch } from '@/actions/funcTypes';
import { pushNotification } from '@actions/notifications';

// Selectors
// import { shopingCartStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/shoping-cart';

const mapDispatchToProps = {
  checkoutSend,
  restaurantCardFetch,
  shopingCartReset,
  shopingCartSave,
  shopingCartSaveResetUi,
  restaurantStoresFetch,
  changeAddressModalShow,
  profileResetAllUiModels,
  pushNotification,
  promocodeFetch,
  promocodeReset,
  authModalShow,
  showShoppingCardModal,
  fTypesFetch,
};
const withConnect = connect(null, mapDispatchToProps);

export const enhance = compose(withConnect);
