/* eslint-disable @typescript-eslint/no-explicit-any */
import { TInfoModel } from '@models/profile/types';
import { TChangeAddressModalShow } from '@actions/checkout/modals/action-types';
import { TProfileResetAllUiModels } from '@actions/profile/action-types';
import { TPushNotification } from '@actions/notifications/action-types';
import { TPromocodeReset } from '@actions/promocode/action-types';
import { TPromocodeResult } from '@/type/promocode';
import { TCitiesItem } from '@/type/cities';
import { TNewGeocodeData } from '@type/new-geocode';
import { TShowShoppingCardModal } from '@actions/restaurant/modal/action-types';
import { TShopingCartSaveResetUi } from '@actions/shoping-cart/save/action-types';

export interface TCheckoutProps {
  city: TCitiesItem;
  orderId?: string;
  // profile: TProfileModel;
  info: TInfoModel;
  auth: string;
  address: string;
  geocode: TNewGeocodeData;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  shopingCart: Record<string, any>;
  checkoutSend: () => void;
  shopingCartReset: () => void;
  shopingCartSave: (data: Record<string, string>) => void;
  checkoutStatus: string;
  checkoutData: Record<string, string>;
  checkoutIsPending?: boolean;
  authModalShow: (data: boolean) => void;
  // restaurantStoresFetch: (restId: string) => void;
  // restaurantCardFetch: (data: Record<string, string>) => void;
  // stores: TRestaurantStore[];
  shopingCartSaveSuccess: boolean;
  shopingCartSaveError: boolean;
  changeAddressModalShow: TChangeAddressModalShow;
  checkoutIsError: boolean;
  checkoutOptionalError: { type: string; message: string };
  showCheckoutAddressModal: boolean;
  profileResetAllUiModels: TProfileResetAllUiModels;
  pushNotification: TPushNotification;
  resultCoast: number;
  shopingCartFetchSuccess: boolean;
  shopingCartFetchError: boolean;
  // paymentTypes: number;
  promocodeReset: TPromocodeReset;

  promocodeValidIsPending: boolean;
  promocodeValidIsError: boolean;
  promocodeData: TPromocodeResult;
  showModal: boolean;
  showShoppingCardModal: TShowShoppingCardModal;
  shopingCartSaveResetUi: TShopingCartSaveResetUi;
}

export interface TDeliveryInfo {
  deliveryType: string;
  deliveryDetails: TDeliveryDetails;
  shortChange?: string;
  selfPickupStoreId?: string;
  selfPickupStoreAddress?: string;
  deliverToTime?: string;
  personsCount: number;
}

export interface TDeliveryDetails {
  cityId: string;
  address?: string;
  street?: string;
  house?: string;
  building?: string;
  porch?: string;
  floor?: string;
  flat?: string;
  coordinates: TDeliveryCoordinates;
}

export type TDeliveryCoordinates = {
  latitude: number;
  longitude: number;
};

export interface TDeliveryPaymentsDetails {
  paymentMethod: string;
  paymentToken?: string;
  cardHolderName?: string;
  successReturnUrl: string;
  failureReturnUrl: string;
}

export interface TDeliveryMarketingDetails {
  customerSourceType: number;
  customerVisitStartingPageUrl?: string;
}

export interface TDeliveryCheckoutData {
  userId?: string;
  cardId?: string;
  comment?: string;
  promoCodeId?: string;
  deliveryInfo: TDeliveryInfo;
  paymentToken?: string;
  paymentMethod?: string;
  paymentDetails: TDeliveryPaymentsDetails;
  cardHolderName?: string;
  phoneNumber: string;
  marketingDetails: TDeliveryMarketingDetails;
  noCallingBack: boolean;
}

export type TCheckoutFormVals = {
  cityId: string;
  dontCall: boolean;
  address: string;
  house: string;
  street: string;
  lat: number;
  lng: number;
  phoneNumber: string;
  deliveryType: string;
  paymentType: string;
  comment: string;
  selectedTimeTo: string;
  selectedTimeFrom: string;
  promocode: string;
  floor: string;
  flat: string;
  porch: string;
  paymentToken?: string;
  paymentMethod: string;
  cardHolderName?: string;
  commentToggle?: boolean;
  deliveryTimeToggle?: boolean;
  promocodeToggle?: boolean;
  pickupAddress?: string;
  deliveryToTime: Record<string, string>;
  personsCount?: number;
  shortChange?: string;
  promoCodeId?: string;
  agreement: boolean;
};
