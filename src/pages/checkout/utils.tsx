import { isMobile } from 'react-device-detect';

import { getFormatedCurrentDate } from '@/lib/restaurants/getHoursAndMinutes';
import {
  TDeliveryCheckoutData,
  TCheckoutFormVals,
  TDeliveryInfo,
  TDeliveryDetails,
  TDeliveryMarketingDetails,
} from './types';

export const getDeliveryDetails = (data: TCheckoutFormVals): TDeliveryDetails => ({
  cityId: data.cityId,
  house: data.house,
  street: data.street,
  porch: data.porch,
  floor: data.floor,
  flat: data.flat,
  coordinates: {
    latitude: data.lat,
    longitude: data.lng,
  },
});

export const getDeliveryInfo = (data: TCheckoutFormVals): TDeliveryInfo => ({
  deliveryType: data.deliveryType,
  selfPickupStoreId: data.pickupAddress,
  selfPickupStoreAddress: null,
  shortChange: data.shortChange?.length ? data.shortChange : null,
  deliveryDetails: data.deliveryType === 'delivery' ? getDeliveryDetails(data) : null,
  deliverToTime: data?.deliveryToTime ? getFormatedCurrentDate(data?.deliveryToTime?.id) : null,
  personsCount: data.personsCount || 1,
});

export const getPaymentsDetails = (data: TCheckoutFormVals): TDeliveryPaymentsDetails => ({
  paymentMethod: data.paymentMethod,
  successReturnUrl: `https://${window.location.host}/checkout/success?orderId={orderId}`,
  failureReturnUrl: `${window.location.href}?payment=fail`,
});

export const getMarketingDetails = (): TDeliveryMarketingDetails => ({
  customerSourceType: isMobile ? 3 : 4,
});

export const deliveryCheckoutData = (data: TCheckoutFormVals): TDeliveryCheckoutData => ({
  comment: data.comment,
  promoCodeId: data.promoCodeId,
  deliveryInfo: getDeliveryInfo(data),
  paymentToken: data.paymentToken,
  paymentMethod: data.paymentMethod,
  cardHolderName: data.cardHolderName,
  phoneNumber: data.phoneNumber.replace(/[^\d]/g, ''),
  paymentDetails: getPaymentsDetails(data),
  marketingDetails: getMarketingDetails(),
  noCallingBack: data.dontCall || false,
});
export interface TDeliveryPaymentsDetails {
  paymentMethod: string;
  paymentToken?: string;
  cardHolderName?: string;
  successReturnUrl: string;
  failureReturnUrl: string;
}
