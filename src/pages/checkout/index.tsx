// Modules
import React, { FC, memo, useEffect, useState } from 'react';
import loadable from '@loadable/component';
import isNumber from 'lodash/isNumber';
import { useHistory } from 'react-router';
import classnames from 'classnames';
import { useMediaQuery } from 'react-responsive';
// Utils
import { Consts } from '@utils';
import { trackEvent } from '@utils/analytics';
// Componentns
import {
  getHoursAndMinutes,
  roundToHalfAnHour,
  currTimeToTicks,
} from '@lib/restaurants/getHoursAndMinutes';
import RestaurantCloseTimer from '@components/RestaurantCloseTimer';
import CheckoutChangeAddress from '@components/CheckoutAddressModal';
import CheckoutBasketModal from '@components/CheckoutBasketModal';
import CheckoutPromocodeModal from '@components/CheckoutPromocodeModal';
import BlackModalBase from '@components/BlackModalBase';
import ObjectForm from './Form';
// Enhance
import { enhance } from './enhance';
// Types
import { TCheckoutProps } from './types';

const HALF_HOUR = 18000000000;
const DAY = 864000000000;
const { CITIES } = Consts.ROUTES;
const Header = loadable(() => import('@components/Header'));
const ShopingCart = loadable(() => import('@components/ShopingCart'));

const CheckoutComponent: FC<TCheckoutProps> = ({
  city,
  orderId,
  shopingCart,
  auth,
  info,
  address,
  geocode,
  checkoutSend,
  shopingCartReset,
  checkoutStatus,
  checkoutData,
  shopingCartSave,
  checkoutIsPending,
  // restaurantStoresFetch,
  // restaurantCardFetch,
  // stores,
  shopingCartSaveError,
  checkoutIsError,
  checkoutOptionalError,
  showCheckoutAddressModal,
  // paymentTypes,
  profileResetAllUiModels,
  pushNotification,
  promocodeValidIsPending,
  promocodeValidIsError,
  promocodeReset,
  promocodeData,
  authModalShow,
  showModal,
  showShoppingCardModal,
  shopingCartSaveResetUi,
  fTypesFetch,
}) => {
  const isTablet = useMediaQuery({
    minWidth: 769,
    maxWidth: 1024,
  });

  // добавляем cloudpayments скрипт
  // useEffect(() => {
  //   const script = document.createElement('script');

  //   script.src = 'https://widget.cloudpayments.ru/bundles/cloudpayments.js';
  //   script.async = true;
  //   document.body.appendChild(script);

  //   return () => {
  //     document.body.removeChild(script);
  //   };
  // }, []);

  useEffect(
    () => {
      fTypesFetch();
      if (checkoutIsError) {
        pushNotification({
          type: 'error',
          content: 'Не удалось оформить заказ',
        });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [checkoutIsError],
  );
  return (
    <div className="checkout-page grid-wrapper">
      <header className="grid-wrapper__header">
        <Header goBackText="На главную" goBackLink="/" noAddress noMenu title="Объект" />
      </header>
      <main className="grid-wrapper__main">
        <div className="checkout-page__content-wrap">
          <div className="checkout-page__form">
            <CheckoutChangeAddress />
            <ObjectForm pushNotification={pushNotification} />
          </div>
        </div>
      </main>
    </div>
  );
};
// @ts-ignore
export default memo(enhance(CheckoutComponent));
