// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import {
  restaurantReviewsFetch,
  restaurantReviewsReset,
} from '@actions/restaurant/reviews/current';
import { pushNotification } from '@actions/notifications';
// Selectors
import { restaurantReviewsStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/restaurant/reviews';

const mapDispatchToProps = {
  restaurantReviewsFetch,
  restaurantReviewsReset,
  pushNotification,
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
