// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import {
  profileGetLastOrderWithoutReview,
  profileGetLastOrderWithoutReviewResetData,
} from '@actions/profile/orders';
import { profileShowOrdersReviewModal } from '@actions/profile';
// Selectors
import { restaurantLastOrderWithoutReviewStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/restaurant/reviews';

const mapDispatchToProps = {
  profileGetLastOrderWithoutReview,
  profileShowOrdersReviewModal,
  profileGetLastOrderWithoutReviewResetData,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
