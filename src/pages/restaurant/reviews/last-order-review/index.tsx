// Modules
import React, { FC, memo, useEffect, useMemo, useCallback, useState, useRef } from 'react';
import isNil from 'lodash/isNil';
import { useMediaQuery } from 'react-responsive';
// Components
import { ScoresIcon } from 'chibbis-ui-kit';
import OrderReviewModal from '@components/OrderModal';
// Enhance
import { enhance } from './enhance';
// Types
import { TRestaurantLastOrderReviewProps } from './types';

const RestaurantLastOrderReviewComponent: FC<TRestaurantLastOrderReviewProps> = ({
  order,
  orderIsIdle,
  profile,
  restaurantId,
  profileGetLastOrderWithoutReview,
  profileGetLastOrderWithoutReviewResetData,
  profileShowOrdersReviewModal,
  reviewAddIsSuccess,
  reviewAddList,
}) => {
  const isVerticalMobile = useMediaQuery({
    maxWidth: 530,
  });

  const prevReviewsListLength = useRef<number>();

  const [isReviewSend, setIsReviewSend] = useState<boolean>(false);
  const [wasModalOpened, setWasModalOpened] = useState<boolean>(false);

  useEffect(
    () => {
      prevReviewsListLength.current = reviewAddList.length;
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  useEffect(
    () => {
      if (!isNil(profile.auth) && orderIsIdle && restaurantId && restaurantId.length) {
        profileGetLastOrderWithoutReview({
          restaurantId,
        });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [profile.auth, restaurantId, orderIsIdle],
  );

  useEffect(() => {
    if (
      prevReviewsListLength.current !== reviewAddList.length &&
      reviewAddIsSuccess &&
      wasModalOpened
    ) {
      setIsReviewSend(true);
    }
  }, [reviewAddIsSuccess, reviewAddList.length, wasModalOpened]);

  useEffect(
    () => () => profileGetLastOrderWithoutReviewResetData(),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const onClick = useCallback(
    (e) => {
      e.stopPropagation();
      profileShowOrdersReviewModal({
        showModal: true,
        orderId: '',
      });
      setWasModalOpened(true);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const renderContent = useMemo(() => {
    if (!isNil(profile.auth) && !isNil(order) && !isReviewSend) {
      return (
        <div className="restaurant-reviews__last-order-inner" onClick={onClick} role="presentation">
          <span>
            Оцените предыдущий заказ
            {isVerticalMobile ? <br /> : ' '}и получите&nbsp;
            <span className="restaurant-reviews__last-order-bonus">
              <ScoresIcon />
              100 баллов
            </span>
          </span>
        </div>
      );
    }

    return null;
  }, [order, profile.auth, onClick, isVerticalMobile, isReviewSend]);

  return (
    <>
      {renderContent}
      <OrderReviewModal lastOrderModal />
    </>
  );
};

const RestaurantLastOrderReviewMemo = memo(RestaurantLastOrderReviewComponent);
export default enhance(RestaurantLastOrderReviewMemo);
