// Main types
import { TProfileModel } from '@models/profile/types';
import { TUserOrder } from '@type/profile';
import {
  TProfileGetLastOrderWithoutReview,
  TProfileGetLastOrderWithoutReviewResetData,
} from '@actions/profile/orders/action-types';
import { TProfileShowOrdersReviewModal } from '@actions/profile/action-types';

export interface TRestaurantLastOrderReviewProps {
  order: TUserOrder;
  orderIsIdle: boolean;
  profile: TProfileModel;
  restaurantId: string;
  profileGetLastOrderWithoutReview: TProfileGetLastOrderWithoutReview;
  profileGetLastOrderWithoutReviewResetData: TProfileGetLastOrderWithoutReviewResetData;
  profileShowOrdersReviewModal: TProfileShowOrdersReviewModal;
  reviewAddList: string[];
  reviewAddIsSuccess: boolean;
}
