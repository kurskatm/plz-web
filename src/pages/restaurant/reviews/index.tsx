// Modules
import React, { FC, memo, useCallback, useEffect, useRef, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { parse, stringify } from 'qs';
// Modules Components
import { LoadingSpinner } from 'chibbis-ui-kit';
// Components
import ReviewsList from './reviews-list';
import ReviewsPanel from './panel';
import LastOrderWithoutReview from './last-order-review';
// Config
import { options, optionsParse } from './config/options';
import { getParamReviewRef } from './config/param-review-ref';
import { getReviews } from './config/get-reviews';
import { setReviews } from './config/set-reviews';
// Enhance
import { enhance } from './enhance';
// Types
import { TRestaurantReviewsProps } from './types';

const RestaurantReviewsComponent: FC<TRestaurantReviewsProps> = ({
  restaurantCard,
  reviews,
  reviewsIsIdle,
  reviewsIsPending,
  reviewsIsSuccess,
  reviewsIsError,
  reviewsPagination,
  restaurantReviewsFetch,
  restaurantReviewsReset,
  pushNotification,
}) => {
  useEffect(
    () => {
      if (reviewsIsError) {
        pushNotification({
          type: 'error',
          content: 'Не удалось получить отзывы',
        });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [reviewsIsError],
  );

  const { replace, location } = useHistory();

  const mountRef = useRef<boolean>(false);
  const paramReviewRef = useRef<boolean>(getParamReviewRef(location));

  const getReviewsCallback = useCallback(
    () => getReviews(location),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [location.search],
  );

  const setReviewsCallback = useCallback(
    () => setReviews(location),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [location.search],
  );

  const [active, setActive] = useState<string>(getReviewsCallback() || 'all');

  useEffect(
    () => {
      const search = parse(location.search, optionsParse);

      if (!mountRef.current) {
        mountRef.current = true;
      }

      if (!search?.reviews) {
        const data = { reviews: active };
        const params = stringify(data, options);
        replace({ search: params });
        paramReviewRef.current = true;
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  useEffect(
    () => {
      if (paramReviewRef.current && mountRef.current) {
        restaurantReviewsReset();
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [active],
  );

  const fetchReviews = useCallback(() => {
    if (!reviewsIsPending && reviewsPagination.hasMore && restaurantCard) {
      restaurantReviewsFetch({
        liked: setReviewsCallback(),
        restaurantId: restaurantCard?.id,
        pageNumber: reviewsPagination.page,
        takeCount: reviewsPagination.limit,
      });
    }
  }, [
    restaurantCard,
    restaurantReviewsFetch,
    reviewsIsPending,
    reviewsPagination,
    setReviewsCallback,
  ]);

  useEffect(() => {
    if (paramReviewRef.current && reviewsIsIdle) {
      fetchReviews();
    }
  }, [reviewsIsIdle, paramReviewRef, fetchReviews]);

  return (
    <div className="restaurant-reviews">
      <div className="restaurant-reviews__last-order">
        <LastOrderWithoutReview />
      </div>
      <div className="restaurant-reviews-panel">
        <ReviewsPanel active={active} setActive={setActive} />
      </div>

      {(!!reviews.length || reviewsIsPending) && (
        <div className="restaurant-reviews-list__wrap">
          <ReviewsList list={reviews} fetchReviews={fetchReviews} />
          {reviewsIsPending && (
            <LoadingSpinner
              className="restaurant-reviews-list__spinner"
              size="big"
              color="orange"
            />
          )}
        </div>
      )}

      {reviewsIsSuccess && !reviews.length && (
        <div>
          <div className="restaurant-empty-reviews-list" />
          <div className="restaurant-empty-reviews-list-text">
            Сделайте заказ и оставьте отзыв первым
          </div>
        </div>
      )}
    </div>
  );
};

const RestaurantReviewsMemo = memo(RestaurantReviewsComponent);
export default enhance(RestaurantReviewsMemo);
