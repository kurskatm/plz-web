// Main Types
import {
  TRestaurantCardReviewsFetch,
  TRestaurantCardReviewsReset,
} from '@actions/restaurant/reviews/current/action-types';
import { TPaginationObject } from '@models/pagination/types';
import { TRestaurantCard } from '@type/restaurants/restaurant-card';
import { TReviewList } from '@type/restaurants/reviews';
import { TPushNotification } from '@/actions/notifications/action-types';

export interface TRestaurantReviewsProps {
  restaurantCard: TRestaurantCard;
  reviews: TReviewList;
  reviewsIsIdle: boolean;
  reviewsIsSuccess: boolean;
  reviewsIsPending: boolean;
  reviewsIsError: boolean;
  reviewsPagination: TPaginationObject;
  restaurantReviewsFetch: TRestaurantCardReviewsFetch;
  restaurantReviewsReset: TRestaurantCardReviewsReset;
  pushNotification: TPushNotification;
}
