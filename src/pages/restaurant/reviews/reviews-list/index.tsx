// Modules
import loadable from '@loadable/component';
import React, { FC, memo, useMemo } from 'react';
// Main Types
import { TReview } from '@type/restaurants/reviews';
// Component
import ContentViewer from '../content-viewer';
// Types
import { TReviewsListProps } from './types';

const Review = loadable(() => import('@components/Review'));

const ReviewWrapper: FC = ({ children }) => (
  <div className="restaurants-reviews__list">{children}</div>
);

const ReviewsListComponent: FC<TReviewsListProps> = ({ list, fetchReviews }) => {
  const renderItem = useMemo(() => (item: TReview) => <Review key={item.id} item={item} />, []);

  return (
    <div className="rest-reviews-list">
      <ContentViewer
        list={list}
        renderItem={renderItem}
        wrapper={ReviewWrapper}
        fetchReviews={fetchReviews}
      />
    </div>
  );
};

export default memo(ReviewsListComponent);
