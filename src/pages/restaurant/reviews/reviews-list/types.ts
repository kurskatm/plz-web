// Main Types
import { TReviewList } from '@type/restaurants/reviews';

export interface TReviewsListProps {
  list: TReviewList;
  fetchReviews: () => void;
}
