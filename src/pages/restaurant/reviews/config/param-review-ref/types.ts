// Modules Types
import { Location } from 'history';

export type TGetParamReviewRef = (location: Location) => boolean;
