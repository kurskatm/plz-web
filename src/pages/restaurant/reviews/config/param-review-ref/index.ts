// Modules
import { parse } from 'qs';
import isNil from 'lodash/isNil';
// Config
import { optionsParse } from '../options';
// Types
import { TGetParamReviewRef } from './types';

export const getParamReviewRef: TGetParamReviewRef = (location) => {
  const reviews = parse(location.search, optionsParse)?.reviews as string | null;
  return !isNil(reviews);
};
