// Modules Types
import { Location } from 'history';

export type TGetReviews = (location: Location) => string | null;
