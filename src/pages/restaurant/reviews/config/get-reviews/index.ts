// Modules
import { parse } from 'qs';
import isNil from 'lodash/isNil';
// Config
import { optionsParse } from '../options';
// Types
import { TGetReviews } from './types';

export const getReviews: TGetReviews = (location) => {
  const search = parse(location.search, optionsParse);
  return isNil(search?.reviews) ? null : search.reviews.toString();
};
