// Config
import { getReviews } from '../get-reviews';
// Types
import { TSetReviews } from './types';

export const setReviews: TSetReviews = (location) => {
  const reviewsParam = getReviews(location);

  switch (reviewsParam) {
    case 'positive': {
      return true;
    }

    case 'negative': {
      return false;
    }

    case 'all':
    default: {
      return null;
    }
  }
};
