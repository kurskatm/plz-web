// Modules Types
import { Location } from 'history';

export type TSetReviews = (location: Location) => boolean | null;
