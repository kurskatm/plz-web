export const optionsParse = {
  ignoreQueryPrefix: true,
};

export const options = {
  addQueryPrefix: true,
  skipNulls: true,
};
