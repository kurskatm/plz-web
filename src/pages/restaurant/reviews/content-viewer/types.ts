/* eslint-disable @typescript-eslint/no-explicit-any */

import { TContent } from '@/type/content';
import { TReview, TReviewList } from '@/type/restaurants/reviews';

export interface TContentViewerProps {
  list: TReviewList;
  renderItem: (item: TReview) => JSX.Element;
  wrapper: TContent;
  fetchReviews: () => void;
}
