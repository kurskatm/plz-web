// Types
import { TGetScrollPercent } from './types';

export const getScrollPercent: TGetScrollPercent = ({ isDesktop, isTablet, isMobile }) => {
  if (isMobile) {
    return 50;
  }

  if (isTablet) {
    return 40;
  }

  if (isDesktop) {
    return 30;
  }

  return 20;
};
