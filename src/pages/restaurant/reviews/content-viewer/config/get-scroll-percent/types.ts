interface TGetScrollPercentArgs {
  isDesktop: boolean;
  isTablet: boolean;
  isMobile: boolean;
}

export type TGetScrollPercent = (data: TGetScrollPercentArgs) => number;
