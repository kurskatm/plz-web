// Types
export interface TResponsive {
  desktop: number;
  tablet: number;
  mobile: number;
}

interface TGetQuantityArgs {
  isDesktop: boolean;
  isTablet: boolean;
  isMobile: boolean;
  responsive: TResponsive;
}

export type TGetQuantity = (data: TGetQuantityArgs) => number | null;
