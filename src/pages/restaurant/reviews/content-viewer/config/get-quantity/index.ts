// Types
import { TGetQuantity } from './types';

export const getQuantity: TGetQuantity = ({ isDesktop, isTablet, isMobile, responsive }) => {
  if (isMobile) {
    return responsive.mobile;
  }

  if (isTablet) {
    return responsive.tablet;
  }

  if (isDesktop) {
    return responsive.desktop;
  }

  return null;
};
