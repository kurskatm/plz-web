// Modules
import React, { FC, memo, useEffect, useCallback } from 'react';
import { useMediaQuery } from 'react-responsive';
import { getScrollPercent } from './config/get-scroll-percent';
// Types
import { TContentViewerProps } from './types';

const ContentViewerComponent: FC<TContentViewerProps> = ({
  list,
  renderItem,
  wrapper: Wrapper,
  fetchReviews,
}) => {
  const isDesktop = useMediaQuery({
    minWidth: 1025,
  });

  const isTablet = useMediaQuery({
    minWidth: 768,
    maxWidth: 1024,
  });

  const isMobile = useMediaQuery({
    maxWidth: 767,
  });

  const scrollEvent = useCallback(
    ({ target }: Event) => {
      const { scrollingElement } = target as Document;
      const { scrollHeight, clientHeight, scrollTop } = scrollingElement;
      const maxScroll = scrollHeight - clientHeight;
      const scrollPercent = getScrollPercent({
        isDesktop,
        isTablet,
        isMobile,
      });
      const scrollPercentFetch = (scrollHeight / 100) * scrollPercent;
      const isCalledFetch = scrollTop >= maxScroll - scrollPercentFetch;

      if (isCalledFetch) {
        fetchReviews();
      }
    },
    [fetchReviews, isDesktop, isMobile, isTablet],
  );

  useEffect(() => {
    if (fetchReviews) {
      document.addEventListener('scroll', scrollEvent);

      return () => {
        document.removeEventListener('scroll', scrollEvent);
      };
    }

    return undefined;
  }, [fetchReviews, scrollEvent]);

  return (
    <div className="review-content-viewer">
      <div className="review-content-viewer__content">
        {/* @ts-ignore */}
        <Wrapper>{list.map(renderItem)}</Wrapper>
      </div>
    </div>
  );
};

ContentViewerComponent.defaultProps = {
  list: [],
};

export default memo(ContentViewerComponent);
