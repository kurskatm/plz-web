// Modules
import React, { FC, memo, useCallback, useMemo } from 'react';
import { useHistory } from 'react-router-dom';
import classNames from 'classnames';
import { stringify } from 'qs';
// Config
import { panelConfig } from './config/panel-tabs';
// Types
import { TPanelConfigItem } from './config/panel-tabs/types';
import { TReviewsPanelProps } from './types';

const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

const ReviewsPanelComponent: FC<TReviewsPanelProps> = ({ active, setActive }) => {
  const { push } = useHistory();

  const getStylesItem = useCallback(
    (id: string) => classNames('reviews-panel-item', active === id && 'reviews-panel-item-active'),
    [active],
  );

  const onClick = useCallback(
    (id) => () => {
      if (id !== active) {
        const data = { reviews: id };
        const params = stringify(data, options);

        setActive(id);
        push({ search: params });
      }
    },
    [active, push, setActive],
  );

  const renderItem = useMemo(
    () => ({ id, name }: TPanelConfigItem) => (
      <div key={id} role="presentation" className={getStylesItem(id)} onClick={onClick(id)}>
        {name}
      </div>
    ),
    [getStylesItem, onClick],
  );

  return <div className="reviews-panel">{panelConfig.map(renderItem)}</div>;
};

export default memo(ReviewsPanelComponent);
