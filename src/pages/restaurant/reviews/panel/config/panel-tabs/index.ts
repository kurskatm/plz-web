// Types
import { TPanelConfig } from './types';

export const panelConfig: TPanelConfig = [
  {
    id: 'all',
    name: 'Все',
  },

  {
    id: 'positive',
    name: 'Положительные',
  },

  {
    id: 'negative',
    name: 'Отрицательные',
  },
];
