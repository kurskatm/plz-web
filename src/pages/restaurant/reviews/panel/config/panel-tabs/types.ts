export interface TPanelConfigItem {
  id: string;
  name: string;
}

export type TPanelConfig = TPanelConfigItem[];
