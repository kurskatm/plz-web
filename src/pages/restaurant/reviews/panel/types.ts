export interface TReviewsPanelProps {
  active: string;
  setActive: (active: string) => void;
}
