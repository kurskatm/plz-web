// Main Type
import { TRestaurantCardMainReset } from '@actions/restaurant/main/main/action-types';
import {
  TRestaurantCardIdFetch,
  TRestaurantCardIdReset,
} from '@actions/restaurant/main/id/action-types';
import { TChangeAddressModalShow } from '@actions/checkout/modals/action-types';
import { TRestaurantCardFetch } from '@actions/restaurant/main/card/action-types';
import { TRestaurantCard } from '@type/restaurants/restaurant-card';
import { TProfileModel } from '@/models/profile/types';
import { TProfileResetAllUiModels } from '@actions/profile/action-types';
import { TPushNotification } from '@actions/notifications/action-types';
import { TRestaurantCardReviewsResetReset } from '@/actions/restaurant/reviews/main/action-types';
import { TRestaurantCardInfoResetReset } from '@/actions/restaurant/info/main/action-types';
import { TRestaurantCardReviewsReset } from '@/actions/restaurant/reviews/current/action-types';
import { TShowShoppingCardModal } from '@actions/restaurant/modal/action-types';
import { TShopingCartSaveResetUi } from '@actions/shoping-cart/save/action-types';

export interface TParamsRouter {
  cityUrl: string | null;
  restaurantName: string | null;
  tabName: string | null;
}

export interface TRestaurantMainProps {
  cityId: string | null;
  profile: TProfileModel;
  getRestaurantCard: TRestaurantCardFetch;
  getRestaurantId: TRestaurantCardIdFetch;
  restaurantCardIdReset: TRestaurantCardIdReset;
  restaurantCardId: string | null;
  restaurantCardIdIsPending: boolean;
  restaurantCardIdIsIdle: boolean;
  restaurantCardIdIsSuccess: boolean;
  restaurantCard: TRestaurantCard;
  restaurantCardIsIdle: boolean;
  restaurantCardIsSuccess: boolean;
  restaurantCardIsError: boolean;
  restaurantCardMainReset: TRestaurantCardMainReset;
  shopingCartSaveError: boolean;
  changeAddressModalShow: TChangeAddressModalShow;
  profileResetAllUiModels: TProfileResetAllUiModels;
  pushNotification: TPushNotification;
  restaurantCardInfoReset: TRestaurantCardInfoResetReset;
  restaurantCardReviewsReset: TRestaurantCardReviewsResetReset;
  restaurantReviewsReset: TRestaurantCardReviewsReset;
  showModal: boolean;
  showShoppingCardModal: TShowShoppingCardModal;
  isOldSiteBannerFull: boolean;
  shopingCartSaveResetUi: TShopingCartSaveResetUi;
}
