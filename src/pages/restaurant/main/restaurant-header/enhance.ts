// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { authModalShow } from '@actions/auth';
import { profileDeleteFavoriteRests, profileSetFavoriteRests } from '@actions/profile/favourite';
// Selectors
import { hitsAuthStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/home';

const mapDispatchToProps = {
  authModalShow,
  profileDeleteFavoriteRests,
  profileSetFavoriteRests,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
