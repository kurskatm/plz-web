// Main Types
import { TProfileModel } from '@/models/profile/types';
import { TRestaurantCard } from '@type/restaurants/restaurant-card';
import { TAuthModalShow } from '@actions/auth/action-types';
import {
  TProfileDeleteFavoriteRests,
  TProfileSetFavoriteRests,
} from '@actions/profile/favourite/action-types';

export interface TRestaurantHeaderProps {
  restaurantCard: TRestaurantCard;
  profile: TProfileModel;
  authModalShow: TAuthModalShow;
  profileDeleteFavoriteRests: TProfileDeleteFavoriteRests;
  profileSetFavoriteRests: TProfileSetFavoriteRests;
}
