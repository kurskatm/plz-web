// Modules
import React, { FC, memo, useCallback, useMemo, useState } from 'react';
// Modules Components
import { SmallFeedback, Liked } from 'chibbis-ui-kit';
// Utils
import { applyImageFallback } from '@/utils';
// Assets
import cardRestDefault from '@/../assets/svg/main/card-rest-default.svg';
// Lib
import { getHoursAndMinutes } from '@lib/restaurants/getHoursAndMinutes';
// Main Types
import { TRestaurantCardSpecialization } from '@type/restaurants/restaurant-card';
// Enhance
import { enhance } from './enhance';
// Types
import { TRestaurantHeaderProps } from './types';

const RestaurantHeaderComponent: FC<TRestaurantHeaderProps> = ({
  restaurantCard,
  authModalShow,
  profile,
  profileDeleteFavoriteRests,
  profileSetFavoriteRests,
}) => {
  const [isLiked, setLiked] = useState<boolean>(restaurantCard?.isFavorite);
  const getTime = useCallback(() => {
    if (restaurantCard?.workingTime && restaurantCard?.status !== 2) {
      const open = getHoursAndMinutes(restaurantCard?.workingTime?.openTime);
      const close = getHoursAndMinutes(restaurantCard?.workingTime?.closeTime);
      if (open === close) {
        return 'Круглосуточно';
      }

      const str = `С ${open} до ${close}`;

      return str;
    }

    return 'Закрыто';
  }, [restaurantCard?.workingTime, restaurantCard?.status]);

  const getMinCoast = useCallback(() => `от ${restaurantCard?.minCost} ₽`, [
    restaurantCard?.minCost,
  ]);

  const getMinDelivery = useCallback(() => {
    if (restaurantCard?.deliveryCost) {
      return `от ${restaurantCard?.deliveryCost} ₽`;
    }

    return 'Доставка бесплатно';
  }, [restaurantCard?.deliveryCost]);

  const renderSpecializations = useMemo(
    () => ({ id, name }: TRestaurantCardSpecialization) => <div key={id}>{name}</div>,
    [],
  );

  const likeClickHandler = useCallback(
    (e) => {
      e.preventDefault();

      if (!profile.auth) {
        authModalShow(true);
      } else {
        setLiked(!isLiked);
        if (isLiked) {
          profileDeleteFavoriteRests({ restId: restaurantCard?.id });
        } else {
          profileSetFavoriteRests({
            restId: restaurantCard?.id,
            // @ts-ignore Property 'banner' is missing in type 'TRestaurantCard'
            newRestaurant: restaurantCard,
          });
        }
      }
    },
    [
      profile.auth,
      authModalShow,
      isLiked,
      profileDeleteFavoriteRests,
      restaurantCard,
      profileSetFavoriteRests,
    ],
  );

  return (
    <div className="restaurant-header">
      <div className="restaurant-header__logo-block">
        <img
          src={restaurantCard?.logo || cardRestDefault}
          onError={applyImageFallback(cardRestDefault)}
          alt={restaurantCard?.name}
        />
      </div>

      <div className="restaurant-header__content-block">
        <div className="restaurant-header__content-block__title-like">
          <div className="restaurant-header__content-block__title">{restaurantCard?.name}</div>
          <div className="restaurant-header__like-block">
            <Liked headerLike selected={isLiked} onClick={likeClickHandler} />
          </div>
        </div>

        {!!restaurantCard?.specializations?.length && (
          <div className="restaurant-header__content-block__specializations">
            {restaurantCard?.specializations.map(renderSpecializations)}
          </div>
        )}
      </div>

      <div className="restaurant-header__content-block__info">
        <div className="restaurant-header__open-time">
          <div className="restaurant-header__open-time-icon" />
          <div className="restaurant-header__open-time-text">{getTime()}</div>
        </div>

        <div className="restaurant-header__min-coast">
          <div className="restaurant-header__min-coast-icon" />
          <div className="restaurant-header__min-coast-text">{getMinCoast()}</div>
        </div>

        <div className="restaurant-header__delivery-coast">
          <div className="restaurant-header__delivery-coast-icon" />
          <div className="restaurant-header__delivery-coast-text">{getMinDelivery()}</div>
        </div>

        <div className="restaurant-header__rate-percent">
          <SmallFeedback percent={restaurantCard?.positiveReviewsPercent} />
        </div>
      </div>
    </div>
  );
};

const RestaurantHeaderMemo = memo(RestaurantHeaderComponent);
export default enhance(RestaurantHeaderMemo);
