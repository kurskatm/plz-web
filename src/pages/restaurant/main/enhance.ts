// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { restaurantCardMainReset } from '@actions/restaurant/main/main';
import { restaurantCardFetch } from '@actions/restaurant/main/card';
import { restaurantCardIdFetch, restaurantCardIdReset } from '@actions/restaurant/main/id';
import { restaurantCategoriesReset } from '@actions/restaurant/menu/categories';
import { restaurantProductsReset } from '@actions/restaurant/menu/products';
import { changeAddressModalShow } from '@actions/checkout/modals';
import { profileResetAllUiModels } from '@actions/profile';
import { pushNotification } from '@actions/notifications';
import { restaurantCardInfoReset } from '@/actions/restaurant/info/main';
import { restaurantReviewsReset } from '@/actions/restaurant/reviews/current';
import { restaurantCardReviewsReset } from '@/actions/restaurant/reviews/main';
import { showShoppingCardModal } from '@actions/restaurant/modal';
import { shopingCartSaveResetUi } from '@actions/shoping-cart/save';

// Selectors
import { restaurantMainStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/restaurant/main';

const mapDispatchToProps = {
  getRestaurantCard: restaurantCardFetch,
  getRestaurantId: restaurantCardIdFetch,
  restaurantCategoriesReset,
  restaurantProductsReset,
  restaurantCardMainReset,
  restaurantCardIdReset,
  changeAddressModalShow,
  profileResetAllUiModels,
  restaurantCardInfoReset,
  restaurantReviewsReset,
  restaurantCardReviewsReset,
  pushNotification,
  showShoppingCardModal,
  shopingCartSaveResetUi,
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
