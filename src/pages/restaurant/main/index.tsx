// Modules
import loadable from '@loadable/component';
import React, { FC, memo, useCallback, useEffect, useRef, useState } from 'react';
import { Route, Switch, useHistory, useParams } from 'react-router-dom';
import NumberFormat from 'react-number-format';
import isNil from 'lodash/isNil';
import urlJoin from 'url-join';
import classnames from 'classnames';
import { useMediaQuery } from 'react-responsive';
import { useInView } from 'react-intersection-observer';
// Lib
import { ShopingCartFooterContext } from '@lib/shoping-cart/context';
// Utils
import { Consts } from '@utils';
// Components
import GoToOldSite from '@components/MoveToOldDesignBanner/Banner';
import CheckoutAddressModal from '@components/CheckoutAddressModal';
import BlackModalBase from '@components/BlackModalBase';
import { getNameFromUrl } from '@/lib/restaurants/getNameFromUrl';
import RestaurantHeader from './restaurant-header';
// Config
import { getPath } from '../config/routes';
// Enhance
import { enhance } from './enhance';
// Types
import { TRestaurantMainProps, TParamsRouter } from './types';

const { ROUTES, ENV } = Consts;
const { IS_CLIENT } = ENV;

const Header = loadable(() => import('@components/Header'));
const Footer = loadable(() => import('@components/Footer'));
const Menu = loadable(() => import('@components/Menu'));
const ShopingCart = loadable(() => import('@components/ShopingCart'));
const MenuComponent = loadable(() => import('../menu'));
const ReviewsComponent = loadable(() => import('../reviews'));
const InfoComponent = loadable(() => import('../info'));

const RestaurantMainComponent: FC<TRestaurantMainProps> = ({
  getRestaurantCard,
  getRestaurantId,
  restaurantCardId,
  restaurantCard,
  restaurantCardIsSuccess,
  restaurantCardIsError,
  restaurantCardMainReset,
  restaurantCardIdReset,
  profile,
  shopingCartSaveError,
  profileResetAllUiModels,
  restaurantCardInfoReset,
  restaurantReviewsReset,
  restaurantCardReviewsReset,
  pushNotification,
  showModal,
  showShoppingCardModal,
  isOldSiteBannerFull,
  shopingCartSaveResetUi,
  changeAddressModalShow,
}) => {
  const { replace, location } = useHistory();
  const { cityUrl, restaurantName, tabName } = useParams<TParamsRouter>();
  const rootRef = useRef<HTMLDivElement>();

  const { ref, inView } = useInView();

  const isTablet = useMediaQuery({
    minWidth: 769,
    maxWidth: 1024,
  });

  const isDesktop = useMediaQuery({
    minWidth: 1025,
  });

  const [isPushed, setIsPushed] = useState<boolean>(false);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  useEffect(
    () => {
      if (restaurantCardIsError) {
        changeAddressModalShow({ show: true });
        pushNotification({
          type: 'error',
          content: 'Не удалось получить ресторан',
        });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [restaurantCardIsError],
  );

  useEffect(
    () => {
      const isNilTabName = isNil(tabName);

      if (isNilTabName) {
        const url = urlJoin(
          ROUTES.CITIES.PATH,
          cityUrl,
          ROUTES.RESTAURANTS.RESTAURANT.MAIN.PATH,
          restaurantName,
          ROUTES.RESTAURANTS.RESTAURANT.SECTIONS.MENU.PATH,
        );

        replace(url);
      }

      return () => {
        restaurantCardMainReset();
        restaurantCardIdReset();
        profileResetAllUiModels();
        restaurantCardInfoReset();
        restaurantReviewsReset();
        restaurantCardReviewsReset();
      };
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  useEffect(
    () => {
      const geodata = {};

      if (profile.newGeoCode.data) {
        // @ts-ignore
        geodata.lat = profile.newGeoCode.data.lat;
        // @ts-ignore
        geodata.lng = profile.newGeoCode.data.lng;
      }

      if (!restaurantCardId) {
        getRestaurantId({
          cityId: profile.city,
          restaurantName: getNameFromUrl(location?.pathname),
        });
      } else if (!restaurantCardIsSuccess) {
        getRestaurantCard({
          restaurantId: restaurantCardId,
          ...geodata,
        });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [restaurantCardId],
  );

  const getRewies = useCallback(() => {
    if (restaurantCard?.reviewsCount) {
      return (
        <>
          Отзывы {/* @ts-ignore */}
          <NumberFormat
            value={restaurantCard.reviewsCount}
            thousandSeparator=" "
            displayType="text"
          />
        </>
      );
    }

    return 'Отзывы';
  }, [restaurantCard?.reviewsCount]);

  const getMenuConfig = useCallback(
    () => [
      {
        id: 'menu',
        localizedName: 'Меню',
        route: urlJoin(
          ROUTES.CITIES.PATH,
          cityUrl,
          ROUTES.RESTAURANTS.RESTAURANT.MAIN.PATH,
          restaurantName,
          ROUTES.RESTAURANTS.RESTAURANT.SECTIONS.MENU.PATH,
        ),
      },

      {
        id: 'reviews',
        localizedName: getRewies(),
        route: urlJoin(
          ROUTES.CITIES.PATH,
          cityUrl,
          ROUTES.RESTAURANTS.RESTAURANT.MAIN.PATH,
          restaurantName,
          ROUTES.RESTAURANTS.RESTAURANT.SECTIONS.REVIEWS.PATH,
        ),
      },

      {
        id: 'info',
        localizedName: 'О ресторане',
        route: urlJoin(
          ROUTES.CITIES.PATH,
          cityUrl,
          ROUTES.RESTAURANTS.RESTAURANT.MAIN.PATH,
          restaurantName,
          ROUTES.RESTAURANTS.RESTAURANT.SECTIONS.INFO.PATH,
        ),
      },
    ],
    [cityUrl, getRewies, restaurantName],
  );

  useEffect(
    () => {
      if (shopingCartSaveError) {
        shopingCartSaveResetUi();
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [shopingCartSaveError],
  );

  useEffect(() => {
    if (IS_CLIENT && isOldSiteBannerFull && isDesktop && !inView) {
      setIsPushed(true);
    } else {
      setIsPushed(false);
    }
  }, [inView, isDesktop, isOldSiteBannerFull]);

  return (
    <>
      <div className="grid-wrapper restaurant-main" ref={rootRef}>
        {showModal && isTablet && (
          <BlackModalBase clickHandler={() => showShoppingCardModal(false)} />
        )}
        <header className="grid-wrapper__header">
          <Header goBackLink={ROUTES.CITIES.PATH} goBackText="Рестораны" />
        </header>
        <main className="grid-wrapper__main restaurant-main__block pad-m">
          <div className="restaurant-main__block-content">
            {restaurantCardIsSuccess ? (
              <>
                <div>
                  <RestaurantHeader restaurantCard={restaurantCard} />
                </div>

                <Menu active={tabName} menuConfig={getMenuConfig()} />

                <div className="restaurant-main__block_divider__header" />
                <div>
                  <Switch>
                    <Route exact path={getPath('menu')}>
                      <MenuComponent />
                    </Route>
                    <Route exact path={getPath('reviews')}>
                      <ReviewsComponent />
                    </Route>
                    <Route exact path={getPath('info')}>
                      <InfoComponent />
                    </Route>
                  </Switch>
                </div>
              </>
            ) : (
              <>
                <div className="restaurant-main__card-skeleton" />
                <div className="restaurant-main__body-skeleton" />
              </>
            )}
          </div>
          <div
            className={classnames(
              'restaurant-main__shopping-cart',
              'shopping-cart__modal-animated',
              {
                'shopping-cart__modal-animated__opened': showModal && isTablet,
              },
            )}>
            {restaurantCardIsSuccess && (
              <ShopingCartFooterContext.Provider value={{ isPushed }}>
                <ShopingCart />
              </ShopingCartFooterContext.Provider>
            )}
          </div>
        </main>

        <footer className="grid-wrapper__footer" ref={ref}>
          <div className="restaurant-main__block_divider__footer" />
          <Footer />
        </footer>
        <CheckoutAddressModal />
      </div>
      <GoToOldSite />
    </>
  );
};

const RestaurantMainMemo = memo(RestaurantMainComponent);
export default enhance(RestaurantMainMemo);
