// Main Types
import { TRestaurantCardInfoFetch } from '@actions/restaurant/info/info/action-types';
import { TRestaurantCard } from '@type/restaurants/restaurant-card';
import { TRestaurantInfo } from '@type/restaurants/restaurant-info';
import { TPushNotification } from '@actions/notifications/action-types';

export interface TRestaurantInfoProps {
  restaurantCard: TRestaurantCard;
  restaurantInfo: TRestaurantInfo;
  restaurantInfoIsIdle: boolean;
  restaurantInfoIsSuccess: boolean;
  restaurantInfoIsError: boolean;
  restaurantInfoFetch: TRestaurantCardInfoFetch;
  pushNotification: TPushNotification;
}
