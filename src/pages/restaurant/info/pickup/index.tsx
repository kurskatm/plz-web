// Modules
import React, { FC, memo, useCallback } from 'react';
// Types
import { TPickupProps } from './types';

const PickupComponent: FC<TPickupProps> = ({ selfPickup }) => {
  const getText = useCallback(() => {
    if (selfPickup) {
      return 'Предусмотрен';
    }

    return 'Не предусмотрен';
  }, [selfPickup]);

  return <div>{getText()}</div>;
};

export default memo(PickupComponent);
