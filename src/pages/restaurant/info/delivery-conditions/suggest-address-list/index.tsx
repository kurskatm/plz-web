// Modules
import React, { FC, memo, useCallback, useMemo } from 'react';
// Types
import { TAddressListProps, TOption } from './types';

const AddressListComponent: FC<TAddressListProps> = ({
  showList,
  list,
  listRef,
  onOptionSelect,
}) => {
  const onClick = useCallback((option: TOption) => onOptionSelect(option), [onOptionSelect]);

  const renderList = useMemo(
    () => ({ name, description }: TOption) => (
      <div
        key={`${name}-${description}`}
        role="presentation"
        className="rest-address-suggest-list__item"
        onClickCapture={(e) => {
          e.stopPropagation();
          onClick({ name, description });
        }}>
        <div className="rest-address-suggest-list__item-name">{name}</div>
        <div className="rest-address-suggest-list__item-description">{description}</div>
      </div>
    ),
    [onClick],
  );

  const renderContent = useCallback(() => {
    if (showList) {
      return list.map(renderList);
    }

    return null;
  }, [list, renderList, showList]);

  return (
    <div className="rest-address-suggest-list">
      <div ref={listRef} className="rest-address-suggest-list__inner">
        {renderContent()}
      </div>
    </div>
  );
};

const AddressListMemo = memo(AddressListComponent);
export default AddressListMemo;
