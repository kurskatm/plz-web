// Main Types
import { TCitiesItem } from '@type/cities';
import { TDeliveryAreaConditionsList } from '@type/restaurants/delivery-conditions';
import {
  TRestaurantAddressesSuggestFetch,
  TRestaurantAddressesSuggestReset,
} from '@actions/restaurant/info/address-suggest/action-types';
import {
  TRestaurantGeocodeSuggestFetch,
  TRestaurantGeocodeSuggestReset,
} from '@actions/restaurant/info/geocode-suggest/action-types';
import {
  TRestaurantDeliveryAreasFetch,
  TRestaurantDeliveryAreasReset,
} from '@actions/restaurant/info/delivery-areas/action-types';
import { TRestaurantCard } from '@/type/restaurants/restaurant-card';
import { TNewGeocodeData } from '@/type/new-geocode';
import { TNewAddressesList } from '@type/new-address';

export interface TDeliveryConditionsProps {
  initialAddress: string;
  initialGeocode: TNewGeocodeData;
  city: TCitiesItem;
  restaurantCard: TRestaurantCard;
  addressList: TNewAddressesList;
  geocode: TNewGeocodeData;
  geocodeSuggestIsSuccess: boolean;
  deliveryAreasList: TDeliveryAreaConditionsList;
  deliveryAreasIsSuccess: boolean;
  addressSuggest: TRestaurantAddressesSuggestFetch;
  addressReset: TRestaurantAddressesSuggestReset;
  geocodeSuggest: TRestaurantGeocodeSuggestFetch;
  geocodeReset: TRestaurantGeocodeSuggestReset;
  areasFetch: TRestaurantDeliveryAreasFetch;
  areasReset: TRestaurantDeliveryAreasReset;
}

export interface TOption {
  name: string;
  description: string;
}

export interface TGeoState {
  name: string;
  description: string;
}

export interface TAreasArgs {
  cityId: string;
  lat: number;
  lon: number;
  restId: string;
  clientTime: string;
}

export { TNewGeocodeData };
