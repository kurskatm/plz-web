// Main Types
import {
  TDeliveryAreaConditionsList,
  TDeliveryAreaCondition,
} from '@type/restaurants/delivery-conditions';

export interface TDeliveryAreasListProps {
  list: TDeliveryAreaConditionsList;
}

export { TDeliveryAreaCondition };
