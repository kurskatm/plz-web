// Modules
import React, { FC, memo, useCallback, useMemo } from 'react';
// Types
import { TDeliveryAreasListProps, TDeliveryAreaCondition } from './types';

const TDeliveryAreasListComponent: FC<TDeliveryAreasListProps> = ({ list }) => {
  const getDeliveryCost = useCallback((deliveryCost: number) => {
    if (deliveryCost) {
      return `${deliveryCost} ₽`;
    }

    return 'бесплатно';
  }, []);

  const renderAreaItem = useMemo(
    () => ({ deliveryAreaId, deliveryCost, orderCost }: TDeliveryAreaCondition) => (
      <div key={deliveryAreaId} className="rest-delivery-conditions-item">
        <div className="rest-delivery__running_man" />
        <div className="rest-delivery-order-cost">
          {`От ${orderCost} ₽ доставка ${getDeliveryCost(deliveryCost)}`}
        </div>
      </div>
    ),
    [getDeliveryCost],
  );

  return <div className="rest-delivery-conditions-list">{list.map(renderAreaItem)}</div>;
};

const TDeliveryAreasListMemo = memo(TDeliveryAreasListComponent);
export default TDeliveryAreasListMemo;
