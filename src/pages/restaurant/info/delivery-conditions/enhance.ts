// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import {
  restaurantAddressesSuggestFetch,
  restaurantAddressesSuggestReset,
} from '@actions/restaurant/info/address-suggest';
import {
  restaurantGeocodeSuggestFetch,
  restaurantGeocodeSuggestReset,
} from '@actions/restaurant/info/geocode-suggest';
import {
  restaurantDeliveryAreasFetch,
  restaurantDeliveryAreasReset,
} from '@actions/restaurant/info/delivery-areas';
// Selectors
import { restaurantDeliveryAreaStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/restaurant/info';

const mapDispatchToProps = {
  addressSuggest: restaurantAddressesSuggestFetch,
  addressReset: restaurantAddressesSuggestReset,
  geocodeSuggest: restaurantGeocodeSuggestFetch,
  geocodeReset: restaurantGeocodeSuggestReset,
  areasFetch: restaurantDeliveryAreasFetch,
  areasReset: restaurantDeliveryAreasReset,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
