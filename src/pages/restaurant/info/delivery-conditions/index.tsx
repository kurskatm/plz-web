// Modules
import React, {
  FC,
  memo,
  useCallback,
  useMemo,
  useState,
  createRef,
  useRef,
  useEffect,
} from 'react';
import debounce from 'lodash/debounce';
import isNil from 'lodash/isNil';
import dayjs from 'dayjs';
// Components
import { Input, GeoIcon, LoadingSpinner } from 'chibbis-ui-kit';
import AreasList from './delivery-areas-list';
import AddressList from './suggest-address-list';
// Enhance
import { enhance } from './enhance';
// Types
import { TDeliveryConditionsProps, TGeoState, TOption, TAreasArgs, TNewGeocodeData } from './types';

const inputHtmlRef = createRef<HTMLInputElement>();
const listRef = createRef<HTMLInputElement>();

const DeliveryConditionsComponent: FC<TDeliveryConditionsProps> = ({
  deliveryAreasList,
  deliveryAreasIsSuccess,
  initialAddress,
  initialGeocode,
  city,
  restaurantCard,
  addressList,
  geocode,
  addressSuggest,
  addressReset,
  geocodeSuggest,
  geocodeReset,
  areasReset,
  areasFetch,
}) => {
  const updateAddress = useRef(
    debounce((query: string) => {
      addressSuggest({
        limit: 5,
        cityId: city.id,
        query,
      });
    }, 200),
  );

  const updateGeocode = useRef(
    debounce(({ name, description }: { name: string; description: string }) => {
      geocodeSuggest({
        cityId: city.id,
        description,
        name,
      });
    }, 200),
  );

  const updateAreas = useRef(
    debounce(({ cityId, lat, lon, restId, clientTime }: TAreasArgs) => {
      areasFetch({
        cityId,
        lat,
        lon,
        restId,
        clientTime,
      });
    }, 200),
  );

  const [address, setAddress] = useState<string>(initialAddress);
  const [geocodeState, setGeocodeState] = useState<TNewGeocodeData>(initialGeocode);
  const [loading, setLoading] = useState<boolean>(false);
  const [showList, setShowList] = useState<boolean>(false);
  const [prevGeoState, setPrevGeoState] = useState<TGeoState>();

  useEffect(
    () => {
      if (initialAddress.length && !isNil(initialGeocode) && initialGeocode.precision === 'exact') {
        const now = dayjs().format('MM/DD/YYYY HH:mm');
        updateAreas.current({
          cityId: city.id,
          lat: initialGeocode.lat,
          lon: initialGeocode.lng,
          restId: restaurantCard.id,
          clientTime: now,
        });
        setLoading(true);
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  useEffect(() => {
    if (isNil(geocodeState) && address.length > 3) {
      updateAddress.current(address);
      setLoading(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [address, city, geocodeState]);

  useEffect(() => {
    if (addressList?.length && address.length < 3) {
      addressReset();
      geocodeReset();
      areasReset();
      setShowList(false);
      setGeocodeState(null);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [address, addressList]);

  useEffect(() => {
    if (addressList?.length > 0) {
      setShowList(true);
      setLoading(false);
    }
    if (!isNil(geocode) && geocode.precision !== 'exact') {
      setShowList(true);
      setLoading(false);
    }
    if (!isNil(geocode) && geocode.precision === 'exact') {
      addressReset();
      const now = dayjs().format('MM/DD/YYYY HH:mm');
      updateAreas.current({
        cityId: city.id,
        lat: geocode.lat,
        lon: geocode.lng,
        restId: restaurantCard.id,
        clientTime: now,
      });
      setLoading(true);
      setGeocodeState(geocode);
    }
  }, [addressList, addressReset, city.id, geocode, restaurantCard.id]);

  useEffect(() => {
    if (deliveryAreasList?.length > 0 || deliveryAreasIsSuccess) {
      setLoading(false);
    }
  }, [deliveryAreasList, deliveryAreasIsSuccess]);

  const onInputChange = useCallback(({ target }) => {
    setAddress(target.value);
  }, []);

  const onInputClear = useCallback(
    () => {
      setAddress('');
      setShowList(true);
      setLoading(false);
      addressReset();
      geocodeReset();
      areasReset();
      setGeocodeState(null);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const onOptionSelect = useCallback(
    (option: TOption) => {
      const validData: TGeoState = {
        name: option.name,
        description: option.description,
      };
      setAddress(option.name);
      if (validData !== prevGeoState) {
        updateGeocode.current({
          name: option.name,
          description: option.description,
        });
        setPrevGeoState(validData);
        setShowList(false);
        setLoading(true);
      }
    },
    [prevGeoState],
  );

  const renderContent = useMemo(() => {
    if (loading) {
      return (
        <LoadingSpinner className="rest-delivery-conditions__spinner" size="big" color="orange" />
      );
    }
    if (!loading && address.length <= 3) {
      return (
        <div className="rest-delivery-conditions__empty-list">
          Укажите интересующий адрес доставки и мы покажем минимальную сумму заказа и стоимость
          доставки
        </div>
      );
    }
    if (!loading && !!addressList?.length) {
      return (
        <AddressList
          list={addressList}
          listRef={listRef}
          showList={showList}
          onOptionSelect={onOptionSelect}
        />
      );
    }
    if (!loading && !!deliveryAreasList?.length) {
      return (
        <div className="rest-delivery-conditions__list">
          <AreasList list={deliveryAreasList} />
        </div>
      );
    }

    if (!loading && !deliveryAreasList?.length && deliveryAreasIsSuccess) {
      return (
        <div className="rest-delivery-conditions__empty-list">
          Сюда не доставляет, проверьте другой адрес
        </div>
      );
    }

    return null;
  }, [
    loading,
    addressList,
    deliveryAreasList,
    deliveryAreasIsSuccess,
    showList,
    onOptionSelect,
    address,
  ]);

  return (
    <div className="rest-delivery-conditions">
      <div className="rest-delivery-conditions__input">
        <Input
          autocomplete={false}
          clear
          icon={GeoIcon}
          inputRef={inputHtmlRef}
          name="deliveryAreaAddress"
          onChange={onInputChange}
          onClear={onInputClear}
          placeholder="Укажите улицу и дом"
          value={address}
          theme="grey"
        />
      </div>
      {renderContent}
    </div>
  );
};

const DeliveryConditionsMemo = memo(DeliveryConditionsComponent);
export default enhance(DeliveryConditionsMemo);
