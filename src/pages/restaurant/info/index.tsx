// Modules
import React, { FC, memo, useEffect } from 'react';
// Components
import { LoadingSpinner } from 'chibbis-ui-kit';
import Specializations from '@components/Specializations';
import DeliveryConditions from './delivery-conditions';
import PaymentTypes from './payment-types';
import WeekSchedule from './week-schedule';
import Pickup from './pickup';
import Legal from './legal';
// Enhance
import { enhance } from './enhance';
// Types
import { TRestaurantInfoProps } from './types';

const RestaurantInfoComponent: FC<TRestaurantInfoProps> = ({
  restaurantCard,
  restaurantInfo,
  restaurantInfoIsIdle,
  restaurantInfoIsSuccess,
  restaurantInfoIsError,
  restaurantInfoFetch,
  pushNotification,
}) => {
  useEffect(
    () => {
      if (restaurantInfoIsIdle) {
        restaurantInfoFetch({
          restaurantId: restaurantCard.id,
        });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  useEffect(
    () => {
      if (restaurantInfoIsError) {
        pushNotification({
          type: 'error',
          content: 'Не удалось загрузить информацию по ресторану',
        });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [restaurantInfoIsError],
  );

  return (
    <div className="restaurant-info">
      {!restaurantInfoIsSuccess && (
        <div className="restaurant-info__spinner">
          <LoadingSpinner size="big" color="orange" />
        </div>
      )}
      {restaurantInfoIsSuccess && restaurantInfo && (
        <div className="restaurant-info-content">
          {restaurantInfo.description && (
            <div className="restaurant-info-description">
              <div className="restaurant-info-title">Описание</div>
              <div className="restaurant-info-text">{restaurantInfo.description}</div>
            </div>
          )}

          {restaurantInfo.paymentTypes && (
            <div className="restaurant-info-payment-types">
              <div className="restaurant-info-title">Способы оплаты</div>
              <PaymentTypes paymentTypes={restaurantInfo.paymentTypes} />
            </div>
          )}

          {!!restaurantInfo.specializations?.length && (
            <div className="restaurant-info-specializations">
              <div className="restaurant-info-title">Специализация</div>
              <Specializations specializations={restaurantInfo.specializations} />
            </div>
          )}

          {!!restaurantInfo.weekSchedule?.length && (
            <div className="restaurant-info-week-schedule">
              <div className="restaurant-info-title">Режим работы</div>
              <WeekSchedule status={restaurantInfo.status} />
            </div>
          )}

          {!!restaurantInfo.deliveryConditions?.length && (
            <div className="restaurant-info-delivery-conditions">
              <div className="restaurant-info-title">Условия доставки</div>
              <div className="restaurant-info-text">
                <DeliveryConditions />
              </div>
            </div>
          )}

          <div className="restaurant-info-self-pickup">
            <div className="restaurant-info-title">Самовывоз</div>
            <div className="restaurant-info-text">
              <Pickup selfPickup={restaurantInfo.selfPickup} />
            </div>
          </div>

          <Legal restaurantCard={restaurantCard} />
        </div>
      )}
    </div>
  );
};

const RestaurantInfoMemo = memo(RestaurantInfoComponent);
export default enhance(RestaurantInfoMemo);
