// Modules
import React, { FC, memo, useCallback } from 'react';
// Lib
import { getPaymentTypes as getPaymentTypesFnc } from '@lib/restaurants/payment-types';
// Types
import { TPaymentTypesProps } from './types';

const PaymentTypesComponent: FC<TPaymentTypesProps> = ({ paymentTypes }) => {
  const getPaymentTypes = useCallback(() => {
    const payment = getPaymentTypesFnc(paymentTypes, false)
      .map(({ name }) => name)
      .join(', ');

    if (payment) {
      return payment;
    }

    return 'Оплата не указана';
  }, [paymentTypes]);

  return <div className="rest-payment-types">{getPaymentTypes()}</div>;
};

export default memo(PaymentTypesComponent);
