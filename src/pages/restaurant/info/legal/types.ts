// Main Types
import { TPushNotification } from '@/actions/notifications/action-types';
import { TRestaurantCardLegalFetch } from '@actions/restaurant/info/legal/action-types';
import { TRestaurantCard } from '@type/restaurants/restaurant-card';
import { TRestaurantLegal } from '@type/restaurants/restaurant-legal';

export interface TLegalProps {
  legal: TRestaurantLegal | null;
  legalIsIdle: boolean;
  legalIsSuccess: boolean;
  legalIsError: boolean;
  legalFetch: TRestaurantCardLegalFetch;
  pushNotification: TPushNotification;
  restaurantCard: TRestaurantCard;
}
