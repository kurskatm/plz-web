// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { restaurantLegalFetch } from '@actions/restaurant/info/legal';
import { pushNotification } from '@actions/notifications';
// Selectors
import { restaurantInfoLegalStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/restaurant/info';

const mapDispatchToProps = {
  legalFetch: restaurantLegalFetch,
  pushNotification,
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
