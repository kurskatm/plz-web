// Modules
import React, { FC, memo, useEffect } from 'react';
// Enhance
import { enhance } from './enhance';
// Types
import { TLegalProps } from './types';

const LegalComponent: FC<TLegalProps> = ({
  legal,
  legalIsIdle,
  legalIsSuccess,
  legalIsError,
  legalFetch,
  pushNotification,
  restaurantCard,
}) => {
  useEffect(
    () => {
      if (legalIsIdle) {
        legalFetch({
          restaurantId: restaurantCard.id,
          storeId: restaurantCard.storeId,
        });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  useEffect(
    () => {
      if (legalIsError) {
        pushNotification({
          type: 'error',
          content: 'Не удалось загрузить юридическую информацию',
        });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [legalIsError],
  );

  if (legalIsSuccess) {
    return (
      <div className="restaurant-info-legal">
        <div className="restaurant-legal-title">Юридическая информация</div>

        <div className="restaurant-legal-content">
          {!!legal.name?.length && (
            <div className="restaurant-legal-str">
              {/* <div>ИП</div> */}
              <div>{legal.name}</div>
            </div>
          )}

          {!!legal.inn?.length && (
            <div className="restaurant-legal-str">
              <div>ИНН</div>
              <div>{legal.inn}</div>
            </div>
          )}

          {!!legal.ogrn?.length && (
            <div className="restaurant-legal-str">
              <div>ОГРН</div>
              <div>{legal.ogrn}</div>
            </div>
          )}

          {!!legal.address?.length && (
            <div className="restaurant-legal-str">
              <div>{legal.address}</div>
            </div>
          )}
        </div>
      </div>
    );
  }

  return null;
};

const LegalMemo = memo(LegalComponent);
export default enhance(LegalMemo);
