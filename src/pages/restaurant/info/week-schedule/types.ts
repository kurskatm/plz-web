interface TDay {
  dayOfWeek: string;
  fromTime: string;
  toTime: string;
}

export interface TWeekScheduleProps {
  status: number | null;
  weekSchedule: Record<string, TDay>;
}
