// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Selectors
import { restaurantInfoWeekScheduleStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/restaurant/info';

const withConnect = connect(mapStateToProps);

export const enhance = compose(withConnect);
