// Modules
import React, { FC, memo, useCallback, useMemo } from 'react';
import classNames from 'classnames';
import dayjs from 'dayjs';
// Lib
import { getClosedStatus } from '@lib/restaurants/closed-status';
import { WEEK } from '@lib/date/week';
import { Consts } from '@utils';
// Main Types
import { TWeekItem } from '@lib/date/week/types';
// Enhance
import { enhance } from './enhance';
// Types
import { TWeekScheduleProps } from './types';

const { IS_CLIENT } = Consts.ENV;

// ломается сср без этого TODO - отрефакторить
const ssrWindow = IS_CLIENT
  ? window
  : {
      innerWidth: 0,
    };

const WeekScheduleComponent: FC<TWeekScheduleProps> = ({ status, weekSchedule }) => {
  const getStatus = useCallback(() => {
    const statusObj = getClosedStatus({
      status,
    });

    return statusObj;
  }, [status]);

  const getTimeOfDay = useCallback(
    (dayName: string) => {
      const dayItem = WEEK.find(({ id }) => id === dayName);
      if (weekSchedule[dayItem.index]) {
        const day = weekSchedule[dayItem.index];
        if (day.fromTime === day.toTime) {
          return 'Круглосуточно';
        }
        const str = `${day.fromTime} – ${day.toTime}`;

        return str;
      }

      return 'Выходной';
    },
    [weekSchedule],
  );

  const getTimeCloseHeader = useCallback(() => {
    const smallMobile = ssrWindow?.innerWidth <= 470;
    const dayIndex = dayjs().day();
    const currentDay = WEEK.find(({ index }) => index === dayIndex);
    if (weekSchedule[currentDay.index]) {
      const day = weekSchedule[currentDay.index];
      if (day.fromTime === day.toTime && smallMobile) {
        return (
          <>
            <br />
            <span className="rest-week-schedule-header__status-close-time">
              Заказы принимаются круглосуточно.
            </span>
          </>
        );
      }
      if (day.fromTime === day.toTime && !smallMobile) {
        return (
          <span className="rest-week-schedule-header__status-close-time">
            Заказы принимаются круглосуточно.
          </span>
        );
      }
      return (
        <span className="rest-week-schedule-header__status-close-time">
          Заказы принимаются до {day.toTime}
        </span>
      );
    }
    return null;
  }, [weekSchedule]);

  const getStylesHeader = useCallback(
    () =>
      classNames('rest-week-schedule-header', {
        'rest-week-schedule-header-open': !!getStatus().status,
        'rest-week-schedule-header-close': !getStatus().status,
      }),
    [getStatus],
  );

  const renderItem = useMemo(
    () => ({ id, name }: TWeekItem) => (
      <div key={id} className="rest-week-schedule-item">
        <div className="rest-week-schedule-item-day">{name}</div>

        <div className="rest-week-schedule-item-time">{getTimeOfDay(id)}</div>
      </div>
    ),
    [getTimeOfDay],
  );

  return (
    <div className="rest-week-schedule">
      <div className={getStylesHeader()}>
        <div className="rest-week-schedule-header__status">{getStatus().message}</div>
        {getTimeCloseHeader()}
      </div>

      <div className="rest-week-schedule-description">Повседневное расписание</div>

      <div className="rest-week-schedule-content">{WEEK.map(renderItem)}</div>
    </div>
  );
};

const WeekScheduleMemo = memo(WeekScheduleComponent);
export default enhance(WeekScheduleMemo);
