// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { restaurantInfoFetch } from '@actions/restaurant/info/info';
import { pushNotification } from '@actions/notifications';
// Selectors
import { restaurantInfoStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/restaurant/info';

const mapDispatchToProps = {
  restaurantInfoFetch,
  pushNotification,
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
