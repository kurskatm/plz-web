// Modules
import urlJoin from 'url-join';
// Utils
import { Consts } from '@utils';

const { ROUTES } = Consts;

export const getPath = (route: string): string =>
  urlJoin(
    ROUTES.CITIES.PATH,
    ROUTES.HOME.PATH,
    ROUTES.RESTAURANTS.RESTAURANT.MAIN.PATH,
    ROUTES.RESTAURANTS.RESTAURANT.MAIN.PARAMS,
    route,
  );
