// Main Types
import { TRestaurantCardPromoFetch } from '@actions/restaurant/menu/promo/action-types';
import { TRestaurantCard } from '@type/restaurants/restaurant-card';
import { TRestaurantPromoList } from '@type/restaurants/promo';
import { TPushNotification } from '@/actions/notifications/action-types';
import { TShowPromoCardModal } from '@actions/restaurant/modal/action-types';

export interface TPromoSliderProps {
  promos: TRestaurantPromoList;
  promosIsIdle: boolean;
  promosIsSuccess: boolean;
  promosIsError: boolean;
  restaurantCard: TRestaurantCard;
  restaurantPromoFetch: TRestaurantCardPromoFetch;
  pushNotification: TPushNotification;
  showPromoCardModal: TShowPromoCardModal;
}
