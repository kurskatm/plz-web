// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { restaurantPromoFetch } from '@actions/restaurant/menu/promo';
import { pushNotification } from '@actions/notifications';
import { showPromoCardModal } from '@actions/restaurant/modal';
// Selectors
import { restaurantMenuPromoSliderStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/restaurant/menu';

const mapDispatchToProps = {
  restaurantPromoFetch,
  pushNotification,
  showPromoCardModal,
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
