// Modules
import loadable from '@loadable/component';
import React, { FC, memo, useEffect, useMemo, useCallback } from 'react';
import Truncate from 'react-truncate';
import { useMediaQuery } from 'react-responsive';
// Main Types
import { TRestaurantPromo } from '@type/restaurants/promo';
// Enhance
import { Consts } from '@/utils';
import { enhance } from './enhance';
// Types
import { TPromoSliderProps } from './types';

const { IS_CLIENT } = Consts.ENV;
const Slider = loadable(() => import('@components/Slider'));

const PromoSliderComponent: FC<TPromoSliderProps> = ({
  promos,
  promosIsIdle,
  promosIsSuccess,
  promosIsError,
  restaurantCard,
  restaurantPromoFetch,
  pushNotification,
  showPromoCardModal,
}) => {
  const isMobile = useMediaQuery({
    maxWidth: 639,
  });

  useEffect(
    () => {
      if (promosIsError) {
        pushNotification({
          type: 'error',
          content: 'Не удалось получить промо-акции',
        });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [promosIsError],
  );

  useEffect(
    () => {
      if (promosIsIdle && restaurantCard) {
        restaurantPromoFetch({
          restaurantId: restaurantCard.id,
          storeId: restaurantCard.storeId,
        });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const getContentLinesNumber = useCallback((length: number, index: number) => {
    if (IS_CLIENT) {
      const titleDiv = document?.body.getElementsByClassName(
        'rest-menu-pr__slider-item__header-title',
      )[index];
      const divHeight = titleDiv?.clientHeight;
      if (length > 25 && Number(divHeight) > 28) {
        return 1;
      }
    }

    return 2;
  }, []);

  const onCardClick = useCallback(
    (id: string) => {
      showPromoCardModal({
        promoId: id,
        showModal: true,
      });
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const renderItem = useMemo(
    () => (item: TRestaurantPromo, index: number) => (
      <div
        key={item.id}
        className="rest-menu-pr__slider-item"
        role="presentation"
        onClick={() => onCardClick(item.id)}>
        <div className="rest-menu-pr__slider-item__header">
          <div className="rest-menu-pr__slider-item__header-title">
            <Truncate lines={2} trimWhitespace>
              {item.name}
            </Truncate>
          </div>

          {/* <div className="rest-menu-pr__slider-item__header-icon">
            {item.imagePath && <img src={item.imagePath} alt={item.name} />}
          </div> */}
        </div>

        <div className="rest-menu-pr__slider-item__content">
          <Truncate lines={getContentLinesNumber(item.name.length, index)} trimWhitespace>
            {item.description}
          </Truncate>
        </div>
      </div>
    ),
    [getContentLinesNumber, onCardClick],
  );

  const getSliderMinMaxStyle = useCallback(() => {
    if (isMobile && promos.length === 1) return '100%';

    return null;
  }, [isMobile, promos.length]);

  if (promosIsSuccess && promos.length) {
    return (
      <div className="restaurant-menu__block-offers-inner">
        <Slider
          list={promos}
          // @ts-ignore missing prop "index"
          renderItem={renderItem}
          minMaxStyle={getSliderMinMaxStyle()}
        />
      </div>
    );
  }

  return null;
};

const PromoSliderMemo = memo(PromoSliderComponent);
export default enhance(PromoSliderMemo);
