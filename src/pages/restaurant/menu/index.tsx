// Modules
import loadable from '@loadable/component';
import React, { FC, memo, useState, useCallback, useMemo, useEffect } from 'react';
import { useQueryParam, StringParam } from 'use-query-params';
// Modules Components
import { LoadingSpinner } from 'chibbis-ui-kit';
// Components
import ModifiersCardModal from '@components/RestaurantModals/ModifiersCardModal';
import PromoCardModal from '@components/RestaurantModals/PromoCardModal';
import PromoSlider from './promo-slider';
import CategoriesSlider from './categories-slider';
import CategoriesList from './categories-list';
// Enhance
import { enhance } from './enhance';
// Types
import { TRestaurantMenuProps } from './types';

const Search = loadable(() => import('@components/Search'));

const RestaurantMenuComponent: FC<TRestaurantMenuProps> = ({
  headerHeight,
  restaurantCard,
  searchStr,
  searchProductsIsPending,
  searchProductsIsSuccess,
  searchProductsIsError,
  restaurantSearchProducts,
  restaurantSearchProductsReset,
  pushNotification,
}) => {
  const [categoriesParam, setCategoriesParam] = useQueryParam('categories', StringParam);
  const [active, setActive] = useState<string | null>(null);
  const [scrollTriggeredBySlider, setScrollTriggeredBySlider] = useState(false);

  useEffect(
    () => {
      if (searchProductsIsError) {
        pushNotification({
          type: 'error',
          content: 'Не удалось получить список продуктов',
        });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [searchProductsIsError],
  );

  const scrollToCategory = useCallback(
    (id: string) => {
      const yOffset = -headerHeight - window.innerHeight / 10;
      const elementId = `rest-category-${id}`;
      const element = document.getElementById(elementId);
      if (element) {
        setScrollTriggeredBySlider(true);
        const y = element.getBoundingClientRect().top + window.pageYOffset + yOffset;
        window.scrollTo({ top: y, behavior: 'smooth' });
      }
    },
    [headerHeight, setScrollTriggeredBySlider],
  );

  const onSearch = useCallback(
    (search: string) => {
      if (search.length) {
        restaurantSearchProducts({
          restaurantId: restaurantCard?.id,
          storeId: restaurantCard?.storeId,
          query: search,
        });
      }

      if (!search.length) {
        restaurantSearchProductsReset();
      }
    },
    [
      restaurantCard?.id,
      restaurantCard?.storeId,
      restaurantSearchProducts,
      restaurantSearchProductsReset,
    ],
  );

  const renderSliderAndCategories = useMemo(() => {
    if (searchProductsIsPending) {
      return (
        <LoadingSpinner className="restaurant-reviews-list__spinner" size="big" color="orange" />
      );
    }

    if (searchProductsIsSuccess) {
      return (
        <div className="restaurant-menu__block-categories">
          <CategoriesList
            active={active}
            setActive={setActive}
            setCategoriesParam={setCategoriesParam}
            scrollToCategory={scrollToCategory}
            scrollTriggeredBySlider={scrollTriggeredBySlider}
            setScrollTriggeredBySlider={setScrollTriggeredBySlider}
          />
        </div>
      );
    }

    return (
      <>
        <div className="restaurant-menu__block-categories-tags">
          <CategoriesSlider
            active={active}
            setActive={setActive}
            categoriesParam={categoriesParam}
            setCategoriesParam={setCategoriesParam}
            scrollToCategory={scrollToCategory}
          />
        </div>

        <div className="restaurant-menu__block-categories">
          <CategoriesList
            active={active}
            setActive={setActive}
            setCategoriesParam={setCategoriesParam}
            scrollToCategory={scrollToCategory}
            scrollTriggeredBySlider={scrollTriggeredBySlider}
            setScrollTriggeredBySlider={setScrollTriggeredBySlider}
          />
        </div>
      </>
    );
  }, [
    active,
    categoriesParam,
    scrollToCategory,
    searchProductsIsSuccess,
    searchProductsIsPending,
    setCategoriesParam,
    scrollTriggeredBySlider,
  ]);

  return (
    <div className="restaurant-menu">
      <div className="restaurant-menu__block-search">
        <Search placeholder="Найти блюдо" action={onSearch} initialValue={searchStr} />
      </div>

      <div className="restaurant-menu__block-offers">
        <PromoSlider />
      </div>

      {renderSliderAndCategories}
      <ModifiersCardModal />
      <PromoCardModal />
    </div>
  );
};

const RestaurantMenuMemo = memo(RestaurantMenuComponent);
export default enhance(RestaurantMenuMemo);
