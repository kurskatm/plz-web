// Main Types
import { TPushNotification } from '@/actions/notifications/action-types';
import { TRestaurantCardCategoriesFetch } from '@actions/restaurant/menu/categories/action-types';
import { TRestaurantCard } from '@type/restaurants/restaurant-card';
import { TRestaurantCategoriesList } from '@type/restaurants/restaurant-categories';

export interface TCategoriesSliderProps {
  active: string | null;
  setActive: React.Dispatch<React.SetStateAction<string>>;
  categories: TRestaurantCategoriesList;
  categoriesIsIdle: boolean;
  categoriesIsSuccess: boolean;
  categoriesIsError: boolean;
  headerHeight: number;
  restaurantCard: TRestaurantCard;
  restaurantCategoriesFetch: TRestaurantCardCategoriesFetch;
  categoriesParam: string;
  setCategoriesParam: (newValue: string, updateType?: string) => void;
  scrollToCategory: (id: string) => void;
  pushNotification: TPushNotification;
}
