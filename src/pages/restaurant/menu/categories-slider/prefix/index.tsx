// Modules
import React, { FC, memo } from 'react';
// Types
import { TPrefixCategoriesProps } from './types';

const PrefixCategoriesComponent: FC<TPrefixCategoriesProps> = ({ logo }) => (
  <div className="rest-categories-logo">
    <img src={logo} alt="logo" />
  </div>
);

export default memo(PrefixCategoriesComponent);
