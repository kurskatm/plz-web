// Modules
import loadable from '@loadable/component';
import React, { FC, memo, useCallback, useEffect, useMemo, useState, useRef } from 'react';
import throttle from 'lodash/throttle';
import classNames from 'classnames';
// Modules Components
import { Button } from 'chibbis-ui-kit';
// Main Types
import { TRestaurantCategory } from '@type/restaurants/restaurant-categories';
// Components
// import PrefixCategories from './prefix';
// Enhance
import { enhance } from './enhance';
// Types
import { TCategoriesSliderProps } from './types';

const Slider = loadable(() => import('@components/Slider'));

const CategoriesSliderComponent: FC<TCategoriesSliderProps> = ({
  active,
  setActive,
  categories,
  categoriesIsSuccess,
  categoriesIsError,
  headerHeight,
  categoriesParam,
  setCategoriesParam,
  scrollToCategory,
  pushNotification,
}) => {
  const sliderRef = useRef<HTMLDivElement>();
  const [sliderFixed, setSliderFixed] = useState<boolean>(false);

  useEffect(
    () => {
      if (categoriesIsError) {
        pushNotification({
          type: 'error',
          content: 'Не удалось получить список категорий',
        });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [categoriesIsError],
  );

  const scrollCollback = throttle(() => {
    const viewportOffset = sliderRef.current?.getBoundingClientRect();

    if (viewportOffset) {
      const top = Math.ceil(viewportOffset.top);
      const isFixed = top <= headerHeight + 16;

      if (isFixed && !sliderFixed) {
        setSliderFixed(true);
        return true;
      }

      if (!isFixed && sliderFixed) {
        setSliderFixed(false);
        return false;
      }
    }

    return false;
  }, 200);

  useEffect(
    () => {
      setSliderFixed(scrollCollback());

      // if (categoriesIsIdle && restaurantCard) {
      //   restaurantCategoriesFetch({
      //     restaurantId: restaurantCard.id,
      //     storeId: restaurantCard.storeId,
      //   });
      // }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  useEffect(
    () => {
      window.addEventListener('scroll', scrollCollback);

      return () => {
        window.removeEventListener('scroll', scrollCollback);
      };
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [headerHeight, sliderFixed],
  );

  const getMainStyles = useCallback(
    () =>
      classNames('restaurant-cat-slider-wrap', { 'restaurant-cat-slider-wrap-fixed': sliderFixed }),
    [sliderFixed],
  );

  const getButtonStyles = useCallback(
    (id: string) =>
      classNames('restaurant-categories-item', {
        'restaurant-categories-item-active': active === id,
      }),
    [active],
  );

  useEffect(() => {
    if (categoriesIsSuccess) {
      const category = categoriesParam
        ? categories.find(({ urlName }) => urlName === categoriesParam)
        : null;
      if (category) {
        setActive(category.id);
      } else if (categories.length) {
        setActive(categories[0].id);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getInlineMainStyles = useCallback(
    () => ({
      top: sliderFixed ? `${headerHeight}px` : 'auto',
    }),
    [headerHeight, sliderFixed],
  );

  const handleCategoryClick = useCallback(
    (id: string, urlName: string) => () => {
      if (id !== active) {
        setActive(id);
        scrollToCategory(id);
        setCategoriesParam(urlName, 'replaceIn');
      }
    },
    [active, setActive, scrollToCategory, setCategoriesParam],
  );

  const getCategoryName = useCallback((name: string, icon: string) => {
    if (icon) {
      switch (icon) {
        case 'fire': {
          return (
            <div className="restaurant-slider-category-name">
              {name}
              <div className="restaurant-slider-category-name__fire" />
            </div>
          );
        }

        case 'chibbcoin': {
          return (
            <div className="restaurant-slider-category-name">
              {name}
              <div className="restaurant-slider-category-name__chibbcoin" />
            </div>
          );
        }

        case 'heart': {
          return (
            <div className="restaurant-slider-category-name">
              {name}
              <div className="restaurant-slider-category-name__heart" />
            </div>
          );
        }

        default: {
          break;
        }
      }
    }

    return name;
  }, []);

  const renderItem = useMemo(
    () => ({ id, name, urlName, icon }: TRestaurantCategory) => (
      <div key={id} id={id} className={getButtonStyles(id)}>
        <Button
          htmlType="button"
          onClick={handleCategoryClick(id, urlName)}
          size="small"
          type="white"
          className={id}>
          {getCategoryName(name, icon)}
        </Button>
      </div>
    ),
    [getButtonStyles, handleCategoryClick, getCategoryName],
  );

  if (categoriesIsSuccess && categories.length) {
    return (
      <div ref={sliderRef} className="restaurant-cat-slider-parent-wrap">
        <div className={getMainStyles()} style={getInlineMainStyles()}>
          <Slider list={categories} renderItem={renderItem} autoScrollTrigger={active} />
        </div>
      </div>
    );
  }

  return null;
};

const CategoriesSliderMemo = memo(CategoriesSliderComponent);
export default enhance(CategoriesSliderMemo);
