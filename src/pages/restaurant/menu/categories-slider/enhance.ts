// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { restaurantCategoriesFetch } from '@actions/restaurant/menu/categories';
import { pushNotification } from '@actions/notifications';
// Selectors
import { restaurantMenuCategoriesStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/restaurant/menu';

const mapDispatchToProps = {
  restaurantCategoriesFetch,
  pushNotification,
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
