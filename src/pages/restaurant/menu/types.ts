// Main Types
import { TRestaurantCardMenuReset } from '@actions/restaurant/menu/main/action-types';
import { TRestaurantCategoriesList } from '@type/restaurants/restaurant-categories';
import {
  TRestaurantSearchProducts,
  TRestaurantSearchProductsReset,
} from '@actions/restaurant/menu/search-products/action-types';
import { TRestaurantCard } from '@type/restaurants/restaurant-card';
import { TPushNotification } from '@actions/notifications/action-types';

export interface TRestaurantMenuProps {
  categories: TRestaurantCategoriesList;
  categoriesIsSuccess: boolean;
  restaurantCardMenuReset: TRestaurantCardMenuReset;
  restaurantSearchProducts: TRestaurantSearchProducts;
  restaurantSearchProductsReset: TRestaurantSearchProductsReset;
  headerHeight: number;
  restaurantCard: TRestaurantCard;
  searchStr: string;
  searchProductsIsPending: boolean;
  searchProductsIsSuccess: boolean;
  searchProductsIsError: boolean;
  pushNotification: TPushNotification;
}
