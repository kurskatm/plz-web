// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { shopingCartUpdate } from '@actions/shoping-cart/change';
// Selectors
import { restaurantMenuProductsStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/restaurant/menu';

const mapDispatchToProps = {
  shopingCartUpdate,
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
