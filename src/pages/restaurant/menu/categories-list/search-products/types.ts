// Main Types
import { TUpdateShopingCart } from '@actions/shoping-cart/change/action-types';
import { TShopingCartModel } from '@models/shoping-cart/types';
import { TRestaurantProduct } from '@type/restaurants/restaurant-products';
import { TRestaurantCard } from '@type/restaurants/restaurant-card';
import { TProfileModel } from '@/models/profile/types';
import { TCitiesItem } from '@type/cities';

export interface TSearchProductsProps {
  searchProducts: TRestaurantProduct[];
  restaurantCard: TRestaurantCard;
  shopingCart: TShopingCartModel;
  shopingCartUpdate: TUpdateShopingCart;
  profile: TProfileModel;
  balance: string;
  city?: TCitiesItem;
  address: string;
}
