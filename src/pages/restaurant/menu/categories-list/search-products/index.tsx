// Modules
import React, { FC, memo, useMemo } from 'react';
import { useMediaQuery } from 'react-responsive';
import { useCardResize } from '@utils/useCardResize';
// Main Types
import { TRestaurantProduct } from '@type/restaurants/restaurant-products';
// Utils
import { useShopingCartUpdate } from '@utils';
// Components
import ProductItem from '../category-item/product-item';
// Enhance
import { enhance } from './enhance';
// Types
import { TSearchProductsProps } from './types';

const SearchProductsListComponent: FC<TSearchProductsProps> = ({
  searchProducts,
  restaurantCard,
  shopingCart,
  shopingCartUpdate,
  profile,
  balance,
  city,
  address,
}) => {
  const isTheeCards = useMediaQuery({
    minWidth: 1381,
  });
  const isTwoCards = useMediaQuery({
    minWidth: 640,
    maxWidth: 1380,
  });
  const isOneCard = useMediaQuery({
    maxWidth: 639,
  });

  const { setCardHeight, getCardHeight } = useCardResize({
    cardsLength: searchProducts.length,
    isOneCard,
    isTwoCards,
    isTheeCards,
  });

  const [updateShopingCart] = useShopingCartUpdate({
    shopingCart,
    shopingCartUpdate,
    restaurantCard,
    configName: 'restMenu',
    city,
    profile,
    balance,
    address,
  });

  const renderItem = useMemo(
    () => (item: TRestaurantProduct, index: number) => (
      <ProductItem
        key={item.id}
        item={item}
        shopingCart={shopingCart}
        updateShopingCart={updateShopingCart(item)}
        index={index}
        bodyHeight={getCardHeight(index)}
        setItemHeight={setCardHeight}
      />
    ),
    [shopingCart, setCardHeight, getCardHeight, updateShopingCart],
  );

  return (
    <div className="restaurant-category-item">
      <div className="restaurant-category-item__title">Результаты поиска</div>

      <div className="restaurant-category">{searchProducts.map(renderItem)}</div>
    </div>
  );
};

const SearchProductsListMemo = memo(SearchProductsListComponent);
export default enhance(SearchProductsListMemo);
