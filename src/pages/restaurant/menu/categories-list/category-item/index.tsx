// Modules
import React, { FC, memo, useMemo, useCallback, useEffect } from 'react';
import { useInView } from 'react-intersection-observer';
// import { useMediaQuery } from 'react-responsive';
// import { useCardResize } from '@utils/useCardResize';
// Utils
import { Consts, useShopingCartUpdate } from '@utils';
// Main Types
import { TRestaurantProduct } from '@type/restaurants/restaurant-products';
// Components
import ProductItem from './product-item';
// Enhance
import { enhance } from './enhance';
// Utils
import { useScrollStopListener } from './utils';
// Types
import { TCategoriesItemProps } from './types';

const { IS_CLIENT } = Consts.ENV;

const CategoriesItemComponent: FC<TCategoriesItemProps> = ({
  categoryItem,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  categoryId,
  categoryProducts,
  restaurantCard,
  shopingCart,
  shopingCartUpdate,
  scrollTriggeredBySlider,
  setScrollTriggeredBySlider,
  setCategoriesActiveness,
  headerHeight,
  city,
  profile,
  balance,
  address,
}) => {
  const { ref, inView, entry } = useInView({
    rootMargin: `-${headerHeight + 80}px 0px 0px 0px`,
  });

  // const isTheeCards = useMediaQuery({
  //   minWidth: 1381,
  // });
  // const isTwoCards = useMediaQuery({
  //   minWidth: 640,
  //   maxWidth: 1380,
  // });
  // const isOneCard = useMediaQuery({
  //   maxWidth: 639,
  // });

  // const { setCardHeight, getCardHeight } = useCardResize({
  //   cardsLength: categoryProducts?.products?.length,
  //   isOneCard,
  //   isTwoCards,
  //   isTheeCards,
  // });

  useEffect(() => {
    if (!scrollTriggeredBySlider && IS_CLIENT) {
      setCategoriesActiveness((prevState: []) => {
        const state = prevState.filter((item) => item !== categoryItem.id);
        if (inView) {
          return [...state, categoryItem.id];
        }
        return state;
      });
    }
  }, [inView, scrollTriggeredBySlider, categoryItem, setCategoriesActiveness, entry]);

  if (IS_CLIENT) {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    useScrollStopListener(window, () => setScrollTriggeredBySlider(false));
  }

  const [updateShopingCart] = useShopingCartUpdate({
    city,
    shopingCart,
    shopingCartUpdate,
    profile,
    restaurantCard,
    balance,
    address,
    configName: 'restMenu',
  });

  const renderItem = useMemo(
    () => (item: TRestaurantProduct, index: number) => (
      <ProductItem
        key={item.id}
        item={item}
        shopingCart={shopingCart}
        updateShopingCart={updateShopingCart(item)}
        category={categoryProducts?.urlName}
        index={index}
      />
    ),
    [shopingCart, categoryProducts?.urlName, updateShopingCart],
  );

  const getCategoryName = useCallback((name: string, icon: string) => {
    if (icon) {
      switch (icon) {
        case 'fire': {
          return (
            <div className="restaurant-category-name">
              {name}
              <div className="restaurant-category-name__fire" />
            </div>
          );
        }

        case 'chibbcoin': {
          return (
            <div className="restaurant-category-name">
              {name}
              <div className="restaurant-category-name__chibbcoin" />
            </div>
          );
        }

        case 'heart': {
          return (
            <div className="restaurant-category-name">
              {name}
              <div className="restaurant-category-name__heart" />
            </div>
          );
        }

        default: {
          break;
        }
      }
    }

    return name;
  }, []);

  return categoryProducts ? (
    <div ref={ref} className="restaurant-category-item">
      <div id={`rest-category-${categoryItem.id}`} className="restaurant-category-item__title">
        {getCategoryName(categoryItem.name, categoryItem.icon)}
      </div>

      <div className="restaurant-category">{categoryProducts?.products?.map(renderItem)}</div>
    </div>
  ) : null;
};

const CategoriesItemMemo = memo(CategoriesItemComponent);
export default enhance(CategoriesItemMemo);
