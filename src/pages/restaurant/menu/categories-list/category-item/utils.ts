import { useRef, useEffect } from 'react';
import { TCreateScrollStopListener, TUseScrollStopListener, TCallBack } from './types';

const createScrollStopListener: TCreateScrollStopListener = (element, callback) => {
  let removed = false;
  let handle: number = null;
  const onScroll = () => {
    if (handle) {
      clearTimeout(handle);
    }
    handle = window.setTimeout(callback, 200);
  };
  element.addEventListener('scroll', onScroll);
  return () => {
    if (removed) {
      return;
    }
    removed = true;
    if (handle) {
      clearTimeout(handle);
    }
    element.removeEventListener('scroll', onScroll);
  };
};

export const useScrollStopListener: TUseScrollStopListener = (element, callback) => {
  const callbackRef = useRef<TCallBack>();
  callbackRef.current = callback;
  useEffect(() => {
    const destroyListener = createScrollStopListener(element, () => {
      if (callbackRef.current) {
        callbackRef.current();
      }
    });
    return () => destroyListener();
  }, [element]);
};
