// Main Types
import { TUpdateShopingCart } from '@actions/shoping-cart/change/action-types';
import { TShopingCartModel } from '@models/shoping-cart/types';
import { TRestaurantCategory } from '@type/restaurants/restaurant-categories';
import { TRestaurantCategoryProducts } from '@type/restaurants/restaurant-products';
import { TRestaurantCard } from '@type/restaurants/restaurant-card';
import { TProfileModel } from '@/models/profile/types';
import { TCitiesItem } from '@type/cities';

export interface TCategoriesItemProps {
  categoryItem: TRestaurantCategory;
  categoryProducts: TRestaurantCategoryProducts;
  restaurantCard: TRestaurantCard;
  shopingCart: TShopingCartModel;
  shopingCartUpdate: TUpdateShopingCart;
  setScrollTriggeredBySlider: React.Dispatch<React.SetStateAction<boolean>>;
  setCategoriesActiveness: React.Dispatch<React.SetStateAction<string[]>>;
  headerHeight: number;
  scrollTriggeredBySlider: boolean;
  categoryId: string;
  profile: TProfileModel;
  balance: string;
  city?: TCitiesItem;
  address: string;
}

export type TCallBack = () => void;
export type TCreateScrollStopListener = (element: Window, callback: () => void) => () => void;
export type TUseScrollStopListener = (element: Window, callback: TCallBack) => void;
