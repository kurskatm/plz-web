// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { authModalShow } from '@actions/auth';
import {
  profileDeleteFavoriteProduct,
  profileSetFavoriteProduct,
} from '@actions/profile/favourite';
// Selectors
import { restaurantProductsStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/restaurant/menu';

const mapDispatchToProps = {
  authModalShow,
  profileDeleteFavoriteProduct,
  profileSetFavoriteProduct,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
