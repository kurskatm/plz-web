// Modules
import React, {
  FC,
  memo,
  useCallback,
  // useRef,
} from 'react';
import {} from 'lodash';
// Main Components
import HitCard from '@components/HitCard';
// Main types
import { TFavoriteProduct } from '@/type/new-favorites';
// Enhance
import { enhance } from './enhance';
// Types
import { ProductItemProps } from './types';

const ProductItemComponent: FC<ProductItemProps> = ({
  item,
  shopingCart,
  authModalShow,
  profile,
  profileDeleteFavoriteProduct,
  profileSetFavoriteProduct,
  category,
  index,
  bodyHeight,
  setItemHeight,
  updateShopingCart,
}) => {
  const getQuantity = useCallback(() => {
    const shopingCartItem = shopingCart.products.find(
      (product) => product.id === item.id && product.inBonusPoint === item.availableInBonusPoints,
    );

    return shopingCartItem?.count;
  }, [item, shopingCart.products]);

  const likeClickHandler = useCallback(
    (productId: string, product: TFavoriteProduct) => (
      e: Event,
      liked: boolean,
      cb: () => void,
    ) => {
      e.preventDefault();

      if (!profile.auth) {
        authModalShow(true);
      } else {
        cb();

        if (liked) {
          profileDeleteFavoriteProduct({ productId });
        } else {
          profileSetFavoriteProduct({
            productId,
            newProduct: product,
          });
        }
      }
    },
    [profile.auth, authModalShow, profileDeleteFavoriteProduct, profileSetFavoriteProduct],
  );

  return (
    <div className="restaurant-product-item">
      <HitCard
        // disabled={hasForPoints && hasForPoints.id !== item.id}
        disabled={false}
        item={item}
        // @ts-ignore Property 'restaurant' is missing in type 'TRestaurantProduct'
        onLikeClick={likeClickHandler(item.id, item)}
        updateShopingCart={updateShopingCart}
        quantity={getQuantity()}
        withLike={category !== 'za-bally'}
        index={index}
        setItemHeight={setItemHeight}
        bodyHeight={bodyHeight}
      />
    </div>
  );
};

const ProductItemMemo = memo(ProductItemComponent);
export default enhance(ProductItemMemo);
