// Main Types
import { TShopingCartModel } from '@models/shoping-cart/types';
import { TRestaurantProduct } from '@type/restaurants/restaurant-products';
import { TProfileModel } from '@/models/profile/types';
import { TAuthModalShow } from '@actions/auth/action-types';
import {
  TProfileDeleteFavoriteProducts,
  TProfileSetFavoriteProducts,
} from '@actions/profile/favourite/action-types';

export interface ProductItemProps {
  item: TRestaurantProduct;
  shopingCart: TShopingCartModel;
  authModalShow: TAuthModalShow;
  profile: TProfileModel;
  profileDeleteFavoriteProduct: TProfileDeleteFavoriteProducts;
  profileSetFavoriteProduct: TProfileSetFavoriteProducts;
  category?: string;
  index?: number;
  bodyHeight?: number;
  setItemHeight?: (height: number, index: number) => void;
  updateShopingCart: (quantity: number) => void;
}
