// Modules
import React, { FC, memo, useEffect, useMemo, useRef, useState } from 'react';
// Main Types
import { TRestaurantCategory } from '@type/restaurants/restaurant-categories';
// Components
import CategoriesItem from './category-item';
import SearchProducts from './search-products';
// Enhance
import { enhance } from './enhance';
// Types
import { TCategoriesListProps } from './types';

const CategoriesListComponent: FC<TCategoriesListProps> = ({
  active,
  setActive,
  categories,
  categoriesIsSuccess,
  productsIsIdle,
  productsIsSuccess,
  restaurantProductsFetch,
  restaurantCard,
  setCategoriesParam,
  scrollToCategory,
  searchProducts,
  searchProductsIsSuccess,
  scrollTriggeredBySlider,
  setScrollTriggeredBySlider,
}) => {
  const setActiveCallback = useRef((item: TRestaurantCategory) => {
    const { id, urlName } = item;
    setActive(id);
    setCategoriesParam(urlName, 'replaceIn');
  });

  const [categoriesActiveness, setCategoriesActiveness] = useState([]);

  useEffect(() => {
    if (categoriesActiveness.length) {
      const activeCategories = categories.filter((category) =>
        categoriesActiveness.includes(category.id),
      );
      setActiveCallback.current(activeCategories[0]);
    }
  }, [categoriesActiveness, categories]);

  useEffect(
    () => {
      // if (categoriesIsIdle && restaurantCard) {
      //   restaurantCategoriesFetch({
      //     restaurantId: restaurantCard.id,
      //     storeId: restaurantCard.storeId,
      //   });
      // }

      if (productsIsIdle && restaurantCard) {
        restaurantProductsFetch({
          restaurantId: restaurantCard.id,
          storeId: restaurantCard.storeId,
        });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const renderItem = useMemo(
    () => (item: TRestaurantCategory) => (
      <CategoriesItem
        key={item.id}
        categoryId={item.id}
        categoryItem={item}
        restaurantCard={restaurantCard}
        scrollTriggeredBySlider={scrollTriggeredBySlider}
        setScrollTriggeredBySlider={setScrollTriggeredBySlider}
        setCategoriesActiveness={setCategoriesActiveness}
      />
    ),
    [restaurantCard, setScrollTriggeredBySlider, scrollTriggeredBySlider],
  );

  useEffect(
    () => {
      if (categoriesIsSuccess && productsIsSuccess && categories.length) {
        scrollToCategory(active);
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  if (searchProducts.length) {
    return (
      <div className="restaurant-categories-list">
        <SearchProducts searchProducts={searchProducts} restaurantCard={restaurantCard} />
      </div>
    );
  }

  if (searchProductsIsSuccess && !searchProducts.length) {
    return (
      <>
        <div className="home-restaurants__empty-list" />
        <div className="home-restaurants__empty-list-text">По этому запросу ничего не найдено</div>
      </>
    );
  }

  if (categoriesIsSuccess && productsIsSuccess && categories.length) {
    return <div className="restaurant-categories-list">{categories.map(renderItem)}</div>;
  }

  return null;
};

const CategoriesListMemo = memo(CategoriesListComponent);
export default enhance(CategoriesListMemo);
