// Main Types
import { TRestaurantProduct } from '@/type/restaurants/restaurant-products';
import { TRestaurantCardCategoriesFetch } from '@actions/restaurant/menu/categories/action-types';
import { TRestaurantCardProductsFetch } from '@actions/restaurant/menu/products/action-types';
import { TRestaurantCard } from '@type/restaurants/restaurant-card';
import { TRestaurantCategoriesList } from '@type/restaurants/restaurant-categories';

export interface TCategoriesListProps {
  active: string | null;
  setActive: React.Dispatch<React.SetStateAction<string>>;
  categories: TRestaurantCategoriesList;
  categoriesIsIdle: boolean;
  categoriesIsSuccess: boolean;
  productsIsIdle: boolean;
  productsIsSuccess: boolean;
  restaurantProductsFetch: TRestaurantCardProductsFetch;
  restaurantCard: TRestaurantCard;
  restaurantCategoriesFetch: TRestaurantCardCategoriesFetch;
  setCategoriesParam: (newValue: string, updateType?: string) => void;
  scrollToCategory: (id: string) => void;
  searchProducts: TRestaurantProduct[];
  searchProductsIsSuccess: boolean;
  scrollTriggeredBySlider: boolean;
  setScrollTriggeredBySlider: React.Dispatch<React.SetStateAction<boolean>>;
}
