// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import {
  restaurantCategoriesFetch,
  restaurantCategoriesReset,
} from '@actions/restaurant/menu/categories';
import {
  restaurantProductsFetch,
  restaurantProductsReset,
} from '@actions/restaurant/menu/products';
// Selectors
import { restaurantMenuCategoriesListStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/restaurant/menu';

const mapDispatchToProps = {
  restaurantCategoriesFetch,
  restaurantProductsFetch,
  restaurantProductsReset,
  restaurantCategoriesReset,
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
