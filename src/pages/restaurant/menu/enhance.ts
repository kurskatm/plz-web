// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { restaurantCardMenuReset } from '@actions/restaurant/menu/main';
import { restaurantCategoriesReset } from '@actions/restaurant/menu/categories';
import { restaurantProductsReset } from '@actions/restaurant/menu/products';
import {
  restaurantSearchProducts,
  restaurantSearchProductsReset,
} from '@actions/restaurant/menu/search-products';
import { pushNotification } from '@actions/notifications';
// Selectors
import { restaurantMenuStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/restaurant/menu';

const mapDispatchToProps = {
  restaurantCardMenuReset,
  restaurantProductsReset,
  restaurantCategoriesReset,
  restaurantSearchProducts,
  restaurantSearchProductsReset,
  pushNotification,
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
