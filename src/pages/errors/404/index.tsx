// Modules
import React, { memo, FC } from 'react';
// Components
import HeaderError from '@components/Header/Errors';
import FooterErrors from '@components/Footer/Errors';
import BackButton from '../BackButton';

const Error404Component: FC = () => (
  <div className="error404-page">
    <HeaderError />
    <div className="error404-page__content-wrap">
      <div className="error404-page__img" />
      <div className="error-page__back">
        <BackButton />
      </div>
    </div>
    <FooterErrors />
  </div>
);

export default memo(Error404Component);
