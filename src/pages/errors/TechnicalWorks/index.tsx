// Modules
import React, { memo, FC } from 'react';
// Components
import HeaderError from '@components/Header/Errors';
import FooterErrors from '@components/Footer/Errors';

const Error500Component: FC = () => (
  <div className="error-technical-works-page">
    <HeaderError />
    <div className="error-technical-works-page__content-wrap">
      <div className="error-technical-works-page__img" />
      <div className="error-technical-works-page__title">На сайте ведутся технические работы</div>
      <div className="error-technical-works-page__text">Возвращайтесь через 1 час 40 минут</div>
    </div>
    <FooterErrors />
  </div>
);

export default memo(Error500Component);
