// Modules
import React, { memo, FC } from 'react';
// Components
import HeaderError from '@components/Header/Errors';
import FooterErrors from '@components/Footer/Errors';

const Error500Component: FC = () => (
  <div className="error502-page">
    <HeaderError />
    <div className="error502-page__content-wrap">
      <div className="error502-page__img" />
    </div>
    <FooterErrors />
  </div>
);

export default memo(Error500Component);
