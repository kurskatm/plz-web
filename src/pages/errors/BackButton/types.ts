// Modules
import { History } from 'history';

export interface TBackButtonProps {
  history: History;
}
