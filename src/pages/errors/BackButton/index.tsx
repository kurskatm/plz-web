// Modules
import React, { memo, FC, useCallback } from 'react';
import { Button } from 'chibbis-ui-kit';
// Consts
import { Consts } from '@utils/consts';
// Enhance
import { enhance } from './enhance';
// Types
import { TBackButtonProps } from './types';

const { ROUTES } = Consts;

const BackButtonComponent: FC<TBackButtonProps> = ({ history }) => {
  const onClickBack = useCallback(() => {
    history.push(ROUTES.CITIES.PATH);
  }, [history]);

  return <Button onClick={onClickBack}>Вернуться на главную</Button>;
};

const BackButtonMemo = memo(BackButtonComponent);
export default enhance(BackButtonMemo);
