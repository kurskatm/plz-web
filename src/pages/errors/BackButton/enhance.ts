// Modules
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';

export const enhance = compose(withRouter);
