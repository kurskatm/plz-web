// Modules
import React, { FC, useCallback, memo, useMemo } from 'react';
// import loadable from '@loadable/component';
import { Link, FavoriteIcon } from 'chibbis-ui-kit';
import classNames from 'classnames';
import Slider from '@components/Slider';
// Types
import { TProfileMenuProps, TMenuItem, TMenuList } from './types';

const MENU_ITEMS: TMenuList = [
  {
    title: 'Профиль',
    url: 'info',
  },
  {
    title: 'Заказы',
    url: 'orders',
  },
  {
    title: 'Баллы',
    url: 'scores',
  },
  {
    title: 'Отзывы',
    url: 'reviews',
  },
  {
    url: 'favorite',
    icon: 'favorite',
  },
];

const ProfileMenuComponent: FC<TProfileMenuProps> = ({ location }) => {
  const getStyles = useCallback(
    (currentItem) =>
      classNames('profile-menu__item', { 'is-active': currentItem.indexOf(location) !== -1 }),
    [location],
  );

  const renderItem = useMemo(
    () => (item: TMenuItem) => (
      <div
        className={getStyles(item.url)}
        key={`${item.title}-${item.url}-${item.icon}`}
        id={item.url}>
        <Link noborder to={`${item.url}`} className="profile-menu__link">
          {item.title ? item.title : null}
          {item.icon ? <FavoriteIcon /> : null}
        </Link>
      </div>
    ),
    [getStyles],
  );

  return (
    <div className="profile-menu">
      <Slider
        list={MENU_ITEMS}
        renderItem={renderItem}
        className="profile-menu__slider"
        autoScrollTrigger={location}
      />
    </div>
  );
};

export default memo(ProfileMenuComponent);
