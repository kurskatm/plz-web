export type TProfileMenuProps = {
  location: string;
};

export interface TMenuItem {
  title?: string;
  url?: string;
  icon?: string;
}

export type TMenuList = TMenuItem[];
