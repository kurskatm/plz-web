export type TMailChecker = {
  welcomeText?: string;
  successText?: string;
  checked: boolean;
};
