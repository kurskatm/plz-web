import React, { FC, memo, useCallback, useMemo } from 'react';
import classnames from 'classnames';
import { useHistory } from 'react-router';
import { useParams } from 'react-router-dom';
import urlJoin from 'url-join';
// Icons
import { ScoresIcon } from 'chibbis-ui-kit';
// Utils
import { Consts } from '@utils';
// Main Types
import { TCityRouterParams } from '@type/routes';
// Types
import { TMailChecker } from './types';

const { PATH } = Consts.ROUTES.PROFILE_PAGES.SCORES;

const MailCheckerComponent: FC<TMailChecker> = ({ checked }) => {
  const history = useHistory();
  const { cityUrl } = useParams<TCityRouterParams>();

  const renderUnchecked = () => (
    <span>
      Подтвердите почту и получите промокод на&nbsp;
      <span className="mail-checker__text">
        <ScoresIcon />
        &thinsp;500 баллов
      </span>
    </span>
  );
  const getStyles = useCallback(
    () =>
      classnames('mail-checker', {
        'mail-checker_is-checked': checked,
      }),
    [checked],
  );

  const onClick = useCallback(() => {
    if (!checked) {
      history.push(urlJoin(`/${cityUrl}`, `${PATH}#howtoget`));
    }
  }, [history, cityUrl, checked]);

  const getContent = useMemo(() => (checked ? '✓️️ Почта подтверждена' : renderUnchecked()), [
    checked,
  ]);

  return (
    <div className={getStyles()} role="presentation" onClick={() => onClick()}>
      {getContent}
    </div>
  );
};

export default memo(MailCheckerComponent);
