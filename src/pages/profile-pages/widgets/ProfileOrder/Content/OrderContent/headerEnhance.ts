// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { restaurantCardIdSave } from '@/actions/restaurant/main/id';
import { profileCityUpdate, profileResetDataAferCityUpdate } from '@actions/profile/city';
// Selectors
import { cityCheckStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/cities-list';

const mapDispatchToProps = {
  restaurantCardIdSave,
  profileCityUpdate,
  profileResetDataAferCityUpdate,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
