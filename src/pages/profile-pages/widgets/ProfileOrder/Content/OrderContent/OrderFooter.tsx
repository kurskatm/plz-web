/* eslint-disable max-len */
/* eslint-disable no-nested-ternary */
// Modules
import React, { FC, memo, useCallback, useMemo, useState, useEffect } from 'react';
import { useMediaQuery } from 'react-responsive';
import { Button } from 'chibbis-ui-kit';
import isString from 'lodash/isString';
import isNil from 'lodash/isNil';
import dayjs from 'dayjs';
import classnames from 'classnames';
import { useParams } from 'react-router-dom';
// Main Types
import { TCityRouterParams } from '@type/routes';
// Utils
import { useCounter, secondsToMinutes } from '@utils';
// Enhance
import { enhance } from './footerEnhance';
// Types
import { TOrderFooterProps } from './types';
import { TIMER_COUNTER, TIMER_DELAY } from './constants';

const OrderFooterComponent: FC<TOrderFooterProps> = ({
  isFull,
  orderId,
  profileShowOrdersReviewModal,
  reviewId,
  dateAdded,
  // contact me api
  contactMeIsPending,
  contactMeIsError,
  contactMeIsSuccess,
  contactMeOrderId,
  contactMeStatusDetails,
  profileOrderContactMe,
  profileOrderContactMeReset,
  // repeat order api
  geocode,
  repeatOrderId,
  repeatOrderIsPending,
  repeatOrderIsError,
  profileOrderRepeatOrder,
  profileOrderRepeatOrderReset,
  showRestaurantModals,
  shopingCart,
  cityId,
  orderContactMeClicked,
  setOrderContactMeClicked,
  cityUrlNameFromOrder,
}) => {
  const isTablet = useMediaQuery({
    maxWidth: 768,
  });

  const { cityUrl } = useParams<TCityRouterParams>();

  const [showRepeatOrderMessage, setShowRepeatOrderMessage] = useState<boolean>(false);
  const [currentTick] = useCounter({
    secondsCount:
      contactMeIsError && Boolean(contactMeStatusDetails?.retryAfter)
        ? contactMeStatusDetails?.retryAfter
        : TIMER_COUNTER,
    secondsCountInitial: TIMER_COUNTER,
    delayMs: TIMER_DELAY,
    trigger:
      contactMeIsSuccess || (contactMeIsError && Boolean(contactMeStatusDetails?.retryAfter)),
    callBack: () => {
      profileOrderContactMeReset({ orderId: contactMeOrderId });
    },
  });

  useEffect(() => {
    if (!isFull && repeatOrderId.length && repeatOrderId === orderId) {
      if (repeatOrderIsError) {
        setShowRepeatOrderMessage(true);
        setTimeout(() => {
          profileOrderRepeatOrderReset();
          setShowRepeatOrderMessage(false);
        }, 60000);
      }
    }
  }, [isFull, orderId, profileOrderRepeatOrderReset, repeatOrderId, repeatOrderIsError]);

  const getReviewButtonDisabled = useCallback(() => {
    if (isString(reviewId) && reviewId.length) {
      return true;
    }

    if (dayjs().diff(dayjs(dateAdded), 'day') > 27) {
      return true;
    }

    return false;
  }, [reviewId, dateAdded]);

  const getRepeatOrderDisabled = useCallback(() => {
    if (
      ((repeatOrderIsPending || repeatOrderIsError) && showRepeatOrderMessage) ||
      (cityUrlNameFromOrder.length && cityUrl.length && cityUrlNameFromOrder !== cityUrl)
    ) {
      return true;
    }

    return false;
  }, [
    repeatOrderIsError,
    repeatOrderIsPending,
    showRepeatOrderMessage,
    cityUrlNameFromOrder,
    cityUrl,
  ]);

  const onClickShowModal = useCallback(() => {
    profileShowOrdersReviewModal({
      showModal: true,
      orderId,
    });
  }, [orderId, profileShowOrdersReviewModal]);

  const onContactMeClick = useCallback(() => {
    setOrderContactMeClicked(orderId);
    profileOrderContactMe({
      orderId,
    });
  }, [orderId, profileOrderContactMe, setOrderContactMeClicked]);

  const onRepeatOrderClick = useCallback(() => {
    if (isNil(geocode)) {
      showRestaurantModals({
        showModal: true,
        modalType: 'change-address',
      });
    } else {
      profileOrderRepeatOrder({
        orderId,
        lat: geocode.lat,
        lng: geocode.lng,
        shopingCart,
        cityId,
      });
    }
  }, [geocode, orderId, shopingCart, cityId, profileOrderRepeatOrder, showRestaurantModals]);

  const renderSecondButton = useMemo(() => {
    if (isFull) {
      return (
        <Button
          htmlType="button"
          onClick={onContactMeClick}
          size="small"
          type="primary"
          disabled={(contactMeIsSuccess || contactMeIsError) && contactMeOrderId === orderId}
          loading={contactMeIsPending && orderContactMeClicked === orderId}
          className={classnames('profile-order__footer-content__btn-second', {
            'profile-order__footer-content__btn-second-success':
              contactMeIsSuccess && contactMeOrderId === orderId,
            'profile-order__footer-content__btn-second-fail':
              contactMeIsError && contactMeOrderId === orderId,
          })}>
          {contactMeIsSuccess && contactMeOrderId === orderId
            ? 'запрос отправлен'
            : contactMeIsError && contactMeOrderId === orderId
            ? 'запрос с ошибкой'
            : 'cвяжитесь со мной'}
        </Button>
      );
    }

    return (
      <Button
        htmlType="button"
        onClick={onRepeatOrderClick}
        size="small"
        type="primary"
        disabled={getRepeatOrderDisabled()}>
        Повторить
      </Button>
    );
  }, [
    isFull,
    onContactMeClick,
    contactMeIsPending,
    contactMeIsError,
    contactMeIsSuccess,
    contactMeOrderId,
    orderContactMeClicked,
    orderId,
    getRepeatOrderDisabled,
    onRepeatOrderClick,
  ]);

  const getFooterStyles = useCallback(() => {
    if (isFull && isTablet) {
      return {
        gridTemplateRows: 'auto auto',
        gridTemplateColumns: 'auto',
        gridRowGap: 16,
      };
    }

    if (!isFull && isTablet) {
      return {
        gridTemplateRows: 'auto',
        gridTemplateColumns: 'auto auto',
        gridColumnGap: 16,
      };
    }

    return {
      gridTemplateRows: 'auto',
      gridTemplateColumns: 'auto auto',
      gridColumnGap: 24,
    };
  }, [isFull, isTablet]);

  return (
    <>
      <div className="profile-order__footer-content" style={getFooterStyles()}>
        <Button
          htmlType="button"
          onClick={onClickShowModal}
          size="small"
          type="secondary"
          disabled={getReviewButtonDisabled()}>
          Оценить
        </Button>
        {renderSecondButton}
      </div>
      <div className="checkout-success-page__call-me__message">
        <div>
          {(contactMeIsSuccess || contactMeIsError) &&
            contactMeOrderId === orderId &&
            (Boolean(contactMeStatusDetails?.retryAfter) || contactMeIsSuccess) &&
            `Повторить запрос можно через ${secondsToMinutes(currentTick)}`}
        </div>
      </div>
      <div className="checkout-success-page__call-me__message">
        <div className="checkout-success-page__call-me__message__text">
          {contactMeIsError &&
            contactMeOrderId === orderId &&
            `${contactMeStatusDetails?.errorMessage}`}
        </div>
      </div>
      {showRepeatOrderMessage && (
        <div
          className="profile-order-footer__contact-me-message"
          style={{
            color: '#E73232',
          }}>
          Возникла ошибка. Попробуйте позже
        </div>
      )}
    </>
  );
};

const OrderFooterMemo = memo(OrderFooterComponent);
export default enhance(OrderFooterMemo);
