// Main types
import { TUserOrder, TModifiersCategoryDataArray } from '@type/profile';
import { TProfileShowOrdersReviewModal } from '@actions/profile/action-types';
import {
  TProfileOrderContactMe,
  TProfileOrderContactMeReset,
} from '@actions/profile/contact-me/action-types';
import { TContactMeDetailsModel } from '@/models/profile/types';
import {
  TProfileOrderRepeatOrder,
  TProfileOrderRepeatOrderReset,
} from '@actions/profile/repeat-order/action-types';
import { TShowRestaurantModals } from '@actions/restaurant/modal/action-types';
import { TShopingCartModel } from '@/models/shoping-cart/types';
import { TRestaurantCardIdSave } from '@actions/restaurant/main/id/action-types';
import { TNewGeocodeData } from '@type/new-geocode';
import {
  TProfileCityUpdate,
  TProfileResetDataAferCityUpdate,
} from '@actions/profile/city/action-types';
import { TCitiesItem, TCitiesList } from '@type/cities';

export interface TOrderShortContentProps {
  info: string | string[];
}

export interface TOrderFullContentProps {
  order: TUserOrder;
}

export interface TOrderHeaderProps {
  title: string;
  status: number;
  cost: number;
  logo: string;
  date: string;
  city: string;
  restId: string;
  restUrlName: string;
  restaurantCardIdSave: TRestaurantCardIdSave;
  profileCityUpdate: TProfileCityUpdate;
  profileResetDataAferCityUpdate: TProfileResetDataAferCityUpdate;
  cityItem: TCitiesItem;
  cities: TCitiesList;
}

export interface TOrderFooterProps {
  isFull: boolean;
  orderId: string;
  reviewId: string | null;
  dateAdded: string;
  profileShowOrdersReviewModal: TProfileShowOrdersReviewModal;
  profileOrderContactMe: TProfileOrderContactMe;
  profileOrderContactMeReset: TProfileOrderContactMeReset;
  isAvailableToContactMe: boolean;
  // contact me
  contactMeIsPending: boolean;
  contactMeIsError: boolean;
  contactMeIsSuccess: boolean;
  contactMeOrderId: string;
  contactMeStatusDetails: TContactMeDetailsModel;
  // repeat order
  repeatOrderIsPending: boolean;
  repeatOrderIsError: boolean;
  geocode: TNewGeocodeData;
  repeatOrderId: string;
  profileOrderRepeatOrder: TProfileOrderRepeatOrder;
  profileOrderRepeatOrderReset: TProfileOrderRepeatOrderReset;
  showRestaurantModals: TShowRestaurantModals;
  shopingCart: TShopingCartModel;
  cityId: string;
  orderContactMeClicked: string;
  setOrderContactMeClicked: React.Dispatch<React.SetStateAction<string>>;
  cityUrlNameFromOrder: string;
}

export { TModifiersCategoryDataArray };
