// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { profileShowOrdersReviewModal } from '@actions/profile';
import { profileOrderContactMe, profileOrderContactMeReset } from '@actions/profile/contact-me';
import {
  profileOrderRepeatOrder,
  profileOrderRepeatOrderReset,
} from '@actions/profile/repeat-order';
import { showRestaurantModals } from '@actions/restaurant/modal';
// Selectors
import { profileOrderFullComponentFooter as mapStateToProps } from '@selectors/structured-selectors/profile';

const mapDispatchToProps = {
  profileShowOrdersReviewModal,
  profileOrderContactMe,
  profileOrderContactMeReset,
  profileOrderRepeatOrder,
  profileOrderRepeatOrderReset,
  showRestaurantModals,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
