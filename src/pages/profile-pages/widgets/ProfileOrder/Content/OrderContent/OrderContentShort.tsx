import React, { FC, memo } from 'react';
// Types
import { TOrderShortContentProps } from './types';

const OrderContentShortComponent: FC<TOrderShortContentProps> = ({ info }) => (
  <div className="profile-order-content-short">
    <div className="profile-order-content-short__info">{info}</div>
  </div>
);

export default memo(OrderContentShortComponent);
