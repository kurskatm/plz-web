import React, { FC, useCallback, memo } from 'react';
import classNames from 'classnames';
import dayjs from 'dayjs';
import { useLocation, useHistory } from 'react-router';
// Lib
import { removeAddress, removeGeoCode, setCookieCityData } from '@lib/cookie';
// Utils
import { applyImageFallback } from '@/utils';
// Assets
import cardRestDefault from '@/../assets/svg/main/card-rest-default.svg';
// Enhance
import { enhance } from './headerEnhance';
// Types
import { TOrderHeaderProps } from './types';

const ProfileOrdersPath = new RegExp('/[a-z]*/profile/orders');

const OrderHeaderComponent: FC<TOrderHeaderProps> = ({
  title,
  status,
  cost,
  logo,
  date,
  city,
  restId,
  restUrlName,
  restaurantCardIdSave,
  // change city
  cities,
  cityItem,
  profileCityUpdate,
  profileResetDataAferCityUpdate,
}) => {
  const location = useLocation();
  const history = useHistory();

  const getPayedStyles = useCallback(
    () =>
      classNames('profile-order-header__payed', {
        'is-payed': status === 2,
        'is-cancelled': status === 3,
        'is-in-progress':
          status === 0 || status === 1 || status === 4 || status === 5 || status === 6,
      }),
    [status],
  );

  const getPayedText = useCallback(() => {
    if (status === 1 && location.pathname.match(ProfileOrdersPath)) {
      return 'В процессе';
    }

    if (status === 2 && location.pathname.match(ProfileOrdersPath)) {
      return 'Доставлен';
    }
    if (status === 3 && location.pathname.match(ProfileOrdersPath)) {
      return 'Отменён';
    }

    return '';
  }, [status, location.pathname]);

  const getTime = useCallback(() => {
    const day = dayjs(date).locale('ru').format('DD MMMM');
    const time = dayjs(date).locale('ru').format('HH:mm');

    return `${day} в ${time}`;
  }, [date]);

  const checkCityUrlNameFnc = useCallback(
    (urlNameProps: string) => {
      if (urlNameProps !== cityItem.urlName) {
        const newCity = cities.find(({ urlName }) => urlName === urlNameProps);
        profileCityUpdate(newCity.id);
        setCookieCityData(newCity.id);
        removeAddress();
        removeGeoCode();
        profileResetDataAferCityUpdate();
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [cities, cityItem.urlName],
  );

  const onLinkClick = useCallback(() => {
    checkCityUrlNameFnc(city);
    restaurantCardIdSave({ id: restId });
    history.push(`/${city}/restaurant/${restUrlName}`, { from: 'orders' });
  }, [city, history, restId, restUrlName, restaurantCardIdSave, checkCityUrlNameFnc]);

  return (
    <div className="profile-order-header">
      <img
        src={logo || cardRestDefault}
        onError={applyImageFallback(cardRestDefault)}
        alt={title}
        className="profile-order-header__logo"
        onClick={onLinkClick}
        role="presentation"
      />
      <div className="profile-order-header__row">
        <div className="profile-order-header__title" onClick={onLinkClick} role="presentation">
          {title}
        </div>
        <div className="profile-order-header__cost">{`${cost} ₽`}</div>
      </div>
      <div className="profile-order-header__row">
        <div className="profile-order-header__date">{getTime()}</div>
        <div className={getPayedStyles()}>{getPayedText()}</div>
      </div>
    </div>
  );
};

const OrderHeaderMemo = memo(OrderHeaderComponent);
export default enhance(OrderHeaderMemo);
