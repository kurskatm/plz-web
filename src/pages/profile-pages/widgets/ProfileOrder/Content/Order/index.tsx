import React, { useState, useCallback, FC, memo } from 'react';
import classNames from 'classnames';
// Components
import OrderHeader from '../OrderContent/OrderHeader';
import OrderFooter from '../OrderContent/OrderFooter';
import OrderShortContent from '../OrderContent/OrderContentShort';
import OrderFullContent from '../OrderContent/OrderContentFull';
// Types
import { TOrderProps } from './types';

const OrderComponent: FC<TOrderProps> = ({
  order,
  orderContactMeClicked,
  setOrderContactMeClicked,
}) => {
  const [isFull, setFull] = useState(false);
  const toggleFull = useCallback(() => {
    setFull(!isFull);
  }, [isFull]);
  const getStyles = useCallback(
    () =>
      classNames('order__control', {
        'is-full': isFull,
      }),
    [isFull],
  );

  const getShortInfo = useCallback(() => {
    if (order.order.products.length > 1) {
      return order.order.products
        .map((item) => {
          if (item.quantity === 1) {
            return item.name;
          }
          return `${item.quantity} × ${item.name}`;
        })
        .join(', ');
    }

    return order.order.products.map((item) => {
      if (item.quantity === 1) {
        return item.name;
      }
      return `${item.quantity} × ${item.name}`;
    });
  }, [order.order.products]);
  return (
    <div className="order">
      <div className="order__header">
        <OrderHeader
          title={order.restaurant.restaurantName || ''}
          status={order.order.status}
          date={order.order.addedOn}
          cost={order.order.orderCost.total}
          logo={order.restaurant.restaurantLogo || ''}
          restId={order.restaurant.id}
          restUrlName={order.restaurant.urlName}
          city={order.restaurant.cityUrlName}
        />
      </div>
      <div className="order__content">
        {isFull ? <OrderFullContent order={order} /> : <OrderShortContent info={getShortInfo()} />}
        <div role="presentation" onClick={toggleFull} className={getStyles()}>
          {isFull ? 'Коротко' : 'Подробнее'}
        </div>
        <div className="profile-order__footer">
          <OrderFooter
            isFull={isFull}
            orderId={order.order.id}
            reviewId={order.order.reviewId}
            dateAdded={order.order.addedOn}
            isAvailableToContactMe={order.order.isContactCustomerAvailable}
            orderContactMeClicked={orderContactMeClicked}
            setOrderContactMeClicked={setOrderContactMeClicked}
            cityUrlNameFromOrder={order.restaurant.cityUrlName}
          />
        </div>
      </div>
    </div>
  );
};

export default memo(OrderComponent);
