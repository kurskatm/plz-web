import React, { FC, memo } from 'react';
// Components
import Order from './Content/Order';
// import OrderFooter from './Content/OrderFooter';
// Types
import { TOrderProps } from './types';

const ProfileOrderComponent: FC<TOrderProps> = ({
  order,
  orderContactMeClicked,
  setOrderContactMeClicked,
}) => (
  <div className="profile-order profile-widget">
    <Order
      order={order}
      orderContactMeClicked={orderContactMeClicked}
      setOrderContactMeClicked={setOrderContactMeClicked}
    />
  </div>
);

export default memo(ProfileOrderComponent);
