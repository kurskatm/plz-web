// Main types
import { TUserOrder } from '@type/profile';

export interface TOrderProps {
  order: TUserOrder;
  orderContactMeClicked: string;
  setOrderContactMeClicked: React.Dispatch<React.SetStateAction<string>>;
}
