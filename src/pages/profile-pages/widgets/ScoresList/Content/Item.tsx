import React, { FC, memo, useCallback } from 'react';
// Icons
import { ScoresIcon } from 'chibbis-ui-kit';
// Lib
import { getDate } from '@lib/date/get-date';
// Types
import { TScoresListItem } from '../types';

const ScoresListItemComponent: FC<TScoresListItem> = ({
  description,
  balanceDifference,
  loggedOn,
}) => {
  const getDateCallback = useCallback((date: string) => getDate(date), []);

  const getBalance = useCallback((balance: string) => {
    if (String(balance)[0] === '-') {
      return `${String(balance)[0]} ${String(balance).slice(1)}`;
    }

    return `+ ${balance}`;
  }, []);

  return (
    <div className="scores-list__item">
      <div className="scores-list__col">
        <div className="scores-list__text">{description}</div>
        <div className="scores-list__date">{getDateCallback(loggedOn)}</div>
      </div>
      <div className="scores-list__col">
        <div className="scores-list__scores">
          {getBalance(balanceDifference)}
          &nbsp;
          <ScoresIcon width={16} height={16} />
        </div>
      </div>
    </div>
  );
};

export default memo(ScoresListItemComponent);
