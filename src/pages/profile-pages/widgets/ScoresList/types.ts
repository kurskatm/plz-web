export interface TScoresListItem {
  description: string;
  balanceDifference: string;
  loggedOn: string;
}
export interface TScoresListProps {
  list: TScoresListItem[];
}
