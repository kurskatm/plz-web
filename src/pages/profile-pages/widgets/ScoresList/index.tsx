import React, { FC, useMemo, memo } from 'react';
// Components
import ScoresListItemComponent from './Content/Item';

// Types
import { TScoresListProps, TScoresListItem } from './types';

const ScoresListComponent: FC<TScoresListProps> = ({ list = [] }) => {
  const renderScoresList = useMemo(
    () => (
      <div className="scores-list__list">
        {list.map((score: TScoresListItem) => (
          <ScoresListItemComponent
            key={`${score.balanceDifference} - ${score.loggedOn}`}
            {...score}
          />
        ))}
      </div>
    ),
    [list],
  );

  return (
    <div className="scores-list">
      <div className="scores-list__section">{renderScoresList}</div>
    </div>
  );
};

export default memo(ScoresListComponent);
