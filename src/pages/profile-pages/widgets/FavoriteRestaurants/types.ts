import { TFavoriteRestaurant } from '@/type/new-favorites';
import { TRestaurantCardPropsList } from '@components/RestaurantCard/types';
import { TCitiesItem } from '@type/cities';

export interface TFavoriteRestaurantsProps {
  restaurants: TRestaurantCardPropsList;
  likeClickHandler: (
    id: string,
    newRest: TFavoriteRestaurant,
  ) => (e: MouseEvent, liked: boolean) => void;
  city: TCitiesItem;
}
