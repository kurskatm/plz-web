import React, { FC, useMemo, useCallback, memo } from 'react';
// Components
import ContentViewer from '@components/ContentViewer';
import RestaurantCard from '@components/NewRestaurantCard';
// Types
import { TRestaurantCard } from '@type/restaurants/restaurant-card';
import { TFavoriteRestaurantsProps } from './types';

const CardWrapper: FC = ({ children }) => (
  <div className="favorite-restaurants__list">{children}</div>
);

const FavoriteRestaurantsComponent: FC<TFavoriteRestaurantsProps> = ({
  restaurants,
  likeClickHandler,
  city,
}) => {
  const getUrl = useCallback((urlName: string) => `/${city?.urlName}/restaurant/${urlName}`, [
    city?.urlName,
  ]);

  const renderItem = useMemo(
    () => (item: TRestaurantCard) => (
      <RestaurantCard
        // @ts-ignore Property 'banner' is missing in type 'TRestaurantCard'
        onLikeClick={likeClickHandler(item.id, item)}
        key={item.id}
        item={{ ...item, isFavorite: true }}
        url={getUrl(item.urlName)}
      />
    ),
    [getUrl, likeClickHandler],
  );

  return (
    <div className="favorite-restaurants">
      <div className="favorite-restaurants__section">
        <div className="favorite-restaurants__title">Любимые рестораны</div>
        <ContentViewer
          list={restaurants}
          renderItem={renderItem}
          responsive={{
            desktop: 6,
            tablet: 4,
            mobile: 4,
          }}
          wrapper={CardWrapper}
        />
      </div>
    </div>
  );
};

export default memo(FavoriteRestaurantsComponent);
