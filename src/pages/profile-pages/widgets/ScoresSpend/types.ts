import { TRestaurantCard } from '@type/restaurants/restaurant-card';
import { TCitiesItem } from '@type/cities';
import { TProfileModel } from '@models/profile/types';
import {
  TProfileDeleteFavoriteRests,
  TProfileSetFavoriteRests,
} from '@actions/profile/favourite/action-types';
import { TShopingCartModel } from '@models/shoping-cart/types';
import { TUpdateShopingCart } from '@actions/shoping-cart/change/action-types';
import { TProductFodForPoints } from '@type/foodForPoints';

export interface TScoresSpendProps {
  rest: TRestaurantCard[];
  food: TProductFodForPoints[];
  city: TCitiesItem;
  profile: TProfileModel;
  profileDeleteFavoriteRests: TProfileDeleteFavoriteRests;
  profileSetFavoriteRests: TProfileSetFavoriteRests;
  authModalShow: (data: boolean) => void;
  shopingCart: TShopingCartModel;
  shopingCartUpdate: TUpdateShopingCart;
}
