// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { authModalShow } from '@actions/auth';
import { profileDeleteFavoriteRests, profileSetFavoriteRests } from '@actions/profile/favourite';
import { shopingCartUpdate } from '@actions/shoping-cart/change';

// Selectors
import { profileScoresSpendSelector as mapStateToProps } from '@selectors/structured-selectors/profile';

const mapDispatchToProps = {
  authModalShow,
  profileDeleteFavoriteRests,
  profileSetFavoriteRests,
  shopingCartUpdate,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
