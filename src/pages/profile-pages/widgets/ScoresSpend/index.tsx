import React, { FC, useMemo, memo, useCallback } from 'react';
import { useMediaQuery } from 'react-responsive';
import { useCardResize } from '@utils/useCardResize';
// lib
import { likeClickHandler } from '@lib/like';
// Components
import ContentViewer from '@components/ContentViewer';
import HitCard from '@components/HitCard';
import RestaurantCard from '@components/NewRestaurantCard';
import ModifiersCardModal from '@components/RestaurantModals/ModifiersCardModal';
// Utils
import { useShopingCartUpdate } from '@utils';
// Types
import { TRestaurantProductModifiers } from '@type/restaurants/restaurant-products';
import { TRestaurantCard } from '@type/restaurants/restaurant-card';
import { TScoresSpendProps } from './types';
// Enhance
import { enhance } from './enhance';

const CardWrapper: FC = ({ children }) => <div className="scores-spend__list">{children}</div>;

const ScoresSpendComponent: FC<TScoresSpendProps> = ({
  rest = [],
  food = [],
  city,
  profile,
  shopingCart,
  profileDeleteFavoriteRests,
  profileSetFavoriteRests,
  authModalShow,
  shopingCartUpdate,
}) => {
  const isTwoCards = useMediaQuery({
    minWidth: 769,
  });
  const isOneCard = useMediaQuery({
    maxWidth: 768,
  });

  const { setCardHeight, getCardHeight } = useCardResize({
    cardsLength: food.length,
    isOneCard,
    isTwoCards,
  });

  const getUrl = useCallback((urlName: string) => `/${city?.urlName}/restaurant/${urlName}`, [
    city?.urlName,
  ]);

  // const history = useHistory();

  const renderScoresSpendRests = useMemo(
    () => (item: TRestaurantCard) => (
      <RestaurantCard
        onLikeClick={likeClickHandler({
          id: item.id,
          type: 'restId',
          additionalData: {
            newRestaurant: item,
          },
          likeAction: profileSetFavoriteRests,
          unLikeAction: profileDeleteFavoriteRests,
          auth: !profile.auth && authModalShow,
        })}
        key={item.id}
        item={item}
        url={getUrl(item.urlName)}
      />
    ),
    [getUrl, authModalShow, profileDeleteFavoriteRests, profileSetFavoriteRests, profile],
  );

  const [updateShopingCart] = useShopingCartUpdate({
    city,
    shopingCart,
    shopingCartUpdate,
    profile,
    configName: 'freeFood',
  });

  const renderScoresSpendFood = useMemo(
    () => (item: TRestaurantProductModifiers, index: number) =>
      food && food.length ? (
        <>
          <HitCard
            key={item.id}
            onLikeClick={() => {}}
            item={item}
            updateShopingCart={updateShopingCart(item)}
            withLike={false}
            index={index}
            setItemHeight={setCardHeight}
            bodyHeight={getCardHeight(index)}
            withRestLogo={false}
          />
        </>
      ) : null,
    [food, updateShopingCart, setCardHeight, getCardHeight],
  );

  return (
    <div className="scores-spend">
      <div className="scores-spend__section">
        <div className="scores-spend__title">Рестораны с едой за баллы</div>
        <ContentViewer
          list={rest}
          renderItem={renderScoresSpendRests}
          responsive={{
            desktop: 4,
            tablet: 4,
            mobile: 4,
          }}
          wrapper={CardWrapper}
        />
      </div>
      <div className="scores-spend__section">
        <div className="scores-spend__title">
          Лучшие блюда за баллы
          {/* <MoneyBoyIcon width={20} height={20} /> */}
        </div>
        <ContentViewer
          list={food}
          renderItem={renderScoresSpendFood}
          responsive={{
            desktop: 4,
            tablet: 4,
            mobile: 4,
          }}
          wrapper={CardWrapper}
        />
      </div>
      <ModifiersCardModal />
    </div>
  );
};

export default memo(enhance(ScoresSpendComponent));
