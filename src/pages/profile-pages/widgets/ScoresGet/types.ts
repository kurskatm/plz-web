// Main types
import { TProfileSetSocialActivity } from '@actions/profile/social-activity/action-types';

export interface TScoresGetProps {
  socials: number[];
  invitationId: string;
  email: string;
  accessToken: string;
  profileSetSocialActivity: TProfileSetSocialActivity;
}
