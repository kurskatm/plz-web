// Modules
import React, { FC, memo, useCallback } from 'react';
import { InputField, Button } from 'chibbis-ui-kit';
import { Form } from 'react-final-form';
import classnames from 'classnames';
// Utils
import { trackEvent } from '@utils/analytics';
// Enhance
import { enhance } from './enhance';
// Types
import { TScoresSocialFormProps } from './types';

const ScoresGetSocialsComponent: FC<TScoresSocialFormProps> = ({
  accessToken,
  info,
  profileInfoUpdate,
}) => {
  const onSubmit = useCallback(
    (values) => {
      const data = {
        ...values,
        sendEmailConfirmationCode: true,
      };
      profileInfoUpdate({
        access_token: accessToken,
        payload: data,
      });
      trackEvent('get-500-for-email-confirm');
    },
    [profileInfoUpdate, accessToken],
  );

  const getEmailConfirmStyles = useCallback(
    () =>
      classnames(
        {
          // @ts-ignore
          'is-mail-sent': info?.emailConfirmed,
        },
        'profile-info-form__mail-confirm',
      ),
    // @ts-ignore
    [info?.emailConfirmed],
  );

  const renderEmailHint = useCallback(
    () =>
      // @ts-ignore
      !info?.emailConfirmed ? (
        <div className={getEmailConfirmStyles()}>
          <div
            className="profile-info-form__mail-confirm-hint"
            style={{
              // @ts-ignore
              color: info?.emailConfirmed ? '#27AE60' : '#D0021B',
            }}>
            {/* @ts-ignore */}
            {!info?.emailConfirmed ? 'Почта не подтверждена' : 'Почта подтверждена'}
          </div>
        </div>
      ) : null,
    [info, getEmailConfirmStyles],
  );

  return (
    <Form
      initialValues={{
        // @ts-ignore
        email: info?.emailAddress || '',
        id: info?.id,
        fullName: info?.fullName,
        dateOfBirth: info?.dateOfBirth,
        gender: info?.gender,
        avatar: info?.avatar,
        phoneNumber: info?.phoneNumber,
      }}
      onSubmit={onSubmit}
      render={({ handleSubmit, pristine }) => (
        <>
          <form onSubmit={handleSubmit} className="scores-get-form">
            <div className="scores-get-form__field">
              <InputField
                theme="grey"
                label="Электронная почта"
                name="email"
                // @ts-ignore
                disabled={info?.emailConfirmed}
              />
            </div>
            <div className="scores-get-form__field">
              <Button
                className="scores-get-form__button"
                htmlType="submit"
                // @ts-ignore
                disabled={pristine || info?.emailConfirmed}>
                Отправить
              </Button>
            </div>
          </form>
          {renderEmailHint()}
        </>
      )}
    />
  );
};

const ScoresGetSocialsFormMemo = memo(ScoresGetSocialsComponent);
export default enhance(ScoresGetSocialsFormMemo);
