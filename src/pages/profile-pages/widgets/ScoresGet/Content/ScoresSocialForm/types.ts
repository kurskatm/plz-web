// Main types
import { TInfoDataModel } from '@models/profile/types';
import { TProfileInfoUpdate } from '@actions/profile/action-types';

export interface TScoresSocialFormProps {
  profileInfoUpdate: TProfileInfoUpdate;
  info: TInfoDataModel;
  accessToken: string;
}
