// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { profileInfoUpdate } from '@actions/profile/info';
// Selectors
import { profileEditEmailSelector as mapStateToProps } from '@selectors/structured-selectors/profile';

const mapDispatchToProps = {
  profileInfoUpdate,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
