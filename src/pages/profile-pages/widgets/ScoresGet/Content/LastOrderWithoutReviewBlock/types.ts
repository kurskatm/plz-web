// Main types
import { TUserOrder } from '@type/profile';
import { TProfileGetLastOrderWithoutReview } from '@actions/profile/action-types';

export interface TLastOrderWithoutReviewProps {
  profileGetLastOrderWithoutReview: TProfileGetLastOrderWithoutReview;
  lastOrder: TUserOrder;
  addReviewList: string[];
  lastOrderIsPending: boolean;
  lastOrderIsSuccess: boolean;
  lastOrderIsError: boolean;
}
