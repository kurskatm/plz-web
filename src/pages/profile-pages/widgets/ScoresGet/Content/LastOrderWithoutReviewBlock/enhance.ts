// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { profileGetLastOrderWithoutReview } from '@actions/profile';
// Selectors
import { profileLastOrderWithoutReview as mapStateToProps } from '@selectors/structured-selectors/profile';

const mapDispatchToProps = {
  profileGetLastOrderWithoutReview,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
