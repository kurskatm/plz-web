// Modules
import React, { FC, memo, useEffect, useMemo, useState } from 'react';
import isNil from 'lodash/isNil';
import { ScoresIcon, PlateIcon, LoadingSpinner } from 'chibbis-ui-kit';
// Components
import OrderComponent from '@components/Order/Content/OrderComponent';
// Enhance
import { enhance } from './enhance';
// Types
import { TLastOrderWithoutReviewProps } from './types';

const LastOrderWithoutReviewComponent: FC<TLastOrderWithoutReviewProps> = ({
  lastOrder,
  addReviewList,
  lastOrderIsPending,
  lastOrderIsSuccess,
  lastOrderIsError,
  profileGetLastOrderWithoutReview,
}) => {
  const [reviewAddListLength, setReviewListLength] = useState<number>(addReviewList.length);

  useEffect(() => {
    if (!lastOrderIsPending && !lastOrderIsSuccess) {
      profileGetLastOrderWithoutReview();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (addReviewList.length && addReviewList.length !== reviewAddListLength) {
      // eslint-disable-next-line no-new
      new Promise<void>((resolve) => {
        setTimeout(() => {
          profileGetLastOrderWithoutReview();
          setReviewListLength(addReviewList.length);
          resolve();
        }, 10000);
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [addReviewList.length]);

  const renderContent = useMemo(() => {
    if (lastOrderIsPending) {
      return (
        <LoadingSpinner className="restaurant-reviews-list__spinner" size="big" color="orange" />
      );
    }

    if (lastOrderIsSuccess && !isNil(lastOrder)) {
      return <OrderComponent order={lastOrder} />;
    }

    if (lastOrderIsError && addReviewList.length) {
      return (
        <div className="scores-get__all-orders-has-review" style={{ alignItems: 'center' }}>
          <div className="scores-get__all-orders-has-review-img">
            <PlateIcon />
          </div>
          <div className="scores-get__all-orders-has-review-text">
            Вы оценили все заказы
            <br />
            и получили + 300
            <ScoresIcon />
          </div>
        </div>
      );
    }

    return (
      <div className="scores-get__empty-order" style={{ alignItems: 'center' }}>
        <div className="scores-get__empty-order-img">
          <PlateIcon />
        </div>
        <div className="scores-get__empty-order-text">У вас нет заказов без отзыва</div>
      </div>
    );
  }, [addReviewList.length, lastOrder, lastOrderIsPending, lastOrderIsSuccess, lastOrderIsError]);

  return <div className="scores-get__last-order-review">{renderContent}</div>;
};

const LastOrderWithoutReviewMemo = memo(LastOrderWithoutReviewComponent);
export default enhance(LastOrderWithoutReviewMemo);
