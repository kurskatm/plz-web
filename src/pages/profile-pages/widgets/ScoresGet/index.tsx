import React, { FC, memo, useCallback } from 'react';
// Components
import ScoresSocials from '@components/ScoresSocials';
import ProfileFriendsFooter from '@pages/profile-pages/widgets/ProfileFriends/Content/ProfileFriendsFooter';
import { ScoresIcon } from 'chibbis-ui-kit';
import ScoresSocialForm from './Content/ScoresSocialForm';
import LastOrderReview from './Content/LastOrderWithoutReviewBlock';
// Types
import { TScoresGetProps } from './types';

const SOCIAL_LIST_SUBS = [
  {
    image: 'vk',
    activated: false,
    link: 'https://vk.com/chibbis_rus',
    points: 0,
  },
  // {
  //   image: 'inst',
  //   activated: false,
  //   link: 'https://www.instagram.com/chibbis/?hl=ru',
  //   points: 1,
  // },
  // {
  //   image: 'tiktok',
  //   activated: false,
  //   link: 'https://www.tiktok.com/@chibbis.ru',
  //   points: 2,
  // },
];

const SOCIAL_LIST_SUBS2 = [
  {
    image: 'vk',
    activated: false,
    link: 'https://vk.com/share.php?url=https://msk.chibbis.ru?utm_source=share-vk',
    points: 3,
  },
  // {
  //   image: 'fb',
  //   activated: false,
  //   link:
  //     'https://www.facebook.com/sharer/sharer.php?u=https://kursk.chibbis.ru?utm_source=share-fb',
  //   points: 4,
  // },
  {
    image: 'ok',
    activated: false,
    link: 'https://connect.ok.ru/offer?url=https://kursk.chibbis.ru?utm_source=share-ok',
    points: 5,
  },
];

// list = SOCIAL_LIST_SUBS, followList: number[] = [], offset = 0,
const makeFollowList = (list = SOCIAL_LIST_SUBS, followList: number[] = []) =>
  list.map((item) => ({
    ...item,
    // activated: followList.indexOf(index + offset) !== -1,
    activated: followList.includes(item.points),
  }));

const ScoresGetComponent: FC<TScoresGetProps> = ({
  socials = [],
  invitationId,
  accessToken,
  profileSetSocialActivity,
}) => {
  const onCardClick = useCallback(
    (type: number) => {
      if (accessToken && accessToken.length) {
        profileSetSocialActivity({ type, access_token: accessToken });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [accessToken],
  );
  return (
    <div className="scores-get">
      <div className="scores-get__section">
        <div className="scores-get__title">
          150
          <ScoresIcon /> за подписку на нас в соцсетях
        </div>
        <div className="scores-get__content">
          <ScoresSocials
            socialsList={makeFollowList(SOCIAL_LIST_SUBS, socials)}
            onClick={onCardClick}
          />
        </div>
      </div>
      <div className="scores-get__section">
        <div className="scores-get__title">
          300
          <ScoresIcon /> за репост ссылки в соцсетях
        </div>
        <div className="scores-get__content">
          <ScoresSocials
            socialsList={makeFollowList(SOCIAL_LIST_SUBS2, socials)}
            onClick={onCardClick}
          />
        </div>
      </div>
      <div className="scores-get__section">
        <div className="scores-get__title">
          Промокод на 500
          <ScoresIcon /> в подтверждение электронной почты
        </div>
        <div className="scores-get__content">
          <ScoresSocialForm />
        </div>
      </div>
      <div className="scores-get__section">
        <div className="scores-get__content">
          <ProfileFriendsFooter invitationId={invitationId} />
        </div>
      </div>
      <div className="scores-get__section">
        <div className="scores-get__title">
          100
          <ScoresIcon /> за каждый отзыв
        </div>
        <div className="scores-get__content">
          <LastOrderReview />
        </div>
      </div>
      <div className="scores-get__section">
        <div className="scores-get__title">10% от суммы заказа всегда возвращаются в баллах</div>
        <div className="scores-get__example">
          <div className="scores-get__example-inner">
            <div className="scores-get__example-title">Например:</div>
            <div className="scores-get__example-row">
              <div className="scores-get__example-col">
                <div className="scores-get__example-subtitle">Сумма заказа</div>
                <div className="scores-get__example-sum">700 ₽</div>
              </div>
              <div className="scores-get__example-col">
                <div className="scores-get__example-subtitle">Вернётся баллов</div>
                <div className="scores-get__example-scores">
                  70 <ScoresIcon width={32} height={32} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default memo(ScoresGetComponent);
