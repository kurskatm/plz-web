import React, { FC, memo, useCallback } from 'react';
// Icons
import { ScoresIcon } from 'chibbis-ui-kit';
// Lib
import { getDate } from '@lib/date/get-date';
// Types
import { TProfileScoresListItem } from '../types';

const ProfileScoresListItemComponent: FC<TProfileScoresListItem> = ({
  loggedOn,
  balanceDifference,
  description,
}) => {
  const getDateCallback = useCallback((date: string) => getDate(date), []);

  const getBalance = useCallback((balance: string) => {
    if (String(balance)[0] === '-') {
      return `${String(balance)[0]} ${String(balance).slice(1)}`;
    }

    return `+ ${balance}`;
  }, []);

  return (
    <div className="profile-scores-list__item">
      <div className="profile-scores-list__info">
        <div className="profile-scores-list__info-text">{description}</div>
        <div className="profile-scores-list__info-date">{getDateCallback(loggedOn)}</div>
      </div>
      <div className="profile-scores-list__score">
        {getBalance(balanceDifference)}
        <ScoresIcon className="profile-scores-list__score-icon" />
      </div>
    </div>
  );
};

export default memo(ProfileScoresListItemComponent);
