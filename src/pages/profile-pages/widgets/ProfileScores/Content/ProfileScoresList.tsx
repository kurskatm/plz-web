import React, { FC, memo, useMemo } from 'react';
import ProfileScoresListItem from './ProfileScoresListItem';
// Types
import { TProfileScoresList } from '../types';

const ProfileScoresListComponent: FC<TProfileScoresList> = ({ list = [] }) => {
  const renderList = useMemo(
    () =>
      list?.map(({ description, balanceDifference, loggedOn }) => (
        <ProfileScoresListItem
          key={`${loggedOn} - ${balanceDifference}`}
          description={description}
          balanceDifference={balanceDifference}
          loggedOn={loggedOn}
        />
      )),
    [list],
  );
  return <div className="profile-scores-list">{renderList}</div>;
};

export default memo(ProfileScoresListComponent);
