import React, { FC, useMemo, memo, useCallback } from 'react';
import { useParams } from 'react-router-dom';
import urlJoin from 'url-join';
// Main Types
import { TCityRouterParams } from '@type/routes';
// Utils
import { Consts } from '@utils';
// Components
import { Link, ScoresIcon } from 'chibbis-ui-kit';
import ProfileScoresList from './Content/ProfileScoresList';
// Types
import { TProfileScoresProps } from './types';

const { PATH } = Consts.ROUTES.PROFILE_PAGES.SCORES;

const ProfileScoresComponent: FC<TProfileScoresProps> = ({ list = [], score = '0' }) => {
  const { cityUrl } = useParams<TCityRouterParams>();

  const renderButton = useMemo(
    () => (
      <Link
        color="blue"
        className="profile-info__button-link profile-widget__btn"
        to={urlJoin(`/${cityUrl}`, PATH)}>
        Посмотреть всю историю
      </Link>
    ),
    [cityUrl],
  );

  const getCost = useCallback((costNum: number) => {
    const costStr = String(costNum);
    if (costStr.length > 3) {
      return `${costStr.slice(0, costStr.length - 3)} ${costStr.slice(costStr.length - 3)}`;
    }

    return costStr;
  }, []);

  return (
    <div className="profile-scores profile-widget">
      <div className="profile-scores__header">
        <div className="profile-scores__header-info">
          <div className="profile-scores__header-text">Баллов на счету</div>
          <Link
            color="blue"
            to={urlJoin(`/${cityUrl}`, `${PATH}#howtoget`)}
            className="profile-scores__header-link">
            Как получить?
          </Link>
        </div>
        <div className="profile-scores__header-score">
          <ScoresIcon width={40} height={40} />
          {getCost(Number(score))}
        </div>
      </div>
      <div className="profile-scores__list">
        <ProfileScoresList list={list} />
      </div>
      <div className="profile-info__button">{renderButton}</div>
    </div>
  );
};

export default memo(ProfileScoresComponent);
