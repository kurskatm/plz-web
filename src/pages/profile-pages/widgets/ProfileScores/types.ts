export interface TProfileScoresProps {
  list?: TProfileScoresListItem[];
  score: string;
}

export type TProfileScoresListItem = {
  description: string;
  loggedOn: string;
  balanceDifference: string;
};

export interface TProfileScoresList {
  list?: TProfileScoresListItem[];
}
