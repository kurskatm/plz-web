import React, { FC, memo } from 'react';
// Components
import ProfileFriendsFooter from './Content/ProfileFriendsFooter';
import ProfileFriendsList from './Content/ProfileFriendsList';

// Types
import { TProfileFriends } from '../../info/types';

const ProfileFriendsComponent: FC<TProfileFriends> = ({
  invitees = [],
  invitationId = '',
  cityUrl = '',
}) => (
  <div className="profile-friends profile-widget">
    <div className="profile-friends__header">Друзья</div>
    <div className="profile-friends__list-wrapper">
      <ProfileFriendsList friends={invitees.filter(({ type }) => type === 0)} />
      {invitees.filter(({ type }) => type === 1).length > 0 && (
        <>
          <div className="profile-friends__header__sub">Друзья друзей</div>
          <ProfileFriendsList friends={invitees.filter(({ type }) => type === 1)} />
        </>
      )}
    </div>
    <ProfileFriendsFooter cityUrl={cityUrl} invitationId={invitationId} />
  </div>
);

export default memo(ProfileFriendsComponent);
