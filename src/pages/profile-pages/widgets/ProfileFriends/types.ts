export type TProfileFriendsProps = {
  props?: boolean;
};

export type TProfileFriendsListItem = {
  earnedBonusPoints: number;
  name: string;
  avatar: string;
  type: number;
};

export interface TProfileFriendsListProps {
  friends: TProfileFriendsListItem[];
  invitationId?: string;
}
