import React, { FC, useMemo, memo, useCallback } from 'react';
// Components
import ProfileFriendListItem from './ProfileFriendsListItem';
// Types
import { TProfileFriendsListProps } from '../types';

const ProfileFriendsListComponent: FC<TProfileFriendsListProps> = ({ friends = [] }) => {
  const renderList = useMemo(
    () =>
      friends.map(({ earnedBonusPoints, name, avatar, type }) => (
        <div className="profile-friends__item" key={`${name}-${earnedBonusPoints}`}>
          <ProfileFriendListItem
            earnedBonusPoints={earnedBonusPoints}
            name={name}
            avatar={avatar}
            type={type}
          />
        </div>
      )),
    [friends],
  );

  const render = useCallback(() => {
    if (friends.length) {
      return <div className="profile-friends__list">{renderList}</div>;
    }

    return (
      <div className="profile-friends__list-empty-wrap">
        <div className="profile-friends__list-empty-img" />
      </div>
    );
  }, [friends.length, renderList]);

  return render();
};

export default memo(ProfileFriendsListComponent);
