import React, { FC, useState, useMemo, useCallback, useRef, memo } from 'react';
import { Button, SocialButtons } from 'chibbis-ui-kit';
// libs
import { socialNetworkFullList } from '@lib/social-networks';
// Utils
import { trackEvent } from '@utils/analytics';

const ProfileFriendsFooterComponent: FC<Record<string, string>> = ({
  cityUrl = '',
  invitationId = '',
}) => {
  const [isCopyed, setCopy] = useState(false);
  const shareLink = useMemo(
    () => `https://${cityUrl}.chibbis.ru/home/register?invitationId=${invitationId}`,
    [cityUrl, invitationId],
  );
  const socialList = useMemo(
    () =>
      socialNetworkFullList.map((item) => ({
        ...item,
        link: item.link + shareLink,
      })),
    [shareLink],
  );
  const fakeElem = useRef<HTMLTextAreaElement>(null);
  const copyText = useCallback(() => {
    fakeElem?.current.select();
    document.execCommand('copy');
    setCopy(true);
    trackEvent('приглашение');
    setTimeout(() => setCopy(false), 5000);
  }, []);

  return (
    <div className="profile-friends__footer">
      <div className="profile-friends__footer-text">
        Приглашайте друзей и получайте 10% от их заказов. И 5% от друзей ваших друзей
      </div>
      <div className="profile-friends__footer-btn">
        <Button
          htmlType="button"
          onClick={copyText}
          size="small"
          type="secondary"
          disabled={isCopyed}
          className="profile-widget__btn">
          {isCopyed ? 'Ссылка скопирована' : 'Скопировать ссылку'}
        </Button>
      </div>

      <textarea
        id="fakeTextarea"
        className="profile-friends__fake-elem"
        value={shareLink}
        ref={fakeElem}
        readOnly
      />

      <div className="profile-friends__footer-links">
        <SocialButtons items={socialList} />
      </div>
    </div>
  );
};

export default memo(ProfileFriendsFooterComponent);
