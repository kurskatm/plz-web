import React, { FC, useMemo, memo } from 'react';
import { ScoresIcon } from 'chibbis-ui-kit';
// @libs
import { getAvatarIcon } from '@/lib/get-avatar-icon';
// Types
import { TProfileFriendsListItem } from '../types';

const ProfileFriendsListItemComponent: FC<TProfileFriendsListItem> = ({
  earnedBonusPoints,
  name,
  avatar,
}) => {
  const renderIcon = useMemo(() => {
    const Icon = getAvatarIcon(avatar);
    return <Icon width={30} height={28} />;
  }, [avatar]);

  return (
    <>
      <div className="profile-friends__wrap">
        <div className="profile-friends__avatar">{renderIcon}</div>
        <div className="profile-friends__name">{name}</div>
      </div>
      <div className="profile-friends__score">
        {earnedBonusPoints}
        <ScoresIcon />
      </div>
    </>
  );
};

export default memo(ProfileFriendsListItemComponent);
