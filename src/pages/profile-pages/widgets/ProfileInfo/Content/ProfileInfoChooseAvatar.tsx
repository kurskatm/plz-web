import React, { FC, memo, useMemo } from 'react';
import { RadioField } from 'chibbis-ui-kit';
import classNames from 'classnames';
// Lib
import { getAvatarIcon, AVATARS } from '@lib/get-avatar-icon';
// Types
import { TProfileInfoAvatar } from '../types';

const ProfileInfoChooseAvatarComponent: FC<TProfileInfoAvatar> = ({ currentAvatar = '🐔' }) => {
  const renderFieldsList = useMemo(
    () =>
      AVATARS.map((avatar: string) => {
        const Icon = getAvatarIcon(avatar);
        return (
          <div
            key={`${avatar}_key`}
            className={classNames('profile-info-choose-avatar__item', {
              'is-active': currentAvatar === avatar,
            })}>
            <RadioField name="avatar" value={avatar} view={() => <Icon width={32} height={32} />} />
          </div>
        );
      }),
    [currentAvatar],
  );

  return (
    <div className="profile-info-choose-avatar">
      <div className="profile-info-choose-avatar__title">Аватарка</div>
      <div className="profile-info-choose-avatar__list">{renderFieldsList}</div>
    </div>
  );
};

export default memo(ProfileInfoChooseAvatarComponent);
