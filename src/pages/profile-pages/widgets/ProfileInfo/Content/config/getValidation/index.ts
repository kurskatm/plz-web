// Types
import { TGetValidation, TErrors } from './types';

export const getValidation: TGetValidation = (values) => {
  const errors: TErrors = {};

  const dateMask = new RegExp(/^\d{1,2} [А-Яа-я]* \d{4}$/);
  const emailMask = new RegExp(/^([A-Za-z0-9_\-.])+@([A-Za-z0-9_\-.])+\.([A-Za-z]{2,4})$/);

  if (!values.fullName) {
    errors.fullName = 'Необходимо указать';
  }

  if (!values.email) {
    errors.email = 'Необходимо указать';
  }

  if (values.email && !values.email.match(emailMask)) {
    errors.email = 'Неверный формат почты';
  }

  if (!values.phoneNumber) {
    errors.phoneNumber = 'Необходимо указать';
  }

  if (!values.dateOfBirth || values.dateOfBirth === '') {
    errors.dateOfBirth = 'Необходимо указать';
  }

  if (values.dateOfBirth.length && !values.dateOfBirth.match(dateMask)) {
    errors.dateOfBirth = 'Неверный формат даты';
  }

  if (!values.avatar) {
    errors.avatar = 'Необходимо указать';
  }

  return errors;
};
