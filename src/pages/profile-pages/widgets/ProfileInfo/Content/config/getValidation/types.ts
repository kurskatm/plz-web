import dayjs from 'dayjs';

export interface TValues {
  fullName: string;
  phoneNumber: string;
  email: string;
  dateOfBirth: string;
  dateOfBirthValid: dayjs.Dayjs;
  avatar: string;
  gender: string | null;
}

export interface TErrors {
  fullName?: string;
  phoneNumber?: string;
  email?: string;
  dateOfBirth?: string;
  avatar?: string;
  gender?: string;
}

export type TGetValidation = (data: TValues) => TErrors;
