import dayjs from 'dayjs';

export interface TValues {
  fullName: string;
  phoneNumber: string;
  email: string;
  dateOfBirth: string;
  dateOfBirthValid: dayjs.Dayjs;
  avatar: string;
  gender: string | null;
}

export interface TValidValues {
  fullName: string;
  phoneNumber: number;
  email: string;
  dateOfBirth: number | null;
  avatar: string;
  gender: number | null;
  sendEmailConfirmationCode: boolean;
}

export type TGetSubmitValues = (data: TValues, emailAddress: string) => TValidValues;
