// Modules
import isNil from 'lodash/isNil';
import dayjs from 'dayjs';
// Types
import { TGetSubmitValues, TValidValues } from './types';

const getGender = (data: string | null) => {
  if (isNil(data) || data === 'none') {
    return 0;
  }

  if (data === 'man') {
    return 1;
  }

  return 2;
};

const getDateOfBirth = (date: dayjs.Dayjs, dateStr: string) => {
  if (dateStr === 'Не указана' || dateStr === '') {
    return null;
  }

  return Number(dayjs(date).format('YYYYMMDD'));
};

export const getSubmitValues: TGetSubmitValues = (values, emailAddress) => {
  const validData: TValidValues = {
    avatar: values.avatar,
    fullName: values.fullName,
    phoneNumber: Number(values.phoneNumber),
    email: values.email,
    sendEmailConfirmationCode: emailAddress !== values.email,
    gender: getGender(values.gender),
    dateOfBirth: getDateOfBirth(values.dateOfBirthValid, values.dateOfBirth),
  };

  return validData;
};
