// Modules
import isNil from 'lodash/isNil';
import dayjs from 'dayjs';
// Types
import { TGetInitialValues, TValues } from './types';

const customParseFormat = require('dayjs/plugin/customParseFormat');

dayjs.extend(customParseFormat);

const getNameAndEmailData = (data: string, status: string) => {
  if (status === 'read') {
    if (!isNil(data) && data.length) {
      return data;
    }

    return 'Не указано';
  }

  return data;
};

const getGender = (data: number) => {
  if (data === 0) {
    return null;
  }

  if (data === 1) {
    return 'man';
  }

  return 'woman';
};

export const getDateOfBirth = (data: number | null, status: string): string => {
  if (status === 'read') {
    if (!isNil(data) && data) {
      const dateStr = data.toString();
      const validDate = `${dateStr.slice(6)}.${dateStr.slice(4, 6)}.${dateStr.slice(0, 4)}`;
      return dayjs(validDate, 'DD.MM.YYYY', true).format('DD MMMM YYYY');
    }

    return 'Не указано';
  }

  if (!isNil(data) && data) {
    const dateStr = data.toString();
    const validDate = `${dateStr.slice(6)}.${dateStr.slice(4, 6)}.${dateStr.slice(0, 4)}`;
    return dayjs(validDate, 'DD.MM.YYYY', true).format('DD MMMM YYYY');
  }

  return '';
};

const getDateOfBirthValid = (data: number): dayjs.Dayjs => {
  if (!isNil(data) && data) {
    const dateStr = data.toString();
    const validDate = `${dateStr.slice(6)}.${dateStr.slice(4, 6)}.${dateStr.slice(0, 4)}`;
    return dayjs(validDate, 'DD.MM.YYYY', true);
  }

  return null;
};

export const getInitialValues: TGetInitialValues = (data) => {
  const values: TValues = {
    avatar: data.avatar,
    dateOfBirth: getDateOfBirth(data.dateOfBirth, data.status),
    email: getNameAndEmailData(data.email, data.status),
    fullName: getNameAndEmailData(data.fullName, data.status),
    gender: getGender(data.gender),
    phoneNumber: data.phoneNumber.toString(),
  };

  if (data.dateOfBirth) {
    values.dateOfBirthValid = getDateOfBirthValid(data.dateOfBirth);
  }

  return values;
};

export const getDateOfBirthForPicker = (data: number | null): Date => {
  if (!isNil(data) && data) {
    const dateStr = data.toString();
    const validDate = `${dateStr.slice(6)}.${dateStr.slice(4, 6)}.${dateStr.slice(0, 4)}`;
    return new Date(dayjs(validDate, 'DD.MM.YYYY', true).toISOString());
  }

  return new Date();
};
