// Modules
import dayjs from 'dayjs';

export interface TGetInitialValuesArgs {
  fullName: string;
  phoneNumber: number;
  email: string;
  dateOfBirth: number | null;
  avatar: string;
  gender: number;
  status: string;
}

export interface TValues {
  fullName: string;
  phoneNumber: string;
  email: string;
  dateOfBirth: string;
  avatar: string;
  gender: string | null;
  dateOfBirthValid?: dayjs.Dayjs;
}

export type TGetInitialValues = (data: TGetInitialValuesArgs) => TValues;
