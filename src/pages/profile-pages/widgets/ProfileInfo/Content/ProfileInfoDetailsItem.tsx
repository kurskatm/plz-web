import React, { FC, useMemo, useCallback, memo } from 'react';
import classnames from 'classnames';
// Types
import { TProfileDetailsItemProps } from '../types';

const ProfileInfoDetailsItemComponent: FC<TProfileDetailsItemProps> = ({
  title,
  value,
  hint,
  error,
  sendMail,
  isGender,
}) => {
  const renderHint = useMemo(
    () => (
      <>
        {hint ? (
          <div
            className={classnames('profile-info-details-item__hint', {
              'is-error': error,
            })}>
            {hint}
          </div>
        ) : null}
        {sendMail ? <div className="profile-info-details-item__send">{sendMail}</div> : null}
      </>
    ),
    [hint, error, sendMail],
  );
  const getStyles = useCallback(
    () =>
      classnames('profile-info-details-item__value', {
        'profile-info-details-item__value-gender': isGender,
      }),
    [isGender],
  );

  return (
    <div className="profile-info-details-item">
      <div className="profile-info-details-item__title">{title}</div>
      <div className={getStyles()}>{value}</div>
      {renderHint}
    </div>
  );
};

export default memo(ProfileInfoDetailsItemComponent);
