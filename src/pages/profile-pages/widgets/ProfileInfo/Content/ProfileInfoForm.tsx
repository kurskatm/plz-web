/* eslint-disable @typescript-eslint/indent */
// Modules
import React, { FC, useCallback, useState, memo, useEffect, useMemo } from 'react';
import classnames from 'classnames';
import dayjs from 'dayjs';
import isNil from 'lodash/isNil';
// Icons
import ManIcon from '@/../assets/img/profile-pages/man.png';
import WomanIcon from '@/../assets/img/profile-pages/woman.png';
// Components
import { InputField, InputPhone, RadioField, Button } from 'chibbis-ui-kit';
import { Form } from 'react-final-form';
import { FormApi } from 'final-form';
import DatePicker from 'react-datepicker';
// Lib
import { getProfileData } from '@lib/cookie';
import { profileAuthorized } from '@actions/profile';
import { trackEvent } from '@utils/analytics';
// Component Modules
import ProfileInfoChooseAvatar from './ProfileInfoChooseAvatar';
import ProfileInfoAvatar from './ProfileInfoAvatar';
// Config
import { getInitialValues, getDateOfBirthForPicker } from './config/getInitialValues';
import { getValidation } from './config/getValidation';
import { getSubmitValues } from './config/getSubmitValues';
// Types
import { TProfileFormProps } from '../types';
import { TValues } from './config/getValidation/types';
// Utils
import { months, years } from './utils';

const ProfileInfoFormComponent: FC<TProfileFormProps> = ({
  fullName = 'Не указаны',
  phoneNumber = '+7 987 654-32-10',
  emailAddress,
  dateOfBirth = '',
  gender = null,
  avatar = '🐼',
  emailConfirmed,
  // status = 'read',
  logout,
  update,
  token,
  id,
  updateIsSuccess,
  updateIsError,
  updateIsPending,
  updateInfoDetails,
}) => {
  const [status, setStatus] = useState('read');
  const [selectedDate, setSelectedDate] = useState<Date>();
  const [sentEmailConfirm, setSentEmailConfirm] = useState(false);

  useEffect(() => {
    if (!isNil(dateOfBirth) && dateOfBirth) {
      const validDate = getDateOfBirthForPicker(Number(dateOfBirth));
      if (new Date(validDate)) {
        setSelectedDate(new Date(validDate));
      }
    }
  }, [dateOfBirth]);

  const getGenderStyles = useCallback(
    (gndr: string) =>
      classnames(
        {
          'is-man-checked': gndr === 'man',
          'is-woman-checked': gndr === 'woman',
        },
        {
          'is-read-only': status === 'read',
        },
        'profile-info-form__gender-wrap',
      ),
    [status],
  );
  const getEmailConfirmStyles = useCallback(
    () =>
      classnames(
        {
          'is-mail-sent': emailConfirmed,
        },
        'profile-info-form__mail-confirm',
      ),
    [emailConfirmed],
  );

  const [emailConfirmStyles, setEmailConfirmStyles] = useState(
    'profile-info-form__mail-confirm-hint',
  );
  useEffect(
    () => {
      if (updateIsSuccess && sentEmailConfirm) {
        setEmailConfirmStyles(classnames('is-mail-sent', 'profile-info-form__mail-confirm-hint'));
      } else if (updateIsError && sentEmailConfirm) {
        setEmailConfirmStyles('profile-info-form__mail-confirm-hint');
      } else if (!updateIsPending) {
        setEmailConfirmStyles('profile-info-form__mail-confirm-hint');
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [updateIsSuccess, updateIsError, updateIsPending],
  );
  const renderGender = useCallback(
    (currentGender: string) =>
      status !== 'read' ? (
        <>
          <RadioField
            className="radio-field-man"
            name="gender"
            value="man"
            view={() => (
              <>
                <img src={ManIcon} alt="man" className="radio-field-label__icon" />
                <span className="radio-field-label__text">Мужской</span>
              </>
            )}
          />
          <RadioField
            className="radio-field-woman"
            name="gender"
            value="woman"
            view={() => (
              <>
                <img src={WomanIcon} alt="woman" className="radio-field-label__icon" />
                <span className="radio-field-label__text">Женский</span>
              </>
            )}
          />
          {/* Temporare hack for unselected gender */}
          <RadioField className="radio-field-none" name="gender" value="none" view={null} />
        </>
      ) : (
        <>
          {currentGender === 'man' && (
            <RadioField
              name="gender"
              value="man"
              view={() => (
                <>
                  <img src={ManIcon} alt="man" className="radio-field-label__icon" />
                  <span className="radio-field-label__text">Мужской</span>
                </>
              )}
            />
          )}
          {currentGender === 'woman' && (
            <RadioField
              name="gender"
              value="woman"
              view={() => (
                <>
                  <img src={WomanIcon} alt="woman" className="radio-field-label__icon" />
                  <span>Женский</span>
                </>
              )}
            />
          )}
          {(currentGender === null || currentGender === 'none') && (
            <RadioField name="gender" value="none" view="Не указан" />
          )}
        </>
      ),
    [status],
  );
  const handleClick = useCallback(
    () => () => {
      setStatus((prevStatus) => (prevStatus === 'read' ? 'edit' : 'read'));
    },
    [],
  );

  const onChangeDate = useCallback((date: Date, form: FormApi) => {
    setSelectedDate(date);
    form.change('dateOfBirth', dayjs(date).format('D MMMM YYYY'));
    form.change('dateOfBirthValid', dayjs(date));
  }, []);

  const initValues = useMemo(
    // eslint-disable-next-line arrow-body-style
    () => {
      return getInitialValues({
        avatar,
        dateOfBirth: Number(dateOfBirth),
        email: emailAddress,
        fullName,
        gender,
        phoneNumber: Number(phoneNumber),
        status,
      });
    },
    [avatar, dateOfBirth, emailAddress, fullName, gender, phoneNumber, status],
  );

  const getValidate = useCallback((values) => getValidation(values), []);

  const editProfile = useCallback(
    (values: TValues, resendEmailConfirm?: boolean) => {
      const submitDate = getSubmitValues(values, emailAddress);
      update({
        access_token: token,
        payload: {
          ...submitDate,
          id,
          sendEmailConfirmationCode: !!resendEmailConfirm,
        },
        emailChanged: values.email !== emailAddress,
        onSuccess: () => {
          setStatus('read');
        },
      });
      profileAuthorized(getProfileData());
      setSentEmailConfirm(!!resendEmailConfirm);
    },
    [emailAddress, id, token, update],
  );
  const onSubmit = useCallback(
    (values: TValues) => {
      if (status === 'edit' && values) {
        editProfile(values);
      }
    },
    [status, editProfile],
  );
  const sendConfirmationEmail = useCallback(
    (values: TValues) => {
      if (status === 'read' && values) {
        editProfile(values, true);
        trackEvent('get-500-for-email-confirm');
      }
    },
    [status, editProfile],
  );

  const [emailHint, setEmailHint] = useState('Не подтверждена');
  useEffect(
    () => {
      if (updateIsSuccess && sentEmailConfirm) {
        setEmailHint('Письмо отправлено');
      } else if (updateIsError && sentEmailConfirm) {
        setEmailHint(updateInfoDetails);
      } else if (!updateIsPending && emailAddress) {
        setEmailHint('Не подтверждена');
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [updateIsSuccess, updateIsError, updateIsPending],
  );
  const renderEmailHint = useCallback(
    (values: TValues) =>
      status === 'read' ? (
        <div className={getEmailConfirmStyles()}>
          <div className={emailConfirmStyles}>{!!emailAddress && !emailConfirmed && emailHint}</div>
          {!!emailAddress && !emailConfirmed && (
            // eslint-disable-next-line max-len
            // eslint-disable-next-line jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events
            <div
              className="profile-info-form__mail-confirm-action"
              onClick={() => {
                sendConfirmationEmail(values);
              }}>
              <span>Отправить ещё раз</span>
            </div>
          )}
        </div>
      ) : null,
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [status, emailConfirmed, getEmailConfirmStyles, emailConfirmStyles, emailHint, emailAddress],
  );
  const getFormStyles = useCallback(
    () => classnames('profile-info-form', `profile-info-form_status_${status}`),
    [status],
  );

  const renderDatePickerHeader = useCallback(
    ({
      date,
      changeYear,
      changeMonth,
      decreaseMonth,
      increaseMonth,
      prevMonthButtonDisabled,
      nextMonthButtonDisabled,
    }) => (
      <div className="date-picker-header">
        <div>
          <select
            value={date.getFullYear()}
            onChange={({ target: { value } }) => changeYear(Number(value))}>
            {years.map((option) => (
              <option key={option} value={option}>
                {option}
              </option>
            ))}
          </select>
        </div>
        <div>
          <button type="button" onClick={decreaseMonth} disabled={prevMonthButtonDisabled}>
            {'<'}
          </button>
          <select
            value={months[date.getMonth()]}
            onChange={({ target: { value } }) => changeMonth(months.indexOf(value))}>
            {months.map((option) => (
              <option key={option} value={option}>
                {option}
              </option>
            ))}
          </select>

          <button type="button" onClick={increaseMonth} disabled={nextMonthButtonDisabled}>
            {'>'}
          </button>
        </div>
      </div>
    ),
    [],
  );

  const excludedDateOptions = useMemo(
    () => [
      {
        start: dayjs().startOf('day').add(1, 'day').toDate(),
        end: dayjs('9999-12-31T00:00:00Z').toDate(),
      },
    ],
    [],
  );

  const renderForm = useCallback(
    ({ handleSubmit, values, form, pristine, valid }) => (
      <form id="profileInfoForm" onSubmit={handleSubmit} className={getFormStyles()}>
        <div className="profile-info__avatar">
          {/* {status !== 'read' && <ProfileInfoChooseAvatar currentAvatar={values.avatar} />} */}
          <ProfileInfoAvatar logout={logout} avatar={values.avatar} />
        </div>
        <div className="profile-info-form__fields">
          <div className="profile-info-form__field">
            <InputField
              theme="grey"
              label="Имя или псевдоним"
              name="fullName"
              disabled={status === 'read'}
            />
          </div>
          <div className="profile-info-form__field">
            <InputPhone theme="grey" label="Телефон" name="phoneNumber" disabled />
          </div>
          <div className="profile-info-form__field">
            <DatePicker
              name="dateOfBirth"
              selected={selectedDate}
              onChange={(date) => onChangeDate(date, form)}
              dateFormat="d MMMM yyyy"
              disabled={!!dateOfBirth || status === 'read'}
              locale="ru"
              customInput={
                <InputField
                  theme="grey"
                  label="Дата рождения"
                  name="dateOfBirth"
                  disabled={!!dateOfBirth}
                />
              }
              renderCustomHeader={renderDatePickerHeader}
              excludeDateIntervals={excludedDateOptions}
            />
          </div>
          <div className="profile-info-form__field">
            <InputField
              theme="grey"
              label="Электронная почта"
              name="email"
              disabled={status === 'read'}
            />
            {/* {renderEmailHint(values)} */}
          </div>
        </div>
        {/* <div className="profile-info-form__gender">
      <div className="profile-info-form__title">Пол</div>
      <div className={getGenderStyles(values.gender)}>{renderGender(values.gender)}</div>
    </div> */}
        <div className="profile-info-form__button">
          {status === 'read' ? (
            <Button
              className="profile-widget__btn"
              htmlType="button"
              onClick={handleClick()}
              size="small"
              type="primary">
              Изменить профиль
            </Button>
          ) : (
            <div className="profile-info-form__buttons-wrp">
              <Button
                className="profile-widget__btn"
                htmlType="button"
                onClick={handleClick()}
                size="small">
                Отменить
              </Button>
              <Button
                className="profile-widget__btn"
                size="small"
                type="primary"
                disabled={pristine || !valid}>
                Сохранить
              </Button>
            </div>
          )}
        </div>
      </form>
    ),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [status, logout, renderEmailHint, selectedDate, renderGender],
  );

  return (
    <Form
      onSubmit={onSubmit}
      initialValues={initValues}
      validate={getValidate}
      render={renderForm}
    />
  );
};

export default memo(ProfileInfoFormComponent);
