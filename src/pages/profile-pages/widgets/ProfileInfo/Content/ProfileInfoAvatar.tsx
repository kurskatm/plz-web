import React, { FC, memo, useMemo } from 'react';
// Lib
import { getAvatarIcon } from '@lib/get-avatar-icon';
// Types
import { TProfileInfoAvatar } from '../types';

const ProfileInfoAvatarComponent: FC<TProfileInfoAvatar> = ({ logout, avatar = '🐼' }) => {
  const renderIcon = useMemo(() => {
    const Icon = getAvatarIcon(avatar);

    return <Icon width={72} height={72} />;
  }, [avatar]);
  return (
    <div className="profile-info-avatar">
      <div className="profile-info-avatar__image">👷</div>
      <div
        role="button"
        className="profile-info-avatar__logout"
        onClick={logout}
        onKeyPress={logout}
        tabIndex={0}>
        Выйти
      </div>
    </div>
  );
};

export default memo(ProfileInfoAvatarComponent);
