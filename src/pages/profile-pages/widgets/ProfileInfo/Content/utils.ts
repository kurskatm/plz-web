// Modules
import ru from 'date-fns/locale/ru';
import { registerLocale } from 'react-datepicker';

registerLocale('ru', ru);

export const getYearsArr = (): number[] => {
  const currentYear = new Date().getFullYear();
  const yearsArr = [];
  let startYear = 1950;
  while (startYear <= currentYear) {
    // eslint-disable-next-line no-plusplus
    yearsArr.push(startYear++);
  }
  return yearsArr;
};

export const years = getYearsArr();

export const months = [
  'Январь',
  'Февраль',
  'Март',
  'Апрель',
  'Май',
  'Июнь',
  'Июль',
  'Август',
  'Сентябрь',
  'Октябрь',
  'Ноябрь',
  'Декабрь',
];

export const days = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];
