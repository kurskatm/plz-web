export type TProfileInfoProps = {
  error?: boolean;
  update?: (data: { access_token: string; payload: Record<string, unknown> }) => void;
  updateIsSuccess: boolean;
  updateIsError: boolean;
  updateIsPending: boolean;
  updateInfoDetails: string;
  info: TProfileFormProps;
  logout: () => void;
  token: string;
};

export type TEmailValue = {
  address: string;
  confirmationCode: string;
  confirmed: string;
  verified: string;
};

export type TProfileFormProps = {
  id?: string;
  token?: string;
  avatar?: string;
  fullName?: string;
  phone?: string;
  email?: TEmailValue;
  emailAddress?: string;
  emailConfirmed?: boolean;
  emailVerified?: boolean;
  birthday?: string;
  dateOfBirth?: string;
  gender?: number;
  status?: string;
  phoneNumber?: string;
  logout?: () => void;
  update?: (data: {
    access_token: string;
    payload: Record<string, unknown>;
    emailChanged?: boolean;
    onSuccess?: () => void;
  }) => void;
  updateIsSuccess: boolean;
  updateIsError: boolean;
  updateIsPending: boolean;
  updateInfoDetails: string;
  invitationId?: string;
};

export type TProfileDetailsItemProps = {
  title: string;
  value: string | number;
  hint?: string;
  error?: boolean;
  sendMail?: boolean;
  isGender?: boolean;
};

export type TProfileInfoAvatar = {
  avatar?: string;
  currentAvatar?: string;
  logout?: () => void;
};
