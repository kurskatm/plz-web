import React, { FC, memo } from 'react';

// Components
import ProfileInfoForm from './Content/ProfileInfoForm';
// Types
import { TProfileInfoProps } from './types';

const ProfileInfoComponent: FC<TProfileInfoProps> = ({
  token,
  info,
  logout,
  update,
  updateIsSuccess,
  updateIsError,
  updateIsPending,
  updateInfoDetails,
}) => (
  <div className="profile-info profile-widget">
    <ProfileInfoForm
      updateIsSuccess={updateIsSuccess}
      updateIsError={updateIsError}
      updateIsPending={updateIsPending}
      updateInfoDetails={updateInfoDetails}
      token={token}
      update={update}
      logout={logout}
      {...info}
    />
  </div>
);

export default memo(ProfileInfoComponent);
