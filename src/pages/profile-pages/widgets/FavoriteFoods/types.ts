import { THitProps } from '@models/products/types';
import { TUpdateShopingCart } from '@actions/shoping-cart/change/action-types';
import { TShopingCartModel } from '@/models/shoping-cart/types';
import { TCitiesItem } from '@/type/cities';
import { TFavoriteProduct } from '@/type/new-favorites';
import { TShowRestaurantModals } from '@/actions/restaurant/modal/action-types';
import { TProfileModel } from '@/models/profile/types';

export interface TFavoriteFoodsProps {
  foods: THitProps[];
  foodsForScores?: THitProps[];
  shopingCart: TShopingCartModel;
  city: TCitiesItem;
  shopingCartUpdate: TUpdateShopingCart;
  showRestaurantModals: TShowRestaurantModals;
  profile: TProfileModel;
  likeClickHandler: (
    id: string,
    newProd: TFavoriteProduct,
  ) => (e: MouseEvent, liked: boolean) => void;
}
