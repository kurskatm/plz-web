// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { shopingCartUpdate } from '@actions/shoping-cart/change';

const mapDispatchToProps = {
  shopingCartUpdate,
};

const withConnect = connect(null, mapDispatchToProps);

export const enhance = compose(withConnect);
