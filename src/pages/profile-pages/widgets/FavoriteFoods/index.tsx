import React, { FC, memo } from 'react';
// Utils
import { useShopingCartUpdate } from '@utils';
// Components
import FoodCards from './Content';
// Enhance
import { enhance } from './enhance';
// Types
import { TFavoriteFoodsProps } from './types';

const FavoriteFoodsComponent: FC<TFavoriteFoodsProps> = ({
  likeClickHandler,
  foods,
  foodsForScores,
  shopingCartUpdate,
  city,
  shopingCart,
  profile,
  showRestaurantModals,
}) => {
  const [updateShopingCart] = useShopingCartUpdate({
    city,
    shopingCart,
    shopingCartUpdate,
    profile,
    showRestaurantModals,
    configName: 'favoriteFood',
  });

  return (
    <div className="favorite-foods">
      <div className="favorite-foods__section">
        {foods && foods.length ? (
          <>
            <div className="favorite-foods__title">Любимые блюда</div>
            <FoodCards
              city={city}
              foods={foods.map((item) => ({
                ...item,
                isFavorite: true,
              }))}
              likeClickHandler={likeClickHandler}
              updateShopingCart={updateShopingCart}
              needLink={false}
            />
          </>
        ) : null}
      </div>
      {foodsForScores && foodsForScores.length ? (
        <div className="favorite-foods__section">
          <div className="favorite-foods__title">Любимые блюда за баллы</div>
          <FoodCards
            city={city}
            foods={foods}
            likeClickHandler={likeClickHandler}
            updateShopingCart={updateShopingCart}
            needLink
          />
        </div>
      ) : null}
      {/* <CardModal />
      <RestaurantModal /> */}
    </div>
  );
};

const FavoriteFoodsMemo = memo(FavoriteFoodsComponent);
export default enhance(FavoriteFoodsMemo);
