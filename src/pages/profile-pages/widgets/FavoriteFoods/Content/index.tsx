// Modules
import React, { FC, useMemo, memo, useCallback } from 'react';
import { useMediaQuery } from 'react-responsive';
import { useCardResize } from '@utils/useCardResize';
// Components
import ContentViewer from '@components/ContentViewer';
import HitCard from '@components/HitCard';
// Types
import { TFavouriteCardsListProps, TRestaurantProduct } from './types';

const CardWrapper: FC = ({ children }) => <div className="favorite-foods__list">{children}</div>;

const FavouriteFoodCardsComponent: FC<TFavouriteCardsListProps> = ({
  foods,
  city,
  likeClickHandler,
  needLink = false,
  updateShopingCart,
}) => {
  const isTwoCards = useMediaQuery({
    minWidth: 769,
  });
  const isOneCard = useMediaQuery({
    maxWidth: 768,
  });

  const { setCardHeight, getCardHeight } = useCardResize({
    cardsLength: foods.length,
    isOneCard,
    isTwoCards,
  });

  const getLink = useCallback(
    (item) => {
      if (!needLink) return undefined;
      const { urlName } = item.restaurant;
      return `/${city?.urlName}/restaurant/${urlName}`;
    },
    [city?.urlName, needLink],
  );

  const renderItems = useMemo(
    () => (item: TRestaurantProduct, index: number) => (
      <HitCard
        key={item.id}
        // @ts-ignore Property 'restaurant' is missing in type 'TRestaurantProduct'
        onLikeClick={likeClickHandler(item.id, item)}
        item={item}
        link={getLink(item)}
        // quantity={getQuantity(item)}
        updateShopingCart={updateShopingCart(item)}
        index={index}
        setItemHeight={setCardHeight}
        bodyHeight={getCardHeight(index)}
      />
    ),
    [likeClickHandler, updateShopingCart, getLink, setCardHeight, getCardHeight],
  );

  return (
    <div className="home-hits__content">
      <ContentViewer
        list={foods}
        renderItem={renderItems}
        responsive={{
          desktop: 4,
          tablet: 4,
          mobile: 4,
        }}
        wrapper={CardWrapper}
      />
    </div>
  );
};

export default memo(FavouriteFoodCardsComponent);
