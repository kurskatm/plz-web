// Main types
import { TFavoriteProduct } from '@type/new-favorites';
import { THitProps } from '@models/products/types';
import { TRestaurantProduct } from '@type/restaurants/restaurant-products';
import { TCitiesItem } from '@/type/cities';

export interface TFavouriteCardsListProps {
  foods: THitProps[];
  likeClickHandler: (
    id: string,
    newProd: TFavoriteProduct,
  ) => (e: MouseEvent, liked: boolean) => void;
  updateShopingCart: (item: TRestaurantProduct) => (count: number) => void;
  needLink?: boolean;
  city: TCitiesItem;
}

export { TRestaurantProduct };
