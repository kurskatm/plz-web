import { TRestaurantCardProps } from '@components/RestaurantCard/types';
import { THitProps } from '@models/products/types';
import { TCitiesItem } from '@type/cities';
import { TShopingCartModel } from '@models/shoping-cart/types';
import { TProfileModel } from '@/models/profile/types';
import {
  TProfileDeleteFavoriteProducts,
  TProfileDeleteFavoriteRests,
  TProfileSetFavoriteProducts,
  TProfileSetFavoriteRests,
} from '@actions/profile/favourite/action-types';
import {
  TProfileGetFavorites,
  TProfileGetFavoritesReset,
} from '@actions/profile/new-favorites/action-types';
import { TChangeAddressModalShow } from '@actions/checkout/modals/action-types';
import { TPushNotification } from '@actions/notifications/action-types';
import { TShowRestaurantModals } from '@actions/restaurant/modal/action-types';
import { TShopingCartSaveResetUi } from '@actions/shoping-cart/save/action-types';
import { History } from 'history';

export type TProfileFavoriteComponentProps = {
  profile: TProfileModel;
  cityId: string;
  userId: string;
  city: TCitiesItem;
  hitsAndRestsUiIsIdle: boolean;
  profileGetFavorite: TProfileGetFavorites;
  profileGetFavouriteResetData: TProfileGetFavoritesReset;
  rests: TRestaurantCardProps[];
  products: THitProps[];
  authModalShow: (data: boolean) => void;
  accessToken: string;
  shopingCart: TShopingCartModel;
  profileDeleteFavoriteProduct: TProfileDeleteFavoriteProducts;
  profileDeleteFavoriteRests: TProfileDeleteFavoriteRests;
  profileSetFavoriteRests: TProfileSetFavoriteRests;
  profileSetFavoriteProduct: TProfileSetFavoriteProducts;
  favoritesIsPending: boolean;
  favoritesIsSuccess: boolean;
  favoritesIsError: boolean;
  shopingCartSaveError: boolean;
  changeAddressModalShow: TChangeAddressModalShow;
  pushNotification: TPushNotification;
  showRestaurantModals: TShowRestaurantModals;
  history: History;
  shopingCartSaveResetUi: TShopingCartSaveResetUi;
};
