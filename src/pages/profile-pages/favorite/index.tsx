// Modules
import React, { memo, FC, useEffect, useState, useMemo, useCallback } from 'react';
import loadable from '@loadable/component';
import { useLocation } from 'react-router-dom';
import isNil from 'lodash/isNil';
// Utils
import { Consts } from '@utils';
// Main types
import { TFavoriteRestaurant, TFavoriteProduct } from '@type/new-favorites';
// Components
import CheckoutAddressModal from '@/components/CheckoutAddressModal';
import CardModal from '@components/RestaurantModals/ModifiersCardModal';
import Tabs from '@components/Tabs';
import GoToOldSite from '@components/MoveToOldDesignBanner/Banner';
import ProfileMenu from '../widgets/Menu';
import FavouriteFoods from '../widgets/FavoriteFoods';
import FavoriteRestaurants from '../widgets/FavoriteRestaurants';
// Types
import { TProfileFavoriteComponentProps } from './types';
// Enhance
import { enhance } from './enhance';

const { CITIES } = Consts.ROUTES;
const TABS = [
  {
    anchor: '#foods',
    text: 'Блюда',
  },
  {
    anchor: '#restaraunts',
    text: 'Рестораны',
  },
];

const Header = loadable(() => import('@components/Header'));
const Footer = loadable(() => import('@components/Footer'));
const Content = loadable(() => import('@components/Content'));

const ProfileFavoriteComponent: FC<TProfileFavoriteComponentProps> = ({
  profile,
  cityId,
  city,
  userId,
  rests,
  products,
  profileGetFavorite,
  profileGetFavouriteResetData,
  shopingCart,
  profileDeleteFavoriteProduct,
  profileDeleteFavoriteRests,
  profileSetFavoriteProduct,
  profileSetFavoriteRests,
  favoritesIsPending,
  favoritesIsSuccess,
  favoritesIsError,
  shopingCartSaveError,
  accessToken,
  pushNotification,
  showRestaurantModals,
  history,
  shopingCartSaveResetUi,
}) => {
  const location = useLocation();

  useEffect(
    () => {
      if (!accessToken) {
        history.push('/');
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const [showProducts, setShowProducts] = useState(false);
  const [showRests, setShowRests] = useState(false);

  useEffect(
    () => {
      if (
        userId &&
        cityId &&
        !isNil(profile.newGeoCode.data) &&
        profile.newGeoCode.data.precision === 'exact' &&
        !favoritesIsPending
      ) {
        profileGetFavorite({
          access_token: accessToken,
          cityId,
          latitude: profile.newGeoCode.data.lat,
          longitude: profile.newGeoCode.data.lng,
        });
      }
      if (profile.newGeoCode.data?.precision !== 'exact') {
        showRestaurantModals({
          showModal: true,
          modalType: 'change-address',
        });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [userId, cityId, profile.newGeoCode.data],
  );
  useEffect(
    () => {
      if (favoritesIsError) {
        pushNotification({
          type: 'error',
          content: 'Не удалось загрузить любимые блюда и рестораны',
        });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [favoritesIsError],
  );

  useEffect(
    () => () => {
      profileGetFavouriteResetData();
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const likeClickRestHandler = useCallback(
    (restId: string, newRest: TFavoriteRestaurant) => (e: Event, liked: boolean) => {
      e.preventDefault();

      if (liked) {
        profileDeleteFavoriteRests({ restId });
      } else {
        profileSetFavoriteRests({
          restId,
          newRestaurant: newRest,
        });
      }
    },
    [profileDeleteFavoriteRests, profileSetFavoriteRests],
  );

  const likeClickProdHandler = useCallback(
    (productId: string, newProd: TFavoriteProduct) => (e: Event, liked: boolean) => {
      e.preventDefault();

      if (liked) {
        profileDeleteFavoriteProduct({ productId });
      } else {
        profileSetFavoriteProduct({
          productId,
          newProduct: newProd,
        });
      }
    },
    [profileDeleteFavoriteProduct, profileSetFavoriteProduct],
  );

  useEffect(() => {
    setShowRests(rests?.length > 0 && favoritesIsSuccess);
    setShowProducts(products?.length > 0 && favoritesIsSuccess);
  }, [rests, products, favoritesIsSuccess]);

  const renderProducts = useMemo(
    () => () => {
      if (showProducts) {
        return (
          <FavouriteFoods
            likeClickHandler={likeClickProdHandler}
            foods={products}
            city={city}
            shopingCart={shopingCart}
            showRestaurantModals={showRestaurantModals}
            profile={profile}
          />
        );
      }

      return (
        <div className="profile-page__favourite-nofavorites">
          <div className="profile-page__favourite-nofavorites-img" />
          <div className="profile-page__favourite-nofavorites-text">Отмеченных блюд ещё нет</div>
        </div>
      );

      // if (favoritesIsSuccess) {
      //   return (
      //     <div className="profile-page__favourite-nofavorites">
      //       <div className="profile-page__favourite-nofavorites-img" />
      //       <div className="profile-page__favourite-nofavorites-text">
      //         Отмеченных блюд ещё нет
      //       </div>
      //     </div>
      //   );
      // }

      // return <div className="profile-page__favourite-placeholder" />;
    },
    [
      showProducts,
      likeClickProdHandler,
      products,
      city,
      shopingCart,
      showRestaurantModals,
      profile,
    ],
  );

  const renderRests = useMemo(
    () => () => {
      if (showRests) {
        return (
          <FavoriteRestaurants
            city={city}
            likeClickHandler={likeClickRestHandler}
            restaurants={rests}
          />
        );
      }

      return (
        <div className="profile-page__favourite-nofavorites">
          <div className="profile-page__favourite-nofavorites-img" />
          <div className="profile-page__favourite-nofavorites-text">
            Отмеченных ресторанов ещё нет
          </div>
        </div>
      );

      // if (favoritesIsSuccess) {
      //   return (
      //     <div className="profile-page__favourite-nofavorites">
      //       <div className="profile-page__favourite-nofavorites-img" />
      //       <div className="profile-page__favourite-nofavorites-text">
      //         Отмеченных ресторанов ещё нет
      //       </div>
      //     </div>
      //   );
      // }

      // return <div className="profile-page__favourite-placeholder" />;
    },
    [showRests, rests, city, likeClickRestHandler],
  );

  useEffect(
    () => {
      if (shopingCartSaveError) {
        // #3
        // (questionable, needs debugging for splitting between 'incorrect address' and rest errors)
        shopingCartSaveResetUi();
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [shopingCartSaveError],
  );

  return (
    <>
      <div className="profile-page profile-page-favorite grid-wrapper">
        <header className="grid-wrapper__header">
          <Header
            goBackText="На главную"
            goBackLink={CITIES.PATH}
            noAddress
            noMenu
            title="Личный кабинет"
          />
        </header>
        <main className="grid-wrapper__main">
          <div className="profile-page__header">
            <ProfileMenu location="favorite" />
          </div>
          <div className="profile-page__tabs-favourite">
            <Tabs list={TABS} currentTab={location.hash || '#foods'} />
          </div>
          <Content className="pad-m no-flex">
            <div className="profile-page__content-wrap profile-page-favorite__wrap">
              {location.hash !== '#restaraunts' && renderProducts()}
              {location.hash === '#restaraunts' && renderRests()}
            </div>
            <div className="profile-page__footer-extra-space"> </div>
          </Content>
        </main>
        <footer className="grid-wrapper__footer">
          <Footer />
        </footer>
        <CardModal />
        <CheckoutAddressModal />
      </div>
      <GoToOldSite />
    </>
  );
};

// @ts-ignore
export default enhance(memo(ProfileFavoriteComponent));
