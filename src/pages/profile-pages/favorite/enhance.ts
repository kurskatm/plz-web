// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
import {
  profileDeleteFavoriteProduct,
  profileDeleteFavoriteRests,
  profileSetFavoriteRests,
  profileSetFavoriteProduct,
} from '@actions/profile/favourite';
import { profileFetchFavorites, profileFetchFavoritesReset } from '@actions/profile/new-favorites';
import { authModalShow } from '@actions/auth';
import { changeAddressModalShow } from '@actions/checkout/modals';
import { pushNotification } from '@actions/notifications';
import { showRestaurantModals } from '@actions/restaurant/modal';
import { shopingCartSaveResetUi } from '@actions/shoping-cart/save';
// Selectors
import { profileFavoriteSelector as mapStateToProps } from '@selectors/structured-selectors/profile';

const mapDispatchToProps = {
  profileGetFavorite: profileFetchFavorites,
  profileGetFavouriteResetData: profileFetchFavoritesReset,
  authModalShow,
  profileDeleteFavoriteProduct,
  profileDeleteFavoriteRests,
  profileSetFavoriteRests,
  profileSetFavoriteProduct,
  changeAddressModalShow,
  pushNotification,
  showRestaurantModals,
  shopingCartSaveResetUi,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
