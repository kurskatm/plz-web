import { TBonusFetch, TBonusHistoryFetch, TBonusResetData } from '@actions/bonus/action-types';
import { TCitiesItem } from '@type/cities';
import { TFoodForPoints } from '@type/foodForPoints';
import { TFoodForPointsFetch, TFoodForPointsReset } from '@actions/foodForPoints/action-types';
import { TChangeAddressModalShow } from '@actions/checkout/modals/action-types';
import { TProfileSetSocialActivity } from '@actions/profile/social-activity/action-types';
import { TPushNotification } from '@actions/notifications/action-types';
import { TProfileModel } from '@models/profile/types';
import { TShopingCartSaveResetUi } from '@actions/shoping-cart/save/action-types';
import { History } from 'history';
import { TProfileFormProps } from '../widgets/ProfileInfo/types';

export interface TScoresListItem {
  description: string;
  balanceDifference: string;
  loggedOn: string;
}
export interface TBalanceDifferenceListProps {
  list: TScoresListItem[];
}

export type TProfileScoresComponentProps = {
  bonusHistory: TScoresListItem[];
  cityId: string;
  city: TCitiesItem;
  bonusFetch: TBonusFetch;
  bonusHistoryFetch: TBonusHistoryFetch;
  bonusResetData: TBonusResetData;
  balance: string;
  balanceStatus: string;
  balanceStatusIsError: boolean;
  historyStatus: string;
  historyStatusIsError: boolean;
  accessToken: string;
  socials?: number[];
  info?: TProfileFormProps;
  search: string;
  selectedCuisines: string[];
  selectedFilters: string[];
  profile: TProfileModel;
  shopingCartSaveError: boolean;
  changeAddressModalShow: TChangeAddressModalShow;
  profileSetSocialActivity: TProfileSetSocialActivity;
  pushNotification: TPushNotification;
  history: History;
  shopingCartSaveResetUi: TShopingCartSaveResetUi;
  foodForPoints: TFoodForPoints;
  foodForPointsisIdle: boolean;
  foodForPointsisError: boolean;
  foodForPointsisPending: boolean;
  foodForPointsFetch: TFoodForPointsFetch;
  foodForPointsReset: TFoodForPointsReset;
};
