// Modules
import React, { memo, FC, useEffect, useCallback } from 'react';
import loadable from '@loadable/component';
import { useLocation } from 'react-router-dom';
// Utils
import { Consts } from '@utils';
// Components
import Tabs from '@components/Tabs';
import CheckoutAddressModal from '@/components/CheckoutAddressModal';
import GoToOldSite from '@components/MoveToOldDesignBanner/Banner';
import { ScoresIcon } from 'chibbis-ui-kit';
import ProfileMenu from '../widgets/Menu';
import ScoresList from '../widgets/ScoresList';
import ScoresSpend from '../widgets/ScoresSpend';
import ScoresGet from '../widgets/ScoresGet';

// Icons

// Types
import { TProfileScoresComponentProps } from './types';
// Enhance
import { enhance } from './enhance';

const { CITIES } = Consts.ROUTES;
const TABS = [
  {
    anchor: '#history',
    text: 'История',
  },
  {
    anchor: '#howtoget',
    text: 'Как получить баллы',
  },
  {
    anchor: '#howtospend',
    text: 'Как потратить баллы',
  },
];

const Header = loadable(() => import('@components/Header'));
const Footer = loadable(() => import('@components/Footer'));
const Content = loadable(() => import('@components/Content'));

const ProfileScoresComponent: FC<TProfileScoresComponentProps> = ({
  balance = '0',
  bonusHistory,
  city,
  socials = [],
  info,
  search,
  selectedCuisines,
  selectedFilters,
  profile,
  shopingCartSaveError,
  accessToken,
  bonusFetch,
  bonusHistoryFetch,
  bonusResetData,
  balanceStatus,
  profileSetSocialActivity,
  balanceStatusIsError,
  historyStatusIsError,
  pushNotification,
  cityId,
  history,
  shopingCartSaveResetUi,
  foodForPoints,
  foodForPointsisIdle,
  foodForPointsFetch,
  foodForPointsReset,
}) => {
  const location = useLocation();

  useEffect(
    () => {
      if (!accessToken) {
        history.push('/');
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  useEffect(
    () => {
      if (accessToken && balanceStatus === 'idle') {
        // #1
        bonusFetch({ access_token: accessToken });
        // #2
        bonusHistoryFetch({ access_token: accessToken });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [accessToken, balanceStatus],
  );

  useEffect(
    () => {
      if (balanceStatusIsError) {
        pushNotification({
          type: 'error',
          content: 'Не удалось загрузить баллы пользователя',
        });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [balanceStatusIsError],
  );

  useEffect(
    () => {
      if (historyStatusIsError) {
        pushNotification({
          type: 'error',
          content: 'Не удалось загрузить историю по баллам',
        });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [historyStatusIsError],
  );

  useEffect(
    () => () => {
      bonusResetData();
      foodForPointsReset();
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  useEffect(() => {
    if (foodForPointsisIdle) {
      const foodForPointsArgs = { cityId };

      if (profile.newGeoCode.data) {
        // @ts-ignore
        foodForPointsArgs.lat = profile.newGeoCode.data.lat;
        // @ts-ignore
        foodForPointsArgs.lng = profile.newGeoCode.data.lng;
      }
      foodForPointsFetch(foodForPointsArgs);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    city,
    profile.newGeoCode.data,
    search,
    selectedCuisines,
    selectedFilters,
    foodForPointsisIdle,
  ]);

  useEffect(
    () => {
      if (shopingCartSaveError) {
        shopingCartSaveResetUi();
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [shopingCartSaveError],
  );

  const getCost = useCallback((costNum: number) => {
    const costStr = String(costNum);
    if (costStr.length > 3) {
      return `${costStr.slice(0, costStr.length - 3)} ${costStr.slice(costStr.length - 3)}`;
    }

    return costStr;
  }, []);
  return (
    <>
      <div className="profile-page profile-page-scores grid-wrapper">
        <header className="grid-wrapper__header">
          <Header
            goBackText="На главную"
            goBackLink={CITIES.PATH}
            noAddress
            noMenu
            title="Личный кабинет"
          />
        </header>
        <main className="grid-wrapper__main">
          <div className="profile-page__header">
            <ProfileMenu location="scores" />
          </div>
          <div className="profile-page__score">
            <div className="profile-page__score-text">Баллов на счету</div>
            <div className="profile-page__score-num">
              <ScoresIcon width={40} height={40} />
              {getCost(Number(balance))}
            </div>
          </div>
          <div className="profile-page__tabs-scores">
            <Tabs list={TABS} currentTab={location.hash || '#history'} />
          </div>
          <Content className="pad-m no-flex">
            <div className="profile-page__content-wrap profile-page-scores__wrap">
              {(location.hash === '#history' || !location.hash) && (
                <div className="profile-page__scores">
                  <ScoresList list={bonusHistory} />
                </div>
              )}
              {location.hash === '#howtospend' && (
                <ScoresSpend
                  city={city}
                  rest={foodForPoints?.restaurants || []}
                  food={foodForPoints?.products || []}
                />
              )}
              {location.hash === '#howtoget' && (
                <ScoresGet
                  email={info?.emailAddress}
                  invitationId={info?.invitationId}
                  socials={socials}
                  accessToken={accessToken}
                  profileSetSocialActivity={profileSetSocialActivity}
                />
              )}
            </div>
            <div className="profile-page__footer-extra-space"> </div>
          </Content>
        </main>
        <footer className="grid-wrapper__footer">
          <Footer />
        </footer>
        <CheckoutAddressModal />
      </div>
      <GoToOldSite />
    </>
  );
};

// @ts-ignore Type 'TInfoDataModel' is not assignable to type 'TProfileFormProps'
export default enhance(memo(ProfileScoresComponent));
