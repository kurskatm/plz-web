// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { changeAddressModalShow } from '@actions/checkout/modals';
import { bonusFetch, bonusHistoryFetch, bonusResetData } from '@actions/bonus';
import { profileSetSocialActivity } from '@actions/profile/social-activity';
import { pushNotification } from '@actions/notifications';
import { shopingCartSaveResetUi } from '@actions/shoping-cart/save';
import { foodForPointsFetch, foodForPointsReset } from '@actions/foodForPoints';
// Selectors
import { profileScoresSelector as mapStateToProps } from '@selectors/structured-selectors/profile';

const mapDispatchToProps = {
  changeAddressModalShow,
  bonusFetch,
  bonusHistoryFetch,
  bonusResetData,
  profileSetSocialActivity,
  pushNotification,
  shopingCartSaveResetUi,
  foodForPointsFetch,
  foodForPointsReset,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
