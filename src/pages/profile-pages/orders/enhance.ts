// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { profileGetNewOrders, profileGetNewOrdersReset } from '@actions/profile/new-orders';
import { profileOrderRepeatOrderResetRedirect } from '@actions/profile/repeat-order';
import { profileOrderContactMeReset } from '@actions/profile/contact-me';
import { pushNotification } from '@actions/notifications';
import { showShoppingCardModal } from '@actions/restaurant/modal';

// Selectors
import { profileOrdersSelector as mapStateToProps } from '@selectors/structured-selectors/profile';

const mapDispatchToProps = {
  // Etc
  profileOrderRepeatOrderResetRedirect,
  profileOrderContactMeReset,
  pushNotification,
  showShoppingCardModal,
  // New orders
  profileGetNewOrders,
  profileGetNewOrdersReset,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
