// Main types
import { TUserOrdersData } from '@type/profile';
import { TPaginationObject } from '@models/pagination/types';
import { TOrderToCart } from '@/type/orderFromProfileToCart';
import { TProfileOrderRepeatOrderResetRedirect } from '@actions/profile/repeat-order/action-types';
import { TPushNotification } from '@actions/notifications/action-types';
import { TCitiesItem } from '@type/cities';
import { TShowShoppingCardModal } from '@actions/restaurant/modal/action-types';
import {
  TProfileGetNewOrders,
  TProfileGetNewOrdersReset,
} from '@actions/profile/new-orders/action-types';
import { TProfileOrderContactMeReset } from '@actions/profile/contact-me/action-types';

export interface TOrdresComponentProps {
  accessToken: string;
  repeatOrderData: TOrderToCart;
  repeatOrderNeedRedirect: boolean;
  repeatOrderIsError: boolean;
  profileOrderRepeatOrderResetRedirect: TProfileOrderRepeatOrderResetRedirect;
  pushNotification: TPushNotification;
  showShoppingCardModal: TShowShoppingCardModal;
  city: TCitiesItem;
  profileGetNewOrders: TProfileGetNewOrders;
  profileGetNewOrdersReset: TProfileGetNewOrdersReset;
  newOrdersData: TUserOrdersData;
  newOrdersPagination: TPaginationObject;
  newOrdersAfter: string | null;
  newOrdersIsIdle: boolean;
  newOrdersIsPending: boolean;
  newOrdersIsSuccess: boolean;
  newOrdersIsError: boolean;
  contactMeOrderId: string;
  profileOrderContactMeReset: TProfileOrderContactMeReset;
}
