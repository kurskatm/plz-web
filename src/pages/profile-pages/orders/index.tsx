// Modules
import React, { memo, FC, useState, useEffect, useCallback } from 'react';
import loadable from '@loadable/component';
import isNil from 'lodash/isNil';
import { useHistory } from 'react-router';
import { useParams } from 'react-router-dom';
import { useMediaQuery } from 'react-responsive';
import urlJoin from 'url-join';
// Utils
import { Consts } from '@utils';
// Types
import { TCityRouterParams } from '@type/routes';
// Components
import { LoadingSpinner } from 'chibbis-ui-kit';
import OrderReviewModal from '@components/OrderModal';
import GoToOldSite from '@components/MoveToOldDesignBanner/Banner';
import ProfileMenu from '../widgets/Menu';
import ProfileOrder from '../widgets/ProfileOrder';
// Enhance
import { enhance } from './enhance';
// Types
import { TOrdresComponentProps } from './types';

const { CITIES } = Consts.ROUTES;
const { PATH } = Consts.ROUTES.CHECKOUT.MAIN;

const Header = loadable(() => import('@components/Header'));
const Footer = loadable(() => import('@components/Footer'));
const Content = loadable(() => import('@components/Content'));

const OrdersComponent: FC<TOrdresComponentProps> = ({
  accessToken,
  // repeat order data for redirect
  repeatOrderData,
  repeatOrderNeedRedirect,
  repeatOrderIsError,
  profileOrderRepeatOrderResetRedirect,
  pushNotification,
  showShoppingCardModal,
  // new orders
  newOrdersData,
  newOrdersPagination,
  newOrdersIsIdle,
  newOrdersIsPending,
  newOrdersIsSuccess,
  newOrdersIsError,
  newOrdersAfter,
  profileGetNewOrders,
  profileGetNewOrdersReset,
  contactMeOrderId,
  profileOrderContactMeReset,
}) => {
  const history = useHistory();
  const { cityUrl = '/' } = useParams<TCityRouterParams>();
  const isTabletOrMobile = useMediaQuery({
    query: '(max-width: 768px)',
  });

  useEffect(
    () => {
      if (!accessToken) {
        history.push('/');
      }
      return () => {
        profileOrderContactMeReset({ orderId: contactMeOrderId });
      };
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const [showOrders, setShowOrders] = useState(true);
  const [orderContactMeClicked, setOrderContactMeClicked] = useState<string>('');

  const scrollEvent = useCallback(
    ({ target }: Event) => {
      const { scrollingElement } = target as Document;
      const { scrollHeight, clientHeight, scrollTop } = scrollingElement;
      const maxScroll = scrollHeight - clientHeight;
      const scrollPercentFetch = (scrollHeight / 100) * 20;
      const isCalledFetch = scrollTop >= maxScroll - scrollPercentFetch;

      if (newOrdersPagination.hasMore && isCalledFetch) {
        profileGetNewOrders({
          count: newOrdersPagination.limit,
          access_token: accessToken,
          after: newOrdersAfter,
        });
      }
    },
    [accessToken, newOrdersPagination, newOrdersAfter, profileGetNewOrders],
  );

  useEffect(() => {
    if (accessToken && newOrdersIsIdle) {
      profileGetNewOrders({
        count: newOrdersPagination.limit,
        access_token: accessToken,
        after: newOrdersAfter,
      });
    }
  }, [
    accessToken,
    newOrdersIsIdle,
    newOrdersPagination.limit,
    newOrdersAfter,
    profileGetNewOrders,
  ]);

  useEffect(() => {
    if (newOrdersIsError) {
      pushNotification({
        type: 'error',
        content: 'Не удалось получить заказы',
      });
    }
    if (repeatOrderIsError) {
      pushNotification({
        type: 'error',
        content: 'Некорректный адрес пользователя/нет доставки по адресу',
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [newOrdersIsError, repeatOrderIsError]);

  useEffect(
    () => () => {
      profileGetNewOrdersReset();
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  useEffect(() => {
    if (profileGetNewOrders) {
      document.addEventListener('scroll', scrollEvent);

      return () => {
        document.removeEventListener('scroll', scrollEvent);
      };
    }

    return undefined;
  }, [profileGetNewOrders, scrollEvent]);

  useEffect(() => {
    if (newOrdersData.length && newOrdersIsSuccess) {
      setShowOrders(true);
    }
    if (!newOrdersData.length && newOrdersIsSuccess) {
      setShowOrders(false);
    }
  }, [newOrdersData, newOrdersIsSuccess]);

  useEffect(
    () => {
      if (!isNil(repeatOrderData) && repeatOrderData.products.length && repeatOrderNeedRedirect) {
        if (!isTabletOrMobile) {
          const url = urlJoin(`/${cityUrl}`, PATH);
          history.push(url, { from: 'orders' });
          profileOrderRepeatOrderResetRedirect();
        } else {
          profileOrderRepeatOrderResetRedirect();
          showShoppingCardModal(true);
        }
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [repeatOrderData, repeatOrderNeedRedirect],
  );

  return (
    <>
      <div className="profile-page profile-page-order grid-wrapper">
        <OrderReviewModal />
        <header className="grid-wrapper__header">
          <Header
            goBackText="На главную"
            goBackLink={CITIES.PATH}
            noAddress
            noMenu
            title="Личный кабинет"
          />
        </header>
        <main className="grid-wrapper__main">
          <Content className="pad-m no-flex">
            <div className="profile-page__header">
              <ProfileMenu location="orders" />
            </div>
            <div className="profile-page__content-wrap">
              {showOrders ? (
                newOrdersData.map((order) => (
                  <div className="profile-page-order__order" key={`${order.order.id}`}>
                    <ProfileOrder
                      order={order}
                      orderContactMeClicked={orderContactMeClicked}
                      setOrderContactMeClicked={setOrderContactMeClicked}
                    />
                  </div>
                ))
              ) : (
                <div className="profile-page-order__no-order">
                  <div className="profile-page-order__no-order-img" />
                  <div className="profile-page-order__no-order-text">
                    Вы ещё ничего не заказывали
                  </div>
                </div>
              )}
              {newOrdersIsPending && (
                <LoadingSpinner className="profile-page-order__spinner" size="big" color="orange" />
              )}
            </div>
            <div className="profile-page-order__footer-extra-space"> </div>
          </Content>
        </main>
        <footer className="grid-wrapper__footer">
          <Footer />
        </footer>
      </div>
      <GoToOldSite />
    </>
  );
};

OrdersComponent.defaultProps = {
  newOrdersData: [],
};

export default enhance(memo(OrdersComponent));
