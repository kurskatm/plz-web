// @ts-nocheck
// Modules
import React, { memo, FC, useEffect, useState, useCallback } from 'react';
import loadable from '@loadable/component';
import { LoadingSpinner } from 'chibbis-ui-kit';
import ReviewsList from '@pages/restaurant/reviews/reviews-list';
import GoToOldSite from '@components/MoveToOldDesignBanner/Banner';
import { Consts } from '@utils';
import ProfileMenu from '../widgets/Menu';
import MailChecker from '../widgets/MailChecker';
// Enhance
import { enhance } from './enhance';
// Types
import { TReviewsComponentProps } from './types';

const { CITIES } = Consts.ROUTES;

const Header = loadable(() => import('@components/Header'));
const Footer = loadable(() => import('@components/Footer'));
const Content = loadable(() => import('@components/Content'));

const ProfileReviewsComponent: FC<TReviewsComponentProps> = ({
  reviews = [],
  reviewsLength,
  pagination,
  accessToken,
  profileGetReviews,
  reviewsIsIdle,
  reviewsIsPending,
  reviewsIsSuccess,
  reviewsIsError,
  info,
  profileGetReviewsResetData,
  pushNotification,
  history,
}) => {
  useEffect(
    () => {
      if (!accessToken) {
        history.push('/');
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const [showReviews, setShowReviews] = useState(true);

  const scrollEvent = useCallback(
    ({ target }: Event) => {
      const { scrollingElement } = target as Document;
      const { scrollHeight, clientHeight, scrollTop } = scrollingElement;
      const maxScroll = scrollHeight - clientHeight;
      const scrollPercentFetch = (scrollHeight / 100) * 20;
      const isCalledFetch = scrollTop >= maxScroll - scrollPercentFetch;

      if (pagination.hasMore && isCalledFetch) {
        profileGetReviews({
          pageNumber: pagination.page,
          pageSize: pagination.limit,
          access_token: accessToken,
        });
      }
    },
    [accessToken, pagination, profileGetReviews],
  );

  useEffect(() => {
    if (accessToken && reviewsIsIdle) {
      profileGetReviews({ access_token: accessToken });
    }
  }, [accessToken, profileGetReviews, reviewsIsIdle]);

  useEffect(() => {
    if (reviewsIsError) {
      pushNotification({
        type: 'error',
        content: 'Не удалось загрузить отзывы',
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [reviewsIsError]);

  useEffect(() => {
    if (profileGetReviews) {
      document.addEventListener('scroll', scrollEvent);

      return () => {
        document.removeEventListener('scroll', scrollEvent);
      };
    }

    return undefined;
  }, [profileGetReviews, scrollEvent]);

  useEffect(() => {
    if (reviews.length && reviewsIsSuccess) {
      setShowReviews(true);
    }
    if (!reviews.length && reviewsIsSuccess) {
      setShowReviews(false);
    }
  }, [reviews, reviewsIsSuccess]);

  useEffect(
    () => () => {
      profileGetReviewsResetData();
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const getQuantity = useCallback((quantity: number) => {
    if (quantity % 10 === 1 || quantity % 100 === 1 || quantity % 1000 === 1) {
      if (quantity === 11) {
        return `${quantity} отзывов`;
      }
      return `${quantity} отзыв`;
    }

    const quantityStr = String(quantity);
    if (quantityStr.length === 1) {
      if ([2, 3, 4].includes(quantity % 10)) {
        return `${quantity} отзыва`;
      }

      return `${quantity} отзывов`;
    }

    if (quantityStr.length > 1) {
      if ([12, 13, 14].includes(quantity)) {
        return `${quantity} отзывов`;
      }

      if ([2, 3, 4].includes(Number(quantityStr[quantityStr.length - 1]))) {
        return `${quantity} отзыва`;
      }

      return `${quantity} отзывов`;
    }

    return `${quantity} отзывов`;
  }, []);

  return (
    <>
      <div className="profile-page grid-wrapper">
        <header className="grid-wrapper__header">
          <Header
            goBackText="На главную"
            goBackLink={CITIES.PATH}
            noAddress
            noMenu
            title="Личный кабинет"
          />
        </header>
        <main className="grid-wrapper__main">
          <Content className="pad-m no-flex">
            <div className="profile-page__header">
              <ProfileMenu location="reviews" />
              {showReviews ? (
                <div className="profile-page__header-text">
                  <MailChecker checked={info?.emailConfirmed} />
                </div>
              ) : null}
            </div>
            <div className="profile-page__content-wrap">
              {showReviews ? (
                <div className="profile-page__reviews">
                  <div className="profile-page__reviews-title">{getQuantity(reviewsLength)}</div>
                  <ReviewsList list={reviews} />
                </div>
              ) : (
                <div className="profile-page__reviews-noreviews">
                  <div className="profile-page__reviews-noreviews-img" />
                  <div className="profile-page__reviews-noreviews-text">
                    Вы сможете оставить отзыв после оформления заказа
                  </div>
                </div>
              )}
              {reviewsIsPending && (
                <LoadingSpinner className="profile-page-order__spinner" size="big" color="orange" />
              )}
            </div>
            <div className="profile-page__footer-extra-space"> </div>
          </Content>
        </main>
        <footer className="grid-wrapper__footer">
          <Footer />
        </footer>
      </div>
      <GoToOldSite />
    </>
  );
};

export default enhance(memo(ProfileReviewsComponent));
