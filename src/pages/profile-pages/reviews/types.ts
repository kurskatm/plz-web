// Main types
import { TInfoDataModel } from '@models/profile/types';
import {
  TProfileGetReviews,
  TProfileGetReviewsResetData,
} from '@actions/profile/reviews/action-types';
import { History } from 'history';
import { TUserReviewsList } from '@type/profile';
import { TPaginationObject } from '@/models/pagination/types';
import { TPushNotification } from '@actions/notifications/action-types';

export interface TReviewsComponentProps {
  profileGetReviews: TProfileGetReviews;
  profileGetReviewsResetData: TProfileGetReviewsResetData;
  pushNotification: TPushNotification;
  reviews: TUserReviewsList;
  reviewsLength: number;
  pagination: TPaginationObject;
  accessToken: string;
  reviewsIsIdle: boolean;
  reviewsIsPending: boolean;
  reviewsIsSuccess: boolean;
  reviewsIsError: boolean;
  info: TInfoDataModel;
  history: History;
}
