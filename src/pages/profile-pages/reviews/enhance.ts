// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { profileGetReviews, profileGetReviewsResetData } from '@actions/profile/reviews';
import { pushNotification } from '@actions/notifications';
// Selectors
import { profileReviewsSelector as mapStateToProps } from '@selectors/structured-selectors/profile';

const mapDispatchToProps = {
  profileGetReviews,
  profileGetReviewsResetData,
  pushNotification,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
