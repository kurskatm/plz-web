// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import {
  profileGetInfo,
  profileUnauthorized,
  profileInfoUpdate,
  profileInfoUpdateResetUi,
  profileInfoResetUi,
} from '@actions/profile/info';
import { profileGetFriends } from '@actions/profile/friends';
import { unauthorize, authPhoneClean } from '@actions/auth';
import { bonusFetch, bonusHistoryFetch } from '@actions/bonus';
import { pushNotification } from '@actions/notifications';
// Selectors
import { profileInfoSelector as mapStateToProps } from '@selectors/structured-selectors/profile';

const mapDispatchToProps = {
  profileGetInfo,
  profileInfoUpdate,
  profileUnauthorized,
  bonusFetch,
  bonusHistoryFetch,
  unauthorize,
  profileInfoUpdateResetUi,
  profileInfoResetUi,
  profileGetFriends,
  pushNotification,
  authPhoneClean,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
