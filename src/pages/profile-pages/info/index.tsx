// Modules
import React, { memo, FC, useEffect } from 'react';
import loadable from '@loadable/component';

// Components
import { Consts } from '@utils';
import { removeProfileData } from '@lib/cookie';
import GoToOldSite from '@components/MoveToOldDesignBanner/Banner';
import ProfileInfo from '../widgets/ProfileInfo';
import ProfileScores from '../widgets/ProfileScores';
import ProfileFriends from '../widgets/ProfileFriends';
import ProfileMenu from '../widgets/Menu';
import MailChecker from '../widgets/MailChecker';
// Enhance
import { enhance } from './enhance';
// Types
import { TProfileProps } from './types';

const { CITIES } = Consts.ROUTES;

const Header = loadable(() => import('@components/Header'));
const Footer = loadable(() => import('@components/Footer'));
const Content = loadable(() => import('@components/Content'));

const ProfileComponent: FC<TProfileProps> = ({
  profileGetInfo,
  accessToken,
  info,
  history,
  balance = '0',
  balanceStatus,
  bonusHistory = [],
  profileUnauthorized,
  profileInfoUpdate,
  profileInfoUpdateResetUi,
  profileInfoResetUi,
  profileGetFriends,
  bonusFetch,
  bonusHistoryFetch,
  infoStatusIsIdle,
  friends,
  city,
  unauthorize,
  updateInfoIsError,
  updateInfoIsSuccess,
  updateInfoIsPending,
  updateInfoDetails,
  pushNotification,
  infoStatusIsError,
  balanceStatusIsError,
  historyStatusIsError,
  friendsStatusIsError,
  authPhoneClean,
}) => {
  useEffect(
    () => {
      if (!accessToken) {
        history.push('/');
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  useEffect(() => {
    if (accessToken && infoStatusIsIdle) {
      profileGetInfo({ access_token: accessToken });
      profileGetFriends({ access_token: accessToken });
    }
    if (balanceStatus === 'idle') {
      bonusFetch({ access_token: accessToken });
      bonusHistoryFetch({ access_token: accessToken });
    }
  }, [
    profileGetInfo,
    accessToken,
    balanceStatus,
    bonusFetch,
    bonusHistoryFetch,
    profileGetFriends,
    infoStatusIsIdle,
  ]);
  const logout = () => {
    history.push('/');
    profileUnauthorized();
    removeProfileData();
    unauthorize();
    authPhoneClean(info?.phoneNumber);
  };

  useEffect(
    () => {
      if (infoStatusIsError) {
        pushNotification({
          type: 'error',
          content: 'Не удалось загрузить профиль',
        });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [infoStatusIsError],
  );
  useEffect(
    () => {
      if (balanceStatusIsError) {
        pushNotification({
          type: 'error',
          content: 'Не удалось загрузить баллы пользователя',
        });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [balanceStatusIsError],
  );
  useEffect(
    () => {
      if (historyStatusIsError) {
        pushNotification({
          type: 'error',
          content: 'Не удалось загрузить историю по баллам',
        });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [historyStatusIsError],
  );
  useEffect(
    () => {
      if (friendsStatusIsError) {
        pushNotification({
          type: 'error',
          content: 'Не удалось загрузить список друзей',
        });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [friendsStatusIsError],
  );

  useEffect(
    () => () => {
      profileInfoUpdateResetUi();
      profileInfoResetUi();
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const historyShort = bonusHistory.slice(0, 3);

  useEffect(
    () => {
      if (updateInfoIsSuccess) {
        pushNotification({
          type: 'success',
          content: 'Профиль успешно отредактирован',
        });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [updateInfoIsSuccess],
  );
  useEffect(
    () => {
      if (updateInfoIsError) {
        pushNotification({
          type: 'error',
          content: 'Не удалось отредактировать профиль',
        });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [updateInfoIsError],
  );

  return (
    <>
      <div className="profile-page grid-wrapper">
        <header className="grid-wrapper__header">
          <Header
            // goBackText="На главную"
            // goBackLink={CITIES.PATH}
            noAddress
            noMenu
            title="Личный кабинет"
            noRightSection
          />
        </header>
        <main className="grid-wrapper__main">
          <Content className="pad-m no-flex">
            <div className="profile-page__header">
              {/* <ProfileMenu location="info" /> */}
              {/* <div className="profile-page__header-text">
                <MailChecker checked={info?.emailConfirmed} />
              </div> */}
            </div>
            <div className="profile-page__content-wrap">
              <div className="profile-page__widgets">
                <ProfileInfo
                  token={accessToken}
                  // @ts-ignore
                  update={profileInfoUpdate}
                  updateIsSuccess={updateInfoIsSuccess}
                  updateIsError={updateInfoIsError}
                  updateIsPending={updateInfoIsPending}
                  logout={logout}
                  info={info}
                  updateInfoDetails={updateInfoDetails}
                />
              </div>
            </div>
          </Content>
        </main>
      </div>
    </>
  );
};

// @ts-ignore
export default enhance(memo(ProfileComponent));
