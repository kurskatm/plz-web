import {
  TProfileGetInfo,
  TProfileInfoUpdateResetUi,
  TProfileInfoResetUi,
} from '@actions/profile/info/action-types';
import { TProfileGetFriends } from '@actions/profile/friends/action-types';
import { TUnauthorize, TPhoneSendClean } from '@actions/auth/action-types';
import { TBonusFetch, TBonusHistoryFetch } from '@actions/bonus/action-types';
import { TPushNotification } from '@/actions/notifications/action-types';
import { History } from 'history';
import { TProfileFormProps } from '../widgets/ProfileInfo/types';
import { TProfileScoresListItem } from '../widgets/ProfileScores/types';

export interface TProfileFriends {
  invitationId?: string;
  customerId: string;
  cityUrl?: string;
  invitees: {
    earnedBonusPoints: number;
    name: string;
    avatar: string;
    type: number;
  }[];
}
export interface TProfileProps {
  accessToken: string;
  info: TProfileFormProps;
  updateInfoIsSuccess: boolean;
  updateInfoIsError: boolean;
  updateInfoIsPending: boolean;
  updateInfoDetails: string;
  history: History;
  profileGetInfo: TProfileGetInfo;
  profileInfoUpdate: TProfileGetInfo;
  profileInfoUpdateResetUi: TProfileInfoUpdateResetUi;
  profileInfoResetUi: TProfileInfoResetUi;
  profileUnauthorized: TProfileGetInfo;
  profileGetFriends: TProfileGetFriends;
  bonusFetch: TBonusFetch;
  bonusHistoryFetch: TBonusHistoryFetch;
  pushNotification: TPushNotification;
  balance: string;
  balanceStatus: string;
  balanceStatusIsError: boolean;
  bonusHistory: TProfileScoresListItem[];
  historyStatus: string;
  historyStatusIsError: boolean;
  logout: () => void;
  infoStatusIsIdle?: boolean;
  infoStatusIsError?: boolean;
  friends?: TProfileFriends;
  friendsStatusIsError: boolean;
  invitationId?: string;
  city?: Record<string, string>;
  unauthorize: TUnauthorize;
  authPhoneClean: TPhoneSendClean;
}
