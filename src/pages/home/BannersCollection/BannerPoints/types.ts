import { TProfileModel } from '@models/profile/types';

export type TBannerPointsProps = {
  city: string;
  profile: TProfileModel;
  balance: number;
  authModalShow: (data: boolean) => void;
};
