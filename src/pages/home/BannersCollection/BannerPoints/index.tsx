// Modules
import React, { FC, memo, useMemo, useCallback } from 'react';
import { useHistory } from 'react-router-dom';
// Utiles
import { matchDeclinations } from '@utils/matchDeclinations';
// Constants
import { pointsEnoughBorder } from '../constants';
// Types
import { TBannerPointsProps } from './types';

const BannerPointsComponent: FC<TBannerPointsProps> = ({
  profile,
  balance,
  authModalShow,
  city,
}) => {
  const isBalanceEnough = balance > pointsEnoughBorder;
  const showModal = useCallback(() => authModalShow(true), [authModalShow]);
  const { push } = useHistory();

  const renderPoints = useMemo(
    () => (
      <div className="pointsWrapper">
        <div className="pointsWrapper__title">Баланс</div>
        <div className="pointsWrapper__pointsAmount">
          {`${balance} ${matchDeclinations(balance, ['балл', 'балла', 'баллов'])}`}
        </div>
        <div
          className={`pointsWrapper__Footer ${
            isBalanceEnough ? 'pointsWrapper__Footer__Balance' : 'pointsWrapper__Footer__NoBalance'
          }`}>
          {isBalanceEnough
            ? 'Добавьте +1 блюдо бесплатно из категории «За баллы»'
            : 'Как заработать баллы?'}
        </div>
      </div>
    ),
    [balance, isBalanceEnough],
  );

  const toPoints = useCallback(() => push(`/${city}/profile/scores`), [push, city]);

  return (
    <div className="bannerPoints" onClickCapture={!profile.auth ? showModal : toPoints}>
      <div className="bannerWrapper" style={{ backgroundColor: '#D5E6FF' }}>
        {!profile.auth && (
          <div className="bannerWrapper__text">Зарегистрируйтесь и добавьте +1 блюдо бесплатно</div>
        )}
        {profile.auth && renderPoints}
        <div className="bannerWrapper__img coin" />
      </div>
    </div>
  );
};

const BannerPointsMemo = memo(BannerPointsComponent);
export default BannerPointsMemo;
