export const bannersDraft = [
  {
    text: 'Рестораны с едой за баллы',
    color: '#E1F5E7',
    class: 'pizza',
    type: 'bonuses',
  },
  {
    text: 'Хиты продаж',
    color: '#FFE5E5',
    class: 'salad',
    type: 'hits',
  },
];

export const pointsEnoughBorder = 300;
