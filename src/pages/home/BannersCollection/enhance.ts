// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { authModalShow } from '@actions/auth';
import { filtersSet } from '@actions/filters';
// Selectors
import { bannersCollectionStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/home';

const mapDispatchToProps = {
  authModalShow,
  filtersSet,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
