// Main Types
import { TProfileModel } from '@models/profile/types';
import { TFilter } from '@/type/filters';
import { TFiltersSet } from '@actions/filters/action-types';
import { TRestaurantsAndHits } from '@/models/restaurants-and-hits/types';
import { TCitiesItem } from '@/type/cities';

export type TBannersCollectionProps = {
  city: TCitiesItem;
  profile: TProfileModel;
  balance: number;
  bonusPointsFilter: TFilter;
  restaurantsAndHits: TRestaurantsAndHits;
  authModalShow: (data: boolean) => void;
  filtersSet: TFiltersSet;
  onHitBannerClick: () => void;
};
