import React, { FC, memo, useRef, useCallback } from 'react';
import isNil from 'lodash/isNil';
import { useHistory } from 'react-router';
// Components
import BannerPoints from './BannerPoints';
// Enhance
import { enhance } from './enhance';
// Types
import { TBannersCollectionProps } from './types';
import { bannersDraft } from './constants';

const BannersCollectionComponent: FC<TBannersCollectionProps> = ({
  city,
  profile,
  balance,
  bonusPointsFilter,
  restaurantsAndHits,
  authModalShow,
}) => {
  const history = useHistory();
  const outerRef = useRef<HTMLDivElement>();
  const contentRef = useRef<HTMLDivElement>();

  const onClick = useCallback(
    (type: string) => {
      history.push(`/${city?.urlName}/selection/${type}`);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [bonusPointsFilter],
  );

  if (!isNil(restaurantsAndHits)) {
    return (
      <div className="bannersCollection" ref={outerRef}>
        <div className="bannersMainWrapper pad-m" ref={contentRef}>
          <BannerPoints
            profile={profile}
            balance={balance}
            authModalShow={authModalShow}
            city={city?.urlName}
          />
          {bannersDraft.map((banner) => (
            <div
              key={`${banner.color} - ${banner.class}`}
              className="bannerWrapper"
              style={{ backgroundColor: banner.color }}
              role="presentation"
              onClick={() => onClick(banner?.type)}>
              <div className="bannerWrapper__text">{banner.text}</div>
              <div className={`bannerWrapper__img ${banner.class}`} />
            </div>
          ))}
        </div>
      </div>
    );
  }

  return null;
};

const BannersCollectionMemo = memo(BannersCollectionComponent);
export default enhance(BannersCollectionMemo);
