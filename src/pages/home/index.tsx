// Modules
import React, { FC, useState, memo, useCallback, useEffect } from 'react';
import loadable from '@loadable/component';
import { useLocation } from 'react-router';
import { useParams } from 'react-router-dom';
// Components
import GoToOldSite from '@components/MoveToOldDesignBanner/Banner';
import { TCityRouterParams } from '@/type/routes';
import { getCookieCityData } from '@/lib/cookie';
import { Consts } from '@/utils/consts';

const { IS_CLIENT } = Consts.ENV;

const Header = loadable(() => import('@components/Header'));
const CuisinesList = loadable(() => import('./CuisinesList'));
const Footer = loadable(() => import('@components/Footer'));
const RestaurantsList = loadable(() => import('./RestaurantsList'));
const HotOffersList = loadable(() => import('./HotOffersList'));
const HitsList = loadable(() => import('./HitsList'));
const BannersCollection = loadable(() => import('./BannersCollection'));
const Reviews = loadable(() => import('./ReviewsList'));
const FiltersBlock = loadable(() => import('./FiltersBlock'));

const HomeComponent: FC = () => {
  const [viewFullList, setFullList] = useState(false);
  const onHitBannerClickCb = useCallback(() => setFullList(true), []);
  const location = useLocation();
  const { cityUrl } = useParams<TCityRouterParams>();

  useEffect(
    () => {
      // подумать как сделать лучше
      if (cityUrl && !getCookieCityData() && IS_CLIENT) {
        window?.location?.reload();
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [location],
  );
  useEffect(() => {
    if (IS_CLIENT) {
      window.scrollTo(0, 0);
    }
  }, []);

  return (
    <>
      <div className="home-page grid-wrapper">
        <header className="grid-wrapper__header">
          <Header />
        </header>
        <main className="grid-wrapper__main">
          <div className="home-page__body-wrap">
            <BannersCollection onHitBannerClick={onHitBannerClickCb} />
            <CuisinesList />
            <FiltersBlock />
            <RestaurantsList viewFullList={viewFullList} />
            <HitsList />
            <HotOffersList />
            <Reviews />
          </div>
        </main>
        <footer className="grid-wrapper__footer">
          <Footer />
        </footer>
      </div>
      <GoToOldSite />
    </>
  );
};
export default memo(HomeComponent);
