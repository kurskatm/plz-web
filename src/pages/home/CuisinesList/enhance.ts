// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { cuisinesFetch, cuisinesPush, cuisinesRemove, cuisinesUpdate } from '@actions/cuisines';
import { filtersFetch } from '@actions/filters';
import { pushNotification } from '@actions/notifications';
// Selectors
import { homeCuisinesListStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/home';

const mapDispatchToProps = {
  cuisinesPush,
  cuisinesFetch,
  cuisinesRemove,
  cuisinesUpdate,
  filtersFetch,
  pushNotification,
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
