// Modules
import loadable from '@loadable/component';
import React, { FC, memo, useCallback, useEffect, useMemo } from 'react';
import { useLocation, useHistory } from 'react-router';

import classNames from 'classnames';
// Modules Components
import { Button } from 'chibbis-ui-kit';
// Enhance
import { enhance } from './enhance';
// Types
import { TCuisinesListProps, TRenderListItem } from './types';
import { useQueryFilters } from '../Filters/useFiltersQuery';

const Slider = loadable(() => import('@components/Slider'));
const Filters = loadable(() => import('../Filters'));

const CuisinesListComponent: FC<TCuisinesListProps> = ({
  cuisinesChosen,
  cuisinesFetch,
  cuisinesList,
  cuisinesPush,
  cuisinesRemove,
  cuisinesUpdate,
  cuisinesUiIsIdle,
  cuisinesUiIsSuccess,
  cuisinesUiIsError,
  filters,
  filtersFetch,
  filtersIsIdle,
  filtersIsSuccess,
  cityId,
  search,
  pushNotification,
}) => {
  const { cuisines, setFilters } = useQueryFilters();
  const location = useLocation();
  const history = useHistory();

  useEffect(
    () => {
      if (cuisinesUiIsError) {
        pushNotification({
          type: 'error',
          content: 'Не удалось загрузить данные по специализациям ресторанов',
        });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [cuisinesUiIsError],
  );

  useEffect(
    () => {
      if (cuisinesUiIsIdle) {
        cuisinesFetch();
      }

      if (filtersIsIdle && cityId) {
        filtersFetch(cityId);
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [filtersIsIdle, cityId],
  );

  useEffect(
    () => {
      if (cuisinesUiIsSuccess) {
        const newSelectedList = cuisinesList
          .filter(({ urlName }) => cuisines?.includes(urlName))
          .map(({ id }) => id);

        cuisinesUpdate(newSelectedList);
      }
      return () => {
        cuisinesUpdate([]);
      };
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [cuisinesUiIsSuccess],
  );

  useEffect(
    () => {
      if (cuisines?.length && search.length) {
        cuisinesUpdate([]);
        setFilters({ cuisines: undefined });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [cuisines?.length, search.length],
  );

  const isInclude = useCallback(
    (id) => {
      const index = cuisinesChosen.findIndex((listId) => listId === id);
      const isIncludes = index > -1;

      return { index, isIncludes };
    },
    [cuisinesChosen],
  );

  const onClick = useCallback(
    (isIncludes: boolean, index: number, id: string, name: string) => {
      if (location?.pathname.indexOf('specialization')) {
        history.replace(`/${location.pathname.split('/')[1]}`);
      }
      const indexCuisineParam = cuisines?.indexOf(name);

      if (isIncludes && cuisines) {
        const newCuisines =
          indexCuisineParam === -1
            ? []
            : cuisines.slice(0, indexCuisineParam).concat(cuisines.slice(indexCuisineParam + 1));

        setFilters({ cuisines: newCuisines.length ? newCuisines : undefined });

        cuisinesRemove(index);
      } else {
        if (!cuisines) {
          setFilters({ cuisines: [name] });
          cuisinesPush(id);
          return;
        }

        setFilters({ cuisines: [...cuisines, name] });
        cuisinesPush(id);
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [cuisines, cuisinesPush, cuisinesRemove, setFilters],
  );

  const getStylesItem = useCallback(
    (isIncludes: boolean) =>
      classNames('home__cuisines-list__item', isIncludes && 'home__cuisines-list__item-include'),
    [],
  );

  const getMainStyles = useCallback(
    () =>
      classNames(
        'home__cuisines-list',
        'pad-m',
        cuisinesUiIsSuccess && filtersIsSuccess && 'data-is-success',
      ),
    [cuisinesUiIsSuccess, filtersIsSuccess],
  );

  const renderItem = useMemo(
    () => ({ id, name, urlName }: TRenderListItem) => {
      const { index, isIncludes } = isInclude(id);

      return (
        <Button
          key={id}
          className={getStylesItem(isIncludes)}
          htmlType="button"
          onClick={() => onClick(isIncludes, index, id, urlName)}
          size="small"
          type="white">
          {name}
        </Button>
      );
    },
    [getStylesItem, isInclude, onClick],
  );

  if (!search.length) {
    return (
      <div className={getMainStyles()}>
        {cuisinesUiIsSuccess && filtersIsSuccess && cuisinesList.length ? (
          <Slider
            list={cuisinesList}
            prefix={cuisinesUiIsSuccess ? <Filters filters={filters} /> : undefined}
            renderItem={renderItem}
          />
        ) : null}
      </div>
    );
  }

  return null;
};

const CuisinesListMemo = memo(CuisinesListComponent);
export default enhance(CuisinesListMemo);
