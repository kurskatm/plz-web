// Main Types
import {
  TCuisinesFetch,
  TCuisinesPush,
  TCuisinesRemove,
  TCuisinesUpdate,
} from '@actions/cuisines/action-types';
import { TFiltersFetch } from '@actions/filters/action-types';
import { TCuisinesList, TCuisinesItem } from '@models/cuisines/types';
import { TFilter } from '@type/filters';
import { TPushNotification } from '@actions/notifications/action-types';

export interface TCuisinesListProps {
  cityId: string;
  cuisinesChosen: string[];
  cuisinesFetch: TCuisinesFetch;
  cuisinesList: TCuisinesList;
  cuisinesPush: TCuisinesPush;
  cuisinesRemove: TCuisinesRemove;
  cuisinesUpdate: TCuisinesUpdate;
  cuisinesUiIsIdle: boolean;
  cuisinesUiIsSuccess: boolean;
  cuisinesUiIsError: boolean;
  filters: TFilter[];
  filtersFetch: TFiltersFetch;
  filtersIsIdle: boolean;
  filtersIsSuccess: boolean;
  cuisinesUiIsPending?: boolean;
  search: string;
  pushNotification: TPushNotification;
}

export type TRenderListItem = TCuisinesItem;
