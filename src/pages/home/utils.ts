// Modules
import { encodeDelimitedArray, decodeDelimitedArray } from 'serialize-query-params';

export const CommaArrayParam = {
  encode: (array: string[] | null | undefined): string | undefined =>
    encodeDelimitedArray(array, ','),

  decode: (arrayStr: string | string[] | null | undefined): string[] | undefined =>
    decodeDelimitedArray(arrayStr, ','),
};
