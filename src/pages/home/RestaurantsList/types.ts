// Main Types
import {
  TRestaurantsAndHitsFetch,
  TGetFreeProductsArgs,
} from '@actions/restaurants-and-hits/action-types';
import { TRestaurantsAndHits } from '@models/restaurants-and-hits/types';
import { TCitiesItem } from '@type/cities';
import {
  TProfileDeleteFavoriteRests,
  TProfileSetFavoriteRests,
} from '@actions/profile/favourite/action-types';
import { TCuisinesList } from '@models/cuisines/types';
import { TFilter } from '@type/filters';
import { TNewGeocodeData } from '@type/new-geocode';

export type TRestaurantsListComponentProps = {
  city: TCitiesItem;
  fetchRestaurantsAndHits: TRestaurantsAndHitsFetch;
  restaurantsAndHits: TRestaurantsAndHits;
  restaurantsAndHitsIsIdle: boolean;
  restaurantsAndHitsIsPending: boolean;
  restaurantsAndHitsIsSuccess: boolean;
  search: string;
  selectedCuisines: string[];
  selectedFilters: string[];
  profileDeleteFavoriteRests: TProfileDeleteFavoriteRests;
  profileSetFavoriteRests: TProfileSetFavoriteRests;
  cuisinesList: TCuisinesList;
  filtersList: TFilter[];
  authModalShow: (data: boolean) => void;
  viewFullList?: boolean;
  geocode: TNewGeocodeData;
  auth: string;
  userId?: string;
};

export { TGetFreeProductsArgs };
