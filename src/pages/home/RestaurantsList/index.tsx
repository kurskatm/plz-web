// Modules
import React, { useEffect, FC, memo, useCallback, useMemo, useRef } from 'react';
import { isNil } from 'lodash';
import { useQueryParams, withDefault } from 'use-query-params';
import { useParams } from 'react-router-dom';
import { useLocation } from 'react-router';
// Lib
import { likeClickHandler } from '@lib/like';
// Utils
import { CommaArrayParam } from '@pages/home/utils';
// Main Components
import ContentViewer from '@components/ContentViewer';
import RestaurantCard from '@components/NewRestaurantCard';
// Main Types
import { TRestaurantCard } from '@type/restaurants/restaurant-card';
// Enhance
import { enhance } from './enhance';
// Types
import { TRestaurantsListComponentProps, TGetFreeProductsArgs } from './types';

const CardWrapper: FC = ({ children }) => <div className="home-restaurants__list">{children}</div>;
const SelectionForScoresPath = new RegExp('/[a-z]*/selection/bonuses');

const RestaurantsListComponent: FC<TRestaurantsListComponentProps> = ({
  city,
  fetchRestaurantsAndHits,
  restaurantsAndHits,
  restaurantsAndHitsIsPending,
  restaurantsAndHitsIsSuccess,
  search,
  selectedCuisines,
  selectedFilters,
  profileDeleteFavoriteRests,
  profileSetFavoriteRests,
  cuisinesList,
  filtersList,
  authModalShow,
  viewFullList,
  geocode,
  auth,
  userId,
}) => {
  const [query] = useQueryParams({
    cuisines: withDefault(CommaArrayParam, [], false),
    filters: withDefault(CommaArrayParam, [], true),
  });
  // @ts-ignore
  const { specId } = useParams();
  const fetchedRestsAndHits = useRef(false);
  const filtersLength = useRef(0);
  const location = useLocation();
  const currentGeo = useRef({
    lat: geocode?.lat,
    lng: geocode?.lng,
  });

  const currentSearch = useRef(search);
  const currentAuth = useRef(auth);
  const fetchRests = useCallback(() => {
    const filtersFull = [...filtersList, ...cuisinesList]
      .filter(
        (e) =>
          query.cuisines.includes(e.urlName) ||
          query.filters.includes(e.urlName) ||
          e.urlName === specId,
      )
      .map((e) => e.id);
    const filters = search.trim().length ? [] : filtersFull;

    filtersLength.current = filters.length;
    currentGeo.current = {
      lat: geocode?.lat,
      lng: geocode?.lng,
    };
    const data: TGetFreeProductsArgs = {
      cityId: city?.id,
      lat: geocode?.lat,
      lng: geocode?.lng,
      filters,
      search,
      onSuccess: () => {
        fetchedRestsAndHits.current = true;
      },
    };

    if (auth) {
      data.showFavorites = true;
      data.userId = userId;
    }

    fetchRestaurantsAndHits(data);
  }, [
    geocode,
    city,
    cuisinesList,
    filtersList,
    fetchRestaurantsAndHits,
    search,
    query,
    specId,
    auth,
    userId,
  ]);

  // Fetch first time
  useEffect(() => {
    if (city?.id) {
      if (restaurantsAndHitsIsSuccess) {
        fetchedRestsAndHits.current = true;
      }
      if (
        !fetchedRestsAndHits.current &&
        !restaurantsAndHitsIsPending &&
        !restaurantsAndHitsIsSuccess &&
        (geocode?.precision === 'exact' || isNil(geocode))
      ) {
        fetchRests();
      }
      return () => {
        if (currentSearch.current.length || filtersLength?.current) {
          fetchRests();
        }
      };
    }

    return undefined;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // Fetch on auth change
  useEffect(() => {
    if (currentAuth.current !== auth) {
      currentAuth.current = auth;
      fetchRests();
    }
  }, [auth, fetchRests]);

  // Fetch on filters update
  useEffect(() => {
    if (
      !restaurantsAndHitsIsPending &&
      fetchedRestsAndHits.current &&
      (geocode?.precision === 'exact' || isNil(geocode)) &&
      city?.id
    ) {
      const filters = search.trim().length ? [] : [...selectedCuisines, ...selectedFilters];

      if (
        filtersLength?.current !== filters?.length ||
        // @ts-ignore
        currentGeo.current.lat !== geocode?.lat ||
        // @ts-ignore
        currentGeo.current.lng !== geocode?.lng
      ) {
        fetchRests();
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    city,
    geocode?.lng,
    geocode?.lat,
    geocode?.precision,
    selectedCuisines?.length,
    selectedFilters?.length,
  ]);

  // Fetch on search update
  useEffect(() => {
    if (!restaurantsAndHitsIsPending && currentSearch.current !== search && city?.id) {
      currentSearch.current = search;
      fetchRests();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [search]);

  // // Подборки
  useEffect(() => {
    // пока так - но надо доработать - чтобы подборки правильно работали без слежение за локацией
    fetchRests();
    //
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [location]);

  const getUrl = useCallback((urlName: string) => `/${city?.urlName}/restaurant/${urlName}`, [
    city?.urlName,
  ]);

  const renderItem = useMemo(
    () => (item: TRestaurantCard) => (
      <RestaurantCard
        onLikeClick={likeClickHandler({
          id: item.id,
          type: 'restId',
          additionalData: {
            newRestaurant: item,
          },
          likeAction: profileSetFavoriteRests,
          unLikeAction: profileDeleteFavoriteRests,
          auth: !auth && authModalShow,
        })}
        key={item.id}
        item={item}
        url={getUrl(item.urlName)}
      />
    ),
    [getUrl, auth, authModalShow, profileDeleteFavoriteRests, profileSetFavoriteRests],
  );
  if (
    restaurantsAndHitsIsSuccess &&
    !restaurantsAndHits.restaurants?.length &&
    !restaurantsAndHits.products?.length
  ) {
    return (
      <>
        <div className="home-restaurants__empty-list" />
        <div className="home-restaurants__empty-list-text">
          {location.pathname.match(SelectionForScoresPath)
            ? 'В вашем городе нет ресторанов с едой за баллы'
            : 'По этому запросу ничего не найдено'}
        </div>
      </>
    );
  }

  return restaurantsAndHits?.restaurants?.length ? (
    <div className="home-restaurants pad-m">
      <div className="home-restaurants-header">
        <div className="home-restaurants-header__title">Рестораны</div>
      </div>

      <div className="home-restaurants__content">
        <ContentViewer
          list={restaurantsAndHits?.restaurants}
          dataIsPending={restaurantsAndHitsIsPending}
          renderItem={renderItem}
          viewFullList={viewFullList}
          responsive={{
            desktop: 12,
            tablet: 6,
            mobile: 3,
          }}
          wrapper={CardWrapper}
          useAutoLoad
        />
      </div>
    </div>
  ) : null;
  // }
};

const RestaurantsListComponentMemo = memo(RestaurantsListComponent);

export default enhance(RestaurantsListComponentMemo);
