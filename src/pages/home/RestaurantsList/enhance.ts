// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { authModalShow } from '@actions/auth';
import { restaurantsAndHitsFetch } from '@actions/restaurants-and-hits';
import { profileDeleteFavoriteRests, profileSetFavoriteRests } from '@actions/profile/favourite';
// Selectors
import { homeRestaurantsListStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/home';

const mapDispatchToProps = {
  fetchRestaurantsAndHits: restaurantsAndHitsFetch,
  authModalShow,
  profileDeleteFavoriteRests,
  profileSetFavoriteRests,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
