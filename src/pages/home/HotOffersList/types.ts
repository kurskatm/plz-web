import { THotOfferProps } from '@components/HotOffer/types';
import { THotOffersFetch, THotOffersReset } from '@actions/hotOffers/action-types';
import { TRestaurantCardIdSave } from '@/actions/restaurant/main/id/action-types';
import { TPushNotification } from '@actions/notifications/action-types';
import { TRestaurantsAndHits } from '@/models/restaurants-and-hits/types';

export type THotOffersListProps = {
  cityId: string;
  city?: Record<string, string>;
  hotOffersList: THotOfferProps[];
  hotOffersUiIsIdle: false;
  hotOffersUiIsPending: boolean;
  hotOffersUiIsSuccess: boolean;
  hotOffersUiIsError: boolean;
  hotOffersFetch: THotOffersFetch;
  hotOffersReset: THotOffersReset;
  restaurantCardIdSave: TRestaurantCardIdSave;
  pushNotification: TPushNotification;
  restaurantsAndHits: TRestaurantsAndHits;
};
