// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { hotOffersFetch, hotOffersReset } from '@actions/hotOffers';
import { restaurantCardIdSave } from '@/actions/restaurant/main/id';
import { pushNotification } from '@actions/notifications';
// Selectors
import { homeHotOffersListStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/home'; // Actions

const mapDispatchToProps = {
  hotOffersFetch,
  hotOffersReset,
  restaurantCardIdSave,
  pushNotification,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
