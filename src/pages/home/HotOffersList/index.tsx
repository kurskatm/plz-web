// Modules
import React, { useEffect, FC, memo, useMemo, useState } from 'react';
// Components
import { FlameIcon } from 'chibbis-ui-kit';
import HotOffer from '@components/HotOffer';
import ContentViewer from '@components/ContentViewer';

// Types
import { THotOfferProps } from '@components/HotOffer/types';
import { THotOffersListProps } from './types';
// Enhance
import { enhance } from './enhance';

const CardWrapper: FC = ({ children }) => <div className="home-hot-offers__list">{children}</div>;

const HotOffersList: FC<THotOffersListProps> = ({
  cityId,
  city,
  hotOffersList = [],
  restaurantsAndHits = {},
  hotOffersUiIsPending,
  hotOffersUiIsSuccess,
  hotOffersUiIsError,
  hotOffersFetch,
  hotOffersReset,
  pushNotification,
}) => {
  const [prevCityId, setPrevCityId] = useState<string>(cityId);
  // @ts-ignore
  const { restaurants } = restaurantsAndHits || { restaurants: [] };
  const hotOffersListFiltered = hotOffersList?.filter((item) =>
    // @ts-ignore
    restaurants.find((rest) => item?.restaurant.id === rest.id),
  );

  useEffect(() => {
    if (!hotOffersUiIsPending && !hotOffersUiIsSuccess && prevCityId) {
      hotOffersFetch({ cityId: prevCityId });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [prevCityId]);

  useEffect(() => {
    if (cityId !== prevCityId) {
      hotOffersReset();
      setPrevCityId(cityId);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [cityId, prevCityId]);

  const renderItem = useMemo(
    () => (item: THotOfferProps) => (
      <HotOffer
        key={item.id}
        id={item.id}
        imagePath={item.imagePath}
        name={item.name}
        city={city?.urlName}
        description={item.description}
        restaurantId={item?.restaurant?.id}
        restaurantLogo={item?.restaurant?.logo}
        restaurantName={item?.restaurant?.urlName}
      />
    ),
    [city],
  );

  useEffect(
    () => {
      if (hotOffersUiIsError) {
        pushNotification({
          type: 'error',
          content: 'Не удалось получить горячие предложения',
        });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [hotOffersUiIsError],
  );

  if (hotOffersListFiltered?.length) {
    return (
      <div className="home-hot-offers pad-m">
        <div className="home-hot-offers-header">
          <div className="home-hot-offers-header__title">
            Горячее предложение <FlameIcon width={26} height={26} />
          </div>
        </div>

        <div className="home-hot-offers__content">
          <ContentViewer
            list={hotOffersListFiltered}
            renderItem={renderItem}
            responsive={{
              desktop: 12,
              tablet: 6,
              mobile: 3,
            }}
            wrapper={CardWrapper}
          />
        </div>
      </div>
    );
  }

  return null;
};

const HotOffersListMemo = memo(HotOffersList);

// @ts-ignore
export default enhance(HotOffersListMemo);
