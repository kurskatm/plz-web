import { useCallback } from 'react';
import { useQueryParams, withDefault } from 'use-query-params';
import { CommaArrayParam } from '@pages/home/utils';

export const FiltersQueryParamConfigMap = {
  cuisines: withDefault(CommaArrayParam, undefined, false),
  filters: withDefault(CommaArrayParam, undefined, true),
};

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const useQueryFilters = () => {
  const [filtres, setParams] = useQueryParams(FiltersQueryParamConfigMap);

  const setFilters = useCallback(
    (value: Partial<typeof filtres>) => {
      setParams((oldValue) => ({ ...oldValue, ...value }));
    },
    [setParams],
  );

  return { ...filtres, setFilters };
};
