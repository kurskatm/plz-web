/* eslint-disable @typescript-eslint/no-explicit-any */
// Main Types
import { TFiltersSet, TFiltersShowModal } from '@actions/filters/action-types';
import { TFilter } from '@type/filters';

export interface TFilterProps {
  filters: TFilter[];
  filtersSet: TFiltersSet;
  filtersShowModal: TFiltersShowModal;
  selectedFilters: string[];
  search: string;
}

export interface TRenderContent {
  close: () => void;
}

export type TUrlParams = any;

export type TFormState = any;
