// Modules
import React, { FC, memo, useEffect, useMemo, useRef, useState, useCallback } from 'react';
import { useQueryParams, withDefault } from 'use-query-params';
import { useMediaQuery } from 'react-responsive';
// Modules Components
import { Dropdown } from 'chibbis-ui-kit';
// Utils
import { CommaArrayParam } from '@pages/home/utils';
import { trackEvent } from '@utils/analytics';
// Components
import FiltersModal from '@components/HomeFiltersModal';
import Content from './Content';
// Enhance
import { enhance } from './enhance';
// Types
import { TFilterProps, TRenderContent, TFormState } from './types';

const FiltersComponent: FC<TFilterProps> = ({
  filters: stateFilters,
  filtersSet,
  filtersShowModal,
  selectedFilters,
  search,
}) => {
  const [query] = useQueryParams({
    filters: withDefault(CommaArrayParam, []),
  });
  const { filters } = query;

  const isMobile = useMediaQuery({
    maxWidth: 640,
  });

  const isTabletOrMobile = useMediaQuery({
    maxWidth: 768,
  });

  const triggerRef = useRef<HTMLDivElement>(null);
  const [formState, setFormState] = useState<TFormState>([]);

  useEffect(
    () => {
      const initialFilters: string[] = [];

      stateFilters.forEach((filter) => {
        if (filters.includes(filter.urlName)) {
          initialFilters.push(filter.id);
        }
      });

      if (initialFilters.length) {
        setFormState(initialFilters);
        filtersSet(initialFilters);
        if (isTabletOrMobile) {
          trackEvent('applyFiltersMobile');
        } else {
          trackEvent('applyFiltersDesktop');
        }
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [filters],
  );

  const renderContent = useMemo(
    () => ({ close }: TRenderContent) => (
      <Content
        close={close}
        filters={stateFilters}
        filtersSet={filtersSet}
        formState={formState}
        setFormState={setFormState}
        selectedFilters={selectedFilters}
        search={search}
      />
    ),
    [filtersSet, formState, selectedFilters, stateFilters, search],
  );

  const onMobileTriggerClick = useCallback(() => {
    if (isMobile) {
      filtersShowModal(true);
    }
  }, [filtersShowModal, isMobile]);

  return (
    <div>
      {!!selectedFilters.length && (
        <div className="home-filters-trigger-label">{selectedFilters.length}</div>
      )}
      <div className="home-filters-outer-trigger">
        <div
          ref={triggerRef}
          className="home-filters-trigger"
          role="presentation"
          onClick={onMobileTriggerClick}
        />
      </div>
      {!isMobile ? (
        <Dropdown
          arrow={false}
          getTriggerRef={() => triggerRef.current}
          render={renderContent}
          trigger={['click']}
          width={440}
        />
      ) : (
        <FiltersModal
          bodyContent={
            <Content
              close={() => filtersShowModal(false)}
              filters={stateFilters}
              filtersSet={filtersSet}
              formState={formState}
              setFormState={setFormState}
              selectedFilters={selectedFilters}
              search={search}
            />
          }
        />
      )}
    </div>
  );
};

const FiltersMemo = memo(FiltersComponent);
export default enhance(FiltersMemo);
