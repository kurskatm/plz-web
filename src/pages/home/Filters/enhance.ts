// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { filtersSet, filtersShowModal } from '@actions/filters';
// Selectors
import { homeFiltersStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/home';

const mapDispatchToProps = {
  filtersSet,
  filtersShowModal,
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
