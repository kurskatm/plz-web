/* eslint-disable @typescript-eslint/no-explicit-any */
// Main Types
import { TFiltersSet } from '@actions/filters/action-types';
import { TFilter } from '@type/filters';
// Types
import { TFormState } from '../types';

export interface TFiltersListProps {
  filters: TFilter[];
  filtersSet: TFiltersSet;
  formState: TFormState;
  setFormState: React.Dispatch<TFormState>;
  selectedFilters: string[];
}

export type TUrlParams = any;
