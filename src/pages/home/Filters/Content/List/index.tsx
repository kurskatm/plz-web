// Modules
import React, { FC, memo, useCallback, useEffect, useMemo } from 'react';
import { useHistory } from 'react-router-dom';
import { parse } from 'qs';
import keys from 'lodash/keys';
// Modules Components
import { Checkbox } from 'chibbis-ui-kit';
// Main Types
import { TFilter } from '@type/filters';
// Types
import { TFiltersListProps, TUrlParams } from './types';

const parseParams = {
  ignoreQueryPrefix: true,
};

const FiltersListComponent: FC<TFiltersListProps> = ({ filters, formState, setFormState }) => {
  const history = useHistory();

  useEffect(
    () => {
      const initialFilters: string[] = [];
      const urlParams: TUrlParams = keys(parse(history.location.search, parseParams));

      filters.forEach((filter) => {
        if (urlParams.includes(filter.name)) {
          initialFilters.push(filter.id);
        }
      });
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  const getDefaultChecked = useCallback((id: string) => formState.includes(id), [formState]);

  const onChange = useCallback(
    (id: string) => (value: boolean) => {
      if (value) {
        const newFormState = [...formState, id];
        setFormState(newFormState);
        return true;
      }

      const newFormState = formState.filter((key: string) => key !== id);
      setFormState(newFormState);
      return false;
    },
    [formState, setFormState],
  );

  const renderItem = useMemo(
    () => ({ id, name }: TFilter) => (
      <div key={id} className="home-filters-item">
        <Checkbox
          key={getDefaultChecked(id)}
          value={getDefaultChecked(id)}
          onChange={onChange(id)}
          name={id}
          view={name}
        />
      </div>
    ),
    [getDefaultChecked, onChange],
  );

  return <div className="home-filters-list">{filters.map(renderItem)}</div>;
};

export default memo(FiltersListComponent);
