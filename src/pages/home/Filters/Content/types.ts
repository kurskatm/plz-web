/* eslint-disable @typescript-eslint/no-explicit-any */
// Main Types
import { TFiltersSet } from '@actions/filters/action-types';
import { TFilter } from '@type/filters';

export interface TContentProps {
  close: () => void;
  filters: TFilter[];
  filtersSet: TFiltersSet;
  formState: TFormState;
  setFormState: React.Dispatch<TFormState>;
  selectedFilters: string[];
  search: string;
}

export type TFormState = any;
