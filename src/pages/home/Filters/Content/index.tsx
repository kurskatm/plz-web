// Modules
import React, { FC, memo, useCallback, useEffect } from 'react';
import isEqual from 'lodash/isEqual';
import { useMediaQuery } from 'react-responsive';
// Modules Components
import { Button } from 'chibbis-ui-kit';
// Utils
import { trackEvent } from '@utils/analytics';
// Components
import FiltersList from './List';
// Types
import { TContentProps } from './types';
import { useQueryFilters } from '../useFiltersQuery';

const ContentComponent: FC<TContentProps> = ({
  close,
  filters: stateFilters,
  filtersSet,
  formState,
  setFormState,
  selectedFilters,
  search,
}) => {
  const isTabletOrMobile = useMediaQuery({
    maxWidth: 768,
  });

  const { setFilters } = useQueryFilters();
  useEffect(
    () => () => {
      filtersSet([]);
    },
    [filtersSet],
  );
  const onReset = useCallback(
    () => {
      setFormState([]);
      filtersSet([]);
      setFilters({ filters: undefined });

      close();
      if (isTabletOrMobile) {
        trackEvent('clearFiltersMobile');
      } else {
        trackEvent('clearFiltersDesktop');
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [close, filtersSet, setFormState, setFilters],
  );

  const onSubmit = useCallback(() => {
    const selectedFiltersParams = stateFilters
      .filter(({ id }) => formState.includes(id))
      .map(({ urlName }) => urlName);

    setFilters({ filters: selectedFiltersParams?.length ? selectedFiltersParams : undefined });
    filtersSet(formState);
    close();
  }, [close, filtersSet, formState, stateFilters, setFilters]);

  useEffect(() => {
    if (selectedFilters.length && search.length) {
      onReset();
    }
  }, [onReset, search.length, selectedFilters.length]);

  const isDisabledButton = useCallback(() => {
    const selectedFiltersArray = selectedFilters;
    const formStateArray: string[] = formState;

    return isEqual(selectedFiltersArray, formStateArray);
  }, [formState, selectedFilters]);

  return (
    <div className="home-filters-form">
      <div>
        <FiltersList
          filters={stateFilters}
          filtersSet={filtersSet}
          formState={formState}
          setFormState={setFormState}
          selectedFilters={selectedFilters}
        />
      </div>
      <div className="home-filters-form-panel">
        <Button
          disabled={!formState.length && isDisabledButton()}
          htmlType="button"
          onClick={onReset}
          size="small"
          type="secondary"
          className="home-filters-form-panel__button"
          radius="medium">
          Сбросить
        </Button>

        <Button
          key={`filter-reset-${isDisabledButton()}`}
          disabled={isDisabledButton()}
          htmlType="button"
          onClick={onSubmit}
          size="small"
          className="home-filters-form-panel__button"
          radius="medium">
          Применить
        </Button>
      </div>
    </div>
  );
};

export default memo(ContentComponent);
