// Modules
import React, { FC, memo } from 'react';
import loadable from '@loadable/component';
import { useParams } from 'react-router-dom';
// Components
import GoToOldSite from '@components/MoveToOldDesignBanner/Banner';
// Utils
import { Consts } from '@utils';
// Components
import RestaurantsList from './RestaurantsList';
import HitsList from './HitsList';

import SelectionBanner from './SelectionBanner';

// пока не утвержден механизм подборок - хардкодим
const TYPES: Record<string, Record<string, string>> = {
  bonuses: {
    title: 'Рестораны с едой за баллы',
    text: 'Добавляйте 1 блюдо бесплатно к заказу',
  },
  hits: {
    title: 'Хиты продаж',
    text: 'Блюда, которые чаще всего заказывают и пользуются популярностью в @city',
  },
};

const { ROUTES } = Consts;
const Header = loadable(() => import('@components/Header'));
const Footer = loadable(() => import('@components/Footer'));

const SelectionForScoresComponent: FC = () => {
  // @ts-ignore
  const { specId } = useParams();

  return (
    <>
      <div className="home-page selection grid-wrapper">
        <header className="grid-wrapper__header">
          <Header goBackLink={ROUTES.CITIES.PATH} goBackText="На главную" />
        </header>
        <main className="grid-wrapper__main">
          <div className="home-page__body-wrap">
            <SelectionBanner
              type={specId}
              title={TYPES[specId]?.title}
              text={TYPES[specId]?.text}
            />
            {specId !== 'hits' ? <RestaurantsList viewFullList /> : <HitsList />}
          </div>
        </main>
        <footer className="grid-wrapper__footer">
          <Footer />
        </footer>
      </div>
      <GoToOldSite />
    </>
  );
};
export default memo(SelectionForScoresComponent);
