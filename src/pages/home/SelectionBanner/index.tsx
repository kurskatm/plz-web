// Modules
import classnames from 'classnames';
import React, { FC, memo, useCallback } from 'react';
// Types
import { TSelectionBannerProps } from './types';
// Enhance
import { enhance } from './enhance';

const SelectionBanner: FC<TSelectionBannerProps> = ({ text = '', title, type, city }) => {
  const getBannerStyles = useCallback(
    () => classnames('selection-banner', `selection-banner_type_${type}`),
    [type],
  );
  const getBannerImgStyles = useCallback(
    () => classnames('selection-banner__img', `selection-banner__img_${type}`),
    [type],
  );

  const getText = useCallback(() => text.replace('@city', city?.whereName), [text, city]);

  return (
    <div className={getBannerStyles()} role="presentation">
      <div className="selection-banner__wrapper pad-m">
        <div className="selection-banner__info">
          <div className="selection-banner__title">{title}</div>
          <div className="selection-banner__text">{getText()}</div>
        </div>
        <div className={getBannerImgStyles()} />
      </div>
    </div>
  );
};

const SelectionBannerMemo = memo(SelectionBanner);

export default enhance(SelectionBannerMemo);
