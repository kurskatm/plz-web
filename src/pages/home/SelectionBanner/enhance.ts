// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Selectors
import { citiesModalShowStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/cities-list';

const withConnect = connect(mapStateToProps);

export const enhance = compose(withConnect);
