import { TCitiesItem } from '@/type/cities';

export interface TSelectionBannerProps {
  type: string;
  title: string;
  text: string;
  city: TCitiesItem;
}
