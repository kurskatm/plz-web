// Modules
import React, { FC, memo, useMemo, useEffect, useState } from 'react';
import { useMediaQuery } from 'react-responsive';
import { useCardResize } from '@utils/useCardResize';
// Main Components
import { ReviewsIcon } from 'chibbis-ui-kit';
import ContentViewer from '@components/ContentViewer';
import Review from '@components/ReviewMain';
// Main Types
import { TReviewMain } from '@type/reviews-main';
import { TCitiesItem } from '@type/cities';
// Enhance
import { enhance } from './enhance';
// Types
import { TReviewsListProps } from './types';

const CardWrapper: FC = ({ children }) => <div className="home-reviews__list">{children}</div>;

const ReviewsList: FC<TReviewsListProps> = ({
  reviews,
  city,
  restaurantsAndHits,
  restaurantsAndHitsIsSuccess,
  restaurantsAndHitsIsError,
  reviewsFetch,
  reviewsReset,
  reviewsIsPending,
  reviewsIsSuccess,
  reviewsIsError,
  pushNotification,
}) => {
  const isTheeCards = useMediaQuery({
    minWidth: 1025,
  });
  const isTwoCards = useMediaQuery({
    minWidth: 769,
    maxWidth: 1024,
  });
  const isOneCard = useMediaQuery({
    maxWidth: 768,
  });

  const { setCardHeight, getCardHeight } = useCardResize({
    cardsLength: reviews.length,
    isOneCard,
    isTwoCards,
    isTheeCards,
  });

  const [prevCity, setPrevCity] = useState<TCitiesItem>(city);

  useEffect(() => {
    if (reviewsIsError) {
      pushNotification({
        type: 'error',
        content: 'Не удалось загрузить отзывы',
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [reviewsIsError]);

  useEffect(() => {
    if (restaurantsAndHitsIsError) {
      pushNotification({
        type: 'error',
        content: 'Не удалось загрузить рестораны и хиты',
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [restaurantsAndHitsIsError]);

  useEffect(() => {
    if (
      restaurantsAndHitsIsSuccess &&
      restaurantsAndHits.restaurants.length &&
      !reviewsIsPending &&
      !reviewsIsSuccess
    ) {
      const restsIds: string[] = [];
      restaurantsAndHits.restaurants.map((item, index) => {
        if (index < 20) {
          return restsIds.push(item.id);
        }
        return item;
      });
      reviewsFetch({
        restaurantsIds: restsIds,
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [restaurantsAndHitsIsSuccess, restaurantsAndHits]);

  useEffect(() => {
    if (prevCity !== city) {
      reviewsReset();
      setPrevCity(city);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [city, prevCity]);

  const renderItem = useMemo(
    () => (item: TReviewMain, index: number) => (
      <Review
        key={item.id}
        item={item}
        url={`/${city?.urlName}/restaurant`}
        index={index}
        setItemHeight={setCardHeight}
        bodyHeight={getCardHeight(index)}
      />
    ),
    [city, setCardHeight, getCardHeight],
  );

  if (reviews.length) {
    return (
      <div className="home-reviews-wrapper">
        <div className="home-reviews pad-m">
          <div className="home-reviews__title">
            Отзывы <ReviewsIcon width={26} height={26} />
          </div>

          <div className="home-reviews__content">
            <ContentViewer
              list={reviews}
              renderItem={renderItem}
              responsive={{
                desktop: 6,
                tablet: 6,
                mobile: 6,
              }}
              wrapper={CardWrapper}
            />
          </div>
        </div>
      </div>
    );
  }
  return null;
};

const ReviewsListMemo = memo(ReviewsList);

export default enhance(ReviewsListMemo);
