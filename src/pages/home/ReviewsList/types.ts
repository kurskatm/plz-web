// Main Types
import { TReviewsMainList } from '@type/reviews-main';
import { TCitiesItem } from '@type/cities';
import { TRestaurantsAndHits } from '@models/restaurants-and-hits/types';
import { TReviewsFetch, TReviewsReset } from '@actions/reviews/action-types';
import { TPushNotification } from '@/actions/notifications/action-types';

export interface TReviewsListProps {
  city: TCitiesItem;
  reviews: TReviewsMainList;
  reviewsIsIdle: boolean;
  reviewsIsPending: boolean;
  reviewsIsSuccess: boolean;
  reviewsIsError: boolean;
  restaurantsAndHits: TRestaurantsAndHits;
  restaurantsAndHitsIsSuccess: boolean;
  restaurantsAndHitsIsError: boolean;
  reviewsFetch: TReviewsFetch;
  reviewsReset: TReviewsReset;
  pushNotification: TPushNotification;
}
