// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { reviewsFetch, reviewsReset } from '@actions/reviews';
import { pushNotification } from '@actions/notifications';
// Selectors
import { homeReviewsStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/reviews';

const mapDispatchToProps = {
  reviewsFetch,
  reviewsReset,
  pushNotification,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
