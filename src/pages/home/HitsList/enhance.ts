// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { authModalShow } from '@actions/auth';
import { restaurantsAndHitsFetch } from '@actions/restaurants-and-hits';
import {
  profileDeleteFavoriteProduct,
  profileSetFavoriteProduct,
} from '@actions/profile/favourite';
import { shopingCartReset } from '@/actions/shoping-cart';
import { shopingCartSave } from '@actions/shoping-cart/save';
import { shopingCartUpdate } from '@actions/shoping-cart/change';

import { showRestaurantModals, showModifiersCardModal } from '@actions/restaurant/modal';
// Selectors
import { homeHitsListStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/home';

const mapDispatchToProps = {
  fetchRestaurantsAndHits: restaurantsAndHitsFetch,
  shopingCartSave,
  shopingCartUpdate,
  shopingCartReset,
  authModalShow,
  profileDeleteFavoriteProduct,
  profileSetFavoriteProduct,
  showRestaurantModals,
  showModifiersCardModal,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
