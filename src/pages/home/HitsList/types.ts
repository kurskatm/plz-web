// Main Types
import { TUpdateShopingCart } from '@actions/shoping-cart/change/action-types';
import { TRestaurantsAndHitsFetch } from '@actions/restaurants-and-hits/action-types';
import { TShopingCartSave } from '@actions/shoping-cart/save/action-types';
import { TShopingCartReset } from '@/actions/shoping-cart/action-types';
import { TRestaurantsAndHits } from '@models/restaurants-and-hits/types';
import {
  TShopingCartModel,
  TRestaurantShopingCart,
  TShopingCartProduct,
} from '@models/shoping-cart/types';
import { TProfileModel } from '@models/profile/types';
import { TCitiesItem } from '@type/cities';
import {
  TProfileDeleteFavoriteProducts,
  TProfileSetFavoriteProducts,
} from '@actions/profile/favourite/action-types';
import {
  TShowRestaurantModals,
  TShowModifiersCardModal,
} from '@actions/restaurant/modal/action-types';
import { TCuisinesList } from '@models/cuisines/types';
import { TFilter } from '@type/filters';

export type THitsListProps = {
  fetchRestaurantsAndHits: TRestaurantsAndHitsFetch;
  profile: TProfileModel;
  restaurantsAndHits: TRestaurantsAndHits;
  restaurantsAndHitsIsIdle: boolean;
  restaurantsAndHitsIsPending: boolean;
  restaurantsAndHitsIsSuccess: boolean;
  selectedCuisines: string[];
  selectedFilters: string[];
  search: string;
  shopingCart: TShopingCartModel;
  shopingCartUpdate: TUpdateShopingCart;
  authModalShow: (data: boolean) => void;
  profileDeleteFavoriteProduct: TProfileDeleteFavoriteProducts;
  profileSetFavoriteProduct: TProfileSetFavoriteProducts;
  shopingCartSave: TShopingCartSave;
  shopingCartReset: TShopingCartReset;
  shopingCartWithNoApiStatuses: {
    restaurant: TRestaurantShopingCart;
    products: TShopingCartProduct[];
    cutlery: number;
  };
  showRestaurantModals: TShowRestaurantModals;
  showModifiersCardModal: TShowModifiersCardModal;
  cuisinesList: TCuisinesList;
  filtersList: TFilter[];
  city?: TCitiesItem;
};
