// Modules
import React, { useEffect, FC, memo, useCallback, useMemo, useRef } from 'react';
import { isNil } from 'lodash';
import { useQueryParams, withDefault } from 'use-query-params';
import { useMediaQuery } from 'react-responsive';
import { useCardResize } from '@utils/useCardResize';
import { useShopingCartUpdate } from '@utils';
// Utils
import { CommaArrayParam } from '@pages/home/utils';
// Main Components
import ContentViewer from '@components/ContentViewer';
import HitCard from '@components/HitCard';
import ModifiersCardModal from '@components/RestaurantModals/ModifiersCardModal';
import CheckoutAddressModal from '@/components/CheckoutAddressModal';
import { LightingIcon } from 'chibbis-ui-kit';
// Main types
import { TFavoriteProduct } from '@/type/new-favorites';
// Lib
import { manageShopingCartSavingOnUpdate } from '@lib/shoping-cart/save-cart-on-update';
// Main Types
import { TRestaurantProduct } from '@type/restaurants/restaurant-products';
// Utils
import { usePrevious } from '@utils/usePrevious';
// Types
import { THitsListProps } from './types';
// Enhance
import { enhance } from './enhance';

const CardWrapper: FC = ({ children }) => <div className="home-hits__list">{children}</div>;

const HitsList: FC<THitsListProps> = ({
  city,
  profile,
  fetchRestaurantsAndHits,
  restaurantsAndHits,
  authModalShow,
  restaurantsAndHitsIsPending,
  restaurantsAndHitsIsSuccess,
  search,
  shopingCart,
  shopingCartUpdate,
  shopingCartReset,
  shopingCartSave,
  shopingCartWithNoApiStatuses,
  profileDeleteFavoriteProduct,
  profileSetFavoriteProduct,
  showRestaurantModals,
  cuisinesList,
  filtersList,
}) => {
  const [query] = useQueryParams({
    cuisines: withDefault(CommaArrayParam, [], false),
    filters: withDefault(CommaArrayParam, [], true),
  });
  const fetchedRestsAndHits = useRef(false);

  const isFourCards = useMediaQuery({
    minWidth: 1600,
  });
  const isTheeCards = useMediaQuery({
    minWidth: 1024,
    maxWidth: 1599,
  });
  const isTwoCards = useMediaQuery({
    minWidth: 640,
    maxWidth: 1023,
  });
  const isOneCard = useMediaQuery({
    maxWidth: 639,
  });

  const { setCardHeight, getCardHeight } = useCardResize({
    cardsLength: restaurantsAndHits?.products?.length,
    isOneCard,
    isTwoCards,
    isTheeCards,
    isFourCards,
  });

  // Fetch first time
  useEffect(() => {
    if (restaurantsAndHitsIsSuccess) {
      fetchedRestsAndHits.current = true;
    }
    if (
      cuisinesList?.length &&
      filtersList?.length &&
      !fetchedRestsAndHits.current &&
      !restaurantsAndHitsIsPending &&
      !restaurantsAndHitsIsSuccess &&
      (profile.newGeoCode.data?.precision === 'exact' || isNil(profile.newGeoCode.data))
    ) {
      const cuisinesIds = cuisinesList
        .filter((e) => query.cuisines.includes(e.urlName))
        .map((e) => e.id);
      const filtersIds = filtersList
        .filter((e) => query.filters.includes(e.urlName))
        .map((e) => e.id);

      const filters = search.trim().length ? [] : [...cuisinesIds, ...filtersIds];
      const data = {
        cityId: city?.id,
        lat: profile.newGeoCode.data?.lat,
        lng: profile.newGeoCode.data?.lng,
        filters,
        search,
        onSuccess: () => {
          fetchedRestsAndHits.current = true;
        },
      };

      fetchRestaurantsAndHits(data);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const prevProductsLength = usePrevious(shopingCart.products?.length);

  useEffect(
    () => {
      manageShopingCartSavingOnUpdate({
        shopingCart,
        prevProductsLength,
        profile,
        shopingCartReset,
        shopingCartSave,
      });
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [JSON.stringify(shopingCartWithNoApiStatuses)],
  );

  const likeClickHandler = useCallback(
    (productId: string, product: TFavoriteProduct) => (
      e: Event,
      liked: boolean,
      cb: () => void,
    ) => {
      e.preventDefault();

      if (!profile.auth) {
        authModalShow(true);
      } else {
        cb();

        if (liked) {
          profileDeleteFavoriteProduct({ productId });
        } else {
          profileSetFavoriteProduct({
            productId,
            newProduct: product,
          });
        }
      }
    },
    [profile.auth, authModalShow, profileDeleteFavoriteProduct, profileSetFavoriteProduct],
  );

  const getQuantity = useCallback(
    (item: TRestaurantProduct) => {
      const shopingCartItem = shopingCart.products.find(
        (product) => product.id === item.id && product.inBonusPoint === item.availableInBonusPoints,
      );

      return shopingCartItem?.count;
    },
    [shopingCart.products],
  );

  const [updateShopingCart] = useShopingCartUpdate({
    city,
    shopingCart,
    showRestaurantModals,
    shopingCartUpdate,
    profile,
  });

  const renderItem = useMemo(
    () => (item: TRestaurantProduct, index: number) => (
      <HitCard
        key={item.id}
        // @ts-ignore Property 'restaurant' is missing in type 'TRestaurantProduct'
        onLikeClick={likeClickHandler(item.id, item)}
        item={item}
        quantity={getQuantity(item)}
        updateShopingCart={updateShopingCart(item)}
        index={index}
        setItemHeight={setCardHeight}
        bodyHeight={getCardHeight(index)}
      />
    ),
    [likeClickHandler, getQuantity, updateShopingCart, setCardHeight, getCardHeight],
  );

  if (restaurantsAndHitsIsSuccess && restaurantsAndHits?.products?.length) {
    return (
      <div id="hits" className="home-hits pad-m">
        <div className="home-hits__title">
          Хиты продаж <LightingIcon width={26} height={26} />
        </div>

        <div className="home-hits__content">
          <ContentViewer
            list={restaurantsAndHits.products}
            renderItem={renderItem}
            responsive={{
              desktop: 12,
              tablet: 6,
              mobile: 4,
            }}
            wrapper={CardWrapper}
          />
        </div>
        <ModifiersCardModal />
        <CheckoutAddressModal />
      </div>
    );
  }
  return null;
};

const HitsListMemo = memo(HitsList);

export default enhance(HitsListMemo);
