// Modules
import loadable from '@loadable/component';
import React, { FC, memo, useCallback } from 'react';
import { useMediaQuery } from 'react-responsive';
import classNames from 'classnames';
// Enhance
import { enhance } from './enhance';
// Types
import { TFiltersBlockProps } from './types';

const Search = loadable(() => import('@components/Search'));

const FiltersBlockComponent: FC<TFiltersBlockProps> = ({ filtersSearch, search }) => {
  const isTabletOrMobile = useMediaQuery({
    query: '(max-width: 768px)',
  });

  const getStyles = useCallback(
    () =>
      classNames(
        'home-page__filters-block',
        `home-page__filters-block__${isTabletOrMobile ? 'mobile' : 'desktop'}`,
        'pad-m',
      ),
    [isTabletOrMobile],
  );

  return (
    <div className={getStyles()}>
      <div>
        <Search
          action={filtersSearch}
          initialValue={search}
          placeholder="Найти ресторан или блюдо"
          needToClearValueWithUnmount={false}
        />
      </div>
    </div>
  );
};

const FiltersBlocKMemo = memo(FiltersBlockComponent);
export default enhance(FiltersBlocKMemo);
