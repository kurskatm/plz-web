// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { filtersSearch } from '@actions/filters';
// Selectors
import { homeSearchStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/home';

const mapDispatchToProps = {
  filtersSearch,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);
export const enhance = compose(withConnect);
