// Main Types
import { TFiltersSearch } from '@actions/filters/action-types';

export interface TFiltersBlockProps {
  filtersSearch: TFiltersSearch;
  search: string;
}
