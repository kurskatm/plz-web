// Modules
import urlJoin from 'url-join';
// Lib
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
// Types
import { TGetCuisines } from './types';

const { ENV } = Consts;

const endpoint = '/catalog/cuisines';

export const getCuisines: TGetCuisines = () => {
  const url = urlJoin(ENV.API, endpoint);

  return new RequestManager({ url }).getRequest();
};
