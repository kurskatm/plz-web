// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export type TGetCuisines = () => Observable<AjaxResponse>;
