// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export type TBecomePartner = (data: Record<string, unknown>) => Observable<AjaxResponse>;
