// Modules
import urlJoin from 'url-join';
import { omit } from 'lodash';

// Lib
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
// Types
import { TBecomePartner } from './types';

const { ENV } = Consts;

const endpointPartners = '/partners/become';
const getPreparedData = (data: Record<string, string>) => {
  const [head, middle, tale] = data.phoneNumber.replace(/\s/, '').split('-');

  return {
    ...omit(data, ['token', 'agreement']),
    phoneNumber: `${head}-${middle}${tale}`,
  };
};

export const becomePartner: TBecomePartner = (data: Record<string, string>) => {
  const url = urlJoin(ENV.API, endpointPartners);
  const preparedData = getPreparedData(data);
  const reqOptions = {
    url,
    method: 'POST',
    body: preparedData,
    headers: {
      GRecaptchaToken: data.token,
    },
  };

  return new RequestManager(reqOptions).getRequest();
};
