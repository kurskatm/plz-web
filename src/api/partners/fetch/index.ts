// Modules
import urlJoin from 'url-join';
import { isNumber } from 'lodash';

// Lib
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
// Types
import { TGetContacts, TGetCounters } from './types';

const { ENV } = Consts;

const endpointCounters = 'partners/become/counters';
const getEndpointContacts = (cityId: string) => urlJoin('/cities', cityId, 'contacts');

export const getContacts: TGetContacts = ({ cityId } = {}) => {
  const cityIdToString = isNumber(cityId) ? cityId.toString() : '';
  const url = urlJoin(ENV.API, getEndpointContacts(cityIdToString));

  return new RequestManager({ url }).getRequest();
};
export const getCounters: TGetCounters = () => {
  const url = urlJoin(ENV.API, endpointCounters);

  return new RequestManager({ url }).getRequest();
};
