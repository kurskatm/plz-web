// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

interface TGetContactsArgs {
  cityId?: number;
}

export type TBecomePartner = (data: Record<string, unknown>) => Observable<AjaxResponse>;
export type TGetCounters = () => Observable<AjaxResponse>;
export type TGetContacts = (data: TGetContactsArgs) => Observable<AjaxResponse>;
