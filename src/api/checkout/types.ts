// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';
// Main Types
import { TShopingCartSaveData } from '@type/shoping-cart';
import { TDeliveryCheckoutData } from '@/pages/checkout/types';

export interface TSendCheckoutArgs {
  cartId: string;
  userId?: string;
  data?: TShopingCartSaveData;
}

export type TSendCheckout = (data: TDeliveryCheckoutData) => Observable<AjaxResponse>;
