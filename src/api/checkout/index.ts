// Modules
import urlJoin from 'url-join';
// Lib
import { RequestManager } from '@lib/request';
import { getCartId, getProfileData, getAccessToken } from '@/lib/cookie';
// Utils
import { Consts } from '@utils';
import { TDeliveryCheckoutData } from '@/pages/checkout/types';
import { TSendCheckout } from './types';

const { ENV } = Consts;
const endpoint = '/checkout/checkout';

export const sendCheckout: TSendCheckout = (data: TDeliveryCheckoutData) => {
  const method = 'POST';
  const cartIdFromCookie = getCartId();
  const userIdFromCookie = getProfileData();
  const accessToken = getAccessToken() || undefined;

  const params = {
    cartId: cartIdFromCookie,
    userId: userIdFromCookie?.userId,
  };
  // const strParams = stringify(options);
  const url = urlJoin(ENV.API, endpoint);

  return new RequestManager({
    method,
    url,
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
    body: {
      ...data,
      ...params,
    },
  }).getRequest();
};
