// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export interface TGetRestaurantsAndHitsArgs {
  cityId: string;
  lat?: number;
  lng?: number;
  filters?: string[];
  search?: string;
  showFavorites?: boolean;
  userId?: string;
}

export type TGetRestaurantsAndHits = (data: TGetRestaurantsAndHitsArgs) => Observable<AjaxResponse>;

export interface TParams {
  CityId: string;
  Filters?: string[];
  lat?: number;
  lng?: number;
  SearchString?: string;
  showFavorites?: boolean;
  userId?: string;
}
