// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Lib
import { RequestManager } from '@lib/request';
import { getAccessToken } from '@lib/cookie';
// Utils
import { Consts } from '@utils';
// Types
import { TGetRestaurantsAndHits, TParams } from './types';

const { ENV } = Consts;
const endpoint = '/catalog/search';
const endpoint2 = (cityId: string) => `/catalog/main/${cityId}`;
const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const getRestaurantsAndHits: TGetRestaurantsAndHits = ({
  cityId,
  filters,
  lat,
  lng,
  search,
  showFavorites,
  userId,
}) => {
  const params: TParams = {
    CityId: cityId,
    Filters: filters,
    lat,
    lng,
    showFavorites,
    userId,
  };

  if (search) {
    params.SearchString = search;
  }

  const accessToken = getAccessToken() || undefined;
  const strParams = stringify(params, options);
  const url = urlJoin(ENV.API, search ? endpoint : endpoint2(cityId), strParams);
  const reqOptions = {
    url,
    ...(accessToken
      ? {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        }
      : {}),
  };

  return new RequestManager(reqOptions).getRequest();
};
