// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Lib
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
// Types
import { TFetchRestaurantLegal } from './types';

const { ENV } = Consts;

const endpoint = '/partners/legal-details';

const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const fetchRestaurantLegal: TFetchRestaurantLegal = ({ restaurantId, storeId }) => {
  const data = { restaurantId, storeId };
  const params = stringify(data, options);
  const url = urlJoin(ENV.API, endpoint, params);

  return new RequestManager({ url }).getRequest();
};
