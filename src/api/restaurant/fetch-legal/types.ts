// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

interface TFetchRestaurantLegalArgs {
  restaurantId: string;
  storeId: string;
}
export type TFetchRestaurantLegal = (data: TFetchRestaurantLegalArgs) => Observable<AjaxResponse>;
