// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Lib
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
// Types
import { TFetchRestaurantPromo } from './types';

const { ENV } = Consts;

const endpoint = '/promo/restaurant-promos';

const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const fetchRestaurantPromo: TFetchRestaurantPromo = ({ restaurantId, storeId }) => {
  const data = { restaurantId, storeId };
  const params = stringify(data, options);
  const url = urlJoin(ENV.API, endpoint, params);

  return new RequestManager({ url }).getRequest();
};
