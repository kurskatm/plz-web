// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

interface TFetchRestaurantPromoArgs {
  restaurantId: string;
  storeId: string;
}
export type TFetchRestaurantPromo = (data: TFetchRestaurantPromoArgs) => Observable<AjaxResponse>;
