// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
import { isNil } from 'lodash';
// Lib
import { RequestManager } from '@lib/request';
import { getAccessToken } from '@lib/cookie';
// Utils
import { Consts } from '@utils';
// Types
import { TFetchRestaurantCard } from './types';

const { ENV } = Consts;

const getEndpoint = (restaurantId: string) =>
  urlJoin('/catalog/restaurants', restaurantId, '/card');

const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const fetchRestaurantCard: TFetchRestaurantCard = ({ restaurantId, lat, lng }) => {
  const params = {};

  if (!isNil(lat)) {
    // @ts-ignore
    params.lat = lat;
  }

  if (!isNil(lng)) {
    // @ts-ignore
    params.lng = lng;
  }

  const accessToken = getAccessToken();
  const endpoint = getEndpoint(restaurantId);
  const queryParams = stringify(params, options);
  const url = urlJoin(ENV.API, endpoint, queryParams);

  const reqOptions = {
    url,
    method: 'GET',
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  };
  return new RequestManager(reqOptions).getRequest();
};
