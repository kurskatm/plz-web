// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

interface TFetchRestaurantCardArgs {
  restaurantId: string;
  lat?: number;
  lng?: number;
}
export type TFetchRestaurantCard = (data: TFetchRestaurantCardArgs) => Observable<AjaxResponse>;
