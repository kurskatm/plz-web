// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

interface TRestaurantSearchProductsArgs {
  restaurantId: string;
  storeId: string;
  query: string;
}
export type TRestaurantSearchProducts = (
  data: TRestaurantSearchProductsArgs,
) => Observable<AjaxResponse>;
