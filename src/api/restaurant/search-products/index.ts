// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Lib
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
// Types
import { TRestaurantSearchProducts } from './types';

const { ENV } = Consts;

const getEndpoint = (restaurantId: string) =>
  urlJoin('/catalog/restaurant', restaurantId, '/products/search');

const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const restaurantSearchProducts: TRestaurantSearchProducts = ({
  restaurantId,
  storeId,
  query,
}) => {
  const endpoint = getEndpoint(restaurantId);
  const data = { storeId, query };
  const params = stringify(data, options);
  const url = urlJoin(ENV.API, endpoint, params);

  return new RequestManager({ url }).getRequest();
};
