// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export type TFetchRestaurantStores = (data: string) => Observable<AjaxResponse>;
