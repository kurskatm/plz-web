// Modules
import urlJoin from 'url-join';
// Lib
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
// Types
import { TFetchRestaurantStores } from './types';

const { ENV } = Consts;

const getEndpoint = (restaurantId: string) =>
  urlJoin('/catalog/restaurants', restaurantId, '/self-pickup-stores');

export const fetchRestaurantStores: TFetchRestaurantStores = (restaurantId) => {
  const endpoint = getEndpoint(restaurantId);
  const url = urlJoin(ENV.API, endpoint);

  const reqOptions = {
    url,
    method: 'GET',
  };
  return new RequestManager(reqOptions).getRequest();
};
