// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Lib
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
// Types
import { TFetchRestaurantCategories } from './types';

const { ENV } = Consts;

const getEndpoint = (restaurantId: string) =>
  urlJoin('/catalog/restaurant', restaurantId, '/categories');

const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const fetchRestaurantCategories: TFetchRestaurantCategories = ({
  restaurantId,
  storeId,
}) => {
  const endpoint = getEndpoint(restaurantId);
  const data = { storeId, showHidden: false };
  const params = stringify(data, options);
  const url = urlJoin(ENV.API, endpoint, params);

  return new RequestManager({ url }).getRequest();
};
