// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

interface TFetchRestaurantCategoriesArgs {
  restaurantId: string;
  storeId: string;
}
export type TFetchRestaurantCategories = (
  data: TFetchRestaurantCategoriesArgs,
) => Observable<AjaxResponse>;
