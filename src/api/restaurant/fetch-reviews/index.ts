// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Lib
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
// Types
import { TFetchRestaurantReviews } from './types';

const { ENV } = Consts;

const getEndpoint = (restaurantId: string) => urlJoin('/reviews/reviews/restaurant/', restaurantId);

const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const fetchRestaurantReviews: TFetchRestaurantReviews = ({
  restaurantId,
  liked,
  pageNumber,
  takeCount,
}) => {
  const endpoint = getEndpoint(restaurantId);
  const data = {
    restaurantId,
    liked,
    pageNumber,
    takeCount,
  };
  const params = stringify(data, options);
  const url = urlJoin(ENV.API, endpoint, params);

  return new RequestManager({ url }).getRequest();
};
