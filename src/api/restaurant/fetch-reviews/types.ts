// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

interface TFetchRestaurantReviewsArgs {
  restaurantId: string;
  liked?: boolean;
  pageNumber: number;
  takeCount: number;
}
export type TFetchRestaurantReviews = (
  data: TFetchRestaurantReviewsArgs,
) => Observable<AjaxResponse>;
