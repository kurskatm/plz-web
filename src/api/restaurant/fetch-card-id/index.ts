// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Lib
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
// Types
import { TFetchRestaurantCardId } from './types';

const { ENV } = Consts;

const getEndpoint = (cityId: string) => urlJoin('/catalog/restaurants/by-url-name', cityId);

const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const fetchRestaurantCardId: TFetchRestaurantCardId = ({ cityId, restaurantName }) => {
  const endpoint = getEndpoint(cityId);
  const data = { urlName: restaurantName };
  const params = stringify(data, options);
  const url = urlJoin(ENV.API, endpoint, params);

  return new RequestManager({ url }).getRequest();
};
