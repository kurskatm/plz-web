// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

interface TFetchRestaurantCardIdArgs {
  cityId: string;
  restaurantName: string;
}
export type TFetchRestaurantCardId = (data: TFetchRestaurantCardIdArgs) => Observable<AjaxResponse>;
