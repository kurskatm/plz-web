// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Lib
import { RequestManager } from '@lib/request';
import { getAccessToken } from '@lib/cookie';
// Utils
import { Consts } from '@utils';
// Types
import { TFetchRestaurantProducts } from './types';

const { ENV } = Consts;

const getEndpoint = (restaurantId: string) =>
  urlJoin('/catalog/restaurant', restaurantId, '/categories/products');

const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const fetchRestaurantProducts: TFetchRestaurantProducts = ({
  restaurantId,
  storeId,
  showHidden = false,
}) => {
  const endpoint = getEndpoint(restaurantId);
  const data = { restaurantId, storeId, showHidden };
  const params = stringify(data, options);
  const url = urlJoin(ENV.API, endpoint, params);
  const accessToken = getAccessToken() || undefined;

  if (accessToken) {
    const reqOptions = {
      url,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    };

    return new RequestManager(reqOptions).getRequest();
  }

  return new RequestManager({ url }).getRequest();
};
