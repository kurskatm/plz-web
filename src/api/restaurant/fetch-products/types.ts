// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

interface TFetchRestaurantProductsArgs {
  restaurantId: string;
  storeId: string;
  showHidden?: boolean;
}
export type TFetchRestaurantProducts = (
  data: TFetchRestaurantProductsArgs,
) => Observable<AjaxResponse>;
