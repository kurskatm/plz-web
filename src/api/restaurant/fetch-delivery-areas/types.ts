// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

interface TFetchRestaurantDeliveryAreasArgs {
  cityId: string;
  lat: number;
  lon: number;
  restId: string;
  clientTime: string;
}
export type TFetchRestaurantDeliveryAreas = (
  data: TFetchRestaurantDeliveryAreasArgs,
) => Observable<AjaxResponse>;

export interface TParams {
  cityId: string;
  latitude: number;
  longitude: number;
  restId: string;
  clientTime: string;
}
