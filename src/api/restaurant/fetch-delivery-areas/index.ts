// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Lib
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
// Types
import { TFetchRestaurantDeliveryAreas, TParams } from './types';

const { ENV } = Consts;

const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const fetchRestaurantDeliveryAreas: TFetchRestaurantDeliveryAreas = ({
  cityId,
  clientTime,
  lat,
  lon,
  restId,
}) => {
  const params: TParams = {
    cityId,
    clientTime,
    latitude: lat,
    longitude: lon,
    restId,
  };

  const endpoint = '/delivery-areas/api/v1/cost-conditions/all';
  const paramStr = stringify(params, options);
  const url = urlJoin(ENV.API, endpoint, paramStr);

  return new RequestManager({ url }).getRequest();
};
