// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Lib
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
// Types
import { TFetchRestaurantInfo } from './types';

const { ENV } = Consts;

const getEndpoint = (restaurantId: string) =>
  urlJoin('/catalog/restaurants', restaurantId, '/info');

const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const fetchRestaurantInfo: TFetchRestaurantInfo = ({ restaurantId }) => {
  const endpoint = getEndpoint(restaurantId);
  const data = {};
  const params = stringify(data, options);
  const url = urlJoin(ENV.API, endpoint, params);

  return new RequestManager({ url }).getRequest();
};
