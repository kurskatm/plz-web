// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

interface TFetchRestaurantInfoArgs {
  restaurantId: string;
}
export type TFetchRestaurantInfo = (data: TFetchRestaurantInfoArgs) => Observable<AjaxResponse>;
