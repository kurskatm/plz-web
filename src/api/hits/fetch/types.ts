// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export interface TGetHitsArgs {
  cityId: string;
  filters?: string[];
}

export type TGetHits = (data: TGetHitsArgs) => Observable<AjaxResponse>;
