// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Lib
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
import { getCookieCityData } from '@/lib/cookie';
// Types
import { TGetHits } from './types';

const { ENV } = Consts;

const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

const getEndpoint = (cityId: string) =>
  urlJoin('/catalog/city', cityId, '/products/bonus-showcase');

export const getHits: TGetHits = ({ cityId, filters }) => {
  const city = cityId || getCookieCityData();
  const endpoint = getEndpoint(city as string);
  const params = stringify({ filters }, options);
  const url = urlJoin(ENV.API, endpoint, params);

  return new RequestManager({ url }).getRequest();
};
