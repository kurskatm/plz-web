// Modules
import urlJoin from 'url-join';
import { omit } from 'lodash';

// Lib
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
// Types
import { TSendSupportMessage, TSupportMessageValues } from './types';

const { ENV } = Consts;
const endpointTopics = '/support/contact';
const getPreparedData = (data: TSupportMessageValues) => {
  const [head, middle, tale] = data.phoneNumber.replace(/\s/, '').split('-');

  return {
    ...omit(data, ['token', 'agreement']),
    selectedTopicId: data.selectedTopic?.id,
    phoneNumber: `${head}-${middle}${tale}`,
  };
};

export const supportSendMessage: TSendSupportMessage = (data: TSupportMessageValues) => {
  const url = urlJoin(ENV.API, endpointTopics);
  const preparedData = getPreparedData(data);
  const reqOptions = {
    url,
    method: 'POST',
    body: preparedData,
    headers: {
      GRecaptchaToken: data.token,
    },
  };
  return new RequestManager(reqOptions).getRequest();
};
