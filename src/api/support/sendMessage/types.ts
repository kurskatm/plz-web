// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export type TGetSupportTopics = () => Observable<AjaxResponse>;
export type TSendSupportMessage = (data: Record<string, unknown>) => Observable<AjaxResponse>;
export type TSupportMessageValues = {
  name: string,
  email: string,
  phoneNumber: string,
  selectedTopic: Record<string, unknown>,
  message: string,
  token: string,
};
