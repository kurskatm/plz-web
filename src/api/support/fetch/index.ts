// Modules
import urlJoin from 'url-join';

// Lib
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
// Types
import { TGetSupportTopics } from './types';

const { ENV } = Consts;
const endpointTopics = '/support/contact/topics';

export const getSupportTopics: TGetSupportTopics = () => {
  const url = urlJoin(ENV.API, endpointTopics);

  return new RequestManager({ url }).getRequest();
};
