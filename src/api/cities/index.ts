// Modules
import urlJoin from 'url-join';
// Lib
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
// Types
import { TGetCities } from './types';

const { ENV } = Consts;

const endpoint = '/location/cities/geo';

export const getCities: TGetCities = () => {
  const url = urlJoin(ENV.API, endpoint);

  return new RequestManager({ url }).getRequest();
};
