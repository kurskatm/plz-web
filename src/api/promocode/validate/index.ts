// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Lib
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
import { getAccessToken } from '@/lib/cookie/accessToken';

// Types
import { TGetHits } from './types';

const { ENV } = Consts;

const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const validatePromocode: TGetHits = (promocode: string) => {
  const endpoint = '/promocodes/promocodes/validation';
  const params = stringify({ promoCodeName: promocode }, options);
  const url = urlJoin(ENV.API, endpoint, params);
  const accessToken = getAccessToken() || undefined;

  return new RequestManager({
    method: 'GET',
    url,
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  }).getRequest();
};
