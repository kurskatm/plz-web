// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export type TGetHits = (promocode: string) => Observable<AjaxResponse>;
