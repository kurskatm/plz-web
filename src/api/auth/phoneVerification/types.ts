// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export type TVerifyPhoneRequest = (data: Record<string, unknown>) => Observable<AjaxResponse>;
