// Modules
import urlJoin from 'url-join';
// Lib
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
// Types
import { TVerifyPhoneRequest } from './types';

const { ENV } = Consts;

const endpoint = '/api/verify-phone';

export const verifyPhoneRequest: TVerifyPhoneRequest = (data: Record<string, unknown>) => {
  const url = urlJoin(ENV.API_AUTH, endpoint);
  const reqOptions = {
    url,
    method: data?.resendToken ? 'PUT' : 'POST',
    body: data,
  };

  return new RequestManager(reqOptions).getRequest();
};
