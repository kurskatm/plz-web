// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Lib
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
// Types
import { TCleanPhoneRequest } from './types';

const { ENV } = Consts;

const endpoint = '/api/verify-phone';
const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const cleanePhoneRequest: TCleanPhoneRequest = (phone: string) => {
  const params = stringify({ phone }, options);
  const url = urlJoin(ENV.API_AUTH, endpoint, params);

  return new RequestManager({ url, method: 'DELETE' }).getRequest();
};
