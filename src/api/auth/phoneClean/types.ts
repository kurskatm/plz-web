// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export type TCleanPhoneRequest = (phone: string) => Observable<AjaxResponse>;
