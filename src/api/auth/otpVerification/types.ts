// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export type TVerifyOtpRequest = (data: Record<string, unknown>) => Observable<AjaxResponse>;
