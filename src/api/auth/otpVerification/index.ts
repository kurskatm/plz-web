// Modules
import urlJoin from 'url-join';
// Lib
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
// Types
import { TVerifyOtpRequest } from './types';

const { ENV } = Consts;

const endpoint = '/connect/token';

const makeReqOptions = (data: Record<string, unknown>, url: string) => ({
  url,
  method: 'POST',
  body: {
    client_id: 'WebClientCustomer',
    grant_type: data?.refresh_token ? 'refresh_token' : 'phone_number_token',
    scope: 'openid profile offline_access',
    ...data,
  },
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded',
  },
});

export const verifyOtpRequest: TVerifyOtpRequest = (data: Record<string, unknown>) => {
  const url = urlJoin(ENV.API_AUTH, endpoint);
  const reqOptions = makeReqOptions(data, url);

  return new RequestManager(reqOptions).getRequest();
};
