// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export type TGetFilters = (id: string) => Observable<AjaxResponse>;
