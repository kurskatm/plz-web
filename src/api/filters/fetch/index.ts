// Modules
import urlJoin from 'url-join';
// Lib
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
// Types
import { TGetFilters } from './types';

const { ENV } = Consts;

const endpoint = '/catalog/filters/?cityId=';

export const getFilters: TGetFilters = (id) => {
  const url = urlJoin(ENV.API, `${endpoint}${id}`);

  return new RequestManager({ url }).getRequest();
};
