// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';
// Main Types
import { TShopingCartSaveData } from '@type/shoping-cart';

export interface TSaveShopingCartArgs {
  cartId: string;
  userId?: string;
  data?: TShopingCartSaveData;
  lat?: number;
  lng?: number;
}

export type TSaveShopingCart = (data: TSaveShopingCartArgs) => Observable<AjaxResponse>;
