// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
import isNil from 'lodash/isNil';
// Lib
import { RequestManager } from '@lib/request';
import {
  getCartId, setCartId, getProfileData, getCookieCityData,
} from '@lib/cookie';

// Utils
import { Consts } from '@utils';
// Types
import { TProfileData } from '@/lib/cookie/profile/types';
import { TSaveShopingCart } from './types';

const { ENV } = Consts;

const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

const endpoint = '/cart/cart';

export const saveShopingCart: TSaveShopingCart = ({
  cartId,
  userId,
  data,
  lat,
  lng,
}) => {
  let method = 'PUT';
  const cartIdFromCookie = getCartId();
  const profileData: TProfileData = getProfileData() as TProfileData;

  if (cartId && !cartIdFromCookie && data) {
    setCartId(cartId);
  }

  if (data && !data.cityId) {
    data.cityId = getCookieCityData();
  }

  const params = {
    cartId: cartId || cartIdFromCookie,
    userId: userId || profileData.userId,
  };

  if (!isNil(lat)) {
    // @ts-ignore
    params.lat = lat;
  }

  if (!isNil(lng)) {
    // @ts-ignore
    params.lng = lng;
  }

  const strParams = stringify(params, options);
  const url = urlJoin(ENV.API, endpoint, strParams);

  if (!data) {
    method = 'DELETE';
  }

  return new RequestManager({
    method,
    url,
    ...(profileData.access_token
      ? {
        headers: {
          Authorization: `Bearer ${profileData.access_token}`,
        },
      }
      : {}),
    body: { ...data },
  }).getRequest();
};
