// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
import { isNil } from 'lodash';
// Lib
import { RequestManager } from '@lib/request';
import { getCartId, getUserId } from '@/lib/cookie';
// Utils
import { Consts } from '@utils';
// Types
import { TFetchShopingCart } from './types';

const { ENV } = Consts;

const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

const endpoint = '/cart/cart';

export const fetchShopingCart: TFetchShopingCart = ({
  cartId, userId, lat, lng,
}) => {
  const cartIdFromCookie = getCartId();
  const userIdFromCookie = getUserId();
  const params = {
    cartId: cartId || cartIdFromCookie,
    userId: userId || userIdFromCookie,
  };

  if (!isNil(lat)) {
    // @ts-ignore
    params.lat = lat;
  }

  if (!isNil(lng)) {
    // @ts-ignore
    params.lng = lng;
  }

  const strParams = stringify(params, options);
  const url = urlJoin(ENV.API, endpoint, strParams);

  return new RequestManager({
    url,
  }).getRequest();
};
