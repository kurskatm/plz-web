// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export interface TFetchShopingCartArgs {
  cartId?: string;
  userId?: string;
  lat?: number;
  lng?: number;
}

export type TFetchShopingCart = (data: TFetchShopingCartArgs) => Observable<AjaxResponse>;
