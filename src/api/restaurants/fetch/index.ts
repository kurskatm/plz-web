// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Lib
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
// Types
import { TGetRestaurants, TParams } from './types';

const { ENV } = Consts;

const endpoint = '/catalog/restaurants';
const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const getRestaurants: TGetRestaurants = ({
  cityId, filters, lat, lng, search,
}) => {
  const params: TParams = {
    cityId,
    filters,
    lat,
    lng,
  };

  if (search?.length) {
    params.nameQuery = search;
  }

  const strParams = stringify(params, options);
  const url = urlJoin(ENV.API, endpoint, strParams);

  return new RequestManager({ url }).getRequest();
};
