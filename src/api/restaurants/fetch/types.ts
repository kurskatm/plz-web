// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export interface TGetRestaurantsArgs {
  cityId: string;
  lat?: number;
  lng?: number;
  filters?: string[];
  search?: string;
}

export type TGetRestaurants = (data: TGetRestaurantsArgs) => Observable<AjaxResponse>;

export interface TParams {
  cityId: string;
  lat?: number;
  lng?: number;
  filters?: string[];
  nameQuery?: string;
}
