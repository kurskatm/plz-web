// Modules
import urlJoin from 'url-join';
import isNil from 'lodash/isNil';
import { stringify } from 'qs';
// Lib
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
// Types
import { TGetProfileReviews, TParams } from './types';

const { ENV } = Consts;

const endpoint = '/reviews/reviews/user';
const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const getProfileReviews: TGetProfileReviews = ({ access_token, pageNumber, pageSize }) => {
  const params: TParams = {
    pageNumber: !isNil(pageNumber) ? pageNumber : 0,
    takeCount: !isNil(pageSize) ? pageSize : 10,
  };
  const strParams = stringify(params, options);
  const url = urlJoin(ENV.API, endpoint, strParams);
  const reqOptions = {
    url,
    method: 'GET',
    headers: {
      Authorization: `Bearer ${access_token}`,
    },
  };
  return new RequestManager(reqOptions).getRequest();
};
