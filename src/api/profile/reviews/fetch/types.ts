// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export interface TGetProfileReviewsArgs {
  pageNumber: number;
  pageSize: number;
  access_token?: string;
}

export type TGetProfileReviews = (data: TGetProfileReviewsArgs) => Observable<AjaxResponse>;

export interface TParams {
  pageNumber: number;
  takeCount: number;
}
