// Modules
import urlJoin from 'url-join';
// Lib
import { RequestManager } from '@lib/request';
import { getAccessToken } from '@/lib/cookie';
// Utils
import { Consts } from '@utils';
// Types
import { TSetProfileOrderReview } from './types';

const { API } = Consts.ENV;

const endpoint = '/reviews/reviews';

export const setProfileOrderReview: TSetProfileOrderReview = (data) => {
  const { review } = data;
  const accessToken = getAccessToken();

  const url = urlJoin(API, endpoint);
  const reqOptions = {
    url,
    method: 'POST',
    body: { ...review },
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  };

  return new RequestManager(reqOptions).getRequest();
};
