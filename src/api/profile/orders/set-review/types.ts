// Main types
import { TOrderReview } from '@type/profile';
// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export interface TSetProfileOrderReviewArgs {
  review: TOrderReview;
}

export type TSetProfileOrderReview = (data: TSetProfileOrderReviewArgs) => Observable<AjaxResponse>;
