// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export interface TProfileRepeatOrderArgs {
  orderId: string;
  lat: number;
  lng: number;
}

export type TProfileRepeatOrder = (data: TProfileRepeatOrderArgs) => Observable<AjaxResponse>;

export interface TParams {
  orderId: string;
  lat: number;
  lng: number;
}
