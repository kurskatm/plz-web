// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Lib
import { RequestManager } from '@lib/request';
import { getAccessToken } from '@lib/cookie';
// Utils
import { Consts } from '@utils';
// Types
import { TProfileRepeatOrder, TParams } from './types';

const { API } = Consts.ENV;
const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const profileRepeatOrder: TProfileRepeatOrder = ({ orderId, lat, lng }) => {
  const params: TParams = {
    orderId,
    lat,
    lng,
  };
  const endpoint = '/cart/cart';
  const strParams = stringify(params, options);
  const accessToken = getAccessToken();
  const url = urlJoin(API, endpoint, strParams);

  const reqOptions = {
    url,
    method: 'POST',
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  };

  return new RequestManager(reqOptions).getRequest();
};
