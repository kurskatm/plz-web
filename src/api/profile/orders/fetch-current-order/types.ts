// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export interface TGetProfileUserOrderArgs {
  orderId: string;
}

export type TGetProfileUserOrder = (data: TGetProfileUserOrderArgs) => Observable<AjaxResponse>;

export interface TParams {
  orderId: string;
}
