// Modules
import urlJoin from 'url-join';
// Lib
import { RequestManager } from '@lib/request';
import { getAccessToken } from '@/lib/cookie';
// Utils
import { Consts } from '@utils';
// Types
import { TGetProfileUserOrder } from './types';

const { API_BFF } = Consts.ENV;

export const getProfileUserOrder: TGetProfileUserOrder = (data) => {
  const { orderId } = data;
  const endpoint = `/checkout/order/${orderId}`;

  const url = urlJoin(API_BFF, endpoint);

  const accessToken = getAccessToken();

  const reqOptions = {
    url,
    method: 'GET',
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  };

  return new RequestManager(reqOptions).getRequest();
};
