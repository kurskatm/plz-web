// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export interface TGetProfileLastOrderWithoutReviewArgs {
  restaurantId?: string;
}

export type TGetProfileLastOrderWithoutReview = (
  data?: TGetProfileLastOrderWithoutReviewArgs,
) => Observable<AjaxResponse>;

export interface TParams {
  restaurantId: string;
}
