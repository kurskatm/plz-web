// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Lib
import { RequestManager } from '@lib/request';
import { getAccessToken } from '@/lib/cookie';
// Utils
import { Consts } from '@utils';
// Types
import { TGetProfileLastOrderWithoutReview } from './types';

const { API } = Consts.ENV;
const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const getProfileLastOrderWithoutReview: TGetProfileLastOrderWithoutReview = (data = {}) => {
  const baseEndpoint = '/orders/user/orders/review';
  const { restaurantId } = data;
  const endpoint = restaurantId
    ? urlJoin(
        baseEndpoint,
        stringify(
          {
            restaurantId,
          },
          options,
        ),
      )
    : baseEndpoint;

  const url = urlJoin(API, endpoint);

  const accessToken = getAccessToken();

  const reqOptions = {
    url,
    method: 'GET',
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  };

  return new RequestManager(reqOptions).getRequest();
};
