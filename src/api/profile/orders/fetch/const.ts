// Types
import { TParams } from './types';

export const DEFAULT_PARAMS: TParams = {
  pageNumber: 0,
  pageSize: 10,
};
