// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
import isNil from 'lodash/isNil';
// Lib
import { RequestManager } from '@lib/request';
import { getAccessToken } from '@lib/cookie';
// Utils
import { Consts } from '@utils';
// Consts
import { DEFAULT_PARAMS } from './const';
// Types
import { TGetProfileOrders, TParams } from './types';

const { API } = Consts.ENV;
const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const getProfileOrders: TGetProfileOrders = ({ pageNumber, pageSize, access_token }) => {
  const params: TParams = {
    pageNumber: !isNil(pageNumber) ? pageNumber : DEFAULT_PARAMS.pageNumber,
    pageSize: !isNil(pageSize) ? pageSize : DEFAULT_PARAMS.pageSize,
  };
  const endpoint = '/orders/user/orders';
  const strParams = stringify(params, options);
  const accessToken = getAccessToken();
  const url = urlJoin(API, endpoint, strParams);

  const reqOptions = {
    url,
    method: 'GET',
    headers: {
      Authorization: `Bearer ${access_token || accessToken}`,
    },
  };

  return new RequestManager(reqOptions).getRequest();
};
