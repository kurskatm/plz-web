// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export interface TGetProfileOrdersArgs {
  pageNumber: number;
  pageSize: number;
  access_token?: string;
}

export type TGetProfileOrders = (data: TGetProfileOrdersArgs) => Observable<AjaxResponse>;

export interface TParams {
  pageNumber: number;
  pageSize: number;
}
