// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export interface TProfileOrderContactMeArgs {
  orderId: string;
}

export type TProfileOrderContactMe = (data: TProfileOrderContactMeArgs) => Observable<AjaxResponse>;

export interface TParams {
  orderId: string;
}
