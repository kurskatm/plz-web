// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Lib
import { RequestManager } from '@lib/request';
import { getAccessToken } from '@lib/cookie';
// Utils
import { Consts } from '@utils';
// Types
import { TProfileOrderContactMe, TParams } from './types';

const { API_BFF } = Consts.ENV;
const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const profileOrderContactMe: TProfileOrderContactMe = ({ orderId }) => {
  const params: TParams = {
    orderId,
  };
  const endpoint = '/customer/orders/contact-me-request';
  const strParams = stringify(params, options);
  const accessToken = getAccessToken();
  const url = urlJoin(API_BFF, endpoint, strParams);

  const reqOptions = {
    url,
    method: 'POST',
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  };

  return new RequestManager(reqOptions).getRequest();
};
