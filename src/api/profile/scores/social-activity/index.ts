// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Lib
import { RequestManager } from '@lib/request';
import { getAccessToken } from '@lib/cookie';
// Utils
import { Consts } from '@utils';
// Types
import { TSetProfileSocialActivity, TParams } from './types';

const { API } = Consts.ENV;
const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const setProfileSocialActivity: TSetProfileSocialActivity = ({ type }) => {
  const params: TParams = {
    type,
  };
  const endpoint = '/users/bonus-wallet/social';
  const strParams = stringify(params, options);
  const accessToken = getAccessToken();
  const url = urlJoin(API, endpoint, strParams);

  const reqOptions = {
    url,
    method: 'Post',
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  };

  return new RequestManager(reqOptions).getRequest();
};
