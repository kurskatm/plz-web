// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export interface TSetProfileSocialActivityArgs {
  type: number;
}

export type TSetProfileSocialActivity = (
  data: TSetProfileSocialActivityArgs,
) => Observable<AjaxResponse>;

export interface TParams {
  type: number;
}
