// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
import isNil from 'lodash/isNil';
// Lib
import { RequestManager } from '@lib/request';
import { getAccessToken } from '@lib/cookie';
// Utils
import { Consts } from '@utils';
// Consts
import { DEFAULT_PARAMS } from './const';
// Types
import { TGetProfileNewOrders, TParams } from './types';

const { API_BFF } = Consts.ENV;
const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const getProfileNewOrders: TGetProfileNewOrders = ({ after, count, access_token }) => {
  const params: TParams = {
    count: !isNil(count) ? count : DEFAULT_PARAMS.count,
    after: !isNil(after) ? after : DEFAULT_PARAMS.after,
  };
  const endpoint = 'customer/orders';
  const strParams = stringify(params, options);
  const accessToken = getAccessToken();
  const url = urlJoin(API_BFF, endpoint, strParams);

  const reqOptions = {
    url,
    method: 'GET',
    headers: {
      Authorization: `Bearer ${access_token || accessToken}`,
    },
  };

  return new RequestManager(reqOptions).getRequest();
};
