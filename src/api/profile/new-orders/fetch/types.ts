// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export interface TGetProfileNewOrdersArgs {
  count: number;
  after: string | null;
  access_token?: string;
}

export type TGetProfileNewOrders = (data: TGetProfileNewOrdersArgs) => Observable<AjaxResponse>;

export interface TParams {
  count: number;
  after: string | null;
}
