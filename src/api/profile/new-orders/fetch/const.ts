// Types
import { TParams } from './types';

export const DEFAULT_PARAMS: TParams = {
  count: 10,
  after: null,
};
