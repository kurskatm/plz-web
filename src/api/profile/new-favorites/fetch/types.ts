// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export interface TGetFavoritesArgs {
  access_token: string;
  cityId: string;
  latitude: number;
  longitude: number;
}

export type TGetFavorites = ({
  access_token,
  cityId,
  latitude,
  longitude,
}: TGetFavoritesArgs) => Observable<AjaxResponse>;
