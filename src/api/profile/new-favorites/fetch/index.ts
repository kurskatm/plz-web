// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Lib
import { RequestManager } from '@lib/request';
import { getAccessToken } from '@lib/cookie';
// Utils
import { Consts } from '@utils';
// Types
import { TGetFavorites, TGetFavoritesArgs } from './types';

const { ENV } = Consts;
const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

const endpoint = '/favorites/summary';

export const getFavorites: TGetFavorites = ({
  access_token,
  cityId,
  latitude,
  longitude,
}: TGetFavoritesArgs) => {
  const params = {
    cityId,
    latitude,
    longitude,
  };
  const strParams = stringify(params, options);
  const url = urlJoin(ENV.API_BFF, endpoint, strParams);
  const accessToken = getAccessToken();

  return new RequestManager({
    url,
    method: 'GET',
    headers: {
      Authorization: `Bearer ${access_token || accessToken}`,
    },
  }).getRequest();
};
