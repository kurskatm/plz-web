// Modules
import urlJoin from 'url-join';
// Lib
import { RequestManager } from '@lib/request';
import { getAccessToken } from '@lib/cookie';
// Utils
import { Consts } from '@utils';
// Types
import { TGetBonus } from './types';

const { ENV } = Consts;

const endpoint = '/users/bonus-wallet';

export const getBonus: TGetBonus = ({ access_token }) => {
  const url = urlJoin(ENV.API, endpoint);
  const accessToken = getAccessToken();
  const reqOptions = {
    url,
    method: 'GET',
    headers: {
      Authorization: `Bearer ${access_token || accessToken}`,
    },
  };
  return new RequestManager(reqOptions).getRequest();
};
