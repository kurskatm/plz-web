// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export type TGetBonus = (token: Record<string, string>) => Observable<AjaxResponse>;
