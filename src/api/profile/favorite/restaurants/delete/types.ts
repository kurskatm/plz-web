// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export type TDeleteFavoriteRestsArgs = {
  restId: string;
};

export type TDeleteFavoriteRest = ({
  restId,
}: TDeleteFavoriteRestsArgs) => Observable<AjaxResponse>;
