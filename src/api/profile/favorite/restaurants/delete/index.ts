// Modules
import urlJoin from 'url-join';
// Lib
import { RequestManager } from '@lib/request';
import { getAccessToken } from '@lib/cookie';
// Utils
import { Consts } from '@utils';
// Types
import { TDeleteFavoriteRest } from './types';

const { ENV } = Consts;

const endpoint = (restId: string): string => `/user-favorites/restaurants/${restId}`;

export const deleteFavoriteRestaurant: TDeleteFavoriteRest = ({ restId }) => {
  const url = urlJoin(ENV.API, endpoint(restId));
  const accessToken = getAccessToken();
  const reqOptions = {
    url,
    method: 'DELETE',
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  };
  return new RequestManager(reqOptions).getRequest();
};
