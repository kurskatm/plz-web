// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export type TSetFavoriteRestaurantArgs = {
  restId: string;
};

export type TSetFavoriteRestaurant = ({
  restId,
}: TSetFavoriteRestaurantArgs) => Observable<AjaxResponse>;
