// Modules
import urlJoin from 'url-join';
// Lib
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
// Types
import { TGetFavoriteRestaurants } from './types';

const { ENV } = Consts;

const endpoint = (userId: string): string => `/user-favorites/user/${userId}/restaurants`;

export const getFavoriteRestaurants: TGetFavoriteRestaurants = ({ userId }: { userId: string }) => {
  const url = urlJoin(ENV.API, endpoint(userId));

  return new RequestManager({ url }).getRequest();
};
