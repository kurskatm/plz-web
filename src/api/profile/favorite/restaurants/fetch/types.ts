// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export type TGetFavoriteRestaurants = (data: Record<string, string>) => Observable<AjaxResponse>;
