// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export type TGetFavoriteRestaurants = ({ userId }: { userId: string }) => Observable<AjaxResponse>;
