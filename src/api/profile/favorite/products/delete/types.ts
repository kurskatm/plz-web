// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export type TSetFavoriteProductArgs = {
  productId: string;
};

export type TDeleteFavoriteProduct = ({
  productId,
}: TSetFavoriteProductArgs) => Observable<AjaxResponse>;
