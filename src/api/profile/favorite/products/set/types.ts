// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export type TSetFavoriteProductArgs = {
  productId: string;
};

export type TSetFavoriteProduct = ({
  productId,
}: TSetFavoriteProductArgs) => Observable<AjaxResponse>;
