// Modules
import urlJoin from 'url-join';
// Lib
import { RequestManager } from '@lib/request';
import { getAccessToken } from '@lib/cookie';
// Utils
import { Consts } from '@utils';
// Types
import { TSetFavoriteProduct } from './types';

const { ENV } = Consts;

const endpoint = (productId: string): string => `/user-favorites/products/${productId}`;

export const setFavoriteProduct: TSetFavoriteProduct = ({ productId }) => {
  const url = urlJoin(ENV.API, endpoint(productId));
  const accessToken = getAccessToken();
  const reqOptions = {
    url,
    method: 'PUT',
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  };
  return new RequestManager(reqOptions).getRequest();
};
