// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export type TUpdateProfileInfoArgs = {
  access_token: string;
  payload: Record<string, string>;
};

export type TUpdateProfileInfo = (data: TUpdateProfileInfoArgs) => Observable<AjaxResponse>;
