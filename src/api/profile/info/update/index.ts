// Modules
import urlJoin from 'url-join';
// Lib
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
// Types
import { TUpdateProfileInfo } from './types';

const { ENV } = Consts;

const endpoint = '/users/customers/';

export const updateProfileInfo: TUpdateProfileInfo = ({ access_token, payload }) => {
  const url = urlJoin(ENV.API, endpoint);
  const reqOptions = {
    url,
    method: 'PUT',
    headers: {
      Authorization: `Bearer ${access_token}`,
    },
    body: payload,
  };

  return new RequestManager(reqOptions).getRequest();
};
