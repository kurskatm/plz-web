// Modules
import urlJoin from 'url-join';
// Lib
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
// Types
import { TGetProfileInfo } from './types';

const { ENV } = Consts;

const endpoint = '/users/customers/current';

export const getProfileInfo: TGetProfileInfo = ({ access_token }) => {
  const url = urlJoin(ENV.API, endpoint);
  const reqOptions = {
    url,
    method: 'GET',
    headers: {
      Authorization: `Bearer ${access_token}`,
    },
  };
  return new RequestManager(reqOptions).getRequest();
};
