// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export type TGetProfileInfo = (data: Record<string, string>) => Observable<AjaxResponse>;
