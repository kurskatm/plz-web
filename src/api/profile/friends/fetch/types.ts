// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export type TGetProfileFriends = (data: Record<string, string>) => Observable<AjaxResponse>;
