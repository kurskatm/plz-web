// Modules
import urlJoin from 'url-join';
// Lib
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
// Types
import { TGetProfileFriends } from './types';

const { ENV } = Consts;

const endpoint = '/users/customers/invites';

export const getProfileFriends: TGetProfileFriends = ({ access_token }) => {
  const url = urlJoin(ENV.API, endpoint);
  const reqOptions = {
    url,
    method: 'GET',
    headers: {
      Authorization: `Bearer ${access_token}`,
    },
  };
  return new RequestManager(reqOptions).getRequest();
};
