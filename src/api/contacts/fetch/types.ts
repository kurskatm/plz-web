// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

interface TGetContactsArgs {
  cityId?: string;
}
export type TGetContacts = (data: TGetContactsArgs) => Observable<AjaxResponse>;
