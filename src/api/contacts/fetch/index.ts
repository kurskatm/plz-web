// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Lib
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
// Types
import { TGetContacts } from './types';

const { ENV } = Consts;

const endpoint = '/location/city/contacts';
const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const getContacts: TGetContacts = ({ cityId } = {}) => {
  const params = stringify({ cityId }, options);
  const url = urlJoin(ENV.API, endpoint, params);

  return new RequestManager({ url }).getRequest();
};
