// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
import isNil from 'lodash/isNil';
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
// Types
import { TFoodForPoints } from './types';

const { ENV } = Consts;
const endpoint = '/food-for-points';
const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const getFoodForPoints: TFoodForPoints = ({ cityId, lat, lng }) => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const queryParams: any = {};
  if (!isNil(cityId)) {
    queryParams.cityId = cityId;
  }
  if (!isNil(lat)) {
    queryParams.latitude = lat;
  }
  if (!isNil(lng)) {
    queryParams.longitude = lng;
  }
  const queryParamsStr = stringify(queryParams, options);
  const url = urlJoin(ENV.API_BFF, endpoint, queryParamsStr);

  return new RequestManager({ url }).getRequest();
};
