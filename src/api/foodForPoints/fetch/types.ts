// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export interface TGetFoodForPointsArgs {
  cityId: string;
  lat?: number;
  lng?: number;
}

export type TFoodForPoints = (data: TGetFoodForPointsArgs) => Observable<AjaxResponse>;
