// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Lib
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
// Types
import { TFetchReviews } from './types';

const { ENV } = Consts;

const endpoint = '/reviews/reviews/top';
const options = {
  addQueryPrefix: true,
  skipNulls: true,
  indices: false,
};

export const fetchReviews: TFetchReviews = (data) => {
  const params = stringify(data, options);
  const url = urlJoin(ENV.API, endpoint, params);

  return new RequestManager({ url }).getRequest();
};
