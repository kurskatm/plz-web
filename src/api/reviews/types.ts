// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';
// Types
import { TReviewsMainFetchArgs } from '@type/reviews-main';

export type TFetchReviews = (data: TReviewsMainFetchArgs) => Observable<AjaxResponse>;
