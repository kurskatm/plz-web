// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';
// Types
import { TGetHotOffersArgs } from '@type/hotOffers';

export type TGetHotOffers = (data: TGetHotOffersArgs) => Observable<AjaxResponse>;
