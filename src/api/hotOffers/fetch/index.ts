// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Lib
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
// Types
import { TGetHotOffers } from './types';

const { ENV } = Consts;

const endpoint = '/promo/hot-offers';
const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const getHotOffers: TGetHotOffers = ({ cityId }) => {
  const params = stringify({ cityId }, options);
  const url = urlJoin(ENV.API, endpoint, params);

  return new RequestManager({ url }).getRequest();
};
