// Modules
// import urlJoin from 'url-join';
// Lib
import { RequestManager } from '@lib/request';
// Utils
// import { Consts } from '@utils';
// Types
import { TGetCities } from './types';

// const { ENV } = Consts;

const endpoint = 'https://127.0.0.1:7100/api/v1/f_types';

export const getFuncTypes: TGetCities = () => {
  const url = endpoint;

  return new RequestManager({ url }).getRequest();
};
