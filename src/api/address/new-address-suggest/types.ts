// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export interface TGetNewAddressSuggestArgs {
  query: string;
  limit: number;
}
export type TGetNewAddressSuggest = (data: TGetNewAddressSuggestArgs) => Observable<AjaxResponse>;
