// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Lib
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
// Types
import { TGetNewAddressSuggest } from './types';

const { ENV } = Consts;

const endpoint = '/location/v2/address/suggest';
const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const getNewAddressSuggest: TGetNewAddressSuggest = ({ query, limit = 5 }) => {
  const params = stringify({ query, limit }, options);
  const url = urlJoin(ENV.API, endpoint, params);

  return new RequestManager({ url }).getRequest();
};
