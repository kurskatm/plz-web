// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export interface TGetNewAddressGeoCodeArgs {
  name: string;
  description: string;
}
export type TGetNewAddressGeoCode = (data: TGetNewAddressGeoCodeArgs) => Observable<AjaxResponse>;

export interface TParams {
  address: string;
}
