// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Lib
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
// Types
import { TGetNewAddressGeoCode, TParams } from './types';

const { ENV } = Consts;

const endpoint = '/location/v2/address/geocode';
const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const getNewAddressGeoCode: TGetNewAddressGeoCode = ({ description }) => {
  const params: TParams = {
    address: description,
  };
  const paramsStr = stringify(params, options);
  const url = urlJoin(ENV.API, endpoint, paramsStr);

  return new RequestManager({ url }).getRequest();
};
