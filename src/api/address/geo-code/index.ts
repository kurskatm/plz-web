// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Lib
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
// Types
import { TGetAddressGeoCode } from './types';

const { ENV } = Consts;

const endpoint = '/location/address/geocode';
const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const getAddressGeoCode: TGetAddressGeoCode = ({ address, cityId }) => {
  const params = stringify({ address, cityId }, options);
  const url = urlJoin(ENV.API, endpoint, params);

  return new RequestManager({ url }).getRequest();
};
