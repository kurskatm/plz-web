// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export interface TGetAddressGeoCodeArgs {
  address: string;
  cityId: string;
}
export type TGetAddressGeoCode = (data: TGetAddressGeoCodeArgs) => Observable<AjaxResponse>;
