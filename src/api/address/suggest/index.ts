// Modules
import urlJoin from 'url-join';
import { stringify } from 'qs';
// Lib
import { RequestManager } from '@lib/request';
// Utils
import { Consts } from '@utils';
// Types
import { TGetAddressSuggest } from './types';

const { ENV } = Consts;

const endpoint = '/location/address/suggest';
const options = {
  addQueryPrefix: true,
  skipNulls: true,
};

export const getAddressSuggest: TGetAddressSuggest = ({ query, limit = 5, cityId }) => {
  const params = stringify({ query, limit, cityId }, options);
  const url = urlJoin(ENV.API, endpoint, params);

  return new RequestManager({ url }).getRequest();
};
