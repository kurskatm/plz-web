// Modules
import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export interface TGetAddressSuggestArgs {
  query: string;
  limit?: number;
  cityId: string;
  fromHeader: boolean;
}
export type TGetAddressSuggest = (data: TGetAddressSuggestArgs) => Observable<AjaxResponse>;
