// Constants
import {
  RESTAURANTS_AND_HITS_FETCH,
  RESTAURANTS_AND_HITS_SAVE,
  RESTAURANTS_AND_HITS_PENDING,
  RESTAURANTS_AND_HITS_SUCCESS,
  RESTAURANTS_AND_HITS_ERROR,
} from '@constants/restaurants-and-hits';
// Types
import {
  TRestaurantsAndHitsFetch,
  TRestaurantsAndHitsSave,
  TRestaurantsAndHitsPending,
  TRestaurantsAndHitsSuccess,
  TRestaurantsAndHitsError,
} from './action-types';

// Free products
export const restaurantsAndHitsFetch: TRestaurantsAndHitsFetch = (data) => ({
  type: RESTAURANTS_AND_HITS_FETCH,
  payload: data,
});

export const restaurantsAndHitsSave: TRestaurantsAndHitsSave = (data) => ({
  type: RESTAURANTS_AND_HITS_SAVE,
  payload: data,
});

export const restaurantsAndHitsPending: TRestaurantsAndHitsPending = () => ({
  type: RESTAURANTS_AND_HITS_PENDING,
});

export const restaurantsAndHitsSuccess: TRestaurantsAndHitsSuccess = ({ status }) => ({
  type: RESTAURANTS_AND_HITS_SUCCESS,
  payload: { status },
});

export const restaurantsAndHitsError: TRestaurantsAndHitsError = ({ status }) => ({
  type: RESTAURANTS_AND_HITS_ERROR,
  payload: { status },
});
