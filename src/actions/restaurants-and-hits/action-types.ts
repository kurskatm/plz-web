/* eslint-disable @typescript-eslint/indent */
// Modules Types
import { ActionCreator } from '@type/actions';
import { TFreeProductsItem } from '@type/products';
// Constants
import {
  RESTAURANTS_AND_HITS_FETCH,
  RESTAURANTS_AND_HITS_SAVE,
  RESTAURANTS_AND_HITS_PENDING,
  RESTAURANTS_AND_HITS_SUCCESS,
  RESTAURANTS_AND_HITS_ERROR,
} from '@constants/restaurants-and-hits';

export interface TGetFreeProductsArgs {
  cityId: string;
  showFavorites?: boolean;
  lat?: number;
  lng?: number;
  filters?: string[];
  userId?: string;
  search?: string;
  onSuccess?: () => void;
}

export type TRestaurantsAndHitsFetch = ActionCreator<
  typeof RESTAURANTS_AND_HITS_FETCH,
  TGetFreeProductsArgs
>;

export type TRestaurantsAndHitsSave = ActionCreator<
  typeof RESTAURANTS_AND_HITS_SAVE,
  TFreeProductsItem
>;

export type TRestaurantsAndHitsPending = ActionCreator<typeof RESTAURANTS_AND_HITS_PENDING>;

export type TRestaurantsAndHitsSuccess = ActionCreator<
  typeof RESTAURANTS_AND_HITS_SUCCESS,
  { status: number }
>;

export type TRestaurantsAndHitsError = ActionCreator<
  typeof RESTAURANTS_AND_HITS_ERROR,
  { status: number }
>;
