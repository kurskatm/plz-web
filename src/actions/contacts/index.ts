// Constants
import {
  CONTACTS_FETCH,
  CONTACTS_SAVE,
  CONTACTS_PENDING,
  CONTACTS_SUCCESS,
  CONTACTS_ERROR,
} from '@constants/contacts';
// Types
import {
  TContactsFetch,
  TContactsSave,
  TContactsUiPending,
  TContactsUiSuccess,
  TContactsUiError,
} from './action-types';

export const contactsFetch: TContactsFetch = (data) => ({
  type: CONTACTS_FETCH,
  payload: data,
});

export const contactsSave: TContactsSave = (data) => ({
  type: CONTACTS_SAVE,
  payload: data,
});

export const contactsSetPending: TContactsUiPending = () => ({
  type: CONTACTS_PENDING,
});

export const contactsSetSuccess: TContactsUiSuccess = ({ status }) => ({
  type: CONTACTS_SUCCESS,
  payload: { status },
});

export const contactsSetError: TContactsUiError = ({ status }) => ({
  type: CONTACTS_ERROR,
  payload: { status },
});
