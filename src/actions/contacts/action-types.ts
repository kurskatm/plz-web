// Modules Types
import { ActionCreator } from '@type/actions';
// Constants
import {
  CONTACTS_FETCH,
  CONTACTS_SAVE,
  CONTACTS_PENDING,
  CONTACTS_SUCCESS,
  CONTACTS_ERROR,
} from '@constants/contacts';
// Main Types
import { TContacts } from '@type/contacts';

export type TContactsFetch = ActionCreator<typeof CONTACTS_FETCH, { cityId: string }>;

export type TContactsSave = ActionCreator<typeof CONTACTS_SAVE, TContacts>;

export type TContactsUiPending = ActionCreator<typeof CONTACTS_PENDING>;

export type TContactsUiSuccess = ActionCreator<
  typeof CONTACTS_SUCCESS,
  { status: string | number }
>;

export type TContactsUiError = ActionCreator<typeof CONTACTS_ERROR, { status: string | number }>;
