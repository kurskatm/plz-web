// Modules Types
import { ActionCreator } from '@type/actions';
// Constants
import { INSTALL_MOBILE_APP_SHOW } from '@constants/install-mobile-app';

export type TInstallMobileAppShow = ActionCreator<typeof INSTALL_MOBILE_APP_SHOW>;
