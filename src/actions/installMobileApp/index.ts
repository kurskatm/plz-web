// Constants
import { INSTALL_MOBILE_APP_SHOW } from '@constants/install-mobile-app';
// Types
import { TInstallMobileAppShow } from './action-types';

export const installMobileAppShow: TInstallMobileAppShow = (data) => ({
  type: INSTALL_MOBILE_APP_SHOW,
  payload: data,
});
