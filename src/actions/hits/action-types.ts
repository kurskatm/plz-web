// Modules Types
import { ActionCreator } from '@type/actions';
import { THitsItem } from '@type/hits';

// Constants
import { HITS_FETCH, HITS_PENDING, HITS_SUCCESS, HITS_ERROR, HITS_SAVE } from '@constants/hits';

interface TGetHitsArgs {
  cityId: string;
  filters?: string[];
}

// get hot offers
export type THitsFetch = ActionCreator<typeof HITS_FETCH, TGetHitsArgs>;

export type THitsPending = ActionCreator<typeof HITS_PENDING>;

export type THitsSuccess = ActionCreator<typeof HITS_SUCCESS, { status: number }>;

export type THitsError = ActionCreator<typeof HITS_ERROR, { status: number }>;
export type THitsSave = ActionCreator<typeof HITS_SAVE, THitsItem>;
