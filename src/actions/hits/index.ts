// Constants
import { HITS_FETCH, HITS_SAVE, HITS_PENDING, HITS_SUCCESS, HITS_ERROR } from '@constants/hits';
// Types
import { THitsFetch, THitsPending, THitsSuccess, THitsError, THitsSave } from './action-types';

// Restaurants
export const hitsFetch: THitsFetch = (data) => ({
  type: HITS_FETCH,
  payload: data,
});

export const hitsSave: THitsSave = (data) => ({
  type: HITS_SAVE,
  payload: data,
});

export const hitsPending: THitsPending = () => ({
  type: HITS_PENDING,
});

export const hitsSuccess: THitsSuccess = ({ status }) => ({
  type: HITS_SUCCESS,
  payload: { status },
});

export const hitsError: THitsError = ({ status }) => ({
  type: HITS_ERROR,
  payload: { status },
});
