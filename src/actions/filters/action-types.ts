/* eslint-disable @typescript-eslint/indent */
// Modules Types
import { ActionCreator } from '@type/actions';
// Constants
import {
  FILTERS_FETCH,
  FILTERS_SAVE,
  FILTERS_SET,
  FILTERS_SEARCH,
  FILTERS_FETCH_PENDING,
  FILTERS_FETCH_SUCCESS,
  FILTERS_FETCH_ERROR,

  // Modal
  FILTERS_SHOW_MODAL,
} from '@constants/filters';
// Main Types
import { TFilter } from '@type/filters';

export type TFiltersFetch = ActionCreator<typeof FILTERS_FETCH>;

export type TFiltersSave = ActionCreator<typeof FILTERS_SAVE, TFilter[]>;

export type TFiltersSet = ActionCreator<typeof FILTERS_SET, string[]>;

export type TFiltersFetchPending = ActionCreator<typeof FILTERS_FETCH_PENDING>;

export type TFiltersFetchSuccess = ActionCreator<
  typeof FILTERS_FETCH_SUCCESS,
  { status: string | number }
>;

export type TFiltersFetchError = ActionCreator<
  typeof FILTERS_FETCH_ERROR,
  { status: string | number }
>;

export type TFiltersSearch = ActionCreator<typeof FILTERS_SEARCH, string | null>;

export type TFiltersShowModal = ActionCreator<typeof FILTERS_SHOW_MODAL, boolean>;
