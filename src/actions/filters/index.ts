// Constants
import {
  FILTERS_FETCH,
  FILTERS_SAVE,
  FILTERS_SET,
  FILTERS_SEARCH,
  FILTERS_FETCH_PENDING,
  FILTERS_FETCH_SUCCESS,
  FILTERS_FETCH_ERROR,
  // Modal
  FILTERS_SHOW_MODAL,
} from '@constants/filters';
// Types
import {
  TFiltersFetch,
  TFiltersSave,
  TFiltersSet,
  TFiltersSearch,
  TFiltersFetchPending,
  TFiltersFetchSuccess,
  TFiltersFetchError,
  // Modal
  TFiltersShowModal,
} from './action-types';

export const filtersFetch: TFiltersFetch = (data) => ({
  type: FILTERS_FETCH,
  payload: data,
});

export const filtersSave: TFiltersSave = (data) => ({
  type: FILTERS_SAVE,
  payload: data,
});

export const filtersSet: TFiltersSet = (data) => ({
  type: FILTERS_SET,
  payload: data,
});

export const filtersFetchPending: TFiltersFetchPending = () => ({
  type: FILTERS_FETCH_PENDING,
});

export const filtersFetchSuccess: TFiltersFetchSuccess = ({ status }) => ({
  type: FILTERS_FETCH_SUCCESS,
  payload: { status },
});

export const filtersFetchError: TFiltersFetchError = ({ status }) => ({
  type: FILTERS_FETCH_ERROR,
  payload: { status },
});

export const filtersSearch: TFiltersSearch = (data) => ({
  type: FILTERS_SEARCH,
  payload: data,
});

export const filtersShowModal: TFiltersShowModal = (data) => ({
  type: FILTERS_SHOW_MODAL,
  payload: data,
});
