// Constants
import {
  CHECKOUT_SEND,
  CHECKOUT_SEND_PENDING,
  CHECKOUT_SEND_SUCCESS,
  CHECKOUT_SEND_ERROR,
  // CHECKOUT_ADD_ORDER_ID,
} from '@constants/checkout';
// Types
import {
  TCheckoutSend,
  TCheckoutSendPending,
  TCheckoutSendSuccess,
  TCheckoutSendError,
  TCheckoutAddOrderId,
} from './action-types';

export const checkoutSend: TCheckoutSend = (data) => ({
  type: CHECKOUT_SEND,
  payload: data,
});

export const checkoutSendPending: TCheckoutSendPending = () => ({
  type: CHECKOUT_SEND_PENDING,
});

export const checkoutSendSuccess: TCheckoutSendSuccess = (payload) => ({
  type: CHECKOUT_SEND_SUCCESS,
  payload,
});

export const checkoutSendError: TCheckoutSendError = ({ status }) => ({
  type: CHECKOUT_SEND_ERROR,
  payload: { status },
});

export const checkoutAddOrderId: TCheckoutAddOrderId = (id) => ({
  type: CHECKOUT_SEND_ERROR,
  payload: id,
});
