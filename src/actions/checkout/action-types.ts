// Modules Types
import { ActionCreator } from '@type/actions';

// Constants
import {
  CHECKOUT_SEND,
  CHECKOUT_SEND_PENDING,
  CHECKOUT_SEND_SUCCESS,
  CHECKOUT_SEND_ERROR,
  CHECKOUT_ADD_ORDER_ID,
} from '@constants/checkout';

// get restaurants
interface TGetRestaurantsArgs {
  cityId: string;
  lat?: number;
  lng?: number;
  filters?: string[];
  search?: string;
}
export type TCheckoutSend = ActionCreator<typeof CHECKOUT_SEND, TGetRestaurantsArgs>;

export type TCheckoutSendPending = ActionCreator<typeof CHECKOUT_SEND_PENDING>;

export type TCheckoutSendSuccess = ActionCreator<
  typeof CHECKOUT_SEND_SUCCESS,
  { status: number; id: string; paymentResult: Record<string, string> }
>;

export type TCheckoutSendError = ActionCreator<typeof CHECKOUT_SEND_ERROR, { status: number }>;

export type TCheckoutAddOrderId = ActionCreator<typeof CHECKOUT_ADD_ORDER_ID, { id: string }>;
