/* eslint-disable @typescript-eslint/indent */
// Constants
import {
  CHANGE_ADDRESS_MODAL_SHOW,
  CHANGE_ADDRESS_MODAL_HIDE_2ND_TIME,
  CHANGE_BASKET_MODAL_SHOW,
  CHANGE_PROMOCODE_MODAL_SHOW,
} from '@constants/checkout-modals';
// Types
import {
  TChangeAddressModalShow,
  TChangeAddressModalHide2ndTime,
  TChangeBasketModalShow,
  TChangePromocodeModalShow,
} from './action-types';

export const changeAddressModalShow: TChangeAddressModalShow = (data) => ({
  type: CHANGE_ADDRESS_MODAL_SHOW,
  payload: data,
});

export const changeAddressModalHide2ndTime: TChangeAddressModalHide2ndTime = (data) => ({
  type: CHANGE_ADDRESS_MODAL_HIDE_2ND_TIME,
  payload: data,
});

export const changeBasketModalShow: TChangeBasketModalShow = (data) => ({
  type: CHANGE_BASKET_MODAL_SHOW,
  payload: data,
});

export const changePromocodeModalShow: TChangePromocodeModalShow = (data) => ({
  type: CHANGE_PROMOCODE_MODAL_SHOW,
  payload: data,
});
