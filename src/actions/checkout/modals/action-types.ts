/* eslint-disable @typescript-eslint/indent */
// Modules Types
import { ActionCreator } from '@type/actions';
// Constants
import {
  CHANGE_ADDRESS_MODAL_SHOW,
  CHANGE_ADDRESS_MODAL_HIDE_2ND_TIME,
  CHANGE_BASKET_MODAL_SHOW,
  CHANGE_PROMOCODE_MODAL_SHOW,
} from '@constants/checkout-modals';

export type TChangeAddressModalShow = ActionCreator<
  typeof CHANGE_ADDRESS_MODAL_SHOW,
  {
    show: boolean;
  }
>;

// A fix for disabling appearing "mismatch address error" again on redirect to restaurants page
export type TChangeAddressModalHide2ndTime = ActionCreator<
  typeof CHANGE_ADDRESS_MODAL_HIDE_2ND_TIME,
  {
    doNotShow2ndTime: boolean;
  }
>;

export type TChangeBasketModalShow = ActionCreator<typeof CHANGE_BASKET_MODAL_SHOW>;

export type TChangePromocodeModalShow = ActionCreator<typeof CHANGE_PROMOCODE_MODAL_SHOW>;
