/* eslint-disable @typescript-eslint/indent */
// Modules Types
import { ActionCreator } from '@type/actions';

import { TContent } from '@/type/content';
// Constants
import { PUSH_NOTIFICATION, REMOVE_NOTIFICATION } from '@constants/notifications';

export type TPushNotification = ActionCreator<
  typeof PUSH_NOTIFICATION,
  {
    type: 'success' | 'error' | 'info';
    content: TContent;
    duration?: number;
  }
>;

export type TRemoveNotification = ActionCreator<typeof REMOVE_NOTIFICATION, { id: string }>;
