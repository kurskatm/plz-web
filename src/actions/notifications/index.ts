// Constants
import { PUSH_NOTIFICATION, REMOVE_NOTIFICATION } from '@constants/notifications';
// Types
import { TPushNotification, TRemoveNotification } from './action-types';

// Profile info
export const pushNotification: TPushNotification = (payload) => ({
  type: PUSH_NOTIFICATION,
  payload,
});

export const removeNotification: TRemoveNotification = (payload) => ({
  type: REMOVE_NOTIFICATION,
  payload,
});
