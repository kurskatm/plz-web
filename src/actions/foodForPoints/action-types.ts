// Modules Types
import { ActionCreator } from '@type/actions';
import { TFoodForPoints } from '@type/foodForPoints';

// Constants
import {
  FOOD_FOR_POINTS_FETCH,
  FOOD_FOR_POINTS_PENDING,
  FOOD_FOR_POINTS_SUCCESS,
  FOOD_FOR_POINTS_ERROR,
  FOOD_FOR_POINTS_SAVE,
  FOOD_FOR_POINTS_RESET,
} from '@constants/foodForPoints';

interface TGetFoodForPointsArgs {
  cityId: string;
  lat?: string;
  lng?: string;
}

// get free products
export type TFoodForPointsFetch = ActionCreator<
  typeof FOOD_FOR_POINTS_FETCH,
  TGetFoodForPointsArgs
>;

export type TFoodForPointsPending = ActionCreator<typeof FOOD_FOR_POINTS_PENDING>;

export type TFoodForPointsSuccess = ActionCreator<
  typeof FOOD_FOR_POINTS_SUCCESS,
  {
    status: number;
  }
>;

export type TFoodForPointsError = ActionCreator<typeof FOOD_FOR_POINTS_ERROR, { status: number }>;
export type TFoodForPointsSave = ActionCreator<typeof FOOD_FOR_POINTS_SAVE, TFoodForPoints>;
export type TFoodForPointsReset = ActionCreator<typeof FOOD_FOR_POINTS_RESET>;
