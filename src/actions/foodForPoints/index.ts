// Constants
import {
  FOOD_FOR_POINTS_FETCH,
  FOOD_FOR_POINTS_SAVE,
  FOOD_FOR_POINTS_PENDING,
  FOOD_FOR_POINTS_SUCCESS,
  FOOD_FOR_POINTS_ERROR,
  FOOD_FOR_POINTS_RESET,
} from '@constants/foodForPoints';
// Types
import {
  TFoodForPointsFetch,
  TFoodForPointsPending,
  TFoodForPointsSuccess,
  TFoodForPointsError,
  TFoodForPointsSave,
  TFoodForPointsReset,
} from './action-types';

// Free products
export const foodForPointsFetch: TFoodForPointsFetch = (data) => ({
  type: FOOD_FOR_POINTS_FETCH,
  payload: data,
});

export const foodForPointsSave: TFoodForPointsSave = (data) => ({
  type: FOOD_FOR_POINTS_SAVE,
  payload: data,
});

export const foodForPointsPending: TFoodForPointsPending = () => ({
  type: FOOD_FOR_POINTS_PENDING,
});

export const foodForPointsSuccess: TFoodForPointsSuccess = ({ status }) => ({
  type: FOOD_FOR_POINTS_SUCCESS,
  payload: { status },
});

export const foodForPointsError: TFoodForPointsError = ({ status }) => ({
  type: FOOD_FOR_POINTS_ERROR,
  payload: { status },
});

export const foodForPointsReset: TFoodForPointsReset = () => ({
  type: FOOD_FOR_POINTS_RESET,
});
