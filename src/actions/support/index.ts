// Constants
import {
  SUPPORT_TOPICS_FETCH,
  SUPPORT_TOPICS_PENDING,
  SUPPORT_TOPICS_SUCCESS,
  SUPPORT_TOPICS_ERROR,
  SUPPORT_TOPICS_SAVE,
  SUPPORT_MESSAGE_SEND,
  SUPPORT_MESSAGE_SENT,
  SUPPORT_MESSAGE_SUCCESS,
  SUPPORT_MESSAGE_ERROR,
  SUPPORT_MESSAGE_PENDING,
} from '@constants/support';
// Types
import {
  TSupportTopicsFetch,
  TSupportTopicsPending,
  TSupportTopicsSuccess,
  TSupportTopicsError,
  TSupportTopicsSave,
  TsupportMessageSend,
  TsupportMessageSent,
} from './action-types';

// Support
export const supportTopicsFetch: TSupportTopicsFetch = (data) => ({
  type: SUPPORT_TOPICS_FETCH,
  payload: data,
});

export const supportTopicsSave: TSupportTopicsSave = (data) => ({
  type: SUPPORT_TOPICS_SAVE,
  payload: data,
});

export const supportTopicsPending: TSupportTopicsPending = () => ({
  type: SUPPORT_TOPICS_PENDING,
});

export const supportTopicsSuccess: TSupportTopicsSuccess = ({ status }) => ({
  type: SUPPORT_TOPICS_SUCCESS,
  payload: { status },
});

export const supportTopicsError: TSupportTopicsError = ({ status }) => ({
  type: SUPPORT_TOPICS_ERROR,
  payload: { status },
});

export const supportMessagePending: TSupportTopicsPending = () => ({
  type: SUPPORT_MESSAGE_PENDING,
});

export const supportMessageSuccess: TSupportTopicsSuccess = ({ status }) => ({
  type: SUPPORT_MESSAGE_SUCCESS,
  payload: { status },
});

export const supportMessageError: TSupportTopicsError = ({ status }) => ({
  type: SUPPORT_MESSAGE_ERROR,
  payload: { status },
});

export const supportMessageSend: TsupportMessageSend = (data) => ({
  type: SUPPORT_MESSAGE_SEND,
  payload: data,
});

export const supportMessageSent: TsupportMessageSent = (data) => ({
  type: SUPPORT_MESSAGE_SENT,
  payload: data,
});
