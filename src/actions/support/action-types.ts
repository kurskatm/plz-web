// Modules Types
import { ActionCreator } from '@type/actions';

// Constants
import {
  SUPPORT_TOPICS_FETCH,
  SUPPORT_TOPICS_PENDING,
  SUPPORT_TOPICS_SUCCESS,
  SUPPORT_TOPICS_ERROR,
  SUPPORT_TOPICS_SAVE,
  SUPPORT_MESSAGE_SEND,
  SUPPORT_MESSAGE_SENT,
} from '@constants/support';

// get restaurants
export type TSupportTopicsFetch = ActionCreator<typeof SUPPORT_TOPICS_FETCH>;

export type TSupportTopicsPending = ActionCreator<typeof SUPPORT_TOPICS_PENDING>;

export type TSupportTopicsSuccess = ActionCreator<
  typeof SUPPORT_TOPICS_SUCCESS,
  { status: number }
>;

export type TSupportTopicsError = ActionCreator<typeof SUPPORT_TOPICS_ERROR, { status: number }>;
export type TSupportTopicsSave = ActionCreator<typeof SUPPORT_TOPICS_SAVE>;
export type TsupportMessageSend = ActionCreator<typeof SUPPORT_MESSAGE_SEND>;
export type TsupportMessageSent = ActionCreator<typeof SUPPORT_MESSAGE_SENT>;
