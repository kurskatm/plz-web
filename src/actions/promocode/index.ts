// Constants
import {
  PROMOCODE_VALIDATE,
  PROMOCODE_SAVE,
  PROMOCODE_PENDING,
  PROMOCODE_SUCCESS,
  PROMOCODE_ERROR,
  PROMOCODE_RESET,
} from '@constants/promocode';
// Types
import {
  TPromocodeValidate,
  TPromocodePending,
  TPromocodeSuccess,
  TPromocodeError,
  TPromocodeSave,
  TPromocodeReset,
} from './action-types';

// Restaurants
export const promocodeFetch: TPromocodeValidate = (data) => ({
  type: PROMOCODE_VALIDATE,
  payload: data,
});

export const promocodeSave: TPromocodeSave = (data) => ({
  type: PROMOCODE_SAVE,
  payload: data,
});

export const promocodePending: TPromocodePending = () => ({
  type: PROMOCODE_PENDING,
});

export const promocodeSuccess: TPromocodeSuccess = ({ status }) => ({
  type: PROMOCODE_SUCCESS,
  payload: { status },
});

export const promocodeError: TPromocodeError = ({ status }) => ({
  type: PROMOCODE_ERROR,
  payload: { status },
});

export const promocodeReset: TPromocodeReset = () => ({
  type: PROMOCODE_RESET,
});
