// Modules Types
import { ActionCreator } from '@type/actions';
import { TPromocodeResult } from '@type/promocode';

// Constants
import {
  PROMOCODE_VALIDATE,
  PROMOCODE_PENDING,
  PROMOCODE_SUCCESS,
  PROMOCODE_ERROR,
  PROMOCODE_SAVE,
  PROMOCODE_RESET,
} from '@constants/promocode';

export type TPromocodeValidate = ActionCreator<typeof PROMOCODE_VALIDATE, string>;

export type TPromocodePending = ActionCreator<typeof PROMOCODE_PENDING>;

export type TPromocodeSuccess = ActionCreator<typeof PROMOCODE_SUCCESS, { status: number }>;

export type TPromocodeError = ActionCreator<typeof PROMOCODE_ERROR, { status: number }>;
export type TPromocodeSave = ActionCreator<typeof PROMOCODE_SAVE, TPromocodeResult>;
export type TPromocodeReset = ActionCreator<typeof PROMOCODE_RESET>;
