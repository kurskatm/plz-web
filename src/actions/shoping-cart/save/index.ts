// Constants
import {
  SHOPING_CART_SAVE,
  SHOPING_CART_SAVE_PENDING,
  SHOPING_CART_SAVE_SUCCESS,
  SHOPING_CART_SAVE_ERROR,
  SHOPING_CART_SAVE_RESET_UI,
} from '@constants/shoping-cart';
// Types
import {
  TShopingCartSave,
  TShopingCartSavePending,
  TShopingCartSaveSuccess,
  TShopingCartSaveError,
  TShopingCartSaveResetUi,
} from './action-types';

export const shopingCartSave: TShopingCartSave = (data) => ({
  type: SHOPING_CART_SAVE,
  payload: data,
});

export const shopingCartSavePending: TShopingCartSavePending = () => ({
  type: SHOPING_CART_SAVE_PENDING,
});

export const shopingCartSaveSuccess: TShopingCartSaveSuccess = ({ status, data }) => ({
  type: SHOPING_CART_SAVE_SUCCESS,
  payload: { status, data },
});

export const shopingCartSaveError: TShopingCartSaveError = ({ status, details }) => ({
  type: SHOPING_CART_SAVE_ERROR,
  payload: { status, details },
});

export const shopingCartSaveResetUi: TShopingCartSaveResetUi = () => ({
  type: SHOPING_CART_SAVE_RESET_UI,
});
