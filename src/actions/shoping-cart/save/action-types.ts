// Main Types
import { ActionCreator } from '@type/actions';
import { TShopingCartSaveData } from '@type/shoping-cart';
// Constants
import {
  SHOPING_CART_SAVE,
  SHOPING_CART_SAVE_PENDING,
  SHOPING_CART_SAVE_SUCCESS,
  SHOPING_CART_SAVE_ERROR,
  SHOPING_CART_SAVE_RESET_UI,
} from '@constants/shoping-cart';

export interface TShopingCartSaveArgs {
  cartId: string;
  userId?: string;
  data: TShopingCartSaveData;
  lat?: number;
  lng?: number;
}
export type TShopingCartSave = ActionCreator<typeof SHOPING_CART_SAVE, TShopingCartSaveArgs>;

export type TShopingCartSavePending = ActionCreator<typeof SHOPING_CART_SAVE_PENDING>;
export type TShopingCartSaveSuccess = ActionCreator<
  typeof SHOPING_CART_SAVE_SUCCESS,
  { status: number; data: TShopingCartSaveData }
>;
export type TShopingCartSaveError = ActionCreator<
  typeof SHOPING_CART_SAVE_ERROR,
  { status: number; details?: { errorMessage: string } }
>;

export type TShopingCartSaveResetUi = ActionCreator<typeof SHOPING_CART_SAVE_RESET_UI>;
