// Constants
import { SHOPING_CART_RESET } from '@constants/shoping-cart';
// Types
import { TShopingCartReset } from './action-types';

export const shopingCartReset: TShopingCartReset = () => ({
  type: SHOPING_CART_RESET,
});
