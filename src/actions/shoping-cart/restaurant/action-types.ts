// Main Types
import { ActionCreator } from '@type/actions';
import { TRestaurantCard } from '@type/restaurants/restaurant-card';
// Constants
import {
  SHOPCART_RESTAURANT_FETCH,
  SHOPCART_RESTAURANT_SAVE,
  SHOPCART_RESTAURANT_RESET,
  SHOPCART_RESTAURANT_FETCH_PENDING,
  SHOPCART_RESTAURANT_FETCH_SUCCESS,
  SHOPCART_RESTAURANT_FETCH_ERROR,
} from '@constants/shoping-cart-rest';

export type TRestaurantShopCartRestFetch = ActionCreator<
  typeof SHOPCART_RESTAURANT_FETCH,
  { restaurantId: string }
>;

export type TRestaurantShopCartRestSave = ActionCreator<
  typeof SHOPCART_RESTAURANT_SAVE,
  TRestaurantCard
>;

export type TRestaurantShopCartRestReset = ActionCreator<typeof SHOPCART_RESTAURANT_RESET>;

export type TRestaurantShopCartRestFetchPending = ActionCreator<
  typeof SHOPCART_RESTAURANT_FETCH_PENDING
>;

export type TRestaurantShopCartRestFetchSuccess = ActionCreator<
  typeof SHOPCART_RESTAURANT_FETCH_SUCCESS,
  { status: number }
>;

export type TRestaurantShopCartRestFetchError = ActionCreator<
  typeof SHOPCART_RESTAURANT_FETCH_ERROR,
  { status: number }
>;
