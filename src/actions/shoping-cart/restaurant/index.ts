// Constants
import {
  SHOPCART_RESTAURANT_FETCH,
  SHOPCART_RESTAURANT_SAVE,
  SHOPCART_RESTAURANT_RESET,
  SHOPCART_RESTAURANT_FETCH_PENDING,
  SHOPCART_RESTAURANT_FETCH_SUCCESS,
  SHOPCART_RESTAURANT_FETCH_ERROR,
} from '@constants/shoping-cart-rest';
// Types
import {
  TRestaurantShopCartRestFetch,
  TRestaurantShopCartRestSave,
  TRestaurantShopCartRestReset,
  TRestaurantShopCartRestFetchPending,
  TRestaurantShopCartRestFetchSuccess,
  TRestaurantShopCartRestFetchError,
} from './action-types';

export const restaurantShopCartRestFetch: TRestaurantShopCartRestFetch = (data) => ({
  type: SHOPCART_RESTAURANT_FETCH,
  payload: data,
});

export const restaurantShopCartRestSave: TRestaurantShopCartRestSave = (data) => ({
  type: SHOPCART_RESTAURANT_SAVE,
  payload: data,
});

export const restaurantShopCartRestReset: TRestaurantShopCartRestReset = () => ({
  type: SHOPCART_RESTAURANT_RESET,
});

export const restaurantShopCartRestFetchPending: TRestaurantShopCartRestFetchPending = () => ({
  type: SHOPCART_RESTAURANT_FETCH_PENDING,
});

export const restaurantShopCartRestFetchSuccess: TRestaurantShopCartRestFetchSuccess = ({
  status,
}) => ({
  type: SHOPCART_RESTAURANT_FETCH_SUCCESS,
  payload: { status },
});

export const restaurantShopCartRestFetchError: TRestaurantShopCartRestFetchError = ({
  status,
}) => ({
  type: SHOPCART_RESTAURANT_FETCH_ERROR,
  payload: { status },
});
