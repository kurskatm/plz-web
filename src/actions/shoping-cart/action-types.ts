// Main Types
import { ActionCreator } from '@type/actions';
// Constants
import { SHOPING_CART_RESET } from '@constants/shoping-cart';

export type TShopingCartReset = ActionCreator<typeof SHOPING_CART_RESET>;
