// Main Types
import { ActionCreator } from '@type/actions';
import {
  TRestaurantShopingCart,
  TShopingCartProduct,
  TShopingCartOptions,
} from '@type/shoping-cart';
// Constants
import {
  SHOPING_CART_FETCH,
  SHOPING_CART_FETCH_SAVE,
  SHOPING_CART_FETCH_PENDING,
  SHOPING_CART_FETCH_SUCCESS,
  SHOPING_CART_FETCH_ERROR,
} from '@constants/shoping-cart';

export interface TShopingCartSaveArgs {
  cartId?: string;
  userId?: string;
  lat?: number;
  lng?: number;
}
export interface TShopingCartStoresArgs {
  restId?: string;
}
export type TShopingCartFetch = ActionCreator<typeof SHOPING_CART_FETCH, TShopingCartSaveArgs>;

// eslint-disable-next-line max-len
export type TShopingCartStoresFetch = ActionCreator<
  typeof SHOPING_CART_FETCH,
  TShopingCartStoresArgs
>;

export interface TShopingCartFetchSaveData {
  restaurant: TRestaurantShopingCart;
  products: TShopingCartProduct[];
  options: TShopingCartOptions;
}
export type TShopingCartFetchSave = ActionCreator<
  typeof SHOPING_CART_FETCH_SAVE,
  TShopingCartFetchSaveData
>;

export type TShopingCartFetchPending = ActionCreator<typeof SHOPING_CART_FETCH_PENDING>;
export type TShopingCartFetchSuccess = ActionCreator<
  typeof SHOPING_CART_FETCH_SUCCESS,
  { status: number }
>;
export type TShopingCartFetchError = ActionCreator<
  typeof SHOPING_CART_FETCH_ERROR,
  { status: number }
>;
