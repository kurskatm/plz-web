// Constants
import {
  SHOPING_CART_FETCH,
  SHOPING_CART_FETCH_SAVE,
  SHOPING_CART_FETCH_PENDING,
  SHOPING_CART_FETCH_SUCCESS,
  SHOPING_CART_FETCH_ERROR,
  SHOPING_CART_STORES_FETCH_SUCCESS,
} from '@constants/shoping-cart';
// Types
import {
  TShopingCartFetch,
  TShopingCartFetchSave,
  TShopingCartFetchPending,
  TShopingCartFetchSuccess,
  TShopingCartFetchError,
} from './action-types';

export const shopingCartFetch: TShopingCartFetch = (data) => ({
  type: SHOPING_CART_FETCH,
  payload: data,
});

export const shopingCartFetchSave: TShopingCartFetchSave = (data) => ({
  type: SHOPING_CART_FETCH_SAVE,
  payload: data,
});

export const shopingCartFetchPending: TShopingCartFetchPending = () => ({
  type: SHOPING_CART_FETCH_PENDING,
});

export const shopingCartFetchSuccess: TShopingCartFetchSuccess = ({ status }) => ({
  type: SHOPING_CART_FETCH_SUCCESS,
  payload: { status },
});

export const shopingCartStoresFetchSuccess: TShopingCartFetchSuccess = (data) => ({
  type: SHOPING_CART_STORES_FETCH_SUCCESS,
  payload: data,
});

export const shopingCartFetchError: TShopingCartFetchError = ({ status }) => ({
  type: SHOPING_CART_FETCH_ERROR,
  payload: { status },
});
