// Main Types
import { ActionCreator } from '@type/actions';
import { TShopingCartModel } from '@models/shoping-cart/types';
// Constants
import { UPDATE_SHOPING_CART, UPDATE_SHOPING_CART_CUTLERY } from '@constants/shoping-cart';

export type TUpdateShopingCart = ActionCreator<typeof UPDATE_SHOPING_CART, TShopingCartModel>;

export type TUpdateShopingCartCutlery = ActionCreator<typeof UPDATE_SHOPING_CART_CUTLERY, number>;
