// Constants
import { UPDATE_SHOPING_CART, UPDATE_SHOPING_CART_CUTLERY } from '@constants/shoping-cart';
// Types
import { TUpdateShopingCart, TUpdateShopingCartCutlery } from './action-types';

export const shopingCartUpdate: TUpdateShopingCart = (data) => ({
  type: UPDATE_SHOPING_CART,
  payload: data,
});

export const shopingCartUpdateCutlery: TUpdateShopingCartCutlery = (data) => ({
  type: UPDATE_SHOPING_CART_CUTLERY,
  payload: data,
});
