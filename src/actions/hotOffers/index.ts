// Constants
import {
  HOT_OFFERS_FETCH,
  HOT_OFFERS_SAVE,
  HOT_OFFERS_PENDING,
  HOT_OFFERS_SUCCESS,
  HOT_OFFERS_ERROR,
  HOT_OFFERS_RESET,
} from '@constants/hotOffers';
// Types
import {
  THotOffersFetch,
  THotOffersPending,
  THotOffersSuccess,
  THotOffersError,
  THotOffersSave,
  THotOffersReset,
} from './action-types';

// Restaurants
export const hotOffersFetch: THotOffersFetch = (data) => ({
  type: HOT_OFFERS_FETCH,
  payload: data,
});

export const hotOffersSave: THotOffersSave = (data) => ({
  type: HOT_OFFERS_SAVE,
  payload: data,
});

export const hotOffersReset: THotOffersReset = () => ({
  type: HOT_OFFERS_RESET,
});

export const hotOffersPending: THotOffersPending = () => ({
  type: HOT_OFFERS_PENDING,
});

export const hotOffersSuccess: THotOffersSuccess = ({ status }) => ({
  type: HOT_OFFERS_SUCCESS,
  payload: { status },
});

export const hotOffersError: THotOffersError = ({ status }) => ({
  type: HOT_OFFERS_ERROR,
  payload: { status },
});
