/* eslint-disable @typescript-eslint/indent */
// Modules Types
import { ActionCreator } from '@type/actions';
import { THotOffersItem, TGetHotOffersArgs } from '@type/hotOffers';

// Constants
import {
  HOT_OFFERS_FETCH,
  HOT_OFFERS_PENDING,
  HOT_OFFERS_SUCCESS,
  HOT_OFFERS_ERROR,
  HOT_OFFERS_SAVE,
  HOT_OFFERS_RESET,
} from '@constants/hotOffers';

// get hot offers
export type THotOffersFetch = ActionCreator<typeof HOT_OFFERS_FETCH, TGetHotOffersArgs>;

export type THotOffersPending = ActionCreator<typeof HOT_OFFERS_PENDING>;

export type THotOffersSuccess = ActionCreator<typeof HOT_OFFERS_SUCCESS, { status: number }>;

export type THotOffersError = ActionCreator<typeof HOT_OFFERS_ERROR, { status: number }>;
export type THotOffersSave = ActionCreator<typeof HOT_OFFERS_SAVE, THotOffersItem>;

export type THotOffersReset = ActionCreator<typeof HOT_OFFERS_RESET>;
