/* eslint-disable @typescript-eslint/indent */
// Modules Types
import { ActionCreator } from '@type/actions';
import { TReviewsMainList, TReviewsMainFetchArgs } from '@type/reviews-main';

// Constants
import {
  REVIEWS_FETCH,
  REVIEWS_PENDING,
  REVIEWS_SUCCESS,
  REVIEWS_ERROR,
  REVIEWS_SAVE,
  REVIEWS_RESET,
} from '@constants/reviews';

// get reviews
export type TReviewsFetch = ActionCreator<typeof REVIEWS_FETCH, TReviewsMainFetchArgs>;

export type TReviewsPending = ActionCreator<typeof REVIEWS_PENDING>;

export type TReviewsSuccess = ActionCreator<typeof REVIEWS_SUCCESS, { status: number }>;

export type TReviewsError = ActionCreator<typeof REVIEWS_ERROR, { status: number }>;
export type TReviewsSave = ActionCreator<typeof REVIEWS_SAVE, TReviewsMainList>;
export type TReviewsReset = ActionCreator<typeof REVIEWS_RESET>;
