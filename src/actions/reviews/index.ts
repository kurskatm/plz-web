// Constants
import {
  REVIEWS_FETCH,
  REVIEWS_PENDING,
  REVIEWS_SUCCESS,
  REVIEWS_ERROR,
  REVIEWS_SAVE,
  REVIEWS_RESET,
} from '@constants/reviews';
// Types
import {
  TReviewsFetch,
  TReviewsPending,
  TReviewsSuccess,
  TReviewsError,
  TReviewsSave,
  TReviewsReset,
} from './action-types';

// Reviews
export const reviewsFetch: TReviewsFetch = (data) => ({
  type: REVIEWS_FETCH,
  payload: data,
});

export const reviewsSave: TReviewsSave = (data) => ({
  type: REVIEWS_SAVE,
  payload: data,
});

export const reviewsPending: TReviewsPending = () => ({
  type: REVIEWS_PENDING,
});

export const reviewsSuccess: TReviewsSuccess = ({ status }) => ({
  type: REVIEWS_SUCCESS,
  payload: { status },
});

export const reviewsError: TReviewsError = ({ status }) => ({
  type: REVIEWS_ERROR,
  payload: { status },
});

export const reviewsReset: TReviewsReset = () => ({
  type: REVIEWS_RESET,
});
