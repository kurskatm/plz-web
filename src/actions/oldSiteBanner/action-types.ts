/* eslint-disable @typescript-eslint/indent */
// Modules Types
import { ActionCreator } from '@type/actions';
// Constants
import { SHOW_FULL_OLD_SITE_BANNER } from '@constants/oldSiteBanner';

export type TShowFullOldSiteBanner = ActionCreator<typeof SHOW_FULL_OLD_SITE_BANNER, boolean>;
