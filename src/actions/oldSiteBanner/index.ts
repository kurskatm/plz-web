// Constants
import { SHOW_FULL_OLD_SITE_BANNER } from '@constants/oldSiteBanner';
// Types
import { TShowFullOldSiteBanner } from './action-types';

export const showFullBanner: TShowFullOldSiteBanner = (data) => ({
  type: SHOW_FULL_OLD_SITE_BANNER,
  payload: data,
});
