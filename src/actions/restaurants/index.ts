// Constants
import {
  RESTAURANTS_FETCH,
  RESTAURANTS_SAVE,
  RESTAURANTS_PENDING,
  RESTAURANTS_SUCCESS,
  RESTAURANTS_ERROR,
} from '@constants/restaurants';
// Types
import {
  TRestaurantsFetch,
  TRestaurantsPending,
  TRestaurantsSuccess,
  TRestaurantsError,
  TRestaurantsSave,
} from './action-types';

// Restaurants
export const restaurantsFetch: TRestaurantsFetch = (data) => ({
  type: RESTAURANTS_FETCH,
  payload: data,
});

export const restaurantsSave: TRestaurantsSave = (data) => ({
  type: RESTAURANTS_SAVE,
  payload: data,
});

export const restaurantsPending: TRestaurantsPending = () => ({
  type: RESTAURANTS_PENDING,
});

export const restaurantsSuccess: TRestaurantsSuccess = ({ status }) => ({
  type: RESTAURANTS_SUCCESS,
  payload: { status },
});

export const restaurantsError: TRestaurantsError = ({ status }) => ({
  type: RESTAURANTS_ERROR,
  payload: { status },
});
