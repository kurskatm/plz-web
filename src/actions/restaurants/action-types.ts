// Modules Types
import { ActionCreator } from '@type/actions';
import { TRestaurantItem } from '@type/restaurant';

// Constants
import {
  RESTAURANTS_FETCH,
  RESTAURANTS_PENDING,
  RESTAURANTS_SUCCESS,
  RESTAURANTS_ERROR,
  RESTAURANTS_SAVE,
} from '@constants/restaurants';

// get restaurants
interface TGetRestaurantsArgs {
  cityId: string;
  lat?: number;
  lng?: number;
  filters?: string[];
  search?: string;
}
export type TRestaurantsFetch = ActionCreator<typeof RESTAURANTS_FETCH, TGetRestaurantsArgs>;

export type TRestaurantsPending = ActionCreator<typeof RESTAURANTS_PENDING>;

export type TRestaurantsSuccess = ActionCreator<typeof RESTAURANTS_SUCCESS, { status: number }>;

export type TRestaurantsError = ActionCreator<typeof RESTAURANTS_ERROR, { status: number }>;
export type TRestaurantsSave = ActionCreator<typeof RESTAURANTS_SAVE, TRestaurantItem>;
