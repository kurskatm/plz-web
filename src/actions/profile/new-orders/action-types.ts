/* eslint-disable @typescript-eslint/indent */
// Modules Types
import { ActionCreator } from '@type/actions';
// Main types
import { TUserOrdersData } from '@type/profile';
// Constants
import {
  PROFILE_GET_NEW_ORDERS,
  PROFILE_GET_NEW_ORDERS_PENDING,
  PROFILE_GET_NEW_ORDERS_SUCCESS,
  PROFILE_GET_NEW_ORDERS_ERROR,
  PROFILE_GET_NEW_ORDERS_SAVE_DATA,
  PROFILE_GET_NEW_ORDERS_RESET_DATA,
} from '@constants/profile/orders/new-orders';

export type TProfileGetNewOrders = ActionCreator<
  typeof PROFILE_GET_NEW_ORDERS,
  {
    access_token?: string;
    count: number;
    after: string | null;
  }
>;

export type TProfileGetNewOrdersPending = ActionCreator<typeof PROFILE_GET_NEW_ORDERS_PENDING>;

export type TProfileGetNewOrdersSuccess = ActionCreator<
  typeof PROFILE_GET_NEW_ORDERS_SUCCESS,
  { status: string | number }
>;

export type TProfileGetNewOrdersError = ActionCreator<
  typeof PROFILE_GET_NEW_ORDERS_ERROR,
  { status: string | number }
>;

export type TProfileGetNewOrdersSave = ActionCreator<
  typeof PROFILE_GET_NEW_ORDERS_SAVE_DATA,
  {
    data: TUserOrdersData;
    after: string;
  }
>;

export type TProfileGetNewOrdersReset = ActionCreator<typeof PROFILE_GET_NEW_ORDERS_RESET_DATA>;
