// Constants
import {
  PROFILE_GET_NEW_ORDERS,
  PROFILE_GET_NEW_ORDERS_PENDING,
  PROFILE_GET_NEW_ORDERS_SUCCESS,
  PROFILE_GET_NEW_ORDERS_ERROR,
  PROFILE_GET_NEW_ORDERS_SAVE_DATA,
  PROFILE_GET_NEW_ORDERS_RESET_DATA,
} from '@constants/profile/orders/new-orders';
// Types
import {
  TProfileGetNewOrders,
  TProfileGetNewOrdersPending,
  TProfileGetNewOrdersSuccess,
  TProfileGetNewOrdersError,
  TProfileGetNewOrdersSave,
  TProfileGetNewOrdersReset,
} from './action-types';

export const profileGetNewOrders: TProfileGetNewOrders = (payload) => ({
  type: PROFILE_GET_NEW_ORDERS,
  payload,
});

export const profileGetNewOrdersPending: TProfileGetNewOrdersPending = () => ({
  type: PROFILE_GET_NEW_ORDERS_PENDING,
});

export const profileGetNewOrdersSuccess: TProfileGetNewOrdersSuccess = (payload) => ({
  type: PROFILE_GET_NEW_ORDERS_SUCCESS,
  payload,
});

export const profileGetNewOrdersError: TProfileGetNewOrdersError = (payload) => ({
  type: PROFILE_GET_NEW_ORDERS_ERROR,
  payload,
});

export const profileGetNewOrdersSave: TProfileGetNewOrdersSave = (payload) => ({
  type: PROFILE_GET_NEW_ORDERS_SAVE_DATA,
  payload,
});

export const profileGetNewOrdersReset: TProfileGetNewOrdersReset = () => ({
  type: PROFILE_GET_NEW_ORDERS_RESET_DATA,
});
