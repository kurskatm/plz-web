// Constants
import {
  PROFILE_AUTHORIZED,
  PROFILE_UNAUTHORIZED,
  PROFILE_RESET_ALL_UI_MODELS,
  // City
  PROFILE_CITY_UPDATE,
  // Delivery
  PROFILE_UPDATE_ADDRESS,
  PROFILE_UPDATE_PICKUP,
  // User Geocode
  PROFILE_GET_ADDRESSES_GEOCODE,
  PROFILE_SAVE_ADDRESSES_GEOCODE,
  PROFILE_GET_ADDRESSES_GEOCODE_PENDING,
  PROFILE_GET_ADDRESSES_GEOCODE_SUCCESS,
  PROFILE_GET_ADDRESSES_GEOCODE_ERROR,
  PROFILE_RESET_ADDRESSES_GEOCODE,
  // User favorites
  PROFILE_GET_FAVORITE,
  PROFILE_GET_FAVORITE_RESTS,
  PROFILE_GET_FAVORITE_RESTS_PENDING,
  PROFILE_GET_FAVORITE_RESTS_SUCCESS,
  PROFILE_GET_FAVORITE_RESTS_ERROR,
  PROFILE_GET_FAVORITE_RESTS_SAVE_DATA,
  PROFILE_GET_FAVORITE_PRODUCTS,
  PROFILE_GET_FAVORITE_PRODUCTS_PENDING,
  PROFILE_GET_FAVORITE_PRODUCTS_SUCCESS,
  PROFILE_GET_FAVORITE_PRODUCTS_ERROR,
  PROFILE_GET_FAVORITE_PRODUCTS_SAVE_DATA,
  // Info
  PROFILE_GET_INFO,
  PROFILE_GET_INFO_PENDING,
  PROFILE_GET_INFO_SUCCESS,
  PROFILE_GET_INFO_ERROR,
  PROFILE_GET_INFO_SAVE_DATA,
  PROFILE_UPDATE_INFO,
  PROFILE_UPDATE_INFO_PENDING,
  PROFILE_UPDATE_INFO_SUCCESS,
  PROFILE_UPDATE_INFO_ERROR,
  // Reviews
  PROFILE_GET_REVIEWS,
  PROFILE_GET_REVIEWS_PENDING,
  PROFILE_GET_REVIEWS_SUCCESS,
  PROFILE_GET_REVIEWS_ERROR,
  PROFILE_GET_REVIEWS_SAVE_DATA,
  // FRIENDS
  PROFILE_GET_FRIENDS,
  PROFILE_GET_FRIENDS_PENDING,
  PROFILE_GET_FRIENDS_SUCCESS,
  PROFILE_GET_FRIENDS_ERROR,
  PROFILE_GET_FRIENDS_SAVE_DATA,
  // Orders
  PROFILE_GET_ORDERS,
  PROFILE_GET_ORDERS_PENDING,
  PROFILE_GET_ORDERS_SUCCESS,
  PROFILE_GET_ORDERS_ERROR,
  PROFILE_GET_ORDERS_SAVE_DATA,
  PROFILE_SHOW_ORDER_REVIEW_MODAL,
  // Last order without review
  PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW,
  PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_PENDING,
  PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_SUCCESS,
  PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_ERROR,
  PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_SAVE_DATA,
  // Orders Reviews
  PROFILE_SET_ORDER_REVIEW,
  PROFILE_SET_ORDER_REVIEW_PENDING,
  PROFILE_SET_ORDER_REVIEW_SUCCESS,
  PROFILE_SET_ORDER_REVIEW_ERROR,
  PROFILE_SAVE_ORDER_REVIEW_DATA,
  // Current Order
  PROFILE_GET_USER_ORDER,
  PROFILE_GET_USER_ORDER_PENDING,
  PROFILE_GET_USER_ORDER_SUCCESS,
  PROFILE_GET_USER_ORDER_ERROR,
  PROFILE_GET_USER_ORDER_SAVE_DATA,
  // Rests Info
  PROFILE_GET_RESTS_INFO,
  PROFILE_GET_RESTS_INFO_PENDING,
  PROFILE_GET_RESTS_INFO_SUCCESS,
  PROFILE_GET_RESTS_INFO_ERROR,
  PROFILE_GET_RESTS_INFO_SAVE_DATA,
} from '@constants/profile';
// Types
import {
  TProfileResetAllUiModels,
  // City
  TProfileCityUpdate,
  // Delivery
  TProfileUpdateAddress,
  TProfileUpdatePickup,
  // User Geocode
  TProfileGetAddressGeocode,
  TProfileGSaveAddressGeocode,
  TProfileGetAddressGeocodePending,
  TProfileGetAddressGeocodeSuccess,
  TProfileGetAddressGeocodeError,
  TProfileResetAddressGeocode,
  // User favorites
  TProfileGetFavoriteRests,
  TProfileGetFavoriteRestsPending,
  TProfileGetFavoriteRestsSuccess,
  TProfileGetFavoriteRestsError,
  TProfileGetFavoriteSaveData,
  TProfileGetFavoriteProductsSaveData,
  // Info
  TProfileInfo,
  TProfileGetInfo,
  TProfileInfoSuccess,
  TProfileInfoError,
  TProfileInfoUpdate,
  TProfileInfoUpdatePending,
  TProfileInfoUpdateSuccess,
  TProfileInfoUpdateError,
  // Reviews
  TProfileGetReviews,
  TProfileGetReviewsSuccess,
  TProfileGetReviewsError,
  TProfileGetReviewsPending,
  TProfileGetReviewsSaveData,
  // Friends
  TProfileGetFriends,
  TProfileGetFriendsSuccess,
  TProfileGetFriendsError,
  TProfileGetFriendsPending,
  TProfileGetFriendsSaveData,
  TProfileAuth,
  // Orders
  TProfileGetOrders,
  TProfileGetOrdersPending,
  TProfileGetOrdersSuccess,
  TProfileGetOrdersError,
  TProfileGetOrdersSaveData,
  TProfileShowOrdersReviewModal,
  // Last order without review
  TProfileGetLastOrderWithoutReview,
  TProfileGetLastOrderWithoutReviewPending,
  TProfileGetLastOrderWithoutReviewSuccess,
  TProfileGetLastOrderWithoutReviewError,
  TProfileGetLastOrderWithoutReviewSaveData,
  // Orders Reviews
  TProfileSetOrderReview,
  TProfileSetOrderReviewPending,
  TProfileSetOrderReviewSuccess,
  TProfileSetOrderReviewError,
  TProfileSaveOrderReviewData,
  // Current Order
  TProfileGetUserOrder,
  TProfileGetUserOrderPending,
  TProfileGetUserOrderSuccess,
  TProfileGetUserOrderError,
  TProfileGetUserOrderSaveData,
  // Rests info
  TProfileGetRestsInfo,
  TProfileGetRestsInfoPending,
  TProfileGetRestsInfoSuccess,
  TProfileGetRestsInfoError,
  TProfileGetRestsInfoSaveData,
} from './action-types';

export const profileResetAllUiModels: TProfileResetAllUiModels = () => ({
  type: PROFILE_RESET_ALL_UI_MODELS,
});

// City
export const profileCityUpdate: TProfileCityUpdate = (data) => ({
  type: PROFILE_CITY_UPDATE,
  payload: data,
});

// Delivery
export const profileUpdateAddress: TProfileUpdateAddress = (data) => ({
  type: PROFILE_UPDATE_ADDRESS,
  payload: data,
});

export const profileUpdatePickup: TProfileUpdatePickup = (pickup) => ({
  type: PROFILE_UPDATE_PICKUP,
  payload: pickup,
});

// User Geocode
export const profileGetAddressGeocode: TProfileGetAddressGeocode = (params) => ({
  type: PROFILE_GET_ADDRESSES_GEOCODE,
  payload: params,
});

export const profileGSaveAddressGeocode: TProfileGSaveAddressGeocode = (data) => ({
  type: PROFILE_SAVE_ADDRESSES_GEOCODE,
  payload: data,
});

export const profileGetAddressGeocodePending: TProfileGetAddressGeocodePending = () => ({
  type: PROFILE_GET_ADDRESSES_GEOCODE_PENDING,
});

export const profileGetAddressGeocodeSuccess: TProfileGetAddressGeocodeSuccess = ({ status }) => ({
  type: PROFILE_GET_ADDRESSES_GEOCODE_SUCCESS,
  payload: { status },
});

export const profileGetAddressGeocodeError: TProfileGetAddressGeocodeError = ({ status }) => ({
  type: PROFILE_GET_ADDRESSES_GEOCODE_ERROR,
  payload: { status },
});

export const profileResetAddressGeocode: TProfileResetAddressGeocode = () => ({
  type: PROFILE_RESET_ADDRESSES_GEOCODE,
});

// Get favorite rests
export const profileGetFavoriteRests: TProfileGetFavoriteRests = (data) => ({
  type: PROFILE_GET_FAVORITE_RESTS,
  payload: data,
});
export const profileGetFavoriteRestsPending: TProfileGetFavoriteRestsPending = () => ({
  type: PROFILE_GET_FAVORITE_RESTS_PENDING,
});

export const profileGetFavoriteRestsSuccess: TProfileGetFavoriteRestsSuccess = ({ status }) => ({
  type: PROFILE_GET_FAVORITE_RESTS_SUCCESS,
  payload: { status },
});

export const profileGetFavoriteRestsError: TProfileGetFavoriteRestsError = ({ status }) => ({
  type: PROFILE_GET_FAVORITE_RESTS_ERROR,
  payload: { status },
});

export const profileGetFavoriteRestsSaveData: TProfileGetFavoriteSaveData = (data) => ({
  type: PROFILE_GET_FAVORITE_RESTS_SAVE_DATA,
  payload: data,
});
// Get favorite products
export const profileGetFavoriteProduct: TProfileGetFavoriteRests = (data) => ({
  type: PROFILE_GET_FAVORITE_PRODUCTS,
  payload: data,
});
export const profileGetFavoriteProductPending: TProfileGetFavoriteRestsPending = () => ({
  type: PROFILE_GET_FAVORITE_PRODUCTS_PENDING,
});

export const profileGetFavoriteProductSuccess: TProfileGetFavoriteRestsSuccess = ({ status }) => ({
  type: PROFILE_GET_FAVORITE_PRODUCTS_SUCCESS,
  payload: { status },
});

export const profileGetFavoriteProductError: TProfileGetFavoriteRestsError = ({ status }) => ({
  type: PROFILE_GET_FAVORITE_PRODUCTS_ERROR,
  payload: { status },
});

export const profileGetFavoriteProductSaveData: TProfileGetFavoriteProductsSaveData = (data) => ({
  type: PROFILE_GET_FAVORITE_PRODUCTS_SAVE_DATA,
  payload: data,
});

export const profileGetFavorite: TProfileGetFavoriteRests = (data) => ({
  type: PROFILE_GET_FAVORITE,
  payload: data,
});

// Profile info
export const profileAuthorized: TProfileAuth = (data) => ({
  type: PROFILE_AUTHORIZED,
  payload: data,
});

export const profileUnauthorized: TProfileInfo = (data) => ({
  type: PROFILE_UNAUTHORIZED,
  payload: data,
});

export const profileGetInfo: TProfileGetInfo = (data) => ({
  type: PROFILE_GET_INFO,
  payload: data,
});

export const profileGetInfoPending: TProfileInfo = (data) => ({
  type: PROFILE_GET_INFO_PENDING,
  payload: data,
});

export const profileGetInfoSuccess: TProfileInfoSuccess = (data) => ({
  type: PROFILE_GET_INFO_SUCCESS,
  payload: data,
});

export const profileGetInfoError: TProfileInfoError = (data) => ({
  type: PROFILE_GET_INFO_ERROR,
  payload: data,
});

export const profileGetInfoSaveData: TProfileInfo = (data) => ({
  type: PROFILE_GET_INFO_SAVE_DATA,
  payload: data,
});

export const profileInfoUpdate: TProfileInfoUpdate = (data) => ({
  type: PROFILE_UPDATE_INFO,
  payload: data,
});

export const profileInfoUpdatePending: TProfileInfoUpdatePending = () => ({
  type: PROFILE_UPDATE_INFO_PENDING,
});

export const profileInfoUpdateSuccess: TProfileInfoUpdateSuccess = (data) => ({
  type: PROFILE_UPDATE_INFO_SUCCESS,
  payload: data,
});

export const profileInfoUpdateError: TProfileInfoUpdateError = (data) => ({
  type: PROFILE_UPDATE_INFO_ERROR,
  payload: data,
});

// REVIEWS
export const profileGetReviews: TProfileGetReviews = (data) => ({
  type: PROFILE_GET_REVIEWS,
  payload: data,
});

export const profileGetReviewsPending: TProfileGetReviewsPending = (data) => ({
  type: PROFILE_GET_REVIEWS_PENDING,
  payload: data,
});

export const profileGetReviewsSuccess: TProfileGetReviewsSuccess = (data) => ({
  type: PROFILE_GET_REVIEWS_SUCCESS,
  payload: data,
});

export const profileGetReviewsError: TProfileGetReviewsError = (data) => ({
  type: PROFILE_GET_REVIEWS_ERROR,
  payload: data,
});

export const profileGetReviewsSaveData: TProfileGetReviewsSaveData = (data) => ({
  type: PROFILE_GET_REVIEWS_SAVE_DATA,
  payload: data,
});

// Friends
export const profileGetFriends: TProfileGetFriends = (data) => ({
  type: PROFILE_GET_FRIENDS,
  payload: data,
});

export const profileGetFriendsPending: TProfileGetFriendsPending = (data) => ({
  type: PROFILE_GET_FRIENDS_PENDING,
  payload: data,
});

export const profileGetFriendsSuccess: TProfileGetFriendsSuccess = (data) => ({
  type: PROFILE_GET_FRIENDS_SUCCESS,
  payload: data,
});

export const profileGetFriendsError: TProfileGetFriendsError = (data) => ({
  type: PROFILE_GET_FRIENDS_ERROR,
  payload: data,
});

export const profileGetFriendsSaveData: TProfileGetFriendsSaveData = (data) => ({
  type: PROFILE_GET_FRIENDS_SAVE_DATA,
  payload: data,
});

// Orders
export const profileGetOrders: TProfileGetOrders = (data) => ({
  type: PROFILE_GET_ORDERS,
  payload: data,
});

export const profileGetOrdersPending: TProfileGetOrdersPending = (data) => ({
  type: PROFILE_GET_ORDERS_PENDING,
  payload: data,
});

export const profileGetOrdersSuccess: TProfileGetOrdersSuccess = (data) => ({
  type: PROFILE_GET_ORDERS_SUCCESS,
  payload: data,
});

export const profileGetOrdersError: TProfileGetOrdersError = (data) => ({
  type: PROFILE_GET_ORDERS_ERROR,
  payload: data,
});

export const profileGetOrdersSaveData: TProfileGetOrdersSaveData = (data) => ({
  type: PROFILE_GET_ORDERS_SAVE_DATA,
  payload: data,
});

export const profileShowOrdersReviewModal: TProfileShowOrdersReviewModal = (data) => ({
  type: PROFILE_SHOW_ORDER_REVIEW_MODAL,
  payload: data,
});

// Last order without review
export const profileGetLastOrderWithoutReview: TProfileGetLastOrderWithoutReview = (data) => ({
  type: PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW,
  payload: data,
});

export const profileGetLastOrderWithoutReviewPending: TProfileGetLastOrderWithoutReviewPending = (
  data,
) => ({
  type: PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_PENDING,
  payload: data,
});

export const profileGetLastOrderWithoutReviewSuccess: TProfileGetLastOrderWithoutReviewSuccess = (
  data,
) => ({
  type: PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_SUCCESS,
  payload: data,
});

export const profileGetLastOrderWithoutReviewError: TProfileGetLastOrderWithoutReviewError = (
  data,
) => ({
  type: PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_ERROR,
  payload: data,
});

export const profileGetLastOrderWithoutReviewSaveData: TProfileGetLastOrderWithoutReviewSaveData = (
  data,
) => ({
  type: PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_SAVE_DATA,
  payload: data,
});

// Current Order
export const profileGetUserOrder: TProfileGetUserOrder = (data) => ({
  type: PROFILE_GET_USER_ORDER,
  payload: data,
});

export const profileGetUserOrderPending: TProfileGetUserOrderPending = (data) => ({
  type: PROFILE_GET_USER_ORDER_PENDING,
  payload: data,
});

export const profileGetUserOrderSuccess: TProfileGetUserOrderSuccess = (data) => ({
  type: PROFILE_GET_USER_ORDER_SUCCESS,
  payload: data,
});

export const profileGetUserOrderError: TProfileGetUserOrderError = (data) => ({
  type: PROFILE_GET_USER_ORDER_ERROR,
  payload: data,
});

export const profileGetUserOrderSaveData: TProfileGetUserOrderSaveData = (data) => ({
  type: PROFILE_GET_USER_ORDER_SAVE_DATA,
  payload: data,
});

// Orders Reviews
export const profileSetOrderReview: TProfileSetOrderReview = (data) => ({
  type: PROFILE_SET_ORDER_REVIEW,
  payload: data,
});

export const profileSetOrderReviewPending: TProfileSetOrderReviewPending = (data) => ({
  type: PROFILE_SET_ORDER_REVIEW_PENDING,
  payload: data,
});

export const profileSetOrderReviewSuccess: TProfileSetOrderReviewSuccess = (data) => ({
  type: PROFILE_SET_ORDER_REVIEW_SUCCESS,
  payload: data,
});

export const profileSetOrderReviewError: TProfileSetOrderReviewError = (data) => ({
  type: PROFILE_SET_ORDER_REVIEW_ERROR,
  payload: data,
});

export const profileSaveOrderReviewData: TProfileSaveOrderReviewData = (data) => ({
  type: PROFILE_SAVE_ORDER_REVIEW_DATA,
  payload: data,
});

// Rests info
export const profileGetRestsInfo: TProfileGetRestsInfo = (data) => ({
  type: PROFILE_GET_RESTS_INFO,
  payload: data,
});

export const profileGetRestsInfoPending: TProfileGetRestsInfoPending = (data) => ({
  type: PROFILE_GET_RESTS_INFO_PENDING,
  payload: data,
});

export const profileGetRestsInfoSuccess: TProfileGetRestsInfoSuccess = (data) => ({
  type: PROFILE_GET_RESTS_INFO_SUCCESS,
  payload: data,
});

export const profileGetRestsInfoError: TProfileGetRestsInfoError = (data) => ({
  type: PROFILE_GET_RESTS_INFO_ERROR,
  payload: data,
});

export const profileGetRestsInfoSaveData: TProfileGetRestsInfoSaveData = (data) => ({
  type: PROFILE_GET_RESTS_INFO_SAVE_DATA,
  payload: data,
});
