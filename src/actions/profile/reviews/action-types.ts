/* eslint-disable @typescript-eslint/indent */
// Modules Types
import { ActionCreator } from '@type/actions';
// Constants
import {
  // Get reviews
  PROFILE_GET_REVIEWS,
  PROFILE_GET_REVIEWS_PENDING,
  PROFILE_GET_REVIEWS_SUCCESS,
  PROFILE_GET_REVIEWS_ERROR,
  PROFILE_GET_REVIEWS_SAVE_DATA,
  PROFILE_GET_REVIEWS_RESET_DATA,
  // Set review
  PROFILE_SET_ORDER_REVIEW,
  PROFILE_SET_ORDER_REVIEW_PENDING,
  PROFILE_SET_ORDER_REVIEW_SUCCESS,
  PROFILE_SET_ORDER_REVIEW_ERROR,
  PROFILE_SAVE_ORDER_REVIEW_DATA,
} from '@constants/profile/reviews';
import { TOrderReview, TUserReviewsList } from '@type/profile';

// Reviews
export type TProfileReview = ActionCreator<
  typeof PROFILE_GET_REVIEWS,
  {
    id: string;
    restaurantId: string;
    liked: boolean;
    rate: Record<string, unknown>;
    text: string;
    order: Record<string, unknown>[];
    addedOn: string;
    username: string;
    comments: Record<string, unknown>[];
  }
>;
export type TProfileGetReviews = ActionCreator<
  typeof PROFILE_GET_REVIEWS,
  {
    pageNumber: number;
    pageSize: number;
    access_token?: string;
  }
>;
export type TProfileGetReviewsSuccess = ActionCreator<
  typeof PROFILE_GET_REVIEWS_SUCCESS,
  { status: number }
>;
export type TProfileGetReviewsError = ActionCreator<
  typeof PROFILE_GET_REVIEWS_ERROR,
  { status: number }
>;
export type TProfileGetReviewsPending = ActionCreator<typeof PROFILE_GET_REVIEWS_PENDING>;

export type TProfileGetReviewsSaveData = ActionCreator<
  typeof PROFILE_GET_REVIEWS_SAVE_DATA,
  { data: TUserReviewsList; length: number }
>;

export type TProfileGetReviewsResetData = ActionCreator<typeof PROFILE_GET_REVIEWS_RESET_DATA>;

// Orders Reviews
export type TProfileSetOrderReview = ActionCreator<
  typeof PROFILE_SET_ORDER_REVIEW,
  { review: TOrderReview }
>;

export type TProfileSetOrderReviewPending = ActionCreator<typeof PROFILE_SET_ORDER_REVIEW_PENDING>;

export type TProfileSetOrderReviewSuccess = ActionCreator<
  typeof PROFILE_SET_ORDER_REVIEW_SUCCESS,
  { status: number }
>;

export type TProfileSetOrderReviewError = ActionCreator<
  typeof PROFILE_SET_ORDER_REVIEW_ERROR,
  { status: number }
>;

export type TProfileSaveOrderReviewData = ActionCreator<
  typeof PROFILE_SAVE_ORDER_REVIEW_DATA,
  { orderId: string }
>;
