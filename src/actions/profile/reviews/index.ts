// Constants
import {
  // Get reviews
  PROFILE_GET_REVIEWS,
  PROFILE_GET_REVIEWS_PENDING,
  PROFILE_GET_REVIEWS_SUCCESS,
  PROFILE_GET_REVIEWS_ERROR,
  PROFILE_GET_REVIEWS_SAVE_DATA,
  PROFILE_GET_REVIEWS_RESET_DATA,
  // Set review
  PROFILE_SET_ORDER_REVIEW,
  PROFILE_SET_ORDER_REVIEW_PENDING,
  PROFILE_SET_ORDER_REVIEW_SUCCESS,
  PROFILE_SET_ORDER_REVIEW_ERROR,
  PROFILE_SAVE_ORDER_REVIEW_DATA,
} from '@constants/profile/reviews';
// Types
import {
  // Get reviews
  TProfileGetReviews,
  TProfileGetReviewsPending,
  TProfileGetReviewsSuccess,
  TProfileGetReviewsError,
  TProfileGetReviewsSaveData,
  TProfileGetReviewsResetData,
  // Set review
  TProfileSetOrderReview,
  TProfileSaveOrderReviewData,
  TProfileSetOrderReviewError,
  TProfileSetOrderReviewPending,
  TProfileSetOrderReviewSuccess,
} from './action-types';

// REVIEWS
export const profileGetReviews: TProfileGetReviews = (data) => ({
  type: PROFILE_GET_REVIEWS,
  payload: data,
});

export const profileGetReviewsPending: TProfileGetReviewsPending = (data) => ({
  type: PROFILE_GET_REVIEWS_PENDING,
  payload: data,
});

export const profileGetReviewsSuccess: TProfileGetReviewsSuccess = (data) => ({
  type: PROFILE_GET_REVIEWS_SUCCESS,
  payload: data,
});

export const profileGetReviewsError: TProfileGetReviewsError = (data) => ({
  type: PROFILE_GET_REVIEWS_ERROR,
  payload: data,
});

export const profileGetReviewsSaveData: TProfileGetReviewsSaveData = (data) => ({
  type: PROFILE_GET_REVIEWS_SAVE_DATA,
  payload: data,
});

export const profileGetReviewsResetData: TProfileGetReviewsResetData = () => ({
  type: PROFILE_GET_REVIEWS_RESET_DATA,
});

// Orders Reviews
export const profileSetOrderReview: TProfileSetOrderReview = (data) => ({
  type: PROFILE_SET_ORDER_REVIEW,
  payload: data,
});

export const profileSetOrderReviewPending: TProfileSetOrderReviewPending = (data) => ({
  type: PROFILE_SET_ORDER_REVIEW_PENDING,
  payload: data,
});

export const profileSetOrderReviewSuccess: TProfileSetOrderReviewSuccess = (data) => ({
  type: PROFILE_SET_ORDER_REVIEW_SUCCESS,
  payload: data,
});

export const profileSetOrderReviewError: TProfileSetOrderReviewError = (data) => ({
  type: PROFILE_SET_ORDER_REVIEW_ERROR,
  payload: data,
});

export const profileSaveOrderReviewData: TProfileSaveOrderReviewData = (data) => ({
  type: PROFILE_SAVE_ORDER_REVIEW_DATA,
  payload: data,
});
