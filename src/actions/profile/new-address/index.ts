// Constants
import {
  PROFILE_GET_NEW_ADDRESS,
  PROFILE_GET_NEW_ADDRESS_PENDING,
  PROFILE_GET_NEW_ADDRESS_SUCCESS,
  PROFILE_GET_NEW_ADDRESS_ERROR,
  PROFILE_SAVE_NEW_ADDRESS,
  PROFILE_RESET_NEW_ADDRESS,
  PROFILE_RESET_NEW_ADDRESS_SUGGEST,
  PROFILE_UPDATE_NEW_ADDRESS,
} from '@constants/profile/new-address';
// Types
import {
  TFetchNewAddressesSuggest,
  TFetchNewAddressesSuggestPending,
  TFetchNewAddressesSuggestSuccess,
  TFetchNewAddressesSuggestError,
  TSaveNewAddressesSuggest,
  TResetNewAddresses,
  TResetNewAddressesSuggest,
  TUpdateProfileNewAddress,
} from './action-types';

// User Adresses Suggest
export const fetchNewAddressesSuggest: TFetchNewAddressesSuggest = (data) => ({
  type: PROFILE_GET_NEW_ADDRESS,
  payload: data,
});

export const fetchNewAddressesSuggestPending: TFetchNewAddressesSuggestPending = () => ({
  type: PROFILE_GET_NEW_ADDRESS_PENDING,
});

export const fetchNewAddressesSuggestSuccess: TFetchNewAddressesSuggestSuccess = (data) => ({
  type: PROFILE_GET_NEW_ADDRESS_SUCCESS,
  payload: data,
});

export const fetchNewAddressesSuggestError: TFetchNewAddressesSuggestError = (data) => ({
  type: PROFILE_GET_NEW_ADDRESS_ERROR,
  payload: data,
});

export const saveNewAddressesSuggest: TSaveNewAddressesSuggest = (data) => ({
  type: PROFILE_SAVE_NEW_ADDRESS,
  payload: data,
});

export const resetNewAddressesSuggest: TResetNewAddressesSuggest = () => ({
  type: PROFILE_RESET_NEW_ADDRESS_SUGGEST,
});

export const resetNewAddresses: TResetNewAddresses = () => ({
  type: PROFILE_RESET_NEW_ADDRESS,
});

export const updateProfileNewAddress: TUpdateProfileNewAddress = (data) => ({
  type: PROFILE_UPDATE_NEW_ADDRESS,
  payload: data,
});
