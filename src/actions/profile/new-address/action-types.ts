/* eslint-disable @typescript-eslint/indent */
// Modules Types
import { ActionCreator } from '@type/actions';
// Constants
import {
  PROFILE_GET_NEW_ADDRESS,
  PROFILE_GET_NEW_ADDRESS_PENDING,
  PROFILE_GET_NEW_ADDRESS_SUCCESS,
  PROFILE_GET_NEW_ADDRESS_ERROR,
  PROFILE_SAVE_NEW_ADDRESS,
  PROFILE_RESET_NEW_ADDRESS,
  PROFILE_RESET_NEW_ADDRESS_SUGGEST,
  PROFILE_UPDATE_NEW_ADDRESS,
} from '@constants/profile/new-address';
// Main Types
import { TNewAddressItem, TNewAddressesList } from '@type/new-address';

export type TFetchNewAddressesSuggest = ActionCreator<
  typeof PROFILE_GET_NEW_ADDRESS,
  {
    query: string;
    limit: number;
  }
>;

export type TFetchNewAddressesSuggestPending = ActionCreator<
  typeof PROFILE_GET_NEW_ADDRESS_PENDING
>;

export type TFetchNewAddressesSuggestSuccess = ActionCreator<
  typeof PROFILE_GET_NEW_ADDRESS_SUCCESS,
  { status: number }
>;

export type TFetchNewAddressesSuggestError = ActionCreator<
  typeof PROFILE_GET_NEW_ADDRESS_ERROR,
  { status: number }
>;

export type TSaveNewAddressesSuggest = ActionCreator<
  typeof PROFILE_SAVE_NEW_ADDRESS,
  {
    data: TNewAddressesList;
  }
>;

export type TResetNewAddresses = ActionCreator<typeof PROFILE_RESET_NEW_ADDRESS>;

export type TResetNewAddressesSuggest = ActionCreator<typeof PROFILE_RESET_NEW_ADDRESS_SUGGEST>;

export type TUpdateProfileNewAddress = ActionCreator<
  typeof PROFILE_UPDATE_NEW_ADDRESS,
  {
    data: TNewAddressItem;
  }
>;
