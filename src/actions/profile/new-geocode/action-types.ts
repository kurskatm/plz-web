/* eslint-disable @typescript-eslint/indent */
// Modules Types
import { ActionCreator } from '@type/actions';
// Constants
import {
  PROFILE_GET_NEW_ADDRESSES_GEOCODE,
  PROFILE_GET_NEW_ADDRESSES_GEOCODE_PENDING,
  PROFILE_GET_NEW_ADDRESSES_GEOCODE_SUCCESS,
  PROFILE_GET_NEW_ADDRESSES_GEOCODE_ERROR,
  PROFILE_SAVE_NEW_ADDRESSES_GEOCODE,
  PROFILE_RESET_NEW_ADDRESSES_GEOCODE,
} from '@constants/profile/new-geocode';
// Main Types
import { TNewGeocodeData } from '@type/new-geocode';
import { TNewAddressItem } from '@type/new-address';

// User Geocode
export type TProfileGetNewGeocode = ActionCreator<
  typeof PROFILE_GET_NEW_ADDRESSES_GEOCODE,
  TNewAddressItem
>;

export type TProfileGetNewGeocodePending = ActionCreator<
  typeof PROFILE_GET_NEW_ADDRESSES_GEOCODE_PENDING
>;

export type TProfileGetNewGeocodeSuccess = ActionCreator<
  typeof PROFILE_GET_NEW_ADDRESSES_GEOCODE_SUCCESS,
  { status: string | number }
>;

export type TProfileGetNewGeocodeError = ActionCreator<
  typeof PROFILE_GET_NEW_ADDRESSES_GEOCODE_ERROR,
  { status: string | number }
>;

export type TProfileSaveNewGeocode = ActionCreator<
  typeof PROFILE_SAVE_NEW_ADDRESSES_GEOCODE,
  TNewGeocodeData
>;

export type TProfileResetNewGeocode = ActionCreator<typeof PROFILE_RESET_NEW_ADDRESSES_GEOCODE>;
