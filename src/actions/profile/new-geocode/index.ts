// Constants
import {
  PROFILE_GET_NEW_ADDRESSES_GEOCODE,
  PROFILE_GET_NEW_ADDRESSES_GEOCODE_PENDING,
  PROFILE_GET_NEW_ADDRESSES_GEOCODE_SUCCESS,
  PROFILE_GET_NEW_ADDRESSES_GEOCODE_ERROR,
  PROFILE_SAVE_NEW_ADDRESSES_GEOCODE,
  PROFILE_RESET_NEW_ADDRESSES_GEOCODE,
} from '@constants/profile/new-geocode';
// Types
import {
  TProfileGetNewGeocode,
  TProfileGetNewGeocodePending,
  TProfileGetNewGeocodeSuccess,
  TProfileGetNewGeocodeError,
  TProfileSaveNewGeocode,
  TProfileResetNewGeocode,
} from './action-types';

// User Geocode
export const profileGetNewGeocode: TProfileGetNewGeocode = (data) => ({
  type: PROFILE_GET_NEW_ADDRESSES_GEOCODE,
  payload: data,
});

export const profileGetNewGeocodePending: TProfileGetNewGeocodePending = () => ({
  type: PROFILE_GET_NEW_ADDRESSES_GEOCODE_PENDING,
});

export const profileGetNewGeocodeSuccess: TProfileGetNewGeocodeSuccess = (data) => ({
  type: PROFILE_GET_NEW_ADDRESSES_GEOCODE_SUCCESS,
  payload: data,
});

export const profileGetNewGeocodeError: TProfileGetNewGeocodeError = (data) => ({
  type: PROFILE_GET_NEW_ADDRESSES_GEOCODE_ERROR,
  payload: data,
});

export const profileSaveNewGeocode: TProfileSaveNewGeocode = (data) => ({
  type: PROFILE_SAVE_NEW_ADDRESSES_GEOCODE,
  payload: data,
});

export const profileResetNewGeocode: TProfileResetNewGeocode = () => ({
  type: PROFILE_RESET_NEW_ADDRESSES_GEOCODE,
});
