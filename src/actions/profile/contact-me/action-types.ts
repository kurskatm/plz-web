/* eslint-disable @typescript-eslint/indent */
// Modules Types
import { ActionCreator } from '@type/actions';
// Models
import { TContactMeDetailsModel } from '@models/profile/types';
// Constants
import {
  PROFILE_ORDER_CONTACT_ME,
  PROFILE_ORDER_CONTACT_ME_PENDING,
  PROFILE_ORDER_CONTACT_ME_IDLE,
  PROFILE_ORDER_CONTACT_ME_SUCCESS,
  PROFILE_ORDER_CONTACT_ME_ERROR,
  PROFILE_ORDER_CONTACT_ME_RESET,
} from '@constants/profile/orders/contact-me';

export type TProfileOrderContactMe = ActionCreator<
  typeof PROFILE_ORDER_CONTACT_ME,
  {
    orderId: string;
  }
>;

export type TProfileOrderContactMePending = ActionCreator<typeof PROFILE_ORDER_CONTACT_ME_PENDING>;
export type TProfileOrderContactMeIdle = ActionCreator<typeof PROFILE_ORDER_CONTACT_ME_IDLE>;

export type TProfileOrderContactMeSuccess = ActionCreator<
  typeof PROFILE_ORDER_CONTACT_ME_SUCCESS,
  {
    status: number;
    orderId: string;
  }
>;

export type TProfileOrderContactMeError = ActionCreator<
  typeof PROFILE_ORDER_CONTACT_ME_ERROR,
  {
    status: number;
    orderId: string;
    details?: TContactMeDetailsModel;
  }
>;

export type TProfileOrderContactMeReset = ActionCreator<
  typeof PROFILE_ORDER_CONTACT_ME_RESET,
  {
    orderId?: string;
  }
>;
