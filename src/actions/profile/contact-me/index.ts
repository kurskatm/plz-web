// Constants
import {
  PROFILE_ORDER_CONTACT_ME,
  PROFILE_ORDER_CONTACT_ME_PENDING,
  PROFILE_ORDER_CONTACT_ME_IDLE,
  PROFILE_ORDER_CONTACT_ME_SUCCESS,
  PROFILE_ORDER_CONTACT_ME_ERROR,
  PROFILE_ORDER_CONTACT_ME_RESET,
} from '@constants/profile/orders/contact-me';
// Types
import {
  TProfileOrderContactMe,
  TProfileOrderContactMePending,
  TProfileOrderContactMeIdle,
  TProfileOrderContactMeSuccess,
  TProfileOrderContactMeError,
  TProfileOrderContactMeReset,
} from './action-types';

export const profileOrderContactMe: TProfileOrderContactMe = (data) => ({
  type: PROFILE_ORDER_CONTACT_ME,
  payload: data,
});

export const profileOrderContactMePending: TProfileOrderContactMePending = () => ({
  type: PROFILE_ORDER_CONTACT_ME_PENDING,
});

export const profileOrderContactMeIdle: TProfileOrderContactMeIdle = () => ({
  type: PROFILE_ORDER_CONTACT_ME_IDLE,
});

export const profileOrderContactMeSuccess: TProfileOrderContactMeSuccess = (data) => ({
  type: PROFILE_ORDER_CONTACT_ME_SUCCESS,
  payload: data,
});

export const profileOrderContactMeError: TProfileOrderContactMeError = (data) => ({
  type: PROFILE_ORDER_CONTACT_ME_ERROR,
  payload: data,
});

export const profileOrderContactMeReset: TProfileOrderContactMeReset = (data) => ({
  type: PROFILE_ORDER_CONTACT_ME_RESET,
  payload: data,
});
