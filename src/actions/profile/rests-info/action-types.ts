/* eslint-disable @typescript-eslint/indent */
// Modules Types
import { ActionCreator } from '@type/actions';
// Constants
import {
  PROFILE_GET_RESTS_INFO,
  PROFILE_GET_RESTS_INFO_PENDING,
  PROFILE_GET_RESTS_INFO_SUCCESS,
  PROFILE_GET_RESTS_INFO_ERROR,
  PROFILE_GET_RESTS_INFO_SAVE_DATA,
} from '@constants/profile/rests-info';
// Main Types
import { TRestaurantsInfoArray } from '@type/restaurant';

// Rests info
export type TProfileGetRestsInfo = ActionCreator<typeof PROFILE_GET_RESTS_INFO, string[]>;

export type TProfileGetRestsInfoPending = ActionCreator<typeof PROFILE_GET_RESTS_INFO_PENDING>;

export type TProfileGetRestsInfoSuccess = ActionCreator<
  typeof PROFILE_GET_RESTS_INFO_SUCCESS,
  { status: number }
>;

export type TProfileGetRestsInfoError = ActionCreator<
  typeof PROFILE_GET_RESTS_INFO_ERROR,
  { status: number }
>;

export type TProfileGetRestsInfoSaveData = ActionCreator<
  typeof PROFILE_GET_RESTS_INFO_SAVE_DATA,
  { data: TRestaurantsInfoArray }
>;
