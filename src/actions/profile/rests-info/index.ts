// Constants
import {
  PROFILE_GET_RESTS_INFO,
  PROFILE_GET_RESTS_INFO_PENDING,
  PROFILE_GET_RESTS_INFO_SUCCESS,
  PROFILE_GET_RESTS_INFO_ERROR,
  PROFILE_GET_RESTS_INFO_SAVE_DATA,
} from '@constants/profile/rests-info';
// Types
import {
  TProfileGetRestsInfo,
  TProfileGetRestsInfoPending,
  TProfileGetRestsInfoSuccess,
  TProfileGetRestsInfoError,
  TProfileGetRestsInfoSaveData,
} from './action-types';

// Rests info
export const profileGetRestsInfo: TProfileGetRestsInfo = (data) => ({
  type: PROFILE_GET_RESTS_INFO,
  payload: data,
});

export const profileGetRestsInfoPending: TProfileGetRestsInfoPending = (data) => ({
  type: PROFILE_GET_RESTS_INFO_PENDING,
  payload: data,
});

export const profileGetRestsInfoSuccess: TProfileGetRestsInfoSuccess = (data) => ({
  type: PROFILE_GET_RESTS_INFO_SUCCESS,
  payload: data,
});

export const profileGetRestsInfoError: TProfileGetRestsInfoError = (data) => ({
  type: PROFILE_GET_RESTS_INFO_ERROR,
  payload: data,
});

export const profileGetRestsInfoSaveData: TProfileGetRestsInfoSaveData = (data) => ({
  type: PROFILE_GET_RESTS_INFO_SAVE_DATA,
  payload: data,
});
