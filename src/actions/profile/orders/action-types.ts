/* eslint-disable @typescript-eslint/indent */
// Modules Types
import { ActionCreator } from '@type/actions';
// Constants
import {
  // Last order
  PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW,
  PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_PENDING,
  PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_ERROR,
  PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_SAVE_DATA,
  PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_SUCCESS,
  PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_RESET_DATA,
  // All orders
  PROFILE_GET_ORDERS,
  PROFILE_GET_ORDERS_ERROR,
  PROFILE_GET_ORDERS_PENDING,
  PROFILE_GET_ORDERS_SAVE_DATA,
  PROFILE_GET_ORDERS_SUCCESS,
  PROFILE_GET_ORDERS_RESET_DATA,
  // Current order
  PROFILE_GET_USER_ORDER,
  PROFILE_GET_USER_ORDER_ERROR,
  PROFILE_GET_USER_ORDER_PENDING,
  PROFILE_GET_USER_ORDER_SAVE_DATA,
  PROFILE_GET_USER_ORDER_SUCCESS,
  PROFILE_RESET_USER_ORDER,
  // Modal
  PROFILE_SHOW_ORDER_REVIEW_MODAL,
} from '@constants/profile/orders';
// Main Types
import { TUserOrdersData, TUserOrder } from '@type/profile';

// Orders
export type TProfileGetOrders = ActionCreator<
  typeof PROFILE_GET_ORDERS,
  {
    pageNumber: number;
    pageSize: number;
    access_token?: string;
  }
>;

export type TProfileGetOrdersPending = ActionCreator<typeof PROFILE_GET_ORDERS_PENDING>;

export type TProfileGetOrdersSuccess = ActionCreator<
  typeof PROFILE_GET_ORDERS_SUCCESS,
  { status: number }
>;

export type TProfileGetOrdersError = ActionCreator<
  typeof PROFILE_GET_ORDERS_ERROR,
  { status: number }
>;

export type TProfileGetOrdersSaveData = ActionCreator<
  typeof PROFILE_GET_ORDERS_SAVE_DATA,
  { data: TUserOrdersData }
>;

export type TProfileGetOrdersResetData = ActionCreator<typeof PROFILE_GET_ORDERS_RESET_DATA>;

export type TProfileShowOrdersReviewModal = ActionCreator<
  typeof PROFILE_SHOW_ORDER_REVIEW_MODAL,
  {
    orderId: string;
    showModal: boolean;
  }
>;

// Last order without review
export type TProfileGetLastOrderWithoutReview = ActionCreator<
  typeof PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW,
  { restaurantId?: string }
>;

export type TProfileGetLastOrderWithoutReviewPending = ActionCreator<
  typeof PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_PENDING
>;

export type TProfileGetLastOrderWithoutReviewSuccess = ActionCreator<
  typeof PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_SUCCESS,
  { status: number }
>;

export type TProfileGetLastOrderWithoutReviewError = ActionCreator<
  typeof PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_ERROR,
  { status: number }
>;

export type TProfileGetLastOrderWithoutReviewSaveData = ActionCreator<
  typeof PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_SAVE_DATA,
  { order: TUserOrder }
>;

export type TProfileGetLastOrderWithoutReviewResetData = ActionCreator<
  typeof PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_RESET_DATA
>;

// Current Order
export type TProfileGetUserOrder = ActionCreator<
  typeof PROFILE_GET_USER_ORDER,
  { orderId: string }
>;

export type TProfileGetUserOrderPending = ActionCreator<typeof PROFILE_GET_USER_ORDER_PENDING>;

export type TProfileGetUserOrderSuccess = ActionCreator<
  typeof PROFILE_GET_USER_ORDER_SUCCESS,
  { status: number }
>;

export type TProfileGetUserOrderError = ActionCreator<
  typeof PROFILE_GET_USER_ORDER_ERROR,
  { status: number }
>;

export type TProfileGetUserOrderSaveData = ActionCreator<
  typeof PROFILE_GET_USER_ORDER_SAVE_DATA,
  { order: TUserOrder }
>;

export type TProfileResetUserOrderData = ActionCreator<typeof PROFILE_RESET_USER_ORDER>;
