// Constants
import {
  // Last order
  PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW,
  PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_PENDING,
  PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_ERROR,
  PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_SAVE_DATA,
  PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_SUCCESS,
  PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_RESET_DATA,
  // All orders
  PROFILE_GET_ORDERS,
  PROFILE_GET_ORDERS_ERROR,
  PROFILE_GET_ORDERS_PENDING,
  PROFILE_GET_ORDERS_SAVE_DATA,
  PROFILE_GET_ORDERS_SUCCESS,
  PROFILE_GET_ORDERS_RESET_DATA,
  // Current order
  PROFILE_GET_USER_ORDER,
  PROFILE_GET_USER_ORDER_ERROR,
  PROFILE_GET_USER_ORDER_PENDING,
  PROFILE_GET_USER_ORDER_SAVE_DATA,
  PROFILE_GET_USER_ORDER_SUCCESS,
  PROFILE_RESET_USER_ORDER,
  // Modal
  PROFILE_SHOW_ORDER_REVIEW_MODAL,
} from '@constants/profile/orders';
// Types
import {
  // Last order
  TProfileGetLastOrderWithoutReview,
  TProfileGetLastOrderWithoutReviewError,
  TProfileGetLastOrderWithoutReviewPending,
  TProfileGetLastOrderWithoutReviewSaveData,
  TProfileGetLastOrderWithoutReviewSuccess,
  TProfileGetLastOrderWithoutReviewResetData,
  // All orders
  TProfileGetOrders,
  TProfileGetOrdersError,
  TProfileGetOrdersPending,
  TProfileGetOrdersSaveData,
  TProfileGetOrdersSuccess,
  TProfileGetOrdersResetData,
  // Current order
  TProfileGetUserOrder,
  TProfileGetUserOrderError,
  TProfileGetUserOrderPending,
  TProfileGetUserOrderSaveData,
  TProfileGetUserOrderSuccess,
  TProfileResetUserOrderData,
  // Modal
  TProfileShowOrdersReviewModal,
} from './action-types';

// Orders
export const profileGetOrders: TProfileGetOrders = (data) => ({
  type: PROFILE_GET_ORDERS,
  payload: data,
});

export const profileGetOrdersPending: TProfileGetOrdersPending = (data) => ({
  type: PROFILE_GET_ORDERS_PENDING,
  payload: data,
});

export const profileGetOrdersSuccess: TProfileGetOrdersSuccess = (data) => ({
  type: PROFILE_GET_ORDERS_SUCCESS,
  payload: data,
});

export const profileGetOrdersError: TProfileGetOrdersError = (data) => ({
  type: PROFILE_GET_ORDERS_ERROR,
  payload: data,
});

export const profileGetOrdersSaveData: TProfileGetOrdersSaveData = (data) => ({
  type: PROFILE_GET_ORDERS_SAVE_DATA,
  payload: data,
});

export const profileGetOrdersResetData: TProfileGetOrdersResetData = () => ({
  type: PROFILE_GET_ORDERS_RESET_DATA,
});

// Modal
export const profileShowOrdersReviewModal: TProfileShowOrdersReviewModal = (data) => ({
  type: PROFILE_SHOW_ORDER_REVIEW_MODAL,
  payload: data,
});

// Last order without review
export const profileGetLastOrderWithoutReview: TProfileGetLastOrderWithoutReview = (data) => ({
  type: PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW,
  payload: data,
});

export const profileGetLastOrderWithoutReviewPending: TProfileGetLastOrderWithoutReviewPending = (
  data,
) => ({
  type: PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_PENDING,
  payload: data,
});

export const profileGetLastOrderWithoutReviewSuccess: TProfileGetLastOrderWithoutReviewSuccess = (
  data,
) => ({
  type: PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_SUCCESS,
  payload: data,
});

export const profileGetLastOrderWithoutReviewError: TProfileGetLastOrderWithoutReviewError = (
  data,
) => ({
  type: PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_ERROR,
  payload: data,
});

export const profileGetLastOrderWithoutReviewSaveData: TProfileGetLastOrderWithoutReviewSaveData = (
  data,
) => ({
  type: PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_SAVE_DATA,
  payload: data,
});

// eslint-disable-next-line max-len
export const profileGetLastOrderWithoutReviewResetData: TProfileGetLastOrderWithoutReviewResetData = () => ({
  type: PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_RESET_DATA,
});

// Current Order
export const profileGetUserOrder: TProfileGetUserOrder = (data) => ({
  type: PROFILE_GET_USER_ORDER,
  payload: data,
});

export const profileGetUserOrderPending: TProfileGetUserOrderPending = (data) => ({
  type: PROFILE_GET_USER_ORDER_PENDING,
  payload: data,
});

export const profileGetUserOrderSuccess: TProfileGetUserOrderSuccess = (data) => ({
  type: PROFILE_GET_USER_ORDER_SUCCESS,
  payload: data,
});

export const profileGetUserOrderError: TProfileGetUserOrderError = (data) => ({
  type: PROFILE_GET_USER_ORDER_ERROR,
  payload: data,
});

export const profileGetUserOrderSaveData: TProfileGetUserOrderSaveData = (data) => ({
  type: PROFILE_GET_USER_ORDER_SAVE_DATA,
  payload: data,
});

export const profileResetUserOrderData: TProfileResetUserOrderData = () => ({
  type: PROFILE_RESET_USER_ORDER,
});
