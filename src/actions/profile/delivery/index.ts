// Constants
import {
  // Delivery
  PROFILE_UPDATE_ADDRESS,
  PROFILE_UPDATE_PICKUP,
} from '@constants/profile/delivery';
// Types
import { TProfileUpdateAddress, TProfileUpdatePickup } from './action-types';

// Delivery
export const profileUpdateAddress: TProfileUpdateAddress = (data) => ({
  type: PROFILE_UPDATE_ADDRESS,
  payload: data,
});

export const profileUpdatePickup: TProfileUpdatePickup = (pickup) => ({
  type: PROFILE_UPDATE_PICKUP,
  payload: pickup,
});
