/* eslint-disable @typescript-eslint/indent */
// Modules Types
import { ActionCreator } from '@type/actions';
// Constants
import {
  // Delivery
  PROFILE_UPDATE_ADDRESS,
  PROFILE_UPDATE_PICKUP,
} from '@constants/profile/delivery';

// Delivery
export type TProfileUpdateAddress = ActionCreator<
  typeof PROFILE_UPDATE_ADDRESS,
  { name: string; description: string }
>;

export type TProfileUpdatePickup = ActionCreator<typeof PROFILE_UPDATE_PICKUP, boolean>;
