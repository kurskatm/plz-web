/* eslint-disable @typescript-eslint/indent */
// Modules Types
import { ActionCreator } from '@type/actions';
// Constants
import {
  // User info
  PROFILE_GET_INFO,
  PROFILE_GET_INFO_PENDING,
  PROFILE_GET_INFO_SUCCESS,
  PROFILE_GET_INFO_ERROR,
  PROFILE_GET_INFO_RESET_UI,
  // Update
  PROFILE_UPDATE_INFO,
  PROFILE_UPDATE_INFO_PENDING,
  PROFILE_UPDATE_INFO_SUCCESS,
  PROFILE_UPDATE_INFO_ERROR,
  PROFILE_UPDATE_INFO_RESET_UI,
} from '@constants/profile/info';

export type TProfileInfo = ActionCreator<
  typeof PROFILE_GET_INFO,
  {
    id: string;
    fullName: string;
    dateOfBirth: string;
    email: string;
    phoneNumber: string;
    gender: string;
    avatar: string;
    invitationId: string;
  }
>;

export type TProfileAuth = ActionCreator<
  typeof PROFILE_GET_INFO,
  {
    userId?: string;
    access_token?: string;
  }
>;
export type TProfileGetInfo = ActionCreator<typeof PROFILE_GET_INFO, Record<string, string>>;
export type TProfileGetInfoPending = ActionCreator<typeof PROFILE_GET_INFO_PENDING>;
export type TProfileInfoSuccess = ActionCreator<
  typeof PROFILE_GET_INFO_SUCCESS,
  { status: number }
>;
export type TProfileInfoError = ActionCreator<typeof PROFILE_GET_INFO_ERROR, { status: number }>;

export type TProfileInfoResetUi = ActionCreator<typeof PROFILE_GET_INFO_RESET_UI>;

export type TProfileInfoUpdate = ActionCreator<
  typeof PROFILE_UPDATE_INFO,
  {
    access_token: string;
    payload: Record<string, unknown>;
    emailChanged: boolean;
    onSuccess?: () => void;
  }
>;
export type TProfileInfoUpdateSuccess = ActionCreator<
  typeof PROFILE_UPDATE_INFO_SUCCESS,
  { status: number }
>;
export type TProfileInfoUpdateError = ActionCreator<
  typeof PROFILE_UPDATE_INFO_ERROR,
  { status: number }
>;
export type TProfileInfoUpdatePending = ActionCreator<typeof PROFILE_UPDATE_INFO_PENDING>;
export type TProfileInfoUpdateResetUi = ActionCreator<typeof PROFILE_UPDATE_INFO_RESET_UI>;
