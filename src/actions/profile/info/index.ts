// Constants
import { PROFILE_AUTHORIZED, PROFILE_UNAUTHORIZED } from '@constants/profile/auth';
import {
  // User info
  PROFILE_GET_INFO,
  PROFILE_GET_INFO_PENDING,
  PROFILE_GET_INFO_SUCCESS,
  PROFILE_GET_INFO_ERROR,
  PROFILE_GET_INFO_SAVE_DATA,
  PROFILE_GET_INFO_RESET_UI,
  // Update
  PROFILE_UPDATE_INFO,
  PROFILE_UPDATE_INFO_PENDING,
  PROFILE_UPDATE_INFO_SUCCESS,
  PROFILE_UPDATE_INFO_ERROR,
  PROFILE_UPDATE_INFO_SAVE,
  PROFILE_UPDATE_INFO_RESET_UI,
} from '@constants/profile/info';
// Types
import {
  TProfileAuth,
  TProfileGetInfo,
  TProfileInfoSuccess,
  TProfileInfoError,
  TProfileInfo,
  TProfileInfoResetUi,
  // Update
  TProfileInfoUpdate,
  TProfileInfoUpdatePending,
  TProfileInfoUpdateSuccess,
  TProfileInfoUpdateError,
  TProfileInfoUpdateResetUi,
} from './action-types';

// Profile info
export const profileAuthorized: TProfileAuth = (data) => ({
  type: PROFILE_AUTHORIZED,
  payload: data,
});

export const profileUnauthorized: TProfileInfo = (data) => ({
  type: PROFILE_UNAUTHORIZED,
  payload: data,
});

export const profileGetInfo: TProfileGetInfo = (data) => ({
  type: PROFILE_GET_INFO,
  payload: data,
});

export const profileGetInfoPending: TProfileInfo = (data) => ({
  type: PROFILE_GET_INFO_PENDING,
  payload: data,
});

export const profileGetInfoSuccess: TProfileInfoSuccess = (data) => ({
  type: PROFILE_GET_INFO_SUCCESS,
  payload: data,
});

export const profileGetInfoError: TProfileInfoError = (data) => ({
  type: PROFILE_GET_INFO_ERROR,
  payload: data,
});

export const profileGetInfoSaveData: TProfileInfo = (data) => ({
  type: PROFILE_GET_INFO_SAVE_DATA,
  payload: data,
});

export const profileInfoResetUi: TProfileInfoResetUi = () => ({
  type: PROFILE_GET_INFO_RESET_UI,
});

export const profileInfoUpdate: TProfileInfoUpdate = (data) => ({
  type: PROFILE_UPDATE_INFO,
  payload: data,
});

export const profileInfoUpdateSave: TProfileInfoUpdate = (data) => ({
  type: PROFILE_UPDATE_INFO_SAVE,
  payload: data,
});

export const profileInfoUpdatePending: TProfileInfoUpdatePending = () => ({
  type: PROFILE_UPDATE_INFO_PENDING,
});

export const profileInfoUpdateSuccess: TProfileInfoUpdateSuccess = (data) => ({
  type: PROFILE_UPDATE_INFO_SUCCESS,
  payload: data,
});

export const profileInfoUpdateError: TProfileInfoUpdateError = (data) => ({
  type: PROFILE_UPDATE_INFO_ERROR,
  payload: data,
});

export const profileInfoUpdateResetUi: TProfileInfoUpdateResetUi = (data) => ({
  type: PROFILE_UPDATE_INFO_RESET_UI,
  payload: data,
});
