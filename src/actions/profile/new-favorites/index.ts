// Constants
import {
  PROFILE_GET_FAVORITES_INFO,
  PROFILE_GET_FAVORITES_INFO_PENDING,
  PROFILE_GET_FAVORITES_INFO_SUCCESS,
  PROFILE_GET_FAVORITES_INFO_ERROR,
  PROFILE_GET_FAVORITES_INFO_SAVE_DATA,
  PROFILE_GET_FAVORITES_INFO_RESET,
} from '@constants/profile';
// Types
import {
  TProfileGetFavorites,
  TProfileGetFavoritesPending,
  TProfileGetFavoritesSuccess,
  TProfileGetFavoritesError,
  TProfileGetFavoritesSave,
  TProfileGetFavoritesReset,
} from './action-types';

export const profileFetchFavorites: TProfileGetFavorites = (payload) => ({
  type: PROFILE_GET_FAVORITES_INFO,
  payload,
});

export const profileFetchFavoritesPending: TProfileGetFavoritesPending = () => ({
  type: PROFILE_GET_FAVORITES_INFO_PENDING,
});

export const profileFetchFavoritesSuccess: TProfileGetFavoritesSuccess = (payload) => ({
  type: PROFILE_GET_FAVORITES_INFO_SUCCESS,
  payload,
});

export const profileFetchFavoritesError: TProfileGetFavoritesError = (payload) => ({
  type: PROFILE_GET_FAVORITES_INFO_ERROR,
  payload,
});

export const profileFetchFavoritesSave: TProfileGetFavoritesSave = (payload) => ({
  type: PROFILE_GET_FAVORITES_INFO_SAVE_DATA,
  payload,
});

export const profileFetchFavoritesReset: TProfileGetFavoritesReset = () => ({
  type: PROFILE_GET_FAVORITES_INFO_RESET,
});
