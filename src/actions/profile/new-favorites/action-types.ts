/* eslint-disable @typescript-eslint/indent */
// Modules Types
import { ActionCreator } from '@type/actions';
// Constants
import {
  PROFILE_GET_FAVORITES_INFO,
  PROFILE_GET_FAVORITES_INFO_PENDING,
  PROFILE_GET_FAVORITES_INFO_SUCCESS,
  PROFILE_GET_FAVORITES_INFO_ERROR,
  PROFILE_GET_FAVORITES_INFO_SAVE_DATA,
  PROFILE_GET_FAVORITES_INFO_RESET,
} from '@constants/profile';
// Main types
import { TFavoritesSummaryData } from '@type/new-favorites';

// User favorites
export type TProfileGetFavorites = ActionCreator<
  typeof PROFILE_GET_FAVORITES_INFO,
  {
    access_token: string;
    cityId: string;
    latitude: number;
    longitude: number;
  }
>;

export type TProfileGetFavoritesPending = ActionCreator<typeof PROFILE_GET_FAVORITES_INFO_PENDING>;

export type TProfileGetFavoritesSuccess = ActionCreator<
  typeof PROFILE_GET_FAVORITES_INFO_SUCCESS,
  { status: string | number }
>;

export type TProfileGetFavoritesError = ActionCreator<
  typeof PROFILE_GET_FAVORITES_INFO_ERROR,
  { status: string | number }
>;

export type TProfileGetFavoritesSave = ActionCreator<
  typeof PROFILE_GET_FAVORITES_INFO_SAVE_DATA,
  TFavoritesSummaryData
>;

export type TProfileGetFavoritesReset = ActionCreator<typeof PROFILE_GET_FAVORITES_INFO_RESET>;
