// Constants
import {
  PROFILE_GET_FRIENDS,
  PROFILE_GET_FRIENDS_PENDING,
  PROFILE_GET_FRIENDS_SUCCESS,
  PROFILE_GET_FRIENDS_ERROR,
  PROFILE_GET_FRIENDS_SAVE_DATA,
} from '@constants/profile/friends';
// Types
import {
  TProfileGetFriends,
  TProfileGetFriendsPending,
  TProfileGetFriendsSuccess,
  TProfileGetFriendsError,
  TProfileGetFriendsSaveData,
} from './action-types';

// Friends
export const profileGetFriends: TProfileGetFriends = (data) => ({
  type: PROFILE_GET_FRIENDS,
  payload: data,
});

export const profileGetFriendsPending: TProfileGetFriendsPending = (data) => ({
  type: PROFILE_GET_FRIENDS_PENDING,
  payload: data,
});

export const profileGetFriendsSuccess: TProfileGetFriendsSuccess = (data) => ({
  type: PROFILE_GET_FRIENDS_SUCCESS,
  payload: data,
});

export const profileGetFriendsError: TProfileGetFriendsError = (data) => ({
  type: PROFILE_GET_FRIENDS_ERROR,
  payload: data,
});

export const profileGetFriendsSaveData: TProfileGetFriendsSaveData = (data) => ({
  type: PROFILE_GET_FRIENDS_SAVE_DATA,
  payload: data,
});
