/* eslint-disable @typescript-eslint/indent */
// Modules Types
import { ActionCreator } from '@type/actions';
// Constants
import {
  PROFILE_GET_FRIENDS,
  PROFILE_GET_FRIENDS_PENDING,
  PROFILE_GET_FRIENDS_SUCCESS,
  PROFILE_GET_FRIENDS_ERROR,
  PROFILE_GET_FRIENDS_SAVE_DATA,
} from '@constants/profile/friends';

// Friends
export type TProfileGetFriends = ActionCreator<typeof PROFILE_GET_FRIENDS, Record<string, string>>;
export type TProfileGetFriendsSuccess = ActionCreator<
  typeof PROFILE_GET_FRIENDS_SUCCESS,
  { status: number }
>;
export type TProfileGetFriendsError = ActionCreator<
  typeof PROFILE_GET_FRIENDS_ERROR,
  { status: number }
>;
export type TProfileGetFriendsPending = ActionCreator<typeof PROFILE_GET_FRIENDS_PENDING>;

export type TProfileGetFriendsSaveData = ActionCreator<
  typeof PROFILE_GET_FRIENDS_SAVE_DATA,
  { status: number }
>;
