/* eslint-disable @typescript-eslint/indent */
// Modules Types
import { ActionCreator } from '@type/actions';
// Constants
import {
  // Get
  PROFILE_GET_FAVORITE,
  PROFILE_GET_FAVORITE_PRODUCTS,
  PROFILE_GET_FAVORITE_PRODUCTS_PENDING,
  PROFILE_GET_FAVORITE_PRODUCTS_SUCCESS,
  PROFILE_GET_FAVORITE_PRODUCTS_ERROR,
  PROFILE_GET_FAVORITE_PRODUCTS_SAVE_DATA,
  PROFILE_GET_FAVORITE_RESET_DATA,
  // Set
  PROFILE_SET_FAVOURITE_PRODUCTS,
  PROFILE_SET_FAVOURITE_PRODUCTS_PENDING,
  PROFILE_SET_FAVOURITE_PRODUCTS_SUCCESS,
  PROFILE_SET_FAVOURITE_PRODUCTS_ERROR,
  PROFILE_SET_FAVOURITE_PRODUCTS_SAVE_DATA,
  // Delete
  PROFILE_DELETE_FAVOURITE_PRODUCTS,
  PROFILE_DELETE_FAVOURITE_PRODUCTS_PENDING,
  PROFILE_DELETE_FAVOURITE_PRODUCTS_SUCCESS,
  PROFILE_DELETE_FAVOURITE_PRODUCTS_ERROR,
  PROFILE_DELETE_FAVOURITE_PRODUCTS_SAVE_DATA,
} from '@constants/profile/favourite/products';
import {
  // Get
  PROFILE_GET_FAVORITE_RESTS,
  PROFILE_GET_FAVORITE_RESTS_PENDING,
  PROFILE_GET_FAVORITE_RESTS_SUCCESS,
  PROFILE_GET_FAVORITE_RESTS_ERROR,
  PROFILE_GET_FAVORITE_RESTS_SAVE_DATA,
  // Set
  PROFILE_SET_FAVOURITE_REST,
  PROFILE_SET_FAVOURITE_REST_PENDING,
  PROFILE_SET_FAVOURITE_REST_SUCCESS,
  PROFILE_SET_FAVOURITE_REST_ERROR,
  PROFILE_SET_FAVOURITE_REST_SAVE_DATA,
  // Delete
  PROFILE_DELETE_FAVOURITE_REST,
  PROFILE_DELETE_FAVOURITE_REST_PENDING,
  PROFILE_DELETE_FAVOURITE_REST_SUCCESS,
  PROFILE_DELETE_FAVOURITE_REST_ERROR,
  PROFILE_DELETE_FAVOURITE_REST_SAVE_DATA,
} from '@constants/profile/favourite/rests';
// Main types
import { TRestaurantList } from '@type/restaurant';
import { TFavoriteRestaurant, TFavoriteProduct } from '@type/new-favorites';

// User favorites
export type TProfileGetFavorite = ActionCreator<
  typeof PROFILE_GET_FAVORITE,
  Record<string, string>
>;

export type TProfileGetFavoriteProducts = ActionCreator<
  typeof PROFILE_GET_FAVORITE_PRODUCTS,
  Record<string, string>
>;

export type TProfileGetFavoriteProductsPending = ActionCreator<
  typeof PROFILE_GET_FAVORITE_PRODUCTS_PENDING
>;

export type TProfileGetFavoriteProductsSuccess = ActionCreator<
  typeof PROFILE_GET_FAVORITE_PRODUCTS_SUCCESS,
  { status: string | number }
>;

export type TProfileGetFavoriteProductsError = ActionCreator<
  typeof PROFILE_GET_FAVORITE_PRODUCTS_ERROR,
  { status: string | number }
>;

export type TProfileGetFavoriteProductsSaveData = ActionCreator<
  typeof PROFILE_GET_FAVORITE_PRODUCTS_SAVE_DATA,
  TRestaurantList
>;

export type TProfileGetFavoriteResetData = ActionCreator<typeof PROFILE_GET_FAVORITE_RESET_DATA>;

export type TProfileSetFavoriteProducts = ActionCreator<
  typeof PROFILE_SET_FAVOURITE_PRODUCTS,
  {
    productId: string;
    newProduct: TFavoriteProduct;
  }
>;

export type TProfileSetFavoriteProductsPending = ActionCreator<
  typeof PROFILE_SET_FAVOURITE_PRODUCTS_PENDING
>;

export type TProfileSetFavoriteProductsSuccess = ActionCreator<
  typeof PROFILE_SET_FAVOURITE_PRODUCTS_SUCCESS,
  { status: string | number }
>;

export type TProfileSetFavoriteProductsError = ActionCreator<
  typeof PROFILE_SET_FAVOURITE_PRODUCTS_ERROR,
  { status: string | number }
>;

export type TProfileSetFavoriteProductsSaveData = ActionCreator<
  typeof PROFILE_SET_FAVOURITE_PRODUCTS_SAVE_DATA,
  {
    newProduct: TFavoriteProduct;
  }
>;

export type TProfileDeleteFavoriteProducts = ActionCreator<
  typeof PROFILE_DELETE_FAVOURITE_PRODUCTS,
  {
    productId: string;
  }
>;

export type TProfileDeleteFavoriteProductsPending = ActionCreator<
  typeof PROFILE_DELETE_FAVOURITE_PRODUCTS_PENDING
>;

export type TProfileDeleteFavoriteProductsSuccess = ActionCreator<
  typeof PROFILE_DELETE_FAVOURITE_PRODUCTS_SUCCESS,
  { status: string | number }
>;

export type TProfileDeleteFavoriteProductsError = ActionCreator<
  typeof PROFILE_DELETE_FAVOURITE_PRODUCTS_ERROR,
  { status: string | number }
>;

export type TProfileDeleteFavoriteProductsSaveData = ActionCreator<
  typeof PROFILE_DELETE_FAVOURITE_PRODUCTS_SAVE_DATA,
  {
    productId: string;
  }
>;

export type TProfileGetFavoriteRests = ActionCreator<
  typeof PROFILE_GET_FAVORITE_RESTS,
  Record<string, string>
>;

export type TProfileGetFavoriteRestsPending = ActionCreator<
  typeof PROFILE_GET_FAVORITE_RESTS_PENDING
>;

export type TProfileGetFavoriteRestsSuccess = ActionCreator<
  typeof PROFILE_GET_FAVORITE_RESTS_SUCCESS,
  { status: string | number }
>;

export type TProfileGetFavoriteRestsError = ActionCreator<
  typeof PROFILE_GET_FAVORITE_RESTS_ERROR,
  { status: string | number }
>;

export type TProfileGetFavoriteRestsSaveData = ActionCreator<
  typeof PROFILE_GET_FAVORITE_RESTS_SAVE_DATA,
  TRestaurantList
>;

export type TProfileSetFavoriteRests = ActionCreator<
  typeof PROFILE_SET_FAVOURITE_REST,
  {
    restId: string;
    newRestaurant: TFavoriteRestaurant;
  }
>;

export type TProfileSetFavoriteRestsPending = ActionCreator<
  typeof PROFILE_SET_FAVOURITE_REST_PENDING
>;

export type TProfileSetFavoriteRestsSuccess = ActionCreator<
  typeof PROFILE_SET_FAVOURITE_REST_SUCCESS,
  { status: string | number }
>;

export type TProfileSetFavoriteRestsError = ActionCreator<
  typeof PROFILE_SET_FAVOURITE_REST_ERROR,
  { status: string | number }
>;

export type TProfileSetFavoriteRestsSaveData = ActionCreator<
  typeof PROFILE_SET_FAVOURITE_REST_SAVE_DATA,
  {
    newRestaurant: TFavoriteRestaurant;
  }
>;

export type TProfileDeleteFavoriteRests = ActionCreator<
  typeof PROFILE_DELETE_FAVOURITE_REST,
  {
    restId: string;
  }
>;

export type TProfileDeleteFavoriteRestsPending = ActionCreator<
  typeof PROFILE_DELETE_FAVOURITE_REST_PENDING
>;

export type TProfileDeleteFavoriteRestsSuccess = ActionCreator<
  typeof PROFILE_DELETE_FAVOURITE_REST_SUCCESS,
  { status: string | number }
>;

export type TProfileDeleteFavoriteRestsError = ActionCreator<
  typeof PROFILE_DELETE_FAVOURITE_REST_ERROR,
  { status: string | number }
>;

export type TProfileDeleteFavoriteRestsSaveData = ActionCreator<
  typeof PROFILE_DELETE_FAVOURITE_REST_SAVE_DATA,
  {
    restId: string;
  }
>;
