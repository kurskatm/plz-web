// Constants
import {
  // Get
  PROFILE_GET_FAVORITE,
  PROFILE_GET_FAVORITE_PRODUCTS,
  PROFILE_GET_FAVORITE_PRODUCTS_PENDING,
  PROFILE_GET_FAVORITE_PRODUCTS_SUCCESS,
  PROFILE_GET_FAVORITE_PRODUCTS_ERROR,
  PROFILE_GET_FAVORITE_PRODUCTS_SAVE_DATA,
  PROFILE_GET_FAVORITE_RESET_DATA,
  // Set
  PROFILE_SET_FAVOURITE_PRODUCTS,
  PROFILE_SET_FAVOURITE_PRODUCTS_PENDING,
  PROFILE_SET_FAVOURITE_PRODUCTS_SUCCESS,
  PROFILE_SET_FAVOURITE_PRODUCTS_ERROR,
  PROFILE_SET_FAVOURITE_PRODUCTS_SAVE_DATA,
  // Delete
  PROFILE_DELETE_FAVOURITE_PRODUCTS,
  PROFILE_DELETE_FAVOURITE_PRODUCTS_PENDING,
  PROFILE_DELETE_FAVOURITE_PRODUCTS_SUCCESS,
  PROFILE_DELETE_FAVOURITE_PRODUCTS_ERROR,
  PROFILE_DELETE_FAVOURITE_PRODUCTS_SAVE_DATA,
} from '@constants/profile/favourite/products';
import {
  // Get
  PROFILE_GET_FAVORITE_RESTS,
  PROFILE_GET_FAVORITE_RESTS_PENDING,
  PROFILE_GET_FAVORITE_RESTS_SUCCESS,
  PROFILE_GET_FAVORITE_RESTS_ERROR,
  PROFILE_GET_FAVORITE_RESTS_SAVE_DATA,
  // Set
  PROFILE_SET_FAVOURITE_REST,
  PROFILE_SET_FAVOURITE_REST_PENDING,
  PROFILE_SET_FAVOURITE_REST_SUCCESS,
  PROFILE_SET_FAVOURITE_REST_ERROR,
  PROFILE_SET_FAVOURITE_REST_SAVE_DATA,
  // Delete
  PROFILE_DELETE_FAVOURITE_REST,
  PROFILE_DELETE_FAVOURITE_REST_PENDING,
  PROFILE_DELETE_FAVOURITE_REST_SUCCESS,
  PROFILE_DELETE_FAVOURITE_REST_ERROR,
  PROFILE_DELETE_FAVOURITE_REST_SAVE_DATA,
} from '@constants/profile/favourite/rests';
// Types
import {
  // Get products
  TProfileGetFavorite,
  TProfileGetFavoriteProducts,
  TProfileGetFavoriteProductsPending,
  TProfileGetFavoriteProductsSuccess,
  TProfileGetFavoriteProductsError,
  TProfileGetFavoriteProductsSaveData,
  TProfileGetFavoriteResetData,
  // Set products
  TProfileSetFavoriteProducts,
  TProfileSetFavoriteProductsPending,
  TProfileSetFavoriteProductsSuccess,
  TProfileSetFavoriteProductsError,
  TProfileSetFavoriteProductsSaveData,
  // Delete products
  TProfileDeleteFavoriteProducts,
  TProfileDeleteFavoriteProductsPending,
  TProfileDeleteFavoriteProductsSuccess,
  TProfileDeleteFavoriteProductsError,
  TProfileDeleteFavoriteProductsSaveData,
  // Get rests
  TProfileGetFavoriteRests,
  TProfileGetFavoriteRestsPending,
  TProfileGetFavoriteRestsSuccess,
  TProfileGetFavoriteRestsError,
  TProfileGetFavoriteRestsSaveData,
  // Set rests
  TProfileSetFavoriteRests,
  TProfileSetFavoriteRestsPending,
  TProfileSetFavoriteRestsSuccess,
  TProfileSetFavoriteRestsError,
  TProfileSetFavoriteRestsSaveData,
  // Delete rests
  TProfileDeleteFavoriteRests,
  TProfileDeleteFavoriteRestsPending,
  TProfileDeleteFavoriteRestsSuccess,
  TProfileDeleteFavoriteRestsError,
  TProfileDeleteFavoriteRestsSaveData,
} from './action-types';

export const profileGetFavorite: TProfileGetFavorite = (data) => ({
  type: PROFILE_GET_FAVORITE,
  payload: data,
});

export const profileGetFavouriteResetData: TProfileGetFavoriteResetData = () => ({
  type: PROFILE_GET_FAVORITE_RESET_DATA,
});

// Get favorite rests
export const profileGetFavoriteRests: TProfileGetFavoriteRests = (data) => ({
  type: PROFILE_GET_FAVORITE_RESTS,
  payload: data,
});
export const profileGetFavoriteRestsPending: TProfileGetFavoriteRestsPending = () => ({
  type: PROFILE_GET_FAVORITE_RESTS_PENDING,
});

export const profileGetFavoriteRestsSuccess: TProfileGetFavoriteRestsSuccess = ({ status }) => ({
  type: PROFILE_GET_FAVORITE_RESTS_SUCCESS,
  payload: { status },
});

export const profileGetFavoriteRestsError: TProfileGetFavoriteRestsError = ({ status }) => ({
  type: PROFILE_GET_FAVORITE_RESTS_ERROR,
  payload: { status },
});

export const profileGetFavoriteRestsSaveData: TProfileGetFavoriteRestsSaveData = (data) => ({
  type: PROFILE_GET_FAVORITE_RESTS_SAVE_DATA,
  payload: data,
});
// Get favorite products
export const profileGetFavoriteProduct: TProfileGetFavoriteProducts = (data) => ({
  type: PROFILE_GET_FAVORITE_PRODUCTS,
  payload: data,
});
export const profileGetFavoriteProductPending: TProfileGetFavoriteProductsPending = () => ({
  type: PROFILE_GET_FAVORITE_PRODUCTS_PENDING,
});

export const profileGetFavoriteProductSuccess: TProfileGetFavoriteProductsSuccess = ({
  status,
}) => ({
  type: PROFILE_GET_FAVORITE_PRODUCTS_SUCCESS,
  payload: { status },
});

export const profileGetFavoriteProductError: TProfileGetFavoriteProductsError = ({ status }) => ({
  type: PROFILE_GET_FAVORITE_PRODUCTS_ERROR,
  payload: { status },
});

export const profileGetFavoriteProductSaveData: TProfileGetFavoriteProductsSaveData = (data) => ({
  type: PROFILE_GET_FAVORITE_PRODUCTS_SAVE_DATA,
  payload: data,
});

export const profileSetFavoriteProduct: TProfileSetFavoriteProducts = (data) => ({
  type: PROFILE_SET_FAVOURITE_PRODUCTS,
  payload: data,
});
export const profileSetFavoriteProductPending: TProfileSetFavoriteProductsPending = () => ({
  type: PROFILE_SET_FAVOURITE_PRODUCTS_PENDING,
});

export const profileSetFavoriteProductSuccess: TProfileSetFavoriteProductsSuccess = ({
  status,
}) => ({
  type: PROFILE_SET_FAVOURITE_PRODUCTS_SUCCESS,
  payload: { status },
});

export const profileSetFavoriteProductError: TProfileSetFavoriteProductsError = ({ status }) => ({
  type: PROFILE_SET_FAVOURITE_PRODUCTS_ERROR,
  payload: { status },
});

export const profileSetFavoriteProductSaveData: TProfileSetFavoriteProductsSaveData = (data) => ({
  type: PROFILE_SET_FAVOURITE_PRODUCTS_SAVE_DATA,
  payload: data,
});
export const profileDeleteFavoriteProduct: TProfileDeleteFavoriteProducts = (data) => ({
  type: PROFILE_DELETE_FAVOURITE_PRODUCTS,
  payload: data,
});
export const profileDeleteFavoriteProductPending: TProfileDeleteFavoriteProductsPending = () => ({
  type: PROFILE_DELETE_FAVOURITE_PRODUCTS_PENDING,
});

export const profileDeleteFavoriteProductSuccess: TProfileDeleteFavoriteProductsSuccess = ({
  status,
}) => ({
  type: PROFILE_DELETE_FAVOURITE_PRODUCTS_SUCCESS,
  payload: { status },
});

export const profileDeleteFavoriteProductError: TProfileDeleteFavoriteProductsError = ({
  status,
}) => ({
  type: PROFILE_DELETE_FAVOURITE_PRODUCTS_ERROR,
  payload: { status },
});

export const profileDeleteFavoriteProductSaveData: TProfileDeleteFavoriteProductsSaveData = (
  data,
) => ({
  type: PROFILE_DELETE_FAVOURITE_PRODUCTS_SAVE_DATA,
  payload: data,
});

export const profileSetFavoriteRests: TProfileSetFavoriteRests = (data) => ({
  type: PROFILE_SET_FAVOURITE_REST,
  payload: data,
});
export const profileSetFavoriteRestsPending: TProfileSetFavoriteRestsPending = () => ({
  type: PROFILE_SET_FAVOURITE_REST_PENDING,
});

export const profileSetFavoriteRestsSuccess: TProfileSetFavoriteRestsSuccess = ({ status }) => ({
  type: PROFILE_SET_FAVOURITE_REST_SUCCESS,
  payload: { status },
});

export const profileSetFavoriteRestsError: TProfileSetFavoriteRestsError = ({ status }) => ({
  type: PROFILE_SET_FAVOURITE_REST_ERROR,
  payload: { status },
});

export const profileSetFavoriteRestsSaveData: TProfileSetFavoriteRestsSaveData = (data) => ({
  type: PROFILE_SET_FAVOURITE_REST_SAVE_DATA,
  payload: data,
});
export const profileDeleteFavoriteRests: TProfileDeleteFavoriteRests = (data) => ({
  type: PROFILE_DELETE_FAVOURITE_REST,
  payload: data,
});
export const profileDeleteFavoriteRestsPending: TProfileDeleteFavoriteRestsPending = () => ({
  type: PROFILE_DELETE_FAVOURITE_REST_PENDING,
});

export const profileDeleteFavoriteRestsSuccess: TProfileDeleteFavoriteRestsSuccess = ({
  status,
}) => ({
  type: PROFILE_DELETE_FAVOURITE_REST_SUCCESS,
  payload: { status },
});

export const profileDeleteFavoriteRestsError: TProfileDeleteFavoriteRestsError = ({ status }) => ({
  type: PROFILE_DELETE_FAVOURITE_REST_ERROR,
  payload: { status },
});

export const profileDeleteFavoriteRestsSaveData: TProfileDeleteFavoriteRestsSaveData = (data) => ({
  type: PROFILE_DELETE_FAVOURITE_REST_SAVE_DATA,
  payload: data,
});
