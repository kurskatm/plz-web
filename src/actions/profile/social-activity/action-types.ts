/* eslint-disable @typescript-eslint/indent */
// Modules Types
import { ActionCreator } from '@type/actions';
// Constants
import {
  PROFILE_SET_SOCIAL_ACTIVITY,
  PROFILE_SET_SOCIAL_ACTIVITY_PENDING,
  PROFILE_SET_SOCIAL_ACTIVITY_SUCCESS,
  PROFILE_SET_SOCIAL_ACTIVITY_ERROR,
  PROFILE_SET_SOCIAL_ACTIVITY_SAVE_DATA,
} from '@constants/profile/social-activity';

export type TProfileSetSocialActivity = ActionCreator<
  typeof PROFILE_SET_SOCIAL_ACTIVITY,
  {
    type: number;
    access_token: string;
  }
>;

export type TProfileSetSocialActivityPending = ActionCreator<
  typeof PROFILE_SET_SOCIAL_ACTIVITY_PENDING
>;

export type TProfileSetSocialActivitySuccess = ActionCreator<
  typeof PROFILE_SET_SOCIAL_ACTIVITY_SUCCESS,
  { status: number }
>;

export type TProfileSetSocialActivityError = ActionCreator<
  typeof PROFILE_SET_SOCIAL_ACTIVITY_ERROR,
  { status: number }
>;

export type TProfileSetSocialActivitySaveData = ActionCreator<
  typeof PROFILE_SET_SOCIAL_ACTIVITY_SAVE_DATA,
  {
    type: number;
  }
>;
