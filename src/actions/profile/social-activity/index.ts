// Constants
import {
  PROFILE_SET_SOCIAL_ACTIVITY,
  PROFILE_SET_SOCIAL_ACTIVITY_PENDING,
  PROFILE_SET_SOCIAL_ACTIVITY_SUCCESS,
  PROFILE_SET_SOCIAL_ACTIVITY_ERROR,
  PROFILE_SET_SOCIAL_ACTIVITY_SAVE_DATA,
} from '@constants/profile/social-activity';
// Types
import {
  TProfileSetSocialActivity,
  TProfileSetSocialActivityPending,
  TProfileSetSocialActivitySuccess,
  TProfileSetSocialActivityError,
  TProfileSetSocialActivitySaveData,
} from './action-types';

export const profileSetSocialActivity: TProfileSetSocialActivity = (data) => ({
  type: PROFILE_SET_SOCIAL_ACTIVITY,
  payload: data,
});

export const profileSetSocialActivityPending: TProfileSetSocialActivityPending = () => ({
  type: PROFILE_SET_SOCIAL_ACTIVITY_PENDING,
});

export const profileSetSocialActivitySuccess: TProfileSetSocialActivitySuccess = (data) => ({
  type: PROFILE_SET_SOCIAL_ACTIVITY_SUCCESS,
  payload: data,
});

export const profileSetSocialActivityError: TProfileSetSocialActivityError = (data) => ({
  type: PROFILE_SET_SOCIAL_ACTIVITY_ERROR,
  payload: data,
});

export const profileSetSocialActivitySaveData: TProfileSetSocialActivitySaveData = (data) => ({
  type: PROFILE_SET_SOCIAL_ACTIVITY_SAVE_DATA,
  payload: data,
});
