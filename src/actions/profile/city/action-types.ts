/* eslint-disable @typescript-eslint/indent */
// Modules Types
import { ActionCreator } from '@type/actions';
// Constants
import {
  PROFILE_CITY_UPDATE,
  PROFILE_RESET_DATA_AFTER_CITY_UPDATE,
  PROFILE_CITY_AND_ADDRESS_UPDATE,
} from '@constants/profile/city';

// City
export type TProfileCityUpdate = ActionCreator<typeof PROFILE_CITY_UPDATE, string | null>;

export type TProfileResetDataAferCityUpdate = ActionCreator<
  typeof PROFILE_RESET_DATA_AFTER_CITY_UPDATE
>;

export type TProfileCityAndAddressUpdate = ActionCreator<typeof PROFILE_CITY_AND_ADDRESS_UPDATE>;
