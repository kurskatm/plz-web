// Constants
import {
  PROFILE_CITY_UPDATE,
  PROFILE_RESET_DATA_AFTER_CITY_UPDATE,
  PROFILE_CITY_AND_ADDRESS_UPDATE,
} from '@constants/profile/city';
// Types
import {
  TProfileCityUpdate,
  TProfileResetDataAferCityUpdate,
  TProfileCityAndAddressUpdate,
} from './action-types';

// City
export const profileCityUpdate: TProfileCityUpdate = (data) => ({
  type: PROFILE_CITY_UPDATE,
  payload: data,
});

export const profileResetDataAferCityUpdate: TProfileResetDataAferCityUpdate = () => ({
  type: PROFILE_RESET_DATA_AFTER_CITY_UPDATE,
});

export const profileCityAndAddressUpdate: TProfileCityAndAddressUpdate = () => ({
  type: PROFILE_CITY_AND_ADDRESS_UPDATE,
});
