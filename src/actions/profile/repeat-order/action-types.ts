/* eslint-disable @typescript-eslint/indent */
// Modules Types
import { ActionCreator } from '@type/actions';
import { TOrderToCart } from '@type/orderFromProfileToCart';
// Constants
import {
  PROFILE_ORDER_REPEAT_ORDER,
  PROFILE_ORDER_REPEAT_ORDER_PENDING,
  PROFILE_ORDER_REPEAT_ORDER_SUCCESS,
  PROFILE_ORDER_REPEAT_ORDER_ERROR,
  PROFILE_ORDER_REPEAT_ORDER_SAVE,
  PROFILE_ORDER_REPEAT_ORDER_RESET,
  PROFILE_ORDER_REPEAT_ORDER_RESET_REDIRECT,
} from '@constants/profile/orders/repeat-order';
import { TShopingCartModel } from '@/models/shoping-cart/types';

export type TProfileOrderRepeatOrder = ActionCreator<
  typeof PROFILE_ORDER_REPEAT_ORDER,
  {
    orderId: string;
    lat: number;
    lng: number;
    shopingCart: TShopingCartModel;
    cityId: string;
  }
>;

export type TProfileOrderRepeatOrderPending = ActionCreator<
  typeof PROFILE_ORDER_REPEAT_ORDER_PENDING
>;

export type TProfileOrderRepeatOrderSuccess = ActionCreator<
  typeof PROFILE_ORDER_REPEAT_ORDER_SUCCESS,
  { status: number }
>;

export type TProfileOrderRepeatOrderError = ActionCreator<
  typeof PROFILE_ORDER_REPEAT_ORDER_ERROR,
  { status: number }
>;

export type TProfileOrderRepeatOrderSave = ActionCreator<
  typeof PROFILE_ORDER_REPEAT_ORDER_SAVE,
  { data: TOrderToCart }
>;

export type TProfileOrderRepeatOrderReset = ActionCreator<typeof PROFILE_ORDER_REPEAT_ORDER_RESET>;

export type TProfileOrderRepeatOrderResetRedirect = ActionCreator<
  typeof PROFILE_ORDER_REPEAT_ORDER_RESET_REDIRECT
>;
