// Constants
import {
  PROFILE_ORDER_REPEAT_ORDER,
  PROFILE_ORDER_REPEAT_ORDER_PENDING,
  PROFILE_ORDER_REPEAT_ORDER_SUCCESS,
  PROFILE_ORDER_REPEAT_ORDER_ERROR,
  PROFILE_ORDER_REPEAT_ORDER_SAVE,
  PROFILE_ORDER_REPEAT_ORDER_RESET,
  PROFILE_ORDER_REPEAT_ORDER_RESET_REDIRECT,
} from '@constants/profile/orders/repeat-order';
// Types
import {
  TProfileOrderRepeatOrder,
  TProfileOrderRepeatOrderPending,
  TProfileOrderRepeatOrderSuccess,
  TProfileOrderRepeatOrderError,
  TProfileOrderRepeatOrderSave,
  TProfileOrderRepeatOrderReset,
  TProfileOrderRepeatOrderResetRedirect,
} from './action-types';

export const profileOrderRepeatOrder: TProfileOrderRepeatOrder = (data) => ({
  type: PROFILE_ORDER_REPEAT_ORDER,
  payload: data,
});

export const profileOrderRepeatOrderPending: TProfileOrderRepeatOrderPending = () => ({
  type: PROFILE_ORDER_REPEAT_ORDER_PENDING,
});

export const profileOrderRepeatOrderSuccess: TProfileOrderRepeatOrderSuccess = (data) => ({
  type: PROFILE_ORDER_REPEAT_ORDER_SUCCESS,
  payload: data,
});

export const profileOrderRepeatOrderError: TProfileOrderRepeatOrderError = (data) => ({
  type: PROFILE_ORDER_REPEAT_ORDER_ERROR,
  payload: data,
});

export const profileOrderRepeatOrderSave: TProfileOrderRepeatOrderSave = (data) => ({
  type: PROFILE_ORDER_REPEAT_ORDER_SAVE,
  payload: data,
});

export const profileOrderRepeatOrderReset: TProfileOrderRepeatOrderReset = () => ({
  type: PROFILE_ORDER_REPEAT_ORDER_RESET,
});

export const profileOrderRepeatOrderResetRedirect: TProfileOrderRepeatOrderResetRedirect = () => ({
  type: PROFILE_ORDER_REPEAT_ORDER_RESET_REDIRECT,
});
