/* eslint-disable @typescript-eslint/indent */
// Modules Types
import { ActionCreator } from '@type/actions';
// Constants
import {
  PROFILE_RESET_ALL_UI_MODELS,
  // City
  PROFILE_CITY_UPDATE,
  // Delivery
  PROFILE_UPDATE_ADDRESS,
  PROFILE_UPDATE_PICKUP,
  // User Geocode
  PROFILE_GET_ADDRESSES_GEOCODE,
  PROFILE_SAVE_ADDRESSES_GEOCODE,
  PROFILE_GET_ADDRESSES_GEOCODE_PENDING,
  PROFILE_GET_ADDRESSES_GEOCODE_SUCCESS,
  PROFILE_GET_ADDRESSES_GEOCODE_ERROR,
  PROFILE_RESET_ADDRESSES_GEOCODE,
  // User favorites
  PROFILE_GET_FAVORITE_RESTS,
  PROFILE_GET_FAVORITE_RESTS_PENDING,
  PROFILE_GET_FAVORITE_RESTS_SUCCESS,
  PROFILE_GET_FAVORITE_RESTS_ERROR,
  PROFILE_GET_FAVORITE_RESTS_SAVE_DATA,
  PROFILE_GET_FAVORITE_PRODUCTS_SAVE_DATA,

  // User info
  PROFILE_GET_INFO,
  PROFILE_GET_INFO_SUCCESS,
  PROFILE_GET_INFO_ERROR,
  PROFILE_UPDATE_INFO,
  PROFILE_UPDATE_INFO_SUCCESS,
  PROFILE_UPDATE_INFO_ERROR,

  // Reviews
  PROFILE_GET_REVIEWS,
  PROFILE_GET_REVIEWS_SUCCESS,
  PROFILE_GET_REVIEWS_PENDING,
  PROFILE_GET_REVIEWS_ERROR,
  PROFILE_GET_REVIEWS_SAVE_DATA,

  // Friends
  PROFILE_GET_FRIENDS,
  PROFILE_GET_FRIENDS_SUCCESS,
  PROFILE_GET_FRIENDS_PENDING,
  PROFILE_GET_FRIENDS_ERROR,
  PROFILE_GET_FRIENDS_SAVE_DATA,

  // Orders
  PROFILE_GET_ORDERS,
  PROFILE_GET_ORDERS_PENDING,
  PROFILE_GET_ORDERS_SUCCESS,
  PROFILE_GET_ORDERS_ERROR,
  PROFILE_GET_ORDERS_SAVE_DATA,
  PROFILE_SHOW_ORDER_REVIEW_MODAL,

  // Last order without review
  PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW,
  PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_PENDING,
  PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_SUCCESS,
  PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_ERROR,
  PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_SAVE_DATA,

  // Orders Reviews
  PROFILE_SET_ORDER_REVIEW,
  PROFILE_SET_ORDER_REVIEW_PENDING,
  PROFILE_SET_ORDER_REVIEW_SUCCESS,
  PROFILE_SET_ORDER_REVIEW_ERROR,
  PROFILE_SAVE_ORDER_REVIEW_DATA,

  // Current Order
  PROFILE_GET_USER_ORDER,
  PROFILE_GET_USER_ORDER_PENDING,
  PROFILE_GET_USER_ORDER_SUCCESS,
  PROFILE_GET_USER_ORDER_ERROR,
  PROFILE_GET_USER_ORDER_SAVE_DATA,

  // Rests Info
  PROFILE_GET_RESTS_INFO,
  PROFILE_GET_RESTS_INFO_PENDING,
  PROFILE_GET_RESTS_INFO_SUCCESS,
  PROFILE_GET_RESTS_INFO_ERROR,
  PROFILE_GET_RESTS_INFO_SAVE_DATA,
  PROFILE_UPDATE_INFO_PENDING,
} from '@constants/profile';
// Main Types
import { TCloseList } from '@lib/header/address-manager/types';
import { TNewGeocodeData } from '@type/new-geocode';
import { TRestaurantList, TRestaurantsInfoArray } from '@type/restaurant';
import { TUserOrdersData, TUserOrder, TOrderReview } from '@type/profile';

export type TProfileResetAllUiModels = ActionCreator<typeof PROFILE_RESET_ALL_UI_MODELS>;

// City
export type TProfileCityUpdate = ActionCreator<typeof PROFILE_CITY_UPDATE, string>;

// Delivery
export type TProfileUpdateAddress = ActionCreator<
  typeof PROFILE_UPDATE_ADDRESS,
  { name: string; description: string }
>;

export type TProfileUpdatePickup = ActionCreator<typeof PROFILE_UPDATE_PICKUP, boolean>;

// User Geocode
export type TProfileGetAddressGeocode = ActionCreator<
  typeof PROFILE_GET_ADDRESSES_GEOCODE,
  { closeList?: TCloseList; name: string; description: string; cityId: string }
>;

export type TProfileGSaveAddressGeocode = ActionCreator<
  typeof PROFILE_SAVE_ADDRESSES_GEOCODE,
  TNewGeocodeData
>;

export type TProfileGetAddressGeocodePending = ActionCreator<
  typeof PROFILE_GET_ADDRESSES_GEOCODE_PENDING
>;

export type TProfileGetAddressGeocodeSuccess = ActionCreator<
  typeof PROFILE_GET_ADDRESSES_GEOCODE_SUCCESS,
  { status: string | number }
>;

export type TProfileGetAddressGeocodeError = ActionCreator<
  typeof PROFILE_GET_ADDRESSES_GEOCODE_ERROR,
  { status: string | number }
>;

export type TProfileResetAddressGeocode = ActionCreator<typeof PROFILE_RESET_ADDRESSES_GEOCODE>;

// User favorites
export type TProfileGetFavoriteRests = ActionCreator<
  typeof PROFILE_GET_FAVORITE_RESTS,
  Record<string, string>
>;

export type TProfileGetFavoriteRestsPending = ActionCreator<
  typeof PROFILE_GET_FAVORITE_RESTS_PENDING
>;

export type TProfileGetFavoriteRestsSuccess = ActionCreator<
  typeof PROFILE_GET_FAVORITE_RESTS_SUCCESS,
  { status: string | number }
>;

export type TProfileGetFavoriteRestsError = ActionCreator<
  typeof PROFILE_GET_FAVORITE_RESTS_ERROR,
  { status: string | number }
>;

export type TProfileGetFavoriteSaveData = ActionCreator<
  typeof PROFILE_GET_FAVORITE_RESTS_SAVE_DATA,
  TRestaurantList
>;

export type TProfileGetFavoriteProductsSaveData = ActionCreator<
  typeof PROFILE_GET_FAVORITE_PRODUCTS_SAVE_DATA,
  TRestaurantList
>;

export type TProfileInfo = ActionCreator<
  typeof PROFILE_GET_INFO,
  {
    id: string;
    fullName: string;
    dateOfBirth: string;
    email: string;
    phoneNumber: string;
    gender: string;
    avatar: string;
    invitationId: string;
  }
>;

export type TProfileAuth = ActionCreator<
  typeof PROFILE_GET_INFO,
  {
    userId?: string;
    access_token?: string;
  }
>;
export type TProfileGetInfo = ActionCreator<typeof PROFILE_GET_INFO, Record<string, string>>;
export type TProfileInfoSuccess = ActionCreator<
  typeof PROFILE_GET_INFO_SUCCESS,
  { status: number }
>;
export type TProfileInfoError = ActionCreator<typeof PROFILE_GET_INFO_ERROR, { status: number }>;

export type TProfileInfoUpdate = ActionCreator<
  typeof PROFILE_UPDATE_INFO,
  { access_token: string; payload: Record<string, unknown> }
>;
export type TProfileInfoUpdateSuccess = ActionCreator<
  typeof PROFILE_UPDATE_INFO_SUCCESS,
  { status: number }
>;
export type TProfileInfoUpdateError = ActionCreator<
  typeof PROFILE_UPDATE_INFO_ERROR,
  { status: number }
>;
export type TProfileInfoUpdatePending = ActionCreator<typeof PROFILE_UPDATE_INFO_PENDING>;

// Reviews
export type TProfileReview = ActionCreator<
  typeof PROFILE_GET_REVIEWS,
  {
    id: string;
    restaurantId: string;
    liked: boolean;
    rate: Record<string, unknown>;
    text: string;
    order: Record<string, unknown>[];
    addedOn: string;
    username: string;
    comments: Record<string, unknown>[];
  }
>;
export type TProfileGetReviews = ActionCreator<typeof PROFILE_GET_REVIEWS, string>;
export type TProfileGetReviewsSuccess = ActionCreator<
  typeof PROFILE_GET_REVIEWS_SUCCESS,
  { status: number }
>;
export type TProfileGetReviewsError = ActionCreator<
  typeof PROFILE_GET_REVIEWS_ERROR,
  { status: number }
>;
export type TProfileGetReviewsPending = ActionCreator<typeof PROFILE_GET_REVIEWS_PENDING>;

export type TProfileGetReviewsSaveData = ActionCreator<
  typeof PROFILE_GET_REVIEWS_SAVE_DATA,
  { status: number }
>;

// Friends
export type TProfileGetFriends = ActionCreator<typeof PROFILE_GET_FRIENDS, string>;
export type TProfileGetFriendsSuccess = ActionCreator<
  typeof PROFILE_GET_FRIENDS_SUCCESS,
  { status: number }
>;
export type TProfileGetFriendsError = ActionCreator<
  typeof PROFILE_GET_FRIENDS_ERROR,
  { status: number }
>;
export type TProfileGetFriendsPending = ActionCreator<typeof PROFILE_GET_FRIENDS_PENDING>;

export type TProfileGetFriendsSaveData = ActionCreator<
  typeof PROFILE_GET_FRIENDS_SAVE_DATA,
  { status: number }
>;

// Orders
export type TProfileGetOrders = ActionCreator<
  typeof PROFILE_GET_ORDERS,
  {
    pageNumber: number;
    pageSize: number;
    access_token?: string;
  }
>;

export type TProfileGetOrdersPending = ActionCreator<typeof PROFILE_GET_ORDERS_PENDING>;

export type TProfileGetOrdersSuccess = ActionCreator<
  typeof PROFILE_GET_ORDERS_SUCCESS,
  { status: number }
>;

export type TProfileGetOrdersError = ActionCreator<
  typeof PROFILE_GET_ORDERS_ERROR,
  { status: number }
>;

export type TProfileGetOrdersSaveData = ActionCreator<
  typeof PROFILE_GET_ORDERS_SAVE_DATA,
  { data: TUserOrdersData }
>;

export type TProfileShowOrdersReviewModal = ActionCreator<
  typeof PROFILE_SHOW_ORDER_REVIEW_MODAL,
  {
    orderId: string;
    showModal: boolean;
  }
>;

// Orders Reviews
export type TProfileSetOrderReview = ActionCreator<
  typeof PROFILE_SET_ORDER_REVIEW,
  { review: TOrderReview }
>;

export type TProfileSetOrderReviewPending = ActionCreator<typeof PROFILE_SET_ORDER_REVIEW_PENDING>;

export type TProfileSetOrderReviewSuccess = ActionCreator<
  typeof PROFILE_SET_ORDER_REVIEW_SUCCESS,
  { status: number }
>;

export type TProfileSetOrderReviewError = ActionCreator<
  typeof PROFILE_SET_ORDER_REVIEW_ERROR,
  { status: number }
>;

export type TProfileSaveOrderReviewData = ActionCreator<
  typeof PROFILE_SAVE_ORDER_REVIEW_DATA,
  { orderId: string }
>;

// Last order without review
export type TProfileGetLastOrderWithoutReview = ActionCreator<
  typeof PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW,
  { restaurantId?: string }
>;

export type TProfileGetLastOrderWithoutReviewPending = ActionCreator<
  typeof PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_PENDING
>;

export type TProfileGetLastOrderWithoutReviewSuccess = ActionCreator<
  typeof PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_SUCCESS,
  { status: number }
>;

export type TProfileGetLastOrderWithoutReviewError = ActionCreator<
  typeof PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_ERROR,
  { status: number }
>;

export type TProfileGetLastOrderWithoutReviewSaveData = ActionCreator<
  typeof PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW_SAVE_DATA,
  { order: TUserOrder }
>;

// Current Order
export type TProfileGetUserOrder = ActionCreator<
  typeof PROFILE_GET_USER_ORDER,
  { orderId: string }
>;

export type TProfileGetUserOrderPending = ActionCreator<typeof PROFILE_GET_USER_ORDER_PENDING>;

export type TProfileGetUserOrderSuccess = ActionCreator<
  typeof PROFILE_GET_USER_ORDER_SUCCESS,
  { status: number }
>;

export type TProfileGetUserOrderError = ActionCreator<
  typeof PROFILE_GET_USER_ORDER_ERROR,
  { status: number }
>;

export type TProfileGetUserOrderSaveData = ActionCreator<
  typeof PROFILE_GET_USER_ORDER_SAVE_DATA,
  { order: TUserOrder }
>;

// Rests info
export type TProfileGetRestsInfo = ActionCreator<typeof PROFILE_GET_RESTS_INFO, string[]>;

export type TProfileGetRestsInfoPending = ActionCreator<typeof PROFILE_GET_RESTS_INFO_PENDING>;

export type TProfileGetRestsInfoSuccess = ActionCreator<
  typeof PROFILE_GET_RESTS_INFO_SUCCESS,
  { status: number }
>;

export type TProfileGetRestsInfoError = ActionCreator<
  typeof PROFILE_GET_RESTS_INFO_ERROR,
  { status: number }
>;

export type TProfileGetRestsInfoSaveData = ActionCreator<
  typeof PROFILE_GET_RESTS_INFO_SAVE_DATA,
  { data: TRestaurantsInfoArray }
>;
