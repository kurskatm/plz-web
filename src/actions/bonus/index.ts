// Constants
import {
  // Summary
  BONUS_FETCH,
  BONUS_PENDING,
  BONUS_SUCCESS,
  BONUS_ERROR,
  BONUS_SAVE,
  // History
  BONUS_HISTORY_SUCCESS,
  BONUS_HISTORY_ERROR,
  BONUS_HISTORY_FETCH,
  BONUS_HISTORY_PENDING,
  BONUS_HISTORY_SAVE,
  BONUS_RESET_DATA,
} from '@constants/bonus';
// Types
import {
  // Summary
  TBonusFetch,
  TBonusPending,
  TBonusSuccess,
  TBonusError,
  TBonusSave,
  // History
  TBonusHistoryFetch,
  TBonusHistorySuccess,
  TBonusHistoryPending,
  TBonusHistoryError,
  TBonusHistorySave,
  TBonusResetData,
} from './action-types';

// Support
export const bonusFetch: TBonusFetch = (data) => ({
  type: BONUS_FETCH,
  payload: data,
});

export const bonusSave: TBonusSave = (data) => ({
  type: BONUS_SAVE,
  payload: data,
});

export const bonusPending: TBonusPending = () => ({
  type: BONUS_PENDING,
});

export const bonusSuccess: TBonusSuccess = ({ status }) => ({
  type: BONUS_SUCCESS,
  payload: { status },
});

export const bonusError: TBonusError = ({ status }) => ({
  type: BONUS_ERROR,
  payload: { status },
});

export const bonusHistoryFetch: TBonusHistoryFetch = (data) => ({
  type: BONUS_HISTORY_FETCH,
  payload: data,
});

export const bonusHistoryPending: TBonusHistoryPending = (data) => ({
  type: BONUS_HISTORY_PENDING,
  payload: data,
});

export const bonusHistorySuccess: TBonusHistorySuccess = (data) => ({
  type: BONUS_HISTORY_SUCCESS,
  payload: data,
});

export const bonusHistoryError: TBonusHistoryError = (data) => ({
  type: BONUS_HISTORY_ERROR,
  payload: data,
});

export const bonusHistorySave: TBonusHistorySave = (data) => ({
  type: BONUS_HISTORY_SAVE,
  payload: data,
});

export const bonusResetData: TBonusResetData = () => ({
  type: BONUS_RESET_DATA,
});
