/* eslint-disable @typescript-eslint/indent */
// Modules Types
import { ActionCreator } from '@type/actions';

// Constants
import {
  // Summary
  BONUS_FETCH,
  BONUS_PENDING,
  BONUS_SUCCESS,
  BONUS_ERROR,
  BONUS_SAVE,
  // History
  BONUS_HISTORY_FETCH,
  BONUS_HISTORY_PENDING,
  BONUS_HISTORY_SUCCESS,
  BONUS_HISTORY_ERROR,
  BONUS_HISTORY_SAVE,
  BONUS_RESET_DATA,
} from '@constants/bonus';

// get bonus
export type TBonusFetch = ActionCreator<typeof BONUS_FETCH, Record<string, string>>;
export type TBonusPending = ActionCreator<typeof BONUS_PENDING>;
export type TBonusSuccess = ActionCreator<typeof BONUS_SUCCESS, { status: string | number }>;
export type TBonusError = ActionCreator<typeof BONUS_ERROR, { status: string | number }>;
export type TBonusHistoryFetch = ActionCreator<typeof BONUS_HISTORY_FETCH, Record<string, string>>;
export type TBonusHistorySuccess = ActionCreator<
  typeof BONUS_HISTORY_SUCCESS,
  { status: string | number }
>;
export type TBonusHistoryPending = ActionCreator<typeof BONUS_HISTORY_PENDING>;
export type TBonusHistoryError = ActionCreator<
  typeof BONUS_HISTORY_ERROR,
  { status: string | number }
>;
export type TBonusHistorySave = ActionCreator<typeof BONUS_HISTORY_SAVE>;
export type TBonusSave = ActionCreator<typeof BONUS_SAVE>;

export type TBonusResetData = ActionCreator<typeof BONUS_RESET_DATA>;
