/* eslint-disable @typescript-eslint/indent */
// Modules Types
import { ActionCreator } from '@type/actions';
import { TVerifyPhoneRequestArgs, TVerifyOtpRequestArgs } from '@type/auth';

// Constants
import {
  AUTH_MODAL_SHOW,
  AUTH_PHONE_SEND,
  AUTH_OTP_SEND,
  AUTH_SAVE_PHONE_DATA,
  AUTH_SAVE_OTP_DATA,
  AUTH_PENDING,
  AUTH_SUCCESS,
  AUTH_ERROR,
  AUTH_PHONE_STATUS_RESET,
  AUTH_OTP_STATUS_RESET,
  UNAUTHORIZE,
  AUTH_MODAL_SAVE_PHONE,
  AUTH_PHONE_CLEAN,
} from '@constants/auth';

export type TAuthPhoneSend = ActionCreator<typeof AUTH_PHONE_SEND, TVerifyPhoneRequestArgs>;
export type TPhoneSendClean = ActionCreator<typeof AUTH_PHONE_CLEAN, string>;
export type TAuthOtpSend = ActionCreator<typeof AUTH_OTP_SEND, TVerifyOtpRequestArgs>;
export type TAuthSaveResendToken = ActionCreator<
  typeof AUTH_SAVE_PHONE_DATA,
  TVerifyPhoneRequestArgs
>;
export type TAuthPhoneStatusReset = ActionCreator<typeof AUTH_PHONE_STATUS_RESET>;
export type TAuthOtpStatusReset = ActionCreator<typeof AUTH_OTP_STATUS_RESET>;
export type TAuthSaveOtpData = ActionCreator<typeof AUTH_SAVE_OTP_DATA, TVerifyPhoneRequestArgs>;
export type TAuthPending = ActionCreator<typeof AUTH_PENDING>;
export type TUnauthorize = ActionCreator<typeof UNAUTHORIZE>;
export type TAuthModalShow = ActionCreator<typeof AUTH_MODAL_SHOW>;
export type TAuthModalSavePhone = ActionCreator<typeof AUTH_MODAL_SAVE_PHONE, string>;
export type TAuthSuccess = ActionCreator<typeof AUTH_SUCCESS, { status: number }>;
export type TAuthError = ActionCreator<typeof AUTH_ERROR, { status: number }>;
