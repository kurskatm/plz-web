// Constants
import {
  AUTH_PHONE_SEND,
  AUTH_OTP_SEND,
  AUTH_SAVE_PHONE_DATA,
  AUTH_SAVE_OTP_DATA,
  AUTH_PENDING,
  AUTH_SUCCESS,
  AUTH_ERROR,
  AUTH_PHONE_STATUS_RESET,
  AUTH_OTP_PENDING,
  AUTH_OTP_SUCCESS,
  AUTH_OTP_ERROR,
  AUTH_OTP_STATUS_RESET,
  AUTH_CLEAR_RESEND_TOKEN,
  AUTH_MODAL_SHOW,
  UNAUTHORIZE,
  AUTH_MODAL_SAVE_PHONE,
  AUTH_PHONE_CLEAN,
} from '@constants/auth';
// Types
import {
  TAuthPhoneSend,
  TAuthOtpSend,
  TAuthSaveOtpData,
  TAuthPending,
  TAuthSuccess,
  TAuthError,
  TAuthPhoneStatusReset,
  TAuthOtpStatusReset,
  TAuthModalShow,
  TUnauthorize,
  TAuthModalSavePhone,
  TPhoneSendClean,
} from './action-types';

// verify phone
export const authPhoneSend: TAuthPhoneSend = (data) => ({
  type: AUTH_PHONE_SEND,
  payload: data,
});

// cleane phone
export const authPhoneClean: TPhoneSendClean = (data) => ({
  type: AUTH_PHONE_CLEAN,
  payload: data,
});

// send otp token
export const authOtpSend: TAuthOtpSend = (data) => ({
  type: AUTH_OTP_SEND,
  payload: data,
});

export const authSaveResendToken: TAuthOtpSend = (data) => ({
  type: AUTH_SAVE_PHONE_DATA,
  payload: data,
});

export const authSaveOtpData: TAuthSaveOtpData = (data) => ({
  type: AUTH_SAVE_OTP_DATA,
  payload: data,
});

export const authModalShow: TAuthModalShow = (data) => ({
  type: AUTH_MODAL_SHOW,
  payload: data,
});

export const authModalSavePhone: TAuthModalSavePhone = (data) => ({
  type: AUTH_MODAL_SAVE_PHONE,
  payload: data,
});

export const authClearResendTokenData: TAuthSaveOtpData = () => ({
  type: AUTH_CLEAR_RESEND_TOKEN,
});

export const authPending: TAuthPending = () => ({
  type: AUTH_PENDING,
});

export const authSuccess: TAuthSuccess = ({ status }) => ({
  type: AUTH_SUCCESS,
  payload: { status },
});

export const authError: TAuthError = ({ status }) => ({
  type: AUTH_ERROR,
  payload: { status },
});

export const authPhoneStatusReset: TAuthPhoneStatusReset = () => ({
  type: AUTH_PHONE_STATUS_RESET,
});

export const authOtpPending: TAuthPending = () => ({
  type: AUTH_OTP_PENDING,
});

export const authOtpSuccess: TAuthSuccess = ({ status }) => ({
  type: AUTH_OTP_SUCCESS,
  payload: { status },
});

export const authOtpError: TAuthError = ({ status }) => ({
  type: AUTH_OTP_ERROR,
  payload: { status },
});

export const authOtpStatusReset: TAuthOtpStatusReset = () => ({
  type: AUTH_OTP_STATUS_RESET,
});

export const unauthorize: TUnauthorize = () => ({
  type: UNAUTHORIZE,
});
