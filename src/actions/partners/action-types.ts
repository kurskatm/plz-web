// Modules Types
import { ActionCreator } from '@type/actions';
import { TPartnersCounters } from '@type/partners';
// Constants
import {
  PARTNERS_BECOME_PARTNER_ADD,
  PARTNERS_BECOME_PARTNER_IDLE,
  PARTNERS_BECOME_PARTNER_PENDING,
  PARTNERS_BECOME_PARTNER_SUCCESS,
  PARTNERS_BECOME_PARTNER_ERROR,
  PARTNERS_COUNTERS_FETCH,
  PARTNERS_COUNTERS_PENDING,
  PARTNERS_COUNTERS_SUCCESS,
  PARTNERS_COUNTERS_ERROR,
  PARTNERS_COUNTERS_SAVE,
} from '@constants/partners';

// Add partner
export type TPartnersBecomePartner = ActionCreator<typeof PARTNERS_BECOME_PARTNER_ADD, string>;

export type TPartnersBecomePartnerIdle = ActionCreator<typeof PARTNERS_BECOME_PARTNER_IDLE>;

export type TPartnersBecomePartnerPending = ActionCreator<typeof PARTNERS_BECOME_PARTNER_PENDING>;

export type TPartnersBecomePartnerSuccess = ActionCreator<
  typeof PARTNERS_BECOME_PARTNER_SUCCESS,
  { status: number }
>;

export type TPartnersBecomePartnerError = ActionCreator<
  typeof PARTNERS_BECOME_PARTNER_ERROR,
  { status: number }
>;

// Get counters
export type TPartnersCountersGet = ActionCreator<typeof PARTNERS_COUNTERS_FETCH, string>;

export type TPartnersCountersSave = ActionCreator<typeof PARTNERS_COUNTERS_SAVE, TPartnersCounters>;

export type TPartnersCountersPending = ActionCreator<typeof PARTNERS_COUNTERS_PENDING>;

export type TPartnersCountersSuccess = ActionCreator<
  typeof PARTNERS_COUNTERS_SUCCESS,
  { status: number }
>;

export type TPartnersCountersError = ActionCreator<
  typeof PARTNERS_COUNTERS_ERROR,
  { status: number }
>;
