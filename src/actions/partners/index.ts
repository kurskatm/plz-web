// Constants
import {
  PARTNERS_BECOME_PARTNER_ADD,
  PARTNERS_BECOME_PARTNER_IDLE,
  PARTNERS_BECOME_PARTNER_PENDING,
  PARTNERS_BECOME_PARTNER_SUCCESS,
  PARTNERS_BECOME_PARTNER_ERROR,
  PARTNERS_COUNTERS_ERROR,
  PARTNERS_COUNTERS_FETCH,
  PARTNERS_COUNTERS_PENDING,
  PARTNERS_COUNTERS_SUCCESS,
  PARTNERS_COUNTERS_SAVE,
} from '@constants/partners';
// Types
import {
  TPartnersBecomePartner,
  TPartnersBecomePartnerIdle,
  TPartnersBecomePartnerPending,
  TPartnersBecomePartnerSuccess,
  TPartnersBecomePartnerError,
  TPartnersCountersGet,
  TPartnersCountersSave,
  TPartnersCountersError,
  TPartnersCountersPending,
  TPartnersCountersSuccess,
} from './action-types';

// Partners
export const partnersBecomePartner: TPartnersBecomePartner = (data) => ({
  type: PARTNERS_BECOME_PARTNER_ADD,
  payload: data,
});

export const partnersBecomePartnerIdle: TPartnersBecomePartnerIdle = () => ({
  type: PARTNERS_BECOME_PARTNER_IDLE,
});

export const partnersBecomePartnerPending: TPartnersBecomePartnerPending = () => ({
  type: PARTNERS_BECOME_PARTNER_PENDING,
});

export const partnersBecomePartnerSuccess: TPartnersBecomePartnerSuccess = ({ status }) => ({
  type: PARTNERS_BECOME_PARTNER_SUCCESS,
  payload: { status },
});

export const partnersBecomePartnerError: TPartnersBecomePartnerError = ({ status }) => ({
  type: PARTNERS_BECOME_PARTNER_ERROR,
  payload: { status },
});

// Counters
export const partnersCounters: TPartnersCountersGet = () => ({
  type: PARTNERS_COUNTERS_FETCH,
});

export const partnersCountersPending: TPartnersCountersPending = () => ({
  type: PARTNERS_COUNTERS_PENDING,
});

export const partnersCountersSave: TPartnersCountersSave = (data) => ({
  type: PARTNERS_COUNTERS_SAVE,
  payload: data,
});

export const partnersCountersSuccess: TPartnersCountersSuccess = ({ status }) => ({
  type: PARTNERS_COUNTERS_SUCCESS,
  payload: { status },
});

export const partnersCountersError: TPartnersCountersError = ({ status }) => ({
  type: PARTNERS_COUNTERS_ERROR,
  payload: { status },
});
