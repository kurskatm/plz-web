/* eslint-disable @typescript-eslint/indent */
// Modules Types
import { ActionCreator } from '@type/actions';
// Constants
import {
  F_TYPES_MODAL_SHOW,
  F_TYPES_FETCH,
  F_TYPES_FETCH_PENDING,
  F_TYPES_FETCH_SUCCESS,
  F_TYPES_FETCH_ERROR,
  F_TYPES_FETCH_SAVE_DATA,
} from '@constants/funcTypes';
import { TCitiesList } from '@/type/cities';

export type TFTypesModalShow = ActionCreator<typeof F_TYPES_MODAL_SHOW>;

export type TFTypesFetch = ActionCreator<typeof F_TYPES_FETCH>;

export type TFTypesFetchPending = ActionCreator<typeof F_TYPES_FETCH_PENDING>;

export type TFTypesFetchSuccess = ActionCreator<
  typeof F_TYPES_FETCH_SUCCESS,
  { status: string | number }
>;

export type TFTypesFetchError = ActionCreator<
  typeof F_TYPES_FETCH_ERROR,
  { status: string | number }
>;

export type TFTypesFetchSaveData = ActionCreator<
  typeof F_TYPES_FETCH_SAVE_DATA,
  {
    data: TCitiesList;
  }
>;
