// Constants
import {
  F_TYPES_MODAL_SHOW,
  F_TYPES_FETCH,
  F_TYPES_FETCH_PENDING,
  F_TYPES_FETCH_SUCCESS,
  F_TYPES_FETCH_ERROR,
  F_TYPES_FETCH_SAVE_DATA,
} from '@constants/funcTypes';
// Types
import {
  TFTypesModalShow,
  TFTypesFetch,
  TFTypesFetchPending,
  TFTypesFetchSuccess,
  TFTypesFetchError,
  TFTypesFetchSaveData,
} from './action-types';

export const fTypesModalShow: TFTypesModalShow = (data) => ({
  type: F_TYPES_MODAL_SHOW,
  payload: data,
});

export const fTypesFetch: TFTypesFetch = () => ({
  type: F_TYPES_FETCH,
});

export const fTypesFetchPending: TFTypesFetchPending = () => ({
  type: F_TYPES_FETCH_PENDING,
});

export const fTypesFetchSuccess: TFTypesFetchSuccess = (data) => ({
  type: F_TYPES_FETCH_SUCCESS,
  payload: data,
});

export const fTypesFetchError: TFTypesFetchError = (data) => ({
  type: F_TYPES_FETCH_ERROR,
  payload: data,
});

export const fTypesFetchSaveData: TFTypesFetchSaveData = (data) => ({
  type: F_TYPES_FETCH_SAVE_DATA,
  payload: data,
});
