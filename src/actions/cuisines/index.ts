// Constants
import {
  CUISINES_FETCH,
  CUISINES_SAVE,
  CUISINES_PUSH,
  CUISINES_REMOVE,
  CUISINES_UPDATE,
  CUISINES_FETCH_PENDING,
  CUISINES_FETCH_SUCCESS,
  CUISINES_FETCH_ERROR,
} from '@constants/cuisines';
// Types
import {
  TCuisinesFetch,
  TCuisinesSave,
  TCuisinesPush,
  TCuisinesRemove,
  TCuisinesUpdate,
  TCuisinesFetchPending,
  TCuisinesFetchSuccess,
  TCuisinesFetchError,
} from './action-types';

export const cuisinesFetch: TCuisinesFetch = () => ({
  type: CUISINES_FETCH,
});

export const cuisinesSave: TCuisinesSave = (data) => ({
  type: CUISINES_SAVE,
  payload: data,
});

export const cuisinesPush: TCuisinesPush = (id) => ({
  type: CUISINES_PUSH,
  payload: id,
});

export const cuisinesRemove: TCuisinesRemove = (index) => ({
  type: CUISINES_REMOVE,
  payload: index,
});

export const cuisinesUpdate: TCuisinesUpdate = (data) => ({
  type: CUISINES_UPDATE,
  payload: data,
});

export const cuisinesFetchPending: TCuisinesFetchPending = () => ({
  type: CUISINES_FETCH_PENDING,
});

export const cuisinesFetchSuccess: TCuisinesFetchSuccess = ({ status }) => ({
  type: CUISINES_FETCH_SUCCESS,
  payload: { status },
});

export const cuisinesFetchError: TCuisinesFetchError = ({ status }) => ({
  type: CUISINES_FETCH_ERROR,
  payload: { status },
});
