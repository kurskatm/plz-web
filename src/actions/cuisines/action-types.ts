// Modules Types
import { ActionCreator } from '@type/actions';
// Constants
import {
  CUISINES_FETCH,
  CUISINES_SAVE,
  CUISINES_PUSH,
  CUISINES_REMOVE,
  CUISINES_UPDATE,
  CUISINES_FETCH_PENDING,
  CUISINES_FETCH_SUCCESS,
  CUISINES_FETCH_ERROR,
} from '@constants/cuisines';
// Main Types
import { TCuisinesList } from '@models/cuisines/types';

export type TCuisinesFetch = ActionCreator<typeof CUISINES_FETCH>;

export type TCuisinesSave = ActionCreator<typeof CUISINES_SAVE, TCuisinesList>;

export type TCuisinesPush = ActionCreator<typeof CUISINES_PUSH, string>;

export type TCuisinesRemove = ActionCreator<typeof CUISINES_REMOVE, number>;

export type TCuisinesUpdate = ActionCreator<typeof CUISINES_UPDATE, string[]>;

export type TCuisinesFetchPending = ActionCreator<typeof CUISINES_FETCH_PENDING>;

export type TCuisinesFetchSuccess = ActionCreator<
  typeof CUISINES_FETCH_SUCCESS,
  { status: string | number }
>;

export type TCuisinesFetchError = ActionCreator<
  typeof CUISINES_FETCH_ERROR,
  { status: string | number }
>;
