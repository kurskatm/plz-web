/* eslint-disable @typescript-eslint/indent */
// Modules Types
import { ActionCreator } from '@type/actions';
// Constants
import {
  CITIES_MODAL_SHOW,
  CITIES_FETCH,
  CITIES_FETCH_PENDING,
  CITIES_FETCH_SUCCESS,
  CITIES_FETCH_ERROR,
  CITIES_FETCH_SAVE_DATA,
} from '@constants/cities';
import { TCitiesList } from '@/type/cities';

export type TCitiesModalShow = ActionCreator<typeof CITIES_MODAL_SHOW>;

export type TCitiesFetch = ActionCreator<typeof CITIES_FETCH>;

export type TCitiesFetchPending = ActionCreator<typeof CITIES_FETCH_PENDING>;

export type TCitiesFetchSuccess = ActionCreator<
  typeof CITIES_FETCH_SUCCESS,
  { status: string | number }
>;

export type TCitiesFetchError = ActionCreator<
  typeof CITIES_FETCH_ERROR,
  { status: string | number }
>;

export type TCitiesFetchSaveData = ActionCreator<
  typeof CITIES_FETCH_SAVE_DATA,
  {
    data: TCitiesList;
  }
>;
