// Constants
import {
  CITIES_MODAL_SHOW,
  CITIES_FETCH,
  CITIES_FETCH_PENDING,
  CITIES_FETCH_SUCCESS,
  CITIES_FETCH_ERROR,
  CITIES_FETCH_SAVE_DATA,
} from '@constants/cities';
// Types
import {
  TCitiesModalShow,
  TCitiesFetch,
  TCitiesFetchPending,
  TCitiesFetchSuccess,
  TCitiesFetchError,
  TCitiesFetchSaveData,
} from './action-types';

export const citiesModalShow: TCitiesModalShow = (data) => ({
  type: CITIES_MODAL_SHOW,
  payload: data,
});

export const citiesFetch: TCitiesFetch = () => ({
  type: CITIES_FETCH,
});

export const citiesFetchPending: TCitiesFetchPending = () => ({
  type: CITIES_FETCH_PENDING,
});

export const citiesFetchSuccess: TCitiesFetchSuccess = (data) => ({
  type: CITIES_FETCH_SUCCESS,
  payload: data,
});

export const citiesFetchError: TCitiesFetchError = (data) => ({
  type: CITIES_FETCH_ERROR,
  payload: data,
});

export const citiesFetchSaveData: TCitiesFetchSaveData = (data) => ({
  type: CITIES_FETCH_SAVE_DATA,
  payload: data,
});
