// Modules Types
import { ActionCreator } from '@type/actions';
import { TRestaurantStoresItem } from '@type/restaurant';

// Constants
import {
  RESTAURANT_STORES_FETCH,
  RESTAURANT_STORES_PENDING,
  RESTAURANT_STORES_SUCCESS,
  RESTAURANT_STORES_ERROR,
  RESTAURANT_STORES_SAVE,
} from '@constants/restaurant/stores';

export interface TRestaurantStoresArgs {
  restId?: string;
}

export type TRestaurantStoresFetch = ActionCreator<
  typeof RESTAURANT_STORES_FETCH,
  TRestaurantStoresArgs
>;
export type TRestaurantStoresPending = ActionCreator<
  typeof RESTAURANT_STORES_PENDING,
  { status: number }
>;
export type TRestaurantStoresSuccess = ActionCreator<
  typeof RESTAURANT_STORES_SUCCESS,
  { status: number }
>;
export type TRestaurantStoresError = ActionCreator<
  typeof RESTAURANT_STORES_ERROR,
  { status: number }
>;
export type TRestaurantStoresSave = ActionCreator<
  typeof RESTAURANT_STORES_SAVE,
  TRestaurantStoresItem
>;
