// Constants
import {
  RESTAURANT_STORES_FETCH,
  RESTAURANT_STORES_SAVE,
  RESTAURANT_STORES_PENDING,
  RESTAURANT_STORES_SUCCESS,
  RESTAURANT_STORES_ERROR,
} from '@constants/restaurant/stores';
// Types
import {
  TRestaurantStoresFetch,
  TRestaurantStoresPending,
  TRestaurantStoresSuccess,
  TRestaurantStoresError,
  TRestaurantStoresSave,
} from './action-types';

// Restaurants
export const restaurantStoresFetch: TRestaurantStoresFetch = (data) => ({
  type: RESTAURANT_STORES_FETCH,
  payload: data,
});

export const restaurantStoresSave: TRestaurantStoresSave = (data) => ({
  type: RESTAURANT_STORES_SAVE,
  payload: data,
});

export const restaurantStoresPending: TRestaurantStoresPending = () => ({
  type: RESTAURANT_STORES_PENDING,
});

export const restaurantStoresSuccess: TRestaurantStoresSuccess = ({ status }) => ({
  type: RESTAURANT_STORES_SUCCESS,
  payload: { status },
});

export const restaurantStoresError: TRestaurantStoresError = ({ status }) => ({
  type: RESTAURANT_STORES_ERROR,
  payload: { status },
});
