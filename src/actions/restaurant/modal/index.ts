// Constants
import { CLOSE_RESTAURANT_MODAL_SHOW } from '@constants/restaurant/restaurantCloseModal';
import {
  RESTAURANT_SHOW_MODALS,
  RESTAURANT_SHOW_SHOPPING_CARD,
  RESTAURANT_SHOW_MODIFIERS_CARD,
  RESTAURANT_SHOW_PROMO_CARD,
} from '@constants/restaurant/modals';
// Types
import {
  TCloseRestaurantModalShow,
  TShowRestaurantModals,
  TShowShoppingCardModal,
  TShowModifiersCardModal,
  TShowPromoCardModal,
} from './action-types';

export const closeRestaurantModalShow: TCloseRestaurantModalShow = (data) => ({
  type: CLOSE_RESTAURANT_MODAL_SHOW,
  payload: data,
});

export const showRestaurantModals: TShowRestaurantModals = (data) => ({
  type: RESTAURANT_SHOW_MODALS,
  payload: data,
});

export const showShoppingCardModal: TShowShoppingCardModal = (data) => ({
  type: RESTAURANT_SHOW_SHOPPING_CARD,
  payload: data,
});

export const showModifiersCardModal: TShowModifiersCardModal = (data) => ({
  type: RESTAURANT_SHOW_MODIFIERS_CARD,
  payload: data,
});

export const showPromoCardModal: TShowPromoCardModal = (data) => ({
  type: RESTAURANT_SHOW_PROMO_CARD,
  payload: data,
});
