/* eslint-disable @typescript-eslint/indent */
// Modules Types
import { ActionCreator } from '@type/actions';
// Constants
import { CLOSE_RESTAURANT_MODAL_SHOW } from '@constants/restaurant/restaurantCloseModal';
import {
  RESTAURANT_SHOW_MODALS,
  RESTAURANT_SHOW_SHOPPING_CARD,
  RESTAURANT_SHOW_MODIFIERS_CARD,
  RESTAURANT_SHOW_PROMO_CARD,
} from '@constants/restaurant/modals';
import { TRestaurantDataToUpdateCart } from '@/type/restaurants/restaurant-card';

export type TCloseRestaurantModalShow = ActionCreator<typeof CLOSE_RESTAURANT_MODAL_SHOW>;

export type TShowRestaurantModals = ActionCreator<
  typeof RESTAURANT_SHOW_MODALS,
  {
    showModal: boolean;
    modalType:
      | 'not-enough-points'
      | 'another-restaurant'
      | 'another-point-product'
      | 'change-address'
      | '';
    bonusPointsRest?: number;
    dataToUpdateCart?: TRestaurantDataToUpdateCart;
    link?: string;
  }
>;

export type TShowShoppingCardModal = ActionCreator<typeof RESTAURANT_SHOW_SHOPPING_CARD>;

export type TShowModifiersCardModal = ActionCreator<
  typeof RESTAURANT_SHOW_MODIFIERS_CARD,
  {
    showModal: boolean;
    productId: string;
    inBonusPoint?: boolean;
  }
>;

export type TShowPromoCardModal = ActionCreator<
  typeof RESTAURANT_SHOW_PROMO_CARD,
  {
    showModal: boolean;
    promoId: string;
  }
>;
