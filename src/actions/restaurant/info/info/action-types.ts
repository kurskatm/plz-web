// Main Types
import { ActionCreator } from '@type/actions';
import { TRestaurantInfo } from '@type/restaurants/restaurant-info';
// Constants
import {
  RESTAURANT_CARD_INFO_FETCH,
  RESTAURANT_CARD_INFO_SAVE,
  RESTAURANT_CARD_INFO_RESET,
  RESTAURANT_FETCH_CARD_INFO_PENDING,
  RESTAURANT_FETCH_CARD_INFO_SUCCESS,
  RESTAURANT_FETCH_CARD_INFO_ERROR,
} from '@constants/restaurant/info/info';

export type TRestaurantCardInfoFetch = ActionCreator<
  typeof RESTAURANT_CARD_INFO_FETCH,
  { restaurantId: string }
>;

export type TRestaurantCardInfoSave = ActionCreator<
  typeof RESTAURANT_CARD_INFO_SAVE,
  { data: TRestaurantInfo }
>;

export type TRestaurantCardInfoReset = ActionCreator<typeof RESTAURANT_CARD_INFO_RESET>;

export type TRestaurantCardInfoFetchPending = ActionCreator<
  typeof RESTAURANT_FETCH_CARD_INFO_PENDING
>;

export type TRestaurantCardInfoFetchSuccess = ActionCreator<
  typeof RESTAURANT_FETCH_CARD_INFO_SUCCESS,
  { status: number }
>;

export type TRestaurantCardInfoFetchError = ActionCreator<
  typeof RESTAURANT_FETCH_CARD_INFO_ERROR,
  { status: number }
>;
