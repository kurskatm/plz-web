// Constants
import {
  RESTAURANT_CARD_INFO_FETCH,
  RESTAURANT_CARD_INFO_SAVE,
  RESTAURANT_CARD_INFO_RESET,
  RESTAURANT_FETCH_CARD_INFO_PENDING,
  RESTAURANT_FETCH_CARD_INFO_SUCCESS,
  RESTAURANT_FETCH_CARD_INFO_ERROR,
} from '@constants/restaurant/info/info';
// Types
import {
  TRestaurantCardInfoFetch,
  TRestaurantCardInfoSave,
  TRestaurantCardInfoReset,
  TRestaurantCardInfoFetchPending,
  TRestaurantCardInfoFetchSuccess,
  TRestaurantCardInfoFetchError,
} from './action-types';

export const restaurantInfoFetch: TRestaurantCardInfoFetch = (data) => ({
  type: RESTAURANT_CARD_INFO_FETCH,
  payload: data,
});

export const restaurantInfoSave: TRestaurantCardInfoSave = (data) => ({
  type: RESTAURANT_CARD_INFO_SAVE,
  payload: data,
});

export const restaurantInfoReset: TRestaurantCardInfoReset = () => ({
  type: RESTAURANT_CARD_INFO_RESET,
});

export const restaurantInfoFetchPending: TRestaurantCardInfoFetchPending = () => ({
  type: RESTAURANT_FETCH_CARD_INFO_PENDING,
});

export const restaurantInfoFetchSuccess: TRestaurantCardInfoFetchSuccess = ({ status }) => ({
  type: RESTAURANT_FETCH_CARD_INFO_SUCCESS,
  payload: { status },
});

export const restaurantInfoFetchError: TRestaurantCardInfoFetchError = ({ status }) => ({
  type: RESTAURANT_FETCH_CARD_INFO_ERROR,
  payload: { status },
});
