// Constants
import {
  RESTAURANT_GEOCODE_SUGGEST_FETCH,
  RESTAURANT_GEOCODE_SUGGEST_FETCH_PENDING,
  RESTAURANT_GEOCODE_SUGGEST_FETCH_SUCCESS,
  RESTAURANT_GEOCODE_SUGGEST_FETCH_ERROR,
  RESTAURANT_GEOCODE_SUGGEST_SAVE,
  RESTAURANT_GEOCODE_SUGGEST_RESET,
} from '@constants/restaurant/info/geocode-suggest';
// Types
import {
  TRestaurantGeocodeSuggestFetch,
  TRestaurantGeocodeSuggestFetchPending,
  TRestaurantGeocodeSuggestFetchSuccess,
  TRestaurantGeocodeSuggestFetchError,
  TRestaurantGeocodeSuggestSave,
  TRestaurantGeocodeSuggestReset,
} from './action-types';

export const restaurantGeocodeSuggestFetch: TRestaurantGeocodeSuggestFetch = (params) => ({
  type: RESTAURANT_GEOCODE_SUGGEST_FETCH,
  payload: params,
});

export const restaurantGeocodeSuggestSave: TRestaurantGeocodeSuggestSave = (data) => ({
  type: RESTAURANT_GEOCODE_SUGGEST_SAVE,
  payload: data,
});

export const restaurantGeocodeSuggestReset: TRestaurantGeocodeSuggestReset = () => ({
  type: RESTAURANT_GEOCODE_SUGGEST_RESET,
});

export const restaurantGeocodeSuggestFetchPending: TRestaurantGeocodeSuggestFetchPending = () => ({
  type: RESTAURANT_GEOCODE_SUGGEST_FETCH_PENDING,
});

export const restaurantGeocodeSuggestFetchSuccess: TRestaurantGeocodeSuggestFetchSuccess = ({
  status,
}) => ({
  type: RESTAURANT_GEOCODE_SUGGEST_FETCH_SUCCESS,
  payload: { status },
});

export const restaurantGeocodeSuggestFetchError: TRestaurantGeocodeSuggestFetchError = ({
  status,
}) => ({
  type: RESTAURANT_GEOCODE_SUGGEST_FETCH_ERROR,
  payload: { status },
});
