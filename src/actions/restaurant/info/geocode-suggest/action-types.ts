/* eslint-disable @typescript-eslint/indent */
// Modules Types
import { ActionCreator } from '@type/actions';
// Constants
import {
  RESTAURANT_GEOCODE_SUGGEST_FETCH,
  RESTAURANT_GEOCODE_SUGGEST_FETCH_PENDING,
  RESTAURANT_GEOCODE_SUGGEST_FETCH_SUCCESS,
  RESTAURANT_GEOCODE_SUGGEST_FETCH_ERROR,
  RESTAURANT_GEOCODE_SUGGEST_SAVE,
  RESTAURANT_GEOCODE_SUGGEST_RESET,
} from '@constants/restaurant/info/geocode-suggest';
// Main Types
import { TNewGeocodeData } from '@type/new-geocode';

export type TRestaurantGeocodeSuggestFetch = ActionCreator<
  typeof RESTAURANT_GEOCODE_SUGGEST_FETCH,
  { name: string; description: string; cityId: string }
>;

export type TRestaurantGeocodeSuggestFetchPending = ActionCreator<
  typeof RESTAURANT_GEOCODE_SUGGEST_FETCH_PENDING
>;

export type TRestaurantGeocodeSuggestFetchSuccess = ActionCreator<
  typeof RESTAURANT_GEOCODE_SUGGEST_FETCH_SUCCESS,
  { status: string | number }
>;

export type TRestaurantGeocodeSuggestFetchError = ActionCreator<
  typeof RESTAURANT_GEOCODE_SUGGEST_FETCH_ERROR,
  { status: string | number }
>;

export type TRestaurantGeocodeSuggestSave = ActionCreator<
  typeof RESTAURANT_GEOCODE_SUGGEST_SAVE,
  {
    data: TNewGeocodeData;
  }
>;

export type TRestaurantGeocodeSuggestReset = ActionCreator<typeof RESTAURANT_GEOCODE_SUGGEST_RESET>;
