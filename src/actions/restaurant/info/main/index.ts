// Constants
import { RESTAURANT_CARD_INFO_MAIN_RESET } from '@constants/restaurant/info';
// Types
import { TRestaurantCardInfoResetReset } from './action-types';

export const restaurantCardInfoReset: TRestaurantCardInfoResetReset = () => ({
  type: RESTAURANT_CARD_INFO_MAIN_RESET,
});
