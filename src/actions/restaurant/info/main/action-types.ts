// Main Types
import { ActionCreator } from '@type/actions';
// Constants
import { RESTAURANT_CARD_INFO_MAIN_RESET } from '@constants/restaurant/info';

export type TRestaurantCardInfoResetReset = ActionCreator<typeof RESTAURANT_CARD_INFO_MAIN_RESET>;
