// Constants
import {
  RESTAURANT_CARD_LEGAL_FETCH,
  RESTAURANT_CARD_LEGAL_SAVE,
  RESTAURANT_CARD_LEGAL_RESET,
  RESTAURANT_FETCH_CARD_LEGAL_PENDING,
  RESTAURANT_FETCH_CARD_LEGAL_SUCCESS,
  RESTAURANT_FETCH_CARD_LEGAL_ERROR,
} from '@constants/restaurant/info/legal';
// Types
import {
  TRestaurantCardLegalFetch,
  TRestaurantCardLegalSave,
  TRestaurantCardLegalReset,
  TRestaurantCardLegalFetchPending,
  TRestaurantCardLegalFetchSuccess,
  TRestaurantCardLegalFetchError,
} from './action-types';

export const restaurantLegalFetch: TRestaurantCardLegalFetch = (data) => ({
  type: RESTAURANT_CARD_LEGAL_FETCH,
  payload: data,
});

export const restaurantLegalSave: TRestaurantCardLegalSave = (data) => ({
  type: RESTAURANT_CARD_LEGAL_SAVE,
  payload: data,
});

export const restaurantLegalReset: TRestaurantCardLegalReset = () => ({
  type: RESTAURANT_CARD_LEGAL_RESET,
});

export const restaurantLegalFetchPending: TRestaurantCardLegalFetchPending = () => ({
  type: RESTAURANT_FETCH_CARD_LEGAL_PENDING,
});

export const restaurantLegalFetchSuccess: TRestaurantCardLegalFetchSuccess = ({ status }) => ({
  type: RESTAURANT_FETCH_CARD_LEGAL_SUCCESS,
  payload: { status },
});

export const restaurantLegalFetchError: TRestaurantCardLegalFetchError = ({ status }) => ({
  type: RESTAURANT_FETCH_CARD_LEGAL_ERROR,
  payload: { status },
});
