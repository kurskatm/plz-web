// Main Types
import { ActionCreator } from '@type/actions';
import { TRestaurantLegal } from '@type/restaurants/restaurant-legal';
// Constants
import {
  RESTAURANT_CARD_LEGAL_FETCH,
  RESTAURANT_CARD_LEGAL_SAVE,
  RESTAURANT_CARD_LEGAL_RESET,
  RESTAURANT_FETCH_CARD_LEGAL_PENDING,
  RESTAURANT_FETCH_CARD_LEGAL_SUCCESS,
  RESTAURANT_FETCH_CARD_LEGAL_ERROR,
} from '@constants/restaurant/info/legal';

export type TRestaurantCardLegalFetch = ActionCreator<
  typeof RESTAURANT_CARD_LEGAL_FETCH,
  { restaurantId: string; storeId: string }
>;

export type TRestaurantCardLegalSave = ActionCreator<
  typeof RESTAURANT_CARD_LEGAL_SAVE,
  { data: TRestaurantLegal }
>;

export type TRestaurantCardLegalReset = ActionCreator<typeof RESTAURANT_CARD_LEGAL_RESET>;

export type TRestaurantCardLegalFetchPending = ActionCreator<
  typeof RESTAURANT_FETCH_CARD_LEGAL_PENDING
>;

export type TRestaurantCardLegalFetchSuccess = ActionCreator<
  typeof RESTAURANT_FETCH_CARD_LEGAL_SUCCESS,
  { status: number }
>;

export type TRestaurantCardLegalFetchError = ActionCreator<
  typeof RESTAURANT_FETCH_CARD_LEGAL_ERROR,
  { status: number }
>;
