/* eslint-disable @typescript-eslint/indent */
// Modules Types
import { ActionCreator } from '@type/actions';
import { TDeliveryAreaConditionsList } from '@type/restaurants/delivery-conditions';
// Constants
import {
  RESTAURANT_DELIVERY_AREAS_FETCH,
  RESTAURANT_DELIVERY_AREAS_FETCH_PENDING,
  RESTAURANT_DELIVERY_AREAS_FETCH_SUCCESS,
  RESTAURANT_DELIVERY_AREAS_FETCH_ERROR,
  RESTAURANT_DELIVERY_AREAS_SAVE,
  RESTAURANT_DELIVERY_AREAS_RESET,
} from '@constants/restaurant/info/delivery-areas';

export type TRestaurantDeliveryAreasFetch = ActionCreator<
  typeof RESTAURANT_DELIVERY_AREAS_FETCH,
  {
    cityId: string;
    lat: number;
    lon: number;
    restId: string;
    clientTime: string;
  }
>;

export type TRestaurantDeliveryAreasSave = ActionCreator<
  typeof RESTAURANT_DELIVERY_AREAS_SAVE,
  {
    data: TDeliveryAreaConditionsList;
  }
>;

export type TRestaurantDeliveryAreasReset = ActionCreator<typeof RESTAURANT_DELIVERY_AREAS_RESET>;

export type TRestaurantDeliveryAreasFetchPending = ActionCreator<
  typeof RESTAURANT_DELIVERY_AREAS_FETCH_PENDING
>;

export type TRestaurantDeliveryAreasFetchSuccess = ActionCreator<
  typeof RESTAURANT_DELIVERY_AREAS_FETCH_SUCCESS,
  { status: number }
>;

export type TRestaurantDeliveryAreasFetchError = ActionCreator<
  typeof RESTAURANT_DELIVERY_AREAS_FETCH_ERROR,
  { status: number }
>;
