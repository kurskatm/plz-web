// Constants
import {
  RESTAURANT_DELIVERY_AREAS_FETCH,
  RESTAURANT_DELIVERY_AREAS_FETCH_PENDING,
  RESTAURANT_DELIVERY_AREAS_FETCH_SUCCESS,
  RESTAURANT_DELIVERY_AREAS_FETCH_ERROR,
  RESTAURANT_DELIVERY_AREAS_SAVE,
  RESTAURANT_DELIVERY_AREAS_RESET,
} from '@constants/restaurant/info/delivery-areas';
// Types
import {
  TRestaurantDeliveryAreasFetch,
  TRestaurantDeliveryAreasFetchPending,
  TRestaurantDeliveryAreasFetchSuccess,
  TRestaurantDeliveryAreasFetchError,
  TRestaurantDeliveryAreasSave,
  TRestaurantDeliveryAreasReset,
} from './action-types';

export const restaurantDeliveryAreasFetch: TRestaurantDeliveryAreasFetch = (data) => ({
  type: RESTAURANT_DELIVERY_AREAS_FETCH,
  payload: data,
});

export const restaurantDeliveryAreasSave: TRestaurantDeliveryAreasSave = (data) => ({
  type: RESTAURANT_DELIVERY_AREAS_SAVE,
  payload: data,
});

export const restaurantDeliveryAreasReset: TRestaurantDeliveryAreasReset = () => ({
  type: RESTAURANT_DELIVERY_AREAS_RESET,
});

export const restaurantDeliveryAreasFetchPending: TRestaurantDeliveryAreasFetchPending = () => ({
  type: RESTAURANT_DELIVERY_AREAS_FETCH_PENDING,
});

export const restaurantDeliveryAreasFetchSuccess: TRestaurantDeliveryAreasFetchSuccess = ({
  status,
}) => ({
  type: RESTAURANT_DELIVERY_AREAS_FETCH_SUCCESS,
  payload: { status },
});

export const restaurantDeliveryAreasFetchError: TRestaurantDeliveryAreasFetchError = ({
  status,
}) => ({
  type: RESTAURANT_DELIVERY_AREAS_FETCH_ERROR,
  payload: { status },
});
