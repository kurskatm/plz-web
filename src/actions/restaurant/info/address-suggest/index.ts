/* eslint-disable max-len */
// Constants
import {
  RESTAURANT_ADDRESSES_SUGGEST_FETCH,
  RESTAURANT_ADDRESSES_SUGGEST_FETCH_PENDING,
  RESTAURANT_ADDRESSES_SUGGEST_FETCH_SUCCESS,
  RESTAURANT_ADDRESSES_SUGGEST_FETCH_ERROR,
  RESTAURANT_ADDRESSES_SUGGEST_SAVE,
  RESTAURANT_ADDRESSES_SUGGEST_RESET,
} from '@constants/restaurant/info/address-suggest';
// Types
import {
  TRestaurantAddressesSuggestFetch,
  TRestaurantAddressesSuggestFetchPending,
  TRestaurantAddressesSuggestFetchSuccess,
  TRestaurantAddressesSuggestFetchError,
  TRestaurantAddressesSuggestSave,
  TRestaurantAddressesSuggestReset,
} from './action-types';

export const restaurantAddressesSuggestFetch: TRestaurantAddressesSuggestFetch = (params) => ({
  type: RESTAURANT_ADDRESSES_SUGGEST_FETCH,
  payload: params,
});

export const restaurantAddressesSuggestSave: TRestaurantAddressesSuggestSave = (data) => ({
  type: RESTAURANT_ADDRESSES_SUGGEST_SAVE,
  payload: data,
});

export const restaurantAddressesSuggestReset: TRestaurantAddressesSuggestReset = () => ({
  type: RESTAURANT_ADDRESSES_SUGGEST_RESET,
});

export const restaurantAddressesSuggestFetchPending: TRestaurantAddressesSuggestFetchPending = () => ({
  type: RESTAURANT_ADDRESSES_SUGGEST_FETCH_PENDING,
});

export const restaurantAddressesSuggestFetchSuccess: TRestaurantAddressesSuggestFetchSuccess = ({
  status,
}) => ({
  type: RESTAURANT_ADDRESSES_SUGGEST_FETCH_SUCCESS,
  payload: { status },
});

export const restaurantAddressesSuggestFetchError: TRestaurantAddressesSuggestFetchError = ({
  status,
}) => ({
  type: RESTAURANT_ADDRESSES_SUGGEST_FETCH_ERROR,
  payload: { status },
});
