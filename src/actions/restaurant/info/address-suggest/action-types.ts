/* eslint-disable @typescript-eslint/indent */
// Modules Types
import { ActionCreator } from '@type/actions';
// Constants
import {
  RESTAURANT_ADDRESSES_SUGGEST_FETCH,
  RESTAURANT_ADDRESSES_SUGGEST_FETCH_PENDING,
  RESTAURANT_ADDRESSES_SUGGEST_FETCH_SUCCESS,
  RESTAURANT_ADDRESSES_SUGGEST_FETCH_ERROR,
  RESTAURANT_ADDRESSES_SUGGEST_SAVE,
  RESTAURANT_ADDRESSES_SUGGEST_RESET,
} from '@constants/restaurant/info/address-suggest';
// Main Types
import { TNewAddressesList } from '@type/new-address';

// User Adresses Suggest
export type TRestaurantAddressesSuggestFetch = ActionCreator<
  typeof RESTAURANT_ADDRESSES_SUGGEST_FETCH,
  {
    query: string;
    limit: number;
    cityId: string;
  }
>;

export type TRestaurantAddressesSuggestSave = ActionCreator<
  typeof RESTAURANT_ADDRESSES_SUGGEST_SAVE,
  {
    data: TNewAddressesList;
  }
>;

export type TRestaurantAddressesSuggestReset = ActionCreator<
  typeof RESTAURANT_ADDRESSES_SUGGEST_RESET
>;

export type TRestaurantAddressesSuggestFetchPending = ActionCreator<
  typeof RESTAURANT_ADDRESSES_SUGGEST_FETCH_PENDING
>;

export type TRestaurantAddressesSuggestFetchSuccess = ActionCreator<
  typeof RESTAURANT_ADDRESSES_SUGGEST_FETCH_SUCCESS,
  { status: number }
>;

export type TRestaurantAddressesSuggestFetchError = ActionCreator<
  typeof RESTAURANT_ADDRESSES_SUGGEST_FETCH_ERROR,
  { status: number }
>;
