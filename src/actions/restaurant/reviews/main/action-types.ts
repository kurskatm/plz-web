// Main Types
import { ActionCreator } from '@type/actions';
// Constants
import { RESTAURANT_CARD_REVIEWS_MAIN_RESET } from '@constants/restaurant/reviews';

export type TRestaurantCardReviewsResetReset = ActionCreator<
  typeof RESTAURANT_CARD_REVIEWS_MAIN_RESET
>;
