// Constants
import { RESTAURANT_CARD_REVIEWS_MAIN_RESET } from '@constants/restaurant/reviews';
// Types
import { TRestaurantCardReviewsResetReset } from './action-types';

export const restaurantCardReviewsReset: TRestaurantCardReviewsResetReset = () => ({
  type: RESTAURANT_CARD_REVIEWS_MAIN_RESET,
});
