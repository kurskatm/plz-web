/* eslint-disable @typescript-eslint/indent */
// Main Types
import { ActionCreator } from '@type/actions';
import { TRestaurantPromoList } from '@type/restaurants/promo';
// Constants
import {
  RESTAURANT_CARD_REVIEWS_FETCH,
  RESTAURANT_CARD_REVIEWS_SAVE,
  RESTAURANT_CARD_REVIEWS_RESET,
  RESTAURANT_FETCH_CARD_REVIEWS_PENDING,
  RESTAURANT_FETCH_CARD_REVIEWS_SUCCESS,
  RESTAURANT_FETCH_CARD_REVIEWS_ERROR,
  RESTAURANT_CARD_REVIEWS_CHANGE_PAGINATION_LIMIT,
} from '@constants/restaurant/reviews/current';

export type TRestaurantCardReviewsFetch = ActionCreator<
  typeof RESTAURANT_CARD_REVIEWS_FETCH,
  { restaurantId: string; liked?: boolean; pageNumber: number; takeCount: number }
>;

export type TRestaurantCardReviewsSave = ActionCreator<
  typeof RESTAURANT_CARD_REVIEWS_SAVE,
  { data: TRestaurantPromoList }
>;

export type TRestaurantCardReviewsReset = ActionCreator<typeof RESTAURANT_CARD_REVIEWS_RESET>;

export type TRestaurantCardReviewsFetchPending = ActionCreator<
  typeof RESTAURANT_FETCH_CARD_REVIEWS_PENDING
>;

export type TRestaurantCardReviewsFetchSuccess = ActionCreator<
  typeof RESTAURANT_FETCH_CARD_REVIEWS_SUCCESS,
  { status: number }
>;

export type TRestaurantCardReviewsFetchError = ActionCreator<
  typeof RESTAURANT_FETCH_CARD_REVIEWS_ERROR,
  { status: number }
>;

export type TRestaurantCardReviewsChangePaginationLimit = ActionCreator<
  typeof RESTAURANT_CARD_REVIEWS_CHANGE_PAGINATION_LIMIT,
  { limit: number }
>;
