// Constants
import {
  RESTAURANT_CARD_REVIEWS_FETCH,
  RESTAURANT_CARD_REVIEWS_SAVE,
  RESTAURANT_CARD_REVIEWS_RESET,
  RESTAURANT_FETCH_CARD_REVIEWS_PENDING,
  RESTAURANT_FETCH_CARD_REVIEWS_SUCCESS,
  RESTAURANT_FETCH_CARD_REVIEWS_ERROR,
  RESTAURANT_CARD_REVIEWS_CHANGE_PAGINATION_LIMIT,
} from '@constants/restaurant/reviews/current';
// Types
import {
  TRestaurantCardReviewsFetch,
  TRestaurantCardReviewsSave,
  TRestaurantCardReviewsReset,
  TRestaurantCardReviewsFetchPending,
  TRestaurantCardReviewsFetchSuccess,
  TRestaurantCardReviewsFetchError,
  TRestaurantCardReviewsChangePaginationLimit,
} from './action-types';

export const restaurantReviewsFetch: TRestaurantCardReviewsFetch = (data) => ({
  type: RESTAURANT_CARD_REVIEWS_FETCH,
  payload: data,
});

export const restaurantReviewsSave: TRestaurantCardReviewsSave = (data) => ({
  type: RESTAURANT_CARD_REVIEWS_SAVE,
  payload: data,
});

export const restaurantReviewsReset: TRestaurantCardReviewsReset = () => ({
  type: RESTAURANT_CARD_REVIEWS_RESET,
});

export const restaurantReviewsFetchPending: TRestaurantCardReviewsFetchPending = () => ({
  type: RESTAURANT_FETCH_CARD_REVIEWS_PENDING,
});

export const restaurantReviewsFetchSuccess: TRestaurantCardReviewsFetchSuccess = ({ status }) => ({
  type: RESTAURANT_FETCH_CARD_REVIEWS_SUCCESS,
  payload: { status },
});

export const restaurantReviewsFetchError: TRestaurantCardReviewsFetchError = ({ status }) => ({
  type: RESTAURANT_FETCH_CARD_REVIEWS_ERROR,
  payload: { status },
});

// eslint-disable-next-line max-len
export const restaurantCardReviewsChangePaginationLimit: TRestaurantCardReviewsChangePaginationLimit = ({
  limit,
}) => ({
  type: RESTAURANT_CARD_REVIEWS_CHANGE_PAGINATION_LIMIT,
  payload: { limit },
});
