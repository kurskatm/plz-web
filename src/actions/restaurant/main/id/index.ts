// Constants
import {
  RESTAURANT_CARD_ID_FETCH,
  RESTAURANT_CARD_ID_SAVE,
  RESTAURANT_CARD_ID_RESET,
  RESTAURANT_FETCH_CARD_ID_PENDING,
  RESTAURANT_FETCH_CARD_ID_SUCCESS,
  RESTAURANT_FETCH_CARD_ID_ERROR,
} from '@constants/restaurant/main/id';
// Types
import {
  TRestaurantCardIdFetch,
  TRestaurantCardIdSave,
  TRestaurantCardIdReset,
  TRestaurantCardIdFetchPending,
  TRestaurantCardIdFetchSuccess,
  TRestaurantCardIdFetchError,
} from './action-types';

export const restaurantCardIdFetch: TRestaurantCardIdFetch = (data) => ({
  type: RESTAURANT_CARD_ID_FETCH,
  payload: data,
});

export const restaurantCardIdSave: TRestaurantCardIdSave = (data) => ({
  type: RESTAURANT_CARD_ID_SAVE,
  payload: data,
});

export const restaurantCardIdReset: TRestaurantCardIdReset = () => ({
  type: RESTAURANT_CARD_ID_RESET,
});

export const restaurantCardIdFetchPending: TRestaurantCardIdFetchPending = () => ({
  type: RESTAURANT_FETCH_CARD_ID_PENDING,
});

export const restaurantCardIdFetchSuccess: TRestaurantCardIdFetchSuccess = ({ status }) => ({
  type: RESTAURANT_FETCH_CARD_ID_SUCCESS,
  payload: { status },
});

export const restaurantCardIdFetchError: TRestaurantCardIdFetchError = ({ status }) => ({
  type: RESTAURANT_FETCH_CARD_ID_ERROR,
  payload: { status },
});
