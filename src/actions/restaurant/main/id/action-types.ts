// Main Types
import { ActionCreator } from '@type/actions';
// Constants
import {
  RESTAURANT_CARD_ID_FETCH,
  RESTAURANT_CARD_ID_SAVE,
  RESTAURANT_CARD_ID_RESET,
  RESTAURANT_FETCH_CARD_ID_PENDING,
  RESTAURANT_FETCH_CARD_ID_SUCCESS,
  RESTAURANT_FETCH_CARD_ID_ERROR,
} from '@constants/restaurant/main/id';

export type TRestaurantCardIdFetch = ActionCreator<
  typeof RESTAURANT_CARD_ID_FETCH,
  { cityId: string; restaurantName: string }
>;

export type TRestaurantCardIdSave = ActionCreator<typeof RESTAURANT_CARD_ID_SAVE, { id: string }>;

export type TRestaurantCardIdReset = ActionCreator<typeof RESTAURANT_CARD_ID_RESET>;

export type TRestaurantCardIdFetchPending = ActionCreator<typeof RESTAURANT_FETCH_CARD_ID_PENDING>;

export type TRestaurantCardIdFetchSuccess = ActionCreator<
  typeof RESTAURANT_FETCH_CARD_ID_SUCCESS,
  { status: number }
>;

export type TRestaurantCardIdFetchError = ActionCreator<
  typeof RESTAURANT_FETCH_CARD_ID_ERROR,
  { status: number }
>;
