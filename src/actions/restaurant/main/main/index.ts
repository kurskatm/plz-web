// Constants
import { RESTAURANT_CARD_MAIN_RESET } from '@constants/restaurant/main';
// Types
import { TRestaurantCardMainReset } from './action-types';

export const restaurantCardMainReset: TRestaurantCardMainReset = () => ({
  type: RESTAURANT_CARD_MAIN_RESET,
});
