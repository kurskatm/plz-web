// Main Types
import { ActionCreator } from '@type/actions';
// Constants
import { RESTAURANT_CARD_MAIN_RESET } from '@constants/restaurant/main';

export type TRestaurantCardMainReset = ActionCreator<typeof RESTAURANT_CARD_MAIN_RESET>;
