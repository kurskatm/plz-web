// Main Types
import { ActionCreator } from '@type/actions';
import { TRestaurantCard } from '@type/restaurants/restaurant-card';
// Constants
import {
  RESTAURANT_CARD_FETCH,
  RESTAURANT_CARD_SAVE,
  RESTAURANT_CARD_RESET,
  RESTAURANT_FETCH_CARD_PENDING,
  RESTAURANT_FETCH_CARD_SUCCESS,
  RESTAURANT_FETCH_CARD_ERROR,
} from '@constants/restaurant/main/card';

export type TRestaurantCardFetch = ActionCreator<
  typeof RESTAURANT_CARD_FETCH,
  { restaurantId: string }
>;

export type TRestaurantCardSave = ActionCreator<typeof RESTAURANT_CARD_SAVE, TRestaurantCard>;

export type TRestaurantCardReset = ActionCreator<typeof RESTAURANT_CARD_RESET>;

export type TRestaurantCardFetchPending = ActionCreator<typeof RESTAURANT_FETCH_CARD_PENDING>;

export type TRestaurantCardFetchSuccess = ActionCreator<
  typeof RESTAURANT_FETCH_CARD_SUCCESS,
  { status: number }
>;

export type TRestaurantCardFetchError = ActionCreator<
  typeof RESTAURANT_FETCH_CARD_ERROR,
  { status: number }
>;
