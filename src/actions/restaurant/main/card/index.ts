// Constants
import {
  RESTAURANT_CARD_FETCH,
  RESTAURANT_CARD_SAVE,
  RESTAURANT_CARD_RESET,
  RESTAURANT_FETCH_CARD_PENDING,
  RESTAURANT_FETCH_CARD_SUCCESS,
  RESTAURANT_FETCH_CARD_ERROR,
} from '@constants/restaurant/main/card';
// Types
import {
  TRestaurantCardFetch,
  TRestaurantCardSave,
  TRestaurantCardReset,
  TRestaurantCardFetchPending,
  TRestaurantCardFetchSuccess,
  TRestaurantCardFetchError,
} from './action-types';

export const restaurantCardFetch: TRestaurantCardFetch = (data) => ({
  type: RESTAURANT_CARD_FETCH,
  payload: data,
});

export const restaurantCardSave: TRestaurantCardSave = (data) => ({
  type: RESTAURANT_CARD_SAVE,
  payload: data,
});

export const restaurantCardReset: TRestaurantCardReset = () => ({
  type: RESTAURANT_CARD_RESET,
});

export const restaurantCardFetchPending: TRestaurantCardFetchPending = () => ({
  type: RESTAURANT_FETCH_CARD_PENDING,
});

export const restaurantCardFetchSuccess: TRestaurantCardFetchSuccess = ({ status }) => ({
  type: RESTAURANT_FETCH_CARD_SUCCESS,
  payload: { status },
});

export const restaurantCardFetchError: TRestaurantCardFetchError = ({ status }) => ({
  type: RESTAURANT_FETCH_CARD_ERROR,
  payload: { status },
});
