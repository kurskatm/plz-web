// Constants
import {
  RESTAURANT_SEARCH_PRODUCT,
  RESTAURANT_SEARCH_PRODUCT_PENDING,
  RESTAURANT_SEARCH_PRODUCT_SUCCESS,
  RESTAURANT_SEARCH_PRODUCT_ERROR,
  RESTAURANT_SEARCH_PRODUCT_SAVE,
  RESTAURANT_SEARCH_PRODUCT_RESET,
} from '@constants/restaurant/search-products';
// Types
import {
  TRestaurantSearchProducts,
  TRestaurantSearchProductsPending,
  TRestaurantSearchProductsSuccess,
  TRestaurantSearchProductsError,
  TRestaurantSearchProductsSave,
  TRestaurantSearchProductsReset,
} from './action-types';

export const restaurantSearchProducts: TRestaurantSearchProducts = (data) => ({
  type: RESTAURANT_SEARCH_PRODUCT,
  payload: data,
});

export const restaurantSearchProductsSave: TRestaurantSearchProductsSave = (data) => ({
  type: RESTAURANT_SEARCH_PRODUCT_SAVE,
  payload: data,
});

export const restaurantSearchProductsReset: TRestaurantSearchProductsReset = () => ({
  type: RESTAURANT_SEARCH_PRODUCT_RESET,
});

export const restaurantSearchProductsPending: TRestaurantSearchProductsPending = () => ({
  type: RESTAURANT_SEARCH_PRODUCT_PENDING,
});

export const restaurantSearchProductsSuccess: TRestaurantSearchProductsSuccess = (data) => ({
  type: RESTAURANT_SEARCH_PRODUCT_SUCCESS,
  payload: data,
});

export const restaurantSearchProductsError: TRestaurantSearchProductsError = (data) => ({
  type: RESTAURANT_SEARCH_PRODUCT_ERROR,
  payload: data,
});
