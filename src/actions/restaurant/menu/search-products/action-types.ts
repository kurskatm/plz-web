/* eslint-disable @typescript-eslint/indent */
// Main Types
import { ActionCreator } from '@type/actions';
import { TRestaurantCategoryProducts } from '@type/restaurants/restaurant-products';
// Constants
import {
  RESTAURANT_SEARCH_PRODUCT,
  RESTAURANT_SEARCH_PRODUCT_PENDING,
  RESTAURANT_SEARCH_PRODUCT_SUCCESS,
  RESTAURANT_SEARCH_PRODUCT_ERROR,
  RESTAURANT_SEARCH_PRODUCT_SAVE,
  RESTAURANT_SEARCH_PRODUCT_RESET,
} from '@constants/restaurant/search-products';

export type TRestaurantSearchProducts = ActionCreator<
  typeof RESTAURANT_SEARCH_PRODUCT,
  {
    restaurantId: string;
    storeId: string;
    query: string;
  }
>;

export type TRestaurantSearchProductsSave = ActionCreator<
  typeof RESTAURANT_SEARCH_PRODUCT_SAVE,
  { data: TRestaurantCategoryProducts[] }
>;

export type TRestaurantSearchProductsReset = ActionCreator<typeof RESTAURANT_SEARCH_PRODUCT_RESET>;

export type TRestaurantSearchProductsPending = ActionCreator<
  typeof RESTAURANT_SEARCH_PRODUCT_PENDING
>;

export type TRestaurantSearchProductsSuccess = ActionCreator<
  typeof RESTAURANT_SEARCH_PRODUCT_SUCCESS,
  { status: number }
>;

export type TRestaurantSearchProductsError = ActionCreator<
  typeof RESTAURANT_SEARCH_PRODUCT_ERROR,
  { status: number }
>;
