// Main Types
import { ActionCreator } from '@type/actions';
// Constants
import { RESTAURANT_CARD_MENU_RESET } from '@constants/restaurant/menu';

export type TRestaurantCardMenuReset = ActionCreator<typeof RESTAURANT_CARD_MENU_RESET>;
