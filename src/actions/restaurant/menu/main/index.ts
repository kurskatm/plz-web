// Constants
import { RESTAURANT_CARD_MENU_RESET } from '@constants/restaurant/menu';
// Types
import { TRestaurantCardMenuReset } from './action-types';

export const restaurantCardMenuReset: TRestaurantCardMenuReset = () => ({
  type: RESTAURANT_CARD_MENU_RESET,
});
