// Constants
import {
  RESTAURANT_CARD_CATEGORIES_FETCH,
  RESTAURANT_CARD_CATEGORIES_SAVE,
  RESTAURANT_CARD_CATEGORIES_RESET,
  RESTAURANT_FETCH_CARD_CATEGORIES_PENDING,
  RESTAURANT_FETCH_CARD_CATEGORIES_SUCCESS,
  RESTAURANT_FETCH_CARD_CATEGORIES_ERROR,
} from '@constants/restaurant/menu/categories';
// Types
import {
  TRestaurantCardCategoriesFetch,
  TRestaurantCardCategoriesSave,
  TRestaurantCardCategoriesReset,
  TRestaurantCardCategoriesFetchPending,
  TRestaurantCardCategoriesFetchSuccess,
  TRestaurantCardCategoriesFetchError,
} from './action-types';

export const restaurantCategoriesFetch: TRestaurantCardCategoriesFetch = (data) => ({
  type: RESTAURANT_CARD_CATEGORIES_FETCH,
  payload: data,
});

export const restaurantCategoriesSave: TRestaurantCardCategoriesSave = (data) => ({
  type: RESTAURANT_CARD_CATEGORIES_SAVE,
  payload: data,
});

export const restaurantCategoriesReset: TRestaurantCardCategoriesReset = () => ({
  type: RESTAURANT_CARD_CATEGORIES_RESET,
});

export const restaurantCategoriesFetchPending: TRestaurantCardCategoriesFetchPending = () => ({
  type: RESTAURANT_FETCH_CARD_CATEGORIES_PENDING,
});

export const restaurantCategoriesFetchSuccess: TRestaurantCardCategoriesFetchSuccess = ({
  status,
}) => ({
  type: RESTAURANT_FETCH_CARD_CATEGORIES_SUCCESS,
  payload: { status },
});

export const restaurantCategoriesFetchError: TRestaurantCardCategoriesFetchError = ({
  status,
}) => ({
  type: RESTAURANT_FETCH_CARD_CATEGORIES_ERROR,
  payload: { status },
});
