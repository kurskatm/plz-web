// Main Types
import { ActionCreator } from '@type/actions';
import { TRestaurantCategoriesList } from '@type/restaurants/restaurant-categories';
// Constants
import {
  RESTAURANT_CARD_CATEGORIES_FETCH,
  RESTAURANT_CARD_CATEGORIES_SAVE,
  RESTAURANT_CARD_CATEGORIES_RESET,
  RESTAURANT_FETCH_CARD_CATEGORIES_PENDING,
  RESTAURANT_FETCH_CARD_CATEGORIES_SUCCESS,
  RESTAURANT_FETCH_CARD_CATEGORIES_ERROR,
} from '@constants/restaurant/menu/categories';

export type TRestaurantCardCategoriesFetch = ActionCreator<
  typeof RESTAURANT_CARD_CATEGORIES_FETCH,
  { restaurantId: string; storeId: string }
>;

export type TRestaurantCardCategoriesSave = ActionCreator<
  typeof RESTAURANT_CARD_CATEGORIES_SAVE,
  TRestaurantCategoriesList
>;

export type TRestaurantCardCategoriesReset = ActionCreator<typeof RESTAURANT_CARD_CATEGORIES_RESET>;

export type TRestaurantCardCategoriesFetchPending = ActionCreator<
  typeof RESTAURANT_FETCH_CARD_CATEGORIES_PENDING
>;

export type TRestaurantCardCategoriesFetchSuccess = ActionCreator<
  typeof RESTAURANT_FETCH_CARD_CATEGORIES_SUCCESS,
  { status: number }
>;

export type TRestaurantCardCategoriesFetchError = ActionCreator<
  typeof RESTAURANT_FETCH_CARD_CATEGORIES_ERROR,
  { status: number }
>;
