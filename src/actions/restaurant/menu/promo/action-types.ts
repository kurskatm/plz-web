// Main Types
import { ActionCreator } from '@type/actions';
import { TRestaurantPromoList } from '@type/restaurants/promo';
// Constants
import {
  RESTAURANT_CARD_PROMO_FETCH,
  RESTAURANT_CARD_PROMO_SAVE,
  RESTAURANT_CARD_PROMO_RESET,
  RESTAURANT_FETCH_CARD_PROMO_PENDING,
  RESTAURANT_FETCH_CARD_PROMO_SUCCESS,
  RESTAURANT_FETCH_CARD_PROMO_ERROR,
} from '@constants/restaurant/menu/promo';

export type TRestaurantCardPromoFetch = ActionCreator<
  typeof RESTAURANT_CARD_PROMO_FETCH,
  { restaurantId: string; storeId: string }
>;

export type TRestaurantCardPromoSave = ActionCreator<
  typeof RESTAURANT_CARD_PROMO_SAVE,
  { data: TRestaurantPromoList }
>;

export type TRestaurantCardPromoReset = ActionCreator<typeof RESTAURANT_CARD_PROMO_RESET>;

export type TRestaurantCardPromoFetchPending = ActionCreator<
  typeof RESTAURANT_FETCH_CARD_PROMO_PENDING
>;

export type TRestaurantCardPromoFetchSuccess = ActionCreator<
  typeof RESTAURANT_FETCH_CARD_PROMO_SUCCESS,
  { status: number }
>;

export type TRestaurantCardPromoFetchError = ActionCreator<
  typeof RESTAURANT_FETCH_CARD_PROMO_ERROR,
  { status: number }
>;
