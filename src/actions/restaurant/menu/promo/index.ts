// Constants
import {
  RESTAURANT_CARD_PROMO_FETCH,
  RESTAURANT_CARD_PROMO_SAVE,
  RESTAURANT_CARD_PROMO_RESET,
  RESTAURANT_FETCH_CARD_PROMO_PENDING,
  RESTAURANT_FETCH_CARD_PROMO_SUCCESS,
  RESTAURANT_FETCH_CARD_PROMO_ERROR,
} from '@constants/restaurant/menu/promo';
// Types
import {
  TRestaurantCardPromoFetch,
  TRestaurantCardPromoSave,
  TRestaurantCardPromoReset,
  TRestaurantCardPromoFetchPending,
  TRestaurantCardPromoFetchSuccess,
  TRestaurantCardPromoFetchError,
} from './action-types';

export const restaurantPromoFetch: TRestaurantCardPromoFetch = (data) => ({
  type: RESTAURANT_CARD_PROMO_FETCH,
  payload: data,
});

export const restaurantPromoSave: TRestaurantCardPromoSave = (data) => ({
  type: RESTAURANT_CARD_PROMO_SAVE,
  payload: data,
});

export const restaurantPromoReset: TRestaurantCardPromoReset = () => ({
  type: RESTAURANT_CARD_PROMO_RESET,
});

export const restaurantPromoFetchPending: TRestaurantCardPromoFetchPending = () => ({
  type: RESTAURANT_FETCH_CARD_PROMO_PENDING,
});

export const restaurantPromoFetchSuccess: TRestaurantCardPromoFetchSuccess = ({ status }) => ({
  type: RESTAURANT_FETCH_CARD_PROMO_SUCCESS,
  payload: { status },
});

export const restaurantPromoFetchError: TRestaurantCardPromoFetchError = ({ status }) => ({
  type: RESTAURANT_FETCH_CARD_PROMO_ERROR,
  payload: { status },
});
