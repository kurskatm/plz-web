// Constants
import {
  RESTAURANT_CARD_PRODUCTS_FETCH,
  RESTAURANT_CARD_PRODUCTS_SAVE,
  RESTAURANT_CARD_PRODUCTS_RESET,
  RESTAURANT_FETCH_CARD_PRODUCTS_PENDING,
  RESTAURANT_FETCH_CARD_PRODUCTS_SUCCESS,
  RESTAURANT_FETCH_CARD_PRODUCTS_ERROR,
} from '@constants/restaurant/menu/products';
// Types
import {
  TRestaurantCardProductsFetch,
  TRestaurantCardProductsSave,
  TRestaurantCardProductsReset,
  TRestaurantCardProductsFetchPending,
  TRestaurantCardProductsFetchSuccess,
  TRestaurantCardProductsFetchError,
} from './action-types';

export const restaurantProductsFetch: TRestaurantCardProductsFetch = (data) => ({
  type: RESTAURANT_CARD_PRODUCTS_FETCH,
  payload: data,
});

export const restaurantProductsSave: TRestaurantCardProductsSave = (data) => ({
  type: RESTAURANT_CARD_PRODUCTS_SAVE,
  payload: data,
});

export const restaurantProductsReset: TRestaurantCardProductsReset = () => ({
  type: RESTAURANT_CARD_PRODUCTS_RESET,
});

export const restaurantProductsFetchPending: TRestaurantCardProductsFetchPending = () => ({
  type: RESTAURANT_FETCH_CARD_PRODUCTS_PENDING,
});

export const restaurantProductsFetchSuccess: TRestaurantCardProductsFetchSuccess = ({
  status,
}) => ({
  type: RESTAURANT_FETCH_CARD_PRODUCTS_SUCCESS,
  payload: { status },
});

export const restaurantProductsFetchError: TRestaurantCardProductsFetchError = ({ status }) => ({
  type: RESTAURANT_FETCH_CARD_PRODUCTS_ERROR,
  payload: { status },
});
