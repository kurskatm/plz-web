// Main Types
import { ActionCreator } from '@type/actions';
import { TRestaurantCategoryProducts } from '@type/restaurants/restaurant-products';
// Constants
import {
  RESTAURANT_CARD_PRODUCTS_FETCH,
  RESTAURANT_CARD_PRODUCTS_SAVE,
  RESTAURANT_CARD_PRODUCTS_RESET,
  RESTAURANT_FETCH_CARD_PRODUCTS_PENDING,
  RESTAURANT_FETCH_CARD_PRODUCTS_SUCCESS,
  RESTAURANT_FETCH_CARD_PRODUCTS_ERROR,
} from '@constants/restaurant/menu/products';

export type TRestaurantCardProductsFetch = ActionCreator<
  typeof RESTAURANT_CARD_PRODUCTS_FETCH,
  { restaurantId: string; storeId: string }
>;

export type TRestaurantCardProductsSave = ActionCreator<
  typeof RESTAURANT_CARD_PRODUCTS_SAVE,
  { data: TRestaurantCategoryProducts[] }
>;

export type TRestaurantCardProductsReset = ActionCreator<typeof RESTAURANT_CARD_PRODUCTS_RESET>;

export type TRestaurantCardProductsFetchPending = ActionCreator<
  typeof RESTAURANT_FETCH_CARD_PRODUCTS_PENDING
>;

export type TRestaurantCardProductsFetchSuccess = ActionCreator<
  typeof RESTAURANT_FETCH_CARD_PRODUCTS_SUCCESS,
  { status: number }
>;

export type TRestaurantCardProductsFetchError = ActionCreator<
  typeof RESTAURANT_FETCH_CARD_PRODUCTS_ERROR,
  { status: number }
>;
