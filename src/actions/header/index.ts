// Constants
import { HEADER_UX_SET_HEIGHT } from '@constants/header';
// Types
import { THeaderUxSetHeight } from './action-types';

// Ux
export const headerUxSetHeight: THeaderUxSetHeight = (height: number) => ({
  type: HEADER_UX_SET_HEIGHT,
  payload: height,
});
