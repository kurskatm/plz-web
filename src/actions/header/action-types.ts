/* eslint-disable @typescript-eslint/indent */
// Modules Types
import { ActionCreator } from '@type/actions';
// Constants
import {
  // Ux
  HEADER_UX_SET_HEIGHT,
} from '@constants/header';

// Ux
export type THeaderUxSetHeight = ActionCreator<typeof HEADER_UX_SET_HEIGHT, number>;
