// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { RESTAURANTS_AND_HITS_FETCH } from '@constants/restaurants-and-hits';
// Actions
import {
  restaurantsAndHitsSave,
  restaurantsAndHitsPending,
  restaurantsAndHitsSuccess,
  restaurantsAndHitsError,
} from '@actions/restaurants-and-hits';
// Api
import { getRestaurantsAndHits } from '@/api/restaurant-and-hits/fetch';
// Types
import { TRestaurantsAndHitsEpic } from './types';

export const fetchRestaurantsAndHitsEpic: TRestaurantsAndHitsEpic = (action$) =>
  action$.pipe(
    ofType(RESTAURANTS_AND_HITS_FETCH),
    switchMap(({ payload }) =>
      concat(
        of(restaurantsAndHitsPending()),
        getRestaurantsAndHits(payload).pipe(
          switchMap((response) => {
            const { status } = response;

            if (status === 200) {
              const { response: data } = response;

              if (payload.onSuccess) {
                payload.onSuccess();
              }

              return [restaurantsAndHitsSave(data), restaurantsAndHitsSuccess({ status })];
            }

            return [restaurantsAndHitsError({ status })];
          }),
          catchError((error) => {
            console.error(error);
            return of(restaurantsAndHitsError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
