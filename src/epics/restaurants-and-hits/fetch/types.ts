// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/restaurants-and-hits/types';

export type TRestaurantsAndHitsEpic = Epic<ActionTypes, ActionTypes>;
