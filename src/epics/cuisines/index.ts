// Modules
import { combineEpics } from 'redux-observable';
// Epics
import { fetchCuisinesEpic } from './fetch';

export const cuisinesEpic = combineEpics(fetchCuisinesEpic);
