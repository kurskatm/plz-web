// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { CUISINES_FETCH } from '@constants/cuisines';
// Actions
import {
  cuisinesSave,
  cuisinesFetchPending,
  cuisinesFetchSuccess,
  cuisinesFetchError,
} from '@actions/cuisines';
// Api
import { getCuisines } from '@api/cuisines/fetch';
// Types
import { TFetchCuisinesEpic } from './types';

export const fetchCuisinesEpic: TFetchCuisinesEpic = (action$) =>
  action$.pipe(
    ofType(CUISINES_FETCH),
    switchMap(() =>
      concat(
        of(cuisinesFetchPending()),
        getCuisines().pipe(
          switchMap((response) => {
            const { status } = response;

            if (status === 200) {
              const { response: data } = response;
              return [cuisinesSave(data), cuisinesFetchSuccess({ status })];
            }

            return [cuisinesFetchError({ status })];
          }),
          catchError(({ status }) => of(cuisinesFetchError({ status }))),
        ),
      ),
    ),
  );
