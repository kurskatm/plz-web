// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/cuisines/types';

export type TFetchCuisinesEpic = Epic<ActionTypes, ActionTypes>;
