// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { FOOD_FOR_POINTS_FETCH } from '@constants/foodForPoints';
// Actions
import {
  foodForPointsSave,
  foodForPointsPending,
  foodForPointsSuccess,
  foodForPointsError,
} from '@actions/foodForPoints';
// Api
import { getFoodForPoints } from '@/api/foodForPoints/fetch';
// Types
import { TFoodForPointsEpic } from './types';

export const fetchFoodForPointsEpic: TFoodForPointsEpic = (action$) =>
  action$.pipe(
    ofType(FOOD_FOR_POINTS_FETCH),
    switchMap(({ payload }) =>
      concat(
        of(foodForPointsPending()),
        getFoodForPoints(payload).pipe(
          switchMap((response) => {
            const { status } = response;
            if (status === 200) {
              const { response: data } = response;
              return [foodForPointsSuccess({ status }), foodForPointsSave(data)];
            }

            return [foodForPointsError({ status })];
          }),
          catchError((error) => {
            console.error(error);
            return of(foodForPointsError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
