// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/foodForPoints/types';

export type TFoodForPointsEpic = Epic<ActionTypes, ActionTypes>;
