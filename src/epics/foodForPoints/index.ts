// Modules
import { combineEpics } from 'redux-observable';
// Epics
import { fetchFoodForPointsEpic } from './fetch';

export const foodForPointsEpic = combineEpics(fetchFoodForPointsEpic);
