// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/promocode/types';

export type TPromocodeEpic = Epic<ActionTypes, ActionTypes>;
