// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { PROMOCODE_VALIDATE } from '@constants/promocode';
// Actions
import {
  promocodeError,
  promocodeSave,
  promocodePending,
  promocodeSuccess,
} from '@actions/promocode';
// Api
import { validatePromocode } from '@/api/promocode/validate';
// Types
import { TPromocodeEpic } from './types';

export const validatePromocodeEpic: TPromocodeEpic = (action$) =>
  action$.pipe(
    ofType(PROMOCODE_VALIDATE),
    switchMap(({ payload }) =>
      concat(
        of(promocodePending()),
        validatePromocode(payload).pipe(
          switchMap((response) => {
            const { status } = response;

            if (status === 200) {
              const { response: data } = response;

              return [promocodeSuccess({ status }), promocodeSave(data)];
            }

            return [promocodeError({ status })];
          }),
          catchError((error) => {
            console.error(error);
            return of(promocodeError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
