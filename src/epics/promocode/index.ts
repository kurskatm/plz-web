// Modules
import { combineEpics } from 'redux-observable';
// Epics
import { validatePromocodeEpic } from './validate';

export const promocodeEpic = combineEpics(validatePromocodeEpic);
