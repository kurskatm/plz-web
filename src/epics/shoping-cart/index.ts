// Modules
import { combineEpics } from 'redux-observable';
// Epics
import { fetchShopingCartEpic } from './fetch-cart';
import { saveShopingCartEpic } from './save-cart';
import { fetchShopCartRestaurantEpic } from './fetch-restaurant';

export const shopingCartEpic = combineEpics(
  fetchShopingCartEpic,
  saveShopingCartEpic,
  fetchShopCartRestaurantEpic,
);
