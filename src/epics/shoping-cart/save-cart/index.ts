// Modules
import { combineLatest, concat, of, timer } from 'rxjs';
import { catchError, debounceTime, map, mergeMap, switchMap, takeUntil } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { SHOPING_CART_RESET, SHOPING_CART_SAVE } from '@constants/shoping-cart';
// Actions
import {
  shopingCartSavePending,
  shopingCartSaveSuccess,
  shopingCartSaveError,
} from '@actions/shoping-cart/save';
import { pushNotification } from '@/actions/notifications';
import { changeAddressModalShow } from '@/actions/checkout/modals';
// Api
import { saveShopingCart } from '@api/shoping-cart/save';
// Utils
import { Consts } from '@utils';
import { isValidPageForAddressModal } from './utils';
// Types
import { TShopingCartSaveEpic } from './types';

const { MIN_LOADING_MS, REQUEST_MS } = Consts.DEBOUNCE;

export const saveShopingCartEpic: TShopingCartSaveEpic = (action$) =>
  action$.pipe(
    ofType(SHOPING_CART_SAVE),
    debounceTime(REQUEST_MS),
    mergeMap(({ payload }) =>
      concat(
        of(shopingCartSavePending()),
        combineLatest([timer(MIN_LOADING_MS / 2), saveShopingCart(payload)]).pipe(
          map((x) => x[1]),
          switchMap((response) => {
            const { status, response: data } = response;
            if ([200, 204].includes(status)) {
              return [shopingCartSaveSuccess({ status, data })];
            }

            return [shopingCartSaveError({ status })];
          }),
          catchError((error) => {
            const { status, response } = error;
            const result = [
              shopingCartSaveError({
                status,
                details: {
                  errorMessage: error?.response?.detail,
                },
              }),
            ];
            const isValidPageForShowingErr = isValidPageForAddressModal(window.location.pathname);
            // TODO надо бы запросить у бэка коды ошибок
            const showAddressModal = response?.validationError === 'invalidAddress';

            if (isValidPageForShowingErr) {
              if (showAddressModal) {
                result.push(
                  // @ts-ignore
                  changeAddressModalShow({ show: true }),
                );
              } else {
                result.push(
                  // @ts-ignore
                  pushNotification({
                    type: 'error',
                    content: response?.detail,
                  }),
                );
              }
            }

            console.log(error);

            return result;
          }),
        ),
      ).pipe(takeUntil(action$.pipe(ofType(SHOPING_CART_RESET)))),
    ),
  );
