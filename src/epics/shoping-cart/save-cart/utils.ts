const RestaurantPath = new RegExp('/[a-z]*/restaurant/');
const FoodForPointsPath = new RegExp('/[a-z]*/food-for-points');
const ProfileFavoritesPath = new RegExp('/[a-z]*/profile/favorite');
const CheckoutPath = new RegExp('/[a-z]*/checkout');

export const isValidPageForAddressModal = (path: string): boolean =>
  !!path.match(RestaurantPath) ||
  !!path.match(FoodForPointsPath) ||
  !!path.match(ProfileFavoritesPath) ||
  !!path.match(CheckoutPath);
