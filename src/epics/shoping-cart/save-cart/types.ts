// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/shoping-cart/save/types';

export type TShopingCartSaveEpic = Epic<ActionTypes, ActionTypes>;
