// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/shoping-cart/fetch/types';

export type TShopingCartFetchEpic = Epic<ActionTypes, ActionTypes>;
