import { TProductsArray } from '@type/orderFromProfileToCart';

export const getProductsWithModifiers = (products: TProductsArray): TProductsArray =>
  products.map((prod) => {
    const modifiers = prod.selectedModifierGroups.map((group) => ({
      id: group.id,
      name: group.name,
      modifiers: group.selectedModifiers,
    }));
    delete prod.selectedModifierGroups;
    delete prod.externalId;
    delete prod.imageUri;
    return { modifiers, ...prod };
  });
