// Modules
import { combineLatest, concat, of, timer } from 'rxjs';
import { catchError, debounceTime, map, mergeMap, switchMap, takeUntil } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { SHOPING_CART_RESET, SHOPING_CART_FETCH } from '@constants/shoping-cart';
import { TOrderToCart } from '@type/orderFromProfileToCart';

// Actions
import {
  shopingCartFetchSave,
  shopingCartFetchPending,
  shopingCartFetchSuccess,
  shopingCartFetchError,
} from '@actions/shoping-cart/fetch';
// Api
import { fetchShopingCart } from '@api/shoping-cart/fetch';
// Utils
import { setCartId } from '@/lib/cookie';
import { Consts } from '@utils';

import { getProductsWithModifiers } from './config';
// Types
import { TShopingCartFetchEpic } from './types';

const { MIN_LOADING_MS, REQUEST_MS } = Consts.DEBOUNCE;

export const fetchShopingCartEpic: TShopingCartFetchEpic = (action$) =>
  action$.pipe(
    ofType(SHOPING_CART_FETCH),
    debounceTime(REQUEST_MS),
    mergeMap(({ payload }) =>
      concat(
        of(shopingCartFetchPending()),
        combineLatest([timer(MIN_LOADING_MS), fetchShopingCart(payload)]).pipe(
          map((x) => x[1]),
          switchMap((response) => {
            const { status } = response;

            if ([200].includes(status)) {
              const { response: data }: { response: TOrderToCart } = response;
              const { products: productsTmp } = data;
              const products = getProductsWithModifiers(productsTmp);
              setCartId(data.id);
              return [
                shopingCartFetchSuccess({ status }),
                // @ts-ignore
                shopingCartFetchSave({ ...data, products }),
              ];
            }

            return [shopingCartFetchError({ status })];
          }),
          catchError((error) => {
            const { status } = error;
            console.log(error);

            return of(shopingCartFetchError({ status }));
          }),
        ),
      ).pipe(takeUntil(action$.pipe(ofType(SHOPING_CART_RESET)))),
    ),
  );
