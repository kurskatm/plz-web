// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/shoping-cart/restaurant/types';

export type TFetchShopCartRestaurantEpic = Epic<ActionTypes, ActionTypes>;
