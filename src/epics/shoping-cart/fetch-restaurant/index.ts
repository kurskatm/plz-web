// Modules
import { combineLatest, concat, of, timer } from 'rxjs';
import { catchError, debounceTime, map, mergeMap, switchMap, takeUntil } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { SHOPCART_RESTAURANT_FETCH } from '@constants/shoping-cart-rest';
import { SHOPING_CART_RESET } from '@constants/shoping-cart';
// Actions
import {
  restaurantShopCartRestSave,
  restaurantShopCartRestFetchPending,
  restaurantShopCartRestFetchSuccess,
  restaurantShopCartRestFetchError,
} from '@actions/shoping-cart/restaurant';
// Api
import { fetchRestaurantCard } from '@api/restaurant/fetch-card';
// Utils
import { Consts } from '@utils';
// Types
import { changeAddressModalShow } from '@/actions/checkout/modals';
import { TFetchShopCartRestaurantEpic } from './types';

const { MIN_LOADING_MS, REQUEST_MS } = Consts.DEBOUNCE;

export const fetchShopCartRestaurantEpic: TFetchShopCartRestaurantEpic = (action$) =>
  action$.pipe(
    ofType(SHOPCART_RESTAURANT_FETCH),
    debounceTime(REQUEST_MS),
    mergeMap(({ payload }) =>
      concat(
        of(restaurantShopCartRestFetchPending()),
        combineLatest([timer(MIN_LOADING_MS), fetchRestaurantCard(payload)]).pipe(
          map((x) => x[1]),
          switchMap((response) => {
            const { status } = response;

            if ([200].includes(status)) {
              const data = response.response;
              return [
                restaurantShopCartRestSave(data),
                restaurantShopCartRestFetchSuccess({ status }),
              ];
            }

            return [restaurantShopCartRestFetchError({ status })];
          }),
          catchError((error) => {
            const { status } = error;
            console.log(error);

            return [
              changeAddressModalShow({ show: true }),
              restaurantShopCartRestFetchError({ status }),
            ];
          }),
        ),
      ).pipe(takeUntil(action$.pipe(ofType(SHOPING_CART_RESET)))),
    ),
  );
