// Modules
import { combineEpics } from 'redux-observable';
// Epics
import { authPhoneVerificationEpic } from './phoneVerification';
import { authOtpVerificationEpic } from './otpVerification';
import { authPhoneCleanEpic } from './phoneClean';

export const authEpic = combineEpics(
  authPhoneVerificationEpic,
  authOtpVerificationEpic,
  authPhoneCleanEpic,
);
