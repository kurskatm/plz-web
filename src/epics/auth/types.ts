// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/auth/types';

export type TAuthVerificationEpic = Epic<ActionTypes, ActionTypes>;
