// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { AUTH_PHONE_SEND } from '@constants/auth';
// Actions
import { authSaveResendToken, authPending, authSuccess, authError } from '@actions/auth';
// Api
import { verifyPhoneRequest } from '@/api/auth/phoneVerification';
// Types
import { TAuthVerificationEpic } from '../types';

export const authPhoneVerificationEpic: TAuthVerificationEpic = (action$) =>
  action$.pipe(
    ofType(AUTH_PHONE_SEND),
    switchMap(({ payload }) =>
      concat(
        of(authPending()),
        verifyPhoneRequest(payload).pipe(
          switchMap((response) => {
            const { status } = response;

            if (status === 200 || status === 202) {
              const { response: data } = response;

              return [authSaveResendToken(data), authSuccess({ status })];
            }

            return [authError({ status })];
          }),
          catchError((error) => {
            console.error(error);
            return of(authError({ status: error.response.errors.phone[0] }));
          }),
        ),
      ),
    ),
  );
