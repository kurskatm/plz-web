// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { AUTH_PHONE_CLEAN } from '@constants/auth';
// Actions
import { authError } from '@actions/auth';
// Api
import { cleanePhoneRequest } from '@/api/auth/phoneClean';
// Types
import { TAuthVerificationEpic } from '../types';

export const authPhoneCleanEpic: TAuthVerificationEpic = (action$) =>
  action$.pipe(
    ofType(AUTH_PHONE_CLEAN),
    switchMap(({ payload }) =>
      concat(
        cleanePhoneRequest(payload).pipe(
          switchMap((response) => {
            const { status } = response;

            if (status === 200 || status === 202) {
              return [];
            }

            return [];
          }),
          catchError((error) => {
            console.error(error);
            return of(authError({ status: error.response.errors.phone[0] }));
          }),
        ),
      ),
    ),
  );
