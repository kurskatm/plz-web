// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { AUTH_OTP_SEND } from '@constants/auth';
// Actions
import {
  authSaveOtpData,
  authOtpPending,
  authOtpSuccess,
  authOtpError,
  authModalShow,
} from '@actions/auth';
import { profileAuthorized } from '@actions/profile';
import { setProfileData } from '@lib/cookie';
// Api
import { decodeToken } from '@utils/jwt-decode';
import { verifyOtpRequest } from '@/api/auth/otpVerification';
// Type
import { TAuthVerificationEpic } from '../types';

export const authOtpVerificationEpic: TAuthVerificationEpic = (action$) =>
  action$.pipe(
    ofType(AUTH_OTP_SEND),
    switchMap(({ payload }) =>
      concat(
        of(authOtpPending()),
        verifyOtpRequest(payload).pipe(
          switchMap((response) => {
            const { status } = response;

            if (status === 200) {
              const { response: data } = response;
              const profileData = {
                userId: decodeToken(data.access_token)?.sub as string,
                access_token: data.access_token,
                refresh_token: data.refresh_token,
              };
              setProfileData(profileData);

              return [
                authSaveOtpData(data),
                authOtpSuccess({ status }),
                profileAuthorized(profileData),
                authModalShow(false),
              ];
            }

            return [authOtpError({ status })];
          }),
          catchError((error) => {
            console.error(error);
            return of(authOtpError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
