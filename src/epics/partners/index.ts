// Modules
import { combineEpics } from 'redux-observable';
// Epics
import { becomePartnerEpic } from './become';
import { PartnerCountersEpic } from './counters';

export const partnersEpic = combineEpics(becomePartnerEpic, PartnerCountersEpic);
