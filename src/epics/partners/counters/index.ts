// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { PARTNERS_COUNTERS_FETCH } from '@constants/partners';
// Actions
import {
  partnersCountersPending,
  partnersCountersSuccess,
  partnersCountersError,
  partnersCountersSave,
} from '@actions/partners';
// Api
import { getCounters } from '@/api/partners/fetch';
// Types
import { TPartnerCountersEpic } from './types';

export const PartnerCountersEpic: TPartnerCountersEpic = (action$) =>
  action$.pipe(
    ofType(PARTNERS_COUNTERS_FETCH),
    switchMap(() =>
      concat(
        of(partnersCountersPending()),
        getCounters().pipe(
          switchMap((response) => {
            const { status } = response;

            if (status === 200) {
              const { response: data } = response;

              return [partnersCountersSave(data), partnersCountersSuccess({ status })];
            }

            return [partnersCountersError({ status })];
          }),
          catchError((error) => {
            console.error(error);
            return of(partnersCountersError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
