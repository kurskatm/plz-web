// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { PARTNERS_BECOME_PARTNER_ADD } from '@constants/partners';
// Actions
import {
  partnersBecomePartnerPending,
  partnersBecomePartnerSuccess,
  partnersBecomePartnerError,
} from '@actions/partners';
// Api
import { becomePartner } from '@/api/partners/becomePartner';
// Types
import { TBecomePartnerEpic } from './types';

export const becomePartnerEpic: TBecomePartnerEpic = (action$) =>
  action$.pipe(
    ofType(PARTNERS_BECOME_PARTNER_ADD),
    switchMap(({ payload }) =>
      concat(
        of(partnersBecomePartnerPending()),
        becomePartner(payload).pipe(
          switchMap((response) => {
            const { status } = response;

            if (status === 204) {
              return [partnersBecomePartnerSuccess({ status })];
            }

            return [partnersBecomePartnerError({ status })];
          }),
          catchError((error) => {
            console.error(error);
            return of(partnersBecomePartnerError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
