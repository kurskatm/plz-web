// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/partners/types';

export type TBecomePartnerEpic = Epic<ActionTypes, ActionTypes>;
