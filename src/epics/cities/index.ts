// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { CITIES_FETCH } from '@constants/cities';
// Actions
import {
  citiesFetchPending,
  citiesFetchSuccess,
  citiesFetchError,
  citiesFetchSaveData,
} from '@actions/cities';
// Api
import { getCities } from '@api/cities';
// Types
import { TFetchCitiesEpic } from './types';

export const citiesEpic: TFetchCitiesEpic = (action$) =>
  action$.pipe(
    ofType(CITIES_FETCH),
    switchMap(() =>
      concat(
        of(citiesFetchPending()),
        getCities().pipe(
          switchMap((response) => {
            const { status } = response;

            if (status === 200) {
              const { response: data } = response;
              return [citiesFetchSaveData(data), citiesFetchSuccess({ status })];
            }

            return [citiesFetchError({ status })];
          }),
          catchError(({ status }) => of(citiesFetchError({ status }))),
        ),
      ),
    ),
  );
