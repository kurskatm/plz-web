// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { PROFILE_AUTHORIZED } from '@constants/profile/auth';
import { PROFILE_GET_INFO } from '@constants/profile/info';
// Actions
import {
  profileGetInfoPending,
  profileGetInfoSuccess,
  profileGetInfoError,
  profileGetInfoSaveData,
} from '@actions/profile/info';
// Api
import { getProfileInfo } from '@/api/profile/info/fetch';
// Types
import { TProfileInfoEpic } from './types';

export const getProfileInfoEpic: TProfileInfoEpic = (action$) =>
  action$.pipe(
    ofType(PROFILE_GET_INFO, PROFILE_AUTHORIZED),
    switchMap(({ payload }) =>
      concat(
        of(profileGetInfoPending()),
        getProfileInfo(payload).pipe(
          switchMap((response) => {
            const { status } = response;

            if (status === 200) {
              const { response: data } = response;

              return [profileGetInfoSaveData(data), profileGetInfoSuccess({ status })];
            }

            return [profileGetInfoError({ status })];
          }),
          catchError((error) => {
            console.error(error);
            return of(profileGetInfoError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
