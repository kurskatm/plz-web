// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/profile/info/types';

export type TProfileInfoEpic = Epic<ActionTypes, ActionTypes>;
