// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { PROFILE_UPDATE_INFO } from '@constants/profile/info';
// Actions
import {
  profileInfoUpdatePending,
  profileInfoUpdateSuccess,
  profileInfoUpdateError,
  profileInfoUpdateSave,
} from '@actions/profile/info';
// Api
import { updateProfileInfo } from '@/api/profile/info/update';
// Types
import { TProfileInfoEpic } from './types';

export const updateProfileInfoEpic: TProfileInfoEpic = (action$) =>
  action$.pipe(
    ofType(PROFILE_UPDATE_INFO),
    switchMap(({ payload }) =>
      concat(
        of(profileInfoUpdatePending()),
        updateProfileInfo(payload).pipe(
          switchMap((response) => {
            const { status, response: res } = response;
            const confirmReqStatus = res.emailConfirmationRequestStatus;
            if (status === 200 && (!confirmReqStatus || confirmReqStatus.sentSuccessfully)) {
              if (payload?.onSuccess) {
                payload.onSuccess();
              }

              return [profileInfoUpdateSuccess({ status }), profileInfoUpdateSave(payload)];
            }

            const reqError = confirmReqStatus?.errorMessage;
            return [profileInfoUpdateError(reqError)];
          }),
          catchError((error) => {
            console.error(error);
            return of(profileInfoUpdateError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
