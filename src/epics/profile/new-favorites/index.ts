// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { PROFILE_GET_FAVORITES_INFO } from '@constants/profile';
// Actions
import {
  profileFetchFavoritesPending,
  profileFetchFavoritesSuccess,
  profileFetchFavoritesError,
  profileFetchFavoritesSave,
} from '@actions/profile/new-favorites';
// Api
import { getFavorites } from '@api/profile/new-favorites/fetch';
// Config
import { getValidData } from './config';
// Types
import { TProfileFavoritesEpic } from './types';

export const getProfileFavoritesEpic: TProfileFavoritesEpic = (action$) =>
  action$.pipe(
    ofType(PROFILE_GET_FAVORITES_INFO),
    switchMap(({ payload }) =>
      concat(
        of(profileFetchFavoritesPending()),
        getFavorites(payload).pipe(
          switchMap((response) => {
            const { status } = response;

            if (status === 200) {
              const { response: data } = response;

              return [
                profileFetchFavoritesSave(getValidData(data)),
                profileFetchFavoritesSuccess({ status }),
              ];
            }

            return [profileFetchFavoritesError({ status })];
          }),
          catchError((error) => {
            console.error(error);
            return of(profileFetchFavoritesError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
