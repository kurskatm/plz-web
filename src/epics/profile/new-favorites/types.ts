// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/profile/favourite/types';
// Main types
import { TFavoritesSummaryData } from '@type/new-favorites';

export type TProfileFavoritesEpic = Epic<ActionTypes, ActionTypes>;

export type TGetValidData = (data: TFavoritesSummaryData) => TFavoritesSummaryData;
