/* eslint-disable @typescript-eslint/indent */
// Types
import { TGetValidData } from './types';

export const getValidData: TGetValidData = (data) => ({
  restaurants: data.restaurants,
  products: data.products.map((product) => ({
    ...product,
    modifiers: product.modifiers ?? [],
    availableInBonusPoints: product.availableInBonusPoints ?? false,
  })),
});
