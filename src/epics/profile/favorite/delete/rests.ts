// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
import { PROFILE_DELETE_FAVOURITE_REST } from '@constants/profile/favourite/rests';
// Actions
import {
  profileDeleteFavoriteRestsPending,
  profileDeleteFavoriteRestsSuccess,
  profileDeleteFavoriteRestsError,
  profileDeleteFavoriteRestsSaveData,
} from '@actions/profile/favourite';
// Api
import { deleteFavoriteRestaurant } from '@/api/profile/favorite/restaurants/delete';
// Types
import { TProfileFavouriteDeleteEpic } from './types';

export const deleteProfileFavoriteRestsEpic: TProfileFavouriteDeleteEpic = (action$) =>
  action$.pipe(
    ofType(PROFILE_DELETE_FAVOURITE_REST),
    switchMap(({ payload }) =>
      concat(
        of(profileDeleteFavoriteRestsPending()),
        deleteFavoriteRestaurant(payload).pipe(
          switchMap((response) => {
            const { status } = response;

            if ([200, 204].includes(status)) {
              return [
                profileDeleteFavoriteRestsSaveData(payload),
                profileDeleteFavoriteRestsSuccess({ status }),
              ];
            }

            return [profileDeleteFavoriteRestsError({ status })];
          }),
          catchError((error) => {
            console.error(error);
            return of(profileDeleteFavoriteRestsError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
