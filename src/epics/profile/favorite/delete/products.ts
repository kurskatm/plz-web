// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { PROFILE_DELETE_FAVOURITE_PRODUCTS } from '@constants/profile/favourite/products';
// Actions
import {
  profileDeleteFavoriteProductPending,
  profileDeleteFavoriteProductSuccess,
  profileDeleteFavoriteProductError,
  profileDeleteFavoriteProductSaveData,
} from '@actions/profile/favourite';
// Api
import { deleteFavoriteProduct } from '@/api/profile/favorite/products/delete';
// Types
import { TProfileFavouriteDeleteEpic } from './types';

export const deleteProfileFavoriteProductsEpic: TProfileFavouriteDeleteEpic = (action$) =>
  action$.pipe(
    ofType(PROFILE_DELETE_FAVOURITE_PRODUCTS),
    switchMap(({ payload }) =>
      concat(
        of(profileDeleteFavoriteProductPending()),
        deleteFavoriteProduct(payload).pipe(
          switchMap((response) => {
            const { status } = response;

            if ([200, 204].includes(status)) {
              return [
                profileDeleteFavoriteProductSaveData(payload),
                profileDeleteFavoriteProductSuccess({ status }),
              ];
            }

            return [profileDeleteFavoriteProductError({ status })];
          }),
          catchError((error) => {
            console.error(error);
            return of(profileDeleteFavoriteProductError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
