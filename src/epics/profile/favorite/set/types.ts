// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/profile/favourite/types';

export type TProfileSetFavouriteEpic = Epic<ActionTypes, ActionTypes>;
