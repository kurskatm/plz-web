// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { PROFILE_SET_FAVOURITE_PRODUCTS } from '@constants/profile/favourite/products';
// Actions
import {
  profileSetFavoriteProductPending,
  profileSetFavoriteProductSuccess,
  profileSetFavoriteProductError,
  profileSetFavoriteProductSaveData,
} from '@actions/profile/favourite';
// Api
import { setFavoriteProduct } from '@/api/profile/favorite/products/set';
// Types
import { TProfileSetFavouriteEpic } from './types';

export const setProfileFavoriteProductsEpic: TProfileSetFavouriteEpic = (action$) =>
  action$.pipe(
    ofType(PROFILE_SET_FAVOURITE_PRODUCTS),
    switchMap(({ payload }) =>
      concat(
        of(profileSetFavoriteProductPending()),
        setFavoriteProduct(payload).pipe(
          switchMap((response) => {
            const { status } = response;

            if ([200, 204].includes(status)) {
              return [
                profileSetFavoriteProductSaveData(payload),
                profileSetFavoriteProductSuccess({ status }),
              ];
            }

            return [profileSetFavoriteProductError({ status })];
          }),
          catchError((error) => {
            console.error(error);
            return of(profileSetFavoriteProductError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
