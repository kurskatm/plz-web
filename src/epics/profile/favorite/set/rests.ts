// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
import { PROFILE_SET_FAVOURITE_REST } from '@constants/profile/favourite/rests';
// Actions
import {
  profileSetFavoriteRestsPending,
  profileSetFavoriteRestsSuccess,
  profileSetFavoriteRestsError,
  profileSetFavoriteRestsSaveData,
} from '@actions/profile/favourite';
// Api
import { setFavoriteRestaurant } from '@/api/profile/favorite/restaurants/set';
// Types
import { TProfileSetFavouriteEpic } from './types';

export const setProfileFavoriteRestsEpic: TProfileSetFavouriteEpic = (action$) =>
  action$.pipe(
    ofType(PROFILE_SET_FAVOURITE_REST),
    switchMap(({ payload }) =>
      concat(
        of(profileSetFavoriteRestsPending()),
        setFavoriteRestaurant(payload).pipe(
          switchMap((response) => {
            const { status } = response;

            if ([200, 204].includes(status)) {
              return [
                profileSetFavoriteRestsSaveData(payload),
                profileSetFavoriteRestsSuccess({ status }),
              ];
            }

            return [profileSetFavoriteRestsError({ status })];
          }),
          catchError((error) => {
            console.error(error);
            return of(profileSetFavoriteRestsError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
