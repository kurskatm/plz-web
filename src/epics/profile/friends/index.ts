// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { PROFILE_AUTHORIZED } from '@constants/profile/auth';
import { PROFILE_GET_FRIENDS } from '@constants/profile/friends';
// Actions
import {
  profileGetFriendsPending,
  profileGetFriendsSuccess,
  profileGetFriendsError,
  profileGetFriendsSaveData,
} from '@actions/profile/friends';
// Api
import { getProfileFriends } from '@/api/profile/friends/fetch';
// Types
import { TProfileFriendsEpic } from './types';

export const getProfileFriendsEpic: TProfileFriendsEpic = (action$) =>
  action$.pipe(
    ofType(PROFILE_GET_FRIENDS, PROFILE_AUTHORIZED),
    switchMap(({ payload }) =>
      concat(
        of(profileGetFriendsPending()),
        getProfileFriends(payload).pipe(
          switchMap((response) => {
            const { status } = response;

            if (status === 200) {
              const { response: data } = response;

              return [profileGetFriendsSaveData(data), profileGetFriendsSuccess({ status })];
            }

            return [profileGetFriendsError({ status })];
          }),
          catchError((error) => {
            console.error(error);
            return of(profileGetFriendsError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
