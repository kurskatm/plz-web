// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/profile/friends/types';

export type TProfileFriendsEpic = Epic<ActionTypes, ActionTypes>;
