// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/profile/new-address/types';

export type TGetNewAddressesEpic = Epic<ActionTypes, ActionTypes>;
