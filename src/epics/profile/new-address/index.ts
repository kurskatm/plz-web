// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { PROFILE_GET_NEW_ADDRESS } from '@constants/profile/new-address';
// Actions
import {
  fetchNewAddressesSuggestPending,
  fetchNewAddressesSuggestSuccess,
  fetchNewAddressesSuggestError,
  saveNewAddressesSuggest,
} from '@actions/profile/new-address';
// Api
import { getNewAddressSuggest } from '@api/address/new-address-suggest';
// Types
import { TGetNewAddressesEpic } from './types';

export const getNewAddressesEpic: TGetNewAddressesEpic = (action$) =>
  action$.pipe(
    ofType(PROFILE_GET_NEW_ADDRESS),
    switchMap(({ payload }) =>
      concat(
        of(fetchNewAddressesSuggestPending()),
        getNewAddressSuggest(payload).pipe(
          switchMap((response) => {
            const { status } = response;

            if (status === 200) {
              const data = response.response.items;

              return [
                saveNewAddressesSuggest({ data }),
                fetchNewAddressesSuggestSuccess({ status }),
              ];
            }

            return [fetchNewAddressesSuggestError({ status })];
          }),
          catchError((error) => {
            console.log(error);
            return of(fetchNewAddressesSuggestError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
