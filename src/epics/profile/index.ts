// Modules
import { combineEpics } from 'redux-observable';
// Epics
import { getProfileInfoEpic } from './info/get';
import { updateProfileInfoEpic } from './info/update';
import { deleteProfileFavoriteRestsEpic } from './favorite/delete/rests';
import { deleteProfileFavoriteProductsEpic } from './favorite/delete/products';
import { setProfileFavoriteRestsEpic } from './favorite/set/rests';
import { setProfileFavoriteProductsEpic } from './favorite/set/products';
import { getProfileReviewsEpic } from './reviews';
import { getProfileFriendsEpic } from './friends';
import { ordersEpic } from './orders';
import { getProfileFavoritesEpic } from './new-favorites';
import { scoresEpic } from './scores';
import { getNewAddressesEpic } from './new-address';
import { getNewGeocodeEpic } from './new-geocode';

export const profileEpic = combineEpics(
  getProfileInfoEpic,
  updateProfileInfoEpic,
  getProfileReviewsEpic,
  getProfileFriendsEpic,
  ordersEpic,
  deleteProfileFavoriteRestsEpic,
  deleteProfileFavoriteProductsEpic,
  setProfileFavoriteRestsEpic,
  setProfileFavoriteProductsEpic,
  getProfileFavoritesEpic,
  scoresEpic,
  getNewAddressesEpic,
  getNewGeocodeEpic,
);
