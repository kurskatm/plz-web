// Modules
import { combineEpics } from 'redux-observable';
// Epics
import { setProfileOrdersReviewEpic } from './set-orders-review';
import { getProfileLastOrderEpic } from './fetch-last-order-without-review';
import { getProfileUserOrderEpic } from './fetch-current-order';
import { profileOrderContactMeEpic } from './contact-me';
import { profileRepeatOrderEpic } from './repeat-order';
import { profileNewOrdersEpic } from './fetch-new-orders';

export const ordersEpic = combineEpics(
  setProfileOrdersReviewEpic,
  getProfileLastOrderEpic,
  getProfileUserOrderEpic,
  profileOrderContactMeEpic,
  profileRepeatOrderEpic,
  profileNewOrdersEpic,
);
