// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { PROFILE_ORDER_CONTACT_ME } from '@constants/profile/orders/contact-me';
// Actions
import {
  profileOrderContactMePending,
  profileOrderContactMeSuccess,
  profileOrderContactMeError,
} from '@actions/profile/contact-me';
// Api
import { profileOrderContactMe } from '@/api/profile/orders/contact-me';
// Types
import { TProfileOrderContactMeEpic } from './types';

export const profileOrderContactMeEpic: TProfileOrderContactMeEpic = (action$) =>
  action$.pipe(
    ofType(PROFILE_ORDER_CONTACT_ME),
    switchMap(({ payload }) =>
      concat(
        of(profileOrderContactMePending()),
        profileOrderContactMe(payload).pipe(
          switchMap((response) => {
            const { status } = response;
            if (status === 204) {
              return [
                profileOrderContactMeSuccess({
                  status,
                  orderId: payload.orderId,
                }),
              ];
            }

            return [
              profileOrderContactMeError({
                status,
                orderId: payload.orderId,
              }),
            ];
          }),
          catchError((error) => {
            console.error(error);
            return of(
              profileOrderContactMeError({
                status: error.status,
                orderId: payload.orderId,
                details: {
                  retryAfter: error?.response?.retryAfter,
                  errorMessage: error?.response?.detail,
                },
              }),
            );
          }),
        ),
      ),
    ),
  );
