// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/profile/contact-me/types';

export type TProfileOrderContactMeEpic = Epic<ActionTypes, ActionTypes>;
