// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { PROFILE_AUTHORIZED } from '@constants/profile/auth';
import { PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW } from '@constants/profile/orders';
// Action
import {
  profileGetLastOrderWithoutReviewPending,
  profileGetLastOrderWithoutReviewSuccess,
  profileGetLastOrderWithoutReviewError,
  profileGetLastOrderWithoutReviewSaveData,
} from '@actions/profile/orders';
// API
import { getProfileLastOrderWithoutReview } from '@api/profile/orders/fetch-last-order-without-review';
// Types
import { TProfileLastOrderEpic } from './types';

export const getProfileLastOrderEpic: TProfileLastOrderEpic = (action$) =>
  action$.pipe(
    ofType(PROFILE_GET_LAST_ORDER_WIHTOUT_REVIEW, PROFILE_AUTHORIZED),
    switchMap(({ payload }) =>
      concat(
        of(profileGetLastOrderWithoutReviewPending()),
        getProfileLastOrderWithoutReview(payload).pipe(
          switchMap((response) => {
            const { status } = response;

            if (status === 200) {
              const data = response.response;

              return [
                profileGetLastOrderWithoutReviewSuccess({ status }),
                profileGetLastOrderWithoutReviewSaveData({ order: data }),
              ];
            }

            return [profileGetLastOrderWithoutReviewError({ status })];
          }),
          catchError((error) => {
            console.log(error);
            return of(profileGetLastOrderWithoutReviewError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
