// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { PROFILE_SET_ORDER_REVIEW } from '@constants/profile/reviews';
// Actions
import {
  profileSetOrderReviewPending,
  profileSetOrderReviewSuccess,
  profileSetOrderReviewError,
  profileSaveOrderReviewData,
  profileGetReviewsResetData,
} from '@actions/profile/reviews';
import { bonusResetData } from '@actions/bonus';
// Api
import { setProfileOrderReview } from '@/api/profile/orders/set-review';
// Types
import { TProfileSetOrdersReviewEpic } from './types';

export const setProfileOrdersReviewEpic: TProfileSetOrdersReviewEpic = (action$) =>
  action$.pipe(
    ofType(PROFILE_SET_ORDER_REVIEW),
    switchMap(({ payload }) =>
      concat(
        of(profileSetOrderReviewPending()),
        setProfileOrderReview(payload).pipe(
          switchMap((response) => {
            const { status } = response;

            if ([200, 204].includes(status)) {
              return [
                profileSaveOrderReviewData({
                  orderId: payload.review.orderId,
                }),
                profileSetOrderReviewSuccess({ status }),
                bonusResetData(),
                profileGetReviewsResetData(),
              ];
            }

            return [profileSetOrderReviewError({ status })];
          }),
          catchError((error) => {
            console.error(error);
            return of(profileSetOrderReviewError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
