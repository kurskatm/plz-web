// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/profile/new-orders/types';
// Main types
import { TOrderItem } from '@type/new-orders';
import { TUserOrdersData } from '@type/profile';

export type TProfileNewOrdersEpic = Epic<ActionTypes, ActionTypes>;

export type TGetValidOrdersData = (data: TOrderItem[]) => TUserOrdersData;
