// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { PROFILE_GET_NEW_ORDERS } from '@constants/profile/orders/new-orders';
import { PROFILE_AUTHORIZED } from '@constants/profile/auth';
import { CHECKOUT_SEND_SUCCESS } from '@constants/checkout';
// Actions
import {
  profileGetNewOrdersPending,
  profileGetNewOrdersSuccess,
  profileGetNewOrdersError,
  profileGetNewOrdersSave,
} from '@actions/profile/new-orders';
// Api
import { getProfileNewOrders } from '@api/profile/new-orders/fetch';
// Config
import { getValidOrders } from './config';
// Types
import { TProfileNewOrdersEpic } from './types';

export const profileNewOrdersEpic: TProfileNewOrdersEpic = (action$) =>
  action$.pipe(
    ofType(PROFILE_GET_NEW_ORDERS, PROFILE_AUTHORIZED, CHECKOUT_SEND_SUCCESS),
    switchMap(({ payload }) =>
      concat(
        of(profileGetNewOrdersPending()),
        getProfileNewOrders(payload).pipe(
          switchMap((response) => {
            const { status } = response;

            if (status === 200) {
              const data = getValidOrders(response.response.items);

              return [
                profileGetNewOrdersSave({ data, after: response.response.after }),
                profileGetNewOrdersSuccess({ status }),
              ];
            }

            return [profileGetNewOrdersError({ status })];
          }),
          catchError((error) => {
            console.error(error);
            return of(profileGetNewOrdersError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
