/* eslint-disable @typescript-eslint/indent */
// Types
import { TGetValidOrdersData } from './types';

export const getValidOrders: TGetValidOrdersData = (newOrders) =>
  newOrders.map((order) => ({
    promoCode: order.promoCode?.orderId.length
      ? {
          orderId: order.promoCode?.orderId,
          promoCodeName: order.promoCode?.name,
          promoCodeCoefficient: order.promoCode?.coefficient,
        }
      : null,
    restaurant: {
      id: order.restaurant.id,
      restaurantName: order.restaurant.name,
      restaurantLogo: order.restaurant.logo,
      urlName: order.restaurant.urlName,
      cityUrlName: order.restaurant.cityUrlName,
      phoneNumber: order.restaurant.phoneNumber,
    },
    order: {
      id: order.order.id,
      restaurant: {
        restaurantId: order.restaurant.id,
        name: order.restaurant.name,
        logo: order.restaurant.logo,
        storeId: order.order.storeId,
      },
      customerId: order.order.customerId,
      orderCost: {
        total: order.order.totalCost,
        delivery: order.order.deliveryCost,
      },
      bonusBalance: {
        spentAmount: order.order.bonusSpent,
        receivedAmount: order.order.bonusReceived,
      },
      isContactCustomerAvailable: order.order.isContactCustomerAvailable,
      status: order.order.status,
      addedOn: order.order.addedOn,
      deliveryDetails: order.order.deliveryDetails,
      paymentType: order.order.paymentType,
      promoCodeUsageId: order.order.promoCodeUsageId,
      reviewId: order.order.reviewId,
      products: order.order.products.map((product) => ({
        id: product.id,
        name: product.name,
        quantity: product.quantity,
        price: product.price,
        priceWithModifiers: product.priceWithModifiers,
        paidByBonusPoints: product.paidByBonusPoints,
        externalId: product.externalId,
        modifiers: {
          modifiersGroups: product.modifiers.modifiersGroups,
        },
      })),
    },
  }));
