// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/profile/repeat-order/types';
// Types
import { TShopingCartModel } from '@/models/shoping-cart/types';
import { TOrderToCart } from '@type/orderFromProfileToCart';

export type TProfileRepeatOrderEpic = Epic<ActionTypes, ActionTypes>;

export interface TGetShopingCartValidDataArgs {
  shopingCart: TShopingCartModel;
  order: TOrderToCart;
  cityId: string;
}

export type TGetShopingCartValidData = (data: TGetShopingCartValidDataArgs) => TShopingCartModel;
