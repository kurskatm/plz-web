// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { PROFILE_ORDER_REPEAT_ORDER } from '@constants/profile/orders/repeat-order';
// Actions
import {
  profileOrderRepeatOrderPending,
  profileOrderRepeatOrderSuccess,
  profileOrderRepeatOrderError,
  profileOrderRepeatOrderSave,
} from '@actions/profile/repeat-order';
import { shopingCartUpdate } from '@actions/shoping-cart/change';
// Api
import { profileRepeatOrder } from '@/api/profile/orders/repeat-order';
// Config
import { getShopingCartValidData } from './config';
// Types
import { TProfileRepeatOrderEpic } from './types';

export const profileRepeatOrderEpic: TProfileRepeatOrderEpic = (action$) =>
  action$.pipe(
    ofType(PROFILE_ORDER_REPEAT_ORDER),
    switchMap(({ payload }) =>
      concat(
        of(profileOrderRepeatOrderPending()),
        profileRepeatOrder(payload).pipe(
          switchMap((response) => {
            const { status } = response;

            if (status === 200) {
              const data = response.response;

              if (data.products.length) {
                const shopingCartData = getShopingCartValidData({
                  order: data,
                  cityId: payload.cityId,
                  shopingCart: payload.shopingCart,
                });

                return [
                  shopingCartUpdate(shopingCartData),
                  profileOrderRepeatOrderSuccess({ status }),
                  profileOrderRepeatOrderSave(data),
                ];
              }

              if (!data.products.length) {
                return [
                  profileOrderRepeatOrderError({ status }),
                  profileOrderRepeatOrderSave(data),
                ];
              }

              return [
                profileOrderRepeatOrderSuccess({ status }),
                profileOrderRepeatOrderSave(data),
              ];
            }

            return [profileOrderRepeatOrderError({ status })];
          }),
          catchError((error) => {
            console.error(error);
            return of(profileOrderRepeatOrderError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
