/* eslint-disable @typescript-eslint/indent */
// Lib
import { UpdateShopingCartFromProfileManager } from '@lib/shoping-cart/update-cart-from-profile';
// Types
import { TGetShopingCartValidData } from './types';

export const getShopingCartValidData: TGetShopingCartValidData = ({
  order,
  shopingCart,
  cityId,
}) => {
  const data = {
    cityId,
    restaurant: {
      id: order.restaurant.id,
      name: order.restaurant.name,
      logoUrl: order.restaurant.logoUrl,
    },
    products: order.products.map((product) => ({
      id: product.id,
      name: product.name,
      description: product.description,
      price: product.price,
      inBonusPoint: product.inBonusPoint,
      count: product.count,
      modifiers: product.selectedModifierGroups.map((group) => ({
        id: group.id,
        name: group.name,
        modifiers: group.selectedModifiers,
      })),
    })),
    options: {
      total: order.paymentDetails.total,
    },
    shopingCart,
  };

  const validData = new UpdateShopingCartFromProfileManager(data).init();
  return validData;
};
