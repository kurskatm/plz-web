/* eslint-disable @typescript-eslint/indent */
// Types
import { TGetValidOrderData } from './types';

export const getValidOrder: TGetValidOrderData = (newOrders) => ({
  promoCode: newOrders.promoCode?.orderId.length
    ? {
        orderId: newOrders.promoCode?.orderId,
        promoCodeName: newOrders.promoCode?.name,
        promoCodeCoefficient: newOrders.promoCode?.coefficient,
      }
    : null,
  restaurant: {
    id: newOrders.restaurant.id,
    restaurantName: newOrders.restaurant.name,
    restaurantLogo: newOrders.restaurant.logo,
    urlName: newOrders.restaurant.urlName,
    cityUrlName: newOrders.restaurant.cityUrlName,
    phoneNumber: newOrders.restaurant.phoneNumber,
  },
  order: {
    id: newOrders.order.id,
    restaurant: {
      restaurantId: newOrders.restaurant.id,
      name: newOrders.restaurant.name,
      logo: newOrders.restaurant.logo,
      storeId: newOrders.order.storeId,
    },
    customerId: newOrders.order.customerId,
    orderCost: {
      total: newOrders.order.totalCost,
      delivery: newOrders.order.deliveryCost,
    },
    bonusBalance: {
      spentAmount: newOrders.order.bonusSpent,
      receivedAmount: newOrders.order.bonusReceived,
    },
    isContactCustomerAvailable: newOrders.order.isContactCustomerAvailable,
    status: newOrders.order.status,
    addedOn: newOrders.order.addedOn,
    deliveryDetails: newOrders.order.deliveryDetails,
    paymentType: newOrders.order.paymentType,
    promoCodeUsageId: newOrders.order.promoCodeUsageId,
    reviewId: newOrders.order.reviewId,
    products: newOrders.order.products.map((product) => ({
      id: product.id,
      name: product.name,
      quantity: product.quantity,
      price: product.price,
      priceWithModifiers: product.priceWithModifiers,
      paidByBonusPoints: product.paidByBonusPoints,
      externalId: product.externalId,
      modifiers: {
        modifiersGroups: product.modifiers.modifiersGroups,
      },
    })),
  },
});
