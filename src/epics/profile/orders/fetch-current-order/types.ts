// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/profile/orders/types';
// Main types
import { TOrderItem } from '@type/new-orders';
import { TUserOrder } from '@type/profile';

export type TProfileUserOrderEpic = Epic<ActionTypes, ActionTypes>;

export type TGetValidOrderData = (data: TOrderItem) => TUserOrder;
