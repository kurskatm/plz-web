// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { PROFILE_GET_USER_ORDER } from '@constants/profile/orders';
// Action
import {
  profileGetUserOrderPending,
  profileGetUserOrderSuccess,
  profileGetUserOrderError,
  profileGetUserOrderSaveData,
} from '@actions/profile/orders';
// API
import { getProfileUserOrder } from '@api/profile/orders/fetch-current-order';
// Config
import { getValidOrder } from './config';
// Types
import { TProfileUserOrderEpic } from './types';

export const getProfileUserOrderEpic: TProfileUserOrderEpic = (action$) =>
  action$.pipe(
    ofType(PROFILE_GET_USER_ORDER),
    switchMap(({ payload }) =>
      concat(
        of(profileGetUserOrderPending()),
        getProfileUserOrder(payload).pipe(
          switchMap((response) => {
            const { status } = response;

            if (status === 200) {
              const data = getValidOrder(response.response);

              return [
                profileGetUserOrderSuccess({ status }),
                profileGetUserOrderSaveData({ order: data }),
              ];
            }

            return [profileGetUserOrderError({ status })];
          }),
          catchError((error) => {
            console.log(error);
            return of(profileGetUserOrderError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
