// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/profile/orders/types';

export type TProfileOrdersEpic = Epic<ActionTypes, ActionTypes>;
