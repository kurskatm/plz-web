// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { PROFILE_AUTHORIZED } from '@constants/profile/auth';
import { PROFILE_GET_ORDERS } from '@constants/profile/orders';
import { CHECKOUT_SEND_SUCCESS } from '@constants/checkout';
// Actions
import {
  profileGetOrdersPending,
  profileGetOrdersSuccess,
  profileGetOrdersError,
  profileGetOrdersSaveData,
} from '@actions/profile/orders';
// Api
import { getProfileOrders } from '@/api/profile/orders/fetch';
// Types
import { TProfileOrdersEpic } from './types';

export const getProfileOrdersEpic: TProfileOrdersEpic = (action$) =>
  action$.pipe(
    ofType(PROFILE_GET_ORDERS, PROFILE_AUTHORIZED, CHECKOUT_SEND_SUCCESS),
    switchMap(({ payload }) =>
      concat(
        of(profileGetOrdersPending()),
        getProfileOrders(payload).pipe(
          switchMap((response) => {
            const { status } = response;

            if (status === 200) {
              const data = response.response.orders;

              return [profileGetOrdersSaveData(data), profileGetOrdersSuccess({ status })];
            }

            return [profileGetOrdersError({ status })];
          }),
          catchError((error) => {
            console.error(error);
            return of(profileGetOrdersError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
