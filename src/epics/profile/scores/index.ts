// Modules
import { combineEpics } from 'redux-observable';
// Epics
import { profileSetSocialActivityEpic } from './set-social-activity';

export const scoresEpic = combineEpics(profileSetSocialActivityEpic);
