// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/profile/social-activity/types';

export type TProfileSetSocialActivityEpic = Epic<ActionTypes, ActionTypes>;
