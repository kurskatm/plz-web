// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { PROFILE_SET_SOCIAL_ACTIVITY } from '@constants/profile/social-activity';
// Actions
import {
  profileSetSocialActivityPending,
  profileSetSocialActivitySuccess,
  profileSetSocialActivityError,
  profileSetSocialActivitySaveData,
} from '@actions/profile/social-activity';
import { bonusFetch, bonusHistoryFetch } from '@actions/bonus';
// Api
import { setProfileSocialActivity } from '@/api/profile/scores/social-activity';
// Types
import { TProfileSetSocialActivityEpic } from './types';

export const profileSetSocialActivityEpic: TProfileSetSocialActivityEpic = (action$) =>
  action$.pipe(
    ofType(PROFILE_SET_SOCIAL_ACTIVITY),
    switchMap(({ payload }) =>
      concat(
        of(profileSetSocialActivityPending()),
        setProfileSocialActivity(payload).pipe(
          switchMap((response) => {
            const { status } = response;

            if (status === 204) {
              return [
                profileSetSocialActivitySuccess({ status }),
                profileSetSocialActivitySaveData(payload),
                bonusFetch({ access_token: payload.access_token }),
                bonusHistoryFetch({ access_token: payload.access_token }),
              ];
            }

            return [profileSetSocialActivityError({ status })];
          }),
          catchError((error) => {
            console.error(error);
            return of(profileSetSocialActivityError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
