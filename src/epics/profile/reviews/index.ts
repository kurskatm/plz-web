// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { PROFILE_AUTHORIZED } from '@constants/profile/auth';
import { PROFILE_GET_REVIEWS } from '@constants/profile/reviews';
// Actions
import {
  profileGetReviewsPending,
  profileGetReviewsSuccess,
  profileGetReviewsError,
  profileGetReviewsSaveData,
} from '@actions/profile/reviews';
// Api
import { getProfileReviews } from '@/api/profile/reviews/fetch';
// Types
import { TProfileReviewsEpic } from './types';

export const getProfileReviewsEpic: TProfileReviewsEpic = (action$) =>
  action$.pipe(
    ofType(PROFILE_GET_REVIEWS, PROFILE_AUTHORIZED),
    switchMap(({ payload }) =>
      concat(
        of(profileGetReviewsPending()),
        getProfileReviews(payload).pipe(
          switchMap((response) => {
            const { status } = response;

            if (status === 200) {
              const { response: data } = response;

              return [
                profileGetReviewsSaveData({
                  data: data.reviews,
                  length: data.count,
                }),
                profileGetReviewsSuccess({ status }),
              ];
            }

            return [profileGetReviewsError({ status })];
          }),
          catchError((error) => {
            console.error(error);
            return of(profileGetReviewsError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
