// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/profile/reviews/types';

export type TProfileReviewsEpic = Epic<ActionTypes, ActionTypes>;
