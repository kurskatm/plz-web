// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/profile/new-geocode/types';

export type TGetNewGeocodeEpic = Epic<ActionTypes, ActionTypes>;
