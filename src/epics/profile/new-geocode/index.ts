// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { PROFILE_GET_NEW_ADDRESSES_GEOCODE } from '@constants/profile/new-geocode';
// Actions
import {
  // Address
  updateProfileNewAddress,
} from '@actions/profile/new-address';
import {
  profileGetNewGeocodePending,
  profileGetNewGeocodeSuccess,
  profileGetNewGeocodeError,
  profileSaveNewGeocode,
} from '@actions/profile/new-geocode';
// Api
import { getNewAddressGeoCode } from '@api/address/new-geo-code';
// Lib
import { setAddress, setGeoCode } from '@lib/cookie';
// Types
import { TGetNewGeocodeEpic } from './types';

export const getNewGeocodeEpic: TGetNewGeocodeEpic = (action$) =>
  action$.pipe(
    ofType(PROFILE_GET_NEW_ADDRESSES_GEOCODE),
    switchMap(({ payload }) =>
      concat(
        of(profileGetNewGeocodePending()),
        getNewAddressGeoCode(payload).pipe(
          switchMap((response) => {
            const { status } = response;

            if (status === 200) {
              const { response: data } = response;

              if (data.precision === 'exact') {
                // возможно лучше хранить не description, а name
                setAddress(payload.description.replace('Россия, ', ''));
                setGeoCode(data);

                return [
                  updateProfileNewAddress(payload),
                  profileSaveNewGeocode(data),
                  profileGetNewGeocodeSuccess({ status }),
                ];
              }
              return [profileSaveNewGeocode(data), profileGetNewGeocodeSuccess({ status })];
            }

            return [profileGetNewGeocodeError({ status })];
          }),
          catchError((error) => {
            console.log(error);
            return of(profileGetNewGeocodeError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
