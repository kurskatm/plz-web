// Modules
import { combineEpics } from 'redux-observable';
// Epics
import { checkoutSendEpic } from './send';

export const checkoutEpic = combineEpics(checkoutSendEpic);
