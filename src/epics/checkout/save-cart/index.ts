// Modules
import { combineLatest, concat, of, timer } from 'rxjs';
import { catchError, debounceTime, map, mergeMap, switchMap, takeUntil } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { SHOPING_CART_RESET, SHOPING_CART_SAVE } from '@constants/shoping-cart';
// Actions
import {
  shopingCartSavePending,
  shopingCartSaveSuccess,
  shopingCartSaveError,
} from '@actions/shoping-cart/save';
// Api
import { saveShopingCart } from '@api/shoping-cart/save';
// Utils
import { Consts } from '@utils';
// Types
import { TShopingCartSaveEpic } from './types';

const { MIN_LOADING_MS, REQUEST_MS } = Consts.DEBOUNCE;

export const saveShopingCartEpic: TShopingCartSaveEpic = (action$) =>
  action$.pipe(
    ofType(SHOPING_CART_SAVE),
    debounceTime(REQUEST_MS),
    mergeMap(({ payload }) =>
      concat(
        of(shopingCartSavePending()),
        combineLatest([timer(MIN_LOADING_MS), saveShopingCart(payload)]).pipe(
          map((x) => x[1]),
          switchMap((response) => {
            const { status, response: data } = response;

            if ([200].includes(status)) {
              return [shopingCartSaveSuccess({ status, data })];
            }

            return [shopingCartSaveError({ status })];
          }),
          catchError((error) => {
            const { status } = error;
            console.log(error);

            return of(shopingCartSaveError({ status }));
          }),
        ),
      ).pipe(takeUntil(action$.pipe(ofType(SHOPING_CART_RESET)))),
    ),
  );
