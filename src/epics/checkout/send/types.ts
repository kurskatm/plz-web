// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/checkout/types';

export type TCheckoutSendEpic = Epic<ActionTypes, ActionTypes>;
