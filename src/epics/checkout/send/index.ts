// Modules
import { combineLatest, concat, of, timer } from 'rxjs';
import { catchError, debounceTime, map, mergeMap, switchMap, takeUntil } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { SHOPING_CART_RESET } from '@constants/shoping-cart';
import { CHECKOUT_SEND } from '@constants/checkout';
// Actions
import {
  checkoutSendPending,
  checkoutSendSuccess,
  checkoutSendError,
  // checkoutAddOrderId,
} from '@actions/checkout';
// Api
import { sendCheckout } from '@api/checkout';
// Utils
import { Consts } from '@utils';
// Types
import { TCheckoutSendEpic } from './types';

const { MIN_LOADING_MS, REQUEST_MS } = Consts.DEBOUNCE;

export const checkoutSendEpic: TCheckoutSendEpic = (action$) =>
  action$.pipe(
    ofType(CHECKOUT_SEND),
    debounceTime(REQUEST_MS),
    mergeMap(({ payload }) =>
      concat(
        of(checkoutSendPending()),
        combineLatest([timer(MIN_LOADING_MS), sendCheckout(payload)]).pipe(
          map((x) => x[1]),
          switchMap((response) => {
            const { status, response: data } = response;

            if ([200].includes(status)) {
              return [
                checkoutSendSuccess({
                  status,
                  id: data?.orderId,
                  paymentResult: data?.paymentResult,
                  // @ts-ignore
                  validationResult: data?.validationResult,
                }),
              ];
            }

            return [checkoutSendError({ status })];
          }),
          catchError((error) => {
            const { status } = error;
            console.log(error);

            return of(checkoutSendError({ status }));
          }),
        ),
      ).pipe(takeUntil(action$.pipe(ofType(SHOPING_CART_RESET)))),
    ),
  );
