// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { CONTACTS_FETCH } from '@constants/contacts';
// Actions
import {
  contactsSave,
  contactsSetPending,
  contactsSetSuccess,
  contactsSetError,
} from '@actions/contacts';
// Api
import { getContacts } from '@api/contacts/fetch';
// Types
import { TFetchContactsEpic } from './types';

export const fetchContactsEpic: TFetchContactsEpic = (action$) =>
  action$.pipe(
    ofType(CONTACTS_FETCH),
    switchMap(({ payload }) =>
      concat(
        of(contactsSetPending()),
        getContacts(payload).pipe(
          switchMap((response) => {
            const { status } = response;

            if (status === 200) {
              const { response: data } = response;
              return [contactsSave(data), contactsSetSuccess({ status })];
            }

            return [contactsSetError({ status })];
          }),
          catchError(({ status }) => of(contactsSetError({ status }))),
        ),
      ),
    ),
  );
