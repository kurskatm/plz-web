// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/contacts/types';

export type TFetchContactsEpic = Epic<ActionTypes, ActionTypes>;
