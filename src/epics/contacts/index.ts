// Modules
import { combineEpics } from 'redux-observable';
// Epics
import { fetchContactsEpic } from './fetch';

export const contactsEpic = combineEpics(fetchContactsEpic);
