// Modules
import { combineEpics } from 'redux-observable';
// Epics
import { fetchFiltersEpic } from './fetch';

export const filtersEpic = combineEpics(fetchFiltersEpic);
