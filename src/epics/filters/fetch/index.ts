// Modules
import { combineLatest, concat, of, timer } from 'rxjs';
import { catchError, debounceTime, map, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { FILTERS_FETCH } from '@constants/filters';
// Actions
import {
  filtersSave,
  filtersFetchPending,
  filtersFetchSuccess,
  filtersFetchError,
} from '@actions/filters';
// Api
import { getFilters } from '@api/filters/fetch';
// Utils
import { Consts } from '@utils';
// Types
import { TFetchFiltersEpic } from './types';

const { MIN_LOADING_MS, REQUEST_MS } = Consts.DEBOUNCE;

export const fetchFiltersEpic: TFetchFiltersEpic = (action$) =>
  action$.pipe(
    ofType(FILTERS_FETCH),
    debounceTime(REQUEST_MS),
    switchMap(({ payload }) =>
      concat(
        of(filtersFetchPending()),
        combineLatest([timer(MIN_LOADING_MS), getFilters(payload)]).pipe(
          map((x) => x[1]),
          switchMap((response) => {
            const { status } = response;

            if ([200].includes(status)) {
              const data = response.response;
              return [filtersSave(data), filtersFetchSuccess({ status })];
            }

            return [filtersFetchError({ status })];
          }),
          catchError((error) => {
            const { status } = error;
            console.log(error);

            return of(filtersFetchError({ status }));
          }),
        ),
      ),
    ),
  );
