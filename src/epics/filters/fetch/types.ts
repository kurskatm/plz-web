// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/filters/types';

export type TFetchFiltersEpic = Epic<ActionTypes, ActionTypes>;
