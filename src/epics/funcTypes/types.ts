// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/cities/types';

export type TFetchCitiesEpic = Epic<ActionTypes, ActionTypes>;
