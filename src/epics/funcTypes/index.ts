// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { F_TYPES_FETCH } from '@constants/funcTypes';
// Actions
import {
  fTypesFetchPending,
  fTypesFetchSuccess,
  fTypesFetchError,
  fTypesFetchSaveData,
} from '@actions/funcTypes';
// Api
import { getFuncTypes } from '@api/funcTypes';
// Types
import { TFetchCitiesEpic } from './types';

export const fTypesEpic: TFetchCitiesEpic = (action$) =>
  action$.pipe(
    ofType(F_TYPES_FETCH),
    switchMap(() =>
      concat(
        of(fTypesFetchPending()),
        getFuncTypes().pipe(
          switchMap((response) => {
            const { status } = response;

            if (status === 200) {
              const { response: data } = response;
              return [fTypesFetchSaveData(data), fTypesFetchSuccess({ status })];
            }

            return [fTypesFetchError({ status })];
          }),
          catchError(({ status }) => of(fTypesFetchError({ status }))),
        ),
      ),
    ),
  );
