// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { REVIEWS_FETCH } from '@constants/reviews';
// Actions
import { reviewsSave, reviewsPending, reviewsSuccess, reviewsError } from '@actions/reviews';
// Api
import { fetchReviews } from '@api/reviews';
// Config
import { saveLimitedData } from './config';
// Types
import { TReviewsEpic } from './types';

export const fetchReviewsEpic: TReviewsEpic = (action$) =>
  action$.pipe(
    ofType(REVIEWS_FETCH),
    switchMap(({ payload }) =>
      concat(
        of(reviewsPending()),
        fetchReviews(payload).pipe(
          switchMap((response) => {
            const { status } = response;

            if (status === 200) {
              const { response: data } = response;

              return [reviewsSuccess({ status }), reviewsSave(saveLimitedData(data))];
            }

            return [reviewsError({ status })];
          }),
          catchError((error) => {
            console.error(error);
            return of(reviewsError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
