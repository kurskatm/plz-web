// Types
import { TSaveLimitedData } from './types';

export const saveLimitedData: TSaveLimitedData = (data: []) => {
  if (data.length > 6) {
    return data.slice(0, 6);
  }
  return data;
};
