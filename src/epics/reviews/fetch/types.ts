// Main Types
import { TReviewsMainList } from '@type/reviews-main';
// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/reviews/types';

export type TReviewsEpic = Epic<ActionTypes, ActionTypes>;

export type TSaveLimitedData = (data: TReviewsMainList) => TReviewsMainList;
