// Modules
import { combineEpics } from 'redux-observable';
// Epics
import { fetchReviewsEpic } from './fetch';

export const reviewsEpic = combineEpics(fetchReviewsEpic);
