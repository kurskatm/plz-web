// Modules
import { combineEpics } from 'redux-observable';
// Epics
import { citiesEpic } from './cities';
import { contactsEpic } from './contacts';
import { cuisinesEpic } from './cuisines';
import { profileEpic } from './profile';
import { partnersEpic } from './partners';
import { restaurantsEpic } from './restaurants';
import { fetchRestaurantsAndHitsEpic } from './restaurants-and-hits/fetch';
import { authEpic } from './auth';
import { hotOffersEpic } from './hotOffers';
import { hitsEpic } from './hits';
import { supportEpic } from './support';
import { restaurantEpic } from './restaurant';
import { filtersEpic } from './filters';
import { shopingCartEpic } from './shoping-cart';
import { bonusEpic } from './bonus';
import { reviewsEpic } from './reviews';
import { checkoutEpic } from './checkout';
import { promocodeEpic } from './promocode';
import { foodForPointsEpic } from './foodForPoints';
import { fTypesEpic } from './funcTypes';

export const allEpics = combineEpics(
  citiesEpic,
  contactsEpic,
  cuisinesEpic,
  profileEpic,
  partnersEpic,
  restaurantsEpic,
  fetchRestaurantsAndHitsEpic,
  authEpic,
  hotOffersEpic,
  hitsEpic,
  supportEpic,
  restaurantEpic,
  filtersEpic,
  shopingCartEpic,
  bonusEpic,
  reviewsEpic,
  checkoutEpic,
  promocodeEpic,
  foodForPointsEpic,
  fTypesEpic,
);
