// Modules
import { combineEpics } from 'redux-observable';
// Epics
import { fetchRestaurantsEpic } from './fetch';

export const restaurantsEpic = combineEpics(fetchRestaurantsEpic);
