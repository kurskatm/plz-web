// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/restaurants/types';

export type TRestaurantsEpic = Epic<ActionTypes, ActionTypes>;
