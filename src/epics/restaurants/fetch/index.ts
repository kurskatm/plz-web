// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { RESTAURANTS_FETCH } from '@constants/restaurants';
// Actions
import {
  restaurantsSave,
  restaurantsPending,
  restaurantsSuccess,
  restaurantsError,
} from '@actions/restaurants';
// Api
import { getRestaurants } from '@/api/restaurants/fetch';
// Types
import { TRestaurantsEpic } from './types';

export const fetchRestaurantsEpic: TRestaurantsEpic = (action$) =>
  action$.pipe(
    ofType(RESTAURANTS_FETCH),
    switchMap(({ payload }) =>
      concat(
        of(restaurantsPending()),
        getRestaurants(payload).pipe(
          switchMap((response) => {
            const { status } = response;

            if (status === 200) {
              const { response: data } = response;

              return [restaurantsSuccess({ status }), restaurantsSave(data)];
            }

            return [restaurantsError({ status })];
          }),
          catchError((error) => {
            console.error(error);
            return of(restaurantsError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
