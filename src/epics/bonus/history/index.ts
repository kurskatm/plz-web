// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { BONUS_HISTORY_FETCH } from '@constants/bonus';
import { PROFILE_AUTHORIZED } from '@constants/profile';

// Actions
import {
  bonusHistorySave,
  bonusHistoryPending,
  bonusHistorySuccess,
  bonusHistoryError,
} from '@actions/bonus';
// Api
import { getBonusHistory } from '@/api/profile/bonus/history';
// Types
import { TBonusEpic } from './types';

export const fetchBonusHistoryEpic: TBonusEpic = (action$) =>
  action$.pipe(
    ofType(BONUS_HISTORY_FETCH, PROFILE_AUTHORIZED),
    switchMap(({ payload }) =>
      concat(
        of(bonusHistoryPending()),
        getBonusHistory(payload).pipe(
          switchMap((response) => {
            const { status } = response;

            if (status === 200) {
              const { response: data } = response;

              return [bonusHistorySuccess({ status }), bonusHistorySave(data)];
            }

            return [bonusHistoryError({ status })];
          }),
          catchError((error) => {
            console.error(error);
            return of(bonusHistoryError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
