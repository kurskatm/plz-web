// Modules
import { combineEpics } from 'redux-observable';
// Epics
import { fetchBonusEpic } from './balance';
import { fetchBonusHistoryEpic } from './history';

export const bonusEpic = combineEpics(fetchBonusEpic, fetchBonusHistoryEpic);
