// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { BONUS_FETCH } from '@constants/bonus';
import { PROFILE_AUTHORIZED } from '@constants/profile';
// Actions
import { bonusSave, bonusPending, bonusSuccess, bonusError } from '@actions/bonus';
// Api
import { getBonus } from '@/api/profile/bonus/balance';
// Types
import { TBonusEpic } from './types';

export const fetchBonusEpic: TBonusEpic = (action$) =>
  action$.pipe(
    ofType(BONUS_FETCH, PROFILE_AUTHORIZED),
    switchMap(({ payload }) =>
      concat(
        of(bonusPending()),
        getBonus(payload).pipe(
          switchMap((response) => {
            const { status } = response;

            if (status === 200) {
              const { response: data } = response;

              return [bonusSuccess({ status }), bonusSave(data)];
            }

            return [bonusError({ status })];
          }),
          catchError((error) => {
            console.error(error);
            return of(bonusError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
