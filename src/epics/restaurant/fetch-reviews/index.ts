// Modules
import { combineLatest, concat, of, timer } from 'rxjs';
import { catchError, debounceTime, map, mergeMap, switchMap, takeUntil } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { RESTAURANT_CARD_REVIEWS_FETCH } from '@constants/restaurant/reviews/current';
import { RESTAURANT_CARD_REVIEWS_MAIN_RESET } from '@constants/restaurant/reviews';
// Actions
import {
  restaurantReviewsSave,
  restaurantReviewsFetchPending,
  restaurantReviewsFetchSuccess,
  restaurantReviewsFetchError,
} from '@actions/restaurant/reviews/current';
// Api
import { fetchRestaurantReviews } from '@api/restaurant/fetch-reviews';
// Utils
import { Consts } from '@utils';
// Types
import { TFetchRestaurantReviewsEpic } from './types';

const { MIN_LOADING_MS, REQUEST_MS } = Consts.DEBOUNCE;

export const fetchRestaurantReviewsEpic: TFetchRestaurantReviewsEpic = (action$) =>
  action$.pipe(
    ofType(RESTAURANT_CARD_REVIEWS_FETCH),
    debounceTime(REQUEST_MS),
    mergeMap(({ payload }) =>
      concat(
        of(restaurantReviewsFetchPending()),
        combineLatest([timer(MIN_LOADING_MS), fetchRestaurantReviews(payload)]).pipe(
          map((x) => x[1]),
          switchMap((response) => {
            const { status } = response;

            if ([200, 204].includes(status)) {
              const data = response.response;
              return [restaurantReviewsSave(data), restaurantReviewsFetchSuccess({ status })];
            }

            return [restaurantReviewsFetchError({ status })];
          }),
          catchError((error) => {
            const { status } = error;
            console.log(error);

            return of(restaurantReviewsFetchError({ status }));
          }),
        ),
      ).pipe(takeUntil(action$.pipe(ofType(RESTAURANT_CARD_REVIEWS_MAIN_RESET)))),
    ),
  );
