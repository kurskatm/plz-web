// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/restaurant/reviews/current/types';

export type TFetchRestaurantReviewsEpic = Epic<ActionTypes, ActionTypes>;
