// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/restaurant/info/geocode-suggest/types';

export type TFetchRestaurantGeocodeSuggestEpic = Epic<ActionTypes, ActionTypes>;
