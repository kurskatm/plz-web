// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { RESTAURANT_GEOCODE_SUGGEST_FETCH } from '@constants/restaurant/info/geocode-suggest';
// Actions
import {
  restaurantGeocodeSuggestFetchPending,
  restaurantGeocodeSuggestFetchSuccess,
  restaurantGeocodeSuggestFetchError,
  restaurantGeocodeSuggestSave,
} from '@actions/restaurant/info/geocode-suggest';
// Api
import { getAddressGeoCode } from '@api/address/geo-code';
// Types
import { TFetchRestaurantGeocodeSuggestEpic } from './types';

export const fetchRestaurantGeocodeSuggestEpic: TFetchRestaurantGeocodeSuggestEpic = (action$) =>
  action$.pipe(
    ofType(RESTAURANT_GEOCODE_SUGGEST_FETCH),
    switchMap(({ payload }) =>
      concat(
        of(restaurantGeocodeSuggestFetchPending()),
        getAddressGeoCode({
          cityId: payload.cityId,
          address: payload.description || payload.name,
        }).pipe(
          switchMap((response) => {
            const { status } = response;

            if (status === 200) {
              const { response: data } = response;

              return [
                restaurantGeocodeSuggestFetchSuccess({ status }),
                restaurantGeocodeSuggestSave({ data }),
              ];
            }

            return [restaurantGeocodeSuggestFetchError({ status })];
          }),
          catchError((error) => {
            console.error(error);
            return of(restaurantGeocodeSuggestFetchError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
