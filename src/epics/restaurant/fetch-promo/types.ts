// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/restaurant/menu/promo/types';

export type TFetchRestaurantPromoEpic = Epic<ActionTypes, ActionTypes>;
