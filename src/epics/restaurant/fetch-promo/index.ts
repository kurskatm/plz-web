// Modules
import { combineLatest, concat, of, timer } from 'rxjs';
import { catchError, debounceTime, map, mergeMap, switchMap, takeUntil } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { RESTAURANT_CARD_PROMO_FETCH } from '@constants/restaurant/menu/promo';
import { RESTAURANT_CARD_MENU_RESET } from '@constants/restaurant/menu';
// Actions
import {
  restaurantPromoSave,
  restaurantPromoFetchPending,
  restaurantPromoFetchSuccess,
  restaurantPromoFetchError,
} from '@actions/restaurant/menu/promo';
// Api
import { fetchRestaurantPromo } from '@api/restaurant/fetch-promo';
// Utils
import { Consts } from '@utils';
// Types
import { TFetchRestaurantPromoEpic } from './types';

const { MIN_LOADING_MS, REQUEST_MS } = Consts.DEBOUNCE;

export const fetchRestaurantPromoEpic: TFetchRestaurantPromoEpic = (action$) =>
  action$.pipe(
    ofType(RESTAURANT_CARD_PROMO_FETCH),
    debounceTime(REQUEST_MS),
    mergeMap(({ payload }) =>
      concat(
        of(restaurantPromoFetchPending()),
        combineLatest([timer(MIN_LOADING_MS), fetchRestaurantPromo(payload)]).pipe(
          map((x) => x[1]),
          switchMap((response) => {
            const { status } = response;

            if ([200, 204].includes(status)) {
              const data = response.response;
              return [restaurantPromoSave(data), restaurantPromoFetchSuccess({ status })];
            }

            return [restaurantPromoFetchError({ status })];
          }),
          catchError((error) => {
            const { status } = error;
            console.log(error);

            return of(restaurantPromoFetchError({ status }));
          }),
        ),
      ).pipe(takeUntil(action$.pipe(ofType(RESTAURANT_CARD_MENU_RESET)))),
    ),
  );
