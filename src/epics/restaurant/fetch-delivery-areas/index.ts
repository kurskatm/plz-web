// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { RESTAURANT_DELIVERY_AREAS_FETCH } from '@constants/restaurant/info/delivery-areas';
// Actions
import {
  restaurantDeliveryAreasFetchPending,
  restaurantDeliveryAreasFetchSuccess,
  restaurantDeliveryAreasFetchError,
  restaurantDeliveryAreasSave,
} from '@actions/restaurant/info/delivery-areas';
// Api
import { fetchRestaurantDeliveryAreas } from '@api/restaurant/fetch-delivery-areas';
// Types
import { TFetchRestaurantDeliveryAreasEpic } from './types';

export const fetchRestaurantDeliveryAreasEpic: TFetchRestaurantDeliveryAreasEpic = (action$) =>
  action$.pipe(
    ofType(RESTAURANT_DELIVERY_AREAS_FETCH),
    switchMap(({ payload }) =>
      concat(
        of(restaurantDeliveryAreasFetchPending()),
        fetchRestaurantDeliveryAreas(payload).pipe(
          switchMap((response) => {
            const { status } = response;

            if (status === 200) {
              const { response: data } = response;

              return [
                restaurantDeliveryAreasFetchSuccess({ status }),
                restaurantDeliveryAreasSave({ data }),
              ];
            }

            return [restaurantDeliveryAreasFetchError({ status })];
          }),
          catchError((error) => {
            console.error(error);
            return of(restaurantDeliveryAreasFetchError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
