// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/restaurant/info/delivery-areas/types';

export type TFetchRestaurantDeliveryAreasEpic = Epic<ActionTypes, ActionTypes>;
