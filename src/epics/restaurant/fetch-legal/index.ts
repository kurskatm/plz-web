// Modules
import { combineLatest, concat, of, timer } from 'rxjs';
import { catchError, debounceTime, map, mergeMap, switchMap, takeUntil } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { RESTAURANT_CARD_LEGAL_FETCH } from '@constants/restaurant/info/legal';
import { RESTAURANT_CARD_INFO_MAIN_RESET } from '@constants/restaurant/info';
// Actions
import {
  restaurantLegalSave,
  restaurantLegalFetchPending,
  restaurantLegalFetchSuccess,
  restaurantLegalFetchError,
} from '@actions/restaurant/info/legal';
// Api
import { fetchRestaurantLegal } from '@api/restaurant/fetch-legal';
// Utils
import { Consts } from '@utils';
// Types
import { TFetchRestaurantLegalEpic } from './types';

const { MIN_LOADING_MS, REQUEST_MS } = Consts.DEBOUNCE;

export const fetchRestaurantLegalEpic: TFetchRestaurantLegalEpic = (action$) =>
  action$.pipe(
    ofType(RESTAURANT_CARD_LEGAL_FETCH),
    debounceTime(REQUEST_MS),
    mergeMap(({ payload }) =>
      concat(
        of(restaurantLegalFetchPending()),
        combineLatest([timer(MIN_LOADING_MS), fetchRestaurantLegal(payload)]).pipe(
          map((x) => x[1]),
          switchMap((response) => {
            const { status } = response;

            if ([200, 204].includes(status)) {
              const data = response.response;
              return [restaurantLegalSave(data), restaurantLegalFetchSuccess({ status })];
            }

            return [restaurantLegalFetchError({ status })];
          }),
          catchError((error) => {
            const { status } = error;
            console.log(error);

            return of(restaurantLegalFetchError({ status }));
          }),
        ),
      ).pipe(takeUntil(action$.pipe(ofType(RESTAURANT_CARD_INFO_MAIN_RESET)))),
    ),
  );
