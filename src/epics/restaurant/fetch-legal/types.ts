// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/restaurant/info/legal/types';

export type TFetchRestaurantLegalEpic = Epic<ActionTypes, ActionTypes>;
