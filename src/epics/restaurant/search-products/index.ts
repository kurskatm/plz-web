// Modules
import { combineLatest, concat, of, timer } from 'rxjs';
import { catchError, debounceTime, map, mergeMap, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { RESTAURANT_SEARCH_PRODUCT } from '@constants/restaurant//search-products';
// Actions
import {
  restaurantSearchProductsPending,
  restaurantSearchProductsSuccess,
  restaurantSearchProductsError,
  restaurantSearchProductsSave,
} from '@actions/restaurant/menu/search-products';
// Api
import { restaurantSearchProducts } from '@api/restaurant/search-products';
// Utils
import { Consts } from '@utils';
// Types
import { TRestaurantSearchProductsEpic } from './types';

const { MIN_LOADING_MS, REQUEST_MS } = Consts.DEBOUNCE;

export const restaurantSearchProductsEpic: TRestaurantSearchProductsEpic = (action$) =>
  action$.pipe(
    ofType(RESTAURANT_SEARCH_PRODUCT),
    debounceTime(REQUEST_MS),
    mergeMap(({ payload }) =>
      concat(
        of(restaurantSearchProductsPending()),
        combineLatest([timer(MIN_LOADING_MS), restaurantSearchProducts(payload)]).pipe(
          map((x) => x[1]),
          switchMap((response) => {
            const { status } = response;

            if ([200, 204].includes(status)) {
              const data = response.response;
              return [
                restaurantSearchProductsSave({ data }),
                restaurantSearchProductsSuccess({ status }),
              ];
            }

            return [restaurantSearchProductsError({ status })];
          }),
          catchError((error) => {
            const { status } = error;
            console.log(error);

            return of(restaurantSearchProductsError({ status }));
          }),
        ),
      ),
    ),
  );
