// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/restaurant/menu/search-products/types';

export type TRestaurantSearchProductsEpic = Epic<ActionTypes, ActionTypes>;
