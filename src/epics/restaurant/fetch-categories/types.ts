// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/restaurant/menu/categories/types';

export type TFetchRestaurantCategoriesEpic = Epic<ActionTypes, ActionTypes>;
