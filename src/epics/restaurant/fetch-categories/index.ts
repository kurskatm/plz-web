// Modules
import { combineLatest, concat, of, timer } from 'rxjs';
import { catchError, debounceTime, map, mergeMap, switchMap, takeUntil } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { RESTAURANT_CARD_CATEGORIES_FETCH } from '@constants/restaurant/menu/categories';
import { RESTAURANT_CARD_MENU_RESET } from '@constants/restaurant/menu';
// Actions
import {
  restaurantCategoriesSave,
  restaurantCategoriesFetchPending,
  restaurantCategoriesFetchSuccess,
  restaurantCategoriesFetchError,
} from '@actions/restaurant/menu/categories';
// Api
import { fetchRestaurantCategories } from '@api/restaurant/fetch-categories';
// Utils
import { Consts } from '@utils';
// Types
import { TFetchRestaurantCategoriesEpic } from './types';

const { MIN_LOADING_MS, REQUEST_MS } = Consts.DEBOUNCE;

export const fetchRestaurantCategoriesEpic: TFetchRestaurantCategoriesEpic = (action$) =>
  action$.pipe(
    ofType(RESTAURANT_CARD_CATEGORIES_FETCH),
    debounceTime(REQUEST_MS),
    mergeMap(({ payload }) =>
      concat(
        of(restaurantCategoriesFetchPending()),
        combineLatest([timer(MIN_LOADING_MS), fetchRestaurantCategories(payload)]).pipe(
          map((x) => x[1]),
          switchMap((response) => {
            const { status } = response;

            if ([200, 204].includes(status)) {
              const data = response.response;
              return [restaurantCategoriesSave(data), restaurantCategoriesFetchSuccess({ status })];
            }

            return [restaurantCategoriesFetchError({ status })];
          }),
          catchError((error) => {
            const { status } = error;
            console.log(error);

            return of(restaurantCategoriesFetchError({ status }));
          }),
        ),
      ).pipe(takeUntil(action$.pipe(ofType(RESTAURANT_CARD_MENU_RESET)))),
    ),
  );
