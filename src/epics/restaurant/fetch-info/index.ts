// Modules
import { combineLatest, concat, of, timer } from 'rxjs';
import { catchError, debounceTime, map, mergeMap, switchMap, takeUntil } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { RESTAURANT_CARD_INFO_FETCH } from '@constants/restaurant/info/info';
import { RESTAURANT_CARD_INFO_MAIN_RESET } from '@constants/restaurant/info';
// Actions
import {
  restaurantInfoSave,
  restaurantInfoFetchPending,
  restaurantInfoFetchSuccess,
  restaurantInfoFetchError,
} from '@actions/restaurant/info/info';
// Api
import { fetchRestaurantInfo } from '@api/restaurant/fetch-info';
// Utils
import { Consts } from '@utils';
// Types
import { TFetchRestaurantInfoEpic } from './types';

const { MIN_LOADING_MS, REQUEST_MS } = Consts.DEBOUNCE;

export const fetchRestaurantInfoEpic: TFetchRestaurantInfoEpic = (action$) =>
  action$.pipe(
    ofType(RESTAURANT_CARD_INFO_FETCH),
    debounceTime(REQUEST_MS),
    mergeMap(({ payload }) =>
      concat(
        of(restaurantInfoFetchPending()),
        combineLatest([timer(MIN_LOADING_MS), fetchRestaurantInfo(payload)]).pipe(
          map((x) => x[1]),
          switchMap((response) => {
            const { status } = response;

            if ([200, 204].includes(status)) {
              const data = response.response;
              return [restaurantInfoSave(data), restaurantInfoFetchSuccess({ status })];
            }

            return [restaurantInfoFetchError({ status })];
          }),
          catchError((error) => {
            const { status } = error;
            console.log(error);

            return of(restaurantInfoFetchError({ status }));
          }),
        ),
      ).pipe(takeUntil(action$.pipe(ofType(RESTAURANT_CARD_INFO_MAIN_RESET)))),
    ),
  );
