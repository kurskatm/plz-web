// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/restaurant/info/info/types';

export type TFetchRestaurantInfoEpic = Epic<ActionTypes, ActionTypes>;
