// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/restaurant/info/address-suggest/types';

export type TFetchRestaurantAddressSuggestEpic = Epic<ActionTypes, ActionTypes>;
