// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { RESTAURANT_ADDRESSES_SUGGEST_FETCH } from '@constants/restaurant/info/address-suggest';
// Actions
import {
  restaurantAddressesSuggestFetchPending,
  restaurantAddressesSuggestFetchSuccess,
  restaurantAddressesSuggestFetchError,
  restaurantAddressesSuggestSave,
} from '@actions/restaurant/info/address-suggest';
// Api
import { getAddressSuggest } from '@api/address/suggest';
// Types
import { TFetchRestaurantAddressSuggestEpic } from './types';

export const fetchRestaurantAddressSuggestEpic: TFetchRestaurantAddressSuggestEpic = (action$) =>
  action$.pipe(
    ofType(RESTAURANT_ADDRESSES_SUGGEST_FETCH),
    switchMap(({ payload }) =>
      concat(
        of(restaurantAddressesSuggestFetchPending()),
        getAddressSuggest(payload).pipe(
          switchMap((response) => {
            const { status } = response;

            if (status === 200) {
              const { response: data } = response;

              return [
                restaurantAddressesSuggestFetchSuccess({ status }),
                restaurantAddressesSuggestSave({ data: data.items }),
              ];
            }

            return [restaurantAddressesSuggestFetchError({ status })];
          }),
          catchError((error) => {
            console.error(error);
            return of(restaurantAddressesSuggestFetchError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
