// Modules
import { combineEpics } from 'redux-observable';
// Epics
import { fetchRestaurantCardEpic } from './fetch-card';
import { fetchRestaurantCardIdEpic } from './fetch-card-id';
import { fetchRestaurantCategoriesEpic } from './fetch-categories';
import { fetchRestaurantPromoEpic } from './fetch-promo';
import { fetchRestaurantReviewsEpic } from './fetch-reviews';
import { fetchRestaurantInfoEpic } from './fetch-info';
import { fetchRestaurantLegalEpic } from './fetch-legal';
import { fetchRestaurantProductsEpic } from './fetch-products';
import { fetchRestaurantAddressSuggestEpic } from './fetch-address-suggest';
import { fetchRestaurantGeocodeSuggestEpic } from './fetch-geocode-suggest';
import { fetchRestaurantDeliveryAreasEpic } from './fetch-delivery-areas';
import { fetchRestaurantStoresEpic } from './fetch-stores';
import { restaurantSearchProductsEpic } from './search-products';

export const restaurantEpic = combineEpics(
  fetchRestaurantCardEpic,
  fetchRestaurantCardIdEpic,
  fetchRestaurantCategoriesEpic,
  fetchRestaurantPromoEpic,
  fetchRestaurantReviewsEpic,
  fetchRestaurantInfoEpic,
  fetchRestaurantLegalEpic,
  fetchRestaurantProductsEpic,
  fetchRestaurantAddressSuggestEpic,
  fetchRestaurantGeocodeSuggestEpic,
  fetchRestaurantDeliveryAreasEpic,
  restaurantSearchProductsEpic,
  fetchRestaurantStoresEpic,
);
