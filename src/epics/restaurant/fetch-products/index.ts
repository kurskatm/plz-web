// Modules
import { combineLatest, concat, of, timer } from 'rxjs';
import { catchError, debounceTime, map, mergeMap, switchMap, takeUntil } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { RESTAURANT_CARD_PRODUCTS_FETCH } from '@constants/restaurant/menu/products';
import { RESTAURANT_CARD_MENU_RESET } from '@constants/restaurant/menu';
// Actions
import {
  restaurantProductsSave,
  restaurantProductsFetchPending,
  restaurantProductsFetchSuccess,
  restaurantProductsFetchError,
} from '@actions/restaurant/menu/products';
// Api
import { fetchRestaurantProducts } from '@api/restaurant/fetch-products';
// Utils
import { Consts } from '@utils';
// Config
import { getValidProductsData } from './config';
// Types
import { TFetchRestaurantProductsEpic } from './types';

const { MIN_LOADING_MS, REQUEST_MS } = Consts.DEBOUNCE;

export const fetchRestaurantProductsEpic: TFetchRestaurantProductsEpic = (action$) =>
  action$.pipe(
    ofType(RESTAURANT_CARD_PRODUCTS_FETCH),
    debounceTime(REQUEST_MS),
    mergeMap(({ payload }) =>
      concat(
        of(restaurantProductsFetchPending()),
        combineLatest([timer(MIN_LOADING_MS), fetchRestaurantProducts(payload)]).pipe(
          map((x) => x[1]),
          switchMap((response) => {
            const { status } = response;

            if ([200, 204].includes(status)) {
              const data = getValidProductsData(response.response);
              return [restaurantProductsSave({ data }), restaurantProductsFetchSuccess({ status })];
            }

            return [restaurantProductsFetchError({ status })];
          }),
          catchError((error) => {
            const { status } = error;
            console.log(error);

            return of(restaurantProductsFetchError({ status }));
          }),
        ),
      ).pipe(takeUntil(action$.pipe(ofType(RESTAURANT_CARD_MENU_RESET)))),
    ),
  );
