/* eslint-disable @typescript-eslint/indent */
// Types
import { TGetValidProducts } from './types';

export const getValidProductsData: TGetValidProducts = (data) =>
  data.map((item) => ({
    ...item,
    products: item.products.map((product) => ({
      ...product,
      availableInBonusPoints: item.name === 'За баллы',
    })),
  }));
