/* eslint-disable @typescript-eslint/indent */
// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/restaurant/menu/products/types';
import { TRestaurantCategoryProducts } from '@/type/restaurants/restaurant-products';

export type TFetchRestaurantProductsEpic = Epic<ActionTypes, ActionTypes>;

export type TGetValidProducts = (
  data: TRestaurantCategoryProducts[],
) => TRestaurantCategoryProducts[];
