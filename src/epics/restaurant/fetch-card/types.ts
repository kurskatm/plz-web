// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/restaurant/main/card/types';

export type TFetchRestaurantCardEpic = Epic<ActionTypes, ActionTypes>;
