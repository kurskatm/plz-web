// Modules
import { combineLatest, concat, of, timer } from 'rxjs';
import { catchError, debounceTime, map, mergeMap, switchMap, takeUntil } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { RESTAURANT_CARD_FETCH } from '@constants/restaurant/main/card';
import { RESTAURANT_CARD_MAIN_RESET } from '@constants/restaurant/main';
// Actions
import {
  restaurantCardSave,
  restaurantCardFetchPending,
  restaurantCardFetchSuccess,
  restaurantCardFetchError,
} from '@actions/restaurant/main/card';
// Api
import { fetchRestaurantCard } from '@api/restaurant/fetch-card';
// Utils
import { Consts } from '@utils';
// Types
import { TFetchRestaurantCardEpic } from './types';

const { MIN_LOADING_MS, REQUEST_MS } = Consts.DEBOUNCE;

export const fetchRestaurantCardEpic: TFetchRestaurantCardEpic = (action$) =>
  action$.pipe(
    ofType(RESTAURANT_CARD_FETCH),
    debounceTime(REQUEST_MS),
    mergeMap(({ payload }) =>
      concat(
        of(restaurantCardFetchPending()),
        combineLatest([timer(MIN_LOADING_MS), fetchRestaurantCard(payload)]).pipe(
          map((x) => x[1]),
          switchMap((response) => {
            const { status } = response;

            if ([200].includes(status)) {
              const data = response.response;
              return [restaurantCardSave(data), restaurantCardFetchSuccess({ status })];
            }

            return [restaurantCardFetchError({ status })];
          }),
          catchError((error) => {
            const { status } = error;
            console.log(error);

            return of(restaurantCardFetchError({ status }));
          }),
        ),
      ).pipe(takeUntil(action$.pipe(ofType(RESTAURANT_CARD_MAIN_RESET)))),
    ),
  );
