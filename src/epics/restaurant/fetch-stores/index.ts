// Modules
import { combineLatest, concat, of, timer } from 'rxjs';
import { catchError, debounceTime, map, mergeMap, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { RESTAURANT_STORES_FETCH } from '@constants/restaurant/stores';
// Actions
import {
  restaurantStoresPending,
  restaurantStoresSuccess,
  restaurantStoresSave,
  restaurantStoresError,
} from '@actions/restaurant/stores';
// Api
import { fetchRestaurantStores } from '@api/restaurant/fetch-stores';
// Utils
import { Consts } from '@utils';
// Types
import { TRestaurantStoresFetchEpic } from './types';

const { MIN_LOADING_MS, REQUEST_MS } = Consts.DEBOUNCE;

export const fetchRestaurantStoresEpic: TRestaurantStoresFetchEpic = (action$) =>
  action$.pipe(
    ofType(RESTAURANT_STORES_FETCH),
    debounceTime(REQUEST_MS),
    mergeMap(({ payload }) =>
      concat(
        of(restaurantStoresPending()),
        combineLatest([timer(MIN_LOADING_MS), fetchRestaurantStores(payload)]).pipe(
          map((x) => x[1]),
          switchMap((response) => {
            const { status } = response;

            if ([200].includes(status)) {
              const { response: data } = response;

              return [restaurantStoresSuccess({ status }), restaurantStoresSave(data)];
            }

            return [restaurantStoresError({ status })];
          }),
          catchError((error) => {
            const { status } = error;
            console.log(error);

            return of(restaurantStoresError({ status }));
          }),
        ),
      ),
    ),
  );
