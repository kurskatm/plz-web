// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/restaurant/main/id/types';

export type TFetchRestaurantCardIdEpic = Epic<ActionTypes, ActionTypes>;
