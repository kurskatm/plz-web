// Modules
import { combineLatest, concat, of, timer } from 'rxjs';
import { catchError, debounceTime, map, mergeMap, switchMap, takeUntil } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { RESTAURANT_CARD_ID_FETCH } from '@constants/restaurant/main/id';
import { RESTAURANT_CARD_MAIN_RESET } from '@constants/restaurant/main';
// Actions
import {
  restaurantCardIdSave,
  restaurantCardIdFetchPending,
  restaurantCardIdFetchSuccess,
  restaurantCardIdFetchError,
} from '@actions/restaurant/main/id';
// Api
import { fetchRestaurantCardId } from '@api/restaurant/fetch-card-id';
// Utils
import { Consts } from '@utils';
// Types
import { TFetchRestaurantCardIdEpic } from './types';

const { MIN_LOADING_MS, REQUEST_MS } = Consts.DEBOUNCE;

export const fetchRestaurantCardIdEpic: TFetchRestaurantCardIdEpic = (action$) =>
  action$.pipe(
    ofType(RESTAURANT_CARD_ID_FETCH),
    debounceTime(REQUEST_MS),
    mergeMap(({ payload }) =>
      concat(
        of(restaurantCardIdFetchPending()),
        combineLatest([timer(MIN_LOADING_MS), fetchRestaurantCardId(payload)]).pipe(
          map((x) => x[1]),
          switchMap((response) => {
            const { status } = response;

            if ([200].includes(status)) {
              const data = response.response;
              return [restaurantCardIdSave(data), restaurantCardIdFetchSuccess({ status })];
            }

            return [restaurantCardIdFetchError({ status })];
          }),
          catchError((error) => {
            const { status } = error;
            console.log(error);

            return of(restaurantCardIdFetchError({ status }));
          }),
        ),
      ).pipe(takeUntil(action$.pipe(ofType(RESTAURANT_CARD_MAIN_RESET)))),
    ),
  );
