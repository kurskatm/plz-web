// Modules
import { combineEpics } from 'redux-observable';
// Epics
import { fetchHitsEpic } from './fetch';

export const hitsEpic = combineEpics(fetchHitsEpic);
