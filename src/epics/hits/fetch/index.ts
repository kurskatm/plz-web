// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { HITS_FETCH } from '@constants/hits';
// Actions
import { hitsSave, hitsPending, hitsSuccess, hitsError } from '@actions/hits';
// Api
import { getHits } from '@/api/hits/fetch';
// Types
import { THitsEpic } from './types';

export const fetchHitsEpic: THitsEpic = (action$) =>
  action$.pipe(
    ofType(HITS_FETCH),
    switchMap(({ payload }) =>
      concat(
        of(hitsPending()),
        getHits(payload).pipe(
          switchMap((response) => {
            const { status } = response;

            if (status === 200) {
              const { response: data } = response;

              return [hitsSuccess({ status }), hitsSave(data)];
            }

            return [hitsError({ status })];
          }),
          catchError((error) => {
            console.error(error);
            return of(hitsError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
