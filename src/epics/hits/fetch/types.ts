// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/hits/types';

export type THitsEpic = Epic<ActionTypes, ActionTypes>;
