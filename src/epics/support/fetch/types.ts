// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/support/types';

export type TSupportTopicsEpic = Epic<ActionTypes, ActionTypes>;
