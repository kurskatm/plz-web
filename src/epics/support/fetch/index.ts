// Modules
import { combineLatest, concat, of, timer } from 'rxjs';
import { catchError, switchMap, debounceTime, map } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { SUPPORT_TOPICS_FETCH } from '@constants/support';
// Actions
import {
  supportTopicsSave,
  supportTopicsPending,
  supportTopicsSuccess,
  supportTopicsError,
} from '@actions/support';
// Api
import { getSupportTopics } from '@/api/support/fetch';
// Utils
import { Consts } from '@utils';
// Types
import { TSupportTopicsEpic } from './types';

const { MIN_LOADING_MS, REQUEST_MS } = Consts.DEBOUNCE;

export const fetchSupportTopicsEpic: TSupportTopicsEpic = (action$) =>
  action$.pipe(
    ofType(SUPPORT_TOPICS_FETCH),
    debounceTime(REQUEST_MS),
    switchMap(() =>
      concat(
        of(supportTopicsPending()),
        combineLatest([timer(MIN_LOADING_MS), getSupportTopics()]).pipe(
          map((x) => x[1]),
          switchMap((response) => {
            const { status } = response;

            if (status === 200) {
              const { response: data } = response;

              return [supportTopicsSuccess({ status }), supportTopicsSave(data)];
            }

            return [supportTopicsError({ status })];
          }),
          catchError((error) => {
            console.error(error);
            return of(supportTopicsError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
