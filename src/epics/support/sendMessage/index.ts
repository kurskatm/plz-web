// Modules
import { combineLatest, concat, of, timer } from 'rxjs';
import { catchError, switchMap, debounceTime, map } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { SUPPORT_MESSAGE_SEND } from '@constants/support';
// Actions
import {
  supportMessageSent,
  supportMessagePending,
  supportMessageSuccess,
  supportMessageError,
} from '@actions/support';
// Api
import { supportSendMessage } from '@/api/support/sendMessage';
// Utils
import { Consts } from '@utils';
// Types
import { TSupportTopicsEpic } from './types';

const { MIN_LOADING_MS, REQUEST_MS } = Consts.DEBOUNCE;

export const sendMessageSupportEpic: TSupportTopicsEpic = (action$) =>
  action$.pipe(
    ofType(SUPPORT_MESSAGE_SEND),
    debounceTime(REQUEST_MS),
    switchMap(({ payload }) =>
      concat(
        of(supportMessagePending()),
        combineLatest([timer(MIN_LOADING_MS), supportSendMessage(payload)]).pipe(
          map((x) => x[1]),
          switchMap((response) => {
            const { status } = response;

            if (status === 200) {
              const { response: data } = response;

              return [supportMessageSuccess({ status }), supportMessageSent(data)];
            }

            return [supportMessageError({ status })];
          }),
          catchError((error) => {
            console.error(error);
            return of(supportMessageError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
