// Modules
import { combineEpics } from 'redux-observable';
// Epics
import { fetchSupportTopicsEpic } from './fetch';
import { sendMessageSupportEpic } from './sendMessage';

export const supportEpic = combineEpics(fetchSupportTopicsEpic, sendMessageSupportEpic);
