// Modules
import { concat, of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';
import { ofType } from 'redux-observable';
// Constants
import { HOT_OFFERS_FETCH } from '@constants/hotOffers';
// Actions
import {
  hotOffersSave,
  hotOffersPending,
  hotOffersSuccess,
  hotOffersError,
} from '@actions/hotOffers';
// Api
import { getHotOffers } from '@/api/hotOffers/fetch';
// Types
import { THotOffersEpic } from './types';

export const fetchHotOffersEpic: THotOffersEpic = (action$) =>
  action$.pipe(
    ofType(HOT_OFFERS_FETCH),
    switchMap(({ payload }) =>
      concat(
        of(hotOffersPending()),
        getHotOffers(payload).pipe(
          switchMap((response) => {
            const { status } = response;

            if (status === 200) {
              const { response: data } = response;

              return [hotOffersSuccess({ status }), hotOffersSave(data)];
            }

            return [hotOffersError({ status })];
          }),
          catchError((error) => {
            console.error(error);
            return of(hotOffersError({ status: error.status }));
          }),
        ),
      ),
    ),
  );
