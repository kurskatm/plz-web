// Modules
import { Epic } from 'redux-observable';
// Actions
import { ActionTypes } from '@actions/hotOffers/types';

export type THotOffersEpic = Epic<ActionTypes, ActionTypes>;
