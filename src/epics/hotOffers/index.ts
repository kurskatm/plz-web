// Modules
import { combineEpics } from 'redux-observable';
// Epics
import { fetchHotOffersEpic } from './fetch';

export const hotOffersEpic = combineEpics(fetchHotOffersEpic);
