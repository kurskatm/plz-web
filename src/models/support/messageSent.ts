// Models
import { uiModel } from '../ui';
// Types
import { TSupportMessageSentModel } from './types';

export const supportMessageSentModel: TSupportMessageSentModel = {
  ui: uiModel,
};
