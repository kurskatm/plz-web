// Models
import { uiModel } from '../ui';
// Types
import { TSupportTopicsModel } from './types';

export const supportTopicsModel: TSupportTopicsModel = {
  data: [],
  ui: uiModel,
};
