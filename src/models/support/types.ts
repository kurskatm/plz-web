// Types
import { TUiModel } from '@models/ui/types';
import { TRestaurantCardPropsList } from '@components/RestaurantCard/types';

export interface TSupportTopicsModel {
  data: TRestaurantCardPropsList;
  ui: TUiModel;
}
export interface TSupportMessageSentModel {
  ui: TUiModel;
}

export interface TSupportModel {
  topics: TSupportTopicsModel;
  messageSent: TSupportMessageSentModel;
}
