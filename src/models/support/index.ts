// Types
import { TSupportModel } from './types';

import { supportTopicsModel } from './topics';
import { supportMessageSentModel } from './messageSent';

export const supportModel: TSupportModel = {
  topics: supportTopicsModel,
  messageSent: supportMessageSentModel,
};
