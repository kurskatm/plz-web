// Models
import { restaurantMainModel } from './main';
import { restaurantMenuModel } from './menu';
import { restaurantReviewsModel } from './reviews';
import { restaurantInfoModel } from './info';
import { restaurantModalModel } from './modal';
import { resstaurantSearchProductModel } from './search-products';
// Types
import { TRestaurantModel } from './types';

export const restaurantModel: TRestaurantModel = {
  main: restaurantMainModel,
  menu: restaurantMenuModel,
  reviews: restaurantReviewsModel,
  info: restaurantInfoModel,
  modal: restaurantModalModel,
  searchProducts: resstaurantSearchProductModel,
};
