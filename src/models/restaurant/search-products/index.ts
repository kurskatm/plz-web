// Models
import { uiModel } from '@models/ui';
// Types
import { TRestaurantSearchProductsModel } from './types';

export const resstaurantSearchProductModel: TRestaurantSearchProductsModel = {
  search: '',
  products: {
    data: [],
    ui: uiModel,
  },
};
