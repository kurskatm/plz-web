// Main Types
import { TUiModel } from '@models/ui/types';
import { TRestaurantProduct } from '@type/restaurants/restaurant-products';

export interface TRestaurantSearchProductsModel {
  search: string;
  products: {
    data: TRestaurantProduct[];
    ui: TUiModel;
  };
}
