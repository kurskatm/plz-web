// Models
import { uiModel } from '../../ui';
// Types
import { TRestaurantMainModel } from './types';

export const restaurantMainModel: TRestaurantMainModel = {
  id: {
    data: null,
    ui: uiModel,
  },

  stores: [],

  info: {
    data: null,
    ui: uiModel,
  },
};
