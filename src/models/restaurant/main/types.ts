// Main Types
import { TUiModel } from '@models/ui/types';
import { TRestaurantCard } from '@type/restaurants/restaurant-card';

export interface TRestaurantMainModel {
  id: {
    data: {
      id: string;
    } | null;
    ui: TUiModel;
  };
  stores: [];
  info: {
    data: TRestaurantCard | null;
    ui: TUiModel;
  };
}
