// Main Types
import { TReviewList } from '@type/restaurants/reviews';
import { TPaginationObject } from '../../pagination/types';
import { TUiModel } from '../../ui/types';

export interface TRestaurantReviewsModel {
  current: {
    data: TReviewList;
    pagination: TPaginationObject;
    ui: TUiModel;
  };
}
