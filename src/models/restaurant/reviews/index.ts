// Models
import { getPaginationModel } from '../../pagination';
import { uiModel } from '../../ui';
// Types
import { TRestaurantReviewsModel } from './types';

export const restaurantReviewsCurrentPaginationModel = getPaginationModel({ limit: 10 });

export const restaurantReviewsModel: TRestaurantReviewsModel = {
  current: {
    data: [],
    pagination: restaurantReviewsCurrentPaginationModel,
    ui: uiModel,
  },
};
