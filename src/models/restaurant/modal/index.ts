// Types
import { TRestaurantModalModel } from './types';

export const restaurantModalModel: TRestaurantModalModel = {
  closeRestaurantModal: {
    showModal: false,
  },
  otherModals: {
    showModal: false,
    modalType: '',
    pointsRest: 0,
    dataToUpdateCart: null,
    link: null,
  },
  shoppingCardModal: {
    showModal: false,
  },
  modifiersCardModel: {
    showModal: false,
    productId: null,
    inBonusPoint: false,
  },
  promoCardModal: {
    showModal: false,
    promoId: null,
  },
};
