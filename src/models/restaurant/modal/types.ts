import { TRestaurantDataToUpdateCart } from '@/type/restaurants/restaurant-card';

export interface TRestaurantModalModel {
  closeRestaurantModal: {
    showModal: boolean;
  };
  otherModals: {
    showModal: boolean;
    modalType:
      | 'not-enough-points'
      | 'another-restaurant'
      | 'another-point-product'
      | 'change-address'
      | '';
    pointsRest: number;
    dataToUpdateCart: TRestaurantDataToUpdateCart | null;
    link: string | null;
  };
  shoppingCardModal: {
    showModal: boolean;
  };
  modifiersCardModel: {
    showModal: boolean;
    productId: string | null;
    inBonusPoint: boolean;
  };
  promoCardModal: {
    showModal: boolean;
    promoId: string | null;
  };
}
