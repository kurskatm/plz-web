// Models
import { uiModel } from '../../ui';
// Types
import { TRestaurantMenuModel } from './types';

export const restaurantMenuModel: TRestaurantMenuModel = {
  categories: {
    data: [],
    ui: uiModel,
  },

  products: {
    data: [],
    ui: uiModel,
  },

  promo: {
    data: [],
    ui: uiModel,
  },
};
