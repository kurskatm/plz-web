// Main Types
import { TUiModel } from '@models/ui/types';
import { TRestaurantCategoriesList } from '@type/restaurants/restaurant-categories';
import { TRestaurantCategoryProducts } from '@type/restaurants/restaurant-products';
import { TRestaurantPromoList } from '@type/restaurants/promo';

export interface TRestaurantMenuModel {
  categories: {
    data: TRestaurantCategoriesList;
    ui: TUiModel;
  };

  products: {
    data: TRestaurantCategoryProducts[];
    ui: TUiModel;
  };

  promo: {
    data: TRestaurantPromoList;
    ui: TUiModel;
  };
}
