// Types
import { TRestaurantMainModel } from './main/types';
import { TRestaurantMenuModel } from './menu/types';
import { TRestaurantReviewsModel } from './reviews/types';
import { TRestaurantInfoModel } from './info/types';
import { TRestaurantModalModel } from './modal/types';
import { TRestaurantSearchProductsModel } from './search-products/types';

export interface TRestaurantModel {
  main: TRestaurantMainModel;
  menu: TRestaurantMenuModel;
  reviews: TRestaurantReviewsModel;
  info: TRestaurantInfoModel;
  modal: TRestaurantModalModel;
  searchProducts: TRestaurantSearchProductsModel;
}
