// Main Types
import { TRestaurantInfo } from '@type/restaurants/restaurant-info';
import { TRestaurantLegal } from '@type/restaurants/restaurant-legal';
import { TUiModel } from '../../ui/types';
import { TDeliveryAreasModel } from './delivery-areas/types';

export interface TRestaurantInfoModel {
  info: {
    data: TRestaurantInfo | null;
    ui: TUiModel;
  };

  legal: {
    data: TRestaurantLegal | null;
    ui: TUiModel;
  };
  deliveryAreas: TDeliveryAreasModel;
}
