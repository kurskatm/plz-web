// Models
import { uiModel } from '../../ui';
import { deliveryAreasModel } from './delivery-areas';
// Types
import { TRestaurantInfoModel } from './types';

export const restaurantInfoModel: TRestaurantInfoModel = {
  info: {
    data: null,
    ui: uiModel,
  },

  legal: {
    data: null,
    ui: uiModel,
  },
  deliveryAreas: deliveryAreasModel,
};
