// Main Types
import { TNewAddressesList } from '@type/new-address';
import { TNewGeocodeData } from '@type/new-geocode';
import { TUiModel } from '@models/ui/types';
import { TDeliveryAreaConditionsList } from '@type/restaurants/delivery-conditions';

export interface TDeliveryAreasModel {
  address: {
    data: TNewAddressesList | null;
    ui: TUiModel;
  };
  geocode: {
    data: TNewGeocodeData | null;
    ui: TUiModel;
  };
  deliveryAreas: {
    data: TDeliveryAreaConditionsList;
    ui: TUiModel;
  };
}
