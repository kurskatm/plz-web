// Models
import { uiModel } from '@models/ui';
// Types
import { TDeliveryAreasModel } from './types';

export const deliveryAreasModel: TDeliveryAreasModel = {
  address: {
    data: null,
    ui: uiModel,
  },
  geocode: {
    data: null,
    ui: uiModel,
  },
  deliveryAreas: {
    data: [],
    ui: uiModel,
  },
};
