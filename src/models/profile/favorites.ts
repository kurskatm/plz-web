// Types
import { uiModel } from '../ui';
import { TFavoritesSummaryModel } from './types';

export const favoritesSummaryModel: TFavoritesSummaryModel = {
  data: null,
  ui: uiModel,
};
