// Models
import { reviewsModel } from './reviews';
import { infoModel } from './info';
import { friendsModel } from './friends';
import { ordersModel } from './orders';
import { favoritesSummaryModel } from './favorites';
import { newAddressModel } from './new-address';
import { newGeoCodeModel } from './new-geo-code';
// Types
import { TProfileModel } from './types';

export const profileModel: TProfileModel = {
  auth: null,
  city: null,
  pickup: false,
  newAddress: newAddressModel,
  newGeoCode: newGeoCodeModel,
  info: infoModel,
  reviews: reviewsModel,
  favorites: favoritesSummaryModel,
  friends: friendsModel,
  orders: ordersModel,
  userId: '',
};
