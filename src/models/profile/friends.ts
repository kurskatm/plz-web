// Models
import { uiModel } from '../ui';
// Types
import { TFriendsModel } from './types';

export const friendsModel: TFriendsModel = {
  data: null,
  ui: uiModel,
};
