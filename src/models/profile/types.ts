// Main Types
import { TUiModel } from '@models/ui/types';
import { TEmailValue } from '@/pages/profile-pages/widgets/ProfileInfo/types';
import { TPaginationObject } from '@models/pagination/types';
import { TUserOrdersData, TUserOrder, TUserReviewsList } from '@type/profile';
import { TOrderToCart } from '@type/orderFromProfileToCart';
import { TFavoritesSummaryData } from '@type/new-favorites';
import { TNewAddressModel } from './new-address/types';
import { TNewGeoCodeModel } from './new-geo-code/types';

export interface TInfoDataModel {
  avatar: string;
  dateOfBirth: string;
  email: TEmailValue;
  fullName: string;
  gender: 0;
  id: string;
  invitationId: string;
  phoneNumber: string;
}

export interface TProfileDataModel {
  userId: string;
  access_token: number;
  refresh_token?: string;
}

export interface TInfoModel {
  data: TInfoDataModel;
  ui: TUiModel;
  updateUi: TUiModel;
}
export interface TFriendsModel {
  data: Record<string, unknown>[];
  ui: TUiModel;
}
export interface TReviewsModel {
  data: TUserReviewsList;
  reviewsLength: number;
  pagination: TPaginationObject;
  ui: TUiModel;
}

export interface TContactMeDetailsModel {
  retryAfter: number;
  errorMessage: string;
}

export interface TProfileOrdersModel {
  newOrders: {
    data: TUserOrdersData;
    pagination: TPaginationObject;
    after: string | null;
    ui: TUiModel;
  };
  modal: {
    showModal: boolean;
    orderId: string;
  };
  reviews: {
    add: {
      data: string[];
      ui: TUiModel;
    };
    lastOrder: {
      data: TUserOrder | null;
      ui: TUiModel;
    };
  };
  userOrder: {
    data: TUserOrder | null;
    ui: TUiModel;
  };
  contactMe: {
    orderId: string;
    ui: TUiModel;
    details: TContactMeDetailsModel;
  };
  repeatOrder: {
    data: TOrderToCart | null;
    ui: TUiModel;
    orderId: string;
    needRedirect: boolean;
  };
}

export interface TProfileModel {
  auth: string | null;
  city: string;
  pickup: boolean | null;
  newAddress: TNewAddressModel;
  newGeoCode: TNewGeoCodeModel;
  favorites: TFavoritesSummaryModel;
  reviews: TReviewsModel;
  friends: TFriendsModel;
  orders: TProfileOrdersModel;
  info: TInfoModel;
  userId: string;
}

export interface TFavoritesSummaryModel {
  data: TFavoritesSummaryData;
  ui: TUiModel;
}
