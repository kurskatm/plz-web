// Models
import { uiModel } from '../ui';
import { getPaginationModel } from '../pagination';
// Types
import { TProfileOrdersModel } from './types';

export const profileOrdersPaginationModel = getPaginationModel({ limit: 10 });

export const ordersModel: TProfileOrdersModel = {
  newOrders: {
    data: [],
    pagination: profileOrdersPaginationModel,
    after: null,
    ui: uiModel,
  },
  modal: {
    showModal: false,
    orderId: '',
  },
  reviews: {
    add: {
      data: [],
      ui: uiModel,
    },
    lastOrder: {
      data: null,
      ui: uiModel,
    },
  },
  userOrder: {
    data: null,
    ui: uiModel,
  },
  contactMe: {
    orderId: '',
    ui: uiModel,
    details: {
      retryAfter: 0,
      errorMessage: '',
    },
  },
  repeatOrder: {
    data: null,
    ui: uiModel,
    orderId: '',
    needRedirect: false,
  },
};
