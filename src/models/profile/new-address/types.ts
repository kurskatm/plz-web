// Main types
import { TNewAddressesList, TNewAddressItem } from '@type/new-address';
// Models
import { TUiModel } from '@models/ui/types';

export interface TSuggestInfo {
  data: TNewAddressesList;
  ui: TUiModel;
}

export interface TNewAddressModel {
  address: TNewAddressItem | null;
  suggest: TSuggestInfo;
}
