// Model
import { uiModel } from '@models/ui';
// Types
import { TNewAddressModel } from './types';

export const newAddressModel: TNewAddressModel = {
  address: null,
  suggest: {
    data: [],
    ui: uiModel,
  },
};
