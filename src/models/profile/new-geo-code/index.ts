// Models
import { uiModel } from '@models/ui';
// Types
import { TNewGeoCodeModel } from './types';

export const newGeoCodeModel: TNewGeoCodeModel = {
  data: null,
  ui: uiModel,
};
