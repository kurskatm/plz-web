// Main types
import { TNewGeocodeData } from '@type/new-geocode';
import { TUiModel } from '@models/ui/types';

export interface TNewGeoCodeModel {
  data: TNewGeocodeData | null;
  ui: TUiModel;
}
