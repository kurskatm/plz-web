// Models
import { uiModel } from '../ui';
// Types
import { TInfoModel } from './types';

export const infoModel: TInfoModel = {
  data: null,
  ui: uiModel,
  updateUi: uiModel,
};
