// Models
import { uiModel } from '../ui';
import { getPaginationModel } from '../pagination';
// Types
import { TReviewsModel } from './types';

export const profileReviewsPaginationModel = getPaginationModel({ limit: 10 });

export const reviewsModel: TReviewsModel = {
  data: [],
  pagination: profileReviewsPaginationModel,
  reviewsLength: 0,
  ui: uiModel,
};
