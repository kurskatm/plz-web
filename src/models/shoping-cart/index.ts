// Models
import { uiModel } from '../ui';
// Types
import { TRestaurantShopingCart, TShopingCartModel } from './types';

export const shopingCartRestaurantModel: TRestaurantShopingCart = {
  id: null,
  name: null,
  logoUrl: null,
  stores: [],
};

export const shopingCartModel: TShopingCartModel = {
  id: null,
  restaurant: shopingCartRestaurantModel,

  products: [],

  options: {
    cutlery: null,
  },

  fetch: {
    ui: uiModel,
  },

  fetchRestaurant: {
    ui: uiModel,
  },

  save: {
    ui: uiModel,
    details: {
      errorMessage: '',
    },
  },

  checkout: {
    error: null,
    ui: uiModel,
    id: null,
    paymentResult: {},
  },
  bonusProductId: null,
};
