// Main Types
import {
  TRestaurantShopingCart,
  TShopingCartProduct,
  TShopingCartOptions,
} from '@type/shoping-cart';
import { TRestaurantProductModifier } from '@type/restaurants/restaurant-products';
// Types
import { TUiModel } from '../ui/types';

export interface TShopingCartModel {
  id: string | boolean;
  restaurant: TRestaurantShopingCart;
  products: TShopingCartProduct[];
  options: TShopingCartOptions;
  fetch: {
    ui: TUiModel;
  };
  fetchRestaurant: {
    ui: TUiModel;
  };
  save: {
    ui: TUiModel;
    details: {
      errorMessage: string;
    };
  };
  checkout: {
    ui: TUiModel;
    id: string;
    paymentResult: Record<string, string>;
    error: { type: string; message: string };
  };
  bonusProductId: string | null;
}

export interface TShopingCartProductUpdate {
  id: string;
  count: number;
  inBonusPoint: boolean;
  modifiers?: TRestaurantProductModifier[];
}
export interface TShopingCartUpdateModel {
  restaurantId: string | boolean;
  products: TShopingCartProductUpdate[];
  cutleryCount: number;
}

export { TRestaurantShopingCart, TShopingCartProduct, TShopingCartOptions };
