// Models
import { uiModel } from '@models/ui';
// Types
import { THotOffersModel } from './types';

export const hotOffersModel: THotOffersModel = {
  data: [],
  ui: uiModel,
};
