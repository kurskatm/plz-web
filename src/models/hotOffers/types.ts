// Types
import { TUiModel } from '@models/ui/types';
import { THotOffersList } from '@type/hotOffers';

export interface THotOffersModel {
  data: THotOffersList;
  ui: TUiModel;
}
