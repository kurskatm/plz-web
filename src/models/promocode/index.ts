// Models
import { uiModel } from '../ui';
// Types
import { TPromocodeModel } from './types';

export const promocodeModel: TPromocodeModel = {
  data: null,
  ui: uiModel,
  validUi: uiModel,
};
