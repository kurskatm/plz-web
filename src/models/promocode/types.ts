import { TPromocodeResult } from '@type/promocode';
import { TUiModel } from '../ui/types';

export interface TPromocodeModel {
  data: TPromocodeResult;
  ui: TUiModel;
  validUi: TUiModel;
}
