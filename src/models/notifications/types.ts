// Main types
import { TContent } from '@/type/content';

export interface TNotification {
  id: string;
  type: 'success' | 'error' | 'info';
  content: TContent;
  duration: number; // use Infinity or NaN for infinite duration
}

export interface TNotificationsModel {
  notifications: TNotification[];
}
