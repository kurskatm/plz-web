// Models
import { uiModel } from '../ui';
// Types
import { TFiltersModel } from './types';

export const filtersModel: TFiltersModel = {
  chosen: [],

  list: {
    data: [],
    ui: uiModel,
  },

  search: '',

  showModal: false,
};
