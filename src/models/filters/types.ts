// Main Types
import { TFilter } from '@type/filters';
// Models
import { TUiModel } from '../ui/types';

export interface TFiltersModel {
  chosen: string[];

  list: {
    data: TFilter[];
    ui: TUiModel;
  };

  search: string | null;

  showModal: boolean;
}
