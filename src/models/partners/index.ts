// Models
import { partnersMembersModel } from './members';
import { partnersCountersModel } from './counters';

export const partnersModel = {
  addresses: partnersMembersModel,
  counters: partnersCountersModel,
};
