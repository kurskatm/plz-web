// Models
import { uiModel } from '../ui';
// Types
import { TPartnersCountersModel } from './types';

export const partnersCountersModel: TPartnersCountersModel = {
  data: [],
  ui: uiModel,
};
