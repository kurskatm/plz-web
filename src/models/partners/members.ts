// Models
import { uiModel } from '../ui';
// Types
import { TPartnersMembersModel } from './types';

export const partnersMembersModel: TPartnersMembersModel = {
  data: [],
  ui: uiModel,
};
