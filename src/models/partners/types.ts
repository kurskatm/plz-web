// Main Types
import { TUiModel } from '@models/ui/types';

export interface TPartnersMembersModel {
  data: [];
  ui: TUiModel;
}

export interface TPartnersCountersModel {
  data: [];
  ui: TUiModel;
}

export interface TPartnersModel {
  counters: TPartnersCountersModel;
  members: TPartnersMembersModel;
}
