// Lib
import { Cookies } from '@lib/cookie/cookie-manage';
// Types
import { TOldSiteBannerModel } from './types';

const oldSiteCookie = Cookies.get('new-site', true);

export const oldSiteBannerModel: TOldSiteBannerModel = {
  isFull: !oldSiteCookie || oldSiteCookie !== 0,
};
