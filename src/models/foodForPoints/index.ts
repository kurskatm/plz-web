// Models
import { uiModel } from '@models/ui';
// Types
import { TFoodForPointsModel } from './types';

export const foodForPointsModel: TFoodForPointsModel = {
  data: null,
  ui: uiModel,
};
