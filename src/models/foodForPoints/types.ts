// Types
import { TUiModel } from '@models/ui/types';
// Main Types
import { TFoodForPoints } from '@type/foodForPoints';

export interface TFoodForPointsModel {
  data: TFoodForPoints;
  ui: TUiModel;
}
