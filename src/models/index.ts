// Models
import { authModel } from './auth';
import { citiesModel } from './cities';
import { contactsModel } from './contacts';
import { cuisinesModel } from './cuisines';
import { filtersModel } from './filters';
import { headerModel } from './header';
import { hotOffersModel } from './hotOffers';
import { profileModel } from './profile';
import { restaurantsModel } from './restaurants';
import { shopingCartModel } from './shoping-cart';
import { supportModel } from './support';
import { restaurantModel } from './restaurant';
import { restaurantsAndHitsModel } from './restaurants-and-hits';
import { reviewsModel } from './reviews';
import { checkoutModalsModel } from './checkout-modals';
import { notificationsModel } from './notifications';
import { oldSiteBannerModel } from './oldSiteBanner';
import { foodForPointsModel } from './foodForPoints';

export const initialState = {
  auth: authModel,
  cities: citiesModel,
  contacts: contactsModel,
  checkoutModals: checkoutModalsModel,
  cuisines: cuisinesModel,
  filters: filtersModel,
  header: headerModel,
  hotOffers: hotOffersModel,
  oldSiteBanner: oldSiteBannerModel,
  profile: profileModel,
  restaurant: restaurantModel,
  restaurants: restaurantsModel,
  restaurantsAndHits: restaurantsAndHitsModel,
  reviews: reviewsModel,
  shopingCart: shopingCartModel,
  support: supportModel,
  notifications: notificationsModel,
  foodForPoints: foodForPointsModel,
};
