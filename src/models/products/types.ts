// Types
import { TUiModel } from '@models/ui/types';
// Main Types
import { TProfileModel } from '@models/profile/types';
import { TUpdateShopingCart } from '@actions/shoping-cart/change/action-types';
import { TRestaurantModel } from '@models/restaurant/types';
import { TShopingCartModel } from '@models/shoping-cart/types';
import { TCitiesItem } from '@type/cities';
import { TRestaurantCardIdSave } from '@actions/restaurant/main/id/action-types';
import { TChangeAddressModalShow } from '@actions/checkout/modals/action-types';

export interface THitProps {
  id: string;
  disabledButton: boolean;
  imageUrls: string[];
  name: string;
  description: string;
  scores?: string;
  price?: string | number;
  weight?: string;
  forScores?: boolean;
  liked?: boolean;
  updateShopingCart: (quantity: number) => void;
  quantity?: number;
  authModalShow: (data: boolean) => void;
  profile: TProfileModel;
  availableInBonusPoints: boolean;
  shopingCart: TShopingCartModel;
  shopingCartUpdate: TUpdateShopingCart;
  restaurantCardIdSave: TRestaurantCardIdSave;
  city: TCitiesItem;
  restaurant: TRestaurantModel;
  shopingCartSaveError: boolean;
  changeAddressModalShow: TChangeAddressModalShow;
  withLike?: boolean;
}

export interface TProductsModel {
  data: THitProps[];
  ui: TUiModel;
}
