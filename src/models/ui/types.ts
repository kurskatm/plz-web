/* eslint-disable @typescript-eslint/no-explicit-any */
export interface TUiModel {
  status: string;
  details: string | number;
}
