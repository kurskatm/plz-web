// Utils
import { Consts } from '@utils';
// Types
import { TUiModel } from './types';

const { IDLE } = Consts.REQUEST.UI.STATUS;

export const uiModel: TUiModel = {
  status: IDLE,
  details: null,
};
