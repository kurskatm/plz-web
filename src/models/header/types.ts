// Main Types
import { THeaderUxModel } from './ux/types';

export interface TheaderModel {
  ux: THeaderUxModel;
}
