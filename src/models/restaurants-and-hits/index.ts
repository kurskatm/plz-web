// Models
import { uiModel } from '@models/ui';
// Types
import { TRestaurantsAndHitsModel } from './types';

export const restaurantsAndHitsModel: TRestaurantsAndHitsModel = {
  data: null,
  ui: uiModel,
};
