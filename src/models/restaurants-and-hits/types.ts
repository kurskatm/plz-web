// Types
import { TUiModel } from '@models/ui/types';
import { TRestaurantCard } from '@type/restaurants/restaurant-card';
import { TRestaurantProduct } from '@type/restaurants/restaurant-products';

export interface TRestaurantsAndHits {
  products: TRestaurantProduct[];
  restaurants: TRestaurantCard[];
}

export interface TRestaurantsAndHitsModel {
  data: TRestaurantsAndHits;
  ui: TUiModel;
}
