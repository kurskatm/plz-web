export interface TModalDataModel {
  show: boolean;
  doNotShow2ndTime?: boolean;
  actionOnClose?: () => void;
}
