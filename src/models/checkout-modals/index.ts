// Models
import { addressModalModel } from './address';
import { basketModalModel } from './basket';
import { promocodeModalModel } from './promocode';
// Types
import { TCheckoutModalsModel } from './types';

export const checkoutModalsModel: TCheckoutModalsModel = {
  address: addressModalModel,
  basket: basketModalModel,
  promocode: promocodeModalModel,
};
