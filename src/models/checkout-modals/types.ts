// Types
import { TModalDataModel } from './address/types';

export interface TCheckoutModalsModel {
  address: TModalDataModel;
  basket: TModalDataModel;
  promocode: TModalDataModel;
}
