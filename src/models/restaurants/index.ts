// Models
import { uiModel } from '@models/ui';
// Types
import { TRestaurantsModel } from './types';

export const restaurantsModel: TRestaurantsModel = {
  data: [],
  ui: uiModel,
};
