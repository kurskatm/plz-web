// Types
import { TUiModel } from '@models/ui/types';
import { TRestaurantCard } from '@type/restaurants/restaurant-card';

export interface TRestaurantsModel {
  data: TRestaurantCard[];
  ui: TUiModel;
}
