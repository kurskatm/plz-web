// Models
import { uiModel } from '../ui';
// Types
import { TBonusBalanceModel } from './types';

export const bonusBalanceModel: TBonusBalanceModel = {
  data: null,
  ui: uiModel,
};
