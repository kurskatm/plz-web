// Types
import { TUiModel } from '@models/ui/types';

export interface TBonusData {
  balance: string | number;
  earnedBySocialActivity: number[];
}

export interface TBonusBalanceModel {
  data: TBonusData | null;
  ui: TUiModel;
}
export interface TBonusHistoryModel {
  ui: TUiModel;
  data: Record<string, unknown>[];
}

export interface TSocialActivityModel {
  ui: TUiModel;
}

export interface TBonusModel {
  balance: TBonusBalanceModel;
  history: TBonusHistoryModel;
  socialActivity: TSocialActivityModel;
}
