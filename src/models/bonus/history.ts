// Models
import { uiModel } from '../ui';
// Types
import { TBonusHistoryModel } from './types';

export const bonusHistoryModel: TBonusHistoryModel = {
  ui: uiModel,
  data: [],
};
