// Types
import { TBonusModel } from './types';

import { bonusBalanceModel } from './balance';
import { bonusHistoryModel } from './history';
import { socialActivityModel } from './social-activity';

export const bonusModel: TBonusModel = {
  balance: bonusBalanceModel,
  history: bonusHistoryModel,
  socialActivity: socialActivityModel,
};
