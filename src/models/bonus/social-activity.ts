// Models
import { uiModel } from '../ui';
// Types
import { TSocialActivityModel } from './types';

export const socialActivityModel: TSocialActivityModel = {
  ui: uiModel,
};
