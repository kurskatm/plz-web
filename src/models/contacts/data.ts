// Types
import { TContacts } from '@type/contacts';

export const contactsDataModel: TContacts = {
  phoneNumber: null,
  email: null,
  address: null,
  lat: null,
  lng: null,
};
