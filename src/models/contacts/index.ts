// Models
import { uiModel } from '@models/ui';
import { contactsDataModel } from './data';
// Types
import { TContactsModel } from './types';

export const contactsModel: TContactsModel = {
  data: contactsDataModel,
  ui: uiModel,
};

export { contactsDataModel };
