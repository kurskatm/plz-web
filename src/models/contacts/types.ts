// Main Types
import { TContacts } from '@type/contacts';
import { TUiModel } from '@models/ui/types';

export interface TContactsModel {
  data: TContacts | [];
  ui: TUiModel;
}
