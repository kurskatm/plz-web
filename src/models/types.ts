// Types
import { TCitiesModel } from './cities/types';
import { TContactsModel } from './contacts/types';
import { TCuisinesModel } from './cuisines/types';
import { TFiltersModel } from './filters/types';
import { TheaderModel } from './header/types';
import { THotOffersModel } from './hotOffers/types';
import { TProfileModel } from './profile/types';
import { TPartnersModel } from './partners/types';
import { TRestaurantModel } from './restaurant/types';
import { TRestaurantsModel } from './restaurants/types';
import { TRestaurantsAndHitsModel } from './restaurants-and-hits/types';
import { TShopingCartModel } from './shoping-cart/types';
import { TReviewsModel } from './reviews/types';
import { TCheckoutModalsModel } from './checkout-modals/types';
import { TNotificationsModel } from './notifications/types';
import { TOldSiteBannerModel } from './oldSiteBanner/types';
import { TFoodForPointsModel } from './foodForPoints/types';

export interface TReduxState {
  cities: TCitiesModel;
  contacts: TContactsModel;
  cuisines: TCuisinesModel;
  filters: TFiltersModel;
  header: TheaderModel;
  hotOffers: THotOffersModel;
  profile: TProfileModel;
  partners: TPartnersModel;
  restaurant: TRestaurantModel;
  restaurantsAndHits: TRestaurantsAndHitsModel;
  restaurants: TRestaurantsModel;
  shopingCart: TShopingCartModel;
  reviews: TReviewsModel;
  checkoutModals: TCheckoutModalsModel;
  notifications: TNotificationsModel;
  oldSiteBanner: TOldSiteBannerModel;
  foodForPoints: TFoodForPointsModel;
}
