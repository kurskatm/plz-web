// Models
import { TUiModel } from '../ui/types';

export interface TCuisinesItem {
  id: string;
  name: string;
  urlName: string;
  sortOrder: number;
}

export type TCuisinesList = TCuisinesItem[];

export interface TCuisinesModel {
  chosen: string[];
  list: {
    data: TCuisinesList;
    ui: TUiModel;
  };
}
