// Models
import { uiModel } from '../ui';
// Types
import { TCuisinesModel } from './types';

export const cuisinesModel: TCuisinesModel = {
  chosen: [],

  list: {
    data: [],
    ui: uiModel,
  },
};
