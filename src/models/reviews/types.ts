// Main Types
import { TReviewsMainList } from '@type/reviews-main';
import { TUiModel } from '@models/ui/types';

export interface TReviewsModel {
  data: TReviewsMainList;
  ui: TUiModel;
}
