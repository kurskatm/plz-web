// Models
import { uiModel } from '@models/ui';
// Types
import { TReviewsModel } from './types';

export const reviewsModel: TReviewsModel = {
  data: [],
  ui: uiModel,
};
