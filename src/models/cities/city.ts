// Types
import { TCitiesItem } from '@type/cities';

export const cityModelDefault: TCitiesItem = {
  id: null,
  name: null,
  urlName: null,
  whereName: null,
  utcOffset: null,
  center: {
    lat: null,
    lng: null,
  },
};
