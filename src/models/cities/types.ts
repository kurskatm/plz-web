// Types
import { TUiModel } from '@models/ui/types';
import { TCitiesList } from '@type/cities';

export interface TCitiesModel {
  list: {
    data: TCitiesList;
    ui: TUiModel;
  };
  modal: TCitiesModalModel;
}

export interface TCitiesModalModel {
  show: boolean;
}
