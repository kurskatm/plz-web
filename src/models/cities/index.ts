// Models
import { uiModel } from '@models/ui';
import { cityModelDefault } from './city';
// Types
import { TCitiesModel } from './types';

export const citiesModel: TCitiesModel = {
  list: {
    data: [],
    ui: uiModel,
  },
  modal: {
    show: false,
  },
};

export { cityModelDefault };
