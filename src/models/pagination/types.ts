export interface TPaginationObject {
  page: number;
  limit: number;
  hasMore: boolean;
}

export interface TPaginationObjectArgs {
  page?: number;
  limit?: number;
  hasMore?: boolean;
}

export type TGetPaginationModel = (data?: TPaginationObjectArgs) => TPaginationObject;
