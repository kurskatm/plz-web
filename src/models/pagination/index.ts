// Utils
import { Consts } from '@utils';
// Types
import { TPaginationObject, TGetPaginationModel } from './types';

const { SKIP, TAKE, HAS_MORE } = Consts.PAGINATION.DEFAULT;

const DEFAULT_PAGINATION: TPaginationObject = {
  page: SKIP,
  limit: TAKE,
  hasMore: HAS_MORE,
};

export const getPaginationModel: TGetPaginationModel = (data = {}) => ({
  ...DEFAULT_PAGINATION,
  ...data,
});
