// Main Types
import { TUiModel } from '@models/ui/types';

export interface TAuthVerifyPhoneModel {
  data: Record<string, unknown>;
  ui: TUiModel;
}

export interface TAuthVerifyOtpModel {
  data: Record<string, unknown>;
  ui: TUiModel;
}

export interface TAuthVerifyModel {
  verifyOtp: TAuthVerifyOtpModel;
  verifyPhone: TAuthVerifyPhoneModel;
}

export interface TAuthModalModel {
  show: boolean;
  phone: string;
}
