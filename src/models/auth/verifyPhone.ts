// Models
import { uiModel } from '../ui';
// Types
import { TAuthVerifyPhoneModel } from './types';

export const authVerifyPhoneModel: TAuthVerifyPhoneModel = {
  data: {},
  ui: uiModel,
};
