// Models
import { authVerifyOtpModel } from './verifyOtp';
import { authVerifyPhoneModel } from './verifyPhone';

export const authModel = {
  verifyOtp: authVerifyOtpModel,
  verifyPhone: authVerifyPhoneModel,
};
