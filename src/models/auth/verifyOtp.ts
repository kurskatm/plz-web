// Models
import { uiModel } from '../ui';
// Types
import { TAuthVerifyOtpModel } from './types';

export const authVerifyOtpModel: TAuthVerifyOtpModel = {
  data: {},
  ui: uiModel,
};
