// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { contactsFetch } from '@actions/contacts';
import { profileAuthorized } from '@actions/profile';
import { authOtpSend } from '@/actions/auth';

// Selectors
import { loadStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/load';

const mapDispatchToProps = {
  contactsFetch,
  profileAuthorized,
  authOtpSend,
};
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export const enhance = compose(withConnect);
