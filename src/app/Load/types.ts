// Main Types
import { TAuthOtpSend } from '@/actions/auth/action-types';
import { TContactsFetch } from '@actions/contacts/action-types';
import { TProfileInfo } from '@actions/profile/action-types';
import { TProfileModel } from '@models/profile/types';

export interface TLoadProps {
  contactsFetch: TContactsFetch;
  profileAuthorized: TProfileInfo;
  contactsIsIdle: boolean;
  profile: TProfileModel;
  authOtpSend: TAuthOtpSend;
}
