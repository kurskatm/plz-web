// Modules
import {
  FC, memo, useEffect, useRef,
} from 'react';
// Lib
import { setCookieCityData, getProfileData, getRefreshToken } from '@lib/cookie';
// Enhance
import { enhance } from './enhance';
// Types
import { TLoadProps } from './types';

const LoadComponent: FC<TLoadProps> = ({
  contactsFetch,
  contactsIsIdle,
  profileAuthorized,
  authOtpSend,
  profile,
}) => {
  const firstRender = useRef<boolean>(false);

  useEffect(
    () => {
      if (firstRender.current) {
        const data = {
          cityId: profile.city,
        };

        setCookieCityData(profile.city);
        contactsFetch(data);
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [profile.city],
  );

  useEffect(
    () => {
      if (!firstRender.current) {
        if (contactsIsIdle) {
          const data = {
            cityId: profile.city,
          };

          contactsFetch(data);
        }
        // TODO
        // @ts-ignore
        const { access_token: accessToken } = getProfileData();
        const refreshToken = getRefreshToken();

        if (refreshToken && !accessToken) {
          console.log('dawdadw');
          authOtpSend({
            refresh_token: refreshToken as string,
          });
        }
        if (accessToken) {
          // @ts-ignore
          profileAuthorized(getProfileData());
        }

        firstRender.current = true;
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );

  return null;
};

const LoadMemo = memo(LoadComponent);
// @ts-ignore
export default enhance(LoadMemo);
