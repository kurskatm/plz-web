// Modules
import {
  FC, memo, useEffect, useRef,
} from 'react';
import { useHistory } from 'react-router-dom';
import isNil from 'lodash/isNil';
// Lib
import { checkNewCityUrlName } from '@lib/address';
// Enhance
import { enhance } from './enhance';
// Types
import { TCitiesCheckProps } from './types';

const CheckCityComponent: FC<TCitiesCheckProps> = ({
  cityItem,
  cities,
  shopingCartId,
  shopingCartSave,
  profileCityAndAddressUpdate,
}) => {
  const history = useHistory();
  const cityIdRef = useRef<string>(cityItem?.id);

  useEffect(
    () => {
      if (cityItem && cityIdRef.current !== cityItem.id && isNil(history.location.state)) {
        const data = {
          cartId: shopingCartId,
        };
        // @ts-ignore
        shopingCartSave(data);
        cityIdRef.current = cityItem.id;
        // needed for pause before fetch new shoping cart
        setTimeout(() => {
          profileCityAndAddressUpdate();
        }, 100);
        setTimeout(() => {
          checkNewCityUrlName({
            citiesList: cities,
            newCityId: cityItem.id,
            history,
          });
        }, 200);
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [cityItem?.id, cityItem?.urlName, history, cities],
  );

  return null;
};

const CheckCityMemo = memo(CheckCityComponent);
export default enhance(CheckCityMemo);
