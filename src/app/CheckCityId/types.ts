// Main types
import { TCitiesItem, TCitiesList } from '@type/cities';
import { TProfileCityAndAddressUpdate } from '@actions/profile/city/action-types';
import { TShopingCartSave } from '@actions/shoping-cart/save/action-types';

export interface TCitiesCheckProps {
  cityItem: TCitiesItem;
  cities: TCitiesList;
  shopingCartId: string;
  shopingCartSave: TShopingCartSave;
  profileCityAndAddressUpdate: TProfileCityAndAddressUpdate;
}
