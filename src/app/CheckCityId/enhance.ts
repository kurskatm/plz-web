// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { profileCityAndAddressUpdate } from '@actions/profile/city';
import { shopingCartSave } from '@actions/shoping-cart/save';
// Selectors
import { cityCheckStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/cities-list';

const mapDispatchToProps = {
  profileCityAndAddressUpdate,
  shopingCartSave,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);
export const enhance = compose(withConnect);
