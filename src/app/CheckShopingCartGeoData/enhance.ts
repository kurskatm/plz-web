// Modules
import { compose } from 'redux';
import { connect } from 'react-redux';
// Actions
import { shopingCartSave } from '@actions/shoping-cart/save';
// Selectors
import { checkShopingCartGeoStructuredSelector as mapStateToProps } from '@selectors/structured-selectors/shoping-cart';

const mapDispatchToProps = {
  shopingCartSave,
};

const withConnect = connect(mapStateToProps, mapDispatchToProps);
export const enhance = compose(withConnect);
