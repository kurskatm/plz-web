// Modules
import {
  FC, memo, useEffect, useRef,
} from 'react';
import isEqual from 'lodash/isEqual';
import isNil from 'lodash/isNil';
// Enhance
import { enhance } from './enhance';
// Types
import { TShopingCartCheckGeoProps, TNewGeocodeData } from './types';

const CheckShopingCartGeoComponent: FC<TShopingCartCheckGeoProps> = ({
  geocode,
  shopingCart,
  fetchPending,
  savePending,
  shopingCartSave,
}) => {
  const geoRef = useRef<TNewGeocodeData | null>(geocode);

  useEffect(() => {
    if (
      geocode
      && !isEqual(geocode, geoRef.current)
      && geocode.precision === 'exact'
      && shopingCart?.id
      && !isNil(shopingCart?.restaurant?.id)
      && !fetchPending
      && !savePending
    ) {
      const data = {
        cartId: shopingCart.id as string,
        lat: geocode.lat,
        lng: geocode.lng,
        data: {
          cityId: geocode.cityId,
          lat: geocode.lat,
          lng: geocode.lng,
          id: shopingCart.id as string,
          restaurantId: shopingCart.restaurant.id,
          products: shopingCart.products?.map((item) => ({
            id: item.id,
            count: item.count,
            inBonusPoint: item.inBonusPoint,
            selectedModifierGroups: item.modifiers?.map((category) => ({
              id: category.id,
              selectedModifiers: category.modifiers?.map((modifier) => ({
                id: modifier.id,
              })),
            })),
          })),
          cutleryCount: shopingCart.options.cutlery,
        },
      };

      // @ts-ignore
      shopingCartSave(data);
      geoRef.current = geocode;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [geocode?.lat]);

  return null;
};

const CheckShopingCartGeoMemo = memo(CheckShopingCartGeoComponent);
export default enhance(CheckShopingCartGeoMemo);
