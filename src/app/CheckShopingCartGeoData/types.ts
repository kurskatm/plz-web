// Main types
import { TNewGeocodeData } from '@type/new-geocode';
import { shopingCartWithSortProductSelector } from '@selectors/selectors/shoping-cart';
import { TShopingCartSave } from '@actions/shoping-cart/save/action-types';

export interface TShopingCartCheckGeoProps {
  geocode: TNewGeocodeData;
  shopingCart: ReturnType<typeof shopingCartWithSortProductSelector>;
  savePending: boolean;
  fetchPending: boolean;
  shopingCartSave: TShopingCartSave;
}

export { TNewGeocodeData };
