// Types
import { TReduxState } from '@models/types';

export interface TAppProps {
  path?: string;
  reduxData?: TReduxState;
}
