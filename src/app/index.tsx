// Modules
import React, { FC, StrictMode, memo } from 'react';
import { Provider } from 'react-redux';
import { createBrowserHistory } from 'history';
import { LastLocationProvider } from 'react-router-last-location';
import { YMaps } from 'react-yandex-maps';
import TagManager from 'react-gtm-module';
import { YMInitializer } from 'react-yandex-metrika';
import { TalkMeChat } from '@/utils/talkMeChat';

// Store
import { initStore } from '@redux-store';
// QA
import { QaSelectorsProvider } from '@qa/provider';
import qaSelectors from '@qa/qaSelectors';
// Utils
import { Consts } from '@utils';
import { tagManagerArgs, yandexMetriksAcc } from '@utils/analytics/config';
// Layouts
import { Router } from '@layouts';
// Routes
import { Routes, ProviderRouter } from '@routes';
// Components
import Load from './Load';
import CityCheck from './CheckCityId';
import ShopingCartGeoCheck from './CheckShopingCartGeoData';
// Types
import { TAppProps } from './types';

import('dayjs/locale/ru');

const { IS_CLIENT, YMAP_API_KEY } = Consts.ENV;

const history = IS_CLIENT ? createBrowserHistory() : null;

if (process?.env.IS_PRODUCTION && IS_CLIENT && process?.env.API === 'https://api.chibbis.ru') {
  TagManager.initialize(tagManagerArgs);
  TalkMeChat();
}

const AppComponent: FC<TAppProps> = ({ path, reduxData }) => (
  <StrictMode>
    {/* @ts-ignore */}
    <YMInitializer accounts={yandexMetriksAcc} />
    {/* @ts-ignore */}
    <Provider store={initStore(history, reduxData)}>
      <ProviderRouter history={history}>
        <QaSelectorsProvider qaSelectors={qaSelectors}>
          <Router path={path}>
            {/* @ts-ignore */}
            <LastLocationProvider>
              <CityCheck />
              <ShopingCartGeoCheck />
              <Load />
              {/* @ts-ignore */}
              <YMaps
                query={{
                  lang: 'ru_RU',
                  apikey: YMAP_API_KEY,
                }}
              >
                <Routes />
              </YMaps>
            </LastLocationProvider>
          </Router>
        </QaSelectorsProvider>
      </ProviderRouter>
    </Provider>
  </StrictMode>
);

export default memo(AppComponent);
