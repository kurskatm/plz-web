// Modules
import { createStructuredSelector } from 'reselect';
// Selectors
import {
  supportTopicsDataSelector,
  supportTopicsSelectorStatusIsIdle,
  supportTopicsSelectorStatusIsError,
  supporMessageStatusSelector,
  supporMessageStatusIsErrorSelector,
  supporMessageStatusIsSuccessSelector,
} from '../../selectors/support';

export const supportStructuredSelector = createStructuredSelector({
  messageSendStatus: supporMessageStatusSelector,
  messageSendIsSuccess: supporMessageStatusIsSuccessSelector,
  messageSendIsError: supporMessageStatusIsErrorSelector,
  topics: supportTopicsDataSelector,
  topicsStatusIsIdle: supportTopicsSelectorStatusIsIdle,
  topicsStatusIsError: supportTopicsSelectorStatusIsError,
});
