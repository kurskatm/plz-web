// Modules
import { createStructuredSelector } from 'reselect';
// Selectors
import { showShoppingCardSelector } from '@selectors/selectors/restaurant/modal';
import { getCity } from '@/selectors/selectors/cities';
import { profileSelector } from '@/selectors/selectors/profile';
import { headerUxHeightSelector } from '@/selectors/selectors/header';
import { cuisinesDataSelector, cuisinesDataUiIsIdleSelector } from '@/selectors/selectors/cuisines';
import {
  shopingCartSumCoastSelector,
  shopingCartOptionsSelector,
  shopingCartForPointsCostSelector,
  shopingCartRestaurantDataSelector,
} from '@/selectors/selectors/shoping-cart';
import { bonusBalanceDataSelector } from '@/selectors/selectors/bonus';
import { currentNewAddressSelector } from '@selectors/selectors/profile/new-address';
import { newGeoCodeDataSelector } from '@selectors/selectors/profile/new-geo-code';

export const headerStructuredSelector = createStructuredSelector({
  initialAddress: currentNewAddressSelector,
  geocode: newGeoCodeDataSelector,
  cost: shopingCartSumCoastSelector,
  costForPoints: shopingCartForPointsCostSelector,
  city: getCity,
  headerHeight: headerUxHeightSelector,
});

export const headerCartStructuredSelector = createStructuredSelector({
  shopingCartOptions: shopingCartOptionsSelector,
  shopingCartRestaurant: shopingCartRestaurantDataSelector,
});

export const headerLogoStructuredSelector = createStructuredSelector({
  city: getCity,
});

export const headerGoBackStructuredSelector = createStructuredSelector({
  city: getCity,
});

export const headerProfileStructuredSelector = createStructuredSelector({
  profile: profileSelector,
});

export const headerBurgerProfileStructuredSelector = createStructuredSelector({
  profile: profileSelector,
  city: getCity,
  balance: bonusBalanceDataSelector,
});

export const headerAddressStructuredSelector = createStructuredSelector({
  profile: profileSelector,
});

export const headerInputAddressStructuredSelector = createStructuredSelector({
  initialAddress: currentNewAddressSelector,
  city: getCity,
});

export const headerMenuMiddleStructuredSelector = createStructuredSelector({
  city: getCity,
  cuisines: cuisinesDataSelector,
  cuisinesIsIdle: cuisinesDataUiIsIdleSelector,
});

export const checkoutAddressStructuredSelector = createStructuredSelector({
  initialAddress: currentNewAddressSelector,
});

export const showShoppingCardStructuredSelector = createStructuredSelector({
  showModal: showShoppingCardSelector,
});
