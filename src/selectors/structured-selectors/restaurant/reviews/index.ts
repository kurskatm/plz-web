// Modules
import { createStructuredSelector } from 'reselect';
// Selectors
import {
  profileLastOrderDataSelector,
  profileLastOrderDataStatusIsIdle,
  profileOrderReviewsListSelector,
  profileOrderReviewStatusIsSuccess,
  profileSelector,
} from '@/selectors/selectors/profile';
import { restaurantMainInfoDataSelector } from '@selectors/selectors/restaurant/main/info';
import {
  restaurantReviewsCurrentDataSelector,
  restaurantReviewsCurrentPaginationSelector,
  restaurantReviewsCurrentUiStatusIsIdleSelector,
  restaurantReviewsCurrentUiStatusIsSuccessSelector,
  restaurantReviewsCurrentUiStatusIsPendingSelector,
  restaurantReviewsCurrentUiStatusIsErrorSelector,
} from '@selectors/selectors/restaurant/reviews/current';
import { restaurantMainIdDataSelector } from '@/selectors/selectors/restaurant/main/id';

export const restaurantReviewsStructuredSelector = createStructuredSelector({
  restaurantCard: restaurantMainInfoDataSelector,
  reviews: restaurantReviewsCurrentDataSelector,
  reviewsPagination: restaurantReviewsCurrentPaginationSelector,
  reviewsIsIdle: restaurantReviewsCurrentUiStatusIsIdleSelector,
  reviewsIsPending: restaurantReviewsCurrentUiStatusIsPendingSelector,
  reviewsIsSuccess: restaurantReviewsCurrentUiStatusIsSuccessSelector,
  reviewsIsError: restaurantReviewsCurrentUiStatusIsErrorSelector,
});

export const restaurantLastOrderWithoutReviewModalSelector = createStructuredSelector({
  order: profileLastOrderDataSelector,
});

export const restaurantLastOrderWithoutReviewStructuredSelector = createStructuredSelector({
  order: profileLastOrderDataSelector,
  orderIsIdle: profileLastOrderDataStatusIsIdle,
  profile: profileSelector,
  restaurantId: restaurantMainIdDataSelector,
  reviewAddList: profileOrderReviewsListSelector,
  reviewAddIsSuccess: profileOrderReviewStatusIsSuccess,
});
