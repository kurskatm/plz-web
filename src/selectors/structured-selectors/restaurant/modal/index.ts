// Modules
import { createStructuredSelector } from 'reselect';
// Selectors
import {
  restaurantCloaseModalShowSelector,
  restaurantModalRestsLinksSelector,
  restaurantModalCityLinkSelector,
  restaurantOtherModalsShowSelector,
  restaurantOtherModalTypeSelector,
  restaurantOtherModalsBonusPointsSelector,
  restaurantOtherModalsDataToUpdateCartSelector,
  restaurantModalProductListSelector,
  showModifiersModalSelector,
  currentProductModifiersModalSelector,
  restaurantOtherModalsLinkSelector,
  promoCardShowModalSelector,
  currentPromoCardSelector,
  currentPromoCardIndexSelector,
} from '@selectors/selectors/restaurant/modal';
import { shopingCartFetchUiStatusIsPendingSelector } from '@/selectors/selectors/shoping-cart/fetch';
import { profileAuthSelector } from '@/selectors/selectors/profile';
import { citiesDataSelector, getCity, getCityId } from '@/selectors/selectors/cities';
import {
  shopingCartSelector,
  shopingCartRestaurantDataSelector,
} from '@/selectors/selectors/shoping-cart';
import { bonusBalanceDataSelector } from '@/selectors/selectors/bonus';
import {
  currentNewAddressSelector,
  newAddressSuggestDataSelector,
  newAddressSuggestIsSuccessSelector,
} from '@/selectors/selectors/profile/new-address';
import {
  newGeoCodeDataSelector,
  newGeoCodeIsPendingSelector,
} from '@/selectors/selectors/profile/new-geo-code';

export const restaurantModalShowStructuredSelector = createStructuredSelector({
  modalShow: restaurantCloaseModalShowSelector,
});

export const restaurantModalBodyStructuredSelector = createStructuredSelector({
  rests: restaurantModalRestsLinksSelector,
  city: restaurantModalCityLinkSelector,
});

export const restaurantOtherModalsShowStructuredSelector = createStructuredSelector({
  showModal: restaurantOtherModalsShowSelector,
  modalType: restaurantOtherModalTypeSelector,
  restBonusPoints: restaurantOtherModalsBonusPointsSelector,
  dataToUpdateCart: restaurantOtherModalsDataToUpdateCartSelector,
  link: restaurantOtherModalsLinkSelector,
});

export const anotherRestaurantModalBodyStructuredSelector = createStructuredSelector({
  shoppingCartFetchPending: shopingCartFetchUiStatusIsPendingSelector,
  shopingCartRest: shopingCartRestaurantDataSelector,
});

export const anotherPointProductModalBodyStructuredSelector = createStructuredSelector({
  shoppingCartFetchPending: shopingCartFetchUiStatusIsPendingSelector,
  productsList: restaurantModalProductListSelector,
});

export const changeAddressInputModalStructuredSelector = createStructuredSelector({
  initialAddress: currentNewAddressSelector,
  geocode: newGeoCodeDataSelector,
  list: newAddressSuggestDataSelector,
  listIsSuccess: newAddressSuggestIsSuccessSelector,
  geocodeIsPending: newGeoCodeIsPendingSelector,
  cityId: getCityId,
  citiesList: citiesDataSelector,
});

export const modifiersModalStructuredSelector = createStructuredSelector({
  showModal: showModifiersModalSelector,
  showOtherRestaurantModals: restaurantOtherModalsShowSelector,
  product: currentProductModifiersModalSelector,
});

export const modifiersModalBodyStructuredSelector = createStructuredSelector({
  city: getCity,
  shopingCart: shopingCartSelector,
  initialAddress: currentNewAddressSelector,
  geocode: newGeoCodeDataSelector,
  balance: bonusBalanceDataSelector,
  auth: profileAuthSelector,
});

export const promoModalStructuredSelector = createStructuredSelector({
  showModal: promoCardShowModalSelector,
  promo: currentPromoCardSelector,
  index: currentPromoCardIndexSelector,
});
