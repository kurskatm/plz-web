// Modules
import { createStructuredSelector } from 'reselect';
// Selectors
import { profileSelector } from '@/selectors/selectors/profile';
import { bonusBalanceDataSelector } from '@/selectors/selectors/bonus';
import { restaurantMainInfoDataSelector } from '@selectors/selectors/restaurant/main/info';
import {
  restaurantMenuExtendCategoriesDataSelector,
  restaurantMenuCategoriesUiStatusIsIdleSelector,
  restaurantMenuCategoriesUiStatusIsSuccessSelector,
  restaurantMenuCategoriesUiStatusIsErrorSelector,
} from '@selectors/selectors/restaurant/menu/categories';
import {
  restaurantMenuProductsCategorySelector,
  restaurantMenuProductsUiStatusIsIdleSelector,
  restaurantMenuProductsUiStatusIsSuccessSelector,
} from '@selectors/selectors/restaurant/menu/products';
import {
  restaurantMenuPromoDataSelector,
  restaurantMenuPromoUiStatusIsIdleSelector,
  restaurantMenuPromoUiStatusIsSuccessSelector,
  restaurantMenuPromoUiStatusIsErrorSelector,
} from '@selectors/selectors/restaurant/menu/promo';
import { headerUxHeightSelector } from '@selectors/selectors/header';
import { shopingCartSelector } from '@selectors/selectors/shoping-cart';
import {
  searchProductsDataSelector,
  searchProductsIsSuccessSelector,
  searchProductsIsPendingSelector,
  searchProductsIsErrorSelector,
  restaurantSearchStringSelector,
} from '@selectors/selectors/restaurant/search-products';
import { currentNewAddressSelector } from '@selectors/selectors/profile/new-address';
import { getCity } from '@/selectors/selectors/cities';

export const restaurantMenuStructuredSelector = createStructuredSelector({
  categories: restaurantMenuExtendCategoriesDataSelector,
  categoriesIsSuccess: restaurantMenuCategoriesUiStatusIsSuccessSelector,
  headerHeight: headerUxHeightSelector,
  restaurantCard: restaurantMainInfoDataSelector,
  searchStr: restaurantSearchStringSelector,
  searchProductsIsPending: searchProductsIsPendingSelector,
  searchProductsIsSuccess: searchProductsIsSuccessSelector,
  searchProductsIsError: searchProductsIsErrorSelector,
});

export const restaurantMenuPromoSliderStructuredSelector = createStructuredSelector({
  promos: restaurantMenuPromoDataSelector,
  promosIsIdle: restaurantMenuPromoUiStatusIsIdleSelector,
  promosIsSuccess: restaurantMenuPromoUiStatusIsSuccessSelector,
  promosIsError: restaurantMenuPromoUiStatusIsErrorSelector,
  restaurantCard: restaurantMainInfoDataSelector,
});

export const restaurantMenuCategoriesStructuredSelector = createStructuredSelector({
  categories: restaurantMenuExtendCategoriesDataSelector,
  categoriesIsIdle: restaurantMenuCategoriesUiStatusIsIdleSelector,
  categoriesIsSuccess: restaurantMenuCategoriesUiStatusIsSuccessSelector,
  categoriesIsError: restaurantMenuCategoriesUiStatusIsErrorSelector,
  headerHeight: headerUxHeightSelector,
  restaurantCard: restaurantMainInfoDataSelector,
});

export const restaurantMenuCategoriesListStructuredSelector = createStructuredSelector({
  categories: restaurantMenuExtendCategoriesDataSelector,
  categoriesIsIdle: restaurantMenuCategoriesUiStatusIsIdleSelector,
  categoriesIsSuccess: restaurantMenuCategoriesUiStatusIsSuccessSelector,
  productsIsIdle: restaurantMenuProductsUiStatusIsIdleSelector,
  productsIsSuccess: restaurantMenuProductsUiStatusIsSuccessSelector,
  restaurantCard: restaurantMainInfoDataSelector,
  searchProducts: searchProductsDataSelector,
  searchProductsIsSuccess: searchProductsIsSuccessSelector,
});

export const restaurantMenuProductsStructuredSelector = createStructuredSelector({
  categoryProducts: restaurantMenuProductsCategorySelector,
  headerHeight: headerUxHeightSelector,
  shopingCart: shopingCartSelector,
  city: getCity,
  profile: profileSelector,
  balance: bonusBalanceDataSelector,
  address: currentNewAddressSelector,
});

export const restaurantProductsStructuredSelector = createStructuredSelector({
  profile: profileSelector,
});
