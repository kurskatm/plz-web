// Modules
import { createStructuredSelector } from 'reselect';
// Selectors
import { getCity } from '@selectors/selectors/cities';
import { restaurantMainInfoDataSelector } from '@selectors/selectors/restaurant/main/info';
import {
  restaurantInfoDataSelector,
  restaurantInfoWeekScheduleObjectSelector,
  restaurantInfoUiStatusIsIdleSelector,
  restaurantInfoUiStatusIsSuccessSelector,
  restaurantInfoUiStatusIsErrorSelector,
} from '@selectors/selectors/restaurant/info/info';
import {
  restaurantInfoLegalDataSelector,
  restaurantInfoLegalUiStatusIsIdleSelector,
  restaurantInfoLegalUiStatusIsSuccessSelector,
  restaurantInfoLegalUiStatusIsErrorSelector,
} from '@selectors/selectors/restaurant/info/legal';
import {
  addressSuggestDataSelector,
  geocodeSuggestDataSelector,
  geocodeSuggestIsSuccessSelector,
  deliveryAreasDataSelector,
  deliveryAreasIsSuccessSelector,
} from '@selectors/selectors/restaurant/info/delivery-areas';
import { currentNewAddressSelector } from '@selectors/selectors/profile/new-address';
import { newGeoCodeDataSelector } from '@selectors/selectors/profile/new-geo-code';

export const restaurantInfoStructuredSelector = createStructuredSelector({
  restaurantCard: restaurantMainInfoDataSelector,
  restaurantInfo: restaurantInfoDataSelector,
  restaurantInfoIsIdle: restaurantInfoUiStatusIsIdleSelector,
  restaurantInfoIsSuccess: restaurantInfoUiStatusIsSuccessSelector,
  restaurantInfoIsError: restaurantInfoUiStatusIsErrorSelector,
});

export const restaurantInfoWeekScheduleStructuredSelector = createStructuredSelector({
  weekSchedule: restaurantInfoWeekScheduleObjectSelector,
});

export const restaurantInfoLegalStructuredSelector = createStructuredSelector({
  legal: restaurantInfoLegalDataSelector,
  legalIsIdle: restaurantInfoLegalUiStatusIsIdleSelector,
  legalIsSuccess: restaurantInfoLegalUiStatusIsSuccessSelector,
  legalIsError: restaurantInfoLegalUiStatusIsErrorSelector,
});

export const restaurantInfoSpecializationsStructuredSelector = createStructuredSelector({
  city: getCity,
});

export const restaurantDeliveryAreaStructuredSelector = createStructuredSelector({
  initialAddress: currentNewAddressSelector,
  initialGeocode: newGeoCodeDataSelector,
  city: getCity,
  restaurantCard: restaurantMainInfoDataSelector,
  addressList: addressSuggestDataSelector,
  geocode: geocodeSuggestDataSelector,
  geocodeSuggestIsSuccess: geocodeSuggestIsSuccessSelector,
  deliveryAreasList: deliveryAreasDataSelector,
  deliveryAreasIsSuccess: deliveryAreasIsSuccessSelector,
});
