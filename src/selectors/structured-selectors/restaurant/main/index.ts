// Modules
import { createStructuredSelector } from 'reselect';
// Selectors
import { getCityId } from '@selectors/selectors/cities';
import {
  restaurantMainIdDataSelector,
  restaurantMainIdUiStatusIsIdleSelector,
  restaurantMainIdUiStatusIsPendingSelector,
  restaurantMainIdUiStatusIsSuccessSelector,
} from '@selectors/selectors/restaurant/main/id';
import {
  restaurantMainInfoDataSelector,
  restaurantMainInfoUiStatusIsIdleSelector,
  restaurantMainInfoUiStatusIsSuccessSelector,
  restaurantMainInfoUiStatusIsErrorSelector,
} from '@selectors/selectors/restaurant/main/info';
import { profileSelector } from '@selectors/selectors/profile';
import { shopingCartSaveIsErrorSelector } from '@selectors/selectors/shoping-cart';
import { showShoppingCardSelector } from '@selectors/selectors/restaurant/modal';
import { isBannerFullShowSelector } from '@selectors/selectors/oldSiteBanner';

export const restaurantMainStructuredSelector = createStructuredSelector({
  cityId: getCityId,
  profile: profileSelector,
  restaurantCardId: restaurantMainIdDataSelector,
  restaurantCardIdIsIdle: restaurantMainIdUiStatusIsIdleSelector,
  restaurantCardIdIsPending: restaurantMainIdUiStatusIsPendingSelector,
  restaurantCardIdIsSuccess: restaurantMainIdUiStatusIsSuccessSelector,
  restaurantCard: restaurantMainInfoDataSelector,
  restaurantCardIsIdle: restaurantMainInfoUiStatusIsIdleSelector,
  restaurantCardIsSuccess: restaurantMainInfoUiStatusIsSuccessSelector,
  restaurantCardIsError: restaurantMainInfoUiStatusIsErrorSelector,
  shopingCartSaveError: shopingCartSaveIsErrorSelector,
  showModal: showShoppingCardSelector,
  isOldSiteBannerFull: isBannerFullShowSelector,
});
