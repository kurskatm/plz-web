// Modules
import { createStructuredSelector } from 'reselect';
// Selectors
import { getCity } from '@/selectors/selectors/cities';

export const agreementStructuredSelector = createStructuredSelector({
  city: getCity,
});
