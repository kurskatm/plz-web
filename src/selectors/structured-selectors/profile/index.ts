// Modules
import { createStructuredSelector } from 'reselect';
// Selectors
import { getCityId, getCity } from '@selectors/selectors/cities';
import {
  // Auth
  profileAuthSelector,
  // Info
  profileInfoDataSelector,
  profileInfoDataUiIsIdleSelector,
  profileInfoDataUiIsErrorSelector,
  profileInfoUpdateIsSuccessSelector,
  profileInfoUpdateIsErrorSelector,
  profileInfoUpdateIsPendingSelector,
  profileInfoUpdateDetailsSelector,
  // User
  profileUserIdSelector,
  // Favourites
  profileFavoriteRestsSelector,
  profileFavoriteProductsSelector,
  profileFavoritesIsSuccessSelector,
  profileFavoritesIsErrorSelector,
  profileFavoritesIsPendingSelector,
  // Reviews
  profileReviewsDataSelector,
  profileReviewsDataUiIsIdleSelector,
  profileReviewsLengthSelector,
  profilePaginationDataSelector,
  profileReviewsDataUiIsSuccessSelector,
  profileReviewsDataUiIsErrorSelector,
  profileReviewsDataUiIsPendingSelector,
  // Friends
  profileFriendsIsIdleSelector,
  profileFriendsIsErrorSelector,
  profileFriendsDataSelector,
  // Order modal
  profileOrdersModalDataSelector,
  profileOrderModalCurrentOrderSelector,
  profileOrderReviewStatusIsSuccess,
  profileOrderReviewStatusIsError,
  // Last order
  profileLastOrderDataStatusIsSuccess,
  profileLastOrderDataStatusIsPending,
  profileLastOrderDataSelector,
  profileOrderReviewsListSelector,
  profileLastOrderDataStatusIsError,
  // Current user's order
  profileUserOrderDataSelector,
  profileUserOrderIsPending,
  profileUserOrderIsSuccess,
  profileUserOrderIsError,
  // Profile
  profileSelector,
  // Contact me
  contactMeIsPendingSelector,
  contactMeIsSuccessSelector,
  contactMeIsErrorSelector,
  profileOrderContactMeOrderIdSelector,
  profileOrderContactMeStatusDetailsSelector,
  // Repeat order
  repeatOrderIsPendingSelector,
  repeatOrderIsErrorSelector,
  profileOrderIdToRepeatSelector,
  profileRepeatOrderDataSelector,
  profileRepeatOrderRedirectSelector,
  // New orders
  newOrdersDataSelector,
  newOrdersPaginationSelector,
  newOrdersAfterSelector,
  newOrdersIsIdleSelector,
  newOrdersIsPendingSelector,
  newOrdersIsSuccessSelector,
  newOrdersIsErrorSelector,
} from '@selectors/selectors/profile';
import { restaurantAndHitsUiStatusIsIdleSelector } from '@/selectors/selectors/restaurants-and-hits';

import {
  bonusBalanceDataSelector,
  bonusBalanceStatusSelector,
  bonusBalanceStatusIsErrorSelector,
  bonusHistoryDataSelector,
  bonusHistoryStatusSelector,
  bonusHistoryStatusIsErrorSelector,
  bonusSocialsSelector,
} from '@selectors/selectors/bonus';
import {
  shopingCartSelector,
  shopingCartSaveIsErrorSelector,
} from '@selectors/selectors/shoping-cart';
import { filtersSearchSelector, filtersChosenSelector } from '@selectors/selectors/filters';
import { cuisinesChosenSelector } from '@selectors/selectors/cuisines';
import { newGeoCodeDataSelector } from '@selectors/selectors/profile/new-geo-code';

import {
  foodForPointsDataSelector,
  foodForPointsDataUiIsIdleSelector,
  foodForPointsDataUiIsErrorSelector,
  foodForPointsDataUiIsPendingSelector,
} from '../../selectors/foodForPoints';

export const profileFavoriteSelector = createStructuredSelector({
  profile: profileSelector,
  cityId: getCityId,
  city: getCity,
  accessToken: profileAuthSelector,
  userId: profileUserIdSelector,
  rests: profileFavoriteRestsSelector,
  products: profileFavoriteProductsSelector,
  hitsAndRestsUiIsIdle: restaurantAndHitsUiStatusIsIdleSelector,
  shopingCart: shopingCartSelector,
  shopingCartSaveError: shopingCartSaveIsErrorSelector,
  info: profileInfoDataSelector,
  favoritesIsPending: profileFavoritesIsPendingSelector,
  favoritesIsSuccess: profileFavoritesIsSuccessSelector,
  favoritesIsError: profileFavoritesIsErrorSelector,
});

export const profileScoresSelector = createStructuredSelector({
  city: getCity,
  cityId: getCityId,

  rests: profileFavoriteRestsSelector,
  balance: bonusBalanceDataSelector,
  balanceStatus: bonusBalanceStatusSelector,
  balanceStatusIsError: bonusBalanceStatusIsErrorSelector,
  bonusHistory: bonusHistoryDataSelector,
  historyStatus: bonusHistoryStatusSelector,
  historyStatusIsError: bonusHistoryStatusIsErrorSelector,
  accessToken: profileAuthSelector,
  socials: bonusSocialsSelector,
  info: profileInfoDataSelector,
  search: filtersSearchSelector,
  selectedCuisines: cuisinesChosenSelector,
  selectedFilters: filtersChosenSelector,
  profile: profileSelector,
  shopingCartSaveError: shopingCartSaveIsErrorSelector,
  foodForPoints: foodForPointsDataSelector,
  foodForPointsisIdle: foodForPointsDataUiIsIdleSelector,
  foodForPointsisError: foodForPointsDataUiIsErrorSelector,
  foodForPointsisPending: foodForPointsDataUiIsPendingSelector,
});

export const profileScoresSpendSelector = createStructuredSelector({
  profile: profileSelector,
  shopingCart: shopingCartSelector,
});

export const profileInfoSelector = createStructuredSelector({
  info: profileInfoDataSelector,
  updateInfoIsSuccess: profileInfoUpdateIsSuccessSelector,
  updateInfoIsError: profileInfoUpdateIsErrorSelector,
  updateInfoIsPending: profileInfoUpdateIsPendingSelector,
  updateInfoDetails: profileInfoUpdateDetailsSelector,
  accessToken: profileAuthSelector,
  balance: bonusBalanceDataSelector,
  balanceStatus: bonusBalanceStatusSelector,
  balanceStatusIsError: bonusBalanceStatusIsErrorSelector,
  bonusHistory: bonusHistoryDataSelector,
  historyStatus: bonusHistoryStatusSelector,
  historyStatusIsError: bonusHistoryStatusIsErrorSelector,
  infoStatusIsIdle: profileInfoDataUiIsIdleSelector,
  infoStatusIsError: profileInfoDataUiIsErrorSelector,
  friendsStatusIsIdle: profileFriendsIsIdleSelector,
  friendsStatusIsError: profileFriendsIsErrorSelector,
  friends: profileFriendsDataSelector,
  city: getCity,
});

export const profileEditEmailSelector = createStructuredSelector({
  info: profileInfoDataSelector,
  accessToken: profileAuthSelector,
});

export const profileReviewsSelector = createStructuredSelector({
  reviews: profileReviewsDataSelector,
  reviewsLength: profileReviewsLengthSelector,
  pagination: profilePaginationDataSelector,
  accessToken: profileAuthSelector,
  reviewsIsIdle: profileReviewsDataUiIsIdleSelector,
  reviewsIsPending: profileReviewsDataUiIsPendingSelector,
  reviewsIsSuccess: profileReviewsDataUiIsSuccessSelector,
  reviewsIsError: profileReviewsDataUiIsErrorSelector,
  info: profileInfoDataSelector,
});

export const profileOrdersSelector = createStructuredSelector({
  city: getCity,
  accessToken: profileAuthSelector,
  repeatOrderData: profileRepeatOrderDataSelector,
  repeatOrderNeedRedirect: profileRepeatOrderRedirectSelector,
  repeatOrderIsError: repeatOrderIsErrorSelector,
  newOrdersData: newOrdersDataSelector,
  newOrdersPagination: newOrdersPaginationSelector,
  newOrdersAfter: newOrdersAfterSelector,
  newOrdersIsIdle: newOrdersIsIdleSelector,
  newOrdersIsPending: newOrdersIsPendingSelector,
  newOrdersIsSuccess: newOrdersIsSuccessSelector,
  newOrdersIsError: newOrdersIsErrorSelector,
  contactMeOrderId: profileOrderContactMeOrderIdSelector,
});

export const profileOrderReviewModalSelector = createStructuredSelector({
  showModal: profileOrdersModalDataSelector,
});

export const profileOrderReviewDataSelector = createStructuredSelector({
  order: profileOrderModalCurrentOrderSelector,
});

export const profileSetOrderReviewSelector = createStructuredSelector({
  setOrderReviewIsSuccess: profileOrderReviewStatusIsSuccess,
  setOrderReviewIsError: profileOrderReviewStatusIsError,
});

export const profileLastOrderWithoutReview = createStructuredSelector({
  lastOrder: profileLastOrderDataSelector,
  addReviewList: profileOrderReviewsListSelector,
  lastOrderIsPending: profileLastOrderDataStatusIsPending,
  lastOrderIsSuccess: profileLastOrderDataStatusIsSuccess,
  lastOrderIsError: profileLastOrderDataStatusIsError,
});

export const profileUserOrder = createStructuredSelector({
  userOrder: profileUserOrderDataSelector,
  userOrderIsPending: profileUserOrderIsPending,
  userOrderIsSuccess: profileUserOrderIsSuccess,
  userOrderIsError: profileUserOrderIsError,
  contactMeIsPending: contactMeIsPendingSelector,
  contactMeIsError: contactMeIsErrorSelector,
  contactMeIsSuccess: contactMeIsSuccessSelector,
  contactMeOrderId: profileOrderContactMeOrderIdSelector,
  city: getCity,
  contactMeStatusDetails: profileOrderContactMeStatusDetailsSelector,
});

export const profileOrderFullComponentFooter = createStructuredSelector({
  contactMeIsPending: contactMeIsPendingSelector,
  contactMeIsError: contactMeIsErrorSelector,
  contactMeOrderId: profileOrderContactMeOrderIdSelector,
  contactMeStatusDetails: profileOrderContactMeStatusDetailsSelector,
  repeatOrderIsPending: repeatOrderIsPendingSelector,
  repeatOrderIsError: repeatOrderIsErrorSelector,
  contactMeIsSuccess: contactMeIsSuccessSelector,
  geocode: newGeoCodeDataSelector,
  repeatOrderId: profileOrderIdToRepeatSelector,
  shopingCart: shopingCartSelector,
  cityId: getCityId,
});
