// Modules
import { createStructuredSelector } from 'reselect';
// Selectors
import {
  authVerifyPhoneStatusIsIdleSelector,
  authResendTokenSelector,
  authVerifyPhoneStatusSelector,
  authVerifyPhoneStatusDetails,
  authVerifyOtpStatusSelector,
  authAccessTokenSelector,
  authModalShowSelector,
  authModalPhoneSelector,
} from '@selectors/selectors/auth';
import { getCity } from '@/selectors/selectors/cities';
import { profileAuthSelector } from '@/selectors/selectors/profile';

export const authStructuredSelector = createStructuredSelector({
  authVerifyPhoneStatusIsIdle: authVerifyPhoneStatusIsIdleSelector,
  resendToken: authResendTokenSelector,
  phoneRequestStatus: authVerifyPhoneStatusSelector,
  phoneRequestDetails: authVerifyPhoneStatusDetails,
  otpRequestStatus: authVerifyOtpStatusSelector,
  accessToken: authAccessTokenSelector,
  modalShow: authModalShowSelector,
  modalPhone: authModalPhoneSelector,
  city: getCity,
  auth: profileAuthSelector,
});
