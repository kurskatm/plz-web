// Modules
import { createStructuredSelector } from 'reselect';
// Selectors
import { getCity, getCityId } from '@selectors/selectors/cities';
import {
  cuisinesDataUiIsIdleSelector,
  cuisinesDataUiIsSuccessSelector,
  cuisinesDataUiIsErrorSelector,
  cuisinesDataSelector,
  cuisinesChosenSelector,
  cuisinesDataUiIsPendingSelector,
} from '@selectors/selectors/cuisines';
import {
  filtersDataSelector,
  filtersChosenSelector,
  filtersSearchSelector,
  filtersDataUiIsIdleSelector,
  filtersDataUiIsSuccessSelector,
  filtersShowModalSelector,
  pointsFoodFilterSelector,
} from '@selectors/selectors/filters';
import {
  restaurantAndHitsDataSelector,
  restaurantAndHitsUiStatusIsIdleSelector,
  restaurantAndHitsUiStatusIsPendingSelector,
  restaurantAndHitsUiStatusIsSuccessSelector,
  getRestaurantById,
} from '@selectors/selectors/restaurants-and-hits';
import {
  hotOffersDataSelector,
  hotOffersDataUiIsIdleSelector,
  hotOffersDataUiIsPendingSelector,
  hotOffersDataUiIsSuccessSelector,
  hotOffersDataUiIsErrorSelector,
} from '@selectors/selectors/hotOffers';
import {
  profileAuthSelector,
  profileSelector,
  profileUserIdSelector,
} from '@selectors/selectors/profile';
import {
  shopingCartSelector,
  shopingCartWithNoApiStatusesSelector,
  shopingCartSaveIsErrorSelector,
} from '@selectors/selectors/shoping-cart';
import { bonusBalanceDataSelector } from '@selectors/selectors/bonus';
import { newGeoCodeDataSelector } from '@/selectors/selectors/profile/new-geo-code';
import { currentNewAddressSelector } from '@selectors/selectors/profile/new-address';

export const homeCuisinesListStructuredSelector = createStructuredSelector({
  cityId: getCityId,
  cuisinesChosen: cuisinesChosenSelector,
  cuisinesList: cuisinesDataSelector,
  cuisinesUiIsIdle: cuisinesDataUiIsIdleSelector,
  cuisinesUiIsPending: cuisinesDataUiIsPendingSelector,
  cuisinesUiIsSuccess: cuisinesDataUiIsSuccessSelector,
  cuisinesUiIsError: cuisinesDataUiIsErrorSelector,
  filters: filtersDataSelector,
  filtersIsIdle: filtersDataUiIsIdleSelector,
  filtersIsSuccess: filtersDataUiIsSuccessSelector,
  search: filtersSearchSelector,
});

export const homeFiltersStructuredSelector = createStructuredSelector({
  selectedFilters: filtersChosenSelector,
  search: filtersSearchSelector,
});

export const homeFiltersModalStructuredSelector = createStructuredSelector({
  showModal: filtersShowModalSelector,
});

export const homeRestaurantsListStructuredSelector = createStructuredSelector({
  city: getCity,
  selectedCuisines: cuisinesChosenSelector,
  selectedFilters: filtersChosenSelector,
  search: filtersSearchSelector,
  restaurantsAndHits: restaurantAndHitsDataSelector,
  restaurantsAndHitsIsIdle: restaurantAndHitsUiStatusIsIdleSelector,
  restaurantsAndHitsIsPending: restaurantAndHitsUiStatusIsPendingSelector,
  restaurantsAndHitsIsSuccess: restaurantAndHitsUiStatusIsSuccessSelector,
  cuisinesList: cuisinesDataSelector,
  filtersList: filtersDataSelector,
  geocode: newGeoCodeDataSelector,
  auth: profileAuthSelector,
  userId: profileUserIdSelector,
});

export const homeHotOffersListStructuredSelector = createStructuredSelector({
  cityId: getCityId,
  city: getCity,
  hotOffersList: hotOffersDataSelector,
  restaurantsAndHits: restaurantAndHitsDataSelector,
  hotOffersUiIsIdle: hotOffersDataUiIsIdleSelector,
  hotOffersUiIsPending: hotOffersDataUiIsPendingSelector,
  hotOffersUiIsSuccess: hotOffersDataUiIsSuccessSelector,
  hotOffersUiIsError: hotOffersDataUiIsErrorSelector,
});

export const homeHotOfferStructuredSelector = createStructuredSelector({
  restaurant: getRestaurantById,
});

export const homeHitsListStructuredSelector = createStructuredSelector({
  city: getCity,
  profile: profileSelector,
  restaurantsAndHits: restaurantAndHitsDataSelector,
  restaurantsAndHitsIsIdle: restaurantAndHitsUiStatusIsIdleSelector,
  restaurantsAndHitsIsPending: restaurantAndHitsUiStatusIsPendingSelector,
  restaurantsAndHitsIsSuccess: restaurantAndHitsUiStatusIsSuccessSelector,
  selectedCuisines: cuisinesChosenSelector,
  selectedFilters: filtersChosenSelector,
  search: filtersSearchSelector,
  shopingCart: shopingCartSelector,
  shopingCartWithNoApiStatuses: shopingCartWithNoApiStatusesSelector,
  shopingCartSaveError: shopingCartSaveIsErrorSelector,
  cuisinesList: cuisinesDataSelector,
  filtersList: filtersDataSelector,
  initialAddress: currentNewAddressSelector,
});

export const hitsAuthStructuredSelector = createStructuredSelector({
  city: getCity,
  profile: profileSelector,
  balance: bonusBalanceDataSelector,
  restaurant: getRestaurantById,
});

export const homeSearchStructuredSelector = createStructuredSelector({
  search: filtersSearchSelector,
});

export const bannersCollectionStructuredSelector = createStructuredSelector({
  city: getCity,
  profile: profileSelector,
  balance: bonusBalanceDataSelector,
  bonusPointsFilter: pointsFoodFilterSelector,
  restaurantsAndHits: restaurantAndHitsDataSelector,
});

export const newDesignBannerSctructuredSelector = createStructuredSelector({
  auth: profileAuthSelector,
});
