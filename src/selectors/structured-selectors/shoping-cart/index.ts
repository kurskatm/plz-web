// Modules
import { createStructuredSelector } from 'reselect';
// Selectors
import { restaurantMainInfoDataSelector } from '@/selectors/selectors/restaurant/main/info';
import { headerUxHeightSelector } from '@/selectors/selectors/header';
import {
  shopingCartWithSortProductSelector,
  shopingCartSumCoastSelector,
  shopingCartOptionsSelector,
  shopingCartCurrentRestaurantSelector,
  shopingCartCheckoutStatusSelector,
  shopingCartOrderIdSelector,
  shopingCartCheckoutStatusIsPendingSelector,
  shopingCartCheckoutStatusIsErrorSelector,
  shopingCartWithNoApiStatusesSelector,
  shopingCartCheckoutDataSelector,
  shopingCartCheckoutOptionalErrorSelector,
} from '@/selectors/selectors/shoping-cart';
import { getCity } from '@/selectors/selectors/cities';
import {
  shopingCartSaveUiStatusIsPendingSelector,
  shopingCartSaveUiStatusIsSuccessSelector,
  shopingCartSaveUiStatusIsErrorSelector,
} from '@/selectors/selectors/shoping-cart/save';
import {
  shopingCartFetchUiStatusIsSuccessSelector,
  shopingCartFetchUiStatusIsErrorSelector,
  shopingCartFetchUiStatusIsPendingSelector,
} from '@/selectors/selectors/shoping-cart/fetch';
import {
  promocodeUiIsPendingSelector,
  promocodeUiIsErrorSelector,
  promocodeDataSelector,
} from '@/selectors/selectors/promocode';
import { shopingCartFetchRestaurantUiStatusIsPendingSelector } from '@/selectors/selectors/shoping-cart/fetch-restaurant';
import {
  profileAuthSelector,
  profileSelector,
  profileInfoSelector,
} from '@/selectors/selectors/profile';
import { checkoutAddressModalSelector } from '@/selectors/selectors/checkout-modals';
import { currentNewAddressSelector } from '@selectors/selectors/profile/new-address';
import { newGeoCodeDataSelector } from '@selectors/selectors/profile/new-geo-code';
import { showShoppingCardSelector } from '@selectors/selectors/restaurant/modal';

export const shopingCartStructuredSelector = createStructuredSelector({
  city: getCity,
  headerHeight: headerUxHeightSelector,
  shopingCart: shopingCartWithSortProductSelector,
  shopingCartIsPending: shopingCartSaveUiStatusIsPendingSelector,
  shopingCartRestaurantIsPending: shopingCartFetchRestaurantUiStatusIsPendingSelector,
  shopingCartWithNoApiStatuses: shopingCartWithNoApiStatusesSelector,
  profile: profileSelector,
  info: profileInfoSelector,
  auth: profileAuthSelector,
  address: currentNewAddressSelector,
  geocode: newGeoCodeDataSelector,
  resultCoast: shopingCartSumCoastSelector,
  checkoutStatus: shopingCartCheckoutStatusSelector,
  checkoutData: shopingCartCheckoutDataSelector,
  orderId: shopingCartOrderIdSelector,
  checkoutIsPending: shopingCartCheckoutStatusIsPendingSelector,
  checkoutIsError: shopingCartCheckoutStatusIsErrorSelector,
  checkoutOptionalError: shopingCartCheckoutOptionalErrorSelector,
  showCheckoutAddressModal: checkoutAddressModalSelector,
  shopingCartSaveSuccess: shopingCartSaveUiStatusIsSuccessSelector,
  shopingCartSaveError: shopingCartSaveUiStatusIsErrorSelector,

  shopingCartFetchSuccess: shopingCartFetchUiStatusIsSuccessSelector,
  shopingCartFetchError: shopingCartFetchUiStatusIsErrorSelector,

  promocodeValidIsPending: promocodeUiIsPendingSelector,
  promocodeValidIsError: promocodeUiIsErrorSelector,
  promocodeData: promocodeDataSelector,

  showModal: showShoppingCardSelector,
});

export const shopingCartContentStructuredSelector = createStructuredSelector({
  restaurantCard: restaurantMainInfoDataSelector,
});

export const shopingCartBodyStructuredSelector = createStructuredSelector({
  city: getCity,
  profile: profileSelector,
});

export const shopingCartFooterStructuredSelector = createStructuredSelector({
  options: shopingCartOptionsSelector,
  restaurantCard: restaurantMainInfoDataSelector,
});

export const shopingCartRestInfoStructuredSelector = createStructuredSelector({
  restaurant: shopingCartCurrentRestaurantSelector,
});

export const shopingCartPromocodeStructuredSelector = createStructuredSelector({
  promocodeData: promocodeDataSelector,
  auth: profileAuthSelector,
});

export const checkShopingCartGeoStructuredSelector = createStructuredSelector({
  geocode: newGeoCodeDataSelector,
  shopingCart: shopingCartWithSortProductSelector,
  savePending: shopingCartSaveUiStatusIsPendingSelector,
  fetchPending: shopingCartFetchUiStatusIsPendingSelector,
});
