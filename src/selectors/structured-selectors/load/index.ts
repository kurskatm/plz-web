// Modules
import { createStructuredSelector } from 'reselect';
// Selectors
import { contactsUiStatusIsIdle } from '@selectors/selectors/contacts';
import { profileSelector } from '@selectors/selectors/profile';

export const loadStructuredSelector = createStructuredSelector({
  contactsIsIdle: contactsUiStatusIsIdle,
  profile: profileSelector,
});
