// Modules
import { createStructuredSelector } from 'reselect';
// Selectors
import { isBannerFullShowSelector } from '@selectors/selectors/oldSiteBanner';

export const oldSiteBannerSelector = createStructuredSelector({
  isFull: isBannerFullShowSelector,
});
