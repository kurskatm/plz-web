// Modules
import { createStructuredSelector } from 'reselect';
// Selectors
import { getCityId, getCity } from '@selectors/selectors/cities';
import {
  shopingCartSaveIsErrorSelector,
  shopingCartSelector,
} from '@selectors/selectors/shoping-cart';
import { profileSelector } from '../../selectors/profile';

import {
  foodForPointsDataSelector,
  foodForPointsDataUiIsIdleSelector,
  foodForPointsDataUiIsErrorSelector,
  foodForPointsDataUiIsPendingSelector,
} from '../../selectors/foodForPoints';

export const freeProductsStructuredSelector = createStructuredSelector({
  cityId: getCityId,
  city: getCity,
  profile: profileSelector,
  shopingCartSaveError: shopingCartSaveIsErrorSelector,
  foodForPoints: foodForPointsDataSelector,
  foodForPointsisIdle: foodForPointsDataUiIsIdleSelector,
  foodForPointsisError: foodForPointsDataUiIsErrorSelector,
  foodForPointsisPending: foodForPointsDataUiIsPendingSelector,
  shopingCart: shopingCartSelector,
});
