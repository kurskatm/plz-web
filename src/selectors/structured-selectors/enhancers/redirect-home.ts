// Modules
import { createStructuredSelector } from 'reselect';
// Selectors
import { enhanceIsRedirectHomeSelector } from '../../selectors/enhancers/redirect-home';

export const enhanceWithRedirectHomeStructuredSelector = createStructuredSelector({
  redirectUrl: enhanceIsRedirectHomeSelector,
});
