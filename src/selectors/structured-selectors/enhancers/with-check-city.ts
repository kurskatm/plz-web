// Modules
import { createStructuredSelector } from 'reselect';
// Selectors
import { enhanceWithCheckCitySelector } from '@selectors/selectors/enhancers/withCheckCity';
import { shopingCartIdSelector } from '@selectors/selectors/shoping-cart/index';
import { shopingCartFetchUiStatusIsSuccessSelector } from '@selectors/selectors/shoping-cart/fetch';
import { newGeoCodeDataSelector } from '@selectors/selectors/profile/new-geo-code';

export const enhanceWithCheckCityStructuredSelector = createStructuredSelector({
  isCity: enhanceWithCheckCitySelector,
  shopingCartId: shopingCartIdSelector,
  shopingCartFetchSuccess: shopingCartFetchUiStatusIsSuccessSelector,
  geocode: newGeoCodeDataSelector,
});
