// Modules
import { createStructuredSelector } from 'reselect';
// Selectors
import {
  bonusBalanceDataSelector,
  bonusBalanceStatusSelector,
  bonusHistoryDataSelector,
  bonusHistoryStatusSelector,
  bonusSocialsSelector,
} from '../../selectors/bonus';

export const bonusStructuredSelector = createStructuredSelector({
  balance: bonusBalanceDataSelector,
  balanceStatus: bonusBalanceStatusSelector,
  history: bonusHistoryDataSelector,
  historyStatus: bonusHistoryStatusSelector,
  socials: bonusSocialsSelector,
});
