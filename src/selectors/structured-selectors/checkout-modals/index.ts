// Modules
import { createStructuredSelector } from 'reselect';
// Selectors
import {
  checkoutAddressModalSelector,
  checkoutBasketModalSelector,
  checkoutPromocodeModalSelector,
  checkoutModalCityLinkSelector,
} from '@selectors/selectors/checkout-modals';
import { currentNewAddressSelector } from '@selectors/selectors/profile/new-address';

export const addressModalStructuredSelector = createStructuredSelector({
  showModal: checkoutAddressModalSelector,
  city: checkoutModalCityLinkSelector,
  address: currentNewAddressSelector,
});

export const basketModalStructuredSelector = createStructuredSelector({
  showModal: checkoutBasketModalSelector,
});

export const promocodeModalStructuredSelector = createStructuredSelector({
  showModal: checkoutPromocodeModalSelector,
});
