// Modules
import { createStructuredSelector } from 'reselect';
// Selector
import { citiesLocationsSelector, getCity } from '@selectors/selectors/cities';
import {
  partnersCounterSelectorData,
  partnersCounterSelectorStatusIsIdle,
  partnersCounterSelectorStatusIsError,
  partnersMembersSelector,
} from '../../selectors/partners';

export const partnersStructuredSelector = createStructuredSelector({
  counters: partnersCounterSelectorData,
  countersStatusIsIdle: partnersCounterSelectorStatusIsIdle,
  countersStatusIsError: partnersCounterSelectorStatusIsError,
  locations: citiesLocationsSelector,
  members: partnersMembersSelector,
  city: getCity,
});
