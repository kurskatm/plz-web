// Modules
import { createStructuredSelector } from 'reselect';
// Selectors
import {
  citiesDataSelector,
  citiesModalShowSelector,
  getCity,
  citiesIsIdle,
} from '@selectors/selectors/cities';
import { shopingCartIdSelector } from '@/selectors/selectors/shoping-cart';

export const citiesListListStructuredSelector = createStructuredSelector({
  cities: citiesDataSelector,
});

export const citiesListTitleStructuredSelector = createStructuredSelector({
  cities: citiesDataSelector,
});

export const citiesModalShowStructuredSelector = createStructuredSelector({
  showModal: citiesModalShowSelector,
  city: getCity,
});

export const citiesFetchStructuredSelector = createStructuredSelector({
  citiesIsIdle,
});

export const cityCheckStructuredSelector = createStructuredSelector({
  cities: citiesDataSelector,
  cityItem: getCity,
  shopingCartId: shopingCartIdSelector,
});
