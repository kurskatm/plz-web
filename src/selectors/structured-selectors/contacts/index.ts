// Modules
import { createStructuredSelector } from 'reselect';
// Selectors
import { getCity } from '../../selectors/cities';
import { contactsDataSelector } from '../../selectors/contacts';

export const contactsStructuredSelector = createStructuredSelector({
  city: getCity,
  contacts: contactsDataSelector,
});
