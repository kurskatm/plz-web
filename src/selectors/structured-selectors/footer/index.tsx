// Modules
import { createStructuredSelector } from 'reselect';
// Selectors
import { profileAuthSelector } from '@selectors/selectors/profile';
import { contactsDataSelector } from '../../selectors/contacts';

export const footerInfoStructuredSelector = createStructuredSelector({
  contacts: contactsDataSelector,
});
export const footerAuthInfoStructuredSelector = createStructuredSelector({
  profileAuth: profileAuthSelector,
});
