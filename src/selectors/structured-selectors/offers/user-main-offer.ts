// Modules
import { createStructuredSelector } from 'reselect';
// Selectors
import { profileSelector } from '@selectors/selectors/profile';

export const userMainOfferStructuredSelector = createStructuredSelector({
  profile: profileSelector,
});
