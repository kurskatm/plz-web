// Modules
import { createStructuredSelector } from 'reselect';
// Selectors
import {
  reviewsDataSelector,
  reviewsUiStatusIsIdleSelector,
  reviewsUiStatusIsPendingSelector,
  reviewsUiStatusIsSuccessSelector,
  reviewsUiStatusIsErrorSelector,
} from '@selectors/selectors/reviews';
import {
  restaurantAndHitsDataSelector,
  restaurantAndHitsUiStatusIsSuccessSelector,
  restaurantAndHitsUiStatusIsErrorSelector,
} from '@selectors/selectors/restaurants-and-hits';
import { getCity } from '@selectors/selectors/cities';

export const homeReviewsStructuredSelector = createStructuredSelector({
  city: getCity,
  reviews: reviewsDataSelector,
  reviewsIsIdle: reviewsUiStatusIsIdleSelector,
  reviewsIsPending: reviewsUiStatusIsPendingSelector,
  reviewsIsSuccess: reviewsUiStatusIsSuccessSelector,
  reviewsIsError: reviewsUiStatusIsErrorSelector,
  restaurantsAndHits: restaurantAndHitsDataSelector,
  restaurantsAndHitsIsSuccess: restaurantAndHitsUiStatusIsSuccessSelector,
  restaurantsAndHitsIsError: restaurantAndHitsUiStatusIsErrorSelector,
});
