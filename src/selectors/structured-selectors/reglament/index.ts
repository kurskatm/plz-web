// Modules
import { createStructuredSelector } from 'reselect';
// Selectors
import { getCity } from '@/selectors/selectors/cities';

export const reglamentStructuredSelector = createStructuredSelector({
  city: getCity,
});
