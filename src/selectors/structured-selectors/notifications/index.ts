import { createStructuredSelector } from 'reselect';
import { notificationsSelector } from '@selectors/selectors/notifications';

export const notificationsStructuredSelector = createStructuredSelector({
  notifications: notificationsSelector,
});
