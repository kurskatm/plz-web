// Modules
import { createStructuredSelector } from 'reselect';
// Selectors
import { restaurantsDataSelector } from '../../selectors/restaurants';

export const restaurantsStructuredSelector = createStructuredSelector({
  restaurants: restaurantsDataSelector,
});
