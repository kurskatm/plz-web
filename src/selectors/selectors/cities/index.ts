// Modules
import { createSelector } from 'reselect';
import isNil from 'lodash/isNil';
// Lib
import { getCookieCityData } from '@lib/cookie';
// Main Types
import { TCitiesModel } from '@models/cities/types';
import { TCitiesList, TCitiesLocationList, TCitiesItem } from '@type/cities';
// Utils
import { Consts } from '@utils';
// Selectors
import { profileSelector } from '../profile';

const { IDLE, SUCCESS, PENDING, ERROR } = Consts.REQUEST.UI.STATUS;

export const citiesSelector = ({ cities }: { cities: TCitiesModel }): TCitiesModel => cities;

export const citiesListSelector = createSelector(citiesSelector, ({ list }) => list);

export const citiesDataSelector = createSelector(
  citiesListSelector,
  ({ data }): TCitiesList =>
    [...data].sort((a, b) => {
      const nameA = a.name.toUpperCase();
      const nameB = b.name.toUpperCase();

      if (nameA < nameB) {
        return -1;
      }

      if (nameA > nameB) {
        return 1;
      }

      return 0;
    }),
);

export const citiesLocationsSelector = createSelector(
  citiesListSelector,
  ({ data }): TCitiesLocationList => data.map((city: TCitiesItem) => city.center),
);

export const getCity = createSelector([citiesDataSelector, profileSelector], (cities, profile) => {
  if (!isNil(profile?.city)) {
    return cities.find(({ id }) => id === profile.city);
  }
  const cityIdCookie = getCookieCityData();
  if (!isNil(cityIdCookie)) {
    return cities.find(({ id }) => id === cityIdCookie);
  }

  return null;
});

export const getCityId = createSelector([getCity], (props) => {
  if (!isNil(props)) {
    return props.id;
  }

  return null;
});

export const citiesModalShowSelector = createSelector(citiesSelector, ({ modal }) => modal.show);

export const citiesUiStatusSelector = createSelector(citiesListSelector, ({ ui }) => ui.status);

export const citiesIsIdle = createSelector(citiesUiStatusSelector, (status) => status === IDLE);

export const citiesIsPending = createSelector(
  citiesUiStatusSelector,
  (status) => status === PENDING,
);

export const citiesIsSuccess = createSelector(
  citiesUiStatusSelector,
  (status) => status === SUCCESS,
);

export const citiesIsError = createSelector(citiesUiStatusSelector, (status) => status === ERROR);
