// Main Types
import { TReduxState } from '@models/types';
import {
  TShopingCartModel,
  TRestaurantShopingCart,
  TShopingCartProduct,
  TShopingCartOptions,
} from '@models/shoping-cart/types';

export type TShopingCartSelector = (state: TReduxState) => TShopingCartModel;
export type TShopingCartProductsMemoSelector = (state: TReduxState) => TShopingCartProduct[];
export type TShopingCartRestaurantMemoSelector = (state: TReduxState) => TRestaurantShopingCart;
export type TShopingCartOptionsMemoSelector = (state: TReduxState) => TShopingCartOptions;
