/* eslint-disable no-return-assign */
// Modules
import { createSelector } from 'reselect';
import isNil from 'lodash/isNil';
// Utils
import { Consts } from '@utils';
// Types
import {
  TShopingCartSelector,
  TShopingCartProductsMemoSelector,
  TShopingCartRestaurantMemoSelector,
  TShopingCartOptionsMemoSelector,
} from './types';
import { restaurantAndHitsDataSelector } from '../restaurants-and-hits';

const { PENDING, ERROR } = Consts.REQUEST.UI.STATUS;

export const shopingCartSelector: TShopingCartSelector = ({ shopingCart }) => shopingCart;

const shopingCartProductsMemoSelector: TShopingCartProductsMemoSelector = ({ shopingCart }) =>
  shopingCart.products;
export const shopingCartRestaurantMemoSelector: TShopingCartRestaurantMemoSelector = ({
  shopingCart,
}) => shopingCart.restaurant;
const shopingCartOptionsMemoSelector: TShopingCartOptionsMemoSelector = ({ shopingCart }) =>
  shopingCart.options;

export const shopingCartWithSortProductSelector = createSelector(
  [shopingCartSelector],
  (shopingCart) => ({
    ...shopingCart,
    products: shopingCart.products.slice().sort((a, b) => {
      if (a.inBonusPoint && !b.inBonusPoint) {
        return -1;
      }

      if (!a.inBonusPoint && b.inBonusPoint) {
        return 1;
      }

      return 0;
    }),
  }),
);

export const shopingCartSumCoastSelector = createSelector(
  [shopingCartSelector],
  (shopingCart) => shopingCart?.options?.total || 0,
);
// shopingCart.products.reduce((acc, product) => {
//   let modifiersCost = 0;
//   if (product?.modifiers?.length) {
//     modifiersCost = product.modifiers
//       .reduce((accModifiers, val) => [...accModifiers, ...val.modifiers], [])
//       .reduce((accCost, modifier) => (accCost += modifier.price), 0);
//   }

//   const coast = product.inBonusPoint ? 0 : (product.price + modifiersCost) * product.count;

export const shopingCartForPointsCostSelector = createSelector(
  [shopingCartSelector],
  (shopingCart) => {
    const inBonusPointProduct = shopingCart.products.find(({ inBonusPoint }) => inBonusPoint);

    if (!isNil(inBonusPointProduct)) {
      return inBonusPointProduct.price;
    }

    return 0;
  },
);

export const shopingCartHasForPointsSelector = createSelector(
  [shopingCartSelector],
  (shopingCart) => shopingCart.products.find((item) => item.inBonusPoint),
);

export const shopingCartHasForPointsCashSelector = createSelector(
  [shopingCartSelector],
  (shopingCart) =>
    shopingCart.products.reduce((acc, value) => {
      const coast = value.inBonusPoint ? value.price * value.count : 0;

      return acc + coast;
    }, 0),
);
export const shopingCartIdSelector = createSelector(
  [shopingCartSelector],
  (shopingCart) => shopingCart.id,
);
export const shopingCartOptionsSelector = createSelector(
  [shopingCartSelector],
  (shopingCart) => shopingCart.options,
);
export const shopingCartCheckoutStatusSelector = createSelector(
  [shopingCartSelector],
  (shopingCart) => shopingCart.checkout?.ui?.status,
);
export const shopingCartCheckoutOptionalErrorSelector = createSelector(
  [shopingCartSelector],
  (shopingCart) => shopingCart?.checkout?.error,
);
export const shopingCartOrderIdSelector = createSelector(
  [shopingCartSelector],
  (shopingCart) => shopingCart.checkout?.id,
);
export const shopingCartCheckoutDataSelector = createSelector(
  [shopingCartSelector],
  (shopingCart) => shopingCart.checkout?.paymentResult,
);

export const shopingCartCheckoutStatusIsPendingSelector = createSelector(
  shopingCartCheckoutStatusSelector,
  (status) => status === PENDING,
);

export const shopingCartCheckoutStatusIsErrorSelector = createSelector(
  shopingCartCheckoutStatusSelector,
  (status) => status === ERROR,
);

export const shopingCartRestaurantSelector = createSelector(
  shopingCartWithSortProductSelector,
  (shopingCart) => {
    if (!isNil(shopingCart.restaurant)) {
      return shopingCart.restaurant.id;
    }

    return null;
  },
);

export const shopingCartRestaurantDataSelector = createSelector(
  shopingCartSelector,
  (shopingCart) => shopingCart.restaurant,
);

export const shopingCartCurrentRestaurantSelector = createSelector(
  [restaurantAndHitsDataSelector, shopingCartRestaurantSelector],
  (restaurantsAndHits, restId) => {
    if (restaurantsAndHits?.restaurants?.length && !isNil(restId)) {
      return restaurantsAndHits.restaurants.find(({ id }) => id === restId);
    }

    return null;
  },
);

const shoppingCartCutlerySelector = createSelector(
  [shopingCartOptionsMemoSelector],
  ({ cutlery }) => cutlery,
);

export const shopingCartWithNoApiStatusesSelector = createSelector(
  [shopingCartProductsMemoSelector, shopingCartRestaurantMemoSelector, shoppingCartCutlerySelector],
  (products, restaurant, cutlery) => ({ products, restaurant, cutlery }),
);

// Shoping cart update ui states
export const shopingCartSaveUiSelector = createSelector(
  [shopingCartSelector],
  ({ save }) => save.ui,
);

export const shopingCartSaveIsErrorSelector = createSelector(
  [shopingCartSaveUiSelector],
  ({ status }) => status === ERROR,
);
