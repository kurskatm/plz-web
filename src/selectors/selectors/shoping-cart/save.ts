// Modules
import { createSelector } from 'reselect';
// Utils
import { Consts } from '@utils';
// Types
import { TShopingCartSelector } from './types';

const { IDLE, PENDING, SUCCESS, ERROR } = Consts.REQUEST.UI.STATUS;

export const shopingCartSelector: TShopingCartSelector = ({ shopingCart }) => shopingCart;

export const shopingCartSaveUiStatusSelector = createSelector(
  [shopingCartSelector],
  ({ save }) => save.ui.status,
);

export const shopingCartSaveUiStatusIsIdleSelector = createSelector(
  shopingCartSaveUiStatusSelector,
  (status) => status === IDLE,
);

export const shopingCartSaveUiStatusIsPendingSelector = createSelector(
  shopingCartSaveUiStatusSelector,
  (status) => status === PENDING,
);

export const shopingCartSaveUiStatusIsSuccessSelector = createSelector(
  shopingCartSaveUiStatusSelector,
  (status) => status === SUCCESS,
);

export const shopingCartSaveUiStatusIsErrorSelector = createSelector(
  shopingCartSaveUiStatusSelector,
  (status) => status === ERROR,
);
