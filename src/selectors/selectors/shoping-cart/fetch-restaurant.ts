// Modules
import { createSelector } from 'reselect';
// Utils
import { Consts } from '@utils';
// Types
import { TShopingCartSelector } from './types';

const { IDLE, PENDING, SUCCESS, ERROR } = Consts.REQUEST.UI.STATUS;

export const shopingCartSelector: TShopingCartSelector = ({ shopingCart }) => shopingCart;

export const shopingCartFetchRestaurantUiStatusSelector = createSelector(
  [shopingCartSelector],
  ({ fetchRestaurant }) => fetchRestaurant?.ui?.status,
);

export const shopingCartFetchRestaurantUiStatusIsIdleSelector = createSelector(
  shopingCartFetchRestaurantUiStatusSelector,
  (status) => status === IDLE,
);

export const shopingCartFetchRestaurantUiStatusIsPendingSelector = createSelector(
  shopingCartFetchRestaurantUiStatusSelector,
  (status) => status === PENDING,
);

export const shopingCartSaveUiStatusIsSuccessSelector = createSelector(
  shopingCartFetchRestaurantUiStatusSelector,
  (status) => status === SUCCESS,
);

export const shopingCartFetchRestaurantUiStatusIsErrorSelector = createSelector(
  shopingCartFetchRestaurantUiStatusSelector,
  (status) => status === ERROR,
);
