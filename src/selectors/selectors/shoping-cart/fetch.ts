// Modules
import { createSelector } from 'reselect';
// Utils
import { Consts } from '@utils';
// Types
import { TShopingCartSelector } from './types';

const { IDLE, PENDING, SUCCESS, ERROR } = Consts.REQUEST.UI.STATUS;

export const shopingCartSelector: TShopingCartSelector = ({ shopingCart }) => shopingCart;

export const shopingCartFetchUiStatusSelector = createSelector(
  [shopingCartSelector],
  ({ fetch }) => fetch?.ui?.status,
);

export const shopingCartFetchUiStatusIsIdleSelector = createSelector(
  shopingCartFetchUiStatusSelector,
  (status) => status === IDLE,
);

export const shopingCartFetchUiStatusIsPendingSelector = createSelector(
  shopingCartFetchUiStatusSelector,
  (status) => status === PENDING,
);

export const shopingCartFetchUiStatusIsSuccessSelector = createSelector(
  shopingCartFetchUiStatusSelector,
  (status) => status === SUCCESS,
);

export const shopingCartFetchUiStatusIsErrorSelector = createSelector(
  shopingCartFetchUiStatusSelector,
  (status) => status === ERROR,
);
