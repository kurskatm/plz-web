/* eslint-disable @typescript-eslint/no-explicit-any */
// Constants
import { Consts } from '@utils';
// Modules
import { createSelector } from 'reselect';

const { IDLE, ERROR, SUCCESS } = Consts.REQUEST.UI.STATUS;

export const supportSelector = ({ support }: { support: any }): any => support;

export const supportTopicsSelector = createSelector(supportSelector, ({ topics }) => topics);
export const supportMessageSelector = createSelector(
  supportSelector,
  ({ messageSent }) => messageSent,
);

export const supporMessageStatusSelector = createSelector(
  supportMessageSelector,
  ({ ui }) => ui.status,
);

export const supporMessageStatusIsSuccessSelector = createSelector(
  supporMessageStatusSelector,
  (status) => status === SUCCESS,
);

export const supporMessageStatusIsErrorSelector = createSelector(
  supporMessageStatusSelector,
  (status) => status === ERROR,
);

export const supporTopicsStatusSelector = createSelector(
  supportTopicsSelector,
  ({ ui }) => ui.status,
);

export const supportTopicsDataSelector = createSelector(supportTopicsSelector, ({ data }) => data);

export const supportTopicsSelectorStatusIsIdle = createSelector(
  supporTopicsStatusSelector,
  (status) => status === IDLE,
);

export const supportTopicsSelectorStatusIsError = createSelector(
  supporTopicsStatusSelector,
  (status) => status === ERROR,
);
