// Modules
import { createSelector } from 'reselect';
// Utils
import { Consts } from '@utils';
// Main Types
import { TFilter } from '@type/filters';
import { TFiltersModel } from '@models/filters/types';
import { TUiModel } from '@models/ui/types';

const { IDLE, SUCCESS } = Consts.REQUEST.UI.STATUS;

export const filtersDataSelector = ({ filters }: { filters: TFiltersModel }): TFilter[] =>
  filters.list.data;

export const filtersDataUiSelector = ({ filters }: { filters: TFiltersModel }): TUiModel =>
  filters.list.ui;

export const filtersChosenSelector = ({ filters }: { filters: TFiltersModel }): string[] =>
  filters.chosen;

export const filtersSearchSelector = ({ filters }: { filters: TFiltersModel }): string =>
  filters.search;

export const filtersDataUiIsIdleSelector = createSelector(
  filtersDataUiSelector,
  ({ status }) => status === IDLE,
);

export const filtersDataUiIsSuccessSelector = createSelector(
  filtersDataUiSelector,
  ({ status }) => status === SUCCESS,
);

export const filtersShowModalSelector = ({ filters }: { filters: TFiltersModel }): boolean =>
  filters.showModal;

export const pointsFoodFilterSelector = createSelector(filtersDataSelector, (filters) => {
  if (filters.length) {
    return filters.find(({ name }) => name === 'За бонусные баллы');
  }

  return null;
});
