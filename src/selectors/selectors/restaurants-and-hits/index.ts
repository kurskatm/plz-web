// Modules
import { createSelector } from 'reselect';
import isNil from 'lodash/isNil';
// Utils
import { Consts } from '@utils';
// Types
import { TRestaurantMAndHitsSelector, TOwnRestaurantProps } from './types';

const { IDLE, PENDING, SUCCESS, ERROR } = Consts.REQUEST.UI.STATUS;

const idSelector = (_: unknown, ownProps: TOwnRestaurantProps): string =>
  ownProps?.restaurantId || ownProps?.item?.restaurant?.id;

export const restaurantAndHitsSelector: TRestaurantMAndHitsSelector = ({ restaurantsAndHits }) =>
  restaurantsAndHits;

export const restaurantAndHitsDataSelector = createSelector(
  [restaurantAndHitsSelector],
  // ({ data }) => data,
  ({ data }) => {
    if (!isNil(data) && data.products?.length && data.restaurants?.length) {
      const newProducts = data.products.map((product) => {
        const currentRestaurant = data.restaurants.find(
          ({ id }) =>
            // @ts-ignore
            id === product?.restaurant?.id,
        );
        return {
          ...product,
          restaurant: {
            // @ts-ignore
            ...product.restaurant,
            urlName: currentRestaurant?.urlName,
          },
        };
      });

      return {
        ...data,
        products: newProducts,
      };
    }

    return data;
  },
);

export const getRestaurants = createSelector(
  [restaurantAndHitsDataSelector],
  (data) => data?.restaurants,
);

export const getRestaurantById = createSelector([getRestaurants, idSelector], (rests = [], id) =>
  rests.find((item) => item.id === id),
);

export const restaurantsWithBonusProductsSelector = createSelector(
  [restaurantAndHitsDataSelector],
  (data) => data?.restaurants.filter((rest) => rest.hasPaymentForPoints),
);

export const restaurantAndHitsUiStatusSelector = createSelector(
  [restaurantAndHitsSelector],
  ({ ui }) => ui.status,
);

export const restaurantAndHitsUiStatusIsIdleSelector = createSelector(
  restaurantAndHitsUiStatusSelector,
  (status) => status === IDLE,
);

export const restaurantAndHitsUiStatusIsPendingSelector = createSelector(
  restaurantAndHitsUiStatusSelector,
  (status) => status === PENDING,
);

export const restaurantAndHitsUiStatusIsSuccessSelector = createSelector(
  restaurantAndHitsUiStatusSelector,
  (status) => status === SUCCESS,
);

export const restaurantAndHitsUiStatusIsErrorSelector = createSelector(
  restaurantAndHitsUiStatusSelector,
  (status) => status === ERROR,
);
