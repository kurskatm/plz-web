// Main Types
import { TReduxState } from '@models/types';
import { TRestaurantsAndHitsModel } from '@models/restaurants-and-hits/types';

export type TRestaurantMAndHitsSelector = (state: TReduxState) => TRestaurantsAndHitsModel;

interface TRestaurantId {
  restaurantId: string;
}

interface TRestaurantItem {
  item: {
    restaurant: {
      id: string;
    };
  };
}

export type TOwnRestaurantProps = TRestaurantId & TRestaurantItem;
