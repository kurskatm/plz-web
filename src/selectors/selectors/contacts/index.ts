// Modules
import { createSelector } from 'reselect';
// Utils
import { Consts } from '@utils';
// Main Types
import { TContactsModel } from '@models/contacts/types';
import { TUiModel } from '@models/ui/types';

const { STATUS } = Consts.REQUEST.UI;

export const contactsSelector = ({ contacts }: { contacts: TContactsModel }): TContactsModel =>
  contacts;

export const contactsDataSelector = createSelector(contactsSelector, ({ data }) => data);

export const contactsUiStatusSelector = createSelector(
  contactsSelector,
  ({ ui }: { ui: TUiModel }) => ui.status,
);

export const contactsUiStatusIsIdle = createSelector(
  contactsUiStatusSelector,
  (status) => status === STATUS.IDLE,
);
