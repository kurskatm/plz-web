// Main Types
import { TReduxState } from '@models/types';
import { TFoodForPointsModel } from '@models/foodForPoints/types';

export type TFoodForPointsSelector = (state: TReduxState) => TFoodForPointsModel;
