/* eslint-disable @typescript-eslint/no-explicit-any */
// Constants
import { Consts } from '@utils';
// Modules
import { createSelector } from 'reselect';
// Types
import { TFoodForPointsSelector } from './types';

const { IDLE, ERROR, PENDING } = Consts.REQUEST.UI.STATUS;

// eslint-disable-next-line max-len
export const foodForPointsSelector: TFoodForPointsSelector = ({ foodForPoints }) => foodForPoints;

export const foodForPointsDataSelector = createSelector(foodForPointsSelector, ({ data }) => data);

export const foodForPointsStatusSelector = createSelector(
  foodForPointsSelector,
  ({ ui }) => ui.status,
);

export const foodForPointsDataUiIsIdleSelector = createSelector(
  foodForPointsStatusSelector,
  (status) => status === IDLE,
);

export const foodForPointsDataUiIsErrorSelector = createSelector(
  foodForPointsStatusSelector,
  (status) => status === ERROR,
);

export const foodForPointsDataUiIsPendingSelector = createSelector(
  foodForPointsStatusSelector,
  (status) => status === PENDING,
);
