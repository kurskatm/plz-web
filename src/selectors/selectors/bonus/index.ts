/* eslint-disable @typescript-eslint/no-explicit-any */
// Modules
import { createSelector } from 'reselect';
// Types
import { TBonusModel } from '@models/bonus/types';
// Constants
import { Consts } from '@utils';

const { ERROR } = Consts.REQUEST.UI.STATUS;

export const bonusSelector = ({ bonus }: { bonus: TBonusModel }): TBonusModel => bonus;

export const bonusBalanceSelector = createSelector(bonusSelector, ({ balance }) => balance);
export const bonusBalanceStatusSelector = createSelector(
  bonusBalanceSelector,
  ({ ui }) => ui.status,
);
export const bonusBalanceStatusIsErrorSelector = createSelector(
  bonusBalanceStatusSelector,
  (status) => status === ERROR,
);
export const bonusSocialsSelector = createSelector(bonusBalanceSelector, ({ data }) =>
  data ? data.earnedBySocialActivity : [],
);
export const bonusBalanceDataSelector = createSelector(bonusBalanceSelector, ({ data }) =>
  data ? data.balance : '0',
);

export const bonusHistorySelector = createSelector(bonusSelector, ({ history }) => history);

export const bonusHistoryStatusSelector = createSelector(
  bonusHistorySelector,
  ({ ui }) => ui.status,
);
export const bonusHistoryStatusIsErrorSelector = createSelector(
  bonusHistoryStatusSelector,
  (status) => status === ERROR,
);

export const bonusHistoryDataSelector = createSelector(bonusHistorySelector, ({ data }) => data);
