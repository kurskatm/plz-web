/* eslint-disable @typescript-eslint/no-explicit-any */
// Constants
import { Consts } from '@utils';
// Modules
import { createSelector } from 'reselect';

const { IDLE, PENDING, SUCCESS, ERROR } = Consts.REQUEST.UI.STATUS;

export const hotOffersSelector = ({ hotOffers }: { hotOffers: any }): any => hotOffers;

export const hotOffersDataSelector = createSelector(hotOffersSelector, ({ data }) => data);

export const hotOffersStatusSelector = createSelector(hotOffersSelector, ({ ui }) => ui.status);

export const hotOffersDataUiIsIdleSelector = createSelector(
  hotOffersStatusSelector,
  (status) => status === IDLE,
);

export const hotOffersDataUiIsPendingSelector = createSelector(
  hotOffersStatusSelector,
  (status) => status === PENDING,
);

export const hotOffersDataUiIsSuccessSelector = createSelector(
  hotOffersStatusSelector,
  (status) => status === SUCCESS,
);

export const hotOffersDataUiIsErrorSelector = createSelector(
  hotOffersStatusSelector,
  (status) => status === ERROR,
);
