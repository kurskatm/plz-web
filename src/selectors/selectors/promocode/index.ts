import { createSelector } from 'reselect';
import { TPromocodeModel } from '@models/promocode/types';
import { Consts } from '@utils';

const { IDLE, PENDING, SUCCESS, ERROR } = Consts.REQUEST.UI.STATUS;

export const promocodeSelector = ({ promocode }: { promocode: TPromocodeModel }): TPromocodeModel =>
  promocode;

export const promocodeDataSelector = createSelector(promocodeSelector, ({ data }) => data);

export const promocodeUiSelector = createSelector(promocodeSelector, ({ validUi }) => validUi);

export const promocodeUiIsPendingSelector = createSelector(
  promocodeUiSelector,
  ({ status }) => status === PENDING,
);

export const promocodeUiIsSuccessSelector = createSelector(
  promocodeUiSelector,
  ({ status }) => status === SUCCESS,
);

export const promocodeUiIsErrorSelector = createSelector(
  promocodeUiSelector,
  ({ status }) => status === ERROR,
);

export const promocodeUiIsIdleSelector = createSelector(
  promocodeUiSelector,
  ({ status }) => status === IDLE,
);
