// Modules
import { createSelector } from 'reselect';
// Lib
import { getCookieCityData } from '@lib/cookie';
// Selectors
import { citiesDataSelector } from '@selectors/selectors/cities';
// Types
import { TCheckoutModalsSelector } from './types';

export const checkoutModalsSelector: TCheckoutModalsSelector = ({ checkoutModals }) =>
  checkoutModals;

export const checkoutAddressModalSelector = createSelector(
  checkoutModalsSelector,
  ({ address }) => address.show,
);

export const checkoutBasketModalSelector = createSelector(
  checkoutModalsSelector,
  ({ basket }) => basket.show,
);

export const checkoutPromocodeModalSelector = createSelector(
  checkoutModalsSelector,
  ({ promocode }) => promocode.show,
);

export const checkoutModalCityLinkSelector = createSelector(citiesDataSelector, (cities) => {
  if (cities.length) {
    const cityId = getCookieCityData();
    const city = cities.find(({ id }) => id === cityId);
    return city?.urlName;
  }

  return '';
});
