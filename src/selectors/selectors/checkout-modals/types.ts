// Main Types
import { TReduxState } from '@models/types';
import { TCheckoutModalsModel } from '@models/checkout-modals/types';

export type TCheckoutModalsSelector = (state: TReduxState) => TCheckoutModalsModel;
