// Modules
import { createSelector } from 'reselect';
// Utils
import { Consts } from '@utils';
// Types
import { TRestaurantMainSelector } from './types';

const { IDLE, PENDING, SUCCESS, ERROR } = Consts.REQUEST.UI.STATUS;

export const restaurantMainSelector: TRestaurantMainSelector = ({ restaurant }) => restaurant.main;

export const restaurantMainInfoDataSelector = createSelector(
  [restaurantMainSelector],
  ({ info }) => info.data,
);

export const restaurantMainInfoUiStatusSelector = createSelector(
  [restaurantMainSelector],
  ({ info }) => info.ui.status,
);

export const restaurantMainInfoPaymentTypesSelector = createSelector(
  [restaurantMainInfoDataSelector],
  (info) => info?.paymentTypes,
);

export const restaurantMainInfoUiStatusIsIdleSelector = createSelector(
  restaurantMainInfoUiStatusSelector,
  (status) => status === IDLE,
);

export const restaurantMainInfoUiStatusIsPendingSelector = createSelector(
  restaurantMainInfoUiStatusSelector,
  (status) => status === PENDING,
);

export const restaurantMainInfoUiStatusIsSuccessSelector = createSelector(
  restaurantMainInfoUiStatusSelector,
  (status) => status === SUCCESS,
);

export const restaurantMainInfoUiStatusIsErrorSelector = createSelector(
  restaurantMainInfoUiStatusSelector,
  (status) => status === ERROR,
);
