// Main Types
import { TReduxState } from '@models/types';
import { TRestaurantMainModel } from '@models/restaurant/main/types';

export type TRestaurantMainSelector = (state: TReduxState) => TRestaurantMainModel;
