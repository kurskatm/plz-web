// Modules
import { createSelector } from 'reselect';
// Types
import { TRestaurantMainSelector } from './types';

const restaurantMainSelector: TRestaurantMainSelector = ({ restaurant }) => restaurant.main;

export const restaurantStoresSelector = createSelector(
  [restaurantMainSelector],
  ({ stores }) => stores,
);
