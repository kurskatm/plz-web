// Modules
import { createSelector } from 'reselect';
// Utils
import { Consts } from '@utils';
// Types
import { TRestaurantMainSelector } from './types';

const { IDLE, PENDING, SUCCESS, ERROR } = Consts.REQUEST.UI.STATUS;

export const restaurantMainSelector: TRestaurantMainSelector = ({ restaurant }) => restaurant.main;

export const restaurantMainIdDataSelector = createSelector(
  [restaurantMainSelector],
  ({ id }) => id.data?.id,
);

export const restaurantMainIdUiStatusSelector = createSelector(
  [restaurantMainSelector],
  ({ id }) => id.ui.status,
);

export const restaurantMainIdUiStatusIsIdleSelector = createSelector(
  restaurantMainIdUiStatusSelector,
  (status) => status === IDLE,
);

export const restaurantMainIdUiStatusIsPendingSelector = createSelector(
  restaurantMainIdUiStatusSelector,
  (status) => status === PENDING,
);

export const restaurantMainIdUiStatusIsSuccessSelector = createSelector(
  restaurantMainIdUiStatusSelector,
  (status) => status === SUCCESS,
);

export const restaurantMainIdUiStatusIsErrorSelector = createSelector(
  restaurantMainIdUiStatusSelector,
  (status) => status === ERROR,
);
