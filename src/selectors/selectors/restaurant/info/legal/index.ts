// Modules
import { createSelector } from 'reselect';
// Utils
import { Consts } from '@utils';
// Types
import { TRestaurantInfoSelector } from './types';

const { IDLE, PENDING, SUCCESS, ERROR } = Consts.REQUEST.UI.STATUS;

export const restaurantInfoSelector: TRestaurantInfoSelector = ({ restaurant }) => restaurant.info;

export const restaurantInfoLegalDataSelector = createSelector(
  [restaurantInfoSelector],
  ({ legal }) => legal.data,
);

export const restaurantInfoLegalUiStatusSelector = createSelector(
  [restaurantInfoSelector],
  ({ legal }) => legal.ui.status,
);

export const restaurantInfoLegalUiStatusIsIdleSelector = createSelector(
  restaurantInfoLegalUiStatusSelector,
  (status) => status === IDLE,
);

export const restaurantInfoLegalUiStatusIsPendingSelector = createSelector(
  restaurantInfoLegalUiStatusSelector,
  (status) => status === PENDING,
);

export const restaurantInfoLegalUiStatusIsSuccessSelector = createSelector(
  restaurantInfoLegalUiStatusSelector,
  (status) => status === SUCCESS,
);

export const restaurantInfoLegalUiStatusIsErrorSelector = createSelector(
  restaurantInfoLegalUiStatusSelector,
  (status) => status === ERROR,
);
