// Modules
import { createSelector } from 'reselect';
// Lib
import { getHoursAndMinutes } from '@lib/restaurants/getHoursAndMinutes';
// Utils
import { Consts } from '@utils';
// Types
import { TRestaurantInfoSelector } from './types';

const { IDLE, PENDING, SUCCESS, ERROR } = Consts.REQUEST.UI.STATUS;

export const restaurantInfoSelector: TRestaurantInfoSelector = ({ restaurant }) => restaurant.info;

export const restaurantInfoDataSelector = createSelector(
  [restaurantInfoSelector],
  ({ info }) => info.data,
);

export const restaurantInfoWeekScheduleObjectSelector = createSelector(
  [restaurantInfoSelector],
  ({ info }) =>
    info.data.weekSchedule.reduce(
      (acc, value) => ({
        ...acc,
        [value.dayOfWeek]: {
          dayOfWeek: value.dayOfWeek,
          fromTime: getHoursAndMinutes(value.fromTime),
          toTime: getHoursAndMinutes(value.toTime),
        },
      }),
      {},
    ),
);

export const restaurantInfoUiStatusSelector = createSelector(
  [restaurantInfoSelector],
  ({ info }) => info.ui.status,
);

export const restaurantInfoUiStatusIsIdleSelector = createSelector(
  restaurantInfoUiStatusSelector,
  (status) => status === IDLE,
);

export const restaurantInfoUiStatusIsPendingSelector = createSelector(
  restaurantInfoUiStatusSelector,
  (status) => status === PENDING,
);

export const restaurantInfoUiStatusIsSuccessSelector = createSelector(
  restaurantInfoUiStatusSelector,
  (status) => status === SUCCESS,
);

export const restaurantInfoUiStatusIsErrorSelector = createSelector(
  restaurantInfoUiStatusSelector,
  (status) => status === ERROR,
);
