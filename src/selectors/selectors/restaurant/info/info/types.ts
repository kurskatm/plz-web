// Main Types
import { TReduxState } from '@models/types';
import { TRestaurantInfoModel } from '@models/restaurant/info/types';

export type TRestaurantInfoSelector = (state: TReduxState) => TRestaurantInfoModel;
