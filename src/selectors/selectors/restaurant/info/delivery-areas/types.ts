// Main Types
import { TReduxState } from '@models/types';
import { TDeliveryAreasModel } from '@models/restaurant/info/delivery-areas/types';

export type TRestaurantDeliveryAreasSelector = (state: TReduxState) => TDeliveryAreasModel;
