// Modules
import { createSelector } from 'reselect';
// Utils
import { Consts } from '@utils';
// Types
import { TRestaurantDeliveryAreasSelector } from './types';

const { IDLE, PENDING, SUCCESS, ERROR } = Consts.REQUEST.UI.STATUS;

export const deliveryAreasSelector: TRestaurantDeliveryAreasSelector = ({ restaurant }) =>
  restaurant.info.deliveryAreas;

// address
export const restaurantAddressSuggestSelector = createSelector(
  deliveryAreasSelector,
  ({ address }) => address,
);

export const addressSuggestDataSelector = createSelector(
  restaurantAddressSuggestSelector,
  ({ data }) => data,
);

export const addressSuggestStatusSelector = createSelector(
  restaurantAddressSuggestSelector,
  ({ ui }) => ui.status,
);

export const addressSuggestIsIdleSelector = createSelector(
  addressSuggestStatusSelector,
  (status) => status === IDLE,
);

export const addressSuggestIsPendingSelector = createSelector(
  addressSuggestStatusSelector,
  (status) => status === PENDING,
);

export const addressSuggestIsSuccessSelector = createSelector(
  addressSuggestStatusSelector,
  (status) => status === SUCCESS,
);

export const addressSuggestIsErrorSelector = createSelector(
  addressSuggestStatusSelector,
  (status) => status === ERROR,
);

// geocode
export const restaurantGeocodeSuggestSelector = createSelector(
  deliveryAreasSelector,
  ({ geocode }) => geocode,
);

export const geocodeSuggestDataSelector = createSelector(
  restaurantGeocodeSuggestSelector,
  ({ data }) => data,
);

export const geocodeSuggestStatusSelector = createSelector(
  restaurantGeocodeSuggestSelector,
  ({ ui }) => ui.status,
);

export const geocodeSuggestIsIdleSelector = createSelector(
  geocodeSuggestStatusSelector,
  (status) => status === IDLE,
);

export const geocodeSuggestIsPendingSelector = createSelector(
  geocodeSuggestStatusSelector,
  (status) => status === PENDING,
);

export const geocodeSuggestIsSuccessSelector = createSelector(
  geocodeSuggestStatusSelector,
  (status) => status === SUCCESS,
);

export const geocodeSuggestIsErrorSelector = createSelector(
  geocodeSuggestStatusSelector,
  (status) => status === ERROR,
);

export const restaurantDeliveryAreasSelector = createSelector(
  deliveryAreasSelector,
  ({ deliveryAreas }) => deliveryAreas,
);

export const deliveryAreasDataSelector = createSelector(
  restaurantDeliveryAreasSelector,
  ({ data }) => data,
);

export const deliveryAreasStatusSelector = createSelector(
  restaurantDeliveryAreasSelector,
  ({ ui }) => ui.status,
);

export const deliveryAreasIsIdleSelector = createSelector(
  deliveryAreasStatusSelector,
  (status) => status === IDLE,
);

export const deliveryAreasIsPendingSelector = createSelector(
  deliveryAreasStatusSelector,
  (status) => status === PENDING,
);

export const deliveryAreasIsSuccessSelector = createSelector(
  deliveryAreasStatusSelector,
  (status) => status === SUCCESS,
);

export const deliveryAreasIsErrorSelector = createSelector(
  deliveryAreasStatusSelector,
  (status) => status === ERROR,
);
