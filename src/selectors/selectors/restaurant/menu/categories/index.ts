// Modules
import { createSelector } from 'reselect';
import isNil from 'lodash/isNil';
// Utils
import { Consts } from '@utils';
// Selectors
import { profileAuthSelector } from '@selectors/selectors/profile';
// Config
import { transformResponse } from './config/transform-response';
// Types
import { TRestaurantMenuSelector } from './types';

import { restaurantMenuFavoriteCategorySelector } from '../products';

const { IDLE, PENDING, SUCCESS, ERROR } = Consts.REQUEST.UI.STATUS;

export const restaurantMenuSelector: TRestaurantMenuSelector = ({ restaurant }) => restaurant.menu;

export const restaurantMenuCategoriesDataSelector = createSelector(
  [restaurantMenuSelector],
  ({ categories }) => categories.data,
);

export const restaurantMenuExtendCategoriesDataSelector = createSelector(
  [
    restaurantMenuCategoriesDataSelector,
    profileAuthSelector,
    restaurantMenuFavoriteCategorySelector,
  ],
  (categories, auth, favorites) => {
    if (!isNil(auth) && favorites) {
      return transformResponse([
        ...categories,
        {
          id: '955cae82-favorite',
          name: 'Любимые',
          urlName: 'favorite',
        },
      ]);
    }

    return transformResponse(categories);
  },
);

export const restaurantMenuCategoriesUiStatusSelector = createSelector(
  [restaurantMenuSelector],
  ({ categories }) => categories.ui.status,
);

export const restaurantMenuCategoriesUiStatusIsIdleSelector = createSelector(
  restaurantMenuCategoriesUiStatusSelector,
  (status) => status === IDLE,
);

export const restaurantMenuCategoriesUiStatusIsPendingSelector = createSelector(
  restaurantMenuCategoriesUiStatusSelector,
  (status) => status === PENDING,
);

export const restaurantMenuCategoriesUiStatusIsSuccessSelector = createSelector(
  restaurantMenuCategoriesUiStatusSelector,
  (status) => status === SUCCESS,
);

export const restaurantMenuCategoriesUiStatusIsErrorSelector = createSelector(
  restaurantMenuCategoriesUiStatusSelector,
  (status) => status === ERROR,
);
