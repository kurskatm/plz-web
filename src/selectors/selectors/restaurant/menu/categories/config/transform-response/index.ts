// Lib
import { hardRestCategoriesList } from '@lib/restaurants/categories';
// Types
import { TTransformResponse } from './types';

export const transformResponse: TTransformResponse = (data) =>
  data.map((item) => {
    const extendItem = hardRestCategoriesList.find(({ id }) => id === item.id);

    const result = { ...item };

    if (extendItem) {
      result.icon = extendItem.name;
    }

    return result;
  });
