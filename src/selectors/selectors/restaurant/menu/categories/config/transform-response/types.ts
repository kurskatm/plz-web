// Main Types
import { TRestaurantCategoriesList } from '@type/restaurants/restaurant-categories';

export type TTransformResponse = (data: TRestaurantCategoriesList) => TRestaurantCategoriesList;
