// Main Types
import { TReduxState } from '@models/types';
import { TRestaurantMenuModel } from '@models/restaurant/menu/types';

export type TGetReactCategoryIdSelector = (
  state: TReduxState,
  props: { categoryId: string },
) => string;

export type TRestaurantMenuSelector = (state: TReduxState) => TRestaurantMenuModel;
