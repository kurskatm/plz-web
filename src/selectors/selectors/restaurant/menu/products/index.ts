// Modules
import { createSelector } from 'reselect';
// Utils
import { Consts } from '@utils';
// Types
import { TRestaurantCategoryProducts } from '@/type/restaurants/restaurant-products';
import { TGetReactCategoryIdSelector, TRestaurantMenuSelector } from './types';

const { IDLE, PENDING, SUCCESS, ERROR } = Consts.REQUEST.UI.STATUS;

// @ts-ignore
const getFavorites = (arr) =>
  arr.reduce(
    // @ts-ignore
    (acc, item) => ({
      ...acc,
      products: [
        ...acc.products,
        ...item?.products?.filter(
          (product: Record<string, string>) =>
            product.isFavorite &&
            !acc?.products?.find((x: Record<string, string>) => x.id === product.id),
        ),
      ],
    }),
    {
      id: '955cae82-favorite',
      name: 'Любимые',
      products: [],
      urlName: 'favorite',
    },
  );
const getReactCategoryIdSelector: TGetReactCategoryIdSelector = (_, { categoryId }) => categoryId;

export const restaurantMenuSelector: TRestaurantMenuSelector = ({ restaurant }) => restaurant.menu;

export const restaurantMenuProductsDataSelector = createSelector(
  [restaurantMenuSelector],
  ({ products }) => {
    const favorites: TRestaurantCategoryProducts = getFavorites(products.data);

    if (favorites && favorites.products.length > 0) {
      return [...products.data, favorites];
    }

    return products.data;
  },
);

export const restaurantMenuProductsCategorySelector = createSelector(
  [getReactCategoryIdSelector, restaurantMenuProductsDataSelector],
  (categoryId, products) => products.find(({ id }) => id === categoryId),
);

export const restaurantMenuFavoriteCategorySelector = createSelector(
  [restaurantMenuProductsDataSelector],
  (products) => products.find(({ id }) => id === '955cae82-favorite'),
);

export const restaurantMenuProductsUiStatusSelector = createSelector(
  [restaurantMenuSelector],
  ({ products }) => products.ui.status,
);

export const restaurantMenuProductsUiStatusIsIdleSelector = createSelector(
  restaurantMenuProductsUiStatusSelector,
  (status) => status === IDLE,
);

export const restaurantMenuProductsUiStatusIsPendingSelector = createSelector(
  restaurantMenuProductsUiStatusSelector,
  (status) => status === PENDING,
);

export const restaurantMenuProductsUiStatusIsSuccessSelector = createSelector(
  restaurantMenuProductsUiStatusSelector,
  (status) => status === SUCCESS,
);

export const restaurantMenuProductsUiStatusIsErrorSelector = createSelector(
  restaurantMenuProductsUiStatusSelector,
  (status) => status === ERROR,
);
