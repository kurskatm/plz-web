// Modules
import { createSelector } from 'reselect';
// Utils
import { Consts } from '@utils';
// Types
import { TRestaurantMenuSelector } from './types';

const { IDLE, PENDING, SUCCESS, ERROR } = Consts.REQUEST.UI.STATUS;

export const restaurantMenuSelector: TRestaurantMenuSelector = ({ restaurant }) => restaurant.menu;

export const restaurantMenuPromoDataSelector = createSelector(
  [restaurantMenuSelector],
  ({ promo }) => promo.data,
);

export const restaurantMenuPromoUiStatusSelector = createSelector(
  [restaurantMenuSelector],
  ({ promo }) => promo.ui.status,
);

export const restaurantMenuPromoUiStatusIsIdleSelector = createSelector(
  restaurantMenuPromoUiStatusSelector,
  (status) => status === IDLE,
);

export const restaurantMenuPromoUiStatusIsPendingSelector = createSelector(
  restaurantMenuPromoUiStatusSelector,
  (status) => status === PENDING,
);

export const restaurantMenuPromoUiStatusIsSuccessSelector = createSelector(
  restaurantMenuPromoUiStatusSelector,
  (status) => status === SUCCESS,
);

export const restaurantMenuPromoUiStatusIsErrorSelector = createSelector(
  restaurantMenuPromoUiStatusSelector,
  (status) => status === ERROR,
);
