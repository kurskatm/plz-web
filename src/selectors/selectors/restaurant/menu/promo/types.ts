// Main Types
import { TReduxState } from '@models/types';
import { TRestaurantMenuModel } from '@models/restaurant/menu/types';

export type TRestaurantMenuSelector = (state: TReduxState) => TRestaurantMenuModel;
