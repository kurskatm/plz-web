// Main Types
import { TReduxState } from '@models/types';
import { TRestaurantSearchProductsModel } from '@models/restaurant/search-products/types';

export type TRestaurantSearchProductsSelector = (
  state: TReduxState,
) => TRestaurantSearchProductsModel;
