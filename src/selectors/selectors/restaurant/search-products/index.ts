// Modules
import { createSelector } from 'reselect';
// Utils
import { Consts } from '@utils';
// Types
import { TRestaurantSearchProductsSelector } from './types';

const { IDLE, PENDING, SUCCESS, ERROR } = Consts.REQUEST.UI.STATUS;

export const restaurantSearchProductsSelector: TRestaurantSearchProductsSelector = ({
  restaurant,
}) => restaurant.searchProducts;

export const restaurantSearchStringSelector = createSelector(
  restaurantSearchProductsSelector,
  ({ search }) => search,
);

export const searchProductsSelector = createSelector(
  restaurantSearchProductsSelector,
  ({ products }) => products,
);

export const searchProductsDataSelector = createSelector(
  searchProductsSelector,
  ({ data }) => data,
);

export const searchProductsUiStatusSelector = createSelector(
  searchProductsSelector,
  ({ ui }) => ui.status,
);

export const searchProductsIsIdleSelector = createSelector(
  searchProductsUiStatusSelector,
  (status) => status === IDLE,
);

export const searchProductsIsPendingSelector = createSelector(
  searchProductsUiStatusSelector,
  (status) => status === PENDING,
);

export const searchProductsIsSuccessSelector = createSelector(
  searchProductsUiStatusSelector,
  (status) => status === SUCCESS,
);

export const searchProductsIsErrorSelector = createSelector(
  searchProductsUiStatusSelector,
  (status) => status === ERROR,
);
