// Main Types
import { TReduxState } from '@models/types';
import { TRestaurantReviewsModel } from '@models/restaurant/reviews/types';

export type TRestaurantReviewsSelector = (state: TReduxState) => TRestaurantReviewsModel;
