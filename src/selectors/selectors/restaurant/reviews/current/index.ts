// Modules
import { createSelector } from 'reselect';
// Utils
import { Consts } from '@utils';
// Types
import { TRestaurantReviewsSelector } from './types';

const { IDLE, PENDING, SUCCESS, ERROR } = Consts.REQUEST.UI.STATUS;

export const restaurantReviewsSelector: TRestaurantReviewsSelector = ({ restaurant }) =>
  restaurant.reviews;

export const restaurantReviewsCurrentDataSelector = createSelector(
  [restaurantReviewsSelector],
  ({ current }) => current.data,
);

export const restaurantReviewsCurrentPaginationSelector = createSelector(
  [restaurantReviewsSelector],
  ({ current }) => current.pagination,
);

export const restaurantReviewsCurrentUiStatusSelector = createSelector(
  [restaurantReviewsSelector],
  ({ current }) => current.ui.status,
);

export const restaurantReviewsCurrentUiStatusIsIdleSelector = createSelector(
  restaurantReviewsCurrentUiStatusSelector,
  (status) => status === IDLE,
);

export const restaurantReviewsCurrentUiStatusIsPendingSelector = createSelector(
  restaurantReviewsCurrentUiStatusSelector,
  (status) => status === PENDING,
);

export const restaurantReviewsCurrentUiStatusIsSuccessSelector = createSelector(
  restaurantReviewsCurrentUiStatusSelector,
  (status) => status === SUCCESS,
);

export const restaurantReviewsCurrentUiStatusIsErrorSelector = createSelector(
  restaurantReviewsCurrentUiStatusSelector,
  (status) => status === ERROR,
);
