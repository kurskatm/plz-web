// Modules
import { createSelector } from 'reselect';
import isNil from 'lodash/isNil';
// Lib
import { getHoursAndMinutes } from '@lib/restaurants/getHoursAndMinutes';
import { getCookieCityData } from '@lib/cookie';
// Selectors
import { restaurantMenuProductsDataSelector } from '@selectors/selectors/restaurant/menu/products';
import { profileFavoriteProductsSelector } from '@selectors/selectors/profile';
import { restaurantAndHitsDataSelector } from '@selectors/selectors/restaurants-and-hits';
import { citiesDataSelector } from '@selectors/selectors/cities';
import { restaurantMainInfoDataSelector } from '@selectors/selectors/restaurant/main/info';
import { restaurantMenuPromoDataSelector } from '@selectors/selectors/restaurant/menu/promo';
// Main types
import {
  TRestaurantProduct,
  TRestaurantProductModifiers,
} from '@type/restaurants/restaurant-products';
// Types
import { TRestaurantModalSelector } from './types';

export const restaurantModalSelector: TRestaurantModalSelector = ({ restaurant }) =>
  restaurant.modal;

export const restaurantCloaseModalShowSelector = createSelector(
  restaurantModalSelector,
  ({ closeRestaurantModal }) => closeRestaurantModal.showModal,
);

export const restaurantOtherModalsSelector = createSelector(
  restaurantModalSelector,
  ({ otherModals }) => otherModals,
);

export const restaurantOtherModalsShowSelector = createSelector(
  restaurantOtherModalsSelector,
  ({ showModal }) => showModal,
);

export const restaurantOtherModalTypeSelector = createSelector(
  restaurantOtherModalsSelector,
  ({ modalType }) => modalType,
);

export const restaurantOtherModalsBonusPointsSelector = createSelector(
  restaurantOtherModalsSelector,
  ({ pointsRest }) => pointsRest,
);

export const restaurantOtherModalsDataToUpdateCartSelector = createSelector(
  restaurantOtherModalsSelector,
  ({ dataToUpdateCart }) => dataToUpdateCart,
);

export const restaurantOtherModalsLinkSelector = createSelector(
  restaurantOtherModalsSelector,
  ({ link }) => link,
);

export const restaurantModalRestsLinksSelector = createSelector(
  restaurantAndHitsDataSelector,
  (restaurantsAndHits) => {
    if (restaurantsAndHits?.restaurants?.length) {
      return restaurantsAndHits.restaurants.filter((item) => {
        const closeTime = getHoursAndMinutes(item?.workingTime?.closeTime);
        const closeHours = closeTime.split(':')[0];
        const closeMinutes = closeTime.split(':')[1];
        const currentDate = new Date().setHours(+closeHours, +closeMinutes);
        const deference = Math.floor((currentDate - Date.now()) / 60000);
        return deference > 0;
      });
    }

    return [];
  },
);

export const restaurantModalCityLinkSelector = createSelector(citiesDataSelector, (cities) => {
  if (cities.length) {
    const cityId = getCookieCityData();
    const city = cities.find(({ id }) => id === cityId);
    return city?.urlName;
  }

  return '';
});

export const restaurantModalProductListSelector = createSelector(
  restaurantMenuProductsDataSelector,
  (products) => products.find(({ name }) => name === 'За баллы')?.products,
);

export const showShoppingCardSelector = createSelector(
  restaurantModalSelector,
  ({ shoppingCardModal }) => shoppingCardModal.showModal,
);

export const modifiersModalSelector = createSelector(
  restaurantModalSelector,
  ({ modifiersCardModel }) => modifiersCardModel,
);

export const showModifiersModalSelector = createSelector(
  modifiersModalSelector,
  ({ showModal }) => showModal,
);

export const productIdModifiersModalSelector = createSelector(
  modifiersModalSelector,
  ({ productId }) => productId,
);

export const productInBonusPointSelector = createSelector(
  modifiersModalSelector,
  ({ inBonusPoint }) => inBonusPoint,
);

export const currentProductModifiersModalSelector = createSelector(
  [
    restaurantMenuProductsDataSelector,
    restaurantAndHitsDataSelector,
    restaurantMainInfoDataSelector,
    profileFavoriteProductsSelector,
    productIdModifiersModalSelector,
    productInBonusPointSelector,
  ],
  (products, restaurantsAndHits, restInfo, favouriteProducts, productId, inBonusPoint) => {
    if (!isNil(favouriteProducts) && !isNil(productId)) {
      return favouriteProducts.find(
        (item) => item.id === productId && item.availableInBonusPoints === inBonusPoint,
      );
    }

    if (products.length && !isNil(productId)) {
      const allProducts: TRestaurantProduct[] = [];
      products.forEach((category) => allProducts.push(...category.products));

      const currentProduct = allProducts.find(
        (item) => item.id === productId && item.availableInBonusPoints === inBonusPoint,
      );
      const validProduct: TRestaurantProductModifiers = {
        id: currentProduct.id,
        availableInBonusPoints: currentProduct.availableInBonusPoints,
        description: currentProduct.description,
        imageUrls: currentProduct.imageUrls,
        isFavorite: currentProduct.isFavorite,
        modifiers: currentProduct.modifiers,
        name: currentProduct.name,
        price: currentProduct.price,
        weight: currentProduct.weight,
        restaurant: restInfo,
      };
      return validProduct;
    }

    if (!isNil(restaurantsAndHits) && !isNil(productId)) {
      return restaurantsAndHits.products.find((item) => item.id === productId);
    }

    return null;
  },
);

const promoModalSelector = createSelector(
  restaurantModalSelector,
  ({ promoCardModal }) => promoCardModal,
);

export const promoCardShowModalSelector = createSelector(
  promoModalSelector,
  ({ showModal }) => showModal,
);

export const promoCardIdSelector = createSelector(promoModalSelector, ({ promoId }) => promoId);

export const currentPromoCardSelector = createSelector(
  [restaurantMenuPromoDataSelector, promoCardIdSelector],
  (promosList, promoId) => {
    if (promosList.length && !isNil(promoId)) {
      return promosList.find(({ id }) => id === promoId);
    }

    return null;
  },
);

export const currentPromoCardIndexSelector = createSelector(
  [restaurantMenuPromoDataSelector, promoCardIdSelector],
  (promosList, promoId) => {
    if (promosList.length && !isNil(promoId)) {
      return promosList.findIndex(({ id }) => id === promoId);
    }

    return 0;
  },
);
