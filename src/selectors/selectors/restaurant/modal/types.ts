// Main Types
import { TReduxState } from '@models/types';
import { TRestaurantModalModel } from '@models/restaurant/modal/types';

export type TRestaurantModalSelector = (state: TReduxState) => TRestaurantModalModel;
