// Main Types
import { TReduxState } from '@models/types';
import { TReviewsModel } from '@models/reviews/types';

export type TReviewsSelector = (state: TReduxState) => TReviewsModel;
