// Modules
import { createSelector } from 'reselect';
// Utils
import { Consts } from '@utils';
// Types
import { TReviewsSelector } from './types';

const { IDLE, PENDING, SUCCESS, ERROR } = Consts.REQUEST.UI.STATUS;

export const reviewsSelector: TReviewsSelector = ({ reviews }) => reviews;

export const reviewsDataSelector = createSelector(reviewsSelector, ({ data }) => data);

export const reviewsUiStatusSelector = createSelector(reviewsSelector, ({ ui }) => ui.status);

export const reviewsUiStatusIsIdleSelector = createSelector(
  reviewsUiStatusSelector,
  (status) => status === IDLE,
);

export const reviewsUiStatusIsPendingSelector = createSelector(
  reviewsUiStatusSelector,
  (status) => status === PENDING,
);

export const reviewsUiStatusIsSuccessSelector = createSelector(
  reviewsUiStatusSelector,
  (status) => status === SUCCESS,
);

export const reviewsUiStatusIsErrorSelector = createSelector(
  reviewsUiStatusSelector,
  (status) => status === ERROR,
);
