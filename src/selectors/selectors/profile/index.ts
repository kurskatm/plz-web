/* eslint-disable @typescript-eslint/no-explicit-any */
// Modules
import { createSelector } from 'reselect';
// Main Types
import { TProfileModel } from '@models/profile/types';
// Constants
import { Consts } from '@utils';

const { IDLE, PENDING, SUCCESS, ERROR } = Consts.REQUEST.UI.STATUS;

export const profileSelector = ({ profile }: { profile: TProfileModel }): TProfileModel => profile;

// Auth
export const profileAuthSelector = createSelector(profileSelector, ({ auth }) => auth);

// Favourites
export const profileFavoriteSelector = createSelector(
  profileSelector,
  ({ favorites }) => favorites,
);

export const profileFavoriteRestsSelector = createSelector(
  profileFavoriteSelector,
  ({ data }) => data?.restaurants,
);
export const profileFavoriteProductsSelector = createSelector(
  profileFavoriteSelector,
  ({ data }) => data?.products,
);
export const profileFavoritesStatusSelector = createSelector(
  profileFavoriteSelector,
  ({ ui }) => ui.status,
);
export const profileFavoritesIsPendingSelector = createSelector(
  profileFavoritesStatusSelector,
  (status) => status === PENDING,
);
export const profileFavoritesIsSuccessSelector = createSelector(
  profileFavoritesStatusSelector,
  (status) => status === SUCCESS,
);
export const profileFavoritesIsErrorSelector = createSelector(
  profileFavoritesStatusSelector,
  (status) => status === ERROR,
);

// Info
export const profileInfoSelector = createSelector(profileSelector, ({ info }) => info);
export const profileInfoDataSelector = createSelector(profileInfoSelector, ({ data }) => data);
export const profileUserIdSelector = createSelector(profileInfoDataSelector, (data) => {
  if (data && data.id) {
    return data.id;
  }
  return '';
});

export const profileInfoDataStatusSelector = createSelector(
  profileInfoSelector,
  ({ ui }) => ui.status,
);

export const profileInfoDataUiIsIdleSelector = createSelector(
  profileInfoDataStatusSelector,
  (status) => status === IDLE,
);

export const profileInfoDataUiIsErrorSelector = createSelector(
  profileInfoDataStatusSelector,
  (status) => status === ERROR,
);

// Review
export const profileReviewsSelector = createSelector(profileSelector, ({ reviews }) => reviews);

export const profileReviewsDataSelector = createSelector(
  profileReviewsSelector,
  ({ data }) => data,
);

export const profilePaginationDataSelector = createSelector(
  profileReviewsSelector,
  ({ pagination }) => pagination,
);

export const profileReviewsLengthSelector = createSelector(
  profileReviewsSelector,
  ({ reviewsLength }) => reviewsLength,
);

export const profileReviewsStatusSelector = createSelector(
  profileReviewsSelector,
  ({ ui }) => ui.status,
);

export const profileReviewsDataUiIsIdleSelector = createSelector(
  profileReviewsStatusSelector,
  (status) => status === IDLE,
);

export const profileReviewsDataUiIsPendingSelector = createSelector(
  profileReviewsStatusSelector,
  (status) => status === PENDING,
);

export const profileReviewsDataUiIsSuccessSelector = createSelector(
  profileReviewsStatusSelector,
  (status) => status === SUCCESS,
);

export const profileReviewsDataUiIsErrorSelector = createSelector(
  profileReviewsStatusSelector,
  (status) => status === ERROR,
);

// Friends
export const profileFriendsSelector = createSelector(profileSelector, ({ friends }) => friends);

export const profileFriendsDataSelector = createSelector(
  profileFriendsSelector,
  ({ data }) => data,
);
export const profileFriendsStatusSelector = createSelector(
  profileFriendsSelector,
  ({ ui }) => ui.status,
);

export const profileFriendsIsIdleSelector = createSelector(
  profileFriendsStatusSelector,
  (status) => status === IDLE,
);

export const profileFriendsIsErrorSelector = createSelector(
  profileFriendsStatusSelector,
  (status) => status === ERROR,
);

// Orders
export const profileOrdersSelector = createSelector(profileSelector, ({ orders }) => orders);

export const profileOrdersModalSelector = createSelector(
  profileOrdersSelector,
  ({ modal }) => modal,
);

export const profileOrdersModalDataSelector = createSelector(
  profileOrdersModalSelector,
  ({ showModal }) => showModal,
);

export const profileOrdersModalOrderIdSelector = createSelector(
  profileOrdersModalSelector,
  ({ orderId }) => orderId,
);

// add review
export const profileOrderReviewSelector = createSelector(
  profileOrdersSelector,
  ({ reviews }) => reviews.add,
);

export const profileOrderReviewsListSelector = createSelector(
  profileOrderReviewSelector,
  ({ data }) => data,
);

export const profileOrderReviewStatusSelector = createSelector(
  profileOrderReviewSelector,
  ({ ui }) => ui.status,
);

export const profileOrderReviewStatusIsPending = createSelector(
  profileOrderReviewStatusSelector,
  (status) => status === PENDING,
);

export const profileOrderReviewStatusIsSuccess = createSelector(
  profileOrderReviewStatusSelector,
  (status) => status === SUCCESS,
);

export const profileOrderReviewStatusIsError = createSelector(
  profileOrderReviewStatusSelector,
  (status) => status === ERROR,
);

// LastOrderWithoutReview
export const profileLastOrderSelector = createSelector(
  profileOrdersSelector,
  ({ reviews }) => reviews.lastOrder,
);

export const profileLastOrderDataSelector = createSelector(
  profileLastOrderSelector,
  ({ data }) => data,
);

export const profileLastOrderDataStatusSelector = createSelector(
  profileLastOrderSelector,
  ({ ui }) => ui.status,
);

export const profileLastOrderDataStatusIsIdle = createSelector(
  profileLastOrderDataStatusSelector,
  (status) => status === IDLE,
);

export const profileLastOrderDataStatusIsPending = createSelector(
  profileLastOrderDataStatusSelector,
  (status) => status === PENDING,
);

export const profileLastOrderDataStatusIsSuccess = createSelector(
  profileLastOrderDataStatusSelector,
  (status) => status === SUCCESS,
);

export const profileLastOrderDataStatusIsError = createSelector(
  profileLastOrderDataStatusSelector,
  (status) => status === ERROR,
);

// Current Order
export const profileUserOrderSelector = createSelector(
  profileOrdersSelector,
  ({ userOrder }) => userOrder,
);

export const profileUserOrderDataSelector = createSelector(
  profileUserOrderSelector,
  ({ data }) => data,
);

export const profileUserOrderUiSelector = createSelector(
  profileUserOrderSelector,
  ({ ui }) => ui.status,
);

export const profileUserOrderIsPending = createSelector(
  profileUserOrderUiSelector,
  (status) => status === PENDING,
);

export const profileUserOrderIsSuccess = createSelector(
  profileUserOrderUiSelector,
  (status) => status === SUCCESS,
);

export const profileUserOrderIsError = createSelector(
  profileUserOrderUiSelector,
  (status) => status === ERROR,
);

// Order Contact Me
export const profileOrderContactMeSelector = createSelector(
  profileOrdersSelector,
  ({ contactMe }) => contactMe,
);

export const profileOrderContactMeOrderIdSelector = createSelector(
  profileOrderContactMeSelector,
  ({ orderId }) => orderId,
);

export const profileOrderContactMeStatusSelector = createSelector(
  profileOrderContactMeSelector,
  ({ ui }) => ui.status,
);

export const profileOrderContactMeStatusDetailsSelector = createSelector(
  profileOrderContactMeSelector,
  ({ details }) => details,
);

export const contactMeIsPendingSelector = createSelector(
  profileOrderContactMeStatusSelector,
  (status) => status === PENDING,
);

export const contactMeIsSuccessSelector = createSelector(
  profileOrderContactMeStatusSelector,
  (status) => status === SUCCESS,
);

export const contactMeIsErrorSelector = createSelector(
  profileOrderContactMeStatusSelector,
  (status) => status === ERROR,
);

// Ui statuses for profile update
export const profileInfoUpdateUiStatusSelector = createSelector(
  profileInfoSelector,
  ({ updateUi }) => updateUi,
);

export const profileInfoUpdateIsPendingSelector = createSelector(
  profileInfoUpdateUiStatusSelector,
  ({ status }) => status === PENDING,
);

export const profileInfoUpdateIsSuccessSelector = createSelector(
  profileInfoUpdateUiStatusSelector,
  ({ status }) => status === SUCCESS,
);

export const profileInfoUpdateIsErrorSelector = createSelector(
  profileInfoUpdateUiStatusSelector,
  ({ status }) => status === ERROR,
);

export const profileInfoUpdateDetailsSelector = createSelector(
  profileInfoUpdateUiStatusSelector,
  ({ details }) => details,
);

// Repeat Order from Profile to Cart
export const profileRepeatOrderSelector = createSelector(
  profileOrdersSelector,
  ({ repeatOrder }) => repeatOrder,
);

export const profileRepeatOrderDataSelector = createSelector(
  profileRepeatOrderSelector,
  ({ data }) => data,
);

export const profileOrderIdToRepeatSelector = createSelector(
  profileRepeatOrderSelector,
  ({ orderId }) => orderId,
);

export const profileRepeatOrderRedirectSelector = createSelector(
  profileRepeatOrderSelector,
  ({ needRedirect }) => needRedirect,
);

export const profileRepeatOrderStatusSelector = createSelector(
  profileRepeatOrderSelector,
  ({ ui }) => ui.status,
);

export const repeatOrderIsPendingSelector = createSelector(
  profileRepeatOrderStatusSelector,
  (status) => status === PENDING,
);

export const repeatOrderIsSuccessSelector = createSelector(
  profileRepeatOrderStatusSelector,
  (status) => status === SUCCESS,
);

export const repeatOrderIsErrorSelector = createSelector(
  profileRepeatOrderStatusSelector,
  (status) => status === ERROR,
);

// New orders
export const newOrdersSelector = createSelector(
  profileOrdersSelector,
  ({ newOrders }) => newOrders,
);

export const newOrdersDataSelector = createSelector(newOrdersSelector, ({ data }) => data);

export const newOrdersPaginationSelector = createSelector(
  newOrdersSelector,
  ({ pagination }) => pagination,
);

export const newOrdersAfterSelector = createSelector(newOrdersSelector, ({ after }) => after);

export const newOrdersUiStatusSelector = createSelector(newOrdersSelector, ({ ui }) => ui.status);

export const newOrdersIsIdleSelector = createSelector(
  newOrdersUiStatusSelector,
  (status) => status === IDLE,
);

export const newOrdersIsPendingSelector = createSelector(
  newOrdersUiStatusSelector,
  (status) => status === PENDING,
);

export const newOrdersIsSuccessSelector = createSelector(
  newOrdersUiStatusSelector,
  (status) => status === SUCCESS,
);

export const newOrdersIsErrorSelector = createSelector(
  newOrdersUiStatusSelector,
  (status) => status === ERROR,
);

export const profileOrderModalCurrentOrderSelector = createSelector(
  [newOrdersDataSelector, profileOrdersModalOrderIdSelector],
  (list, orderId) => list.find((order) => order.order.id === orderId),
);
