// Modules
import { createSelector } from 'reselect';
import isNil from 'lodash/isNil';
// Constants
import { Consts } from '@utils';
// Main selector
import { profileSelector } from '..';

const { IDLE, PENDING, SUCCESS, ERROR } = Consts.REQUEST.UI.STATUS;

// Main
const addressSelector = createSelector(profileSelector, ({ newAddress }) => newAddress);

// Data
export const newAddressDataSelector = createSelector(addressSelector, ({ address }) => address);

export const currentNewAddressSelector = createSelector(
  newAddressDataSelector,
  (address) => (!isNil(address) && address.name) ?? '',
);

export const newAddressSuggestDataSelector = createSelector(
  addressSelector,
  ({ suggest }) => suggest.data,
);

// Ui
export const newAddressSuggestStatusSelector = createSelector(
  addressSelector,
  ({ suggest }) => suggest.ui.status,
);

export const newAddressSuggestIsEldeSelector = createSelector(
  newAddressSuggestStatusSelector,
  (status) => status === IDLE,
);

export const newAddressSuggestIsPendingSelector = createSelector(
  newAddressSuggestStatusSelector,
  (status) => status === PENDING,
);

export const newAddressSuggestIsSuccessSelector = createSelector(
  newAddressSuggestStatusSelector,
  (status) => status === SUCCESS,
);

export const newAddressSuggestIsErrorSelector = createSelector(
  newAddressSuggestStatusSelector,
  (status) => status === ERROR,
);
