// Modules
import { createSelector } from 'reselect';
// Constants
import { Consts } from '@utils';
// Main selector
import { profileSelector } from '..';

const { IDLE, PENDING, SUCCESS, ERROR } = Consts.REQUEST.UI.STATUS;

// Main
const geoCodeSelector = createSelector(profileSelector, ({ newGeoCode }) => newGeoCode);

// Data
export const newGeoCodeDataSelector = createSelector(geoCodeSelector, ({ data }) => data);

// Ui
export const newGeoCodeStatusSelector = createSelector(geoCodeSelector, ({ ui }) => ui.status);

export const newGeoCodeIsEldeSelector = createSelector(
  newGeoCodeStatusSelector,
  (status) => status === IDLE,
);

export const newGeoCodeIsPendingSelector = createSelector(
  newGeoCodeStatusSelector,
  (status) => status === PENDING,
);

export const newGeoCodeIsSuccessSelector = createSelector(
  newGeoCodeStatusSelector,
  (status) => status === SUCCESS,
);

export const newGeoCodeIsErrorSelector = createSelector(
  newGeoCodeStatusSelector,
  (status) => status === ERROR,
);
