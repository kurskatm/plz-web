import { TNotification } from '@/models/notifications/types';
import { TReduxState } from '@/models/types';

export type TNotificationsSelector = (state: TReduxState) => TNotification[];
