import { TNotificationsSelector } from './types';

export const notificationsSelector: TNotificationsSelector = ({ notifications }) =>
  notifications.notifications;
