/* eslint-disable @typescript-eslint/no-explicit-any */
// Constants
import { Consts } from '@utils';
// Modules
import { createSelector } from 'reselect';
// Models
import { TUiModel } from '@models/ui/types';

const { IDLE } = Consts.REQUEST.UI.STATUS;

export const authSelector = ({ auth }: { auth: any }): any => auth;

export const authVerifyPhoneSelector = createSelector(authSelector, (data) => data.verifyPhone);
export const authVerifyOtpSelector = createSelector(authSelector, (data) => data.verifyOtp);
export const authModalSelector = createSelector(authSelector, (data) => data.modal);
export const authVerifyPhoneStatusSelector = createSelector(
  authVerifyPhoneSelector,
  ({ ui }: { ui: TUiModel }) => ui.status,
);
export const authVerifyOtpStatusSelector = createSelector(
  authVerifyOtpSelector,
  ({ ui }: { ui: TUiModel }) => ui.status,
);
export const authResendTokenSelector = createSelector(
  authVerifyPhoneSelector,
  ({ data }) => data.resendToken,
);
export const authAccessTokenSelector = createSelector(
  authVerifyOtpSelector,
  ({ data }) => data.access_token,
);

export const authVerifyPhoneStatusIsIdleSelector = createSelector(
  authVerifyPhoneStatusSelector,
  (status) => status === IDLE,
);
export const authVerifyOtpStatusIsIdleSelector = createSelector(
  authVerifyOtpStatusSelector,
  (status) => status === IDLE,
);
export const authModalShowSelector = createSelector(authModalSelector, (data) => data.show);
export const authModalPhoneSelector = createSelector(authModalSelector, (data) => data.phone);

export const authVerifyPhoneStatusDetails = createSelector(
  authVerifyPhoneSelector,
  ({ ui }: { ui: TUiModel }) => ui.details,
);
