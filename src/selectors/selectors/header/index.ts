// Types
import { TReduxState } from '@models/types';

// Ux
export const headerUxHeightSelector = (state: TReduxState): number => state.header.ux.height;
