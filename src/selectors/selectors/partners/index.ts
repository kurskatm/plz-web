/* eslint-disable @typescript-eslint/no-explicit-any */
// Constants
import { Consts } from '@utils';
// Modules
import { createSelector } from 'reselect';

const { IDLE, ERROR } = Consts.REQUEST.UI.STATUS;

export const partnersSelector = ({ partners }: { partners: any }): any => partners;

export const partnersCounterSelector = createSelector(partnersSelector, (data) => data.counters);

export const partnersCounterSelectorStatus = createSelector(
  partnersCounterSelector,
  ({ ui }) => ui.status,
);

export const partnersCounterSelectorData = createSelector(
  partnersCounterSelector,
  ({ data }) => data,
);

export const partnersMembersSelector = createSelector(partnersSelector, (data) => data.members);

export const partnersMemberAddedStatusSelector = createSelector(
  partnersMembersSelector,
  ({ ui }) => ui.status,
);

export const partnersMemberAddedStatusIsErrorSelector = createSelector(
  partnersMemberAddedStatusSelector,
  (status) => status === ERROR,
);

export const partnersCounterSelectorStatusIsIdle = createSelector(
  partnersCounterSelectorStatus,
  (status) => status === IDLE,
);

export const partnersCounterSelectorStatusIsError = createSelector(
  partnersCounterSelectorStatus,
  (status) => status === ERROR,
);
