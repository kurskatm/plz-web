/* eslint-disable @typescript-eslint/no-explicit-any */
// Constants
import { Consts } from '@utils';
// Modules
import { createSelector } from 'reselect';

const { IDLE, ERROR } = Consts.REQUEST.UI.STATUS;

const idSelector = (_: any, ownProps: any): string =>
  ownProps?.restaurantId || ownProps?.item?.restaurant?.id;

export const restaurantsSelector = ({ restaurants }: { restaurants: any }): any => restaurants;

export const restaurantsDataSelector = createSelector(restaurantsSelector, ({ data }) => data);

export const restaurantsStatusSelector = createSelector(restaurantsSelector, ({ ui }) => ui.status);

export const restaurantsDataUiIsIdleSelector = createSelector(
  restaurantsStatusSelector,
  (status) => status === IDLE,
);

export const restaurantsDataUiIsErrorSelector = createSelector(
  restaurantsStatusSelector,
  (status) => status === ERROR,
);

export const getRestaurantById = createSelector([restaurantsDataSelector, idSelector], (data, id) =>
  data.find((item: any) => item.id === id),
);

export const getRestaurantLogoById = createSelector(
  getRestaurantById,
  (restaurant) => restaurant?.logo,
);
