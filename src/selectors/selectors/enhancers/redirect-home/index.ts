// Modules
import { createSelector } from 'reselect';
import urlJoin from 'url-join';
// Selectors
import { getCity } from '../../cities';

export const enhanceIsRedirectHomeSelector = createSelector([getCity], (city) => {
  if (city) {
    const url = urlJoin('/', city.urlName);
    return url;
  }

  return null;
});
