// Types
import { TReduxState } from '@models/types';

export type TCityUrlSelector = (state: TReduxState, props: unknown) => string | undefined;
