// Modules
import { createSelector } from 'reselect';
import isEmpty from 'lodash/isEmpty';
import isNil from 'lodash/isNil';
import isString from 'lodash/isString';
// Selectors
import { citiesDataSelector } from '../../cities';
// Types
import { TCityUrlSelector } from './types';

const cityUrlSelector: TCityUrlSelector = (_, { computedMatch }) => computedMatch.params?.cityUrl;

export const enhanceWithCheckCitySelector = createSelector(
  [citiesDataSelector, cityUrlSelector],
  (cities, cityUrl) => {
    const isNilCityUrl = isNil(cityUrl);

    if (!isNilCityUrl) {
      const isValidCityUrl = isString(cityUrl) && !isEmpty(cityUrl);

      if (isValidCityUrl) {
        const city = cities.find(({ urlName }) => urlName === cityUrl);

        if (!isNil(city)) {
          return true;
        }
      }

      return false;
    }

    return true;
  },
);
