// Modules
import { createSelector } from 'reselect';
// Utils
import { Consts } from '@utils';
// Main Types
import { TCuisinesModel, TCuisinesList } from '@models/cuisines/types';
import { TUiModel } from '@models/ui/types';

const { IDLE, SUCCESS, PENDING, ERROR } = Consts.REQUEST.UI.STATUS;

export const cuisinesDataSelector = ({ cuisines }: { cuisines: TCuisinesModel }): TCuisinesList =>
  cuisines.list.data;

export const cuisinesDataUiSelector = ({ cuisines }: { cuisines: TCuisinesModel }): TUiModel =>
  cuisines.list.ui;

export const cuisinesChosenSelector = ({ cuisines }: { cuisines: TCuisinesModel }): string[] =>
  cuisines.chosen;

export const cuisinesDataUiIsIdleSelector = createSelector(
  cuisinesDataUiSelector,
  ({ status }) => status === IDLE,
);

export const cuisinesDataUiIsSuccessSelector = createSelector(
  cuisinesDataUiSelector,
  ({ status }) => status === SUCCESS,
);

export const cuisinesDataUiIsPendingSelector = createSelector(
  cuisinesDataUiSelector,
  ({ status }) => status === PENDING,
);

export const cuisinesDataUiIsErrorSelector = createSelector(
  cuisinesDataUiSelector,
  ({ status }) => status === ERROR,
);
