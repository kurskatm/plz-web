/* eslint-disable @typescript-eslint/no-explicit-any */
// Constants
import { Consts } from '@utils';
// Modules
import { createSelector } from 'reselect';

const { IDLE } = Consts.REQUEST.UI.STATUS;

export const hitsSelector = ({ hits }: { hits: any }): any => hits;

export const hitsDataSelector = createSelector(hitsSelector, ({ data }) => data);

export const hitsStatusSelector = createSelector(hitsSelector, ({ ui }) => ui.status);

export const hitsDataUiIsIdleSelector = createSelector(
  hitsStatusSelector,
  (status) => status === IDLE,
);
