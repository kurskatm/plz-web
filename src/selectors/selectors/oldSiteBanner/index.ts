// Modules
import { createSelector } from 'reselect';
// Types
import { TOldSiteBannerSelector } from './types';

export const oldSiteBannerSelector: TOldSiteBannerSelector = ({ oldSiteBanner }) => oldSiteBanner;

export const isBannerFullShowSelector = createSelector(
  oldSiteBannerSelector,
  ({ isFull }) => isFull,
);
