// Main Types
import { TReduxState } from '@models/types';
import { TOldSiteBannerModel } from '@models/oldSiteBanner/types';

export type TOldSiteBannerSelector = (state: TReduxState) => TOldSiteBannerModel;
