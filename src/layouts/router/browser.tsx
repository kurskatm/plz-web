import React, { FC, memo } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { QueryParamProvider } from 'use-query-params';

import { TBrowserRouterProps } from './types';

const BrowserRouterComponent: FC<TBrowserRouterProps> = ({ children }) => (
  // @ts-ignore
  <Router basename="/">
    <QueryParamProvider ReactRouterRoute={Route}>{children}</QueryParamProvider>
  </Router>
);

export default memo(BrowserRouterComponent);
