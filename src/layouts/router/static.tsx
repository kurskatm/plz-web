// Modules
import React, { FC, memo } from 'react';
import { StaticRouter as Router, Route } from 'react-router-dom';
import { QueryParamProvider } from 'use-query-params';

import { TStaticRouterProps } from './types';

const StaticRouterComponent: FC<TStaticRouterProps> = ({ children, path }) => (
  <Router location={path}>
    <QueryParamProvider ReactRouterRoute={Route}>{children}</QueryParamProvider>
  </Router>
);

export default memo(StaticRouterComponent);
