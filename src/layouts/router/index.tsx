import React, { FC, memo } from 'react';

import { Consts } from '@utils';
import BrowserRouter from './browser';
import StaticRouter from './static';
import { TRouterProps } from './types';

const { IS_CLIENT } = Consts.ENV;

const RouterComponent: FC<TRouterProps> = ({ children, path }) => {
  if (IS_CLIENT) {
    return <BrowserRouter>{children}</BrowserRouter>;
  }

  return <StaticRouter path={path}>{children}</StaticRouter>;
};

export default memo(RouterComponent);
