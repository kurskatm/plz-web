import { ReactNode } from 'react';

export type TRouterProps = {
  children: ReactNode;
  path: string;
};

export type TStaticRouterProps = {
  children: unknown;
  path: string;
};

export type TBrowserRouterProps = {
  children: unknown;
};
