// Modules
import { Dispatch, SetStateAction } from 'react';
// Types
import { TCitiesItem, TCitiesList } from '@type/cities';

export type TCity = string | void | null | undefined;

export type TUpdateSupposedCity = Dispatch<SetStateAction<TCitiesItem | null>>;

export interface TSearchCityManagerConstructor {
  cities: TCitiesList;
  updateSupposedCity: TUpdateSupposedCity;
}

export type TSearchCity = (city: TCity) => void;
