// Modules
import isString from 'lodash/isString';
import isNil from 'lodash/isNil';
// Main Types
import { TCitiesList } from '@type/cities';
// Types
import { TSearchCity, TSearchCityManagerConstructor, TUpdateSupposedCity } from './types';

export class SearchCityManager {
  private readonly cities: TCitiesList;
  private readonly updateSupposedCity: TUpdateSupposedCity;

  constructor(data: TSearchCityManagerConstructor) {
    this.cities = data.cities;
    this.updateSupposedCity = data.updateSupposedCity;
  }

  public searchCity: TSearchCity = (city) => {
    if (isString(city) && city.length) {
      const validCity = city.toLowerCase();
      const cityObj = this.cities.find(({ name }) => name.toLowerCase() === validCity);

      if (!isNil(cityObj)) {
        this.updateSupposedCity(cityObj);
      }
    }
  };
}
