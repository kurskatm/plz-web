// Modules
import { Dispatch, SetStateAction } from 'react';
// Types
import { TCitiesList, TCitiesItem } from '@type/cities';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type TGeoCoderLocation = any;

export interface TGeoCoderError {
  message: string;
}

export type TUpdateSupposedCity = Dispatch<SetStateAction<TCitiesItem | null>>;

export interface TCreateYandexLocationConstructor {
  cities: TCitiesList;
  updateSupposedCity: TUpdateSupposedCity;
}
