// Modules
import isNil from 'lodash/isNil';
// Utils
import { Consts } from '@utils';
// Main Types
import { TCitiesList } from '@type/cities';
// MainLib
import { Cookies } from '@lib/cookie';
import { SearchCityManager } from '@lib/city';
// Lib
import { LOCATION_OPTIONS, SCRIPT_ID } from '../config';
// Types
import {
  TCreateYandexLocationConstructor,
  TGeoCoderLocation,
  TGeoCoderError,
  TUpdateSupposedCity,
} from './types';

const { IS_CLIENT, YANDEX_KEY } = Consts.ENV;
const { CITY_DATA } = Consts.COOKIE.DATA;

export class CreateYandexLocation {
  private readonly cities: TCitiesList;
  private readonly SearchCityManager: SearchCityManager;
  private readonly updateSupposedCity: TUpdateSupposedCity;

  constructor(data: TCreateYandexLocationConstructor) {
    this.cities = data.cities;
    this.updateSupposedCity = data.updateSupposedCity;
    this.SearchCityManager = new SearchCityManager({
      cities: this.cities,
      updateSupposedCity: this.updateSupposedCity,
    });
  }

  public inject = (): void => {
    if (IS_CLIENT) {
      const geoDataCookie = Cookies.get(CITY_DATA);

      if (isNil(geoDataCookie)) {
        this.createScript();
      }
    }
  };

  private createUrl = (): string => {
    const { PROTOCOL, URL, VERSION, LANG, FORMAT, RESULTS } = LOCATION_OPTIONS;
    const params = `?lang=${LANG}&apikey=${YANDEX_KEY}&format=${FORMAT}&results=${RESULTS}`;
    const url = `${PROTOCOL}://${URL}/${VERSION}${params}`;

    return url;
  };

  private setCallback = (): void => {
    window.ymaps.ready(this.getLocation);
  };

  private getLocation = (): void => {
    // @ts-ignore
    const { geolocation } = window.ymaps;

    geolocation
      .get({
        provider: 'yandex',
        autoReverseGeocode: true,
      })
      .then((location: TGeoCoderLocation) => {
        if (location) {
          try {
            // const cityObj = location.geoObjects.get(0).properties.get('metaDataProperty')
            //   .GeocoderMetaData.AddressDetails.Country.AdministrativeArea;

            // const city = location.geoObjects.get(0).getLocalities()[0] as string;

            const city = location.geoObjects.get(0).getLocalities()[0] as string;

            if (!isNil(city)) {
              this.SearchCityManager.searchCity(city);
            }
          } catch (error) {
            console.error(error);
          }
        }
      })
      .catch((error: TGeoCoderError) => {
        console.error(error);
      });
  };

  private createScript = (): void => {
    const scriptNode = document.getElementById(SCRIPT_ID);

    if (isNil(scriptNode)) {
      const script = document.createElement('script');

      script.id = SCRIPT_ID;
      script.type = 'text/javascript';
      script.src = this.createUrl();
      script.onload = this.setCallback;

      document.head.appendChild(script);
    } else {
      this.setCallback();
    }
  };
}
