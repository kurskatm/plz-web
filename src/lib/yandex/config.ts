export const SCRIPT_ID = 'yandex-geocoder-script';

export const LOCATION_OPTIONS = {
  PROTOCOL: 'https',
  URL: 'api-maps.yandex.ru',
  VERSION: '2.1',
  LANG: 'ru_RU',
  FORMAT: 'json',
  RESULTS: 1,
};
