// Modules
import isNil from 'lodash/isNil';
import { fromEvent } from 'rxjs';
import { tap, throttleTime } from 'rxjs/operators';
// Utils
import { Consts } from '@utils';
// Lib
import { LOCATION_OPTIONS, SCRIPT_ID } from '../config';
// Types
import {
  TYandexAutoLocationConstructor,
  TSetAddress,
  TSetIsAutoLocation,
  TSetAutoLocationPending,
  TSetIsCityValid,
  TObservable,
  TSubscription,
  TGeoCoderError,
  TGeoCoderLocation,
  TCitiesList,
} from './types';

const { IS_CLIENT, YANDEX_KEY } = Consts.ENV;

const EVENT_THROTTLE = 200;

export class AutoYandexLocation {
  private readonly setAddress: TSetAddress;
  private readonly setIsAutoLocation: TSetIsAutoLocation;
  private readonly setAutoLocationPending: TSetAutoLocationPending;
  private readonly setIsCityValid: TSetIsCityValid;
  private readonly autoLocationRef: HTMLDivElement;
  private readonly cities: TCitiesList;

  private refClickObservable: TObservable;
  private refClickSubscribe: TSubscription;

  constructor(data: TYandexAutoLocationConstructor) {
    this.setAddress = data.setAddress;
    this.setIsAutoLocation = data.setIsAutoLocation;
    this.setAutoLocationPending = data.setAutoLocationPending;
    this.setIsCityValid = data.setIsCityValid;
    this.autoLocationRef = data.autoLocationRef;
    this.cities = data.cities;

    this.refClickObservable = this.divClickEvent();
    this.refClickSubscribe = this.refClickObservable.subscribe();
  }

  private divClickEvent = () =>
    fromEvent(this.autoLocationRef, 'click').pipe(
      throttleTime(EVENT_THROTTLE),
      tap(() => {
        this.initScript();
      }),
    );

  private createUrl = (): string => {
    const { PROTOCOL, URL, VERSION, LANG, FORMAT, RESULTS } = LOCATION_OPTIONS;
    const params = `?lang=${LANG}&apikey=${YANDEX_KEY}&format=${FORMAT}&results=${RESULTS}`;
    const url = `${PROTOCOL}://${URL}/${VERSION}${params}`;

    return url;
  };

  private setCallback = (): void => {
    window.ymaps.ready(this.getLocation);
    this.setAutoLocationPending(true);
  };

  private catchError = (): void => {
    this.setAutoLocationPending(false);
    this.setIsCityValid(false);
    this.setIsAutoLocation(true);
  };

  private isExactCheck = (precision: string, kind: string): boolean => {
    if (precision === 'exact') {
      return true;
    }

    if (kind === 'house' || kind === 'street') {
      return true;
    }

    return false;
  };

  private getLocation = (): void => {
    // @ts-ignore
    const { geolocation } = window.ymaps;

    geolocation
      .get()
      .then((location: TGeoCoderLocation) => {
        if (location) {
          try {
            this.setAutoLocationPending(false);

            const currentGeoObject = location.geoObjects.get(0);

            const kindPrm = currentGeoObject.properties.get('metaDataProperty').GeocoderMetaData
              .kind;

            const precisionPrm = currentGeoObject.properties.get('metaDataProperty')
              .GeocoderMetaData.precision;

            const city = currentGeoObject.getLocalities()[0] as string;

            const address = currentGeoObject.getAddressLine();

            const isCityIncluded = this.cities
              .map(({ name }) => name.toLowerCase())
              .includes(city.toLowerCase());

            if (this.isExactCheck(precisionPrm, kindPrm) && isCityIncluded) {
              // идеальные условия точности
              this.setAddress(address);
              this.setIsAutoLocation(true);
              this.setIsCityValid(true);
            } else if (!this.isExactCheck(precisionPrm, kindPrm) && isCityIncluded) {
              // Не смогли определить локацию
              this.setAddress(address);
              this.setIsAutoLocation(true);
              this.setIsCityValid(false);
            } else if (!isCityIncluded) {
              // нас в этом городе нет
              this.setIsAutoLocation(false);
              this.setIsCityValid(false);
            }
          } catch (error) {
            console.error(error);
          }
        }
      })
      .catch((error: TGeoCoderError) => {
        console.error(error);
        this.catchError();
      });
  };

  private initScript = (): void => {
    if (IS_CLIENT) {
      const scriptNode = document.getElementById(SCRIPT_ID);

      if (isNil(scriptNode)) {
        const script = document.createElement('script');

        script.id = SCRIPT_ID;
        script.type = 'text/javascript';
        script.src = this.createUrl();
        script.onload = this.setCallback;

        document.head.appendChild(script);
      } else {
        this.setCallback();
      }
    }
  };

  public unsubscribe = (): void => {
    if (!isNil(this.refClickSubscribe)) {
      this.refClickSubscribe.unsubscribe();
      this.refClickSubscribe = null;
    }
  };
}
