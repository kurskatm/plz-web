// Modules
import { Dispatch, SetStateAction } from 'react';
import { Observable, Subscription } from 'rxjs';
// Types
import { TCitiesList } from '@type/cities';

export type TObservable = Observable<Event> | null;
export type TSubscription = Subscription | null;

export type TSetAddress = Dispatch<SetStateAction<string>>;
export type TSetIsAutoLocation = Dispatch<SetStateAction<boolean>>;
export type TSetAutoLocationPending = Dispatch<SetStateAction<boolean>>;
export type TSetIsCityValid = Dispatch<SetStateAction<boolean>>;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type TGeoCoderLocation = any;

export interface TGeoCoderError {
  message: string;
}

export interface TYandexAutoLocationConstructor {
  setAddress: TSetAddress;
  setIsAutoLocation: TSetIsAutoLocation;
  setAutoLocationPending: TSetAutoLocationPending;
  setIsCityValid: TSetIsCityValid;
  autoLocationRef: HTMLDivElement;
  cities: TCitiesList;
}

export { TCitiesList };
