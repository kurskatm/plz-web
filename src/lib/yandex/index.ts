import { CreateYandexLocation } from './location';
import { AutoYandexLocation } from './autoLocation';

export { CreateYandexLocation, AutoYandexLocation };
