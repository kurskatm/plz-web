export const getItemsCountInRow = (
  breakpointsObj: { [key: string]: string },
  size: number,
): string => {
  if (!breakpointsObj || !size) {
    return 's';
  }
  let prevPoint = 0;

  const breakpoints = Object.keys(breakpointsObj);
  const currentPoint = breakpoints.find((val: string) => {
    const result = size >= prevPoint && size <= Number(val);
    prevPoint = Number(val);

    return result;
  }, 0);

  return breakpointsObj[currentPoint] || 's';
};
