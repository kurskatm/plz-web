export const BREAKPOINTS = {
  0: 's',
  795: 's',
  1180: 'm',
  1550: 'l',
  2560: 'x',
};

export const COLS_RULES = {
  s: 1,
  m: 2,
  l: 3,
  x: 4,
};

export const ROWS_RULES = {
  s: 8,
  m: 6,
  l: 4,
  x: 3,
};
