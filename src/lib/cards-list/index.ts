import { useWindowSize } from './hooks/useWindowSize';
import { getItemsCountInRow } from './getItemsCountInRow';
import { makeTable } from './makeTable';

export { useWindowSize, getItemsCountInRow, makeTable };
