type TUnknownList = unknown[];

export const makeTable = (list: TUnknownList, maxCols: number, maxRows: number): TUnknownList[] => {
  const rows = maxRows;
  const itemsTable = [...Array(rows)].map(() => []);
  let currentRowIndex = 0;
  let currentColIndex = 0;

  list.forEach((item: unknown) => {
    if (currentRowIndex < rows) {
      if (currentColIndex < maxCols) {
        itemsTable[currentRowIndex].push(item);
        currentColIndex += 1;
      } else if (currentRowIndex + 1 < rows) {
        currentRowIndex += 1;
        itemsTable[currentRowIndex].push(item);
        currentColIndex = 1;
      }
    }
  });

  return itemsTable;
};
