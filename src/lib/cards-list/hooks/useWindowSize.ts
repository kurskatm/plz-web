import { useLayoutEffect, useEffect, useState } from 'react';
// Utils
import { Consts } from '@utils';

const { IS_CLIENT } = Consts.ENV;

const useIsomorphicLayoutEffect = IS_CLIENT ? useLayoutEffect : useEffect;

export function useWindowSize(): number[] {
  const [size, setSize] = useState([0, 0]);

  useIsomorphicLayoutEffect(() => {
    function updateSize() {
      setSize([window.innerWidth, window.innerHeight]);
    }
    window.addEventListener('resize', updateSize);
    updateSize();
    return () => window.removeEventListener('resize', updateSize);
  }, []);

  return size;
}
