// Modules
import isNil from 'lodash/isNil';
import uniq from 'lodash/uniq';
// Types
import { TGetCorrectSentenceCheck, TGetValidSymbolsInSentenceCheck } from './types';

export const checkOfCorrectSentence: TGetCorrectSentenceCheck = (str, lng) => {
  const lowStr = str.toLowerCase();
  for (let i = 0; i < lowStr.length - lng + 1; i += 1) {
    let j = i;
    const indices: number[] = [];
    while (j !== i + lng) {
      if (lowStr[j] === lowStr[i]) {
        indices.push(i);
      }
      j += 1;
    }
    if (indices.length === lng) {
      return false;
    }
  }

  return true;
};

export const checkSymbolsInSentence: TGetValidSymbolsInSentenceCheck = (str, lng = 5) => {
  const lowStr = str.toLowerCase();

  const latinMask = new RegExp(/[a-z]/gi);
  const kirilMask = new RegExp(/[а-я]/gi);
  const numericMask = new RegExp(/[0-9]/gi);
  const specialSymbolsMask = new RegExp(/[@$!%*?&]/gi);
  const all = []
    .concat(lowStr.match(latinMask))
    .concat(lowStr.match(kirilMask))
    .concat(lowStr.match(numericMask))
    .concat(lowStr.match(specialSymbolsMask));
  let matched = all.filter((elem) => !isNil(elem));
  matched = uniq(matched);
  return matched?.length > lng;
};
