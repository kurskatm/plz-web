export type TGetCorrectSentenceCheck = (str: string, lng: number) => boolean;

export type TGetValidSymbolsInSentenceCheck = (str: string, lng?: number) => boolean;
