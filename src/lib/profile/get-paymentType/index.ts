// Types
import { TGetPaymentType } from './types';

export const getPaymentType: TGetPaymentType = (data) => {
  if (data === 1) {
    return 'Наличными';
  }
  if (data === 2) {
    return 'Картой курьеру';
  }
  if (data === 4) {
    return 'Картой онлайн';
  }
  if (data === 8) {
    return 'ApplePay';
  }
  if (data === 16) {
    return 'GooglePay';
  }
  return 'Неизвестно';
};
