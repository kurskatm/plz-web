// Types
import { TGetOrderStatus } from './types';

export const getOrderStatus: TGetOrderStatus = (status) => {
  if (status === 0) {
    return 'Заказ создан';
  }

  if (status === 1) {
    return 'Заказ в обработке';
  }

  if (status === 2) {
    return 'Заказ доставлен';
  }

  if (status === 3) {
    return 'Заказ отменён';
  }

  if (status === 4) {
    return 'Заказ принят';
  }

  if (status === 5) {
    return 'Заказ ожидает онлайн оплаты';
  }

  return 'Заказ ожидает ответа по интеграции с партнёром';
};
