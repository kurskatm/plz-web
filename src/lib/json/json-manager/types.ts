export type TJsonParse = (data: unknown) => unknown | null;

export type TJsonStringify = (data: unknown) => unknown | null;
