// Modules
import isNil from 'lodash/isNil';
import isEmpty from 'lodash/isEmpty';
import isString from 'lodash/isString';
// Types
import { TJsonParse, TJsonStringify } from './types';

export class JsonManager {
  jsonParse: TJsonParse = (data) => {
    if (isString(data) && !isEmpty(data)) {
      try {
        const parseData = JSON.parse(data);
        return parseData;
      } catch (error) {
        console.error(error);
        return null;
      }
    }

    return null;
  };

  jsonStringify: TJsonStringify = (data) => {
    if (!isNil(data)) {
      try {
        const strData = JSON.stringify(data);
        return strData;
      } catch (error) {
        console.log(error);
        return null;
      }
    }

    return null;
  };
}
