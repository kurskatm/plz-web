import { TLikeClickHandlerProps } from './types';

export const likeClickHandler = ({
  id,
  type,
  additionalData,
  auth,
  likeAction,
  unLikeAction,
}: TLikeClickHandlerProps) => (e: Event, liked: boolean, cb: () => void): void => {
  e.preventDefault();
  if (auth) {
    auth(true);
  } else {
    cb();

    if (liked) {
      unLikeAction({
        [type]: id,
        ...(additionalData ?? {}),
      });
    } else {
      likeAction({
        [type]: id,
        ...(additionalData ?? {}),
      });
    }
  }
};
