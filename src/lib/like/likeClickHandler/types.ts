import { ActionCreator } from '@type/actions';

export type TLikeClickHandlerProps = {
  id: string;
  type: string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  additionalData?: Record<string, any>;
  auth?: (data: boolean) => void;
  likeAction: ActionCreator<string>;
  unLikeAction: ActionCreator<string>;
};
