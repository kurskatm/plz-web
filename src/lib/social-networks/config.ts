// Modules
import keyBy from 'lodash/keyBy';
// Utils
import { trackEvent } from '@utils/analytics';

const defaultParamsSocialNetwork = {
  newTab: true,
  width: 32,
  height: 32,
};

const socialNetworkConfig = [
  {
    name: 'vk',
    link: 'https://vk.com/share.php?url=',
    onClick: () => {
      trackEvent('приглашениесоцсети');
    },
  },
  // {
  //   name: 'instagram',
  //   link: 'https://vk.com/share.php?url=',
  // },
  // {
  //   name: 'whatsApp',
  //   link: 'https://www.instagram.com/chibbis/',
  // },
  // {
  //   name: 'fb',
  //   link: 'https://www.facebook.com/sharer/sharer.php?u=',
  // },
  // {
  //   name: 'viber',
  //   link: 'https://www.instagram.com/chibbis/',
  // },
  {
    name: 'ok',
    link: 'https://connect.ok.ru/dk?st.cmd=WidgetSharePreview&st.shareUrl=',
    onClick: () => {
      trackEvent('приглашениесоцсети');
    },
  },
];

const socialNetworkMainConfig = [
  {
    name: 'vk',
    link: 'https://vk.com/chibbis_rus/',
  },
  // {
  //   name: 'instagram',
  //   link: 'https://www.instagram.com/chibbis/',
  // },
  {
    name: 'tiktok',
    link: 'https://www.tiktok.com/@chibbis.ru',
  },
];

export const socialNetworkByName = keyBy(socialNetworkConfig, 'name');

export const socialNetworkList = socialNetworkMainConfig.map((item) => ({
  ...defaultParamsSocialNetwork,
  ...item,
}));

export const socialNetworkFullList = socialNetworkConfig.map((item) => ({
  ...defaultParamsSocialNetwork,
  ...item,
}));
