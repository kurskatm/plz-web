// Modules
import { fromEvent } from 'rxjs';
import { debounceTime, tap } from 'rxjs/operators';
import isNil from 'lodash/isNil';
import throttle from 'lodash/throttle';
// Config
import { WIDTH } from './config';
// Types
import { TEvent, TObservable, TSubscription, TCitiesListHeightManageConstructor } from './types';

export class CitiesListHeightManage {
  private readonly blockRef: HTMLDivElement;
  private width: number | null;
  private height: number | null;
  private colls: number | null;
  private isMobile: boolean;
  private resizeObservable: TSubscription;

  constructor(data: TCitiesListHeightManageConstructor) {
    this.setColls = throttle(this.setColls, 400);

    this.blockRef = data.blockRef;
    this.width = null;
    this.height = null;
    this.colls = null;
    this.isMobile = window.innerWidth < 560;
    this.resizeObservable = this.resizeWindow().subscribe();

    this.init();
  }

  private setColls = (): void => {
    const newWidth = this.blockRef.clientWidth;

    if (newWidth) {
      this.width = newWidth;
      this.blockRef.style.cssText = `
        opacity: 0;
        max-height: auto;
        flex-wrap: nowrap;
      `;

      const blockHeight = this.blockRef.offsetHeight;
      this.colls = Math.floor(this.width / WIDTH);
      this.height = Math.ceil(blockHeight / this.colls);

      this.height += 40;

      this.blockRef.style.cssText = `
        max-height: ${this.height}px;
        flex-wrap: wrap;
        opacity: 1;
      `;
    }
  };

  resetStyles = (): void => {
    this.blockRef.style.cssText = `
        max-height: auto;
        flex-wrap: wrap;
        opacity: 1;
      `;
  };

  private resizeWindow = (): TObservable =>
    fromEvent(window, 'resize').pipe(
      debounceTime(200),
      tap<TEvent>((e) => {
        if (e.target.innerWidth > 500) {
          this.isMobile = false;
          this.setColls();
        } else if (!this.isMobile) {
          this.isMobile = true;
          this.resetStyles();
        }
      }),
    );

  private init = (): void => {
    if (this.isMobile) {
      this.setColls();
    }
  };

  public getColls = (): void => {
    if (this.isMobile) {
      this.resetStyles();
    } else {
      this.setColls();
    }
  };

  public unsubscribe = (): void => {
    if (!isNil(this.resizeObservable) && this.resizeObservable.closed === false) {
      this.resizeObservable.unsubscribe();
    }
  };
}
