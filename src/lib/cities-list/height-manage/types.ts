// Modules
import { Observable, Subscription } from 'rxjs';

export type TObservable = Observable<Event>;
export type TSubscription = Subscription | null;
export interface TEvent extends Event {
  target: {
    innerWidth: number;
  } & EventTarget;
}

export interface TCitiesListHeightManageConstructor {
  blockRef: HTMLDivElement;
}
