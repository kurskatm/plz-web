// Types
import { Dispatch, SetStateAction } from 'react';
import { TCitiesList } from '@type/cities';

export type TInitialState = TCitiesList[];

export type TSelectedCities = ({
  cities,
  search,
  setCitiesReact,
}: {
  cities: TCitiesList;
  search: string | undefined;
  setCitiesReact?: Dispatch<SetStateAction<TCitiesList[]>>;
}) => TCitiesList[];
