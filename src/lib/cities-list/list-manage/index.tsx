// Modules
import isString from 'lodash/isString';
import { isNil } from 'lodash';
import debounce from 'lodash/debounce';
// Types
import { TSelectedCities, TInitialState } from './types';

export class CitiesListManage {
  private initialState: TInitialState;

  constructor() {
    this.initialState = null;
    this.getCities = debounce(this.getCities, 400);
  }

  private selectedCities: TSelectedCities = ({ cities, search }) => {
    const isStringSearch = isString(search);
    const validSearch = isString(search) ? search.trim().toLowerCase() : undefined;
    let firstChart: string | null = null;

    const groupList = cities.reduce((acc, value) => {
      const isSearch = !isStringSearch || value.name.toLowerCase().includes(validSearch);

      if (isSearch) {
        if (value.name[0].toLocaleLowerCase() !== firstChart) {
          firstChart = value.name[0].toLocaleLowerCase();
          acc.push([value]);
        } else {
          acc[acc.length - 1].push(value);
        }
      }

      return [...acc];
    }, []);

    return groupList;
  };

  public initCities: TSelectedCities = (data) => {
    if (isNil(this.initialState)) {
      this.initialState = this.selectedCities(data);
    }

    return this.initialState;
  };

  public getCities: TSelectedCities = (data) => {
    const cities = this.selectedCities(data);

    data.setCitiesReact(cities);
    return cities;
  };
}
