// Utils
import { trackEvent } from '@utils/analytics';

const defaultParramsApps = {
  newTab: true,
  width: 96,
  height: 32,
};

const instalAppsParams = {
  newTab: true,
  width: 106,
  height: 32,
};

const appsConfig = [
  {
    name: 'android',
    link: 'https://play.google.com/store/apps/details?id=ru.chibbis',
    onClick: () => {
      trackEvent('download-app-footer-android');
    },
  },
  {
    name: 'ios',
    link: 'https://apps.apple.com/ru/app/chibbis-ru/id1249836426/',
    onClick: () => {
      trackEvent('download-app-footer-ios');
    },
  },
  {
    name: 'huawei',
    link: 'https://appgallery.huawei.com/#/app/C104625695/',
    onClick: () => {
      trackEvent('download-app-footer-huawei');
    },
  },
];

export const appsList = appsConfig.map((item) => ({
  ...defaultParramsApps,
  ...item,
}));

export const installAppsList = appsConfig.map((item) => ({
  ...instalAppsParams,
  ...item,
}));
