// Types
import { TConstructor } from './types';

export class HideScrollXManager {
  private readonly contentRef: HTMLDivElement;
  private readonly outerRef: HTMLDivElement;

  constructor(data: TConstructor) {
    this.contentRef = data.contentRef;
    this.outerRef = data.outerRef;
  }

  public readonly init = (): void => {
    this.setOuterHeight();
  };

  private readonly setOuterHeight = () => {
    this.outerRef.style.height = `${this.contentRef.clientHeight}px`;
  };
}
