import { AlienIcon, BirdIcon, PandaIcon, FoxIcon, LionIcon, HorseIcon } from 'chibbis-ui-kit';

export const AVATARS = ['👽', '🐔', '🐼', '🦊', '🦁', '🦄'];

export const getAvatarIcon = (
  avatar: string,
): ((props: { width: number; height: number }) => JSX.Element) => {
  switch (avatar) {
    case '👽': {
      return AlienIcon;
    }
    case '🐔': {
      return BirdIcon;
    }
    case '🐼': {
      return PandaIcon;
    }
    case '🦊': {
      return FoxIcon;
    }
    case '🦁': {
      return LionIcon;
    }
    case '🦄': {
      return HorseIcon;
    }
    default: {
      return AlienIcon;
    }
  }
};
