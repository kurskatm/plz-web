// Types
import { TCheckNewCityUrlName } from './types';

const HomePath = new RegExp('/[a-z]*');
const RestPath = new RegExp('/[a-z]*/restaurant/');
const ProfilePath = new RegExp('/[a-z]*/profile/');

const isHomePath = (path: string): boolean => {
  if (path.match(HomePath) && path.split('/').length === 2) {
    return true;
  }
  return false;
};

export const checkNewCityUrlName: TCheckNewCityUrlName = ({ citiesList, newCityId, history }) => {
  const newCity = citiesList.find(({ id }) => id === newCityId);
  if (isHomePath(history.location.pathname) || history.location.pathname.match(RestPath)) {
    history.replace(`/${newCity.urlName}`);
  } else if (history.location.pathname.match(ProfilePath)) {
    const pathElements = history.location.pathname.split('/').slice(2);
    const newPath = ['', newCity.urlName, ...pathElements].join('/');
    history.replace(newPath);
  }
};
