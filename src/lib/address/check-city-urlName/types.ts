// Main types
import { TCitiesList } from '@type/cities';
import { History } from 'history';

export interface TCheckNewCityUrlNameArgs {
  citiesList: TCitiesList;
  newCityId: string;
  history: History<unknown>;
}

export type TCheckNewCityUrlName = (data: TCheckNewCityUrlNameArgs) => void;
