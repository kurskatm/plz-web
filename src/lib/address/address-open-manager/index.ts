// Modules
import { fromEvent } from 'rxjs';
import { tap, throttleTime } from 'rxjs/operators';
import isNil from 'lodash/isNil';
// Types
import {
  TObservable,
  TSubscription,
  TCallAddressManagerConstructor,
  TInitialState,
  TShowRestaurantModals,
} from './types';

const EVENT_THROTTLE = 200;

export class OpenAddressManager {
  private readonly inputRef: HTMLInputElement;
  private readonly inputWrapperRef: HTMLDivElement;
  private initialState: TInitialState;
  private callModal: TShowRestaurantModals;

  private inputFocusObservable: TObservable;
  private inputFocusSubscribe: TSubscription;

  constructor(data: TCallAddressManagerConstructor) {
    this.inputRef = data.inputRef;
    this.inputWrapperRef = data.inputWrapperRef;
    this.initialState = {
      open: data.initialOpen,
    };
    this.callModal = data.callModal;

    this.inputFocusObservable = this.inputFocusEvent();
    this.inputFocusSubscribe = null;

    this.initEvents();
  }

  private initEvents = () => {
    if (!this.initialState.open) {
      this.inputFocusSubscribe = this.inputFocusObservable.subscribe();
    }
  };

  private inputFocusEvent = () =>
    fromEvent(this.inputWrapperRef, 'click').pipe(
      throttleTime(EVENT_THROTTLE),
      tap(() => {
        if (!this.initialState.open) {
          this.initialState.open = true;
          this.inputRef.focus();
          setTimeout(() => this.inputRef.blur());
          this.callModal({
            showModal: true,
            modalType: 'change-address',
          });
          this.inputFocusSubscribe.unsubscribe();
        }
      }),
    );

  public unsubscribe = (): void => {
    if (!isNil(this.inputFocusSubscribe) && this.inputFocusSubscribe.closed === false) {
      this.inputFocusSubscribe.unsubscribe();
      this.inputFocusSubscribe = null;
    }
    if (this.initialState.open) {
      this.initialState.open = false;
    }
  };
}
