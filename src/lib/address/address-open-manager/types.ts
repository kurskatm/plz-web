// Modules Types
import { Observable, Subscription } from 'rxjs';
import { TShowRestaurantModals } from '@actions/restaurant/modal/action-types';

export type TObservable = Observable<Event> | null;
export type TSubscription = Subscription | null;

export interface TInitialState {
  open: boolean;
}

export interface TCallAddressManagerConstructor {
  inputRef: HTMLInputElement;
  inputWrapperRef: HTMLDivElement;
  initialOpen: boolean;
  callModal: TShowRestaurantModals;
}

export { TShowRestaurantModals };
