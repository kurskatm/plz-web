// Modules
import { fromEvent } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, tap } from 'rxjs/operators';
import isNil from 'lodash/isNil';
import isEmpty from 'lodash/isEmpty';
// Types
import {
  TObservable,
  TSubscription,
  TAddressInputManagerConstructor,
  TFetchNewAddressesSuggest,
  TCitiesList,
} from './types';

const EVENT_THROTTLE = 400;

export class AddressInputManager {
  private readonly inputRef: HTMLInputElement;
  private readonly fetchSuggest: TFetchNewAddressesSuggest;
  private readonly cityId: string;
  private readonly citiesList: TCitiesList;
  private readonly initialAddress: string;

  private inputOnChangeObservable: TObservable;
  private inputOnChangeSubscribe: TSubscription;

  constructor(data: TAddressInputManagerConstructor) {
    this.inputRef = data.inputRef;
    this.cityId = data.cityId;
    this.citiesList = data.citiesList;
    this.initialAddress = data.initialAddress;
    this.fetchSuggest = data.fetchSuggest;

    this.inputOnChangeObservable = this.onChangeInputEvent();
    this.inputOnChangeSubscribe = this.inputOnChangeObservable.subscribe();
  }

  private getValidQuery = (query: string) => {
    const citiesNamesArray = this.citiesList.map((city) => city.name);
    const cityInQuery = citiesNamesArray.find((name) =>
      query.toLowerCase().match(name.toLowerCase()),
    );

    if (!isNil(cityInQuery)) {
      return query;
    }

    const currentCity = this.citiesList.find(({ id }) => id === this.cityId);
    return `${currentCity.name}, ${query}`;
  };

  private onChangeInputEvent = () =>
    fromEvent(this.inputRef, 'input').pipe(
      debounceTime(EVENT_THROTTLE),
      map(({ target }) => target instanceof HTMLInputElement && target.value.trim()),
      distinctUntilChanged(),
      filter((value) => !isEmpty(value) && value.length > 2 && value !== this.initialAddress),
      tap((value) => {
        this.fetchSuggest({
          query: this.getValidQuery(value),
          limit: 5,
        });
      }),
    );

  public unsubscribe = (): void => {
    if (!isNil(this.inputOnChangeSubscribe)) {
      this.inputOnChangeSubscribe.unsubscribe();
      this.inputOnChangeSubscribe = null;
    }
  };
}
