// Modules Types
import { Observable, Subscription } from 'rxjs';
// Main types
import { TFetchNewAddressesSuggest } from '@actions/profile/new-address/action-types';
import { TCitiesList } from '@type/cities';

export type TObservable = Observable<string> | null;
export type TSubscription = Subscription | null;

export interface TAddressInputManagerConstructor {
  cityId: string;
  citiesList: TCitiesList;
  fetchSuggest: TFetchNewAddressesSuggest;
  inputRef: HTMLInputElement;
  initialAddress: string;
}

export { TFetchNewAddressesSuggest, TCitiesList };
