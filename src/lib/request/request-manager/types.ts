import { Observable } from 'rxjs';
import { AjaxResponse } from 'rxjs/ajax';

export type TGetRequest = () => Observable<AjaxResponse>;
