// Modules
import { ajax } from 'rxjs/ajax';
// Main Types
import { TRequestParams } from '@type/request';
// Lib
import { EnhanceManager } from '../enhance-manager';
// Types
import { TGetRequest } from './types';

export class RequestManager extends EnhanceManager {
  constructor(request: TRequestParams) {
    super(request);

    this.getEnhances();
  }

  // @ts-ignore
  getRequest: TGetRequest = () => console.log('***', this.requestParams) || ajax(this.requestParams);
}
