// Modules
import isNil from 'lodash/isNil';
import isObject from 'lodash/isObject';
// @ts-ignore
// import { XMLHttpRequest } from 'xmlhttprequest';
// Main Types
import { TRequestParams } from '@type/request';
// Lib
import { WithOptions } from './with-options';
import { WithContentType } from './with-content-type';
// Types
import { TGetEnhances } from './types';

// eslint-disable-next-line @typescript-eslint/no-redeclare, prefer-destructuring
const XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;

function createXHR() {
  return new XMLHttpRequest();
}

export class EnhanceManager {
  public requestParams: TRequestParams;
  private WithOptions: WithOptions;
  private WithContentType: WithContentType;

  constructor(requestParams: TRequestParams) {
    this.requestParams = {
      createXHR,
      ...requestParams,
    };
    console.log(this.requestParams, '---------->>>>')

    this.WithOptions = new WithOptions();
    this.WithContentType = new WithContentType();
  }

  getEnhances: TGetEnhances = () => {
    if (!isObject(this.requestParams) || isNil(this.requestParams)) {
      return this.requestParams;
    }

    this.requestParams = this.WithOptions.init(this.requestParams);
    this.requestParams = this.WithContentType.init(this.requestParams);
    console.log(this.requestParams, '---------->>>>')

    return this.requestParams;
  };
}
