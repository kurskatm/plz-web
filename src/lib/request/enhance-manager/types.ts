// Main Types
import { TRequestParams } from '@type/request';

export type TWithOptions = (requestParams: TRequestParams) => TRequestParams;

export type TWithContentType = (requestParams: TRequestParams) => TRequestParams;

export type TGetEnhances = () => TRequestParams;
