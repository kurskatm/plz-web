// Types
import { TWithOptions } from './types';

export class WithOptions {
  init: TWithOptions = (requestParams) => {
    requestParams = {
      async: true,
      method: 'GET',
      ...requestParams,
    };

    return requestParams;
  };
}
