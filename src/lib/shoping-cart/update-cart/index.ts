// Modules
import assign from 'lodash/assign';
import isNil from 'lodash/isNil';
import omit from 'lodash/omit';
import set from 'lodash/set';
import isEqual from 'lodash/isEqual';
// Models
import { shopingCartRestaurantModel } from '@models/shoping-cart';
// Utils
import { Consts } from '@utils';
// Config
import { CONTANTS_CONFIG } from './config/constants';
// Types
import {
  TShopingCartModel,
  TRestaurantShopingCart,
  TShopingCartProduct,
  TConstructor,
  TInit,
  TDataForUpdate,
} from './types';

const { PENDING } = Consts.REQUEST.UI.STATUS;
export class UpdateShopingCartManager {
  private readonly initialState: TShopingCartModel | null;
  private state: TShopingCartModel | null;
  private readonly restaurant: TRestaurantShopingCart | null;
  private readonly product: TShopingCartProduct | null;
  private indexProduct: number | null;
  private cityId: string;

  constructor(data: TConstructor) {
    this.initialState = data.shopingCart;
    this.restaurant = assign({}, data.restaurant);
    this.product = assign({}, data.product);
    this.indexProduct = null;
    this.cityId = data.cityId;

    this.state = JSON.parse(JSON.stringify(this.initialState));
  }

  public readonly init: TInit = () => {
    this.indexProduct = this.findIndexProduct();
    this.setRestaurant();
    this.addProduct();
    this.filterProductsForPoints();
    this.subtractProduct();
    this.finalyOperations();

    return this.state;
  };

  public readonly dataForUpdate: TDataForUpdate = () => {
    const { state } = this;

    return {
      cityId: this.cityId,
      restaurantId: state.restaurant?.id,
      products: state.products.map((product) => ({
        id: product.id,
        count: product.count,
        inBonusPoint: product.inBonusPoint,
      })),
      cutleryCount: state.options.cutlery,
    };
  };

  private readonly findIndexProduct = () => {
    const index = this.initialState.products?.findIndex((product) =>
      isEqual(omit(product, ['count', 'modifiers']), omit(this.product, ['count', 'modifiers'])),
    );

    return index === -1 ? null : index;
  };

  private readonly setRestaurant = () => {
    if (this.initialState.restaurant?.id !== this.restaurant.id) {
      set(this.state, 'restaurant', this.restaurant);
      set(this.state, 'products', []);
    }
  };

  private readonly addProduct = () => {
    const isNilProduct = isNil(this.indexProduct);

    if (isNilProduct || this.initialState.products[this.indexProduct].count < this.product.count) {
      if (isNilProduct && this.product.count > 0) {
        this.state.products.push(this.product);
      } else {
        set(this.state, `products[${this.indexProduct}].count`, this.product.count);
      }
    }
  };

  private readonly filterProductsForPoints = () => {
    if (this.state.products.filter((product) => product.inBonusPoint).length > 1) {
      const newProducts = [].concat(
        this.state.products.filter((product) => !product.inBonusPoint),
        this.state.products.filter((product) => product.inBonusPoint).slice(1),
      );

      set(this.state, 'products', newProducts);
    }
  };

  private readonly subtractProduct = () => {
    const isNilProduct = isNil(this.indexProduct);

    if (!isNilProduct && this.initialState.products[this.indexProduct].count > this.product.count) {
      if (this.product.count > 0) {
        set(this.state, `products[${this.indexProduct}].count`, this.product.count);
      } else {
        const newProducts = [].concat(
          this.state.products.slice(0, this.indexProduct),
          this.state.products.slice(this.indexProduct + 1),
        );

        set(this.state, 'products', newProducts);
      }
    }
  };

  private readonly finalyOperations = () => {
    const isClearState = !this.state.products.length && !isNil(this.state.restaurant.id);
    const isNilCutlery = isNil(this.state.options.cutlery);

    this.state.save.ui.status = PENDING;

    if (isClearState) {
      set(this.state, 'restaurant', assign({}, shopingCartRestaurantModel));

      if (!isNilCutlery) {
        set(this.state, 'options.cutlery', null);
      }
    } else if (isNilCutlery) {
      set(this.state, 'options.cutlery', CONTANTS_CONFIG.DEFAULT_CUTLERY);
    }
  };
}
