import { removeCartId } from '@lib/cookie';
import { isNil } from 'lodash';
import { TmanageShopingCartSavingOnUpdate } from './types';

export const manageShopingCartSavingOnUpdate: TmanageShopingCartSavingOnUpdate = ({
  shopingCart,
  prevProductsLength,
  profile,
  shopingCartReset,
  shopingCartSave,
}) => {
  const canDeleteCart = shopingCart.products?.length === 0 && prevProductsLength > 0;

  if (shopingCart?.restaurant && shopingCart.products?.length > 0 && !canDeleteCart) {
    const data = {
      cartId: shopingCart.id,
      data: {
        cityId: profile.city,
        id: shopingCart.id,
        restaurantId: shopingCart.restaurant.id,
        cutleryCount: shopingCart.options.cutlery,
        products: shopingCart.products.map((item) => ({
          id: item.id,
          count: item.count,
          inBonusPoint: item.inBonusPoint,
          selectedModifierGroups: item.modifiers?.map((category) => ({
            id: category.id,
            selectedModifiers: category.modifiers?.map((modifier) => ({
              id: modifier.id,
            })),
          })),
        })),
      },
    };

    if (!isNil(profile.newGeoCode.data?.lat)) {
      // @ts-ignore
      data.lat = profile.newGeoCode.data.lat;
    }

    if (!isNil(profile.newGeoCode.data?.lng)) {
      // @ts-ignore
      data.lng = profile.newGeoCode.data.lng;
    }

    shopingCartSave(data);
  }
  if (shopingCart?.restaurant && canDeleteCart) {
    const data = {
      cartId: shopingCart.id,
    };
    removeCartId();
    shopingCartSave(data);
    // pause just waiting for shoping cart epic start's
    setTimeout(() => {
      shopingCartReset();
    }, 300);
  }
};
