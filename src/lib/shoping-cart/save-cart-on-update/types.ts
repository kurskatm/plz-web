import { TShopingCartModel } from '@models/shoping-cart/types';
import { TShopingCartSave } from '@actions/shoping-cart/save/action-types';
import { TShopingCartReset } from '@/actions/shoping-cart/action-types';
import { TProfileModel } from '@models/profile/types';

export type TmanageShopingCartSavingOnUpdate = ({
  shopingCart,
  prevProductsLength,
  profile,
  shopingCartReset,
  shopingCartSave,
}: {
  shopingCart: TShopingCartModel;
  prevProductsLength: number;
  profile: TProfileModel;
  shopingCartReset: TShopingCartSave;
  shopingCartSave: TShopingCartReset;
}) => void;
