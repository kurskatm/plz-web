// Main Types
import {
  TRestaurantShopingCart,
  TShopingCartProduct,
  TShopingCartModel,
  TShopingCartUpdateModel,
} from '@models/shoping-cart/types';
import { TRestaurantProductModifier } from '@type/restaurants/restaurant-products';

export interface TConstructor {
  restaurant: TRestaurantShopingCart;
  product: TShopingCartProduct;
  shopingCart: TShopingCartModel;
  cityId?: string;
}

export type TDataForUpdate = () => TShopingCartUpdateModel;
export type TInit = () => TShopingCartModel;

export {
  TShopingCartModel,
  TRestaurantShopingCart,
  TShopingCartProduct,
  TRestaurantProductModifier,
};
