/* eslint-disable @typescript-eslint/indent */
// Modules
import assign from 'lodash/assign';
import isNil from 'lodash/isNil';
import set from 'lodash/set';
// Models
// import { shopingCartRestaurantModel } from '@models/shoping-cart';
// Utils
import { Consts } from '@utils';
// Config
import { CONTANTS_CONFIG } from './config/constants';
// Types
import {
  TShopingCartModel,
  TRestaurantShopingCart,
  TShopingCartProduct,
  TConstructor,
  TInit,
  TDataForUpdate,
  TManagerOptions,
} from './types';

const { PENDING } = Consts.REQUEST.UI.STATUS;

// WTF is this abstraction responsible for? o_O
export class UpdateShopingCartFromProfileManager {
  private readonly initialState: TShopingCartModel | null;
  private state: TShopingCartModel | null;
  private readonly restaurant: TRestaurantShopingCart | null;
  private readonly products: TShopingCartProduct[];
  private readonly options: TManagerOptions;
  private cityId: string;

  constructor(data: TConstructor) {
    this.initialState = data.shopingCart;
    this.restaurant = assign({}, data.restaurant);
    this.products = assign([], data.products);
    this.options = assign({}, data.options);
    this.cityId = data.cityId;

    this.state = JSON.parse(JSON.stringify(this.initialState));
  }

  public readonly init: TInit = () => {
    this.setRestaurant();
    this.setProducts();
    this.finalyOperations();

    return this.state;
  };

  public readonly dataForUpdate: TDataForUpdate = () => {
    const { state } = this;

    return {
      cityId: this.cityId,
      restaurantId: state.restaurant?.id,
      products: state.products.map((product) => ({
        id: product.id,
        count: product.count,
        inBonusPoint: product.inBonusPoint,
        selectedModifierGroups: product.modifiers.map((category) => ({
          id: category.id,
          selectedModifiers: category.modifiers.map((modifier) => ({
            id: modifier.id,
          })),
        })),
      })),
      cutleryCount: state.options.cutlery,
    };
  };

  private readonly setRestaurant = () => {
    set(this.state, 'restaurant', this.restaurant);
    set(this.state, 'products', []);
  };

  private readonly setProducts = () => {
    set(this.state, 'products', this.products);
  };

  private readonly finalyOperations = () => {
    const isClearState = !this.state.products.length && !isNil(this.state.restaurant.id);
    const isNilCutlery = isNil(this.state.options.cutlery);

    this.state.save.ui.status = PENDING;

    if (isClearState) {
      // set(this.state, 'restaurant', assign({}, shopingCartRestaurantModel));

      if (!isNilCutlery) {
        set(this.state, 'options.cutlery', null);
      }
    } else if (isNilCutlery) {
      set(this.state, 'options.cutlery', CONTANTS_CONFIG.DEFAULT_CUTLERY);
    }

    set(this.state, 'options.total', this.options.total);
  };
}
