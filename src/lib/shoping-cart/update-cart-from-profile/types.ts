// Main Types
import {
  TRestaurantShopingCart,
  TShopingCartProduct,
  TShopingCartModel,
  TShopingCartUpdateModel,
} from '@models/shoping-cart/types';
import { TRestaurantProductModifier } from '@type/restaurants/restaurant-products';

export interface TManagerOptions {
  total: number;
}

export interface TConstructor {
  restaurant: TRestaurantShopingCart;
  products: TShopingCartProduct[];
  shopingCart: TShopingCartModel;
  cityId?: string;
  options: TManagerOptions;
}

export type TDataForUpdate = () => TShopingCartUpdateModel;
export type TInit = () => TShopingCartModel;

export {
  TShopingCartModel,
  TRestaurantShopingCart,
  TShopingCartProduct,
  TRestaurantProductModifier,
};
