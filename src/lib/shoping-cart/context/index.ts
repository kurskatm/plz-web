// Modules
import { createContext } from 'react';

export const ShopingCartFooterContext = createContext({ isPushed: false });
