// Types
import { TWeek } from './types';

export const WEEK: TWeek = [
  {
    id: 'monday',
    index: 1,
    name: 'Понедельник',
  },

  {
    id: 'tuesday',
    index: 2,
    name: 'Вторник',
  },

  {
    id: 'wednesday',
    index: 3,
    name: 'Среда',
  },

  {
    id: 'thursday',
    index: 4,
    name: 'Четверг',
  },

  {
    id: 'friday',
    index: 5,
    name: 'Пятница',
  },

  {
    id: 'saturday',
    index: 6,
    name: 'Суббота',
  },

  {
    id: 'sunday',
    index: 0,
    name: 'Воскресенье',
  },
];

export const WEEK_OBJECT = WEEK.reduce(
  (acc, value) => ({
    ...acc,
    [value.id]: value,
  }),
  {},
);
