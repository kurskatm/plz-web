export interface TWeekItem {
  id: string;
  index: number;
  name: string;
}

export type TWeek = TWeekItem[];
