// Modules
import dayjs from 'dayjs';
// Types
import { TGetDate } from './types';

export const getDate: TGetDate = (date) => {
  const YESTERDAY = new Date();
  YESTERDAY.setDate(YESTERDAY.getDate() - 1);
  const DATE_TODAY = dayjs(new Date()).locale('ru').format('D MMMM YYYY');
  const DATE_YESTERDAY = dayjs(YESTERDAY).locale('ru').format('D MMMM YYYY');
  const commentDate = dayjs(date).locale('ru').format('D MMMM YYYY');
  const commentTime = dayjs(date).format('HH:mm');
  const isToday = DATE_TODAY === commentDate;
  const isYesterday = DATE_YESTERDAY === commentDate;

  if (isToday) {
    return `Сегодня в ${commentTime}`;
  }

  if (isYesterday) {
    return `Вчера в ${commentTime}`;
  }

  return `${commentDate} в ${commentTime}`;
};
