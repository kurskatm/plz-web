function ticksToDate(ticks: number): Date {
  return Number.isInteger(ticks) ? new Date(ticks / 10000 + new Date(2001, 0, 1).getTime()) : null;
}

export const getFormatedCurrentDate = (time: string): string => {
  const currentDate = new Date();
  const day = `0${currentDate.getDate()}`.slice(-2);
  const month = `0${currentDate.getMonth() + 1}`.slice(-2);
  const year = currentDate.getFullYear();

  return `${year}-${month}-${day}T${time}:00Z`;
};

export const addMinutesToDate = (ticks: number, minutes: number): Date =>
  Number.isInteger(ticks)
    ? new Date(ticks / 10000 + (new Date(2001, 0, 1).getTime() + minutes * 60000))
    : null;

export const getHoursAndMinutes = (timestamp: number): string => {
  const date = new Date(ticksToDate(timestamp));
  const hours = date.getHours();
  const minutes = date.getMinutes();

  return `${hours >= 10 ? hours : `0${hours}`}:${minutes >= 10 ? minutes : `0${minutes}`}`;
};

export const roundToHalfAnHour = (number: number, increment: number, offset: number): number =>
  Math.round((number - offset) / increment) * increment + offset;

export const currTimeToTicks = (minutes?: number): number => {
  const date = new Date();
  const hours = date.getHours();

  return (
    (new Date(2001, 0, 1, hours, minutes || date.getMinutes()).getTime() -
      new Date(2001, 0, 1).getTime()) *
    10000
  );
};
