// Types
import { TRestaurantCardStockProps } from '@components/RestaurantCard/types';
import { TRibbonLabel } from './types';

export const getHoursAndMinutes = (timestamp: number): string => {
  const date = new Date(timestamp);
  const hours = date.getHours();
  const minutes = date.getMinutes();

  return `${hours}-${minutes}`;
};

export const getRibbonProps = ({
  isNew,
  hasSpecialPlacement,
  hasPaymentForPoints,
  hasPromotions,
}: TRestaurantCardStockProps): TRibbonLabel => {
  if (hasSpecialPlacement) {
    return {
      text: 'Рекомендуем',
      color: 'blue',
    };
  }
  if (hasPaymentForPoints) {
    return {
      text: 'За Баллы',
      color: 'orange',
    };
  }
  if (hasPromotions) {
    return {
      text: 'Акция',
      color: 'red',
    };
  }
  if (isNew) {
    return {
      text: 'Новый',
      color: 'green',
    };
  }

  return null;
};
