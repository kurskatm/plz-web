export type TRibbonLabel = {
  text: string;
  color: string;
};
