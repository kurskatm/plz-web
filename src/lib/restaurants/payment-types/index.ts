// Types
import { TPaymentTypes, TGetPaymentTypes, TGetInitialPaymentType } from './types';

export const paymentTypesConfig: TPaymentTypes = [
  {
    type: 1,
    name: 'наличными',
    value: 'cash',
  },

  {
    type: 2,
    name: 'картой курьеру',
    value: 'byCardToCourier',
  },

  {
    type: 4,
    name: 'картой онлайн',
    value: 'byCardOnline',
  },
];

export const getPaymentTypes: TGetPaymentTypes = (paymentTypes, isReverse = true) => {
  if (paymentTypes) {
    const types =
      paymentTypes &&
      paymentTypesConfig
        // eslint-disable-next-line no-bitwise
        .filter(({ type }) => paymentTypes & type);
    if (isReverse) {
      return types.reverse();
    }
    return types;
  }

  return [];
};

export const getInitialPaymentType: TGetInitialPaymentType = (paymentTypes) => {
  const types = getPaymentTypes(paymentTypes);
  if (types.map((pay) => pay.type).includes(4)) {
    return types.find(({ type }) => type === 4).value;
  }

  if (types.length) {
    return types[0].value;
  }

  return '';
};
