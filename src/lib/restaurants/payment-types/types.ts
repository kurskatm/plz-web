interface TPaymentType {
  type: number;
  name: string;
  value: string;
}

export type TPaymentTypes = TPaymentType[];

export type TGetPaymentTypes = (types: number, isReverse?: boolean) => TPaymentTypes;

export type TGetInitialPaymentType = (types: number) => string;
