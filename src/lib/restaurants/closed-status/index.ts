// Types
import { TGetClosedStatus } from './types';

export const getClosedStatus: TGetClosedStatus = ({ status }) => {
  switch (status) {
    case 1: {
      return {
        message: 'Сегодня открыт.',
        status: true,
      };
    }

    case 2: {
      return {
        message: 'Сегодня закрыт.',
        status: false,
      };
    }

    case 3: {
      return {
        message: 'Сегодня закрыт.', // Заблокирован
        status: false,
      };
    }

    default: {
      return {
        message: 'Не известно.',
        status: null,
      };
    }
  }
};
