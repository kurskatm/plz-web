interface TGetClosedStatusArgs {
  status: number | null;
}

interface TGetClosedStatusReturn {
  message: string;
  status: boolean | null;
}

export type TGetClosedStatus = (data: TGetClosedStatusArgs) => TGetClosedStatusReturn;
