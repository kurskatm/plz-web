export interface THardRestCategory {
  id: string;
  emoji?: string;
  name: string;
}

export type THardRestCategoryList = THardRestCategory[];
