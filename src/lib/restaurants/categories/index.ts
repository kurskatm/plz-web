// Types
import { THardRestCategoryList } from './types';

export const hardRestCategoriesList: THardRestCategoryList = [
  {
    id: '955cae82-682b-4521-a8ac-8be2e5f13497',
    emoji: '🔥',
    name: 'fire',
  },
  {
    id: '51b0be7d-9863-4d83-8a33-7e39ab29fef1',
    emoji: '',
    name: 'chibbcoin',
  },
  {
    id: '955cae82-favorite',
    emoji: '❤️',
    name: 'heart',
  },
];
