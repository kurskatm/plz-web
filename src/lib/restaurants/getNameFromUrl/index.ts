export const getNameFromUrl = (url: string): string => (url?.length ? url.split('/')[3] : '');
