import { useRef, useEffect } from 'react';

export const useInterval = (callback: () => void, delay: number, flag: boolean): void => {
  const savedCallback = useRef<() => void>();
  const savedIntervalId = useRef<number>();

  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  useEffect(() => {
    function tick() {
      if (savedCallback && savedCallback.current) {
        savedCallback.current();
      }
    }
    if (delay !== null && flag && !savedIntervalId.current) {
      // eslint-disable-next-line react-hooks/exhaustive-deps
      savedIntervalId.current = window.setInterval(tick, delay);
    }
    if (savedIntervalId.current && !flag) {
      clearInterval(savedIntervalId.current);
      savedIntervalId.current = null;
    }
  }, [delay, flag]);
};
