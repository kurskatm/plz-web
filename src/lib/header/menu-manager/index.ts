// Modules
import { fromEvent } from 'rxjs';
import { filter, tap, throttleTime } from 'rxjs/operators';
import isNil from 'lodash/isNil';
// Types
import {
  TObservable,
  TSubscription,
  TSetMenuIsOpenReact,
  TInitialMenuState,
  TMenuManagerConstructor,
} from './types';

const EVENT_THROTTLE = 100;

export class MenuManager {
  private readonly bodyRef: HTMLElement;
  private readonly closeIconRef: HTMLDivElement;
  private readonly menuTriggerRef: HTMLDivElement;
  private readonly menuRef: HTMLDivElement;
  private readonly cuisinesLinkRef: HTMLDivElement;

  private readonly setMenuIsOpenReact: TSetMenuIsOpenReact;
  private state: TInitialMenuState;

  private closeIconTriggerClickObservable: TObservable;
  private closeIconTriggerClickSubscription: TSubscription;

  private menuTriggerClickObservable: TObservable;
  private menuTriggerClickSubscription: TSubscription;

  private menuBodyClickObservable: TObservable;
  private menuBodyClickSubscription: TSubscription;

  private cuisinesLinkClickObservable: TObservable;
  private cuisinesLinkClickSubscription: TSubscription;

  constructor(data: TMenuManagerConstructor) {
    this.bodyRef = document.body;
    this.closeIconRef = data.closeIconRef;
    this.menuTriggerRef = data.menuTriggerRef;
    this.menuRef = data.menuRef;
    this.cuisinesLinkRef = data.cuisinesLinkRef;

    this.setMenuIsOpenReact = data.setMenuIsOpenReact;
    this.state = { open: data.initialMenuState };

    this.closeIconTriggerClickObservable = this.iconCloseTriggerClickEvent();
    this.closeIconTriggerClickSubscription = null;

    this.menuTriggerClickObservable = this.menuTriggerClickEvent();
    this.menuTriggerClickSubscription = null;

    this.menuBodyClickObservable = this.bodyClickEvent();
    this.menuBodyClickSubscription = null;

    this.cuisinesLinkClickObservable = this.cuisinesLinkClickEvent();
    this.cuisinesLinkClickSubscription = null;

    this.initEvents();
  }

  private initEvents = () => {
    if (this.state.open) {
      document.body.classList.add('body-no-scroll');
      this.closeIconTriggerClickSubscription = this.closeIconTriggerClickObservable.subscribe();
      this.menuBodyClickSubscription = this.menuBodyClickObservable.subscribe();
      this.cuisinesLinkClickSubscription = this.cuisinesLinkClickObservable.subscribe();
    } else {
      this.menuTriggerClickSubscription = this.menuTriggerClickObservable.subscribe();
    }
  };

  private isContainsMenu = (target: EventTarget) => {
    const isTargetMenuTriggerBody = target === this.menuTriggerRef;
    const isContainsMTBody = target instanceof HTMLElement && this.menuTriggerRef.contains(target);

    const isTargetMenuBody = target === this.menuRef;
    const isContainsMenuBody = target instanceof HTMLElement && this.menuRef.contains(target);

    const isTargetCloseIconBody = target === this.closeIconRef;
    const isContainsCloseIcon = target instanceof HTMLElement && this.closeIconRef.contains(target);

    const isNotTrigger = !isTargetMenuTriggerBody && !isContainsMTBody;
    const isNotMenuBody = !isTargetMenuBody && !isContainsMenuBody;

    const isNotIcon = !isTargetCloseIconBody && !isContainsCloseIcon;

    return isNotTrigger && isNotMenuBody && isNotIcon;
  };

  private iconCloseTriggerClickEvent = () =>
    fromEvent(this.closeIconRef, 'click').pipe(
      throttleTime(EVENT_THROTTLE),
      tap(() => {
        if (this.state.open) {
          this.state.open = false;
          this.setMenuIsOpenReact(false);

          this.closeIconTriggerClickSubscription.unsubscribe();
          this.menuBodyClickSubscription.unsubscribe();
          this.cuisinesLinkClickSubscription.unsubscribe();
          this.menuTriggerClickSubscription = this.menuTriggerClickObservable.subscribe();

          document.body.classList.remove('body-no-scroll');
        }
      }),
    );

  private menuTriggerClickEvent = () =>
    fromEvent(this.menuTriggerRef, 'click').pipe(
      throttleTime(EVENT_THROTTLE),
      tap(() => {
        if (!this.state.open) {
          this.state.open = true;
          this.setMenuIsOpenReact(true);
          this.menuTriggerClickSubscription = this.closeIconTriggerClickObservable.subscribe();
          this.menuBodyClickSubscription = this.menuBodyClickObservable.subscribe();
          this.cuisinesLinkClickSubscription = this.cuisinesLinkClickObservable.subscribe();
          this.closeIconTriggerClickSubscription = this.closeIconTriggerClickObservable.subscribe();

          document.body.classList.add('body-no-scroll');
        }
      }),
    );

  private bodyClickEvent = () =>
    fromEvent(this.bodyRef, 'click').pipe(
      throttleTime(EVENT_THROTTLE),
      filter(({ target }) => this.isContainsMenu(target)),
      tap(() => {
        if (this.state.open) {
          this.state.open = false;
          this.setMenuIsOpenReact(false);
          this.closeIconTriggerClickSubscription.unsubscribe();
          this.menuBodyClickSubscription.unsubscribe();
          this.cuisinesLinkClickSubscription.unsubscribe();
          this.menuTriggerClickSubscription = this.menuTriggerClickObservable.subscribe();

          document.body.classList.remove('body-no-scroll');
        }
      }),
    );

  private cuisinesLinkClickEvent = () =>
    fromEvent(this.cuisinesLinkRef, 'click').pipe(
      throttleTime(EVENT_THROTTLE),
      tap(() => {
        if (this.state.open) {
          this.state.open = false;
          this.setMenuIsOpenReact(false);
          this.closeIconTriggerClickSubscription.unsubscribe();
          this.menuBodyClickSubscription.unsubscribe();
          this.cuisinesLinkClickSubscription.unsubscribe();
          this.menuTriggerClickSubscription = this.menuTriggerClickObservable.subscribe();

          document.body.classList.remove('body-no-scroll');
        }
      }),
    );

  public unsubscribe = (): void => {
    if (
      !isNil(this.menuTriggerClickSubscription) &&
      this.menuTriggerClickSubscription.closed === false
    ) {
      this.menuTriggerClickSubscription.unsubscribe();
      this.menuTriggerClickSubscription = null;
    }

    if (!isNil(this.menuBodyClickSubscription) && this.menuBodyClickSubscription.closed === false) {
      this.menuBodyClickSubscription.unsubscribe();
      this.menuBodyClickSubscription = null;
    }

    if (
      !isNil(this.closeIconTriggerClickSubscription) &&
      this.closeIconTriggerClickSubscription.closed === false
    ) {
      this.closeIconTriggerClickSubscription.unsubscribe();
      this.closeIconTriggerClickSubscription = null;
    }

    if (
      !isNil(this.cuisinesLinkClickSubscription) &&
      this.cuisinesLinkClickSubscription.closed === false
    ) {
      this.cuisinesLinkClickSubscription.unsubscribe();
      this.cuisinesLinkClickSubscription = null;
    }

    document.body.classList.remove('body-no-scroll');
  };
}
