// Modules Types
import { Dispatch, SetStateAction } from 'react';
import { Observable, Subscription } from 'rxjs';

export type TObservable = Observable<Event> | null;
export type TSubscription = Subscription | null;

export type TSetMenuIsOpenReact = Dispatch<SetStateAction<boolean>>;

export interface TInitialMenuState {
  open: boolean;
}

export interface TMenuManagerConstructor {
  initialMenuState: boolean;
  closeIconRef: HTMLDivElement;
  menuTriggerRef: HTMLDivElement;
  menuRef: HTMLDivElement;
  cuisinesLinkRef: HTMLDivElement;
  setMenuIsOpenReact: TSetMenuIsOpenReact;
}
