// Modules
import { fromEvent } from 'rxjs';
import { filter, tap, throttleTime } from 'rxjs/operators';
import isNil from 'lodash/isNil';
// Types
import {
  TObservable,
  TSubscription,
  TInitialState,
  TAddressManagerConstructor,
  TSetReactCitiesListOpen,
  TCloseList,
  TNewGeocodeData,
} from './types';

const EVENT_THROTTLE = 200;

export class AddressManager {
  private readonly bodyRef: HTMLElement;
  private readonly inputRef: HTMLDivElement;
  private readonly listRef: HTMLDivElement;

  private readonly geocode: TNewGeocodeData;

  private readonly setReactCitiesListOpen: TSetReactCitiesListOpen;
  private initialState: TInitialState;

  private inputFocusObservable: TObservable;
  private inputFocusSubscribe: TSubscription;

  private bodyClickObservable: TObservable;
  private bodyClickSubscribe: TSubscription;

  constructor(data: TAddressManagerConstructor) {
    this.bodyRef = document.body;
    this.inputRef = data.inputRef;
    this.listRef = data.listRef;

    this.geocode = data.geocode;

    this.setReactCitiesListOpen = data.setReactCitiesListOpen;
    this.initialState = {
      open: data.initialOpen,
      geocode: data.geocode,
      fromHeader: data.fromHeader,
    };

    this.inputFocusObservable = this.inputFocusEvent();
    this.inputFocusSubscribe = null;

    this.bodyClickObservable = this.bodyClickEvent();
    this.bodyClickSubscribe = null;

    this.initEvents();
    this.forceOpenList();
  }
  private initEvents = () => {
    if (this.initialState.open) {
      // document.body.classList.add('body-no-scroll');
      this.bodyClickSubscribe = this.bodyClickObservable.subscribe();
    } else {
      this.inputFocusSubscribe = this.inputFocusObservable.subscribe();
    }
  };

  private isContainsListAddress = (target: EventTarget) => {
    const isTargetList = target === this.listRef;
    const isContainsList = target instanceof HTMLElement && this.listRef.contains(target);
    const isTargetInput = target === this.inputRef;
    const isContainsInput = target instanceof HTMLElement && this.inputRef.contains(target);

    return !isTargetList && !isContainsList && !isTargetInput && !isContainsInput;
  };

  private inputFocusEvent = () =>
    fromEvent(this.inputRef, 'click').pipe(
      throttleTime(EVENT_THROTTLE),
      tap(() => {
        if (!this.initialState.open) {
          this.initialState.open = true;
          this.setReactCitiesListOpen(true);
          this.inputFocusSubscribe.unsubscribe();
          this.bodyClickSubscribe = this.bodyClickObservable.subscribe();

          // document.body.classList.add('body-no-scroll');
        }
      }),
    );

  private bodyClickEvent = () =>
    fromEvent(this.bodyRef, 'click').pipe(
      throttleTime(EVENT_THROTTLE),
      filter(({ target }) => this.isContainsListAddress(target)),
      tap(() => {
        if (
          this.initialState.open &&
          (isNil(this.initialState.geocode) || this.initialState.geocode.precision === 'exact')
        ) {
          this.initialState.open = false;
          this.setReactCitiesListOpen(false);
          this.bodyClickSubscribe.unsubscribe();
          this.inputFocusSubscribe = this.inputFocusObservable.subscribe();

          // document.body.classList.remove('body-no-scroll');
        }
      }),
    );

  public closeList: TCloseList = () => {
    if (this.initialState.open) {
      this.initialState.open = false;
      this.setReactCitiesListOpen(false);
      this.bodyClickSubscribe.unsubscribe();
      this.inputFocusSubscribe = this.inputFocusObservable.subscribe();

      // document.body.classList.remove('body-no-scroll');
    }
  };

  private forceOpenList: TCloseList = () => {
    if (
      !this.initialState.open &&
      !isNil(this.initialState.geocode) &&
      this.initialState.geocode.precision !== 'exact' &&
      this.initialState.fromHeader
    ) {
      this.initialState.open = true;
      this.setReactCitiesListOpen(true);
      this.inputFocusSubscribe.unsubscribe();
      this.bodyClickSubscribe = this.bodyClickObservable.subscribe();
    }
  };

  public unsubscribe = (): void => {
    if (!isNil(this.inputFocusSubscribe) && this.inputFocusSubscribe.closed === false) {
      this.inputFocusSubscribe.unsubscribe();
      this.inputFocusSubscribe = null;
    }

    if (!isNil(this.bodyClickSubscribe) && this.bodyClickSubscribe.closed === false) {
      this.bodyClickSubscribe.unsubscribe();
      this.bodyClickSubscribe = null;
    }

    // document.body.classList.remove('body-no-scroll');
  };
}
