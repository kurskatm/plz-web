// Modules Types
import { Dispatch, SetStateAction } from 'react';
import { Observable, Subscription } from 'rxjs';
import { TNewGeocodeData } from '@type/new-geocode';

export type TObservable = Observable<Event> | null;
export type TSubscription = Subscription | null;

export type TSetReactCitiesListOpen = Dispatch<SetStateAction<boolean>>;

export interface TInitialState {
  open: boolean;
  geocode: TNewGeocodeData | null;
  fromHeader: boolean;
}

export interface TAddressManagerConstructor {
  inputRef: HTMLDivElement;
  initialOpen: boolean;
  listRef: HTMLDivElement;
  setReactCitiesListOpen: TSetReactCitiesListOpen;
  geocode: TNewGeocodeData | null;
  fromHeader: boolean;
}

export type TCloseList = () => void;

export { TNewGeocodeData };
