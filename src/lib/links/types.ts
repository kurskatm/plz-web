export interface TScheduleItem {
  id: string;
  name: string;
  router: string;
  isArrow: boolean;
  outside?: boolean;
}

export type TScheduleItemList = TScheduleItem[];

export interface TTScheduleGetMenuArgs {
  cityUrl?: string;
  profileAuth?: string;
}

export type TScheduleGetMenu = (data: TTScheduleGetMenuArgs) => TScheduleItemList;
