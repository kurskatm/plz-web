// Modules
import urlJoin from 'url-join';
import isString from 'lodash/isString';
import isNil from 'lodash/isNil';
// Utils
import { Consts } from '@utils';
// Types
import { TScheduleGetMenu } from './types';

const { INFO_PAGES } = Consts.ROUTES;

export const getFooterMenuList: TScheduleGetMenu = ({ cityUrl }) => {
  const prefix = isString(cityUrl) && cityUrl.length ? `/${cityUrl}` : '/';

  return [
    ...(cityUrl
      ? [
          {
            id: 'food-info-page',
            name: 'Бесплатная еда',
            router: urlJoin(prefix, INFO_PAGES.FOOD.PATH),
            isArrow: false,
          },
        ]
      : []),
    {
      id: 'help-info-page',
      name: 'Помощь',
      router: urlJoin(prefix, INFO_PAGES.HELP.PATH),
      isArrow: false,
    },
  ];

  return [];
};

export const getFooterCompanyLinks: TScheduleGetMenu = ({ cityUrl, profileAuth }) => {
  const prefix = isString(cityUrl) && cityUrl.length ? `/${cityUrl}` : '/';

  if (!isNil(profileAuth)) {
    return [
      {
        id: 'about-info-page',
        name: 'О проекте',
        router: urlJoin(prefix, INFO_PAGES.ABOUT.PATH),
        isArrow: false,
        outside: false,
      },
      {
        id: 'work-with-us-page',
        name: 'Работа у нас',
        router: INFO_PAGES.WORK_WITH_US.PATH,
        isArrow: false,
        outside: true,
      },
      {
        id: 'contacts-info-page',
        name: 'Связаться с нами',
        router: urlJoin(prefix, INFO_PAGES.CONTACTS.PATH),
        isArrow: false,
        outside: false,
      },
      {
        id: 'partners-info-page',
        name: 'Партнёрство для ресторанов',
        router: urlJoin(prefix, INFO_PAGES.PARTNERS.PATH),
        isArrow: false,
        outside: false,
      },
      {
        id: 'terms-info-page',
        name: 'Пользовательское соглашение',
        router: urlJoin(prefix, INFO_PAGES.REGLAMENT.PATH),
        isArrow: false,
        outside: false,
      },
    ];
  }

  return [
    {
      id: 'about-info-page',
      name: 'О проекте',
      router: urlJoin(prefix, INFO_PAGES.ABOUT.PATH),
      isArrow: false,
      outside: false,
    },
    {
      id: 'work-with-us-page',
      name: 'Работа у нас',
      router: INFO_PAGES.WORK_WITH_US.PATH,
      isArrow: false,
      outside: true,
    },
    {
      id: 'terms-info-page',
      name: 'Пользовательское соглашение',
      router: urlJoin(prefix, INFO_PAGES.REGLAMENT.PATH),
      isArrow: false,
      outside: false,
    },
  ];
};
