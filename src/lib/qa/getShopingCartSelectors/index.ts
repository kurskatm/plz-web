export const qaShopingCartSelector = (isMobile: boolean, text: string): string => {
  if (isMobile) return `${text}-mobile`;

  return `${text}-desktop`;
};
