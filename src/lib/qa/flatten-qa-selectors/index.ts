// Modules
import assign from 'lodash/assign';
import isString from 'lodash/isString';
import keys from 'lodash/keys';
// Maim Types
import { TFlattenSelectors } from '@type/qa/flatten-selectors';

// eslint-disable-next-line arrow-body-style
export const flattenSelectors: TFlattenSelectors = (nestedSelectors, prefix = '') => {
  return keys(nestedSelectors).reduce((selectors: Record<string, string>, key) => {
    const prefixedKey = prefix ? `${prefix}.${key}` : key;
    const value = nestedSelectors[key];

    if (isString(value)) {
      // eslint-disable-next-line no-param-reassign
      selectors[prefixedKey] = value;
    } else {
      assign(selectors, flattenSelectors(value, prefixedKey));
    }
    return selectors;
  }, {});
};
