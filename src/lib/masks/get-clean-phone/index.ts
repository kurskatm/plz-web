// Modules
import isString from 'lodash/isString';
// Types
import { TGetCleanPhoneInit } from './types';

const regExpClean = /[()\s-]+/g;
const regExpReplace = /^8/;

export class GetCleanPhone {
  init: TGetCleanPhoneInit = (phone) => {
    if (isString(phone) && phone.length) {
      return phone.replace(regExpClean, '').replace(regExpReplace, '+7');
    }

    return '';
  };
}
