export type TGetCleanPhoneInit = (phone: string) => string | null;
