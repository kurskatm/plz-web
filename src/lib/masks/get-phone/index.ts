// Modules
import isString from 'lodash/isString';
// Types
import { TGetPhoneInit } from './types';

// +7 800 123 45 67

export class GetPhone {
  init: TGetPhoneInit = (phone) => {
    if (isString(phone) && phone.length) {
      const phoneNumber = phone.replace(/[()\s-]+/g, '');

      return ''.concat(
        phoneNumber.substring(0, 2),
        ' ',
        phoneNumber.substring(2, 5),
        ' ',
        phoneNumber.substring(5, 8),
        ' - ',
        phoneNumber.substring(8, 10),
        ' - ',
        phoneNumber.substring(10, 12),
      );
    }

    return null;
  };
}
