// Modules
import { fromEvent } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, tap } from 'rxjs/operators';
import isNil from 'lodash/isNil';
import isEmpty from 'lodash/isEmpty';
// Main Types
import { TPromocodeValidate, TPromocodeReset } from '@actions/promocode/action-types';
// Types
import { TObservable, TSubscription, TCheckoutPromocodeManagerConstructor } from './types';

const EVENT_THROTTLE = 1000;

export class CheckoutPromocodeManager {
  private readonly inputRef: HTMLInputElement;
  private readonly closeIconRef: Element;
  private readonly fetchSuggest: TPromocodeValidate;
  private readonly resetSuggest: TPromocodeReset;

  private inputOnChangeObservable: TObservable;
  private inputOnChangeSubscribe: TSubscription;
  private inputOnClearObservable: TObservable;
  private inputOnClearSubscribe: TSubscription;

  constructor(data: TCheckoutPromocodeManagerConstructor) {
    this.inputRef = data.inputRef;
    this.closeIconRef = data.closeIconRef;
    this.fetchSuggest = data.fetchSuggest;
    this.resetSuggest = data.resetSuggest;

    this.inputOnChangeObservable = this.onChangeInputEvent();
    this.inputOnChangeSubscribe = this.inputOnChangeObservable.subscribe();
    this.inputOnClearObservable = this.onClearInputEvent();
    this.inputOnClearSubscribe = this.inputOnClearObservable.subscribe();
  }

  private onChangeInputEvent = () =>
    fromEvent(this.inputRef, 'input').pipe(
      debounceTime(EVENT_THROTTLE),
      map(({ target }) => target instanceof HTMLInputElement && target.value.trim()),
      distinctUntilChanged(),
      tap((value) => {
        if (!isEmpty(value) && value.length > 2) {
          this.fetchSuggest(value);
        } else {
          this.resetSuggest();
        }
      }),
    );

  private onClearInputEvent = () =>
    fromEvent(this.closeIconRef, 'click').pipe(
      debounceTime(EVENT_THROTTLE / 10),
      tap(() => {
        this.resetSuggest();
      }),
    );

  public unsubscribe = (): void => {
    if (!isNil(this.inputOnChangeSubscribe)) {
      this.inputOnChangeSubscribe.unsubscribe();
      this.inputOnChangeSubscribe = null;
      this.inputOnClearSubscribe.unsubscribe();
      this.inputOnClearSubscribe = null;
    }
  };
}
