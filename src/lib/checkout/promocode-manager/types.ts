// Modules Types
import { Observable, Subscription } from 'rxjs';
// Main Types
import { TPromocodeValidate, TPromocodeReset } from '@actions/promocode/action-types';
import { TContent } from '@/type/content';

export type TObservable = Observable<TContent> | null;
export type TSubscription = Subscription | null;

export interface TCheckoutPromocodeManagerConstructor {
  fetchSuggest: TPromocodeValidate;
  resetSuggest: TPromocodeReset;
  inputRef: HTMLInputElement;
  closeIconRef: Element;
}
