// Address
import { getAddress, setAddress, removeAddress } from './address';
// Cookie
import { Cookies } from './cookie-manage';
// City data
import { getCookieCityData, setCookieCityData, removeCookieCityData } from './city-data';
// Geo Code
import { getGeoCode, setGeoCode, removeGeoCode } from './geo-code';
// Pickup
import { getPickup, setPickup } from './pickup';
// Access Token
import { getAccessToken, setAccessToken } from './accessToken';
// Refresh Token
import { getRefreshToken } from './refreshToken';
// Profile
import { setProfileData, getProfileData, removeProfileData, getUserId } from './profile';
// CartId
import { getCartId, setCartId, removeCartId } from './cartId';
// Install mobile app
import { getMobileAppInstallShow, setMobileAppInstallShow } from './installMobileApp';
// Smart Banner
import { getCookieSmartBanner, setCookieSmartBanner, removeCookieSmartBanner } from './smartbanner';

export {
  // Address
  getAddress,
  setAddress,
  removeAddress,
  // Cookie
  Cookies,
  // City data
  getCookieCityData,
  setCookieCityData,
  removeCookieCityData,
  // Geo Code
  getGeoCode,
  setGeoCode,
  removeGeoCode,
  // Pickup
  getPickup,
  setPickup,
  // Profile,
  getProfileData,
  setProfileData,
  removeProfileData,
  getUserId,
  // Access Token
  getAccessToken,
  setAccessToken,
  // Cart Id
  getCartId,
  setCartId,
  removeCartId,
  // Install mobile app
  getMobileAppInstallShow,
  setMobileAppInstallShow,
  // Smart Banner
  getCookieSmartBanner,
  setCookieSmartBanner,
  removeCookieSmartBanner,
  getRefreshToken,
};
