// Utils
import { Consts } from '@utils';
// Lib
import { Cookies } from '../cookie-manage';
// Types
import { TGetAccessToken, TSetAccessToken } from './types';

const { PROFILE } = Consts.COOKIE.DATA;

export const getAccessToken: TGetAccessToken = () => Cookies.get(PROFILE.ACCESS_TOKEN);

export const setAccessToken: TSetAccessToken = (token: string) =>
  Cookies.set(PROFILE.ACCESS_TOKEN, token, { expires: 1800 });
