export type TGetAccessToken = () => unknown | null;

export type TSetAccessToken = (token: string) => string | null;
