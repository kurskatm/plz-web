// Utils
import { Consts } from '@utils';
// Lib
import { Cookies } from '../cookie-manage';
// Types
import { TGetRefreshToken, TSetRefreshToken } from './types';

const { PROFILE } = Consts.COOKIE.DATA;

export const getRefreshToken: TGetRefreshToken = () => Cookies.get(PROFILE.REFRESH_TOKEN);

export const setRefreshToken: TSetRefreshToken = (token: string) =>
  Cookies.set(PROFILE.ACCESS_TOKEN, token, { expires: 1800 });
