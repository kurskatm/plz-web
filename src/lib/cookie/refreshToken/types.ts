export type TGetRefreshToken = () => unknown | null;

export type TSetRefreshToken = (token: string) => string | null;
