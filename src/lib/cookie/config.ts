// Utils
import { Consts } from '@utils';
// Types
import { TOptionsCookieInitial } from './cookie-manage/types';

const { COOKIE } = Consts;

export const INITIAL_PARAMS_COOKIE: TOptionsCookieInitial = {
  expires: COOKIE.EXPIRES,
  path: '/',
  secure: true,
  httpOnly: false,
  sameSite: 'lax',
};
