// Utils
import { Consts } from '@utils';
// Lib
import { Cookies } from '../cookie-manage';
// Types
import { TGetAddress, TSetAddress, TRemoveAddress } from './types';

const { ADDRESS } = Consts.COOKIE.DATA;

export const getAddress: TGetAddress = () => Cookies.get(ADDRESS);

export const setAddress: TSetAddress = (address = null) => Cookies.set(ADDRESS, address);

export const removeAddress: TRemoveAddress = () => Cookies.remove(ADDRESS);
