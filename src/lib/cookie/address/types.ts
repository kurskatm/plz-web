export type TGetAddress = () => unknown | null;

interface TAddressData {
  name: string;
  description: string;
}

export type TSetAddress = (address: TAddressData | null) => string | null;

export type TRemoveAddress = () => void;
