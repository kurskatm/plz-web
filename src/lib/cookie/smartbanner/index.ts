// Utils
import { Consts } from '@utils';
// Lib
import { Cookies } from '../cookie-manage';
// Types
import { TGetCookieSmartBanner, TSetCookieSmartBanner, TRemoveCookieSmartBanner } from './types';

const { SMART_BANNER } = Consts.COOKIE.DATA;

export const getCookieSmartBanner: TGetCookieSmartBanner = () =>
  Cookies.get(SMART_BANNER) as string;

export const setCookieSmartBanner: TSetCookieSmartBanner = (data: string) =>
  Cookies.set(SMART_BANNER, data, { expires: 1 });

export const removeCookieSmartBanner: TRemoveCookieSmartBanner = () => Cookies.remove(SMART_BANNER);
