export type TGetCookieSmartBanner = () => string | null;

export type TSetCookieSmartBanner = (data: string) => string | null;

export type TRemoveCookieSmartBanner = () => void;
