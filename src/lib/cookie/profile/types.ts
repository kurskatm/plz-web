// Main Types
import { TProfileDataModel } from '@models/profile/types';

export type TGetProfileData = () => TProfileData | null;

export type TProfileData = {
  userId: string;
  access_token: string;
  refresh_token?: string;
};

export type TSetProfileData = ({ userId, access_token }: TProfileDataModel) => void;

export type TGetUserId = () => unknown | null;
