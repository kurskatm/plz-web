// Utils
import { Consts } from '@utils';
// Lib
import { Cookies } from '../cookie-manage';
// Types
import { TGetProfileData, TSetProfileData, TProfileData, TGetUserId } from './types';

const { PROFILE } = Consts.COOKIE.DATA;
const ONE_HOUR = 1 / 24;

export const getProfileData: TGetProfileData = (): TProfileData => ({
  userId: Cookies.get(PROFILE.USER_ID) as string,
  access_token: Cookies.get(PROFILE.ACCESS_TOKEN) as string,
});

export const setProfileData: TSetProfileData = ({ userId, access_token, refresh_token }) => {
  Cookies.set(PROFILE.USER_ID, userId, { expires: ONE_HOUR });
  Cookies.set(PROFILE.ACCESS_TOKEN, access_token, { expires: ONE_HOUR });
  Cookies.set(PROFILE.REFRESH_TOKEN, refresh_token, { expires: 3 });
};

export const removeProfileData: () => void = () => {
  Cookies.remove(PROFILE.USER_ID);
  Cookies.remove(PROFILE.ACCESS_TOKEN);
  Cookies.remove(PROFILE.REFRESH_TOKEN);
};

export const getUserId: TGetUserId = () => Cookies.get(PROFILE.USER_ID);
