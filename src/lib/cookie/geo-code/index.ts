// Utils
import { Consts } from '@utils';
// Lib
import { Cookies } from '../cookie-manage';
// Types
import { TGetGeoCode, TSetGeoCode, TRemoveGeoCode } from './types';

const { GEO_CODE } = Consts.COOKIE.DATA;

export const getGeoCode: TGetGeoCode = () => Cookies.get(GEO_CODE);

export const setGeoCode: TSetGeoCode = (geoCode) => Cookies.set(GEO_CODE, geoCode);

export const removeGeoCode: TRemoveGeoCode = () => Cookies.remove(GEO_CODE);
