// Main Types
import { TNewGeocodeData } from '@type/new-geocode';

export type TGetGeoCode = () => unknown | null;

export type TSetGeoCode = (geoCode: TNewGeocodeData) => string | null;

export type TRemoveGeoCode = () => void;
