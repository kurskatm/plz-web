export type TGetMobileAppInstallShow = () => unknown | null;

export type TSetMobileAppInstallShow = () => string | null;
