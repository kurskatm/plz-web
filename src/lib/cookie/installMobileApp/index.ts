// Utils
import { Consts } from '@utils';
// Lib
import { Cookies } from '../cookie-manage';
// Types
import { TGetMobileAppInstallShow, TSetMobileAppInstallShow } from './types';

const { INSTALL_MOBILE_APP_SHOW } = Consts.COOKIE.DATA;

export const getMobileAppInstallShow: TGetMobileAppInstallShow = () =>
  Cookies.get(INSTALL_MOBILE_APP_SHOW);

export const setMobileAppInstallShow: TSetMobileAppInstallShow = () =>
  Cookies.set(INSTALL_MOBILE_APP_SHOW, INSTALL_MOBILE_APP_SHOW, { expires: 10 });
