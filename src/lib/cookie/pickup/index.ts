// Utils
import { Consts } from '@utils';
// Lib
import { Cookies } from '../cookie-manage';
// Types
import { TGetPickup, TSetPickup } from './types';

const { PICKUP } = Consts.COOKIE.DATA;

export const getPickup: TGetPickup = () => Cookies.get(PICKUP);

export const setPickup: TSetPickup = (pickup = false) => Cookies.set(PICKUP, pickup);
