export type TGetPickup = () => unknown | null;

export type TSetPickup = (pickup: boolean) => string | null;
