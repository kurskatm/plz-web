export type TGetCartId = () => unknown | null;

export type TSetCartId = (cartId: string) => string | null;
