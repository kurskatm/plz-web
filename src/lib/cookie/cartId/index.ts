// Utils
import { Consts } from '@utils';
// Lib
import { Cookies } from '../cookie-manage';
// Types
import { TGetCartId, TSetCartId } from './types';

const { CART_ID } = Consts.COOKIE.DATA;
const { USER_ID } = Consts.COOKIE.DATA.PROFILE;

export const getCartId: TGetCartId = () => Cookies.get(CART_ID);

export const getUserId: TGetCartId = () => Cookies.get(USER_ID);

export const setCartId: TSetCartId = (id: string) => Cookies.set(CART_ID, id, { expires: 36000 });

export const removeCartId: () => void = () => {
  Cookies.remove(CART_ID);
};
