export type TGetCookieCityData = () => string | null;

export type TSetCookieCityData = (data: string) => string | null;

export type TRemoveCookieCityData = () => void;
