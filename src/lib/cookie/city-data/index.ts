// Utils
import { Consts } from '@utils';
// Lib
import { Cookies } from '../cookie-manage';
// Types
import { TGetCookieCityData, TSetCookieCityData, TRemoveCookieCityData } from './types';

const { CITY_DATA } = Consts.COOKIE.DATA;

export const getCookieCityData: TGetCookieCityData = () => Cookies.get(CITY_DATA) as string;

export const setCookieCityData: TSetCookieCityData = (data) => Cookies.set(CITY_DATA, data);

export const removeCookieCityData: TRemoveCookieCityData = () => Cookies.remove(CITY_DATA);
