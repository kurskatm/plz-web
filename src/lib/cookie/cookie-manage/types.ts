export type TIsValidCookieName = (name: unknown) => boolean;

export type TGet = (name?: string, doNotNeedToDecode?: boolean) => unknown | null;

export interface TOptionsCookieInitial {
  expires: number;
  path: string;
  secure: boolean;
  httpOnly: boolean;
  sameSite: 'strict' | 'lax' | 'none' | 'Strict' | 'Lax' | 'None';
}

export interface TOptionsCookieSet {
  expires?: number;
  path?: string;
  secure?: boolean;
  httpOnly?: boolean;
  domain?: string;
  sameSite?: 'strict' | 'lax' | 'none' | 'Strict' | 'Lax' | 'None';
}

export type TSet = (
  name: string,
  data: unknown,
  options?: TOptionsCookieSet,
  doNotNeedToEncode?: boolean,
) => string | null;

export type TRemove = (name: string) => void;
