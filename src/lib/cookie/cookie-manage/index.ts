// Modules
import jsCookie from 'js-cookie';
import isEmpty from 'lodash/isEmpty';
import isString from 'lodash/isString';
// lib
import { INITIAL_PARAMS_COOKIE } from '../config';
import { JsonManager } from '../../json';
// Types
import { TIsValidCookieName, TGet, TSet, TRemove } from './types';

const btoaImplementation = (text: string) => Buffer.from(text, 'binary').toString('base64');
const atobImplementation = (base64: string) => Buffer.from(base64, 'base64').toString('binary');
class CookieManage extends JsonManager {
  isValidCookieName: TIsValidCookieName = (name) => isString(name) && !isEmpty(name);

  get: TGet = (name, doNotNeedToDecode = false) => {
    if (this.isValidCookieName(name)) {
      const cookieData = jsCookie.get(name);

      if (isString(cookieData) && !isEmpty(cookieData)) {
        if (doNotNeedToDecode) {
          return this.jsonParse(cookieData);
        }

        const cookieDecode = decodeURIComponent(atobImplementation(cookieData));
        return this.jsonParse(cookieDecode);
      }

      return cookieData;
    }

    return null;
  };

  set: TSet = (name, data, options = {}, doNotNeedToEncode = false) => {
    if (this.isValidCookieName(name)) {
      const params = { ...INITIAL_PARAMS_COOKIE, ...options };
      let cookieData = data;

      cookieData = this.jsonStringify(data);

      if (!isString(cookieData)) {
        return null;
      }

      if (doNotNeedToEncode) {
        return jsCookie.set(name, cookieData, params);
      }

      cookieData = encodeURIComponent(cookieData);

      const stringData = btoaImplementation(cookieData as string);
      return jsCookie.set(name, stringData, params);
    }

    return null;
  };

  remove: TRemove = (name) => {
    if (this.isValidCookieName(name)) {
      jsCookie.remove(name);
    }
  };
}

export const Cookies = new CookieManage();
