// eslint-disable-next-line @typescript-eslint/no-var-requires
const path = require('path');
// Constants
const { MAIN_CONSTANTS } = require('../webpack/constants');
// Utils
const { getClientAliases } = require('../webpack/utils/aliases');

module.exports = {
  parser: '@typescript-eslint/parser',
  parserOptions: {
    tsconfigRootDir: path.resolve(MAIN_CONSTANTS.APP_DIR, 'src'),
    project: path.resolve(MAIN_CONSTANTS.APP_DIR, './src/tsconfig.json'),
    createDefaultProgram: true,
    ecmaVersion: 2018,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
    },
  },
  env: {
    browser: true,
    es2020: true,
  },
  extends: [
    'plugin:@typescript-eslint/recommended',
    'plugin:react/recommended',
    'plugin:react-hooks/recommended',
    'airbnb-typescript',
    'plugin:import/typescript',
  ],
  plugins: ['@typescript-eslint', 'import', 'react', 'optimize-regex', 'prettier', 'promise'],
  rules: {
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/ban-ts-comment': 'off',
    '@typescript-eslint/no-var-requires': 'off',
    '@typescript-eslint/lines-between-class-members': 'off',
    'linebreak-style': 0,
    'eslint linebreak-style': [0, 'error', MAIN_CONSTANTS.OS.IS_WINDOWS ? 'windows' : 'unix'],
    'import/prefer-default-export': 'off',
    'import/no-extraneous-dependencies': 'off',
    'no-console': 'off',
    'react/prop-types': 'off',
    'implicit-arrow-linebreak': 'off',
    'prefer-destructuring': ['error', { object: true, array: false }],
    'no-param-reassign': 'off',
    'react/jsx-props-no-spreading': 'off',
    'no-irregular-whitespace': 0,
    'import/no-cycle': 0,
  },
  settings: {
    'import/extensions': ['.js', '.ts', '.tsx', 'json'],
    'import/resolver': {
      alias: {
        map: Object.entries(getClientAliases()),
        extensions: ['.js', '.ts', '.tsx', 'json'],
      },
    },
  },
};
