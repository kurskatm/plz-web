// Modules
import { compose } from 'redux';
// Enhancers
import { withCheckCity } from '@enhancers/withCheckCity';

export const enhance = compose(withCheckCity());
