// Modules
import React, { FC, memo } from 'react';
import { Route as ReactRoute } from 'react-router-dom';
// Enhance
import { enhance } from './enhance';
// Types
import { TRouteProps } from './types';

const RouteComponent: FC<TRouteProps> = ({ path, exact, component }) => (
  <ReactRoute key={path} path={path} exact={exact} component={component} />
);

const RouteMemo = memo(RouteComponent);
export const Route = enhance(RouteMemo);
