import { LoadableComponent } from '@loadable/component';
import { Dispatch } from 'redux';

export interface TComputedMatch {
  isExact: boolean;
  params: Record<string, unknown>;
  path: string;
  url: string;
}

export interface TRouteProps {
  component: LoadableComponent<Record<string, unknown>>;
  computedMatch: TComputedMatch;
  dispatch: Dispatch;
  exact: boolean;
  isCity: boolean;
  path: string;
}
