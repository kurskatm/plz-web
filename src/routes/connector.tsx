// Modules
import React, { FC, memo } from 'react';
import { ConnectedRouter } from 'connected-react-router';
// Utils
import { Consts } from '@utils';
// Types
import { TProviderRouterComponent } from './types';

const { IS_CLIENT } = Consts.ENV;

const ProviderRouterComponent: FC<TProviderRouterComponent> = ({ children, history }) => {
  if (IS_CLIENT && history) {
    // @ts-ignore
    return <ConnectedRouter history={history}>{children}</ConnectedRouter>;
  }

  return <>{children}</>;
};

export const ProviderRouter = memo(ProviderRouterComponent);
