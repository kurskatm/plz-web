// Modules
import React, { FC, memo, useMemo } from 'react';
import { Switch } from 'react-router-dom';
// Components
import { Route } from './Route';
import { RedirectNotFound } from './RedirectNotFound';
// Routes
import { routerConfig } from './config';
// Types
import { TObjConfig } from './types';

const RoutesComponent: FC = () => {
  const renderRouter = useMemo(
    () => ({ path, exact, component }: TObjConfig) => (
      <Route key={path} path={path} exact={exact} component={component} />
    ),
    [],
  );

  return (
    <Switch>
      {routerConfig.map(renderRouter)}
      {/* <RedirectNotFound /> */}
    </Switch>
  );
};

export const Routes = memo(RoutesComponent);
