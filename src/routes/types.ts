// Modules
import { ReactNode } from 'react';
import { History } from 'history';
import { LoadableComponent } from '@loadable/component';

export interface TObjConfig {
  path: string;
  exact: boolean;
  component: LoadableComponent<Record<string, unknown>>;
}
export type TRouterConfig = TObjConfig[];

export interface TProviderRouterComponent {
  children: ReactNode;
  history: History;
}
