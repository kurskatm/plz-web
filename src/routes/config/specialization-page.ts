// Modules
import loadable from '@loadable/component';
import urlJoin from 'url-join';
// Utils
import { Consts } from '@utils';
// Routes
import { TGetRoutes } from '@type/routes';
// Main Types

const { ROUTES } = Consts;

export const routerSpecializationPageConfig: TGetRoutes = (prefix) => [
  {
    path: urlJoin(prefix, ROUTES.SPECIALIZATION.PATH),
    exact: true,
    component: loadable(() => import('@pages/home')),
  },
  {
    path: ROUTES.SPECIALIZATION.PATH,
    exact: true,
    component: loadable(() => import('@pages/home')),
  },
];
