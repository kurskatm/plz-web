// Modules
import loadable from '@loadable/component';
import urlJoin from 'url-join';
// Utils
import { Consts } from '@utils';
// Main Types
import { TGetRoutes } from '@type/routes';

const { ROUTES } = Consts;

const path = urlJoin(
  ROUTES.HOME.PATH,
  ROUTES.RESTAURANTS.RESTAURANT.MAIN.PATH,
  ROUTES.RESTAURANTS.RESTAURANT.MAIN.PARAMS,
  ROUTES.RESTAURANTS.RESTAURANT.MAIN.CLIENT_PARAMS,
);

export const routerRestaurantPageConfig: TGetRoutes = () => [
  {
    path,
    exact: false,
    component: loadable(() => import('@pages/restaurant/main')),
  },
];
