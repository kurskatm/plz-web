// Modules
import loadable from '@loadable/component';
import urlJoin from 'url-join';
// Utils
import { Consts } from '@utils';
// Main Types
import { TGetRoutes } from '@type/routes';

const { ROUTES } = Consts;

export const routerInfoPagesConfig: TGetRoutes = (prefix = '/') => [
  {
    path: urlJoin(prefix, ROUTES.INFO_PAGES.REGLAMENT.PATH),
    exact: true,
    component: loadable(() => import('@pages/info-pages/reglament')),
  },
  {
    path: '/reglament',
    exact: true,
    component: loadable(() => import('@pages/info-pages/reglament')),
  },
  {
    path: urlJoin(prefix, ROUTES.INFO_PAGES.ABOUT.PATH),
    exact: true,
    component: loadable(() => import('@pages/info-pages/about')),
  },
  {
    path: '/about',
    exact: true,
    component: loadable(() => import('@pages/info-pages/about')),
  },
  {
    path: urlJoin(prefix, ROUTES.INFO_PAGES.CONTACTS.PATH),
    exact: true,
    component: loadable(() => import('@pages/info-pages/contacts')),
  },
  {
    path: urlJoin(prefix, ROUTES.INFO_PAGES.HELP.PATH),
    exact: true,
    component: loadable(() => import('@pages/info-pages/help')),
  },
  {
    path: '/help',
    exact: true,
    component: loadable(() => import('@pages/info-pages/help')),
  },
  {
    path: urlJoin(prefix, ROUTES.INFO_PAGES.PARTNERS.PATH),
    exact: true,
    component: loadable(() => import('@pages/info-pages/partners')),
  },
  {
    path: urlJoin(prefix, ROUTES.INFO_PAGES.FEEDBACK.PATH),
    exact: true,
    component: loadable(() => import('@pages/info-pages/support')),
  },
  {
    path: '/food-for-points',
    exact: true,
    component: loadable(() => import('@pages/info-pages/freeFood')),
  },
  {
    path: urlJoin(prefix, ROUTES.INFO_PAGES.FOOD.PATH),
    exact: true,
    component: loadable(() => import('@pages/info-pages/freeFood')),
  },
];
