// Modules
import loadable from '@loadable/component';
import urlJoin from 'url-join';
// Utils
import { Consts } from '@utils';
// Types
import { TGetRoutes } from '@type/routes';

const { PROFILE_PAGES } = Consts.ROUTES;

export const routerProfilePagesConfig: TGetRoutes = (prefix = '/') => [
  {
    path: PROFILE_PAGES.INFO.PATH,
    exact: true,
    component: loadable(() => import('@pages/profile-pages/info')),
  },
  {
    path: PROFILE_PAGES.ORDERS.PATH,
    exact: true,
    component: loadable(() => import('@pages/profile-pages/orders')),
  },
  {
    path: PROFILE_PAGES.SCORES.PATH,
    exact: true,
    component: loadable(() => import('@pages/profile-pages/scores')),
  },
  {
    path: PROFILE_PAGES.REVIEWS.PATH,
    exact: true,
    component: loadable(() => import('@pages/profile-pages/reviews')),
  },
  {
    path: PROFILE_PAGES.FAVORITE.PATH,
    exact: true,
    component: loadable(() => import('@pages/profile-pages/favorite')),
  },
];
