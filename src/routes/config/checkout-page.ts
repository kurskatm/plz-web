// Modules
import loadable from '@loadable/component';
// Utils
import { Consts } from '@utils';
// Main Types
import { TGetRoutes } from '@type/routes';

const { ROUTES } = Consts;

export const routerCheckoutPageConfig: TGetRoutes = () => [
  {
    path: ROUTES.CHECKOUT.MAIN.PATH,
    exact: true,
    component: loadable(() => import('@pages/checkout')),
  },
  {
    path: ROUTES.CHECKOUT.SUCCESS.PATH,
    exact: true,
    component: loadable(() => import('@pages/checkout-success')),
  },
  {
    path: ROUTES.CHECKOUT.MAIN.PATH,
    exact: true,
    component: loadable(() => import('@pages/checkout')),
  },
  {
    path: ROUTES.CHECKOUT.SUCCESS.PATH,
    exact: true,
    component: loadable(() => import('@pages/checkout-success')),
  },
];
