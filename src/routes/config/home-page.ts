// Modules
import loadable from '@loadable/component';
// Utils
import { Consts } from '@utils';
// Types
import { TGetRoutes } from '@type/routes';

const { ROUTES } = Consts;

export const routerHomePageConfig: TGetRoutes = () => [
  {
    path: ROUTES.HOME.PATH,
    exact: true,
    component: loadable(() => import('@pages/home')),
  },
];
