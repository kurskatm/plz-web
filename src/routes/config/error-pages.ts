// Modules
import loadable from '@loadable/component';
// Utils
import { Consts } from '@utils';
// Main Types
import { TGetRoutes } from '@type/routes';

const { ROUTES } = Consts;

export const routerErrorPagesConfig: TGetRoutes = () => [
  {
    path: ROUTES.ERRORS.ERROR_404.PATH,
    exact: true,
    component: loadable(() => import('@pages/errors/404')),
  },
  {
    path: ROUTES.ERRORS.ERROR_500.PATH,
    exact: true,
    component: loadable(() => import('@pages/errors/500')),
  },
  {
    path: ROUTES.ERRORS.ERROR_404_TECHNICAL_WOKRS.PATH,
    exact: true,
    component: loadable(() => import('@pages/errors/TechnicalWorks')),
  },
  {
    path: ROUTES.ERRORS.ERROR_502.PATH,
    exact: true,
    component: loadable(() => import('@pages/errors/502')),
  },
];
