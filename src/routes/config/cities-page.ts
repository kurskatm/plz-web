// Modules
import loadable from '@loadable/component';
// Utils
import { Consts } from '@utils';
// Main Types
import { TGetRoutes } from '@type/routes';

const { ROUTES } = Consts;

export const routerCitiesPageConfig: TGetRoutes = () => [
  {
    path: ROUTES.CITIES.PATH,
    exact: true,
    component: loadable(() => import('@pages/cities')),
  },
];
