// Modules
import loadable from '@loadable/component';
import urlJoin from 'url-join';
// Utils
import { Consts } from '@utils';
// Routes
import { TGetRoutes } from '@type/routes';
// Main Types

const { ROUTES } = Consts;

export const routerSelectionPageConfig: TGetRoutes = (prefix) => [
  {
    path: urlJoin(prefix, ROUTES.SELECTION.PATH),
    exact: true,
    component: loadable(() => import('@pages/home/SelectionForScores')),
  },
  {
    path: ROUTES.SELECTION.PATH,
    exact: true,
    component: loadable(() => import('@pages/home/SelectionForScores')),
  },
];
