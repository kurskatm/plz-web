import { TRouterConfig } from '@type/routes';
import { Consts } from '@utils';
// Routes
import { routerCitiesPageConfig } from './cities-page';
import { routerErrorPagesConfig } from './error-pages';
import { routerInfoPagesConfig } from './info-pages';
import { routerHomePageConfig } from './home-page';
import { routerProfilePagesConfig } from './profile-pages';
import { routerRestaurantPageConfig } from './restaurant';
import { routerCheckoutPageConfig } from './checkout-page';
import { routerSpecializationPageConfig } from './specialization-page';
import { routerSelectionPageConfig } from './selection-page';

const { ROUTES } = Consts;

export const routerConfig: TRouterConfig = [
  // ...routerCitiesPageConfig(),
  ...routerErrorPagesConfig(),
  ...routerInfoPagesConfig(ROUTES.HOME.PATH),
  ...routerHomePageConfig(),
  ...routerProfilePagesConfig(ROUTES.HOME.PATH),
  ...routerSpecializationPageConfig(ROUTES.HOME.PATH),
  ...routerRestaurantPageConfig(),
  ...routerCheckoutPageConfig(ROUTES.HOME.PATH),
  ...routerSelectionPageConfig(ROUTES.HOME.PATH),
];
