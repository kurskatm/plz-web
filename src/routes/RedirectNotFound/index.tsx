// Modules
import React, { FC, memo } from 'react';
import { Redirect } from 'react-router-dom';
// Utils
import { Consts } from '@utils/consts';

const { ROUTES } = Consts;

const RedirectNotFoundComponent: FC = () => <Redirect to={ROUTES.ERRORS.ERROR_404.PATH} />;

export const RedirectNotFound = memo(RedirectNotFoundComponent);
