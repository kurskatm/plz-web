import React from 'react';
import { hydrate } from 'react-dom';
import { loadableReady } from '@loadable/component';

import { Consts } from '@utils';
import App from '@app';

const { ROOT } = Consts.DOM;
const DOM_ROOT = document.getElementById(ROOT);

loadableReady(() => {
  if (DOM_ROOT) {
    hydrate(<App />, DOM_ROOT);
  }
});

if (module.hot) {
  module.hot.accept();
}

export default App;
